<?php return array(
    'root' => array(
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'type' => 'project',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => NULL,
        'name' => 'laravel/laravel',
        'dev' => false,
    ),
    'versions' => array(
        'barryvdh/laravel-dompdf' => array(
            'pretty_version' => 'v0.8.2',
            'version' => '0.8.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../barryvdh/laravel-dompdf',
            'aliases' => array(),
            'reference' => '7dcdecfa125c174d0abe723603633dc2756ea3af',
            'dev_requirement' => false,
        ),
        'dnoegel/php-xdg-base-dir' => array(
            'pretty_version' => 'v0.1.1',
            'version' => '0.1.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../dnoegel/php-xdg-base-dir',
            'aliases' => array(),
            'reference' => '8f8a6e48c5ecb0f991c2fdcf5f154a47d85f9ffd',
            'dev_requirement' => false,
        ),
        'doctrine/inflector' => array(
            'pretty_version' => '1.4.4',
            'version' => '1.4.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/inflector',
            'aliases' => array(),
            'reference' => '4bd5c1cdfcd00e9e2d8c484f79150f67e5d355d9',
            'dev_requirement' => false,
        ),
        'dompdf/dompdf' => array(
            'pretty_version' => 'v0.8.6',
            'version' => '0.8.6.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../dompdf/dompdf',
            'aliases' => array(),
            'reference' => 'db91d81866c69a42dad1d2926f61515a1e3f42c5',
            'dev_requirement' => false,
        ),
        'erusev/parsedown' => array(
            'pretty_version' => '1.7.4',
            'version' => '1.7.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../erusev/parsedown',
            'aliases' => array(),
            'reference' => 'cb17b6477dfff935958ba01325f2e8a2bfa6dab3',
            'dev_requirement' => false,
        ),
        'guzzlehttp/guzzle' => array(
            'pretty_version' => '6.5.5',
            'version' => '6.5.5.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/guzzle',
            'aliases' => array(),
            'reference' => '9d4290de1cfd701f38099ef7e183b64b4b7b0c5e',
            'dev_requirement' => false,
        ),
        'guzzlehttp/promises' => array(
            'pretty_version' => '1.4.1',
            'version' => '1.4.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/promises',
            'aliases' => array(),
            'reference' => '8e7d04f1f6450fef59366c399cfad4b9383aa30d',
            'dev_requirement' => false,
        ),
        'guzzlehttp/psr7' => array(
            'pretty_version' => '1.8.2',
            'version' => '1.8.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/psr7',
            'aliases' => array(),
            'reference' => 'dc960a912984efb74d0a90222870c72c87f10c91',
            'dev_requirement' => false,
        ),
        'illuminate/auth' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.4.36',
            ),
        ),
        'illuminate/broadcasting' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.4.36',
            ),
        ),
        'illuminate/bus' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.4.36',
            ),
        ),
        'illuminate/cache' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.4.36',
            ),
        ),
        'illuminate/config' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.4.36',
            ),
        ),
        'illuminate/console' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.4.36',
            ),
        ),
        'illuminate/container' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.4.36',
            ),
        ),
        'illuminate/contracts' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.4.36',
            ),
        ),
        'illuminate/cookie' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.4.36',
            ),
        ),
        'illuminate/database' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.4.36',
            ),
        ),
        'illuminate/encryption' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.4.36',
            ),
        ),
        'illuminate/events' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.4.36',
            ),
        ),
        'illuminate/exception' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.4.36',
            ),
        ),
        'illuminate/filesystem' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.4.36',
            ),
        ),
        'illuminate/hashing' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.4.36',
            ),
        ),
        'illuminate/http' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.4.36',
            ),
        ),
        'illuminate/log' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.4.36',
            ),
        ),
        'illuminate/mail' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.4.36',
            ),
        ),
        'illuminate/notifications' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.4.36',
            ),
        ),
        'illuminate/pagination' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.4.36',
            ),
        ),
        'illuminate/pipeline' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.4.36',
            ),
        ),
        'illuminate/queue' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.4.36',
            ),
        ),
        'illuminate/redis' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.4.36',
            ),
        ),
        'illuminate/routing' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.4.36',
            ),
        ),
        'illuminate/session' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.4.36',
            ),
        ),
        'illuminate/support' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.4.36',
            ),
        ),
        'illuminate/translation' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.4.36',
            ),
        ),
        'illuminate/validation' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.4.36',
            ),
        ),
        'illuminate/view' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.4.36',
            ),
        ),
        'jakub-onderka/php-console-color' => array(
            'pretty_version' => 'v0.2',
            'version' => '0.2.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../jakub-onderka/php-console-color',
            'aliases' => array(),
            'reference' => 'd5deaecff52a0d61ccb613bb3804088da0307191',
            'dev_requirement' => false,
        ),
        'jakub-onderka/php-console-highlighter' => array(
            'pretty_version' => 'v0.4',
            'version' => '0.4.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../jakub-onderka/php-console-highlighter',
            'aliases' => array(),
            'reference' => '9f7a229a69d52506914b4bc61bfdb199d90c5547',
            'dev_requirement' => false,
        ),
        'jeremeamia/superclosure' => array(
            'pretty_version' => '2.4.0',
            'version' => '2.4.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../jeremeamia/superclosure',
            'aliases' => array(),
            'reference' => '5707d5821b30b9a07acfb4d76949784aaa0e9ce9',
            'dev_requirement' => false,
        ),
        'kylekatarnls/update-helper' => array(
            'pretty_version' => '1.2.1',
            'version' => '1.2.1.0',
            'type' => 'composer-plugin',
            'install_path' => __DIR__ . '/../kylekatarnls/update-helper',
            'aliases' => array(),
            'reference' => '429be50660ed8a196e0798e5939760f168ec8ce9',
            'dev_requirement' => false,
        ),
        'laravel/framework' => array(
            'pretty_version' => 'v5.4.36',
            'version' => '5.4.36.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../laravel/framework',
            'aliases' => array(),
            'reference' => '1062a22232071c3e8636487c86ec1ae75681bbf9',
            'dev_requirement' => false,
        ),
        'laravel/laravel' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'type' => 'project',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => NULL,
            'dev_requirement' => false,
        ),
        'laravel/tinker' => array(
            'pretty_version' => 'v1.0.10',
            'version' => '1.0.10.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../laravel/tinker',
            'aliases' => array(),
            'reference' => 'ad571aacbac1539c30d480908f9d0c9614eaf1a7',
            'dev_requirement' => false,
        ),
        'league/flysystem' => array(
            'pretty_version' => '1.1.5',
            'version' => '1.1.5.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../league/flysystem',
            'aliases' => array(),
            'reference' => '18634df356bfd4119fe3d6156bdb990c414c14ea',
            'dev_requirement' => false,
        ),
        'league/mime-type-detection' => array(
            'pretty_version' => '1.7.0',
            'version' => '1.7.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../league/mime-type-detection',
            'aliases' => array(),
            'reference' => '3b9dff8aaf7323590c1d2e443db701eb1f9aa0d3',
            'dev_requirement' => false,
        ),
        'maatwebsite/excel' => array(
            'pretty_version' => '2.1.30',
            'version' => '2.1.30.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../maatwebsite/excel',
            'aliases' => array(),
            'reference' => 'f5540c4ba3ac50cebd98b09ca42e61f926ef299f',
            'dev_requirement' => false,
        ),
        'monolog/monolog' => array(
            'pretty_version' => '1.26.1',
            'version' => '1.26.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../monolog/monolog',
            'aliases' => array(),
            'reference' => 'c6b00f05152ae2c9b04a448f99c7590beb6042f5',
            'dev_requirement' => false,
        ),
        'mtdowling/cron-expression' => array(
            'pretty_version' => 'v1.2.3',
            'version' => '1.2.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../mtdowling/cron-expression',
            'aliases' => array(),
            'reference' => '9be552eebcc1ceec9776378f7dcc085246cacca6',
            'dev_requirement' => false,
        ),
        'nesbot/carbon' => array(
            'pretty_version' => '1.39.1',
            'version' => '1.39.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nesbot/carbon',
            'aliases' => array(),
            'reference' => '4be0c005164249208ce1b5ca633cd57bdd42ff33',
            'dev_requirement' => false,
        ),
        'nikic/php-parser' => array(
            'pretty_version' => 'v4.12.0',
            'version' => '4.12.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nikic/php-parser',
            'aliases' => array(),
            'reference' => '6608f01670c3cc5079e18c1dab1104e002579143',
            'dev_requirement' => false,
        ),
        'paragonie/random_compat' => array(
            'pretty_version' => 'v2.0.20',
            'version' => '2.0.20.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../paragonie/random_compat',
            'aliases' => array(),
            'reference' => '0f1f60250fccffeaf5dda91eea1c018aed1adc2a',
            'dev_requirement' => false,
        ),
        'paypal/rest-api-sdk-php' => array(
            'pretty_version' => '1.14.0',
            'version' => '1.14.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../paypal/rest-api-sdk-php',
            'aliases' => array(),
            'reference' => '72e2f2466975bf128a31e02b15110180f059fc04',
            'dev_requirement' => false,
        ),
        'phenx/php-font-lib' => array(
            'pretty_version' => '0.5.2',
            'version' => '0.5.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phenx/php-font-lib',
            'aliases' => array(),
            'reference' => 'ca6ad461f032145fff5971b5985e5af9e7fa88d8',
            'dev_requirement' => false,
        ),
        'phenx/php-svg-lib' => array(
            'pretty_version' => 'v0.3.3',
            'version' => '0.3.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phenx/php-svg-lib',
            'aliases' => array(),
            'reference' => '5fa61b65e612ce1ae15f69b3d223cb14ecc60e32',
            'dev_requirement' => false,
        ),
        'phpoffice/phpexcel' => array(
            'pretty_version' => '1.8.2',
            'version' => '1.8.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpoffice/phpexcel',
            'aliases' => array(),
            'reference' => '1441011fb7ecdd8cc689878f54f8b58a6805f870',
            'dev_requirement' => false,
        ),
        'psr/event-dispatcher-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-message' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-message',
            'aliases' => array(),
            'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
            'dev_requirement' => false,
        ),
        'psr/http-message-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/log' => array(
            'pretty_version' => '1.1.4',
            'version' => '1.1.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/log',
            'aliases' => array(),
            'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
            'dev_requirement' => false,
        ),
        'psr/log-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0.0',
                1 => '1.0',
            ),
        ),
        'psy/psysh' => array(
            'pretty_version' => 'v0.9.12',
            'version' => '0.9.12.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psy/psysh',
            'aliases' => array(),
            'reference' => '90da7f37568aee36b116a030c5f99c915267edd4',
            'dev_requirement' => false,
        ),
        'ralouphie/getallheaders' => array(
            'pretty_version' => '3.0.3',
            'version' => '3.0.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ralouphie/getallheaders',
            'aliases' => array(),
            'reference' => '120b605dfeb996808c31b6477290a714d356e822',
            'dev_requirement' => false,
        ),
        'ramsey/uuid' => array(
            'pretty_version' => '3.9.4',
            'version' => '3.9.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ramsey/uuid',
            'aliases' => array(),
            'reference' => 'be2451bef8147b7352a28fb4cddb08adc497ada3',
            'dev_requirement' => false,
        ),
        'rhumsaa/uuid' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '3.9.4',
            ),
        ),
        'sabberworm/php-css-parser' => array(
            'pretty_version' => '8.3.1',
            'version' => '8.3.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sabberworm/php-css-parser',
            'aliases' => array(),
            'reference' => 'd217848e1396ef962fb1997cf3e2421acba7f796',
            'dev_requirement' => false,
        ),
        'swiftmailer/swiftmailer' => array(
            'pretty_version' => 'v5.4.12',
            'version' => '5.4.12.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../swiftmailer/swiftmailer',
            'aliases' => array(),
            'reference' => '181b89f18a90f8925ef805f950d47a7190e9b950',
            'dev_requirement' => false,
        ),
        'symfony/console' => array(
            'pretty_version' => 'v3.4.47',
            'version' => '3.4.47.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/console',
            'aliases' => array(),
            'reference' => 'a10b1da6fc93080c180bba7219b5ff5b7518fe81',
            'dev_requirement' => false,
        ),
        'symfony/css-selector' => array(
            'pretty_version' => 'v5.3.4',
            'version' => '5.3.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/css-selector',
            'aliases' => array(),
            'reference' => '7fb120adc7f600a59027775b224c13a33530dd90',
            'dev_requirement' => false,
        ),
        'symfony/debug' => array(
            'pretty_version' => 'v3.4.47',
            'version' => '3.4.47.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/debug',
            'aliases' => array(),
            'reference' => 'ab42889de57fdfcfcc0759ab102e2fd4ea72dcae',
            'dev_requirement' => false,
        ),
        'symfony/event-dispatcher' => array(
            'pretty_version' => 'v4.4.30',
            'version' => '4.4.30.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/event-dispatcher',
            'aliases' => array(),
            'reference' => '2fe81680070043c4c80e7cedceb797e34f377bac',
            'dev_requirement' => false,
        ),
        'symfony/event-dispatcher-contracts' => array(
            'pretty_version' => 'v1.1.9',
            'version' => '1.1.9.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/event-dispatcher-contracts',
            'aliases' => array(),
            'reference' => '84e23fdcd2517bf37aecbd16967e83f0caee25a7',
            'dev_requirement' => false,
        ),
        'symfony/event-dispatcher-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.1',
            ),
        ),
        'symfony/finder' => array(
            'pretty_version' => 'v3.4.47',
            'version' => '3.4.47.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/finder',
            'aliases' => array(),
            'reference' => 'b6b6ad3db3edb1b4b1c1896b1975fb684994de6e',
            'dev_requirement' => false,
        ),
        'symfony/http-foundation' => array(
            'pretty_version' => 'v3.4.47',
            'version' => '3.4.47.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/http-foundation',
            'aliases' => array(),
            'reference' => 'b9885fcce6fe494201da4f70a9309770e9d13dc8',
            'dev_requirement' => false,
        ),
        'symfony/http-kernel' => array(
            'pretty_version' => 'v3.4.49',
            'version' => '3.4.49.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/http-kernel',
            'aliases' => array(),
            'reference' => '5aa72405f5bd5583c36ed6e756acb17d3f98ac40',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-ctype' => array(
            'pretty_version' => 'v1.23.0',
            'version' => '1.23.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-ctype',
            'aliases' => array(),
            'reference' => '46cd95797e9df938fdd2b03693b5fca5e64b01ce',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-idn' => array(
            'pretty_version' => 'v1.23.0',
            'version' => '1.23.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-idn',
            'aliases' => array(),
            'reference' => '65bd267525e82759e7d8c4e8ceea44f398838e65',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-normalizer' => array(
            'pretty_version' => 'v1.23.0',
            'version' => '1.23.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-normalizer',
            'aliases' => array(),
            'reference' => '8590a5f561694770bdcd3f9b5c69dde6945028e8',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'v1.23.1',
            'version' => '1.23.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(),
            'reference' => '9174a3d80210dca8daa7f31fec659150bbeabfc6',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php56' => array(
            'pretty_version' => 'v1.20.0',
            'version' => '1.20.0.0',
            'type' => 'metapackage',
            'install_path' => NULL,
            'aliases' => array(),
            'reference' => '54b8cd7e6c1643d78d011f3be89f3ef1f9f4c675',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php70' => array(
            'pretty_version' => 'v1.20.0',
            'version' => '1.20.0.0',
            'type' => 'metapackage',
            'install_path' => NULL,
            'aliases' => array(),
            'reference' => '5f03a781d984aae42cebd18e7912fa80f02ee644',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php72' => array(
            'pretty_version' => 'v1.23.0',
            'version' => '1.23.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php72',
            'aliases' => array(),
            'reference' => '9a142215a36a3888e30d0a9eeea9766764e96976',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php80' => array(
            'pretty_version' => 'v1.23.1',
            'version' => '1.23.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php80',
            'aliases' => array(),
            'reference' => '1100343ed1a92e3a38f9ae122fc0eb21602547be',
            'dev_requirement' => false,
        ),
        'symfony/process' => array(
            'pretty_version' => 'v3.4.47',
            'version' => '3.4.47.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/process',
            'aliases' => array(),
            'reference' => 'b8648cf1d5af12a44a51d07ef9bf980921f15fca',
            'dev_requirement' => false,
        ),
        'symfony/routing' => array(
            'pretty_version' => 'v3.4.47',
            'version' => '3.4.47.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/routing',
            'aliases' => array(),
            'reference' => '3e522ac69cadffd8131cc2b22157fa7662331a6c',
            'dev_requirement' => false,
        ),
        'symfony/translation' => array(
            'pretty_version' => 'v4.3.11',
            'version' => '4.3.11.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/translation',
            'aliases' => array(),
            'reference' => '46e462be71935ae15eab531e4d491d801857f24c',
            'dev_requirement' => false,
        ),
        'symfony/translation-contracts' => array(
            'pretty_version' => 'v1.1.10',
            'version' => '1.1.10.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/translation-contracts',
            'aliases' => array(),
            'reference' => '84180a25fad31e23bebd26ca09d89464f082cacc',
            'dev_requirement' => false,
        ),
        'symfony/translation-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'symfony/var-dumper' => array(
            'pretty_version' => 'v3.4.47',
            'version' => '3.4.47.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/var-dumper',
            'aliases' => array(),
            'reference' => '0719f6cf4633a38b2c1585140998579ce23b4b7d',
            'dev_requirement' => false,
        ),
        'tightenco/collect' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v5.4.36',
            ),
        ),
        'tijsverkoyen/css-to-inline-styles' => array(
            'pretty_version' => '2.2.3',
            'version' => '2.2.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../tijsverkoyen/css-to-inline-styles',
            'aliases' => array(),
            'reference' => 'b43b05cf43c1b6d849478965062b6ef73e223bb5',
            'dev_requirement' => false,
        ),
        'vlucas/phpdotenv' => array(
            'pretty_version' => 'v2.6.7',
            'version' => '2.6.7.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../vlucas/phpdotenv',
            'aliases' => array(),
            'reference' => 'b786088918a884258c9e3e27405c6a4cf2ee246e',
            'dev_requirement' => false,
        ),
    ),
);
