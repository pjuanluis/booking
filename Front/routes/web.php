<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('booking');
});
Route::get('/admin', function () {
    $user = Auth::user();
    if (empty($user)) {
        return redirect()->route('login');
    }
    switch ($user->authenticatable_type) {
        case 'App\Client':
        case 'App\Agency':
            return redirect()->route('bookings.index');
            break;
        default:
            return redirect()->route('dashboard.index');
    }
});

Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', ['as' => 'login', 'uses' => 'Auth\LoginController@login']);
Route::post('/logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);

Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function() {
    Route::get('dashboard', ['as' => 'dashboard.index', 'uses' => 'Admin\DashboardController@index']);
    Route::post('dashboard/export', ['as' => 'dashboard.export', 'uses' => 'Admin\DashboardController@exportToPDF']);
    Route::get('bookings/count', ['as' => 'bookings.count', 'uses' => 'Admin\BookingController@count']);
    Route::get('bookings/news', ['as' => 'bookings.news', 'uses' => 'Admin\BookingController@newBookings']);
    Route::get('bookings/{booking}/logs', ['as' => 'bookings.logs', 'uses' => 'Admin\BookingController@logs']);
    Route::resource('bookings', 'Admin\BookingController');
    Route::post('bookings/class-status', ['as' => 'bookings.class-status', 'uses' => 'Admin\BookingController@updateClassStatus']);
    Route::post('bookings/export/excel', ['as' => 'bookings.export-excel', 'uses' => 'Admin\BookingController@exportToExel']);
    Route::post('bookings/export/csv', ['as' => 'bookings.export-csv', 'uses' => 'Admin\BookingController@exportToCsv']);
    Route::post('bookings/payments/mark-as-invoiced', ['as' => 'bookings.payments.mark_as_invoiced', 'uses' => 'Admin\BookingController@markPaymentsAsInvoiced']);

    Route::put('commands/{command}/accommodation', ['as' => 'commands.accommodations.update', 'uses' => 'Admin\CommandController@storeAccommodation']);

    Route::resource('clients', 'Admin\ClientController');
    Route::post('clients/export/csv', ['as' => 'clients.csv', 'uses' => 'Admin\ClientController@exportToCsv']);
    // Client credential
    Route::get('clients/{client}/credential', ['as' => 'clients.credential', 'uses' => 'Admin\ClientController@generateCredential']);

    Route::resource('agencies', 'Admin\AgencyController');
    Route::get('/accommodations/scheduler', ['as' => 'accommodations.scheduler', 'uses' => 'Admin\AccommodationController@scheduler']);
    Route::get('/accommodations', ['as' => 'accommodations.index', 'uses' => 'Admin\AccommodationController@index']);

    Route::resource('catalogues/housing', 'Admin\HousingController');
    Route::put('catalogues/housing/{housing}/activate',['as' => 'housing.activate', 'uses' => 'Admin\HousingController@activate']);

    // Housing rooms
    Route::get('catalogues/housing/{housing}/rooms',['as' => 'housing.rooms.index', 'uses' => 'Admin\HousingRoomController@index']);
    Route::post('catalogues/housing/{housing}/rooms',['as' => 'housing.rooms.store', 'uses' => 'Admin\HousingRoomController@store']);
    Route::get('catalogues/housing/{housing}/rooms/create',['as' => 'housing.rooms.create', 'uses' => 'Admin\HousingRoomController@create']);
    Route::get('catalogues/housing/rooms/{room}',['as' => 'housing.rooms.show', 'uses' => 'Admin\HousingRoomController@show']);
    Route::get('catalogues/housing/rooms/{room}/edit',['as' => 'housing.rooms.edit', 'uses' => 'Admin\HousingRoomController@edit']);
    Route::put('catalogues/housing/rooms/{room}',['as' => 'housing.rooms.update', 'uses' => 'Admin\HousingRoomController@update']);
    Route::delete('catalogues/housing/rooms/{room}',['as' => 'housing.rooms.destroy', 'uses' => 'Admin\HousingRoomController@destroy']);
    Route::put('catalogues/housing/rooms/{room}/activate',['as' => 'housing.rooms.activate', 'uses' => 'Admin\HousingRoomController@activate']);

    // Housing room package
    Route::get('catalogues/housing/rooms/{room}/packages',['as' => 'housing.rooms.packages.index', 'uses' => 'Admin\HousingRoomPackageController@index']);
    Route::post('catalogues/housing/rooms/{room}/packages',['as' => 'housing.rooms.packages.store', 'uses' => 'Admin\HousingRoomPackageController@store']);
    Route::get('catalogues/housing/rooms/{room}/packages/create',['as' => 'housing.rooms.packages.create', 'uses' => 'Admin\HousingRoomPackageController@create']);
    Route::get('catalogues/housing/rooms/packages/{housingRoomPackage}',['as' => 'housing.rooms.packages.show', 'uses' => 'Admin\HousingRoomPackageController@show']);
    Route::get('catalogues/housing/rooms/packages/{housingRoomPackage}/edit',['as' => 'housing.rooms.packages.edit', 'uses' => 'Admin\HousingRoomPackageController@edit']);
    Route::put('catalogues/housing/rooms/packages/{housingRoomPackage}',['as' => 'housing.rooms.packages.update', 'uses' => 'Admin\HousingRoomPackageController@update']);
    Route::delete('catalogues/housing/rooms/packages/{housingRoomPackage}',['as' => 'housing.rooms.packages.destroy', 'uses' => 'Admin\HousingRoomPackageController@destroy']);
    Route::put('catalogues/housing/rooms/packages/{housingRoomPackage}/activate',['as' => 'housing.rooms.packages.activate', 'uses' => 'Admin\HousingRoomPackageController@activate']);

    Route::get('reports/{report}', ['as' => 'reports.index',  'uses' => 'Admin\ReportController@index']);
    Route::post('reports/export', ['as' => 'reports.export',  'uses' => 'Admin\ReportController@exportToPDF']);
    Route::resource('users', 'Admin\UserController');

    Route::resource('groups', 'Admin\GroupController');
    Route::resource('schedules', 'Admin\ScheduleController');
    Route::post('schedules/print', ['as' => 'schedules.print', 'uses' => 'Admin\ScheduleController@print']);
    Route::post('schedules/export/csv', ['as' => 'schedules.csv', 'uses' => 'Admin\ScheduleController@exportToCsv']);

    Route::get('import', ['as' => 'import.index', 'uses' => 'Admin\ImportController@index']);
    Route::post('import', ['as' => 'import.store', 'uses' => 'Admin\ImportController@store']);
    Route::get('import/{id}', ['as' => 'import.show', 'uses' => 'Admin\ImportController@show']);

    // Centro de consumo
    Route::resource('consumption-centers', 'Admin\ConsumptionCenterController');
    Route::put('consumption-centers/{consumptionCenter}/activate', ['as' => 'consumption-centers.activate', 'uses' => 'Admin\ConsumptionCenterController@activate']);

    // Caja chica
    Route::resource('petty-cashes', 'Admin\PettyCashController');
    Route::post('petty-cashes/export/csv', ['as' => 'petty-cashes.csv', 'uses' => 'Admin\PettyCashController@exportToCsv']);

    // Room-types
    Route::get('catalogues/room-types', ['as' => 'room-types.index', 'uses' => 'Admin\RoomTypeController@index']);
    Route::get('catalogues/room-types/create', ['as' => 'room-types.create', 'uses' => 'Admin\RoomTypeController@create']);
    Route::post('catalogues/room-types', ['as' => 'room-types.store', 'uses' => 'Admin\RoomTypeController@store']);
    Route::get('catalogues/room-types/{roomType}', ['as' => 'room-types.show', 'uses' => 'Admin\RoomTypeController@show']);
    Route::get('catalogues/room-types/{roomType}/edit', ['as' => 'room-types.edit', 'uses' => 'Admin\RoomTypeController@edit']);
    Route::put('catalogues/room-types/{roomType}', ['as' => 'room-types.update', 'uses' => 'Admin\RoomTypeController@update']);
    Route::delete('catalogues/room-types/{roomType}', ['as' => 'room-types.destroy', 'uses' => 'Admin\RoomTypeController@destroy']);
    Route::put('catalogues/room-types/{roomType}/activate', ['as' => 'room-types.activate', 'uses' => 'Admin\RoomTypeController@activate']);

    // Countries
    Route::get('catalogues/countries', ['as' => 'countries.index', 'uses' => 'Admin\CountryController@index']);
    Route::get('catalogues/countries/create', ['as' => 'countries.create', 'uses' => 'Admin\CountryController@create']);
    Route::post('catalogues/countries', ['as' => 'countries.store', 'uses' => 'Admin\CountryController@store']);
    Route::get('catalogues/countries/{country}', ['as' => 'countries.show', 'uses' => 'Admin\CountryController@show']);
    Route::get('catalogues/countries/{country}/edit', ['as' => 'countries.edit', 'uses' => 'Admin\CountryController@edit']);
    Route::put('catalogues/countries/{country}', ['as' => 'countries.update', 'uses' => 'Admin\CountryController@update']);
    Route::delete('catalogues/countries/{country}', ['as' => 'countries.destroy', 'uses' => 'Admin\CountryController@destroy']);
    Route::put('catalogues/countries/{country}/activate', ['as' => 'countries.activate', 'uses' => 'Admin\CountryController@activate']);
    Route::put('catalogues/countries/{country}/sort', ['as' => 'countries.sort', 'uses' => 'Admin\CountryController@sort']);

    // Courses
    Route::resource('catalogues/courses', 'Admin\CourseController');
    Route::put('catalogues/courses/{package}/activate', ['as' => 'courses.activate', 'uses' => 'Admin\CourseController@activate']);

    // Experiences
    Route::post('catalogues/experiences', ['as' => 'experiences.store', 'uses' => 'Admin\ExperienceController@store']);
    Route::get('catalogues/experiences/create', ['as' => 'experiences.create', 'uses' => 'Admin\ExperienceController@create']);

    // People
    Route::get('catalogues/people', ['as' => 'people.index', 'uses' => 'Admin\PersonController@index']);
    Route::get('catalogues/people/create', ['as' => 'people.create', 'uses' => 'Admin\PersonController@create']);
    Route::post('catalogues/people', ['as' => 'people.store', 'uses' => 'Admin\PersonController@store']);
    Route::get('catalogues/people/{person}', ['as' => 'people.show', 'uses' => 'Admin\PersonController@show']);
    Route::get('catalogues/people/{person}/edit', ['as' => 'people.edit', 'uses' => 'Admin\PersonController@edit']);
    Route::put('catalogues/people/{person}', ['as' => 'people.update', 'uses' => 'Admin\PersonController@update']);
    Route::delete('catalogues/people/{person}', ['as' => 'people.destroy', 'uses' => 'Admin\PersonController@destroy']);

    // Places
    Route::get('catalogues/places', ['as' => 'places.index', 'uses' => 'Admin\PlaceController@index']);
    Route::get('catalogues/places/create', ['as' => 'places.create', 'uses' => 'Admin\PlaceController@create']);
    Route::post('catalogues/places', ['as' => 'places.store', 'uses' => 'Admin\PlaceController@store']);
    Route::get('catalogues/places/{place}', ['as' => 'places.show', 'uses' => 'Admin\PlaceController@show']);
    Route::get('catalogues/places/{place}/edit', ['as' => 'places.edit', 'uses' => 'Admin\PlaceController@edit']);
    Route::put('catalogues/places/{place}', ['as' => 'places.update', 'uses' => 'Admin\PlaceController@update']);
    Route::delete('catalogues/places/{place}', ['as' => 'places.destroy', 'uses' => 'Admin\PlaceController@destroy']);

    // Residences
    Route::resource('catalogues/residences', 'Admin\ResidenceController');

    // Rooms
    Route::resource("catalogues/residences/{residence}/rooms", "Admin\RoomController");

    // Roles
    Route::put("roles/{role}/activate", ['as' => 'roles.activate', 'uses' => "Admin\RoleController@activate"]);
    Route::resource("roles", "Admin\RoleController");

    // Moneda
    Route::get('currencies', ['as' => 'currencies.index', 'uses' => 'Admin\CurrencyController@index']);
    Route::get('currencies/create', ['as' => 'currencies.create', 'uses' => 'Admin\CurrencyController@create']);
    Route::post('currencies', ['as' => 'currencies.store', 'uses' => 'Admin\CurrencyController@store']);
    Route::get('currencies/{id}', ['as' => 'currencies.show', 'uses' => 'Admin\CurrencyController@show']);
    Route::get('currencies/{id}/edit', ['as' => 'currencies.edit', 'uses' => 'Admin\CurrencyController@edit']);
    Route::put('currencies/{id}', ['as' => 'currencies.update', 'uses' => 'Admin\CurrencyController@update']);
    Route::delete('currencies/{id}', ['as' => 'currencies.destroy', 'uses' => 'Admin\CurrencyController@destroy']);
});

// public
Route::group(['middleware' => ['auth_client']], function() {
    Route::get('booking', ['as' => 'public.bookings.index', 'uses' => 'BookingController@index']);
    Route::post('booking', ['as' => 'public.bookings.store', 'uses' => 'BookingController@store']);
    Route::get('booking/payment', ['as' => 'public.bookings.payment', 'uses' => 'BookingController@payment']);
    // Pay with conekta
    Route::post('booking/payment', ['as' => 'public.bookings.process_payment', 'uses' => 'BookingController@processPayment']);
    // Personal information
    Route::get('booking/your-info', ['as' => 'public.bookings.your-info', 'uses' => 'BookingController@personalInfo']);
    Route::post('booking/storeClient', ['as' => 'public.bookings.store-client', 'uses' => 'BookingController@storeClient']);
    // Paypal
    Route::post('paypal', array('as' => 'paypal.payment','uses' => 'PaypalController@createPayment',));
    Route::get('paypal', array('as' => 'paypal.execute','uses' => 'PaypalController@executePayment',));
    // Housing
    Route::get('housing/availability', ['as' => 'housing.availability', 'uses' => 'Admin\HousingController@availability']);
    Route::get('housing/occupancy', ['as' => 'housing.occupancy', 'uses' => 'Admin\HousingController@occupancy']);

    // Cancellation policies
    Route::get('cancellation-policies', function()
    {
        return view('cancellation-policies.index');
    })->name('cancellation-policies.index');
});
