const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

 //Scripts
mix.js('resources/assets/js/public-app.js', 'public/js/public-app.js')

// Styles
    .sass('resources/assets/sass/public/app.scss', 'public/css/public-app.css')
    .styles([
        './node_modules/sweetalert2/dist/sweetalert2.min.css',
        './node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
        './node_modules/font-awesome/css/font-awesome.min.css',
        './node_modules/fullcalendar/dist/fullcalendar.min.css',
        './node_modules/admin-lte/plugins/iCheck/minimal/aero.css',
        './node_modules/p-loading/dist/css/p-loading.min.css',
        './node_modules/croppie/croppie.css',
        './node_modules/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
        'resources/assets/libraries/select2/css/select2.min.css',
        'public/css/public-app.css'
    ], 'public/css/public-app.css')

    .copy('./node_modules/font-awesome/fonts/**', './public/fonts')
    .copy('./node_modules/admin-lte/plugins/iCheck/minimal/aero.png', './public/css/')

// Config
    .disableNotifications();
