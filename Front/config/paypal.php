<?php

return [
    // Paypal credentials
    'client_id' =>'ASxlQbZYlWJNJe7syfuSfyZhMwfWA-DTtdvs_B911MxCwBYzV6dJM5JRQXHR1B3pWIhMfIQB8oDcAuGR',
    'secret' => 'EGr6YoZ_-2qifWKXTQJAjslzw3DMisGwjfP1kTOgPGLjj0aOSpwZ2iHpTSLZzdQUFKKHaIPkQpkw5M9y',
    // SDK configuration
    'settings' => [
        // Available option 'sandbox' or 'live'
        'mode' => 'sandbox',
        // Specify the max request time in seconds
        'http.ConnectionTimeOut' => 1000,
        // Whether want to log to a file
        'log.LogEnabled' => true,
        // Specify the file that want to write on
        'log.FileName' => storage_path() . '/logs/paypal.log',
        /**
        * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
        *
        * Logging is most verbose in the 'FINE' level and decreases as you
        * proceed towards ERROR
        */
        'log.LogLevel' => 'FINE'
    ],
];

