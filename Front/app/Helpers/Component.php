<?php

namespace App\Helpers;

class Component
{
    public static function breadCrumb(array $items = [])
    {
        return view('components.breadcrumb')->with(['items' => $items]);
    }

    public static function pagination($paginationSize, $pivote, $totalPages, $params)
    {
        $size = $paginationSize;
        $limit = floor($size / 2);
        $isFirst = ($pivote == 1);
        $isLast = ($pivote == $totalPages);
        $paginate = [];
        $start = 0;
        $params = empty($params) ? [] : $params;
        $href = '';

        $params['page'] = 1;

        // Two items to help in pagination: First | <<
        $paginate[] = [
                'text' => 'First',
                'href' => $isFirst ? '' : '?' . make_url_params($params)
        ];
        $params['page'] = $pivote - 1;
        $paginate[] =[
            'text' => 'Previous', // <<
            'href' =>  $isFirst ? '' : '?' . make_url_params($params)
        ];
        
        // Items are more than limit to show.
        if (($pivote + $limit) >  $totalPages) {
            $start = $totalPages - $size;
        } elseif (($pivote - $limit) < 1) {
            $start = 1;
        } else {
            $start = $pivote -$limit;
        }

        // Items are less than total you want to show.
        if ($totalPages <= $size) {
            $start = 1;
            $end = $totalPages;
        } else {
            $end = $start + $size;
        }

        // Create items with number of page.
        for ($i = $start; $i <= $end ; $i++) {
            $params['page'] = $i;
            $paginate[] = [
                'text' => $i,
                'active' => ($i == $pivote) ? 'active' : '',
                'href' => '?' . make_url_params($params)
            ];
        }

        // Two items to help in pagination: >> | Last
        $params['page'] = $pivote + 1;
        $paginate[] = [ 
            'text' =>'Next', // >>
            'href' => $isLast ? '' : '?' . make_url_params($params)
        ];
        $params['page'] = $totalPages;
        $paginate[] = [
            'text' => 'Last',
            'href' => $isLast ? '' : '?' . make_url_params($params)
        ];

        // Render.
        return view('components.pagination')
            ->with(['paginate' =>$paginate]);
    }

    public static function table(
        $tableId = '',
        $tableTitle = '',
        array $titles = [],
        array $records = [],
        array $actions = []
    ) {
        return view('components.table')
            ->with([
                'tableId' => empty($tableId) ? '' : $tableId,
                'tableTitle' => empty($tableTitle) ? '' : $tableTitle,
                'titles' => $titles,
                'records' => $records,
                'actions' => $actions
            ]);
    }

    public static function fieldInput(array $attributes = [])
    {
        if (empty($attributes['type'])) {
            $attributes['type'] = 'text';
        }
        return view('components.fields.input')
            ->with(['attributes'=> $attributes]);
    }

    public static function fieldTextarea($value = '', array $attributes = [])
    {
        return view('components.fields.textarea')   
            ->with([
                'value' => $value,
                'attributes'=> $attributes
            ]); 
    }

    public static function fieldSubmit($text = '')
    {
        return view('components.fields.submit')
            ->with(['text' => empty($text) ? 'Save' : $text]);
    }
    
    public static function fieldCancel($text = '')
    {
        return view('components.fields.cancel')
            ->with(['text' => empty($text) ? 'Cancel' : $text]);
    }

    public static function fieldUploadImage(array $attributes = [])
    {
        return view('components.fields.upload-image')
            ->with(['attributes' => $attributes]);
    }

    public static function fieldSelect(
        array $attributes = [],
        array $options = []
    ) {
        return view('components.fields.select')
            ->with([
                'attributes' => $attributes,
                'options' => $options
            ]);
    }
}
