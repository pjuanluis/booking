<?php

namespace App\Helpers;

class Constant 
{
    const FORM_CREATE = 1;
    const FORM_EDIT = 2;
    const FORM_SHOW = 3;
    const FORM_COPY = 4;
}
