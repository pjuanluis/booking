<?php

namespace App\Extensions;

use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\GenericUser;
use Illuminate\Contracts\Session\Session;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use \Illuminate\Support\Facades\Cookie;

class ApiUserProvider implements UserProvider
{
    /**
     * The hasher implementation.
     *
     * @var \Illuminate\Contracts\Hashing\Hasher
     */
    protected $hasher;

    /**
     * The session used by the provider.
     *
     * @var \Illuminate\Contracts\Session\Session
     */
    protected $session;

    /**
     * The guzzle client used to fetch data from the experiencia api
     * @var \GuzzleHttp\Client
     */
    protected $client;

    /**
     * Create a new database user provider.
     *
     * @param  \Illuminate\Database\ConnectionInterface  $conn
     * @param  \Illuminate\Contracts\Hashing\Hasher  $hasher
     * @param  \Illuminate\Contracts\Session\Session  $session
     * @return void
     */
    public function __construct(Hasher $hasher, Session $session)
    {
        $this->session = $session;
        $this->hasher = $hasher;
        $this->client = new Client([
            'base_uri' => config('services.api.url'),
        ]);
    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array  $credentials
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        $response = null;
        try {
            $response = $this->client->request('POST', 'api/v1/oauth/token', [
                'form_params' => [
                    "grant_type" => "password",
                    "client_id" => config('services.api.client_id'),
                    "client_secret" => config('services.api.client_secret'),
                    "username" => $credentials['email'],
                    "password" => $credentials['password']
                ],
                'http_errors' => false
            ]);
        } catch (ClientException $e) {}
        if ($response !== null) {
            $response = json_decode($response->getBody());
            if (!isset($response->error)) {
                $response->grant_type = 'password';
                $this->session->put('oauth_token', (array) $response);
                $response = $this->client->request('GET', 'api/v1/users/me', [
                    'headers' => [
                        'authorization' => 'Bearer '.$response->access_token,
                        'accept' => 'application/vnd.experiencia.v1+json',
                        'content-type' => 'application/json'
                    ],
                    'http_errors' => false
                ]);
                $response = json_decode($response->getBody());
                $user = (array) $response->data;
                $user = new GenericUser($user);
                return $user;
            }
        }
        return null;
    }

    /**
     * Retrieve a user by their unique identifier.
     *
     * @param  mixed  $identifier
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveById($identifier)
    {
        $accessToken = $this->session->get('oauth_token')['access_token'];
        $response = $this->findUser($accessToken, $identifier);
        if (!isset($response->data)) {
            $tokens = $this->refreshToken($this->session->get('oauth_token')['refresh_token']);
            $accessToken = $tokens->access_token;
            if ($tokens === null) {
                return null;
            }
            $response = $this->findUser($accessToken, $identifier);
        }
        $user = (array) $response->data;
        return new GenericUser($user);
    }

    /**
     * Retrieve a user by their unique identifier and "remember me" token.
     *
     * @param  mixed  $identifier
     * @param  string  $token
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByToken($identifier, $token)
    {
        // Obtener tokens de la api
        $tokens = Cookie::get($this->getApiCookieName());
        // Buscar usuario usando el id y el remember token
        $response = $this->findUser($tokens['access_token'], $identifier, $token);
        // Si no se obtuvo respuesta positiva
        if (!isset($response->data)) {
            // Si ocurrió un error de autenticación, intentar refrescar el token de acceso
            if ($response->status_code == 500) {
                $tokens = $this->refreshToken($tokens['refresh_token']);
                if ($tokens === null) {
                    return null;
                }
                $response = $this->findUser($tokens->access_token, $identifier, $token);
            }
            else {
                return null;
            }
        }
        $user = (array) $response->data;
        return new GenericUser($user);
    }

    /**
     * Update the "remember me" token for the given user through the experiencia api.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @param  string  $token
     * @return void
     */
    public function updateRememberToken(Authenticatable $user, $token)
    {
        $this->client->request('PUT', 'api/v1/users/' . $user->getAuthIdentifier() . '/remember-token', [
            'headers' => [
                'authorization' => 'Bearer '.$this->session->get('oauth_token')['access_token'],
                'accept' => 'application/vnd.experiencia.v1+json',
                'content-type' => 'application/json'
            ],
            'json' => [
                "remember_token" => $token,
            ],
            'http_errors' => false
        ]);
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @param  array  $credentials
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials): bool
    {
        return $this->hasher->check(
            $credentials['password'], $user->getAuthPassword()
        );
    }

    /**
     * Get the generic user.
     *
     * @param  mixed  $user
     * @return \Illuminate\Auth\GenericUser|null
     */
    protected function getGenericUser($user)
    {
        if (! is_null($user)) {
            return new GenericUser((array) $user);
        }
    }

    /**
     * Find a user by the given id through the experiencia api
     * @param string $accessToken The API experiencia access token
     * @param int $id The user id
     * @param string $rememberToken The user remember token
     * @return array The user data
     */
    protected function findUser($accessToken, $id, $rememberToken = '')
    {
        $url = "api/v1/users/$id/show";
        if (!empty($rememberToken)) {
            $url .= "?remember_token=$rememberToken";
        }
        $response = $this->client->request('GET', $url, [
            'headers' => [
                'authorization' => 'Bearer '.$accessToken,
                'accept' => 'application/vnd.experiencia.v1+json',
                'content-type' => 'application/json'
            ],
            'http_errors' => false
        ]);
        return json_decode($response->getBody());
    }

    /**
     * Perform a refresh token request in the experiencia api
     * @return array|null
     */
    protected function refreshToken($refreshToken)
    {
        $response = $this->client->request('POST', 'api/v1/oauth/token', [
            'form_params' => [
                "grant_type" => "refresh_token",
                "client_id" => config('services.api.client_id'),
                "client_secret" => config('services.api.client_secret'),
                "refresh_token" => $refreshToken,
            ],
            'http_errors' => false
        ]);
        $response = json_decode($response->getBody());
        if (!isset($response->error)) {
            $response->grant_type = 'password';
            $this->session->put('oauth_token', (array) $response);
            return $response;
        }
        return null;
    }

    public function getApiCookieName()
    {
        return 'tokens_' . sha1('api_experiencia');
    }
}
