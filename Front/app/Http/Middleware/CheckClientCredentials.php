<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Session\Session;
use \Illuminate\Support\Facades\Cookie;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class CheckClientCredentials
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $client = new Client([
            'base_uri' => config('services.api.url'),
        ]);
        $response = null;
        if (!$request->session()->has('oauth_token')) {
            try {
                $response = $client->request('POST', 'api/v1/oauth/token', [
                    'form_params' => [
                        "grant_type" => "client_credentials",
                        "client_id" => config('services.api.client_id'),
                        "client_secret" => config('services.api.client_secret'),
                    ],
                    'http_errors' => false
                ]);
            } catch (ClientException $e) {}
            if ($response->getStatusCode() == 200) {
                $response = json_decode($response->getBody());
                $response->grant_type = 'client_credentials';
                $request->session()->put('oauth_token', (array) $response);
            }
        }
        return $next($request);
    }
}
