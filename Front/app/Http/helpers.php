<?php

function make_url_params(array $params)
{
    $url_params = '';
    $count = count($params);
    $i = 1;
    foreach ($params as $key => $value) {
        $url_params .= "$key=$value";
        if ($i < $count) {
            $url_params .= "&";
        }
        $i++;
    }
    return $url_params;
}

