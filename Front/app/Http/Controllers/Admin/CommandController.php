<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Constant;
use Illuminate\Support\Facades\Auth;
use Validator;

class CommandController extends Controller
{

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeAccommodation($id, Request $request)
    {
        $data = $request->all();
        // Enviar request al API
        $response = $this->put("api/v1/commands/$id/accommodations", $data, $request);
        // Si ocurrió algún error
        if ($request->expectsJson()) {
            return response()->json($response, $response->status_code);
        }
    }
}