<?php

namespace App\Http\Controllers\Admin;

use Constant;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CurrencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Crea url para obtener monedas
       $url = "api/v1/currencies?";
       $params = $this->prepareParams($request, ['page']);

       // Obtiene monedas del api
       $currencies = $this->get($url . make_url_params($params), $request);

       return view('currencies.index')
           ->with([
               'currencies' => $currencies,
               'params' => $params
           ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $currencies = $this->get('api/v1/currencies?no-paginate=1', $request);
        return View('currencies.form')
        ->with([
           'formAction' => route('currencies.store'),
           'formType' => Constant::FORM_CREATE,
           'currencies' => $currencies->data
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [];
        $validator = Validator::make([], []);
        $data['name'] = $request->get('name');
        $data['code'] = $request->get('code');
        $exchangeRates = $request->get('exchangeRate');
        $exchangeRatesArray = [];
        foreach ($exchangeRates as $key => $value) {
            if ($value != null && $value != 0) {
                $exchangeRatesArray[] = [
                    'to_currency_id' => $key,
                    'amount' => $value
                ];
            }
        }
        $data['exchange_rates'] = $exchangeRatesArray;
        $response = $this->post("api/v1/currencies", $data, $request);
        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        if ($response->status_code != 200 && $response->status_code != 201) {
            if (isset($response->errors)) {
                foreach ($response->errors as $key => $messages) {
                    foreach ($messages as $msg) {
                        $validator->errors()->add($key, $msg);
                    }
                }
            }
            return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
        }
        return redirect()->route('currencies.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $currency = $this->get("api/v1/currencies/$id?include=exchangeRates&exclude=exchangeRates.baseCurrency", $request);
        $currencies = $this->get('api/v1/currencies?no-paginate=1&?include=exchangeRates&exclude=exchangeRates.baseCurrency', $request);
        if (!isset($currencies->data)) {
            $currencies = [];
        }
        else {
            $currencies = $currencies->data;
        }
        return View('currencies.form')
            ->with([
                'formAction' => route('currencies.update', ['id' => $id]),
                'formType' => Constant::FORM_EDIT,
                'currency' => $currency->data,
                'currencies' => $currencies
            ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make([], []);
        $data = [];
        $data['name'] = $request->get('name');
        $data['code'] = $request->get('code');
        $exchangeRates = $request->get('exchangeRate');
        $exchangeRatesArray = [];
        foreach ($exchangeRates as $key => $value) {
            if ($value != null && $value != 0) {
                $exchangeRatesArray[] = [
                    'to_currency_id' => $key,
                    'amount' => $value
                ];
            }
        }
        $data['exchange_rates'] = $exchangeRatesArray;
        $response = $this->put("api/v1/currencies/$id", $data, $request);
        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        if ($response->status_code != 200) {
            if (isset($response->errors)) {
                foreach ($response->errors as $key => $messages) {
                    foreach ($messages as $msg) {
                        $validator->errors()->add($key, $msg);
                    }
                }
            }
            return redirect()
                ->back()
                ->withInput()
                ->withErrors($validator);
        }
        return redirect()->route('currencies.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $response = $this->delete("api/v1/currencies/$id", $request);
        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        return redirect()->back();
    }

    /**
     * Activate / Deactivate the specified currency.
     *
     * @param  int  $id
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function activate($id, Request $request)
    {
        $response = $this->put("api/v1/currencies/$id/activate", [], $request);
        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        return redirect()->route('currencies.index');
    }
}
