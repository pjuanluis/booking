<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Constant;

class GroupController extends Controller
{
    /**
     * Display a listing of clients.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Obtener listado de grupos por medio del API
        $url = 'api/v1/groups?include=schedules.experiences';
        $params = ['page' => 1];
        if (!empty($request->get('page'))) {
            $params['page'] = $request->get('page');
        }
        if (!empty($request->get('name'))) {
            $params['name'] = $request->get('name');
        }
        $groups = null;
        if ($request->expectsJson()) {
            $params['is_active'] = 1;
            $groups = $this->get($url . '&' . make_url_params($params), $request);
            if ($groups->success) {
                return response()->json($groups, 200);
            }
            else {
                return response()->json(['data' => [], 'meta' => ['pagination' => ['total' => 0]]]);
            }
        }
        else {
            $groups = $this->get($url . '&' . make_url_params($params), $request);
            return view('groups.index')
                    ->with([
                        'groups' => $groups,
                        'params' => $params
                    ]);
        }
    }

    /**
     * Show the form for creating a new group.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $countries = $this->get('api/v1/catalogues/countries', $request, false);
        $countries = !isset($countries->data) ? [] : $countries->data;
        $agencies = [];
        /*$agencies = $this->get('api/v1/agencies', $request, false);
        $agencies = !isset($agencies->data) ? [] : $agencies->data;*/
        return view('groups.form')->with([
            'group' => null,
            'countries' => $countries,
            'agencies' => $agencies,
            'formAction'=> route('groups.store'),
            'formType' => Constant::FORM_CREATE,
        ]);
    }

    /**
     * Store the group data.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $data = [];
        $validator = Validator::make([], []);
        $data = $request->all();
        if (!empty($data['clients'])) {
            $data['clients'] = explode(',', $data['clients']);
        }
        else {
            $data['clients'] = [];
        }
        $response = $this->post("api/v1/groups", $data, $request);
        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        if ($response->status_code != 200 && $response->status_code != 201) {
            if (isset($response->errors)) {
                foreach ($response->errors as $key => $messages) {
                    foreach ($messages as $msg) {
                        $validator->errors()->add($key, $msg);
                    }
                }
            }
            return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
        }
        return redirect()->route('groups.index');
    }

    /**
     * Show the form for editing the specified group.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id The group id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $group = $this->get("api/v1/groups/$id?include=clients:is_active(1),clients.commands.commandCourses.package.experiences", $request);
        $countries = $this->get('api/v1/catalogues/countries', $request, false);
        $countries = !isset($countries->data) ? [] : $countries->data;
        $agencies = [];
        /*$agencies = $this->get('api/v1/agencies', $request, false);
        $agencies = !isset($agencies->data) ? [] : $agencies->data;*/
        return view('groups.form')->with([
            'group' => $group->data,
            'countries' => $countries,
            'agencies' => $agencies,
            'formAction'=> route('groups.update', ['id' => $group->data->id]),
            'formType' => Constant::FORM_EDIT,
        ]);
    }

    /**
     * Update the specified group in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id The group id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = [];
        $validator = Validator::make([], []);
        $data = $request->all();
        if (!empty($data['clients'])) {
            $data['clients'] = explode(',', $data['clients']);
        }
        else {
            $data['clients'] = [];
        }
        $response = $this->put("api/v1/groups/$id", $data, $request);
        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        if ($response->status_code != 200 && $response->status_code != 201) {
            if (isset($response->errors)) {
                foreach ($response->errors as $key => $messages) {
                    foreach ($messages as $msg) {
                        $validator->errors()->add($key, $msg);
                    }
                }
            }
            return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
        }
        return redirect()->route('groups.index');
    }

    /**
     * Show the specified group data.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id The group id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $group = $this->get("api/v1/groups/$id?include=clients:is_active(1),clients.commands.commandCourses.package.experiences", $request);
        return view('groups.form')->with([
            'group' => $group->data,
            'formAction'=> '#',
            'formType' => Constant::FORM_SHOW,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $response = $this->delete("api/v1/groups/$id", $request);
        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        return redirect()->back();
    }
}
