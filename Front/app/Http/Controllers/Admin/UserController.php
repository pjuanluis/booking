<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\Constant;
use Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = 'Users';
        $url = 'api/v1/users?include=role';
        $params = $this->prepareParams($request, ['page']);

        $users = $this->get($url . '&' . make_url_params($params), $request);

        return view('users.index')
            ->with(compact(
                'title',
                'users',
                'params'
            ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $title = 'Create User';
        $formType = Constant::FORM_CREATE;
        $formAction = route('users.store');

        $roles = $this->get('api/v1/roles?is_active=1', $request);

        return view('users.form')
            ->with(compact(
                'title',
                'formType',
                'formAction',
                'roles'
            ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $validator = Validator::make([], []);

        $response = $this->post("api/v1/users", $data, $request);
        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);

        if ($response->status_code != 200 && $response->status_code != 201) {
            if (isset($response->errors)) {
                foreach ($response->errors as $key => $messages) {
                    foreach ($messages as $msg) {
                        $validator->errors()->add($key, $msg);
                    }
                }
            }
            return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
        }
        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $title = 'Edit User';
        $formType = Constant::FORM_EDIT;
        $formAction = route('users.update', ['id' => $id]);

        $user = $this->get("api/v1/users/$id?include=role", $request);
        $roles = $this->get('api/v1/roles', $request);

        return view('users.form')
            ->with(compact(
                'title',
                'formType',
                'formAction',
                'roles',
                'user'
            ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make([], []);
        $data = $request->except('_token');

        $response = $this->put("api/v1/users/$id", $data, $request);
        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        if ($response->status_code != 200) {
            if (isset($response->errors)) {
                foreach ($response->errors as $key => $messages) {
                    foreach ($messages as $msg) {
                        $validator->errors()->add($key, $msg);
                    }
                }
            }
            return redirect()
                ->back()
                ->withInput()
                ->withErrors($validator);
        }
        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $response = $this->delete("api/v1/users/$id", $request);
        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        return redirect()->back();
    }
}
