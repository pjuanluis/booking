<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Constant;

class AgencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $agencies = [];
        $params = $this->prepareParams($request, ['page']);
        $url = 'api/v1/agencies';
        $agencies = $this->get($url . '?' . make_url_params($params), $request);

        return view('agencies.index')
            ->with(compact('agencies', 'params'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $formType = Constant::FORM_CREATE;
        $formAction = route('agencies.store');

        return view('agencies.form')
            ->with(compact('formType', 'formAction'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make([], []);
        $data = $request->only('name', 'email', 'password', 'logo', 'code');

        foreach ($data as $k => $v) {
            if (empty($v)) {
                unset($data[$k]);
            }
        }

        $isLogoEmpty = false;
        foreach ($data['logo'] as $v) {
            if (empty($v)) {
                $isLogoEmpty = true;
                break;
            }
        }
        if ($isLogoEmpty) {
            unset($data['logo']);
        }

        $result = $this->storeInApi('api/v1/agencies', $data, $request);
        if($result !== true ) {
            return $result;
        }
        return redirect()->route('agencies.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $formType = Constant::FORM_EDIT;
        $formAction = route('agencies.update', ['id' => $id]);
        $agency = $this->get("api/v1/agencies/$id?include=file", $request)->data;

        return view('agencies.form')
            ->with(compact('agency', 'formType', 'formAction'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->only('updated_at', 'name', 'email', 'password', 'logo', 'code');

        foreach ($data as $k => $v) {
            if (empty($v)) {
                unset($data[$k]);
            }
        }

        $isLogoEmpty = false;
        foreach ($data['logo'] as $v) {
            if (empty($v)) {
                $isLogoEmpty = true;
                break;
            }
        }
        if ($isLogoEmpty) {
            unset($data['logo']);
        }
        // dd($data);

        $result = $this->updateInApi("api/v1/agencies/$id", $data, $request);
        if($result !== true ) {
            return $result;
        }
        return redirect()->route('agencies.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $response = $this->delete("api/v1/agencies/$id", $request);
        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        return redirect()->back();
    }
}
