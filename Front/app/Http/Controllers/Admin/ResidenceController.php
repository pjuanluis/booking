<?php

namespace App\Http\Controllers\Admin;

use Constant;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ResidenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $url = 'api/v1/residences';
        $params = ['page' => 1];
        if (!empty($request->get('page'))) {
            $params['page'] = $request->get('page');
        }

        // Filtrar por housing
        if (!empty($request->get('housing'))) {
            $params['housing'] = $request->get('housing');
        }
        $residences = $this->get($url . '?' . make_url_params($params), $request);
        return view('residences.index')
                ->with([
                    'residences' => $residences,
                    'params' => $params
                ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $title = "Create residence";
        $housing = $this->get("api/v1/housing?is_active=1", $request);
        return view('residences.form')->with([
            'housing' => $housing,
            'formAction'=> route('residences.store'),
            'formType' => Constant::FORM_CREATE,
            'title' => $title
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make([], []);

        $response = $this->post("api/v1/residences", $request->all(), $request);

        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        if ($response->status_code != 200 && $response->status_code != 201) {
            if (isset($response->errors)) {
                foreach ($response->errors as $key => $messages) {
                    foreach ($messages as $msg) {
                        $validator->errors()->add($key, $msg);
                    }
                }
            }
            return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
        }
        return redirect()->route('residences.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $title = "Show residence";
        $housing = $this->get("api/v1/housing?is_active=1", $request);
        $residence = $this->get("api/v1/residences/$id?include=housing", $request);
        return view('residences.form')->with([
            'housing' => $housing,
            'formAction'=> '',
            'formType' => Constant::FORM_SHOW,
            'title' => $title,
            'residence' => $residence->data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $title = "Edit residence";
        $housing = $this->get("api/v1/housing?is_active=1", $request);
        $residence = $this->get("api/v1/residences/$id?include=housing", $request);
        return view('residences.form')->with([
            'housing' => $housing,
            'formAction'=> route('residences.update', ['id' => $id]),
            'formType' => Constant::FORM_EDIT,
            'title' => $title,
            'residence' => $residence->data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make([], []);

        $response = $this->put("api/v1/residences/$id", $request->all(), $request);

        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        if ($response->status_code != 200 && $response->status_code != 201) {
            if (isset($response->errors)) {
                foreach ($response->errors as $key => $messages) {
                    foreach ($messages as $msg) {
                        $validator->errors()->add($key, $msg);
                    }
                }
            }
            return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
        }
        return redirect()->route('residences.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $response = $this->delete("api/v1/residences/$id", $request);
        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        return redirect()->back();
    }
}
