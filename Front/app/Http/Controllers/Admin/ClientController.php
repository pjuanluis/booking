<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Validator;
use Constant;
use Excel;

class ClientController extends Controller
{
    /**
     * Display a listing of clients.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Obtener listado de clientes por medio del API
        $url = 'api/v1/clients';
        $params = ['page' => 1];
        if (!empty($request->get('page'))) {
            $params['page'] = $request->get('page');
        }
        if (!empty($request->get('name'))) {
            $params['name'] = $request->get('name');
        }
        if (!empty($request->get('country_id'))) {
            $params['country_id'] = $request->get('country_id');
        }
        if (!empty($request->get('agency_id'))) {
            $params['agency_id'] = $request->get('agency_id');
        }
        if (!empty($request->get('experiences'))) {
            $params['experiences'] = $request->get('experiences');
        }
        if (!empty($request->get('course_from'))) {
            $params['course_from'] = $request->get('course_from');
        }
        $clients = null;
        if ($request->expectsJson()) {
            if (!empty($request->get('no-paginate'))) {
                $params['no-paginate'] = $request->get('no-paginate');
            }
            if (!empty($request->get('include'))) {
                $params['include'] = $request->get('include');
            }
            $params['is_active'] = 1;
            $clients = $this->get($url . '?' . make_url_params($params), $request);
            if ($clients->success) {
                return response()->json($clients, 200);
            }
            else {
                return response()->json(['data' => [], 'meta' => ['pagination' => ['total' => 0]]]);
            }
        }
        else {
            $clients = $this->get($url . '?' . make_url_params($params), $request);
            $clientsWithoutPagination = $this->get($url . '?no-paginate=1', $request);
            return view('clients.index')
                    ->with([
                        'clients' => $clients,
                        'clientsWithoutPagination' => $clientsWithoutPagination,
                        'params' => $params
                    ]);
        }
    }
    /**
     * Show the form for creating a new client.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $countries = $this->get('api/v1/catalogues/countries', $request);
        $countries = !isset($countries->data) ? [] : $countries->data;
        $pickup = $this->get('api/v1/catalogues/pickup', $request);
        $pickup = !isset($pickup->data) ? [] : $pickup->data;
        $genders = $this->get('api/v1/catalogues/genders', $request);
        return view('clients.form')->with([
            'client' => null,
            'formAction'=> route('clients.store'),
            'formType' => Constant::FORM_CREATE,
            'countries' => $countries,
            'pickup' => $pickup,
            'genders' => $genders,
        ]);
    }

    /**
     * Store the client data.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $data = [];
        $validator = Validator::make([], []);
        $data = $request->all();
        $data['spanish_knowledge_duration'] = '0';
        if (!empty($data['spanish_level'])) {
            if ($data['spanish_level'] !== 'none') {
                $data['spanish_knowledge_duration'] = $data['spanish_knowledge_number'] . ' ' . $data['spanish_knowledge_type'];
            }
        }
        $data['surf_experience_duration'] = '0';
        if (!empty($data['has_surf_experience'])) {
            $data['surf_experience_duration'] = $data['surf_experience_number'] . ' ' . $data['surf_experience_type'];
        }
        $data['volunteer_experience_duration'] = '0';
        if (!empty($data['has_volunteer_experience'])) {
            $data['volunteer_experience_duration'] = $data['volunteer_experience_number'] . ' ' . $data['volunteer_experience_type'];
        }
        else {
            $data['volunteer_experience'] = '';
        }
        if (empty($data['arrival_date'])) {
            unset($data['arrival_date']);
        }
        if (empty($data['arrival_time'])) {
            unset($data['arrival_time']);
        }
        else if (strlen($data['arrival_time']) === 4) {
            $data['arrival_time'] = '0' . $data['arrival_time'];
        }
        if (empty($data['departure_date'])) {
            unset($data['departure_date']);
        }
        if (empty($data['departure_time'])) {
            unset($data['departure_time']);
        }
        else if (strlen($data['departure_time']) === 4) {
            $data['departure_time'] = '0' . $data['departure_time'];
        }
        if (!empty($data['file_data'])) {
            $name = pathinfo($data['file_name'], PATHINFO_FILENAME);
            $name .= '.png';
            $data['file'] = [
                'contents' => explode('base64,', $data['file_data'])[1],
                'name' => $name
            ];
            unset($data['file_data']);
            unset($data['file_name']);
        }
        // Dar formato a la fecha de cumpleaños
        if (isset($data['birth_date'])) {
            try {
                $data['birth_date'] = Carbon::createFromFormat('d-m-Y', $data['birth_date'])->format('Y-m-d');
            } catch (\Exception $e) {
                // No se pudo formatear la fecha
            }
        }
        $response = $this->post("api/v1/clients", $data, $request);
        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        if ($response->status_code != 200 && $response->status_code != 201) {
            if (isset($response->errors)) {
                foreach ($response->errors as $key => $messages) {
                    foreach ($messages as $msg) {
                        $validator->errors()->add($key, $msg);
                    }
                }
            }
            return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
        }
        return redirect()->route('clients.index');
    }

    /**
     * Show the form for editing the specified client.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id The client id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $client = $this->get("api/v1/clients/$id?include=file,country,commands.commandCourses,commands.travelInformation,gender", $request);
        $countries = $this->get('api/v1/catalogues/countries', $request);
        $countries = !isset($countries->data) ? [] : $countries->data;
        $pickup = $this->get('api/v1/catalogues/pickup', $request);
        $pickup = !isset($pickup->data) ? [] : $pickup->data;
        $genders = $this->get('api/v1/catalogues/genders', $request);

        return view('clients.form')->with([
            'client' => $client->data,
            'formAction'=> route('clients.update', ['id' => $client->data->id]),
            'formType' => Constant::FORM_EDIT,
            'countries' => $countries,
            'pickup' => $pickup,
            'genders' => $genders,
        ]);
    }

    /**
     * Update the specified client in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id The client id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = [];
        $validator = Validator::make([], []);
        $data = $request->all();
        $data['spanish_knowledge_duration'] = '0';
        if (!empty($data['spanish_level'])) {
            if ($data['spanish_level'] !== 'none') {
                $data['spanish_knowledge_duration'] = $data['spanish_knowledge_number'] . ' ' . $data['spanish_knowledge_type'];
            }
        }
        $data['surf_experience_duration'] = '0';
        if (!empty($data['has_surf_experience'])) {
            $data['surf_experience_duration'] = $data['surf_experience_number'] . ' ' . $data['surf_experience_type'];
        }
        $data['volunteer_experience_duration'] = '0';
        if (!empty($data['has_volunteer_experience'])) {
            $data['volunteer_experience_duration'] = $data['volunteer_experience_number'] . ' ' . $data['volunteer_experience_type'];
        }
        else {
            $data['volunteer_experience'] = '';
        }
        if (empty($data['arrival_date'])) {
            unset($data['arrival_date']);
        }
        if (empty($data['arrival_time'])) {
            unset($data['arrival_time']);
        }
        else if (strlen($data['arrival_time']) === 4) {
            $data['arrival_time'] = '0' . $data['arrival_time'];
        }
        if (empty($data['departure_date'])) {
            unset($data['departure_date']);
        }
        if (empty($data['departure_time'])) {
            unset($data['departure_time']);
        }
        else if (strlen($data['departure_time']) === 4) {
            $data['departure_time'] = '0' . $data['departure_time'];
        }
        if (!empty($data['file_data'])) {
            $name = pathinfo($data['file_name'], PATHINFO_FILENAME);
            $name .= '.png';
            $data['file'] = [
                'contents' => explode('base64,', $data['file_data'])[1],
                'name' => $name
            ];
            unset($data['file_data']);
            unset($data['file_name']);
        }

        // Clear travel information
        $keepTravel = false;
        $travelInformation = isset($data['travel_information'])? $data['travel_information'] : [];
        foreach ($travelInformation as $tiK => $tiV) {
            foreach ($tiV as $k => $v) {
                $keepTravel = !empty($v) ;
                if ($keepTravel) {
                    break;
                }
            }

            if (!$keepTravel) {
                unset($data['travel_information'][$tiK]);
            }
        }

        // Dar formato a las fechas
        if (isset($data['birth_date'])) {
            try {
                $data['birth_date'] = Carbon::createFromFormat('d-m-Y', $data['birth_date'])->format('Y-m-d');
            } catch (\Exception $e) {
                // No se pudo formatear la fecha
            }
        }
        foreach ($travelInformation as $key => $ti) {
            try {
                if (isset($ti['arrival_date'])) {
                    $data['travel_information'][$key]['arrival_date'] = Carbon::createFromFormat('d-m-Y', $ti['arrival_date'])->format('Y-m-d');
                }
            } catch (\Exception $e) {
                // No se pudo formatear la fecha
            }
            try {
                if (isset($ti['departure_date'])) {
                    $data['travel_information'][$key]['departure_date'] = Carbon::createFromFormat('d-m-Y', $ti['departure_date'])->format('Y-m-d');
                }
            } catch (\Exception $e) {
                // No se pudo formatear la fecha
            }
        }

        $response = $this->put("api/v1/clients/$id", $data, $request);
        // JSON response
        if ($request->expectsJson()) {
            return json_encode($response);
        }

        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        if ($response->status_code != 200 && $response->status_code != 201) {
            if (isset($response->errors)) {
                foreach ($response->errors as $key => $messages) {
                    foreach ($messages as $msg) {
                        $validator->errors()->add($key, $msg);
                    }
                }
            }
            return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
        }
        return redirect()->route('clients.index');
    }

    /**
     * Show the specified client data.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id The client id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $client = $this->get("api/v1/clients/$id?include=file,country,commands.travelInformation,gender", $request);
        $countries = $this->get('api/v1/catalogues/countries', $request);
        $countries = !isset($countries->data) ? [] : $countries->data;
        $pickup = $this->get('api/v1/catalogues/pickup', $request);
        $pickup = !isset($pickup->data) ? [] : $pickup->data;
        $genders = $this->get('api/v1/catalogues/genders', $request);

        if ($request->expectsJson()) {
            return json_encode($client);
        }

        return view('clients.form')->with([
            'client' => $client->data,
            'formAction'=> '#',
            'formType' => Constant::FORM_SHOW,
            'countries' => $countries,
            'pickup' => $pickup,
            'genders' => $genders,
        ]);
    }

    /**
     * Remove the specified client from storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id The client id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $response = $this->delete("api/v1/clients/$id", $request);
        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        return redirect()->back();
    }

    public function generateCredential(Request $request, $id)
    {
        $client = $this->get("api/v1/clients/$id?include=file,commands", $request);
        $fullName = $client->data->full_name;
        $commands = $client->data->commands->data;
        $file = !empty($client->data->file->data)? $client->data->file->data->path : null;
        $startDate = null;
        $endDate = null;
        $commandTmp = null;
        $validity = '';
        // Encontrar la vigencia
        if (!empty($commands)) {
            // Obtener la primer comanda
            $commandTmp = $commands[0];
            $startDate = $commandTmp->from_date;
            // Obtener la última comanda
            $commandTmp = $commands[count($commands) - 1];
            $endDate = $commandTmp->to_date;
        }
        if ($startDate !== null && $endDate !== null) {
            $validity = (new Carbon($startDate))->format('d-m-Y') . ' - ' . (new Carbon($endDate))->format('d-m-Y');
        }
        $pdf = PDF::loadView('clients.credential', compact('fullName', 'validity', 'file'));
        return $pdf->download("credential_" . str_replace(' ', '_', $fullName) . ".pdf");
    }

    /**
     * Export to csv.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function exportToCsv(Request $request)
    {
        $clients = $this->get('api/v1/clients?include=country&no-paginate=1', $request);

        // Formatear la información
        $dataExport = array_map(function($c) {
            return [
                'first_name' => $c->first_name,
                'last_name' => $c->last_name,
                'email' => $c->email,
                'birth_date' => (new Carbon($c->birth_date))->format('d-m-Y'),
                'phone' => $c->phone,
                'emergency_phone' => $c->emergency_phone,
                'has_alergies' => $c->has_alergies? 'Yes' : 'No',
                'alergies' => $c->alergies,
                'has_diseases' => $c->has_diseases? 'Yes' : 'No',
                'diseases' => $c->diseases,
                'is_smoker' => $c->is_smoker? 'Yes' : 'No',
                'fb_profile' => $c->fb_profile,
                'country' => $c->country->data->name,
                'spanish_level' => $c->spanish_level?? '--',
                'spanish_knowledge_duration' => $c->spanish_knowledge_duration?? '--',
                'has_surf_experience' => isset($c->has_surf_experience) && $c->has_surf_experience? 'Yes' : 'No',
                'surf_experience_duration' => $c->surf_experience_duration?? '--',
                'has_volunteer_experience' => isset($c->has_volunteer_experience) && $c->has_volunteer_experience? 'Yes' : 'No',
                'volunteer_experience_duration' => $c->volunteer_experience_duration?? '--',
                'volunteer_experience' => $c->volunteer_experience?? '--',
            ];
        }, $clients->data);
        // Agregar los títulos de las columnas
        $dataExport = array_merge([['First name', 'Last name', 'Email', 'Birth date', 'Phone', 'Emergency phone', 'Has alergies?', 'Alergies', 'Has diseases?', 'Diseases', 'Is smoker?', 'Fb profile', 'Country', 'Spanish level', 'Spanish knowledge duration', 'Has surf experience?', 'Surf experience duration', 'Has volunteer experience?', 'Volunteer experience duration', 'Volunteer experience']], $dataExport);
        // Exportar los datos
        return Excel::create('clients', function($excel) use($dataExport) {
            $excel->setTitle('clients');
            $excel->setCompany('Experiencia');
            $excel->sheet("clients", function($sheet) use($dataExport) {
                $sheet->fromArray($dataExport, null, 'A1', true, false);
            });
        })->export('csv');
    }
}
