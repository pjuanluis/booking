<?php

namespace App\Http\Controllers\Admin;

use Excel;
use Constant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use App\Http\Controllers\Controller;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $url = 'api/v1/schedules?include=schedulable:with_group_clients(1),responsible,place&';
        $params = $this->prepareParams($request, ['page', 'from', 'to', 'experience']);
        // Get schedules from api
        $schedules = null;
        if ($request->expectsJson()) {
            $params['no-paginate'] = 1;
            $schedules = $this->get($url . make_url_params($params), $request);
            return $schedules->success ?
                response()->json($schedules, 200) :
                response()->json(['data' => [], 'meta' => ['pagination' => ['total' => 0]]]);
        }
        $schedules = $this->get($url . make_url_params($params), $request);
        // Get experiences from api
        $experiences = $this->get('api/v1/experiences', $request)->data;

        return view('schedules.index')
            ->with(compact('schedules', 'experiences', 'params'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // Get groups from api
        $groups = $this->get('api/v1/groups', $request)->data;
        // Get places from api
        $places = $this->get('api/v1/places?no-paginate=1', $request)->data;
        // Get peoples from api
        $peoples = $this->get('api/v1/people?no-paginate=1', $request)->data;
        // Get clients from api
        $clients = $this->get('api/v1/clients?is_active=1', $request)->data;
        // Get experiences from api
        $experiences = $this->get('api/v1/experiences', $request)->data;
        $countries = $this->get('api/v1/catalogues/countries', $request, false);
        $countries = !isset($countries->data) ? [] : $countries->data;
        $agencies = [];
        $group = null;
        $formType = Constant::FORM_CREATE;
        $formAction = route('schedules.store');
        return view('schedules.form')
            ->with(compact('formType', 'formAction', 'groups', 'places', 'peoples', 'clients', 'experiences', 'countries', 'agencies', 'group'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');

        foreach ($data as $k => $v) {
            if (!isset($v)) {
                unset($data[$k]);
            }
        }

        $data['experiences'] = array($data['experiences']);
        $data['is_private'] = isset($data['is_private']) ? (int) $data['is_private'] : 0;
        if (!empty($data['clients'])) {
            $data['clients'] = explode(',', $data['clients']);
        }
        else {
            $data['clients'] = [];
        }

        $data['schedule_time'] = [];

        // Create schedule_time
        if (isset($data['begin_date']) && isset($data['begin_time']) && isset($data['duration'])) {
            $datesBeginAt = explode(',', $data['begin_date']);
            foreach ($datesBeginAt as $dba) {
                $beginAt = Carbon::createFromFormat('Y-m-d h:i A', trim($dba) . ' ' . trim($data['begin_time']))
                    ->format('Y-m-d H:i:s');
                array_push(
                    $data['schedule_time'],
                    array(
                        'begin_at' => $beginAt,
                        'finish_at' => Carbon::parse($beginAt)->addSeconds($data['duration'] * 3600)->format('Y-m-d H:i:s')
                    )
                );
            }
            unset($data['begin_date']);
            unset($data['begin_time']);
            unset($data['duration']);
        }

        $result = $this->storeInApi('api/v1/schedules', $data, $request);
        if($result !== true ) {
            return $result;
        }
        return redirect()->route('schedules.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        // Get groups from api
        $groups = $this->get('api/v1/groups', $request)->data;
        // Get places from api
        $places = $this->get('api/v1/places?no-paginate=1', $request)->data;
        // Get peoples from api
        $peoples = $this->get('api/v1/people?no-paginate=1', $request)->data;
        // Get clients from api
        $clients = $this->get('api/v1/clients?is_active=1', $request)->data;
        // Get experiences from api
        $experiences = $this->get('api/v1/experiences', $request)->data;
        // Get schedule
        $schedule = $this->get("api/v1/schedules/$id?include=responsible,place,experiences,schedulable.clients:is_active(1),schedulable.clients.commands.commandCourses.package.experiences", $request)->data;
        $countries = $this->get('api/v1/catalogues/countries', $request, false);
        $countries = !isset($countries->data) ? [] : $countries->data;
        $agencies = [];
        $group = $schedule->schedulable->data;
        $formType = Constant::FORM_EDIT;
        $formAction = route('schedules.update', ['id' => $id]);
        return view('schedules.form')
            ->with(compact('formType', 'formAction', 'groups', 'places', 'peoples', 'clients', 'schedule',
                'experiences', 'countries', 'agencies', 'group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token', '_method');

        foreach ($data as $k => $v) {
            if (!isset($v)) {
                unset($data[$k]);
            }
        }

        $data['experiences'] = isset($data['experiences']) ? array($data['experiences']) : [];
        $data['is_private'] = isset($data['is_private']) ? (int) $data['is_private'] : 0;
        if (!empty($data['clients'])) {
            $data['clients'] = explode(',', $data['clients']);
        }
        else {
            $data['clients'] = [];
        }
        // Create schedule_time
        if (isset($data['begin_date']) && isset($data['begin_time']) && isset($data['duration'])) {
            $data['schedule_time'] = array();
            $datesBeginAt = explode(',', $data['begin_date']);
            foreach ($datesBeginAt as $dba) {
                $beginAt = Carbon::createFromFormat('Y-m-d h:i A', trim($dba) . ' ' . trim($data['begin_time']))
                    ->format('Y-m-d H:i:s');
                array_push(
                    $data['schedule_time'],
                    array(
                        'begin_at' => $beginAt,
                        'finish_at' => Carbon::parse($beginAt)->addSeconds($data['duration'] * 3600)->format('Y-m-d H:i:s')
                    )
                );
            }
            unset($data['begin_date']);
            unset($data['begin_time']);
            unset($data['duration']);
        }

        $result = $this->updateInApi("api/v1/schedules/$id", $data, $request);
        if($result !== true ) {
            return $result;
        }
        return redirect()->route('schedules.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $response = $this->delete("api/v1/schedules/$id", $request);
        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        return redirect()->back();
    }

    public function print(Request $request) {
        $scheduleData = $request->get('scheduleData');
        $week = $request->get('week');

        $pdf = PDF::loadView('schedules.print', [
            'scheduleData' => $scheduleData,
            'week' => $week
        ]);

        return $pdf->download('schedule.pdf');
    }

    /**
     * Export to excel.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function exportToCsv(Request $request)
    {
        $url = 'api/v1/schedules?include=schedulable:with_group_clients(1),responsible,place&no-paginate=1';
        // Get schedules from api
        $schedules = $this->get($url, $request);

        // Formatear la información
        $dataExport = array_map(function($s) {
            $dataStudents = [];
            foreach($s->schedulable->data->clients->data as $c) {
                $dataStudents[] = $c->first_name . ' ' . $c->last_name;
            }
            return [
                'theme' => $s->subject,
                'level' => 'S' . $s->schedulable->data->level,
                'start_date' => (new Carbon($s->dates->data[0]->begin_at))->format('d-m-Y H:i'),
                'end_date' => (new Carbon($s->dates->data[count($s->dates->data) - 1]->finish_at))->format('d-m-Y H:i'),
                'place' => $s->place->data->name,
                'responsible' => $s->responsible->data->full_name,
                'students' => implode(', ', $dataStudents)
            ];
        }, $schedules->data);
        // Agregar los títulos de las columnas
        $dataExport = array_merge([['Theme', 'Level', 'From', 'To', 'Place', 'Responsable', 'Students']], $dataExport);
        // Exportar los datos
        return Excel::create('schedules', function($excel) use($dataExport) {
            $excel->setTitle('schedules');
            $excel->setCompany('Experiencia');
            $excel->sheet("Bookings", function($sheet) use($dataExport) {
                $sheet->fromArray($dataExport, null, 'A1', true, false);
            });
        })->export('csv');
    }
}
