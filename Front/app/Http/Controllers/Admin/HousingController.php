<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Constant;

class HousingController extends Controller
{
    /**
     * Display a listing of commands.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $url = 'api/v1/housing';
        $params = ['page' => 1];
        if (!empty($request->get('page'))) {
            $params['page'] = $request->get('page');
        }
        $housing = $this->get($url . '?' . make_url_params($params), $request);
        return view('housing.index')
                ->with([
                    'housing' => $housing,
                    'params' => $params
                ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "Create Housing";

        return view('housing.form')->with([
            'formAction'=> route('housing.store'),
            'formType' => Constant::FORM_CREATE,
            'title' => $title
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make([], []);

        $response = $this->post("api/v1/housing", $request->only('name'), $request);

        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        if ($response->status_code != 200 && $response->status_code != 201) {
            if (isset($response->errors)) {
                foreach ($response->errors as $key => $messages) {
                    foreach ($messages as $msg) {
                        $validator->errors()->add($key, $msg);
                    }
                }
            }
            return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
        }
        return redirect()->route('housing.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $title = "Show Housing";
        $housing = $this->get("api/v1/housing/$id", $request);
        return view('housing.form')->with([
            'housing' => $housing->data,
            'formAction'=> '',
            'formType' => Constant::FORM_SHOW,
            'title' => $title
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $title = "Edit Housing";
        $housing = $this->get("api/v1/housing/$id", $request);
        return view('housing.form')->with([
            'housing' => $housing->data,
            'formAction'=> route('housing.update', ['id' => $housing->data->id]),
            'formType' => Constant::FORM_EDIT,
            'title' => $title
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make([], []);

        $response = $this->put("api/v1/housing/$id", $request->only('name', 'updated_at'), $request);

        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        if ($response->status_code != 200 && $response->status_code != 201) {
            if (isset($response->errors)) {
                foreach ($response->errors as $key => $messages) {
                    foreach ($messages as $msg) {
                        $validator->errors()->add($key, $msg);
                    }
                }
            }
            return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
        }
        return redirect()->route('housing.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $response = $this->delete("api/v1/housing/$id", $request);
        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        return redirect()->back();
    }

    /**
     * Activate / deactivate the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate(Request $request, $id)
    {
        $response = $this->put("api/v1/housing/$id/activate", [], $request);
        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        return redirect()->route('housing.index');
    }

    /**
     * Get housing availability on the specified range of dates
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function availability(Request $request)
    {
        // Obtener disponibilidad del hospedaje
        $url = 'api/v1/housing/availability';
        $params = [];
        if (!empty($request->get('from_date'))) {
            $params['from_date'] = $request->get('from_date');
        }
        if (!empty($request->get('to_date'))) {
            $params['to_date'] = $request->get('to_date');
        }
        if (!empty($request->get('housing_id'))) {
            $params['housing_id'] = $request->get('housing_id');
        }
        if (!empty($request->get('housing_room_id'))) {
            $params['housing_room_id'] = $request->get('housing_room_id');
        }
        if (!empty($request->get('people'))) {
            $params['people'] = $request->get('people');
        }
        $data = $this->get($url . '?' . make_url_params($params), $request);
        if ($request->expectsJson()) {
            if ($data->success) {
                return response()->json($data, 200);
            }
            else {
                return response()->json(['data' => []], 200);
            }
        }
    }

    /**
     * Get housing room occupancy on the specified month
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function occupancy(Request $request)
    {
        // Obtener disponibilidad del hospedaje
        $url = 'api/v1/housing/occupancy';
        $params = [];
        if (!empty($request->get('month'))) {
            $params['month'] = $request->get('month');
        }
        if (!empty($request->get('housing_room_id'))) {
            $params['housing_room_id'] = $request->get('housing_room_id');
        }
        $data = $this->get($url . '?' . make_url_params($params), $request);
        if ($request->expectsJson()) {
            if ($data->success) {
                return response()->json($data, 200);
            }
            else {
                return response()->json(['data' => []], 200);
            }
        }
    }
}
