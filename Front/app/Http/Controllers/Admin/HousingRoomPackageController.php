<?php

namespace App\Http\Controllers\Admin;

use Constant;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HousingRoomPackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $housingRoomId)
    {
        $housingRoom = $this->get("api/v1/housing-rooms/$housingRoomId?include=packages", $request);
        return view('housing.rooms.packages.index')->with([
            'housingRoom' => $housingRoom->data,
            'housingId' => $request->get('housing', '')
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $housingRoomId)
    {
        $title = "Create Housing / Room option";

        return view('housing.rooms.packages.form')->with([
            'formAction'=> route('housing.rooms.packages.store',['id' => $housingRoomId]),
            'formType' => Constant::FORM_CREATE,
            'title' => $title
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $housingRoomId)
    {
        $validator = Validator::make([], []);
        $data = $request->all();

        // Se filtran solo los arreglos que por lo menos tengan un valor no vacío, todos los demás son ignorados
        $data['housing_rates'] = array_filter(
            json_decode($request->get('housing_rates'), true),
            function($array) {
                return $array['quantity'] != '' || $array['unit'] != '' || $array['from'] != '' || $array['to'] != '' || $array['unit_cost'] != '';
            }
        );

        // Cambiar el formato a las fechas
        $data = $this->changeDateFormat($data, 'd-m-Y', 'Y-m-d');

        $response = $this->post("api/v1/housing-rooms/$housingRoomId/packages", $data, $request);

        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);

        if ($response->status_code != 200 && $response->status_code != 201) {
            if (isset($response->errors)) {
                foreach ($response->errors as $key => $messages) {
                    foreach ($messages as $msg) {
                        $validator->errors()->add($key, $msg);
                    }
                }
            }
            return redirect()
                ->back()
                ->withInput()
                ->withErrors($validator);
        }
        return redirect()->route('housing.rooms.packages.index', ['id' => $housingRoomId]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $title = "Show housing room option";

        $package = $this->get("api/v1/housing-rooms-packages/$id", $request);

        return view('housing.rooms.packages.form')->with([
            'formAction'=> '',
            'formType' => Constant::FORM_SHOW,
            'package' => $package->data,
            'title' => $title,
            'housingRoom' => $request->get('housing-room', 0)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $title = "Edit housing room option";
        $package = $this->get("api/v1/housing-rooms-packages/$id", $request);

        return view('housing.rooms.packages.form')->with([
            'formAction'=> route('housing.rooms.packages.update', ['id' => $id, 'housing-room' => $request->get('housing-room', '0')]),
            'formType' => Constant::FORM_EDIT,
            'package' => $package->data,
            'title' => $title
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make([], []);

        $data = $request->all();

        // Se filtran solo los arreglos que por lo menos tengan un valor no vacío, todos los demás son ignorados
        $data['housing_rates'] = array_filter(
            json_decode($request->get('housing_rates'), true),
            function($array) {
                return $array['quantity'] != '' || $array['unit'] != '' || $array['from'] != '' || $array['to'] != '' || $array['unit_cost'] != '';
            }
        );

        // Cambiar el formato a las fechas
        $data = $this->changeDateFormat($data, 'd-m-Y', 'Y-m-d');

        $response = $this->put("api/v1/housing-rooms-packages/$id", $data, $request);

        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        if ($response->status_code != 200 && $response->status_code != 201) {
            if (isset($response->errors)) {
                foreach ($response->errors as $key => $messages) {
                    foreach ($messages as $msg) {
                        $validator->errors()->add($key, $msg);
                    }
                }
            }
            return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
        }
        return redirect()->route('housing.rooms.packages.index', ['id' => $request->get('housing-room', '0')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $response = $this->delete("api/v1/housing-rooms-packages/$id", $request);
        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        return redirect()->back();
    }

    /**
     * Activate / deactivate the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate(Request $request, $id)
    {
        $response = $this->put("api/v1/housing-rooms-packages/$id/activate", [], $request);
        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        return redirect()->back();
    }

    /**
     * Change the format of the dates
     *
     * @param  array  $data
     * @param  string  $inputFormat
     * @param  string  $outputFormat
     * @return array;
     */
    private function changeDateFormat($data, $inputFormat, $outputFormat)
    {
        // Dar formato a las fechas
        $housingRates = $data['housing_rates']? $data['housing_rates'] : [];
        foreach ($housingRates as $key => $hr) {
            if ($hr['from']) {
                try {
                    $data['housing_rates'][$key]['from'] = Carbon::createFromFormat($inputFormat, $hr['from'])->format($outputFormat);
                } catch (\Exception $e) {
                    // No se pudo formatear la fecha
                }
            }
            if ($hr['to']) {
                try {
                    $data['housing_rates'][$key]['to'] = Carbon::createFromFormat($inputFormat, $hr['to'])->format($outputFormat);
                } catch (\Exception $e) {
                    // No se pudo formatear la fecha
                }
            }
        }
        return $data;
    }
}
