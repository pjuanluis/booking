<?php

namespace App\Http\Controllers\Admin;

use Excel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Constant;
use Illuminate\Support\Facades\Auth;
use Validator;
use Carbon\Carbon;

class BookingController extends Controller
{
    /**
     * Display a listing of commands.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $url = 'api/v1/commands?include=travelInformation,purchase.user&dir=desc';
        $params = ['page' => 1];
        if (!empty($request->get('page'))) {
            $params['page'] = $request->get('page');
        }
        if (!empty($request->get('from_date'))) {
            $params['from_date'] = $this->changeDateFormat($request->get('from_date'), 'd-m-Y', 'Y-m-d');
        }
        if (!empty($request->get('to_date'))) {
            $params['to_date'] = $this->changeDateFormat($request->get('to_date'), 'd-m-Y', 'Y-m-d');
        }
        if (!empty($request->get('booking_code'))) {
            $params['booking_code'] = $request->get('booking_code');
        }
        if (!empty($request->get('client_email'))) {
            $params['client_email'] = $request->get('client_email');
        }
        if (!empty($request->get('agency_id'))) {
            $params['agency_id'] = $request->get('agency_id');
        }
        if (!empty($request->get('client_id'))) {
            $params['client_id'] = $request->get('client_id');
        }
        if (!empty($request->get('command-type'))) {
            $params['command-type'] = $request->get('command-type');
        }
        if (!empty($request->get('include'))) {
            $params['include'] = $request->get('include');
        }
        if (!empty($request->get('experiences'))) {
            $params['experiences'] = is_array($request->get('experiences'))? implode(",", $request->get('experiences')) : $request->get('experiences');
        }
        if (!empty($request->get('registration_fee'))) {
            $params['registration_fee'] = $request->get('registration_fee');
        }
        if (!empty($request->get('arrival_at'))) {
            $params['arrival_at'] = $this->changeDateFormat($request->get('arrival_at'), 'd-m-Y', 'Y-m-d');
        }
        if (!empty($request->get('departure_at'))) {
            $params['departure_at'] = $this->changeDateFormat($request->get('departure_at'), 'd-m-Y', 'Y-m-d');
        }

        $commands = $this->get($url . '&' . make_url_params($params), $request);
        $experiences = $this->get('api/v1/experiences?no-paginate=1', $request);

        // Cambiar el formato de fecha al formato local
        if (isset($params['arrival_at'])) {
            $params['arrival_at'] = $request->get('arrival_at', '');
        }
        if (isset($params['departure_at'])) {
            $params['departure_at'] = $request->get('departure_at', '');
        }
        if (isset($params['from_date'])) {
            $params['from_date'] = $request->get('from_date', '');
        }
        if (isset($params['to_date'])) {
            $params['to_date'] = $request->get('to_date', '');
        }

        if ($request->expectsJson()) {
            $jsonResponse = response()->json([
                'data' => [],
                'meta' => [
                    'pagination' => [
                        'total' => 0
                    ]
                ]
            ]);

            if ($commands->success) {
                $jsonResponse = response()->json($commands, 200);
            }

            return $jsonResponse;
        }

        return view('commands.bookings')
                ->with([
                    'commands' => $commands,
                    'experiences' => $experiences,
                    'params' => $params
                ]);
    }

    /**
     * Change the format of the dates
     *
     * @param  array  $date
     * @param  string  $inputFormat
     * @param  string  $outputFormat
     * @return array;
     */
    private function changeDateFormat($date, $inputFormat, $outputFormat)
    {
        try {
            $date = Carbon::createFromFormat($inputFormat, $date)->format($outputFormat);
        } catch (\Exception $e) {
            // No se pudo formatear la fecha
        }
        return $date;
    }

    /**
     * Display a listing of new bookings.
     *
     * @return \Illuminate\Http\Response
     */
    public function newBookings(Request $request)
    {
        $month = Carbon::now()->format('m');
        $url = 'api/v1/commands?month=' . $month;
        $params = ['page' => 1];
        if (!empty($request->get('page'))) {
            $params['page'] = $request->get('page');
        }
        if (!empty($request->get('from_date'))) {
            $params['from_date'] = $request->get('from_date');
        }
        if (!empty($request->get('to_date'))) {
            $params['to_date'] = $request->get('to_date');
        }
        if (!empty($request->get('booking_code'))) {
            $params['booking_code'] = $request->get('booking_code');
        }
        if (!empty($request->get('client_email'))) {
            $params['client_email'] = $request->get('client_email');
        }
        if (!empty($request->get('agency_id'))) {
            $params['agency_id'] = $request->get('agency_id');
        }
        if (!empty($request->get('client_id'))) {
            $params['client_id'] = $request->get('client_id');
        }
        $commands = $this->get($url . '&' . make_url_params($params), $request);
        return view('commands.bookings')
                ->with([
                    'commands' => $commands,
                    'params' => $params
                ]);
    }

    /**
     * Get bookings count.
     *
     * @return \Illuminate\Http\Response
     */
    public function count(Request $request)
    {
        $url = 'api/v1/commands/count?';
        $params = [];
        if (!empty($request->get('month'))) {
            $params['month'] = $request->get('month');
        }
        if (!empty($request->get('from_date'))) {
            $params['from_date'] = $request->get('from_date');
        }
        if (!empty($request->get('to_date'))) {
            $params['to_date'] = $request->get('to_date');
        }
        if (!empty($request->get('booking_code'))) {
            $params['booking_code'] = $request->get('booking_code');
        }
        if (!empty($request->get('client_email'))) {
            $params['client_email'] = $request->get('client_email');
        }
        if (!empty($request->get('agency_id'))) {
            $params['agency_id'] = $request->get('agency_id');
        }
        if (!empty($request->get('client_id'))) {
            $params['client_id'] = $request->get('client_id');
        }
        $response = $this->get($url . '&' . make_url_params($params), $request, false);
        return response()->json($response, $response->status_code);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = Auth::user();
        $catalogues = $this->getCatalogues($request);
        return view('commands.form')
                ->with([
                    'formType' => Constant::FORM_CREATE,
                    'status' => 'booked',
                    'user' => $user,
                    'experiences' => $catalogues['experiences'],
                    'packages' => $catalogues['packages'],
                    'housing' => $catalogues['housing'],
                    'pickup' => $catalogues['pickup'],
                    'countries' => $catalogues['countries'],
                    'agencies' => $catalogues['agencies'],
                    'currencies' => $catalogues['currencies'],
                    'methodsPayment' => $catalogues['methodsPayment'],
                    'consumptionCenters' => $catalogues['consumptionCenters'],
                    'statuses' => $catalogues['statuses'],
                    'genders' => $catalogues['genders'],
                    'formAction'=> route('bookings.store'),
                ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make([], []);
        $data = $request->all();
        //dd($data);
        if ($data['new_client'] == 1) {
            unset($data['client_id']);
        }
        else {
            unset($data['client_first_name']);
            unset($data['client_last_name']);
            unset($data['client_email']);
        }
        // Decodificar json de cursos
        $courses = json_decode($request->get("courses"));
        $data['courses'] = $courses;
        // Decodificar json de hospedaje
        $housing = json_decode($request->get("housing"));
        $data['housing'] = $housing;
        // Decodificar json de compras
        $purchases = json_decode($request->get("purchases"));
        $data['purchases'] = $purchases;
        // Decodificar json de pagos
        $payments = json_decode($request->get("payments"));
        $data['payments'] = $payments;
        $data['status'] = 'booked';
        // Cambiar formato a las fechas
        if (!empty($data['arrival_date'])) {
            $data['arrival_date'] = Carbon::createFromFormat('d-m-Y', $data['arrival_date'])->format('Y-m-d');
        }
        if (!empty($data['departure_date'])) {
            $data['departure_date'] = Carbon::createFromFormat('d-m-Y', $data['departure_date'])->format('Y-m-d');
        }
        // Se filtran solo los arreglos que por lo menos tengan un valor no vacío, todos los demás son ignorados
        $data['command_bills'] = array_filter(
            json_decode($request->get('command_bills'), true),
            function($array) {
                return $array['reference_number'] != '' || $array['amount'] != '' || $array['currency_id'] != '';
            }
        );
        $data['pickup_exchange_rates'] = json_decode($data['pickup_exchange_rates']);
        if (!empty($data['pickup_currency_id'])) {
            $data['pickup_currency_id'] = intval($data['pickup_currency_id']);
        }
        // Enviar request al API
        $response = $this->post("api/v1/commands", $data, $request);

        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);

        // Si ocurrió algún error
        if ($response->status_code != 201) {
            if (isset($response->errors)) {
                foreach ($response->errors as $key => $messages) {
                    foreach ($messages as $msg) {
                        $validator->errors()->add($key, $msg);
                    }
                }
            }
            return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
        }

        // Go to edit client data
        if (!empty($response->command)) {
            return redirect()->route('clients.edit', ['id' =>$response->command->data->client_id]);
        }
        return redirect()->route('bookings.index');
    }

    /**
     * Display the specified command.
     *
     * @param  int  $id
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $user = Auth::user();
        $command = $this->get("api/v1/commands/$id?include=client.scheduleDates.schedule.responsible:order_by(begin_at|asc),payments.currency,payments.paymentMethod,commandCourses.package.experiences,commandCourses.purchase.currency,commandHousing.room.residence,commandHousing.purchase.currency,pickup,client.file,travelInformation,agency,bills,client.gender,extraPurchases.purchasable.currency,extraPurchases.purchasable.consumptionCenter,purchase&exclude=client.scheduleDates.schedule.dates", $request);
        $catalogues = $this->getCatalogues($request);
        $rooms = $this->get("api/v1/rooms?include=residence.housing,housingRooms", $request);
        $residences = $this->get("api/v1/residences?include=housing,rooms", $request);
        return view('commands.form')
                ->with([
                    'formType' => Constant::FORM_SHOW,
                    'status' => $command->data->status,
                    'command' => $command,
                    'user' => $user,
                    'rooms' => $rooms->data,
                    'residences' => $residences->data,
                    'experiences' => $catalogues['experiences'],
                    'packages' => $catalogues['packages'],
                    'housing' => $catalogues['housing'],
                    'pickup' => $catalogues['pickup'],
                    'countries' => $catalogues['countries'],
                    'agencies' => $catalogues['agencies'],
                    'currencies' => $catalogues['currencies'],
                    'genders' => $catalogues['genders'],
                    'formAction' => '#'
                ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $user = Auth::user();
        $command = $this->get("api/v1/commands/$id?include=client.scheduleDates.schedule.responsible:order_by(begin_at|asc),payments.currency,payments.paymentMethod,commandCourses.package.experiences,commandCourses.purchase.currency,commandHousing.room.residence,commandHousing.purchase.currency,pickup,client.file,travelInformation,agency,bills,client.gender,extraPurchases.purchasable.currency,extraPurchases.purchasable.consumptionCenter,purchase&exclude=client.scheduleDates.schedule.dates", $request);
        //dd($command);
        $catalogues = $this->getCatalogues($request);
        $rooms = $this->get("api/v1/rooms?include=residence.housing,housingRooms,blockedDates", $request);
        $residences = $this->get("api/v1/residences?include=housing,rooms.housingRooms,rooms.blockedDates", $request);
        return view('commands.form')
                ->with([
                    'formType' => Constant::FORM_EDIT,
                    'status' => $command->data->status,
                    'command' => $command,
                    'user' => $user,
                    'rooms' => $rooms->data,
                    'residences' => $residences->data,
                    'experiences' => $catalogues['experiences'],
                    'packages' => $catalogues['packages'],
                    'housing' => $catalogues['housing'],
                    'pickup' => $catalogues['pickup'],
                    'countries' => $catalogues['countries'],
                    'agencies' => $catalogues['agencies'],
                    'currencies' => $catalogues['currencies'],
                    'methodsPayment' => $catalogues['methodsPayment'],
                    'statuses' => $catalogues['statuses'],
                    'genders' => $catalogues['genders'],
                    'consumptionCenters' => $catalogues['consumptionCenters'],
                    'formAction'=> route('bookings.update', ['id' => $id]),
                ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make([], []);
        $data = $request->all();
        // Decodificar json de cursos
        $courses = json_decode($request->get("courses"));
        $data['courses'] = $courses;
        // Decodificar json de hospedaje
        $housing = json_decode($request->get("housing"));
        $data['housing'] = $housing;
        // Decodificar json de compras
        $purchases = json_decode($request->get("purchases"));
        $data['purchases'] = $purchases;
        // Decodificar json de pagos
        $payments = json_decode($request->get("payments"));
        $data['payments'] = $payments;
        $data['status'] = 'booked';
        // Cambiar formato a las fechas
        if (!empty($data['arrival_date'])) {
            $data['arrival_date'] = Carbon::createFromFormat('d-m-Y', $data['arrival_date'])->format('Y-m-d');
        }
        if (!empty($data['departure_date'])) {
            $data['departure_date'] = Carbon::createFromFormat('d-m-Y', $data['departure_date'])->format('Y-m-d');
        }
        // Se filtran solo los arreglos que por lo menos tengan un valor no vacío, todos los demás son ignorados
        $data['command_bills'] = array_filter(
            json_decode($request->get('command_bills'), true),
            function($array) {
                return $array['reference_number'] != '' || $array['amount'] != '' || $array['currency_id'] != '';
            }
        );
        $data['pickup_exchange_rates'] = json_decode($data['pickup_exchange_rates']);
        if (!empty($data['pickup_currency_id'])) {
            $data['pickup_currency_id'] = intval($data['pickup_currency_id']);
        }
        // Enviar request al API
        $response = $this->put("api/v1/commands/$id", $data, $request);

        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);

        // Si ocurrió algún error
        if ($response->status_code != 200) {
            if (isset($response->errors)) {
                foreach ($response->errors as $key => $messages) {
                    foreach ($messages as $msg) {
                        $validator->errors()->add($key, $msg);
                    }
                }
            }
            return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $response = $this->delete("api/v1/commands/$id", $request);
        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        return redirect()->route('bookings.index');
    }

    /**
     * Get the catalogues used in the command form
     * @return array
     */
    private function getCatalogues(Request $request, array $params = [])
    {
        $experiences = $this->get('api/v1/experiences', $request);
        $experiences = isset($experiences->data) ? $experiences->data : [];
        $packages = $this->get('api/v1/packages?include=rates,experiences&no-paginate=1&is_active=1', $request);
        $packages = isset($packages->data) ? $packages->data : [];
        $housing = $this->get('api/v1/housing?is_active=1&no-paginate=1&include=housingRooms:is_active(1),housingRooms.packages.rates', $request);
        $housing = isset($housing->data) ? $housing->data : [];
        $pickup = $this->get('api/v1/catalogues/pickup', $request);
        $pickup = isset($pickup->data) ? $pickup->data : [];
        $countries = $this->get('api/v1/catalogues/countries', $request);
        $countries = !isset($countries->data) ? [] : $countries->data;
        $agencies = $this->get('api/v1/agencies?no-paginate=1', $request);
        $agencies = !isset($agencies->data) ? [] : $agencies->data;
        $currencies = $this->get('api/v1/currencies?no-paginate=1&is_active=1&include=exchangeRates', $request);
        $currencies = !isset($currencies->data) ? [] : $currencies->data;
        $methodsPayment = $this->get('api/v1/methods-payment?no-paginate=1&is_active=1', $request);
        $methodsPayment = !isset($methodsPayment->data) ? [] : $methodsPayment->data;
        $statuses = $this->get('api/v1/statuses?no-paginate=1&is_active=1', $request);
        $statuses = !isset($statuses->data) ? [] : $statuses->data;
        $genders = $this->get('api/v1/catalogues/genders', $request);
        $consumptionCenters = $this->get('api/v1/consumption-centers', $request);

        return [
            'experiences' => $experiences,
            'packages' => $packages,
            'housing' => $housing,
            'pickup' => $pickup,
            'countries' => $countries,
            'agencies' => $agencies,
            'currencies' => $currencies,
            'methodsPayment' => $methodsPayment,
            'statuses' => $statuses,
            'genders' => $genders,
            'consumptionCenters' => $consumptionCenters,
        ];
    }

    /**
     * Update the specified class status in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateClassStatus(Request $request)
    {
        $validator = Validator::make([], []);
        $data = $request->all();
        // Enviar request al API
        $response = $this->put("api/v1/schedule-date/{$data['schedule_date_id']}/class-status", $data, $request);
        if ($request->expectsJson()) {
            return response()->json($response, $response->status_code);
        }
        // Si ocurrió algún error
        if ($response->status_code != 200) {
            if (isset($response->errors)) {
                foreach ($response->errors as $key => $messages) {
                    foreach ($messages as $msg) {
                        $validator->errors()->add($key, $msg);
                    }
                }
            }
            else {
                $request->session()->flash('error', [
                    'status_code' => $response->status_code,
                    'message' => $response->message
                ]);
            }
            return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
        }
        return redirect()->route('bookings.index');
    }

    /**
     * Update the specified payments in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function markPaymentsAsInvoiced(Request $request)
    {
        $data = $request->all();
        // Enviar request al API
        $response = $this->put("api/v1/commands/payments/mark-as-invoiced", $data, $request);
        if ($request->expectsJson()) {
            return response()->json($response, $response->status_code);
        }
    }

    /**
     * Export to excel.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function exportToExel(Request $request)
    {
        $data = json_decode($request->input('data'));
        $commandType = $request->input('command-type');
        $commandType = $commandType === 'booking'? 'New' : ucfirst($commandType);

        // Formatear la información
        $dataExport = array_map(function($d) {
            return [
                'booking_code' => $d->booking_code,
                'client' => $d->client_name,
                'created_at' => $this->changeDateFormat($d->created_at, 'Y-m-d H:i:s', 'd-m-Y'),
                'arrival_at' => !empty($d->travelInformation->data->arrival_at)? $this->changeDateFormat($d->travelInformation->data->arrival_at, 'Y-m-d H:i:s', 'd-m-Y') : '',
                'departure_at' => !empty($d->travelInformation->data->departure_at)? $this->changeDateFormat($d->travelInformation->data->departure_at, 'Y-m-d H:i:s', 'd-m-Y') : '',
                'from_date' => $d->from_date? $this->changeDateFormat($d->from_date, 'Y-m-d', 'd-m-Y') : '',
                'to_date' => $d->to_date? $this->changeDateFormat($d->to_date, 'Y-m-d', 'd-m-Y') : '',
                'fee_payment' => $d->hasRegistrationPayment || $d->hasReservationPayment ? 'Si' : 'No',
                'client_connected' => $d->hasClientData ? 'Si' : 'No'
            ];
        }, $data);
        // Agregar los títulos de las columnas
        $dataExport = array_merge([['Booking Reference	', 'Client/lead name', 'Reg. date', 'Arrival date', 'Departure date', 'Command start', 'Command end', 'Payment', 'Client connected']], $dataExport);
        // Exportar los datos
        return Excel::create("{$commandType} Bookings", function($excel) use($dataExport, $commandType) {
            $excel->setTitle("{$commandType} Bookings");
            $excel->setCompany('Experiencia');
            $excel->sheet("Bookings", function($sheet) use($dataExport) {
                $sheet->fromArray($dataExport, null, 'A1', true, false);
            });
        })->export('xlsx');
    }

    /**
     * Export to csv.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function exportToCsv(Request $request)
    {
        $url = "api/v1/commands/generate-csv?dir=desc";
        // Filtrar por tipo de comanda
        if (!empty($request->get('command-type'))) {
            $url .= "&command-type={$request->get('command-type')}";
        }

        // Generar el csv en el api y obtener el path
        $response = $this->get($url, $request);

        $apiUrl = config('services.api.url');

        $response->path = (ends_with($apiUrl, '/') ? $apiUrl : $apiUrl . '/') . $response->path;
        if ($response->status_code == 201) {
            return file_get_contents($response->path);
        }
        abort(500);
    }

    public function logs($id, Request $request) {
        $params = [];
        if ($request->has('page')) {
            $params['page'] = $request->input('page');
        }
        $logs = $this->get("api/v1/commands/{$id}/logs?" . make_url_params($params), $request);
        return view('commands.logs', compact('logs', 'params'));
    }
}
