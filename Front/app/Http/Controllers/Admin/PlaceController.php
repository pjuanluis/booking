<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Constant;

class PlaceController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $url = 'api/v1/places';
        $params = ['page' => 1];
        if (!empty($request->get('page'))) {
            $params['page'] = $request->get('page');
        }
        $places = $this->get($url . '?' . make_url_params($params), $request);
        $view = 'places.index';
        return view($view)
                ->with([
                    'places' => $places,
                    'params' => $params
                ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $title = "Create place";

        return view('places.form')->with([
            'formAction'=> route('places.store'),
            'formType' => Constant::FORM_CREATE,
            'title' => $title
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make([], []);

        $response = $this->post("api/v1/places", $request->all(), $request);

        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        if ($response->status_code != 200 && $response->status_code != 201) {
            if (isset($response->errors)) {
                foreach ($response->errors as $key => $messages) {
                    foreach ($messages as $msg) {
                        $validator->errors()->add($key, $msg);
                    }
                }
            }
            return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
        }
        return redirect()->route('places.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $title = "Show place";
        $place = $this->get("api/v1/places/$id", $request);
        return view('places.form')->with([
            'place' => $place->data,
            'formAction'=> '',
            'formType' => Constant::FORM_SHOW,
            'title' => $title
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $title = "Edit place";
        $place = $this->get("api/v1/places/$id", $request);
        return view('places.form')->with([
            'place' => $place->data,
            'formAction'=> route('places.update', ['id' => $place->data->id]),
            'formType' => Constant::FORM_EDIT,
            'title' => $title
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make([], []);

        $response = $this->put("api/v1/places/$id", $request->only('name', 'address','latitude','longitude', 'updated_at'), $request);

        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        if ($response->status_code != 200 && $response->status_code != 201) {
            if (isset($response->errors)) {
                foreach ($response->errors as $key => $messages) {
                    foreach ($messages as $msg) {
                        $validator->errors()->add($key, $msg);
                    }
                }
            }
            return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
        }
        return redirect()->route('places.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $response = $this->delete("api/v1/places/$id", $request);
        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        return redirect()->back();
    }
}
