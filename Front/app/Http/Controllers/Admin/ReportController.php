<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use App\Http\Controllers\Controller;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $report)
    {
        $now = Carbon::now();
        $from = '';
        $to = '';
        if (!empty($request->get('from')) || !empty($request->get('to'))) {
            $from = $this->changeDateFormat($request->get('from', ''), 'd-m-Y', 'Y-m-d');
            $to = $this->changeDateFormat($request->get('to', ''), 'd-m-Y', 'Y-m-d');

            $url = "api/v1/reports/{$report}?from={$from}&to={$to}";
        } else {
            // Si los dos campos están vaciós obtener registros de hoy
            $from = $now->format('Y-m-d');
            $to = $now->format('Y-m-d');
            $url = "api/v1/reports/{$report}?from={$from}&to={$to}";
            $request->merge(array('from' => $this->changeDateFormat($from, 'Y-m-d', 'd-m-Y'), 'to' => $this->changeDateFormat($to, 'Y-m-d', 'd-m-Y')));
        }

        if (!empty($request->get('filter_by'))) {
            $url .= '&filter_by='. $request->get('filter_by');
        }

        $response = $this->get($url, $request);
        $experiences = $this->get('api/v1/experiences', $request);
        if (!empty($response->message)) {
            $validator = Validator::make([], []);
            $request->session()->flash('response', [
                'status_code' => $response->status_code,
                'message' => $response->message
            ]);
            if ($response->status_code != 200 && $response->status_code != 201) {
                if (isset($response->errors)) {
                    foreach ($response->errors as $key => $messages) {
                        foreach ($messages as $msg) {
                            $validator->errors()->add($key, $msg);
                        }
                    }
                }
                return redirect()
                ->back()
                ->withInput()
                ->withErrors($validator);
            }
        }
        return view('reports.index')->with([
            'report' => $response->data,
            'experiences' => $experiences,
            'reportType' => $report
        ]);
    }

    /**
     * Change the format of the dates
     *
     * @param  array  $date
     * @param  string  $inputFormat
     * @param  string  $outputFormat
     * @return array;
     */
    private function changeDateFormat($date, $inputFormat, $outputFormat)
    {
        try {
            $date = Carbon::createFromFormat($inputFormat, $date)->format($outputFormat);
        } catch (\Exception $e) {
            // No se pudo formatear la fecha
        }
        return $date;
    }

    public function exportToPDF(Request $request)
    {
        $report = json_decode($request->input('report'));
        $from = $request->input('from');
        $to = $request->input('to');
        $reportType = $request->input('reportType');
        $filterBy = '';
        if (!($report && $from && $to && $reportType)) {
            return;
        }
        if ($reportType === 'sales') {
            $filterBy = $request->input('filter_by');
        }
        $pdf = PDF::loadView('reports.pdf', compact('report', 'from', 'to', 'reportType', 'filterBy'));
        return $pdf->download("{$reportType}_report_from_{$from}_to_{$to}.pdf");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
