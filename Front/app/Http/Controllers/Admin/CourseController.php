<?php

namespace App\Http\Controllers\Admin;

use Constant;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $url = 'api/v1/courses?type=experience';
        $params = ['page' => 1];
        if (!empty($request->get('page'))) {
            $params['page'] = $request->get('page');
        }
        if (!empty($request->get('experience'))) {
            $params['experience'] = $request->get('experience');
        }
        $packages = $this->get($url . '&' . make_url_params($params), $request);
        $experiences = $this->get('api/v1/experiences', $request);

        return view('courses.index')
            ->with([
                'packages' => $packages,
                'experiences' => $experiences,
                'params' => $params
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $title = "Create course";

        $experiences = $this->get("api/v1/experiences", $request);

        return view('courses.form', [
            'title' => $title,
            'formType' => Constant::FORM_CREATE,
            'formAction' => route('courses.store'),
            'experiences' => $experiences
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make([], []);
        $data = $request->all();
        // Se filtran solo los arreglos que por lo menos tengan un valor no vacío, todos los demás son ignorados
        $data['course_rates'] = array_filter(
            json_decode($request->get('course_rates'), true),
            function($array) {
                return $array['from'] != '' || $array['to'] != '' || $array['unit_cost'] != '';
            }
        );
        $data['experiences'] = explode(',', $request->get('experiences', ''));

        $response = $this->post("api/v1/courses", $data, $request);

        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);

        if ($response->status_code != 200 && $response->status_code != 201) {
            if (isset($response->errors)) {
                foreach ($response->errors as $key => $messages) {
                    foreach ($messages as $msg) {
                        $validator->errors()->add($key, $msg);
                    }
                }
            }
            return redirect()
                ->back()
                ->withInput()
                ->withErrors($validator);
        }
        return redirect()->route('courses.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $title = "Show course";

        $experiences = $this->get("api/v1/experiences", $request);
        $package = $this->get("api/v1/courses/$id?include=rates,experiences", $request);

        return view('courses.form', [
            'title' => $title,
            'formType' => Constant::FORM_SHOW,
            'formAction' => '',
            'experiences' => $experiences,
            'package' => $package->data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $title = "Edit course";

        $experiences = $this->get("api/v1/experiences", $request);
        $package = $this->get("api/v1/courses/$id?include=rates,experiences", $request);

        return view('courses.form', [
            'title' => $title,
            'formType' => Constant::FORM_EDIT,
            'formAction' => route('courses.update', ['id' => $id]),
            'experiences' => $experiences,
            'package' => $package->data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make([], []);

        $data = $request->all();

        // Se filtran solo los arreglos que por lo menos tengan un valor no vacío, todos los demás son ignorados
        $data['course_rates'] = array_filter(
            json_decode($request->get('course_rates'), true),
            function($array) {
                return $array['from'] != '' || $array['to'] != '' || $array['unit_cost'] != '';
            }
        );
        $data['experiences'] = explode(',', $request->get('experiences', ''));

        $response = $this->put("api/v1/courses/$id", $data, $request);

        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        if ($response->status_code != 200 && $response->status_code != 201) {
            if (isset($response->errors)) {
                foreach ($response->errors as $key => $messages) {
                    foreach ($messages as $msg) {
                        $validator->errors()->add($key, $msg);
                    }
                }
            }
            return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
        }
        return redirect()->route('courses.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $response = $this->delete("api/v1/courses/$id", $request);
        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        return redirect()->back();
    }

    /**
     * Activate / deactivate the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate(Request $request, $id)
    {
        $response = $this->put("api/v1/courses/$id/activate", [], $request);
        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        return redirect()->back();
    }
}
