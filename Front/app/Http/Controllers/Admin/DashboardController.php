<?php

namespace App\Http\Controllers\Admin;

use Constant;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * Display the app dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $url = "api/v1/dashboard";
        $response = $this->get($url, $request);
        if (!empty($response->message)) {
            $validator = Validator::make([], []);
            $request->session()->flash('response', [
                'status_code' => $response->status_code,
                'message' => $response->message
            ]);
            if ($response->status_code != 200 && $response->status_code != 201) {
                if (isset($response->errors)) {
                    foreach ($response->errors as $key => $messages) {
                        foreach ($messages as $msg) {
                            $validator->errors()->add($key, $msg);
                        }
                    }
                }
                return redirect()
                ->back()
                ->withInput()
                ->withErrors($validator);
            }
        }
        return view('dashboard.index')->with([
            'dataDashboard' => $response,
        ]);
    }
}
