<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Constant;
use Illuminate\Support\Facades\Auth;
use Validator;
use \Carbon\Carbon;

class AccommodationController extends Controller
{
    private function createSchedulerAccommodationData($rooms)
    {
        $ret = [
            'rooms' => [],
        ];

        foreach ($rooms->data as $room) {
            $roomTypes = [];
            $housingRooms = $room->housingRooms->data;
            for ($i = 0; $i < count($housingRooms); $i++) {
                array_push($roomTypes, $housingRooms[$i]->roomType->data->name);
            }
            array_push($ret['rooms'], [
                'id' => $room->id,
                'name' => $room->name,
                'residence' => $room->residence->data->name,
                'type' => $roomTypes,
                'beds' => $room->beds,
                'housing_slug' => $room->residence->data->housing->data->slug,
                'blockedDates' => $room->blockedDates
            ]);
        }

        return $ret;
    }

    /**
     * Display the accommodation scheduler.
     *
     * @return \Illuminate\Http\Response
     */
    public function scheduler(Request $request)
    {
        $now = Carbon::now();
        $rooms = $this->get(
            'api/v1/rooms?include=blockedDates,housingRooms,residence.housing',
            $request
        );
        $accommodationData = $this->createSchedulerAccommodationData($rooms);
        $accommodationData['rooms'] = $this->sortRecords($accommodationData['rooms'], 'asc');

        return view('accommodations.scheduler')
            ->with(compact(
                'accommodationData',
                'now'
            ));
    }

    /**
     * Ordena los registros de forma ascendente(asc) o descendente(desc)
     */
    private function sortRecords($rooms, $dir = 'asc') {
        $roomsCollection = $this->groupRecords($rooms);
        $roomsOrdered = [];
        // Odenar por nombre de housing
        if ($dir === 'asc') {
            $roomsCollection = $roomsCollection->sortBy(function ($item, $key) {
                return $key;
            });
        } else {
            $roomsCollection = $roomsCollection->sortByDesc(function ($item, $key) {
                return $key;
            });
        }
        // Odenar por nombre de residencia
        foreach ($roomsCollection as $key => $rCollection) {
            if ($dir === 'asc') {
                $roomsCollection[$key] = $rCollection->sortBy(function ($item, $key) {
                    return str_slug($key, '_');
                });
            } else {
                $roomsCollection[$key] = $rCollection->sortByDesc(function ($item, $key) {
                    return str_slug($key, '_');
                });
            }
            // Ordenar por nombre de cuarto
            foreach($roomsCollection[$key] as $key2 => $collection) {
                if ($dir === 'asc') {
                    $roomsCollection[$key][$key2] = $collection->sortBy('name');
                } else {
                    $roomsCollection[$key][$key2] = $collection->sortByDesc('name');
                }
                // Guardar los datos ordenados en el array que se retornará
                $roomsOrdered = array_merge($roomsOrdered, $roomsCollection[$key][$key2]->toArray());
            }
        }

        return $roomsOrdered;
    }

    /**
     * Agrupa los registros por tipo de housing y tipo de residencia
     */
    private function groupRecords($rooms) {
        $roomsCollection = collect($rooms);
        // Agrupar por tipo de housing
        $roomsCollection = $roomsCollection->groupBy('housing_slug');
        // Agrupar por residencia
        foreach ($roomsCollection as $key => $rCollection) {
            $roomsCollection[$key] = $rCollection->groupBy('residence');
        }

        return $roomsCollection;
    }

    public function index(Request $request)
    {
        $url = "api/v1/rooms/accommodations";
        $params = [];
        if ($request->has('monthYear')) {
            $params['monthYear'] = $request->input('monthYear', '');
        }
        $response = $this->get($url . '?' .make_url_params($params), $request);
        if ($request->expectsJson()) {
            return response()->json($response, 200);
        }
    }
}
