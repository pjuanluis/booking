<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Excel;
use Validator;
use Constant;
use Carbon\Carbon;

class PettyCashController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Show petty caches of consumption centers
        if (
            !empty($request->get('show')) &&
            (strcasecmp($request->get('show'), 'consumption-centers') == 0)
        )  {
            $request->request->remove('show');
            $request->request->add(['is-petty-cash' => 1]);
            return (new ConsumptionCenterController)->index($request);
        }

        // Obtener listado de movimientos de un centro de consumo (caja chica)
        $url = "api/v1/petty-cashes";
        $params = ['page' => 1];
        if (!empty($request->get('page'))) {
            $params['page'] = $request->get('page');
        }

        $params['petty-cash'] = $request->get('petty-cash', 0);
        $url .= "?with_consumption_center_id={$params['petty-cash']}";
        $consumptionCenter = $this->get($url . '&' . make_url_params($params), $request);

        return view('petty-cashes.moves.index')
        ->with([
            'consumptionCenter' => $consumptionCenter,
            'params' => $params
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $isDeposit = $request->get('is-deposit', 0);
        $id = $request->get('petty-cash', 0);
        $pettyCash = $this->get("api/v1/consumption-centers/$id", $request);
        return view('petty-cashes.moves.form')->with([
            'formAction'=> route('petty-cashes.store'),
            'formType' => Constant::FORM_CREATE,
            'isDeposit' => $isDeposit,
            'pettyCash' => $pettyCash
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make([], []);
        $data = $request->all();
        $file = $request->file('file');
        if ($file != null && $file->isValid()) {
            $data['file'] = [
                'name' => $file->getClientOriginalName(),
                'contents' => base64_encode(file_get_contents($file->getRealPath()))
            ];
        }

        $response = $this->post("api/v1/petty-cashes", $data, $request);

        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        if ($response->status_code != 200 && $response->status_code != 201) {
            if (isset($response->errors)) {
                foreach ($response->errors as $key => $messages) {
                    foreach ($messages as $msg) {
                        $validator->errors()->add($key, $msg);
                    }
                }
            }
            return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
        }
        return redirect()->route('petty-cashes.index', ['petty-cash' => $request->get('consumption_center_id', 0)]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $move = $this->get("api/v1/petty-cashes/$id?include=consumptionCenter", $request);
        $isDeposit = $move->data->is_deposit;
        $pettyCash = $move->data->consumptionCenter;
        return view('petty-cashes.moves.form')->with([
            'formAction'=> '#',
            'formType' => Constant::FORM_SHOW,
            'isDeposit' => $isDeposit,
            'pettyCash' => $pettyCash,
            'move' => $move->data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Export to csv.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function exportToCsv(Request $request)
    {
        $url = "api/v1/petty-cashes?with_consumption_center_id={$request->get('petty-cash', 0)}&no-paginate=1";
        $consumptionCenter = $this->get($url, $request);

        $moves = null;
        if (isset($consumptionCenter->data->pettyCashes)) {
            $moves = $consumptionCenter->data->pettyCashes;
        } else {
            $moves = $consumptionCenter;
        }

        // Formatear la información
        $dataExport = array_map(function($m) {
            $date = new Carbon($m->date);
            $date->tz = 'America/Mexico_City';
            return [
                'date' => $date->format('d-m-Y H:i:s'),
                'description' => $m->description,
                'responsible' => $m->user->data->full_name?? '--',
                'file' => isset($m->file->data->path)? 'Si': 'No',
                'deposit' => $m->is_deposit? $m->amount: '--',
                'withdrawal' => $m->is_deposit? '--' : $m->amount,
                'balance' => $m->balance
            ];
        }, $moves->data);
        // Agregar los títulos de las columnas
        $dataExport = array_merge([['Fecha', 'Description', 'Responsible', 'File', 'Deposit', 'Withdrawal', 'Balance']], $dataExport);
        // Exportar los datos
        return Excel::create('petty-cash', function($excel) use($dataExport) {
            $excel->setTitle('petty-cash');
            $excel->setCompany('Experiencia');
            $excel->sheet("petty-cash", function($sheet) use($dataExport) {
                $sheet->fromArray($dataExport, null, 'A1', true, false);
            });
        })->export('csv');
    }
}
