<?php

namespace App\Http\Controllers\Admin;
use Constant;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HousingRoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $housingId)
    {
        $housing = $this->get("api/v1/housing/$housingId?include=housingRooms", $request);
        return view('housing.rooms.index')->with([
            'housing' => $housing->data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $housingId)
    {
        $title = "Create Housing / Room type";

        $roomTypes = $this->get("api/v1/room-types?no-paginate=1&is_active=1", $request);

        return view('housing.rooms.form')->with([
            'formAction'=> route('housing.rooms.store',['id' => $housingId]),
            'formType' => Constant::FORM_CREATE,
            'roomTypes' => $roomTypes,
            'title' => $title
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $housingId)
    {
        $validator = Validator::make([], []);

        $response = $this->post("api/v1/housing/$housingId/rooms", $request->all(), $request);

        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        if ($response->status_code != 200 && $response->status_code != 201) {
            if (isset($response->errors)) {
                foreach ($response->errors as $key => $messages) {
                    foreach ($messages as $msg) {
                        $validator->errors()->add($key, $msg);
                    }
                }
            }
            return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
        }
        return redirect()->route('housing.rooms.index', ['id' => $housingId]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $title = "Show Housing / Room type";
        $housingRoom = $this->get("api/v1/housing-rooms/$id", $request);
        $roomTypes = $this->get("api/v1/room-types?no-paginate=1&is_active=1", $request);
        return view('housing.rooms.form')->with([
            'housingRoom' => $housingRoom->data,
            'formAction'=> '',
            'formType' => Constant::FORM_SHOW,
            'roomTypes' => $roomTypes,
            'title' => $title,
            'housing' => $request->get('housing', 0)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $title = "Edit Housing / Room type";
        $housingRoom = $this->get("api/v1/housing-rooms/$id", $request);
        $roomTypes = $this->get("api/v1/room-types?no-paginate=1&is_active=1", $request);
        return view('housing.rooms.form')->with([
            'housingRoom' => $housingRoom->data,
            'formAction'=> route('housing.rooms.update', ['id' => $id, 'housing' => $request->get('housing', '0')]),
            'formType' => Constant::FORM_EDIT,
            'roomTypes' => $roomTypes,
            'title' => $title
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make([], []);

        $response = $this->put("api/v1/housing-rooms/$id", $request->all(), $request);

        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        if ($response->status_code != 200 && $response->status_code != 201) {
            if (isset($response->errors)) {
                foreach ($response->errors as $key => $messages) {
                    foreach ($messages as $msg) {
                        $validator->errors()->add($key, $msg);
                    }
                }
            }
            return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
        }
        return redirect()->route('housing.rooms.index', ['id' => $request->get('housing', '0')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $response = $this->delete("api/v1/housing-rooms/$id", $request);
        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        return redirect()->back();
    }

    /**
     * Activate / deactivate the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate(Request $request, $id)
    {
        $response = $this->put("api/v1/housing-rooms/$id/activate", [], $request);
        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        return redirect()->back();
    }
}
