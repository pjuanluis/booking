<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Constant;
use Validator;

class ImportController extends Controller
{
    /**
     * Display a listing of importations.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $url = 'api/v1/imports?';
        $params = ['page' => 1];
        if (!empty($request->get('page'))) {
            $params['page'] = $request->get('page');
        }
        $imports = $this->get($url . '&' . make_url_params($params), $request);
        return view('imports.index')
                ->with([
                    'imports' => $imports,
                    'params' => $params
                ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = ['success' => true];
        $data = ['description' => $request->get('description')];
        $file = $request->file('file');
        if (!$file->isValid()) {
            $response['success'] = false;
            $response['message'] = "Failed to upload file";
            return response()->json($response, 500);
        }
        $data['file'] = [
            'name' => $file->getClientOriginalName(),
            'contents' => base64_encode(file_get_contents($file->getRealPath()))
        ];
        // Enviar request al API
        $response = $this->post("api/v1/imports", $data, $request);
        return response()->json($response, $response->status_code);
    }

    /**
     * Display the specified import.
     *
     * @param  int  $id
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $import = $this->get("api/v1/imports/$id", $request);
        return response()->json($import, $import->status_code);
    }
}