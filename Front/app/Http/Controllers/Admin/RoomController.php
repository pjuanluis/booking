<?php

namespace App\Http\Controllers\Admin;

use Constant;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \Carbon\Carbon;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $residenceId)
    {
        $url = "api/v1/residences/{$residenceId}?include=rooms";
        $params = ['page' => 1];
        if (!empty($request->get('page'))) {
            $params['page'] = $request->get('page');
        }
        $residence = $this->get($url . '&' . make_url_params($params), $request);
        return view('rooms.index')
                ->with([
                    'residence' => $residence,
                    'params' => $params
                ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $residenceId)
    {
        $title = "Create room";
        $residence = $this->get("api/v1/residences/{$residenceId}?include=housing.housingRooms&is_active=1", $request);
        return view('rooms.form')->with([
            'residence' => $residence,
            'formAction'=> route('rooms.store', ['residence' => $residenceId]),
            'formType' => Constant::FORM_CREATE,
            'title' => $title
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $residenceId)
    {
        $validator = Validator::make([], []);

        $data = $request->all();
        $data['residence_id'] = $residenceId;

        $response = $this->post("api/v1/rooms", $data, $request);

        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        if ($response->status_code != 200 && $response->status_code != 201) {
            if (isset($response->errors)) {
                foreach ($response->errors as $key => $messages) {
                    foreach ($messages as $msg) {
                        $validator->errors()->add($key, $msg);
                    }
                }
            }
            return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
        }
        return redirect()->route('rooms.index', ['residence' => $residenceId]);
    }

    private function converBlockedDates(&$room)
    {
        if (isset($room->data->blockedDates->data)) {
            foreach ($room->data->blockedDates->data as $v) {
                $v->from = Carbon::parse($v->from_at)->format('d-m-Y');
                $v->to = Carbon::parse($v->to_at)->format('d-m-Y');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $residenceId, $id)
    {
        $title = "Show room";
        $residence = $this->get("api/v1/residences/{$residenceId}?include=housing.housingRooms&is_active=1", $request);
        $room = $this->get("api/v1/rooms/{$id}?include=blockedDates,housingRooms", $request);

        $this->converBlockedDates($room);

        return view('rooms.form')->with([
            'residence' => $residence,
            'formAction'=> '',
            'formType' => Constant::FORM_SHOW,
            'room' => $room->data,
            'title' => $title,
            'residenceId' => $residenceId
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $residenceId, $id)
    {
        $title = "Edit room";
        $residence = $this->get("api/v1/residences/{$residenceId}?include=housing.housingRooms&is_active=1", $request);
        $room = $this->get("api/v1/rooms/{$id}?include=blockedDates,housingRooms", $request);

        $this->converBlockedDates($room);

        return view('rooms.form')->with([
            'residence' => $residence,
            'formAction'=> route('rooms.update', ['residence' => $residenceId, 'id' => $id]),
            'formType' => Constant::FORM_EDIT,
            'room' => $room->data,
            'title' => $title
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $residenceId, $id)
    {
        $validator = Validator::make([], []);

        $data = $request->all();
        $data['residence_id'] = $residenceId;
        $data['ranges_date_deleted'] = json_decode($request->input('ranges_date_deleted', []));

        $response = $this->put("api/v1/rooms/$id", $data, $request);

        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        if ($response->status_code != 200 && $response->status_code != 201) {
            if (isset($response->errors)) {
                foreach ($response->errors as $key => $messages) {
                    foreach ($messages as $msg) {
                        $validator->errors()->add($key, $msg);
                    }
                }
            }
            return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
        }
        return redirect()->route('rooms.index', ['residence' => $residenceId]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $residenceId, $id)
    {
        $response = $this->delete("api/v1/rooms/$id", $request);
        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        return redirect()->back();
    }
}
