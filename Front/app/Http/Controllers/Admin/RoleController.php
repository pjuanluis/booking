<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Constant;
use Validator;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = 'Roles';
        $url = 'api/v1/roles';
        $params = $this->prepareParams($request, ['page']);

        $roles = $this->get($url . '?' . make_url_params($params), $request);

        return view('roles.index')
            ->with(compact(
                'title',
                'roles'
            ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $title = 'Edit Role';
        $formType = Constant::FORM_CREATE;
        $formAction = route('roles.store');

        $modules = $this->get("api/v1/system-modules?include=permissions&order=id&dir=asc", $request);
        $accessesTo = [];

        return view('roles.form')
            ->with(compact(
                'title',
                'formType',
                'formAction',
                'accessesTo',
                'modules'
            ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [];
        $validator = Validator::make([], []);
        $data['name'] = $request->get('name');
        $accesses = $request->get('accesses');
        if ($accesses == null) {
            $accesses = array();
        } else {
            $accessesTmp = [];
            foreach ($accesses as $key => $modules) {
                $permissionModule = [];
                foreach ($modules as $key2 => $permission) {
                    $permissionModule[] = [
                        'id' => $permission
                    ];
                }
                // Poner el array en el formato que el API pueda leerlo
                $accessesTmp[] = [
                    'module_id' => $key,
                    'permissions' => $permissionModule
                ];
            }
            $accesses = $accessesTmp;
        }
        $data['accesses'] = $accesses;

        $response = $this->post("api/v1/roles", $data, $request);
        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);

        if ($response->status_code != 200 && $response->status_code != 201) {
            if (isset($response->errors)) {
                foreach ($response->errors as $key => $messages) {
                    foreach ($messages as $msg) {
                        $validator->errors()->add($key, $msg);
                    }
                }
            }
            return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
        }
        return redirect()->route('roles.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $title = 'Edit Role';
        $formType = Constant::FORM_EDIT;
        $formAction = route('roles.update', ['id' => $id]);

        $role = $this->get("api/v1/roles/$id?include=accesses", $request);
        $modules = $this->get("api/v1/system-modules?include=permissions&order=id&dir=asc", $request);

        $accessesTo = [];
        foreach(isset($role->data->accesses->data) ? $role->data->accesses->data : [] as $a) {
            $accessesTo[$a->module->data->id] = collect($a->permissions->data)->pluck('id')->toArray();
        };

        return view('roles.form')
            ->with(compact(
                'title',
                'formType',
                'formAction',
                'role',
                'modules',
                'accessesTo'
            ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make([], []);
        $data = [];
        $data['updated_at'] = $request->get('updated_at');
        $data['name'] = $request->get('name');
        $accesses = $request->get('accesses');
        if ($accesses == null) {
            $accesses = array();
        } else {
            $accessesTmp = [];
            foreach ($accesses as $key => $modules) {
                $permissionModule = [];
                foreach ($modules as $key2 => $permission) {
                    $permissionModule[] = [
                        'id' => $permission
                    ];
                }
                // Poner el array en el formato que el API pueda leerlo
                $accessesTmp[] = [
                    'module_id' => $key,
                    'permissions' => $permissionModule
                ];
            }
            $accesses = $accessesTmp;
        }
        $data['accesses'] = $accesses;
        $response = $this->put("api/v1/roles/$id", $data, $request);
        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        if ($response->status_code != 200) {
            if (isset($response->errors)) {
                foreach ($response->errors as $key => $messages) {
                    foreach ($messages as $msg) {
                        $validator->errors()->add($key, $msg);
                    }
                }
            }
            return redirect()
                ->back()
                ->withInput()
                ->withErrors($validator);
        }
        return redirect()->route('roles.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $response = $this->delete("api/v1/roles/$id", $request);
        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        return redirect()->back();
    }

    /**
     * Activate / deactivate the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate(Request $request, $id)
    {
        $response = $this->put("api/v1/roles/$id/activate", [], $request);
        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        return redirect()->route('roles.index');
    }
}
