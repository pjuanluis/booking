<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;

class BookingController extends Controller
{
    /**
     * Display a listing of commands.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $experiences = $this->get('api/v1/catalogues/experiences', $request);
        $experiences = isset($experiences->data) ? $experiences->data : [];
        $packages = $this->get('api/v1/packages/groupbyexperiences?is_active=1', $request);
        $packages = isset($packages->data) ? $packages->data : [];
        $housing = $this->get('api/v1/catalogues/housing?is_active=1&no-paginate=1&include=housingRooms:is_active(1),housingRooms.packages.rates', $request);
        $housing = isset($housing->data) ? $housing->data : [];
        $pickup = $this->get('api/v1/catalogues/pickup', $request);
        $pickup = isset($pickup->data) ? $pickup->data : [];
        return view('public.booking.index')->with([
            'experiences' => $experiences,
            'packages' => $packages,
            'housing' => $housing,
            'pickup' => $pickup,
            'formAction'=> route('public.bookings.store'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        error_log("Book");
        $data0 = $request->all();
        $data = [];
        $tmp = null;
        // Armar arreglo de cursos
        if (!empty($data0['courses'])) {
            $tmp = $data0['courses'];
            $data['courses'] = [];
            foreach ($tmp as $r) {
                $data['courses'][] = [
                    'package_id' => $r['package_id'],
                    'quantity' => $r['quantity'],
                    'from_date' => $data0['from'],
                    'to_date' => $data0['to'],
                ];
            }
        }
        // Armar arreglo de hospedaje
        if (!empty($data0['housing'])) {
            $tmp = $data0['housing'];
            $data['housing'] = [];
            foreach ($tmp as $r) {
                $data['housing'][] = [
                    'housing_room_package_id' => $r['housing_room_package_id'],
                    'quantity' => $r['quantity'],
                    'unit' => $r['unit'],
                    'from_date' => $r['from'],
                    'to_date' => $r['to'],
                    'people' => $r['people'],
                ];
            }
        }
        $data['travel_partner'] = $data0['travel_partner'];
        if (!empty($data0['pickup'])) {
            $data['pickup_id'] = $data0['pickup']['id'];
        }
        $data['client_first_name'] = $data0['client_first_name'];
        $data['client_last_name'] = $data0['client_last_name'];
        $data['client_email'] = $data0['client_email'];
        $data['client_phone'] = $data0['client_phone'];
        $data['client_comments'] = $data0['comments'];
        error_log("Send to API");
        $response = $this->post("api/v1/bookings", $data, $request);
        error_log("RESPONSE: ".json_encode($response));
        if ($request->expectsJson()) {
            return response()->json($response, 200);
        }
    }

    /**
     * Show the payment form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function payment(Request $request)
    {
        $token = $request->get('token');
        if (empty($token)) {
            abort(404);
        }
        $command = $this->get("api/v1/commands/getbytoken/$token", $request);
        return view('public.booking.prepay')->with([
            'command' => $command->data,
            'formAction'=> route('public.bookings.process_payment'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function processPayment(Request $request)
    {
        $token = $request->get('bk_token');
        $command = $this->get("api/v1/commands/getbytoken/$token", $request);
        $data = [
            'conekta_token_card_id' => $request->get('card_token'),
            'code' => $command->data->booking_code
        ];
        $response = $this->post("api/v1/payments/conekta", $data, $request, true);
        if ($response->status_code == 200 || $response->status_code == 201) {
            return view('public.booking.pay-success');
        }
    }

    /**
     * Show the payment form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function personalInfo(Request $request)
    {
        $token = $request->get('token');
        if (empty($token)) {
            abort(404);
        }
        $command = $this->get("api/v1/commands/getbytoken/$token?include=commandCourses.package,commandHousing,client", $request);
        if (!empty($command->data->client->data)) {
            abort(404);
        }
        $countries = $this->get('api/v1/catalogues/countries?order_by=order', $request);
        $countries = !isset($countries->data) ? [] : $countries->data;
        $pickup = $this->get('api/v1/catalogues/pickup', $request);
        $pickup = !isset($pickup->data) ? [] : $pickup->data;
        $genders = $this->get('api/v1/catalogues/genders', $request);
        return view('public.booking.more-information')->with([
            'command' => $command->data,
            'formAction'=> route('public.bookings.store-client'),
            'countries' => $countries,
            'pickup' => $pickup,
            'genders' => $genders,
        ]);
    }

    /**
     * Store the client data.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeClient(Request $request) {
        $data = [];
        $validator = Validator::make([], []);
        $data = $request->all();
        $data['spanish_knowledge_duration'] = '0';
        if (!empty($data['spanish_level'])) {
            if ($data['spanish_level'] !== 'none') {
                $data['spanish_knowledge_duration'] = $data['spanish_knowlege_number'] . ' ' . $data['spanish_knowlege_type'];
            }
        }
        $data['surf_experience_duration'] = '0';
        if (!empty($data['has_surf_experience'])) {
            $data['surf_experience_duration'] = $data['surf_experience_number'] . ' ' . $data['surf_experience_type'];
        }
        $data['volunteer_experience_duration'] = '0';
        if (!empty($data['has_volunteer_experience'])) {
            $data['volunteer_experience_duration'] = $data['volunteer_experience_number'] . ' ' . $data['volunteer_experience_type'];
        }
        else {
            $data['volunteer_experience'] = '';
        }
        if (empty($data['arrival_date'])) {
            unset($data['arrival_date']);
        } else {
            try {
                $data['arrival_date'] = Carbon::createFromFormat('d-m-Y', $data['arrival_date'])->format('Y-m-d');
            } catch (\Exception $e) {
                // No se pudo formatear la fecha
            }
        }
        if (empty($data['arrival_time'])) {
            unset($data['arrival_time']);
        }
        else if (strlen($data['arrival_time']) === 4) {
            $data['arrival_time'] = '0' . $data['arrival_time'];
        }
        if (empty($data['departure_date'])) {
            unset($data['departure_date']);
        } else {
            try {
                $data['departure_date'] = Carbon::createFromFormat('d-m-Y', $data['departure_date'])->format('Y-m-d');
            } catch (\Exception $e) {
                // No se pudo formatear la fecha
            }
        }
        if (empty($data['departure_time'])) {
            unset($data['departure_time']);
        }
        else if (strlen($data['departure_time']) === 4) {
            $data['departure_time'] = '0' . $data['departure_time'];
        }
        if (!empty($data['photo_data'])) {
            $name = pathinfo($data['photo_name'], PATHINFO_FILENAME);
            $name .= '.png';
            $data['file'] = [
                'contents' => explode('base64,', $data['photo_data'])[1],
                'name' => $name
            ];
            unset($data['photo_data']);
            unset($data['photo_name']);
        }
        // Como es cliente nuevo, agregar un password aleatorio
        $data['password'] = str_random(8);

        $response = $this->post("api/v1/public-clients", $data, $request);
        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);
        if ($response->status_code != 200 && $response->status_code != 201) {
            if (isset($response->errors)) {
                foreach ($response->errors as $key => $messages) {
                    foreach ($messages as $msg) {
                        $validator->errors()->add($key, $msg);
                    }
                }
            }
            return redirect()
                    ->back()
                    ->withInput()
                    ->withErrors($validator);
        }
        return view('public.booking.info-success');
    }
}
