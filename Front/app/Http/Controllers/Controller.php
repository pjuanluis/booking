<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Client;
use Validator;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    /**
     * The guzzle http client to fetch data from the api
     * @var \GuzzleHttp\Client 
     */
    protected $client;
    
    /**
     * Array with access and refresh tokens
     * @var array 
     */
    protected $tokens;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => config('services.api.url') . '/',
        ]);
    }
    
    /**
     * Perform a request to the specified experiencia API endpoint
     * @param string $endpoint
     * @param Request $request
     * @return mixed
     */
    public function get($endpoint, Request $request)
    {
        $this->tokens = $request->session()->get('oauth_token');
        $response = $this->client->request('GET', $endpoint, [
            'headers' => [
                'authorization' => 'Bearer ' . $this->tokens['access_token'],
                'accept' => 'application/vnd.experiencia.v1+json',
                'content-type' => 'application/json'
            ],
            'http_errors' => false
        ]);
        $statusCode = $response->getStatusCode();
        $response = json_decode($response->getBody());
        $response->status_code = $statusCode;
        $response->success = true;
        if (!isset($response->data)) {
            // Error de autenticación, intentar refrescar el token
            if ($response->status_code == 500 && $response->message === 'Unauthenticated') {
                if ($this->tokens['grant_type'] === 'password') {
                    if ($this->refreshToken($this->tokens['refresh_token'], $request) === null) {
                        // Si no se logró refrescar el token, finalizar la sesión
                        $this->logout($request);
                        throw new \Illuminate\Auth\AuthenticationException();
                    }
                }
                else {
                    if ($this->clientCredentials($request) === null) {
                        abort(500);
                    }
                }
                // Reintentar el request
                $response = $this->client->request('GET', $endpoint, [
                    'headers' => [
                        'authorization' => 'Bearer ' . $this->tokens['access_token'],
                        'accept' => 'application/vnd.experiencia.v1+json',
                        'content-type' => 'application/json'
                    ],
                    'http_errors' => false
                ]);
                $response = json_decode($response->getBody());
                if (!isset($response->data)) {
                    $response->success = false;
                }
            }
            else {
                $response->success = false;
            }
        }
        if (!$response->success) {
            if ($response->status_code == 403) {
                abort(403);
            }
            else if ($response->status_code == 404) {
                abort(404);
            }
            else if ($response->status_code == 500) {
                abort(500);
            }
        }
        return $response;
    }

    public function post($endpoint, array $data, Request $request, $showErrors = false)
    {
        return $this->sendData($endpoint, $data, $request, 'POST', $showErrors);
    }
    
    public function put($endpoint, array $data, Request $request, $showErrors = false) {
        return $this->sendData($endpoint, $data, $request, 'PUT', $showErrors);
    }
    
    private function sendData($endpoint, array $data, Request $request, $method = 'POST', $showErrors = false) {
        $this->tokens = $request->session()->get('oauth_token');
        $response = $this->client->request($method, $endpoint, [
            'headers' => [
                'authorization' => 'Bearer ' . $this->tokens['access_token'],
                'accept' => 'application/vnd.experiencia.v1+json',
                'content-type' => 'application/json'
            ],
            'json' => $data,
            'http_errors' => false
        ]);
        $statusCode = $response->getStatusCode();
        $response = json_decode($response->getBody());
        $response->status_code = $statusCode;
        // Error de autenticación, intentar refrescar el token
        if ($response->status_code == 500 && $response->message === 'Unauthenticated') {
            if ($this->tokens['grant_type'] === 'password') {
                if ($this->refreshToken($this->tokens['refresh_token'], $request) === null) {
                    // Si no se logró refrescar el token, finalizar la sesión
                    $this->logout($request);
                    throw new \Illuminate\Auth\AuthenticationException();
                }
            }
            else {
                if ($this->clientCredentials($request) === null) {
                    abort(500);
                }
            }
            // Reintentar el request
            $response = $this->client->request($method, $endpoint, [
                'headers' => [
                    'authorization' => 'Bearer ' . $this->tokens['access_token'],
                    'accept' => 'application/vnd.experiencia.v1+json',
                    'content-type' => 'application/json'
                ],
                'json' => $data,
                'http_errors' => false
            ]);
            $response = json_decode($response->getBody());
        }
        if ($response->status_code == 200 || $response->status_code == 201) {
            $response->success = true;
        }
        else {
            $response->success = false;
            if ($response->status_code == 403) {
                abort(403);
            }
            else if ($response->status_code == 404) {
                abort(404);
            }
            else if ($response->status_code == 500) {
                if (!$showErrors) {
                    abort(500);
                }
                else {
                    abort(500, $response->message);
                }
            }
        }
        return $response;
    }

    public function delete($endpoint, Request $request)
    {
        $this->tokens = $request->session()->get('oauth_token');
        $response = $this->client->request('DELETE', $endpoint, [
            'headers' => [
                'authorization' => 'Bearer ' . $this->tokens['access_token'],
                'accept' => 'application/vnd.experiencia.v1+json',
                'content-type' => 'application/json'
            ],
            'http_errors' => false
        ]);
        $response = json_decode($response->getBody());
        // Error de autenticación, intentar refrescar el token
        if ($response->status_code == 500 && $response->message === 'Unauthenticated') {
            if ($this->tokens['grant_type'] === 'password') {
                if ($this->refreshToken($this->tokens['refresh_token'], $request) === null) {
                    // Si no se logró refrescar el token, finalizar la sesión
                    $this->logout($request);
                    throw new \Illuminate\Auth\AuthenticationException();
                }
            }
            else {
                if ($this->clientCredentials($request) === null) {
                    abort(500);
                }
            }
            // Reintentar el request
            $response = $this->client->request('DELETE', $endpoint, [
                'headers' => [
                    'authorization' => 'Bearer ' . $this->tokens['access_token'],
                    'accept' => 'application/vnd.experiencia.v1+json',
                    'content-type' => 'application/json'
                ],
                'http_errors' => false
            ]);
            $response = json_decode($response->getBody());
        }
        if ($response->status_code == 200) {
            $response->success = true;
        }
        else {
            $response->success = false;
            if ($response->status_code == 403) {
                abort(403);
            }
            else if ($response->status_code == 404) {
                abort(404);
            }
            else if ($response->status_code == 500) {
                abort(500);
            }
        }
        return $response;
    }
    
    protected function refreshToken($refreshToken, Request $request)
    {
        $response = $this->client->request('POST', 'api/v1/oauth/token', [
            'form_params' => [
                "grant_type" => "refresh_token",
                "client_id" => config('services.api.client_id'),
                "client_secret" => config('services.api.client_secret'),
                "refresh_token" => $refreshToken,
            ],
            'http_errors' => false
        ]);
        $response = json_decode($response->getBody());
        if (!isset($response->error)) {
            $response->grant_type = 'password';
            $request->session()->put('oauth_token', (array) $response);
            $this->tokens = (array) $response;
            // Verificar cookie de tokens (existe cuando se activó la funcion remember me)
            $cookie = Cookie::get(Auth::guard()->getProvider()->getApiCookieName());
            if ($cookie !== null) {
                // Actualizar tokens en la cookie
                Auth::guard()->getCookieJar()->queue(
                    Auth::guard()->getCookieJar()->forever(
                        Auth::guard()->getProvider()->getApiCookieName(),
                        $this->tokens
                    )
                );
            }
            return $response;
        }
        return null;
    }
    
    protected function clientCredentials(Request $request)
    {
        $response = $this->client->request('POST', 'api/v1/oauth/token', [
            'form_params' => [
                "grant_type" => "client_credentials",
                "client_id" => config('services.api.client_id'),
                "client_secret" => config('services.api.client_secret'),
            ],
            'http_errors' => false
        ]);
        if ($response->getStatusCode() == 200) {
            $response = json_decode($response->getBody());
            $response->grant_type = 'client_credentials';
            $request->session()->put('oauth_token', (array) $response);
            $this->tokens = (array) $response;
            return $response;
        }
        return null;
    }
    
    public function logout(Request $request)
    {
        Auth::guard()->logout();
        // Eliminar cookie de tokens del api
        Auth::guard()->getCookieJar()->queue(
            Auth::guard()->getCookieJar()->forget(
                Auth::guard()->getProvider()->getApiCookieName()
            )
        );
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect('/');
    }

    protected function prepareParams(Request $request, $indexes)
    {
        $params = ['page' => 1];
        $tmpRequest = null;
        $index = '';

        for ($i = 0, $size = count($indexes); $i < $size; $i++ ) {
            $index = $indexes[$i];
            $tmpRequest = $request->get($index);
            if (isset($tmpRequest)) {
                $params[$index] = $request->get($index);
            }
        }

        return $params;
    }

    protected function storeInApi($url = '', $data = [], Request $request)
    {
        $validator = Validator::make([], []);
        $response = $this->post($url, $data, $request);
        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);

        if ($response->status_code != 200 && $response->status_code != 201) {
            if (isset($response->errors)) {
                foreach ($response->errors as $key => $messages) {
                    foreach ($messages as $msg) {
                        $validator->errors()->add($key, $msg);
                    }
                }
            }
            return redirect()
                ->back()
                ->withInput()
                ->withErrors($validator);
        }

        return true;
    }

    protected function updateInApi($url = '', $data = [], Request $request)
    {
        $validator = Validator::make([], []);
        $response = $this->put($url, $data, $request);
        $request->session()->flash('response', [
            'status_code' => $response->status_code,
            'message' => $response->message
        ]);

        if ($response->status_code != 200 && $response->status_code != 201) {
            if (isset($response->errors)) {
                foreach ($response->errors as $key => $messages) {
                    foreach ($messages as $msg) {
                        $validator->errors()->add($key, $msg);
                    }
                }
            }
        return redirect()
                ->back()
                ->withInput()
                ->withErrors($validator);
        }

        return true;
    }
}
