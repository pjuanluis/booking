<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use URL;
use Session;
use Redirect;
use Carbon\Carbon;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

class PaypalController extends Controller
{

    private $apiContext;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $config = \Config::get('paypal');
        $this->apiContext = new ApiContext(new OAuthTokenCredential($config['client_id'], $config['secret']));
        $this->apiContext->setConfig($config['settings']);
        parent::__construct();
    }

    /**
     * Store a details of payment with paypal.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createPayment(Request $request)
    {
        $token = $request->get('bk_token');
        $command = $this->get("api/v1/commands/getbytoken/$token?include=payments,commandCourses,commandHousing,pickup&exclude=commandHousing.package", $request);
        $payments = $command->data->payments->data;
        $commandCourses = $command->data->commandCourses->data;
        $commandHousing = $command->data->commandHousing->data;
        $pickup = $command->data->pickup->data;
        $hasPayment = false;
        foreach  ($payments as $p) {
            if ($p->type === 'registration_fee' || $p->type === 'reservation_payment') {
                $hasPayment = true;
                break;
            }
        }
        if ($hasPayment) {
            abort(500, "Your transaction wasn't processed, due to your booking has already been paid.");
        }

        // Calcular el total de todo lo comprado
        $total = 0;
        $itemsArray = [];
        foreach ($commandCourses as $cc) {
            $item = new Item();
            $item->setName($cc->quantity . ' of ' . $cc->package->data->name)
                    ->setCurrency('USD')
                    ->setQuantity(1)
                    ->setPrice($cc->cost);
            $total += $cc->cost;
            $itemsArray[] = $item;
        }
        foreach ($commandHousing as $ch) {
            $item = new Item();
            $item->setName($ch->housing->data->name . " - {$ch->housingRoom->data->name}" . ' from ' .
            (new Carbon($ch->from_date))->format('d-m-Y') . ' to ' . (new Carbon($ch->to_date))->format('d-m-Y'))
                    ->setCurrency('USD')
                    ->setQuantity(1)
                    ->setPrice($ch->cost);
            $total += $ch->cost;
            $itemsArray[] = $item;
        }
        if (!empty($pickup)) {
            $item = new Item();
            $item->setName($pickup->name)
                    ->setCurrency('USD')
                    ->setQuantity(1)
                    ->setPrice($command->data->pickup_cost);
            $total += $command->data->pickup_cost;
            $itemsArray[] = $item;
        }

        $itemList = new ItemList();
        $itemList->setItems($itemsArray);

        $amount = new Amount();
        $amount->setCurrency('USD')
                ->setTotal($total);

        // Guardar el total a pagar
        Session::put('total_to_pay', $total);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
                ->setItemList($itemList)
                ->setDescription('Reservation payment on Experience');

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl(URL::route('paypal.execute') . '?bk_token=' . $command->data->token)
                ->setCancelUrl(URL::route('paypal.execute') . '?bk_token=' . $command->data->token);

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $payment = new Payment();
        $payment->setIntent('Sale')
                ->setPayer($payer)
                ->setRedirectUrls($redirectUrls)
                ->setTransactions(array($transaction));
        try {
            $payment->create($this->apiContext);
        }
        catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                Session::put('paypal_error', 'Connection timeout');
                return redirect()->back();
            }
            else {
                Session::put('paypal_error', 'Some error occur, sorry for the inconvenient.');
                return redirect()->back();
            }
        }
        $redirectUrl = null;
        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirectUrl = $link->getHref();
                break;
            }
        }
        // Add payment ID to session
        Session::put('paypal_payment_id', $payment->getId());
        if (isset($redirectUrl)) {
            // Redirect to paypal
            return Redirect::away($redirectUrl);
        }
        Session::put('paypal_error', 'Unknown error occurred.');
        return redirect()->back();
    }

    public function executePayment(Request $request)
    {
        // Get the payment ID before session clear
        $paymentId = Session::get('paypal_payment_id');
        // Clear the session payment ID
        Session::forget('paypal_payment_id');
        if (empty($request->get('PayerID'))) {
            Session::put('paypal_error', 'Payment failed.');
            return redirect()->route('public.bookings.payment', ['token' => $request->get('bk_token')]);
        }
        $token = $request->get('bk_token');
        $command = $this->get("api/v1/commands/getbytoken/$token?include=payments", $request);
        $payments = $command->data->payments->data;
        $hasPayment = false;
        foreach  ($payments as $p) {
            if ($p->type === 'registration_fee' || $p->type === 'reservation_payment') {
                $hasPayment = true;
                break;
            }
        }
        if ($hasPayment) {
            abort(500, "Your transaction wasn't processed, due to your booking has already been paid.");
        }
        $payment = Payment::get($paymentId, $this->apiContext);
        // PaymentExecution object includes information necessary to execute a PayPal account payment.
        // The payer_id is added to the request query parameters when the user is redirected from
        // Paypal back to your site
        $execution = new PaymentExecution();
        $execution->setPayerId($request->get('PayerID'));
        // Execute the payment
        $result = $payment->execute($execution, $this->apiContext);
        $data = [];
        if ($result->getState() == 'approved') {
            // Send payment to the API
            $data = [
                'concept' => "Reservation payment",
                'amount' => Session::get('total_to_pay'),
                'paid_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'vendor' => "paypal",
                'reference_id' => $paymentId,
                'type' => "reservation_payment",
                'send_mail' => 1
            ];
            $response = $this->post("api/v1/commands/" . $command->data->id . "/payments", $data, $request, true);
            if ($response->status_code == 200 || $response->status_code == 201) {
                // Borrar de la sesión el total
                Session::forget('total_to_pay');
                return redirect('https://www.experienciamexico.mx/confirmation/');
            }
            Session::put('paypal_error', $response->message);
            $route = route('public.bookings.payment');
            return redirect("$route?token=" . $command->data->token);
        }
        Session::put('paypal_error', 'Payment failed.');
        $route = route('public.bookings.payment');
        return redirect("$route?token=" . $command->data->token);
    }

}
