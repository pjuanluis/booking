module.exports = function(grunt) {
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks("grunt-contrib-cssmin");
    grunt.loadNpmTasks("grunt-contrib-copy");

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        // Styles
        less: {
            app: {
                options: {
                    banner: '\n',
                    compress: true
                },
                files: {
                    'resources/assets/grunt/css/app.min.css': [
                        '<%= pkg.project.path.libraries %>/font-awesome-4.7.0/font-awesome.less',
                        '<%= pkg.project.path.less %>/experiencia.less'
                    ]
                }
            },

            public: {
                options: {
                    banner: '\n',
                    compress: true
                },
                files: {
                    'public/css/experiencia.min.css': [
                        '<%= pkg.project.path.libraries %>/bootstrap-3.3.7/less/bootstrap.less',
                        '<%= pkg.project.path.libraries %>/font-awesome-4.7.0/font-awesome.less',
                        '<%= pkg.project.path.less %>/public/main.less'
                    ]
                }
            }
        },

        cssmin: {
            options: {
                mergeIntoShorthands: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                    'resources/assets/grunt/css/styles.min.css': [
                        'resources/assets/css/scheduler.css'
                    ]
                }
            }
        },

        // Scripts
        uglify: {
            app: {
                options: {
                    preserveComments: '/^!/'
                },
                files: {
                    'resources/assets/grunt/js/app.min.js': [
                        '<%= pkg.project.path.js %>/<%= pkg.project.name %>.js',
                        '<%= pkg.project.path.js %>/command.js',
                        '<%= pkg.project.path.js %>/scheduler.js',
                        '<%= pkg.project.path.js %>/clients_btn-add-command.js',
                        '<%= pkg.project.path.js %>/block-room.js',
                        '<%= pkg.project.path.js %>/downloader.js',
                        '<%= pkg.project.path.js %>/admin-app.js'
                    ]
                }
            },

            public: {
                options: {
                    preserveComments: '/^!/'
                },
                files: {
                    'public/js/experiencia.min.js': [
                        '<%= pkg.project.path.libraries %>/jquery-3.2.1.js',
                        '<%= pkg.project.path.libraries %>/bootstrap-3.3.7/bootstrap.js',
                        '<%= pkg.project.path.js %>/public/card.js',
                        '<%= pkg.project.path.js %>/public/booking.js',
                        '<%= pkg.project.path.js %>/public/main.js'
                    ]
                }
            }
        },

        concat: {
            dev: {
                options: {
                    preserveComments: '/^!/'
                },
                files: {
                    'resources/assets/grunt/js/app.min.js': [
                        '<%= pkg.project.path.js %>/<%= pkg.project.name %>.js',
                        '<%= pkg.project.path.js %>/command.js',
                        '<%= pkg.project.path.js %>/scheduler.js',
                        '<%= pkg.project.path.js %>/clients_btn-add-command.js',
                        '<%= pkg.project.path.js %>/block-room.js',
                        '<%= pkg.project.path.js %>/downloader.js',
                        '<%= pkg.project.path.js %>/admin-app.js'
                    ]
                }
            },
            app: {
                options: {
                    banner: '/*! Custom <%= pkg.project.name %> js */',
                    preserveComments: '/^!/'
                },
                files: {
                    'public/js/app.min.js': [
                        'node_modules/jquery/dist/jquery.min.js',
                        'node_modules/underscore/underscore-min.js',
                        'node_modules/moment/min/moment.min.js',
                        'node_modules/validator/validator.min.js',
                        'node_modules/bootstrap/dist/js/bootstrap.min.js',
                        'node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        'node_modules/admin-lte/plugins/timepicker/bootstrap-timepicker.min.js',
                        'node_modules/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                        'node_modules/admin-lte/plugins/iCheck/icheck.min.js',
                        'node_modules/select2/dist/js/select2.full.min.js',
                        'node_modules/admin-lte/dist/js/adminlte.min.js',
                        'node_modules/sweetalert2/dist/sweetalert2.min.js',
                        'node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js',
                        'node_modules/croppie/croppie.min.js',
                        'node_modules/p-loading/dist/js/p-loading.min.js',
                        'node_modules/fullcalendar/dist/fullcalendar.js',
                        'node_modules/qtip2/dist/jquery.qtip.min.js',
                        'node_modules/chart.js/dist/Chart.min.js',
                        'node_modules/jquery-slimscroll/jquery.slimscroll.min.js',
                        'resources/assets/grunt/js/app.min.js',
                    ],
                    'public/css/app.min.css': [
                        'node_modules/bootstrap/dist/css/bootstrap.min.css',
                        'node_modules/font-awesome/css/font-awesome.min.css',
                        'node_modules/admin-lte/plugins/iCheck/square/blue.css',
                        'node_modules/select2/dist/css/select2.min.css',
                        'node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'node_modules/admin-lte/plugins/timepicker/bootstrap-timepicker.min.css',
                        'node_modules/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
                        'node_modules/admin-lte/dist/css/AdminLTE.min.css',
                        'node_modules/admin-lte/dist/css/skins/_all-skins.min.css',
                        'node_modules/sweetalert2/dist/sweetalert2.min.css',
                        'node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css',
                        'node_modules/fullcalendar/dist/fullcalendar.min.css',
                        'node_modules/croppie/croppie.css',
                        'node_modules/p-loading/dist/css/p-loading.min.css',
                        'node_modules/qtip2/dist/jquery.qtip.min.css',
                        'resources/assets/grunt/css/styles.min.css',
                        'resources/assets/grunt/css/app.min.css'
                    ]
                }
            }
        },

        copy: {
            fonts: {
                expand: true,
                cwd: 'node_modules/materialize-css/dist/fonts/roboto/',
                flatten: true,
                src: '*.*',
                dest: 'public/fonts/roboto/'
            },
            main: {
                files: [{
                    src: 'node_modules/admin-lte/plugins/iCheck/square/blue.png',
                    dest: 'public/css/blue.png'
                }, {
                    src: 'node_modules/vue/dist/vue.min.js',
                    dest: 'public/js/vue.min.js'
                }, {
                    src: 'node_modules/datatables.net/js/jquery.dataTables.min.js',
                    dest: 'public/js/jquery.dataTables.min.js'
                }, {
                    src: 'node_modules/sprintf-js/dist/sprintf.min.js',
                    dest: 'public/js/sprintf.min.js'
                }]
            }
        },

        watch: {
            scripts: {
                files: [
                    '<%= pkg.project.path.js %>/**/*.js'
                ],
                tasks: [
                    'uglify:app',
                    'uglify:public'
                ]
            },
            styles: {
                files: [
                    '<%= pkg.project.path.less %>/**/*.less'
                ],
                tasks: [
                    'less:app',
                    'less:public'
                ]
            }
        }
    });

    grunt.registerTask('dev', [
        'less:app',
        'cssmin',
        'concat:dev',
        'concat:app',
        'copy:fonts',
        'copy:main'
    ]);

    grunt.registerTask('prod', [
        'less:app',
        'cssmin',
        'uglify:app',
        'concat:app',
        'copy:fonts',
        'copy:main'
    ]);
};
