@extends('layouts.app')

@section('title')
    Report
@endsection

@section('content-header')
    <h1>Report({{ $reportType }})</h1>
@endsection

@section('content')
    @php
       $from = !empty(Request::get('from'))? Request::get('from') : '';
       $to = !empty(Request::get('to'))? Request::get('to') : '';
   @endphp
<div class="box">
    <div class="box-header with-border">
        <div class="container-fluid">
            <form class="" action="{{ route('reports.index', ['report' => $reportType]) }}" method="get">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group col-md-6 {{ $errors->has('from') ? 'has-error' : '' }}">
                            <div class="input-group">
                                <label class="input-group-addon" for="from">From:</label>
                                <input type="text" class="form-control dpicker-local" name="from" id="from" value="{{ old('from', !empty($from)? $from : '') }}">
                                <label for="from" class="input-group-addon" style="cursor: pointer;">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </label>
                            </div>
                            <span class="help-block">{{ $errors->first('from') }}</span>
                        </div>
                        <div class="form-group col-md-6 {{ $errors->has('to') ? 'has-error' : '' }}">
                            <div class="input-group">
                                <label class="input-group-addon" for="to">To:</label>
                                <input type="text" class="form-control dpicker-local" name="to" id="to" value="{{ old('to', !empty($to)? $to : '') }}">
                                <label for="to" class="input-group-addon" style="cursor: pointer;">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </label>
                            </div>
                            <span class="help-block">{{ $errors->first('to') }}</span>
                        </div>
                    </div>
                </div>
                @if ($reportType !== 'bookings')
                    <div class="row">
                        <div class="form-group col-md-6">
                            @php
                            $selectedExperiences = !empty(Request::get('filter_by'))? explode(',', Request::get('filter_by')) : [];
                            @endphp
                            <label for="" class="control-label col-md-2">Filter by</label>
                            <div class="col-md-10">
                                <input type="hidden" name="filter_by" id="filter_by" value="">
                                <select class="form-control select2-c" multiple="multiple" style="width: 100%;">
                                    <option value="" {{ !$selectedExperiences? 'selected' : '' }}>All</option>
                                    <option value="housing" {{ in_array('housing', $selectedExperiences)? 'selected' : '' }}>Housing</option>
                                    <option value="extra_purchases" {{ in_array('extra_purchases', $selectedExperiences)? 'selected' : '' }}>Extra purchases</option>
                                    @foreach ($experiences->data as $experience)
                                        <option value="{{ $experience->slug }}" {{ in_array($experience->slug, $selectedExperiences)? 'selected' : '' }}>{{ $experience->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group col-md-12">
                            <button type="submit" class="btn btn-primary pull-right" name="generate" value="true">Generate</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="box-body">
        <div class="panel no-margin no-border">
            @if (count($errors) === 0)
                {{-- Solo se puede exportar a PDF cuando no hay errores --}}
                <div class="panel-heading">
                    <form class="" action="{{ route('reports.export') }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="report" value="{{ json_encode($report) }}">
                        <input type="hidden" name="reportType" value="{{ $reportType }}">
                        <input type="hidden" name="from" value="{{ $from }}">
                        <input type="hidden" name="to" value="{{ $to }}">
                        @if ($reportType !== 'bookings')
                            <input type="hidden" name="filter_by" value="{{ Request::get('filter_by') }}">
                        @endif
                        <button type="submit" class="btn btn-primary pull-right">
                            <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                            Export to PDF
                        </button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            @endif
            <div class="panel-body">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            @foreach ($report->titles as $title)
                                <th>{{ $title }}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($report->data as $data)
                            <tr>
                                @foreach ($data as $d)
                                    <td>
                                        @if ($d->type_value === 'currency')
                                            {{ '$' . number_format($d->value, 2) }}
                                        @elseif ($d->type_value === 'date')
                                            {{ (new Carbon\Carbon($d->value))->format('d-m-Y') }}
                                        @else
                                            {{ $d->value }}
                                        @endif
                                    </td>
                                @endforeach
                            </tr>
                        @empty
                            <tr><td colspan="6">No data</td></tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    @parent
    <script type="text/javascript">
    window.addEventListener('load', function() {
        $select2C = $(".select2-c");
        $select2C.select2({
            tags: true,
            tokenSeparators: [',', ' ']
        });
        $select2C.on("change", function(e) {
            var values = $(this).val();
            $('#filter_by').val(values.join());
        });
    });
    </script>
@endsection
