@extends('layouts.app')

@section('title')
    Room types
@endsection

@section('content-header')
    <h1>Room Types</h1>
@endsection

@section('content')
<div class="box">
    <div class="box-header with-border">
        <a href="{{ route('room-types.create') }}" class="btn btn-primary pull-right">New room type</a>
    </div>
    <div class="box-body">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Name</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">View</th>
                    <th class="text-center">Edit</th>
                    <th class="text-center">Delete</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($roomTypes->data as $roomType)
                <tr>
                    <td>{{ $roomType->name }}</td>
                    <td class="text-center fit">
                        <form action="{{ route('room-types.activate', [ 'id' => $roomType->id]) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <button type="submit" class="action-btn table-btn-activate" style="opacity: 1;" title="{{ $roomType->is_active == 1? 'Deactivate' : 'Activate' }}">
                                <i class="fa fa-flag {{ $roomType->is_active == 1 ? 'on' : 'off' }}" aria-hidden="true"></i>
                            </button>
                        </form>
                    </td>
                    <td class="text-center fit">
                        <a class="action-btn" href="{{ route('room-types.show', ['id' => $roomType->id]) }}">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </a>
                    </td>
                    <td class="text-center fit">
                        <a class="action-btn" href="{{ route('room-types.edit', ['id' => $roomType->id]) }}">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                    </td>
                    <td class="text-center fit">
                        <form action="{{ route('room-types.destroy', [ 'id' => $roomType->id]) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="action-btn table-btn-destroy">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </button>
                        </form>
                    </td>
                </tr>
                @empty
                    <tr>
                        <td colspan="40">No data</td>
                    </tr>
                @endforelse
            </tbody>
        </table>

        {!! Component::pagination(
            10,
            $roomTypes->meta->pagination->current_page,
            $roomTypes->meta->pagination->total_pages,
            $params
            ) !!}
    </div>
</div>
<script type="text/javascript">
    window.addEventListener('load', function() {
        EXP.initConfirmButton({
            selector: 'button.table-btn-destroy',
            title: "Delete room type",
            msg: "This action is going to delete the current room type.",
            callback: function(btn) {
                $(btn).parent().submit();
            }
        });

        EXP.initConfirmButton({
            selector: 'button.table-btn-activate',
            title: "Activate/deactivate room type",
            msg: "This action is going to activate/deactivate the current room type.",
            callback: function(btn) {
                $(btn).parent().submit();
            }
        });
    });
</script>
@endsection
