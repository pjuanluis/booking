@extends('layouts.public-booking-base')

@section('content')
    <div class="container" id="app">
        <h1>Cancellation Policy <br> <small>Agency &amp; Experiencia</small></h1>
        <h2>RESERVATION DONE THROUGH AN AGENCY:</h2>
        <p>
            The cancellation has to be done through the agency because each agency applies its own policy.
        </p>
        <h2>RESERVATION DONE DIRECTLY WITH EXPERIENCIA:</h2>
        <p>
            The cancellation policy will depend on how much in advance the client informs about the cancellation. The policy applies to any news or changes regarding the use of the service: Spanish course, surfing lesson, accommodation, transportation or any other school service.
        </p>
        <p>
            - If the notice is given minimum 31 days in advance the school does not charge any cancellation fee. The school will in no case refund the deposit of 60 USD.
        </p>
        <p>
            - If the notice is given with minimum 20 days in advance, 50 % of the paid total is refunded to the client.
        </p>
        <p>
            - If the notice is given with minimum 7 days in advance, 20 % of the paid total is refunded to the client.
        </p>
        <p>
            - If the notice is given with minimum 3 days in advance, there will be no refund to the client.
        </p>
        <p>
            - Services are not transferable to another person, except in cases when the notice is done more than 45 days prior to arrival date.
        </p>
    </div>
@endsection
