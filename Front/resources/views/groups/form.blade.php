@extends('layouts.app')

@section('title')
    @if ($formType === Constant::FORM_CREATE)
        Create group
    @elseif ($formType === Constant::FORM_EDIT)
        Edit group
    @elseif ($formType === Constant::FORM_SHOW)
        View group
    @endif
@endsection

@section('content-header')
    @if ($formType === Constant::FORM_CREATE)
        <h1>Create group</h1>
    @elseif ($formType === Constant::FORM_EDIT)
        <h1>Edit group</h1>
    @elseif ($formType === Constant::FORM_SHOW)
        <h1>View group</h1>
    @endif
@endsection

@section('content')
<style type="text/css">
    .close {
        position: absolute;
        right: 10px;
        top: 5px;
        color: #ff0000;
    }
    .close:focus, .close:hover {
        color: #ff0000;
    }
    #clients-list {
        min-height: 150px;
        max-height: 500px;
        overflow-y: auto;
    }
    #client-filters {
        border-bottom: 1px solid #eee;
        padding-bottom: 10px;
        margin-bottom: 10px;
    }
    .form-control {
        padding: 6px 12px !important;
    }
</style>
<?php
function getData($field, $group, $formType) {
    if (old($field)) {
        return old($field);
    }
    if ($formType === Constant::FORM_EDIT || $formType === Constant::FORM_SHOW) {
        return $group->$field;
    }
    return '';
}
?>
<form id="group-form" action="{{ $formType === Constant::FORM_SHOW ? '#' : $formAction }}" method="POST">
    <div class="box" style="">
        {{ csrf_field() }}
        @if ($formType === Constant::FORM_EDIT)
            {{ method_field('PUT') }}
            <input type="hidden" name="updated_at" value="{{ $group->updated_at }}" />
        @endif
        <div class="box-body">
            <input id="hidden-clients" type="hidden" name="clients" />
            <input id="hidden-clientnames" type="hidden" name="client_names" />
            <input id="hidden-clientcourses" type="hidden" name="client_courses" />
            @php
            $readOnly = ($formType === Constant::FORM_SHOW) ? 'readonly' : '';
            $disabled = ($formType === Constant::FORM_SHOW) ? 'disabled' : '';
            @endphp
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        @php
                        $val = getData('name', $group, $formType);
                        @endphp
                        <label for="name" class="control-label">Name</label>
                        <input id="name" type="text" class="form-control" name="name" value="{{ $val }}" {{ $readOnly }} />
                        <span class="help-block">{{ $errors->first('name') }}</span>
                    </div>
                    <div class="form-group {{ $errors->has('level') ? 'has-error' : '' }}">
                        @php
                        $val = getData('level', $group, $formType);
                        @endphp
                        <label for="level" class="control-label">Level</label>
                        <input id="level" type="text" class="form-control" name="level" value="{{ $val }}" {{ $readOnly }} />
                        <span class="help-block">{{ $errors->first('level') }}</span>
                    </div>
                    <div class="form-group {{ $errors->has('clients') ? 'has-error' : '' }}">
                        <label class="control-label">Students</label>
                        <div id="selected-clients" class="list-group">
                            <?php
                            $clients = getData('clients', $group, $formType);
                            $clientNames = [];
                            $clientCourses = [];
                            if (old('clients') != null) {
                                $clientNames = explode('|', getData('client_names', $group, $formType));
                                $clientCourses = explode('|', getData('client_courses', $group, $formType));
                                $clients = explode(',', $clients);
                            }
                            else if ($formType !== Constant::FORM_CREATE) {
                                $clients = $clients->data;
                            }
                            else {
                                $clients = [];
                            }
                            $id = $name = $courses = $experiences = null;
                            foreach ($clients as $i => $c) {
                                $id = $c;
                                if (old('clients') == null && $formType !== Constant::FORM_CREATE) {
                                    $id = $c->id;
                                    $name = $c->full_name;
                                    $courses = [];
                                    foreach ($c->commands->data as $cc) {
                                        foreach ($cc->commandCourses->data as $ccc) {
                                            $experiences = [];
                                            foreach ($ccc->package->data->experiences->data as $ex) {
                                                $experiences[] = $ex->name;
                                            }
                                            $courses[] = implode('& ', $experiences) . ' - ' . $ccc->package->data->name;
                                        }
                                    }
                                    $courses = implode(', ', $courses);
                                }
                                else {
                                    $name = $clientNames[$i];
                                    $courses = $clientCourses[$i];
                                }
                                ?>
                                <a href="#" class="list-group-item" data-id="{{ $id }}">
                                    <h4 class="list-group-item-heading">{{ $name }}</h4>
                                    <p class="list-group-item-text">{{ $courses }}</p>
                                    @if ($formType !== Constant::FORM_SHOW)
                                    <button type="button" class="close pull-right" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    @endif
                                </a>
                                <?php
                            }
                            ?>
                        </div>
                        <span class="help-block">{{ $errors->first('clients') }}</span>
                    </div>
                </div>
                @if ($formType !== Constant::FORM_SHOW)
                <div class="col-sm-6" style="border-left: 5px dashed #eee;">
                    <div class="panel panel-default" style="margin-bottom: 0;">
                        <div class="panel-heading">
                            <h3 class="panel-title">Listing of students</h3>
                        </div>
                        <div class="panel-body">
                            <div id="client-filters">
                                <div class="form-group">
                                    <label for="filter-name" class="control-label">Experiences</label><br />
                                    <label class="checkbox-inline" style="padding-left: 0;">
                                        <input type="checkbox" id="check-spanish" class="iCheck" value="1" data-experience="spanish"> Spanish
                                    </label>
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="check-surf" class="iCheck" value="1" data-experience="surf"> Surf
                                    </label>
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="check-volunteer" class="iCheck" value="1" data-experience="volunteer"> Volunteer
                                    </label>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <label for="filter-name">Name</label>
                                        <input type="text" class="form-control" id="filter-name" placeholder="Jane Doe">
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="filter-date">Courses begin date</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control dpicker-local" id="filter-date" placeholder="{{ \Carbon\Carbon::now()->format('d-m-Y') }}">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <label for="filter-country">Country</label>
                                        <select class="form-control" id="filter-country" name="country_id">
                                            <option value=""></option>
                                            @foreach ($countries as $country)
                                            <option value="{{ $country->id }}">{{ $country->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="filter-agency">Agency</label>
                                        <select class="form-control" id="filter-agency" name="agency_id">
                                            <option value=""></option>
                                            @foreach ($agencies as $agency)
                                            <option value="{{ $agency->id }}">{{ $agency->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <button id="btn-search-clients" type="button" class="btn btn-default pull-right"><i class="fa fa-search"></i> Search</button>
                                <div class="clearfix"></div>
                            </div>
                            <div id="clients-list" class="list-group"></div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
    <a class="btn btn-default" href="{{ url()->previous() }}">
        @if ($formType !== Constant::FORM_SHOW)
            Cancel
        @else
            Back
        @endif
    </a>
    @if ($formType !== Constant::FORM_SHOW)
        <button id="btn-save-group" type="submit" class="btn btn-primary">Save</button>
    @else
        <a class="btn btn-primary" href="{{ route('groups.edit', ['id' => $group->id]) }}">
            Edit
        </a>
    @endif
</form>

<script type="text/javascript">
    window.addEventListener('load', function() {
        @if ($formType !== Constant::FORM_SHOW)
        $('#selected-clients .list-group-item button.close').click(function(evt) {
            evt.preventDefault();
            $(this).closest('.list-group-item').remove();
        });
        var typingTimer;
        var doneTypingInterval = 500;
        //on keyup, start the countdown
        $('#filter-name').keyup(function(){
            clearTimeout(typingTimer);
            if ($('#filter-name').val()) {
                typingTimer = setTimeout(searchClients, doneTypingInterval);
            }
        });
        $('#btn-search-clients').click(function(evt) {
            evt.preventDefault();
            searchClients();
        });
        $('#filter-country').select2({
            placeholder: "Select country",
            allowClear: true
        });
        $('#filter-agency').select2({
            placeholder: "Select agency",
            allowClear: true
        });
        @endif
        var searchClients = function() {
            // Mostrar loading
            $('#clients-list').ploading({
                action: 'show',
                spinnerHTML: '<i></i>',
                spinnerClass: 'fa fa-spinner fa-spin p-loading-fontawesome'
            });
            var name = $('#filter-name').val();
            var countryId = $('#filter-country').val();
            var agencyId = $('#filter-agency').val();
            var begin = $('#filter-date').val() === ''? $('#filter-date').val() : moment($('#filter-date').val(), 'DD-MM-YYYY').format('YYYY-MM-DD');
            var experiences = [];
            $('#client-filters input[type=checkbox]').each(function(i, checkbox) {
                if (checkbox.checked) {
                    experiences.push($(checkbox).data('experience'));
                }
            });
            // Buscar clientes
            EXP.request.get('/admin/clients?include=commands.commandCourses.package.experiences,commands.commandCourses:from_date(' + begin + ')' +
                    '&name=' + name + '&country_id=' + countryId + '&agency_id=' + agencyId + '&course_from=' + begin + '&experiences=' + experiences.join(',') + '&no-paginate=1', function(resp) {
                var list = $('#clients-list');
                list.html('');
                // Mostrar listado de clientes
                _.each(resp.data, function(r) {
                    var item = $('<a href="#" class="list-group-item" data-id="' + r.id + '"></a>');
                    item.append('<h4 class="list-group-item-heading">' + r.full_name + '</h4>');
                    var details = $('<p class="list-group-item-text"></p>');
                    var packs = [];
                    _.each(r.commands.data, function(c) {
                        _.each(c.commandCourses.data, function(cc) {
                            var experiences = _.map(cc.package.data.experiences.data, function(e) {
                                return e.name;
                            });
                            packs.push(experiences.join('& ') + ' - ' + cc.package.data.name);
                        });
                    });
                    details.html(packs.join(', '));
                    item.append(details);
                    list.append(item);
                });
                if (_.isEmpty(resp.data)) {
                    list.html('<span>No data available</span>');
                }
                initListItemsEvents();
                $('#clients-list').ploading({action: 'hide'});
            });
        };
        var initListItemsEvents = function() {
            $('#clients-list a.list-group-item').click(function(evt) {
                evt.preventDefault();
                var clientId = $(this).data('id');
                // Verificar que el cliente no haya sido añadido previamente a la lista
                var ids = getSelectedClientIds();
                for (var i = 0; i < ids.length; i++) {
                    if (ids[i] == clientId) {
                        return;
                    }
                }
                var item = $('<a href="#" class="list-group-item" data-id="' + clientId + '"></a>');
                item.html($(this).html());
                var closeBtn = $('<button type="button" class="close pull-right" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
                closeBtn.click(function(evt) {
                    evt.preventDefault();
                    item.remove();
                });
                item.append(closeBtn);
                $('#selected-clients').append(item);
            });
        };
        var getSelectedClientIds = function() {
            var items = $('#selected-clients a');
            var ids = _.map(items, function(item) {
                return $(item).data('id');
            });
            return ids;
        };
        $('#group-form').submit(function() {
            $('.content-wrapper').ploading({
                action: 'show',
                spinnerHTML: '<i></i>',
                spinnerClass: 'fa fa-spinner fa-spin p-loading-fontawesome'
            });
            $('#hidden-clients').val(getSelectedClientIds().join(','));
            var items = $('#selected-clients a');
            var temp = _.map(items, function(item) {
                return $(item).find('h4').text();
            });
            $('#hidden-clientnames').val(temp.join('|'));
            var temp = _.map(items, function(item) {
                return $(item).find('p').text();
            });
            $('#hidden-clientcourses').val(temp.join('|'));
        });
    });
</script>

@endsection
