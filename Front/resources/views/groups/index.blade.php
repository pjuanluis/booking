@extends('layouts.app')

@section('title')
    Groups
@endsection

@section('content-header')
    <h1>Groups</h1>
@endsection

@section('content')

<div class="box" style="">
    <div class="box-header with-border">
        @if (false)
            <a href="{{ route('groups.create') }}" class="btn btn-primary pull-right">New group</a>
        @endif
    </div>
    <div class="box-body table-responsive no-padding">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Theme</th>
                    <th>Level</th>
                    <th>Experience</th>
                    <th># Students</th>
                    <th class="text-center fit">View</th>
                    <th class="text-center fit">Edit</th>
                    @if (Auth::user()->authenticatable_type === null)
                        <th class="text-center fit">Delete</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                @php
                    // dd($groups->data);
                @endphp
                @foreach ($groups->data as $group)
                <tr>
                    @php
                        $theme = '';
                        $experiences = '';
                        foreach ($group->schedules->data as $schedule) {
                            $theme .= $schedule->subject . ',';
                            if ($schedule->experiences->data) {
                                $experiences .= implode(',', array_column($schedule->experiences->data, 'name')) . ',';
                            }
                        }
                    @endphp
                    <td>{{ $group->name }}</td>
                    <td>{{ substr($theme, 0, strlen($theme) - 1) }}</td>
                    <td>{{ $group->level }}</td>
                    <td>{{ substr($experiences, 0, strlen($experiences) - 1) }}</td>
                    <td>{{ $group->number_students }}</td>
                    <td class="text-center">
                        <a href="{{ route('groups.show', ['id' => $group->id]) }}" class="action-btn">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a href="{{ route('groups.edit', ['id' => $group->id]) }}" class="action-btn">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                    </td>
                    @if (Auth::user()->authenticatable_type === null)
                        <td class="text-center">
                            <form action="{{ route('groups.destroy', [ 'id' => $group->id]) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="table-btn-destroy action-btn">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                            </form>
                        </td>
                    @endif
                </tr>
                @endforeach
            </tbody>
        </table>
        {!! Component::pagination(
            10,
            $groups->meta->pagination->current_page,
            $groups->meta->pagination->total_pages,
            $params
        ) !!}
    </div>
</div>
<script type="text/javascript">
    window.addEventListener('load', function() {
        EXP.initConfirmButton({
            selector: 'button.table-btn-destroy',
            title: "Delete client",
            msg: "This action is going to delete the current client.",
            callback: function(btn) {
                $(btn).parent().submit();
            }
        });
    });
</script>
@endsection
