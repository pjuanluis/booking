<!-- START LEFT SIDEBAR NAV-->
@php
    $user = Auth::user();
    $authenticatableType = $user->authenticatable_type;
    $accessToClientsAndAgencies = ['App\Agency', 'App\Client']
@endphp
<ul class="sidebar-menu" data-widget="tree" data-current-url="{{ url()->current() }}">
    @empty ($authenticatableType)
        <li class="">
            <a href="{{ route('dashboard.index') }}"><i class="fa fa-tachometer"></i> <span>Dashboard</span></a>
        </li>
    @endempty
    @if (empty($authenticatableType) || in_array($authenticatableType, $accessToClientsAndAgencies))
        <li class="treeview">
            <a href="#">
                <i class="fa fa-address-card"></i> <span>Commands</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li class="">
                    <a href="{{ route('bookings.index') }}?command-type=booking">
                        <span>New Bookings</span>
                    </a>
                </li>
                <li class="">
                    <a href="{{ route('bookings.index') }}?command-type=confirmed">
                        <span>Confirmed Bookings</span>
                    </a>
                </li>
                {{--  <li class=""> TODO: delete this commented.
                    <a href="{{ route('bookings.news') }}">
                        <span>New bookings</span>
                        <span class="pull-right-container">
                            <span id="lbl-newbookingscount" class="label label-primary pull-right"></span>
                        </span>
                    </a>
                </li>
                <li class="">
                    <a href="{{ route('bookings.index') }}">
                        <span>All bookings</span>
                        <span class="pull-right-container">
                            <span id="lbl-allbookingscount" class="label label-primary pull-right"></span>
                        </span>
                    </a>
                </li>  --}}
                @if (empty($authenticatableType))
                    <li class="">
                        <a href="{{ route('import.index') }}"><span>Import bookings</span></a>
                    </li>
                @endif
            </ul>
        </li>
    @endif
    @if (empty($authenticatableType) || in_array($authenticatableType, $accessToClientsAndAgencies))
        <li class="">
            <a href="{{ route('clients.index') }}"><i class="fa fa-address-book"></i> <span>Clients</span></a>
        </li>
    @endif
    @empty ($authenticatableType)
        <li>
           <a href="{{ route('accommodations.scheduler') }}">
                <i class="fa fa-home"></i>
               <span>Housing</span>
            </a>
        </li>
        <li class="">
            <a href="{{ route('groups.index') }}"><i class="fa fa-group"></i> <span>Groups</span></a>
        </li>
        <li><a href="{{ route('schedules.index') }}"><i class="fa fa-calendar"></i> Schedules</a></li>
        <li>
            <a href="{{ route('petty-cashes.index') }}?show=consumption-centers">
                <i class="fa fa-money"></i>
                Petty cash
            </a>
        </li>
    @endempty
    @if (empty($authenticatableType) || $authenticatableType === 'App\Agency')
        <li class="">
            <a href="{{ route('agencies.index') }}">
                <i class="fa fa-building"></i>
                <span>Agencies</span>
            </a>
        </li>
    @endif
    @empty ($authenticatableType)
        <li class="treeview">
            <a href="#">
                <i class="fa fa-bar-chart"></i> <span>Reports</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li class="">
                    <a href="{{ route('reports.index', ['report' => 'sales']) }}">Sales</a>
                </li>
                <li class="">
                    <a href="{{ route('reports.index', ['report' => 'bookings']) }}">Bookings</a>
                </li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-folder"></i> <span>Catalogues</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li class="">
                    <a href="{{ route('room-types.index') }}">Room types</a>
                </li>
                <li class="">
                    <a href="{{ route('housing.index') }}">Housing types</a>
                </li>
                <li class="">
                    <a href="{{ route('courses.index') }}">Courses</a>
                </li>
                <li class="">
                    <a href="{{ route('countries.index') }}">Countries</a>
                </li>
                <li class="">
                    <a href="{{ route('consumption-centers.index') }}">Consumption centers</a>
                </li>
                <li class="">
                    <a href="{{ route('people.index') }}">People</a>
                </li>
                <li class="">
                    <a href="{{ route('places.index') }}">Places</a>
                </li>
                <li class="">
                    <a href="{{ route('residences.index') }}">Residences</a>
                </li>
                <li class="">
                    <a href="{{ route('currencies.index') }}">Currencies</a>
                </li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-folder"></i> <span>System</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li class="">
                    <a href="{{ route('roles.index') }}">Roles</a>
                </li>
                <li class="">
                    <a href="{{ route('users.index') }}">Users</a>
                </li>
            </ul>
        </li>
    @endempty
</ul>

@section('scripts')
@parent
<script type="text/javascript">
    window.addEventListener('load', function() {
        @php
        $month = \Carbon\Carbon::now()->format('m');
        @endphp
        EXP.request.get("{{ route('bookings.count', ['month' => $month]) }}", function(response) {
            $('#lbl-newbookingscount').text(response.count);
        });
        EXP.request.get("{{ route('bookings.count') }}", function(response) {
            $('#lbl-allbookingscount').text(response.count);
        });
    });
</script>
@endsection
<!-- END LEFT SIDEBAR NAV-->
