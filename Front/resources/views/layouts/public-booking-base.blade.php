@extends('layouts.skeleton')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/public-app.css') }}?v={{ config('app.version') }}">
@endsection

@section('container')
    <div class="header">
        <div class="slick">
            <img src="{{ asset('img/public/booking/foto_header_1.jpg') }}" alt="Experiencia">
            <img src="{{ asset('img/public/booking/foto_header_2.jpg') }}" alt="Experiencia">
            <img src="{{ asset('img/public/booking/foto_header_3.jpg') }}" alt="Experiencia">
            <img src="{{ asset('img/public/booking/foto_header_4.jpg') }}" alt="Experiencia">
        </div>
        <div class="tape">
            <a href="https://www.experienciamexico.mx/contact/" target="_blank" style="color: white;">
                CONTACT US
            </a>
        </div>
    </div>

    @yield('content')

    <div class="footer text-center text-left-sm">
        Developed by @include('layouts.logo-cl')
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/public-app.js') }}?v={{ config('app.version') }}"></script>
@endsection
