@extends('layouts.skeleton')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/app.min.css') }}?v={{ config('app.version') }}">
<style>
        .text-color-black {
            color: #535459 !important;
        }
        .table-vertical-align-middle td { 
            vertical-align: middle !important;
        }
</style>
@endsection

@section('container')
    @if (!Auth::guest())
    <div class="wrapper">
        <header class="main-header">
            @if (in_array(config('app.env'), ['local', 'development']))
            <div class="dev-badge" style="{{ in_array(Request::route()->getName(), ['login', 'password.reset', 'password.request']) ? 'text-align: center;' : '' }}">
                <span>Usted está navegando el sistema de desarrollo. <a href="https://booking.experienciamexico.mx/">Haga clic aquí</a> para ir al sistema de producción.</span>
            </div>
            @endif
            <nav class="navbar navbar-static-top">
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <div class="navbar-header pull-left">
                    <span class="navbar-brand">
                       @yield('section-title')
                    </span>
                </div>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu">
                            <a href="#!" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <span>Hi {{ Auth::user()->__get('first_name') }}</span>
                                <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="user-footer">
                                    <div class="pull-right">
                                        <a href="{{ url('/logout') }}" class="btn btn-default btn-flat"
                                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            Sign out
                                        </a>
                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <aside class="main-sidebar">
            <span class="logo">
                <img class="center-block" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAK0AAABWCAYAAAC9+haZAAAABGdBTUEAALGPC/xhBQAAI1NJREFUeAHtnQl4nlWVx0+atE3bdE33fUnpCm3ZhILsKCCLoCiLCjiKiuP6zKPzqOOojDqODriAOiiyCqKCoOxYBNkqFLo33Zd0b9N0b9a28/+dNzd5+zX58jaJtKHvafNt713PPffcc84999ycioqK/ZZCioE2hIF2baitaVNTDDgGUqJNCaHNYSAl2jY3ZGmDU6JNaaDNYSAl2jY3ZGmDU6JNaaDNYSAl2jY3ZGmDU6JNaaDNYSAl2jY3ZGmD81IUtC4GcnLMcngR7N8fbTbWvrVuRUdxaSnRtnDw27XLsfZ5WrAgVFHnnooaq67ea/v0Ob9jruXltrMOHXL9WXXNPtu3L901byHKLSXaZmAwcNPc3BzbvKXCZi8ss5J1u2xDabnt2FVl5ZV7be/e/da1oL0T9NABXWz4oAKbeExP69e7sxNuSrzNQHxtlpzUYSY58tqJWiHU3eU1tmTldnvihdU2Z1GZbd9ZbZVVew2u26F9O3+nVAizqjrirnDjnt072MnH9bWLzhxsRUO7WXulhfumcGgYSIk2Ib4gsApx0OkzN9nDz6y0N+Zttp7dOtqIwV3t2DG9beyoHjZ6WHdx0k6Wh7gg2C+iXbdpty0r2SFuvMXmLykTse+Q6LDPLjl7qL3/vGE2SsRbs3cfkkUKCTGQEm0TiEKpyhUHnSWie/Avy+21WRutW5cOdvE5w+2MkwfYMcO7W0dk1gSA2LBgcZn95fmV9tTfS2xA3852zSVFduk5Q52DpyJDAiQqSUq0WfDEko5i9bsnlttD+tsjseC80wbbDR8c5zJqlqxZH1VJlHj8hRK799FFkonL7borjrFrLh5pHdvnugKXNXP6MCXaxmgAgl0l5erXf1hkz7+6zsZo+f/QhUV2wRlD6mTWxvIm/X35mh32v7+eZW/M3WxXvW+UffaacZYjySIVFbJjMOW0GfjBMoCZ6s35pfbDX8+1tRv32NUXF2kZH229enTMSN3yr2XbK+2bP37DXp+90W66drx97PIimcxS5SwbZlOijWEH6wD6EFaBXzywQNp9rn3mmgn2vrOGuRk2lrRVP24qK7cv3PyKler9R//+LjeN1aRWhUZxnG7j1qIGU1alNgV++WCx/ejXs21Qvy72k2+cZhef/c8lWKrv26uTfemGSYase8+fFrv5DI6fQsMYSDcXhJe8vBzbtafG7np4iSwEy+yECb3tKzdOseEyZ2UDtH2W97dk/ppZXCrz1h7bvafaLQFD+hfY6ScOsFMm9/OdsWzl8OykY/u4ReKRZ5fbrOIy2XN7+wZFU/mOxudHPdGicG0V4d1y1zyb9upaO+tdA+1rnznBunftkJUeFq3Ybo9PW2kvvL5Ou2CV0vpRoHiJsi1YutWee2W1TR7X2/7tE5NtyICCrOXBWc85dZA9+WKJvfj6ejt1cp+UaBvB2FEt07YXhy1Zv8f++47ZNrt4i507dbB9VRy2a5f2jaDL3AR29yOL7Mm/rbIt2yq0A5abVd5lR2zMiO72rc+fZMMGZefcKGA3fesl7bBV2G3/eZr16tbB9jIbUjgAA0etTItIsHLNLrv59pk2c8EWu/KiIvvGTSdkJdgNm/fYN2993X776GIRVpVvKjQle7KtC1e+6+FFTZqy2HU7RgS+dXuVrVq7s25n7YARS7/YUUe0EBnEsWDpdvv6rW/aYhHU9VeMsc9eOyGr7LlOBPvdX7xlr7y1wfOjuCWFPKWduaBUk2Rnk1kQJ9jQ2FRaEXmONZnj6EtwVMm0EBpi5xN/W213/G6hlUtb/8L1x9oHLxiVdeQhuB/fPUd+A9sSb9nGC2QreMfuKtl+N9uIIdlFBBQ40uMxhtmLSXYomw2Y7cjjPr161w50HSBoeFl64b1ODq9L0TY+HBVEyyAie7K8PyD/gYefXaGNgnz7tuTM007on3Wk1m7cbT+9d64tWr5VnLj56EI2raiqyVoXDwf26+zujLtkhdgrRxra3hTAyXNrnXR27a62nfpjyxkPst2yigSgrM6d8twTrXN+nnUr6GCdO0d92qu0NXKnbAvQ/FFoA71jkHLbtbNyLbdsGPzx6ZW2WNzyojOH2fUfGOseWtm6sWL1TvuvnyNCbGsRwVIHHK9DXtOONTiOJyHUqG85zi0Xy3NswZJtxrYwHHrL1krbLotGZdU+t4xQP5wV18kesork5+dadxFs75758krLd08zvNRGahVgN3CfiNf/HaE0/I4jWhgTXAciwe91oTjkvTLYvzlvi2/Dfk3K1gWnD7FO4jTZYL1cCr//yzdt/tIyy0/oxdVYeRBA5w7tbWQTdl/y4zyeDeCqEGypCJOt5mdfXudK2zYphnBVJ2alCWJCpgcaHBxOvFHEXbxsmxNzQZc8J+LB/bvYqVP62SmT+lj/Pp1cxof7HmneZ9lHLhv2jrBn4dgLCF4vMWDe4m327Ctr7VUpTthc8c667vIxNlL+q00BCtMtv5EZTO6ITRF3U2XxnDYN0umFiWMKm0y+YXO5+x5w6gG/XEQEAGLEprxJJyVem7nRfXpx6HEZVc8g0k7ioE0BeALEz1VelLqmZr9t3lphm8sqtEmyxe57rL29+6T+dpb+jh3Tywo6t/dduij14X897EQL0uFEeVrGhcf6pdF/PxhBjvII79GAwZj0fc2G3eI8W2yOCG2xThWskjmLgb/8/JG+0zS+qMfBhTXwCwSB0rVZfgAjBnezjXIdDAcUG0je6E/eL71AsD3kLP6pq8ZntU6EgjaU7vH+9CvspJ21dq6MoUBiw33ihVX26HOrbKmcyiE+/HwPFUK7nJ/X4o5SvKTa4uDEj6mep19cY5PGFdol8vc97fi+zarvUNuXJP1hJVq4w9rNu91fFaIdqnNUwwYWWC8dS+EwIEuhI7e2J+CU5Yo9erdlrt9tKyTHzdWRl/Wbyp0bdJRM2F/nsG744Fi5EQ51H4Ik5ink3sefX2W/+kOxH0z89NXjxK232hopYrQjKUCkbChgViuUV9joYT3so+Lwx4ljJQEsFJ2k8JEXgLsuWbXD7vzjIpsxp1TK3F5XpJKUFdJAqJyOwBqBuIAC1kUKGErZgD6drbB7R+st/4fMXnI4c5MmLRN5zfpd9oH3DnfZvjmTOLSlNd4PK9GClCFaNq+V9/4vf1dsj9670jkTyhNugF2EVNLs0wrJO0jFhok8B3HAakF2HykUJ0zsI0Wimx0vv4HJ4/tosDOHoHF0LSvZbvc8stieenGV71rd+OGJdt7UQdpO3RDVk5BosRCglZ90XKET6dTj+1uRjuCwtCcB+jhXJxsCMSEaPP/aOrvjoUV+bMdP9moyJAUV54TKRgpKFgxhtE5asIHBQcu+vfK9qDhjaLDsCNU+BoebYGnfYSVaGoA8NaBvJ+1GTbGzTh4gL6cltlK7QchXZVr+CkWQg0XYwwZ2dblt715zjbenuAPHVXp1z3clorBnRxFHQuqgYgF1PPzscvvLtBWuZbP3/8kPjbEiDexWbdFiNkq6AmNegii+/PHJvqTCIQ8VNkqeXSauCkExaW+7b76O5pSIu4pzJywPomLyQOCUcdZJctqZ0tdQssL5tX2aDKRpKyauTDwedqKlQWjMeaKOc04ZaJPG9tL5qTVyNllryyW7sV0KFz1bzwb2LXCtFrGiucDhxDUbdum0wCb77WNLXGYdJ3PPxz8wRqawIVrWc6xGLooMalKuAkfDhosZ7UR5azUXZslTDN/as9/V3/7nV3NsmrgsoklS8YSJA3FzVP380wbZmVKk8KPgt2jF2meVlW3fwfyIIFoG2ZEq4u0hJ5GPvb/ILjxjsGSpTdo23Wh/m77WnpQSMlTcltOriAHHjOghjtTVuUemWSdONBAUci+KWvGyrTZfyy8eWHhmMRlu+OBoO+PE/m6zRBZlAh2qgkPbx43qKZFicLzqQ/rM5Pz9k8ucs0+bvt7K3BknGbemj8is44t66pTFSDtR/cKMhQzLs3faMfUjhmjDCINkiIel7bLzh9mZEhlWi+Bw13tjbqlN12nYV97c4MpIvuTHzjLz9OiWL+ULbTvapqUstOtNskWWypSzc0+Va9+U21WDeebJ/e10caHjRve0Qsl1DC7PWgKTx/f2OptbxjMvrfZYCsifeI8lmThOrGp7b/XhA+8ZYRecOcgdyiHSd/KRnSOOaMOgw/H2SoBFyx03qruNlfLwiSv32xoZ/YuXbrMlcnSBmJE7d2pfHxk0U/3tJEsCnHvU0AKX6eBEx43pqTLbe1LNDy2XEpIzQJKBK1R4aJEmCVRJpGguLFy+zR58fImIPiohKcGijJ4wsbfdeNVYxxEKK0FD3ulwxBJtQLxbCWq/IMoOkzP1KO0s5ch2uFdK3K7yamO/vUrKSlzUhdjQ5DGMdxF3BaKJ0HQ8LWRZ9uQL5Z+QBJCwZ8zZZHtli01CcPEyl8py8Z3bZrhdGOUpCWBV6KYNkw9dOMouO3eoTFjt33EiQDY8HPFEm9l4t4PCCgUQqROmTGMHUGxtJohPwVwa5Ka1SRp9IzoMW5lJiJB2rJbN+O8SYVAYk8I/dAL39vvn+zZsUusAYkzRsG72xesn2mQpreDjnSazNoW/Nke08Q4h00GYkTSadCGPl9D4ZzEzt7X+WRsOLLvZAFPbbnH83/yx2EYN62pDB2R3PyRY3dN/L7HH/rrSA9YlIVj6iux96uS+9mnFRxg5pOCoI9YwBkf1cZuAhIbe4Z47d1Xbjd942UolLyexAaPBD+zbRVvHI+zkSf3czTCUvW1HpS2Ug8pLM9b7VjMujxBr8AUI6Rp6DyLS+acPss99ZMJRJw5k4iQl2kyMxL7Du39273x75LmVHrIo9qjRj24ykxUDJY6dvQCYxVjG2YJmAiTZWiYvBMuW8GXnDrN/ufIY34aljqMZ2rR48M8eODjhe04f7EZ+3ByTyLeBGJE99yNQBxDn1v9DOvcFceZ3bGc3XTPezX+IB0c7wYLOelYQkJu+12EALR1z22nH9/Pt5roHCT8gYtT9JcwTkrG9jUvlF3Uc6BJZCCBW5NoUUqLNSgMQCUs8weFGS2MPvq1ZM7XCQ04cEPf2qzdOkqfaYBcRglzbCsW3+SJSmTbBECJTzlBkw5tvn2XbdlQllkcTFH1AEuReJAo2DL50w0RZIbq0eKfugAreIV9Sok04kMi3r87aZD+5e57bZPHbRUZtDYCjo6Rx8uD95w23j15W5A7syLApHIyBlGgPxkmjv2ARmLtoq/3m4cX2ls5nAUlMVo0WqAcs+/yNHan4txeNcF8LvNjguik0jIGUaBvGS6O/wnEJVve36evsoSeXe9A5uKSbsaTWNmXPhUBxe4QwKWuIfHA/dOFwv0CkjxxfjgbfgUaRm/BBSrQJERVPBnfFKsAJ2NdmbnYC5jhK6bZK94PI0XP9ryNgCDVinNF5MY5uT5Dzzqk6d3XSsb2js2NirBBzCk1jICXapnHUaAoIFycXiG2tPM7wOtuog4mbykS8OqpdqndcDTkOxBGaQQrE0VcHFoeKu/YrzHexoK2eHmgUKW/Dg5RoWwHJEC8nZ+s2FmSyQoliqUdcQGnj9AGRxWG5bPemmwTNR3xKtM3HXaM5IWJAQoS/SziI3tPV3/HQ0pd0G7elGGwgf1D8A7E2kCT9qQUYSLdxW4C8NOvhwUBKtIcH72mtLcBAq4oHrpC4OahWqMtoWLBR8jOKSdymGX/Gc+yYQbHhO+Jg2CHKfMbzOJCW8vjLBpiu3HzVQCJy4mvAUu/9kqLVcK+izGwGRNEGDy6sofxYHDLbF+FP9TRSUUN5qI0+4IFGW8OmBLhznCZ0Y6TOcNwnmN68GXoJSiNlgvsA1IdSmQkNleXtU8KA08w8h/K91RQxEAdlETG7XG58maBHfsiwt07ZYuZZr8AUrl3rd55xPh/jOkBZXEG/UdGwa4v1QcFUxI2Gu3YrUrbC9dSjz7P5C2Wxc0UsLA5FNnbKFsP+Djl5E7ADxMfLogysAQMVRISyOH9GUDvaHU9HhZ5WjcQji/Y7ocQnizKE/Aw++XlMWg5dBsKFIJiU4IXYDPF6qIMJTqikHl3be37qBghSx+FOApysXLvLtso3Al+JYQrOMXJoVz+dS/8gsMaAMqh7lfITBooxJD0xfafqwpL+Cp1E24m0iItmGBNwQ4SgeNmBCazw9uz0MaQzXDs1Wqc6CBpNU0K/G2tTtt9bhdPS0C0yrN/36BIPAoeNMhM4ssKt2zd+eIyIpcJ+dOc8W7F2hxMjbniXnDNEp23HeDZMQ9yD8L1fzpF9M9eRAlHf+b3TrWNurs1etMVvU2zIvxUEEgdhmKK0XCBP/7NPGXAAUuECcIvX5EfwwOPLZF/d4/v+/B4ATtOza0e7+UsneHhObm28+eez/Di6T86QUO/UB4fqofhjkxSv6yptxRLJJdhfc1UwQey+9bOZ7myDX/ieckUgv04+svIzqKyM/HS5/+E+BQ+Zr5PG5RWYyuoroQ7qveGK0XbFe5Sn9sQtxEa+Ox5a6GE/CRxHOo4g8YzQopedN9QuPmuo9zlw4fqSowlOnQ8KF3O0RU2wELd6QFk5CgR99Xjd3TtaOKq2O7V9/epbm9zHFyIGx7d/c6pPesoErxw7uu/RZfasbgri3l8mG+1nemOnJorQNZcW6VJsTb745CZJQmgx0cIh4Fi36kqjF9/YIKRFCM6snwaWa4CYdXSCY9/sKEF4zPLMc/pwyK07K62qJs/TxxFeLSIPeTPr4Tvlc2U9AeSYTFdeOKIOQdT3ioKAcIUo3BqOewBbU36IlgGgzRAPdcP5qTOTaL1+1cdgc6x9/pIy+8onJ1mRgoqEpZP8Oz1/peeHW8F9AfBXokOR379jjuelfeyoxYH+UC8ui6GtTBS46/f+b7bybfV+EJA5AHkItnzb/cVCSI5dKp9cZ3Ehgd7p+zRtR9+uNOCrvYI+h1i85Kfv8eg2EOQ2jUl+Va5z3l6aJAFoH89/et8Ce+altf5zZhAVwpg+8Phy3b1WZV/WwUyCOzeHcDViLQOW0VcVVe8lD6AR7ac7B1SnIcb4XzggCBdgqWNGk9YH6sBxcoSFZ+E9tBRkht/Ce8jOeINoEMYkuVvcf/bCMn2PurpHXOyeR5YodleFb58GOQ3OEW8rnxm4AAxKqIt3l+0YWCWgDJZKluXiZdsVTG+h74jFZfbM/PQBoBwilBP1hjaDG8rnNHBme8IAUy6T/rb7ij0wMvkoHyANk4Ty6TNlEHGR2AohDeloK0zm1rvmu4hEWCf6AcCpmbjx/vN7Zh9CeaEv9z+2zAmW9gdipy2h3fSNifLsy2sUO3hNmH8UfUjQIk4LwncJeTPmlaqTzNyo03TiOB1vnqLYprn6jc6DiPGKmUWsAjrUU8spvwXWQc5oKeG3aJb7h9oXTxr/IfaZcgj+RgBkOCtRZWgDyNsuGe8ZIWnsyO4ehRGxgxBJ7WrPb5GX+LFTdbNi3z75dW2F4xDqPSA8Vp0TBeIDsh79Z/s2DCD9JfQo4ZwuVGww7jJoHNqJYPYo+MjWuiT0k4GdqtMSHBUPAH4mKEYXYkcHEeOfn19vbyn2V1CeyAdewQMTk+1k2sTfdq2Ef3hqpe4ym+L9IQ+HLG+/f4FipVW6/E49ECo4K+zRyZkKd1QkAfKsU6jVv2sSgM8wWWkTYVf12GV1xiT6y7HfP71SFw0OkC4T6CBJTVGaFhEtRSCPEtuVhgFOBBrQ/7hpssfHUrtjyxIzOIrd2ktRD/0kgGY8y2Hx8h3OGbt3JUxRjS0Qx4rP1mGDCyLuRnkZgLjwhesm6GhMD3dY+YmWqJdnIKpEnP8tBVuGkAu6dPUlLE5G5VJ6blD4pY9dNtoHrb7oiNM0RLQoSlPP7Guf/9ixEmtEEM+stN/+eZnnZyIjWy5XNPEwePVlHvyJyDTVNfUyLCIFoTi//ulJznkZ+ADglknQLifXOeceeZvh0wA3K+jU3r50/QQ54fR3QvyBxA3C2zMBGBpi3HLNU59eHSUl7Lc/KWjyasnznEED6Oc4uUd++OIRNmWMQjxJ0qCf1E9AvmxAP0vkMMTkZcUBWLk+etkonUwe7qvH/Y8ttYeeWuFjCkfnSlbkcZhbvI/Z6gnPWkE80NIorgRCAV6r1WACqrEEdRDS6Ah/iBIkY9maPK5XHcKZqQsUHI6Tr48+t8J+pXisf5UgHxAAQk8X58H5pDHAgbqzBg6HlHNPHViHCEeQNGLkSKBjh7wDliWIjLYymeBgoa3ZbmKkDyx1KIkFnRUaX7FssQSwpAN7laCmOvrsP2R5cXGgFi8kgxMRqqlCoo3jrxZ3Ef4isWRPRbWvKIG7I3aNEaGfo37nK+rjYGnoJx/XxwnEh0VlEox5o+Ru7qNAtuS+Bem0DhDYUEXu+fbnpth5pwySA3qer0pE2OmtEKq13YoSZ76qbDg0cnkA6iQK+8WKAsQtQoRlJYpjFG84SgWTg3D1dsjQIk7rM1xa4MkKbznt1XVOKBABlxpzsRxadAAGlFCeRJPG/5SQmGdreXjulXVax6NUj7+w2v701xItUYTO5JYXhW1X2omje4kQBylR410EUfs0enB+gmHEgcElJ3bUYxXLa3D/zrpiSZxcA4g8+MQLa9y5mzohNSYJ2u2/fnSCTDRd4kVlfI6ctXFLhNAgOHDSVaGY0KzrZk5Grvqv+8T5OtkEBcLjVkeApXu1QpF+4bv/cPGE9tA3Bhnry3ka/MoYZyYP9SKqgC8mDKwDRsB3vtJ3VjVWRJXkRFsirhhEC9IyLlwHFSwTSiigrKh8/5DlhTYEAAf9CzuLSBUzTRYI2gHHh1kRFFs/eLsw1WUZ0lDcQe8tIlpKY5aeoQiEmJCQaTDxANj01msmBYAQxmj54TGcgU5cf8UxfqHHxi2acXoAAXXsEHJExINcee0lozQB5CAtwgDBmcAvmInQlFGwps/eXCdjIgNyOQixr2hDgZbTTyhw8g/umOvhNCFoBqpYikrtGDnnQHxxe+nB1Xn1HHh84R+6iERK6EyJHzskEtA2FJ9xI7vplpi+PuEayV7fBQ3w1ReP8mDKC8T9aA+Ehs01cG6+t9OSyqlg8NQQ0LemIGTlPrMKEU+4BAU5/BRFrqlKuDo0VQ/PmSSlsoFXVErBE67KpGvAkeMQ2hP/LcnnFhMtCIXYGGS4bADnbvVfnVjDc5LxCJlru0xBYSAoC3kLvuhp9MJsJB1XySNeNAgqDNnwsWmrPBgGS2BoCpyawSZUOxwACa+XFC82HrbU6j+kRX6mfgd9jzhVg7X5j6RHLoRo4dCkp3w48xevm+iyI1y/oUkWL5VxLFBbuqtNYdLwnHyyVDnQrgiftT9EP7faK6XW6qWtUiZtXy9F8Du6dzhYJGAMMJam8JGkAS0mWu5ze+KFtYpLVVJHKCCZxvbUQMBRoQW4BtH9eMaytEKKyoOy2SFKsEwzeBAC2iQcDmKlDDglIdzffWI/O358YT1hNdA76opzHJSsYxSKHi2esimL6z5vkU2Z3R9+A8KSTghQ4ds5Qg9dpsFz2tso6JnXRxrlY/KxI0UwuqLh2eN5hTKp784/LraX39woXEXtoU4YQXfhCxzwHWWViZa1PaHQJt65hA+Z3cut7e9CiUtnnNhJqwOdaTlgU+YWnjARnTFofFoDWkS0cE5MTE/LpITWGxQnEH3le0f4QT22N0PD4aJwPhSq6bM3uXGcqIegiViynELlfgBuSvzFg8W+vFBWlWS4Z15e6wHhss1UiJIlCJEFJB0vk9tN146zQdqOZTBoH8SBJh3KcW1dl3kQ1O1YmZQgfBpEm1xbV78aAgh9rK55qhSHh0jZfIAbMtm49AT5EBNfUFAbKgP5cuW67Tqys5Ha9MeGhjlxfvqqcdIB+kdLOI8E4K8pTT5Kme11v/XRqYmBuq+CnT7GAnxxX/BJiiCOQssYtRTAP/gIUP8p/NL89xYRLX4A7BKx8xQIltmLjRM5l7NQjoDQYj2jMxUV+0SYcLpo5qG5v+u4/nb1+0b6cwJjYGf8mUxXyF3IRJx+hcByscU0ABDbibrZhmBxg/p1seMnFtpZCkvP9UPkA3JUzqziMlcGws4PA0aw5Sni4jQHoqGNQDauRr8++5HxLhu+/KaOlt8zz81pyJ74DzA5uDguO5HpSioRDn9wVoDJwJVUU3V+LIo7C9L8UUTXtR+b+0aoJrZTsVuv0mTDlsIKARP54Z1z3UzFxSzMoaj/9XbXpHUGGuBykiAmYr3APh4YStKyGkrXIqKlQJc/9Q5ZgFsGfJe4DdcInXsapif1XsDyP1IaNdysUpThW7r+hIeRAwlLLalRDNz5JMrqqXwLM6TPeKcK7JUYzwnwBgG7JUC/B4INWSKCDFQQOYU8P329ZMoObu2gvbS1o7jyKZP6SsSJaYahkNr3PNmM8jvslzlngN2rnbedEnVEs5ESsrVcE4AZkJEp46u3J5YGkQQi/tXvF3nUclYO0ECSsbpnYvRwbThE1ruMkpJ9JRgIzOJ9Zw+RriAFsnaFoIZpr6211+dslsLc3XBs2iRFClPjlReNtJpyRjgZMPG4eeg7XzxBlpQ8F7OWluy0r90yw3WYoKwnK+3gVC0iWogFuRUD8RuKwIKcBLAqzNRuzQxdFR8AgiS80GRxtCoZ5LEVxpcPiIXBCUDZcYiILf7LwZ8h1CCnIipkAnL1lHG97fnpG5ywKJM/9szv0f25oU4IpVD9Gi2xIVxCl1lW9D0SRzIHgX7tlOEf2bxjLU4ayz9Yq8LgfgWytIjrxWS+p15cLV2hxLPRLsr8rEQYLl5uKbBKcIsQt/k8+MQy73eEC+1wauIRTYf5hnJ2kkyTzQFER+7DwJbNJEH8o47WgGh9bmZJECLeTZcqDCW3DFYJGWHgQTIcL/4X5EgIlA2I1upEaD51h7/wW/ydnadIoeul+nW4UO0HaAeIjbcVAmpJ+5KNzz6dzO2izYmBqitSFAP+kOXj7eFzfJLH+9Wcz0y06y4vklfaSBcFwtjRZ+qhft6T9aPhFoSxCO8Npzr0X1vEaakOe9wZ0uzLdC3nw8+usA3agw67T/HmYNgPvq10Ajsht2aDnBp5dIVnIQ/LenBxJH3goDyHi4a8fMdjyrV4vmQBykEDJ/R7Z7nzzZi7xcp2VLg/RHx0kLvyxSFZ5gDeMYpTJzIr7cpsL4b70CZMO3DZAJ5fIlM8f1B24P4fuXSUrCtmf9XyjH3b88aohWZAQCEP5ZKGoCFB8TxwUyAKs8Q4kJc6kJnDJCU/85XfPiVXUZSyx6aV2LLVO9wWzgSOui7FT7gIcECdUmyjzYroKW2jf9XaQAB/4CAqI3rO+KCkBhzxLN6fUEeS91ZzAocToEVjFMcDKRPoOvd+ETqTzs/Q9UrIgIwNHWAHia1IAEImjsBMKU2BWNnS5DI3Bm+DBhbPrTCuDMCpclbuLp+HIEN7QY28UL6bZGRFcOduITwOtJVl/UQF0mBnDF8CnIJQJqiTgRwhXwh2soJigeIFkfAc4uCKqEljC7290ZJb6paGkJ8L6riZMeRnOQV3Jet0Y4+2aQ8CPR8j892I2rD1KJS4VrJ60V6Uz4lqD/2nf8tlgVlcu8sW+sNuYG9tq4bJGOpAieYW8lm6gXzpqp1utcEqgiMR/q/HTyh019FZuiybSeV1qtAC4QYbOKx6ldrN1rCa4+0Bb9xnhnWCvrH5MmPeZp/s4AAgFBQrTZy4oyfZX1uNaKmGJRWERWrDwRWjmOA3yxKEv0I8XXgWclFOHkbgGOAwDTT0rFpITsJtQ3Esx9hFsSg0DBJh4ODCKGnxS4i3d7/6Eue2xDaIP2ckAvdj0Npn5MfxJVNJdBEgC/5Y1dhxA9zaACJrIbM90VjE+4a5rF4kCvnCu+NUHJZNGiY0/aY9QaQjXWadkGe40qqh+lCKA0FGODhwPOP9Ce1I8t6qRJukwjTNkY2B+nnAhJByDJs+wqDFMu0R1p+0OS3EQD2RHoHUWtu3+PrRwu6m2VMMvD0YSIn27cFzWksrYiAl2lZEZlrU24OBlGjfHjyntbQiBlKibUVkpkW9PRhIifbtwXNaSyti4P8BFdnJeco9OwUAAAAASUVORK5CYII=" alt="Experiencia">
                ADMIN
            </span>
            <section class="sidebar">
                @include('layouts.sidebar-menu')
            </section>
        </aside>
        <div class="content-wrapper">
            @if(Session::get('response'))
                <div class="alert {{ (Session::get('response')['status_code'] >= 200 && Session::get('response')['status_code'] <300) ? 'alert-success' : 'alert-danger' }}" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ Session::get('response')['message'] }}
                </div>
            @endif
            <section class="content-header">
                @yield('content-header')
            </section>
            <section class="content">
                @yield('content')
            </section>
        </div>
    </div>
    @endif
@endsection

@section('scripts')
<script src="{{ asset('js/app.min.js') }}?v={{ config('app.version') }}"></script>
<script type="text/javascript">
    $(function() {
        var response = {!! json_encode(Session::get('response')) !!};
        var ele = null;
        if (response !== null) {
            ele = $('.wrapper >.flash-message');
            if (response.status_code == 200 || response.status_code == 201) {
                ele.find('.card').addClass('green').addClass('darken-3');
                setTimeout(function() {
                    ele.fadeOut(1000);
                }, 10000);
            }
            else {
                ele.find('.card').addClass('red').addClass('darken-3');
            }
            ele.show();
        }

        changeSidebarHeight();

        $(".sidebar").click(function() {}, function() {
            changeSidebarHeight();
        });

        function changeSidebarHeight() {
            setTimeout(function() {
                $('.sidebar').height($('.sidebar').height() - 150 + $(".main-header").height());
            }, 700);
        }
    });
</script>
@endsection

@php
Session::forget('response');
@endphp
