<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta name="description" content="Experiencia">
        <meta name="author" content="Copyleft">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="msapplication-tap-highlight" content="no">
        <meta name="keywords" content="Experiencia, Experiencia Puerto Escondido, Spanish, Surf, Housing">
        <title>
            @yield('title') - Experiencia
            @show
        </title>

        <!-- Favicons-->
        <link rel="icon" href="{{ asset('favicon.ico') }}" sizes="32x32">
        <!-- Favicons-->
        <link rel="apple-touch-icon-precomposed" href="{{ asset('favicon.ico') }}">
        <!-- For iPhone -->
        <meta name="msapplication-TileColor" content="#00bcd4">
        <meta name="msapplication-TileImage" content="{{ asset('favicon.ico') }}">
        <!-- For Windows Phone -->
        
        @yield('styles')
    </head>
    <body class="sidebar-mini fixed {{ Auth::guest() ? 'login-page' : '' }}">
        @if (in_array(config('app.env'), ['local', 'development']))
        <div class="dev-badge" style="{{ in_array(Request::route()->getName(), ['login', 'password.reset', 'password.request']) ? 'text-align: center;' : '' }}">
            <span>Usted está navegando el sistema de desarrollo. <a href="https://booking.experienciamexico.mx/">Haga clic aquí</a> para ir al sistema de producción.</span>
        </div>
        @endif
        @yield('container')
        @yield('scripts')
    </body>
</html>