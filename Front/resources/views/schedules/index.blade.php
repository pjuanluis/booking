@extends('layouts.app')

@section('title')
    Schedules
@endsection

@section('content-header')
    <h1>Schedules</h1>
@endsection

@section('content')
<style type="text/css">
    .btn-generate-pdf {
        margin-top: 10px;
    }
    .schw .schw-header {
        padding: 10px 0;
    }
    .schw .schw-class-data {
        border: 1px solid #dcdcdc;
    }
    .schw .schw-class-data .header {
        border-bottom: 1px solid #dcdcdc;
        padding: 8px 10px;
    }
    .schw .schw-class-data .body {
        padding: 8px 10px;
    }
    .schw .with-border-dashed {
        border-right: 1px dashed #dcdcdc;
    }
    .schw .schw-class-data .data {
        margin-bottom: 5px;
    }
    .schw .schw-class-data .data span {
        display: block;
    }
    .schw .schw-class-data .data span.inline {
        display: inline-block;
    }
    .schw .schw-class-data .footer {
        border-top: 1px solid #dcdcdc;
        padding: 8px 10px;
    }
    .schw .schw-class-data .footer button {
        padding: 6px 16px;
    }
</style>
<div class="box">
    <div class="box-header with-border">
        <div class="pull-right">
            <ul class="list-inline">
                <li>
                    <div class="btn-group" id="experience-filter">
                        @foreach($experiences as $e)
                            <a href="{{ url()->current() }}?experience={{ $e->slug }}" data-experience="{{ $e->slug }}" class="btn btn-primary {{ isset($params['experience']) ? ($e->slug == $params['experience'] ? 'active' : '') : '' }}">{{ $e->name }}</a>
                        @endforeach
                    </div>
                </li>
                <li>
                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-primary active" title="List view">
                            <input class="list-view1" type="radio" name="view">
                            <i class="fa fa-list" aria-hidden="true" checked></i>
                        </label>
                        <label class="btn btn-primary" title="Calendar view">
                            <input class="calendar-view1" type="radio" name="view">
                            <i class="fa fa-calendar" aria-hidden="true"></i>
                        </label>
                    </div>
                </li>
                <li>
                    <a href="{{ route('schedules.create') }}" class="btn btn-primary">New schedule</a>
                </li>
            </ul>
        </div>
    </div>

    {{--  Table  --}}
    <div class="box-body no-padding" id="schedules-list">
        <div class="pull-right" style="padding: 10px;">
            <button type="submit" class="btn btn-primary" id="btn-export-to-csv">Export to CSV</button>
        </div>
        <div class="clearfix"></div>
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Theme</th>
                        <th>From</th>
                        <th>To</th>
                        <th>Responsible</th>
                        <th>Place</th>
                        <th class="text-center fit">Edit</th>
                        <th class="text-center fit">Delete</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($schedules->data as $s)
                        <tr>
                            <td>{{ $s->subject }}</td>
                            @if (count($s->dates->data) > 0)
                                <td>{{ \Carbon\Carbon::parse($s->dates->data[0]->begin_at)->format('d-m-Y / h:i') }}</td>
                                <td>{{ \Carbon\Carbon::parse($s->dates->data[count($s->dates->data) - 1]->finish_at)->format('d-m-Y / h:i') }}</td>
                            @else
                                <td>No data</td>
                                <td>No data</td>
                            @endif
                            <td>{{ $s->responsible->data->full_name }}</td>
                            <td>{{ $s->place->data->name }}</td>
                            <td class="text-center">
                                <a href="{{ route('schedules.edit', ['id' => $s->id]) }}" class="action-btn">
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </a>
                            </td>
                            <td class="text-center">
                                <form action="{{ route('schedules.destroy', [ 'id' => $s->id]) }}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button type="submit" class="table-btn-destroy action-btn">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="3">No results</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
            {!! Component::pagination(
                10,
                $schedules->meta->pagination->current_page,
                $schedules->meta->pagination->total_pages,
                $params
            ) !!}
        </div>
    </div>

    {{--  Calendar  --}}
    <div class="box-body hidden" id="schedules-calendar">
        <div id="calendar"></div>
    </div>
</div>
<script type="text/javascript">
    window.addEventListener('load', function() {
        let containerFilters = document.getElementById('experience-filter');
        let elViewCalendar = document.querySelector('.calendar-view1');
        containerFilters.addEventListener('click', (e) => {
            let elTarget = e.target;
            if (elTarget.tagName === 'A' && elViewCalendar.checked) {
                e.preventDefault();
                let links = containerFilters.querySelectorAll('a');
                _.each(links, (a, b) => {
                    a.classList.remove('active');
                });
                elTarget.classList.add('active');
                // Quitar el foco
                elTarget.blur();
                // Mostrar datos
                EXP.sch.config.url = elTarget.getAttribute("href");
                EXP.sch.renderData();
            }
        });
        EXP.ClassScheduler = function(config) {
            this.config = config;
            this.ele = $(config.selector);
            this.schedules = this.config.schedules || [];
            this.schedulesGrouped = {};
            var today = moment();
            this.current = {
                firstDate: today.clone().startOf('isoweek'),
                lastDate: today.endOf('isoweek')
            };
            this.current.week = this.current.firstDate.week();
            var self = this;
            this.setCurrent = function(date) {
                this.current.firstDate = date.clone().startOf('isoweek');
                this.current.lastDate = date.endOf('isoweek');
                this.current.week = this.current.firstDate.week();
            };
            this.nextWeek = function() {
                var date = this.current.firstDate.clone().add(1, 'w');
                this.setCurrent(date);
                this.render();
            };
            this.previousWeek = function() {
                var date = this.current.firstDate.clone().subtract(1, 'w');
                this.setCurrent(date);
                this.render();
            };
            this.goto = function(date) {
                if (!(date instanceof moment)) {
                    date = moment(date);
                    if (!date.isValid()) {
                        return;
                    }
                }
                if (this.current.week === date.week()) {
                    return;
                }
                this.setCurrent(date);
                this.render();
            };
            this.renderNav = function() {
                var content = '' +
                        '<strong class="schw-dates"></strong>' +
                        '<button class="schw-prev-button"><i class="fa fa-chevron-left"></i></button>' +
                        '<button class="schw-next-button"><i class="fa fa-chevron-right"></i></button>' +
                        '<button class="schw-today-button">Today</button>';

                this.ele.find('.schw-header').html(content);
                // Inicializar botones
                this.ele.find('.schw-header .schw-next-button').click(function() {
                    self.nextWeek();
                });
                this.ele.find('.schw-header .schw-prev-button').click(function() {
                    self.previousWeek();
                });
                this.ele.find('.schw-header .schw-today-button').click(function() {
                    self.goto(moment());
                });
            };
            this.renderData = function() {
                var template = '' +
                                '<tr>' +
                                    '<td rowspan="%rowspan%"><b>%TIME%</b></td>' +
                                    '<td colspan="8"><b>%DATE%</b></td>' +
                                '</tr>' +
                                '%newRows%';

                var showData = function() {
                    self.schedulesGrouped = {};
                    var oldStartDate = null;
                    var oldEndDate = null;
                    var areSameDate = false;
                    var container = self.ele.find('#data-container');
                    var extraRows = 0;
                    var tpl = '';
                    var rowTemplate = '<tr>%rowContent%</tr>';
                    var colTemplate = '<td colspan="%colspan%" class="%classes%">%colContent%</td>';
                    var editBtnTemplate = '<a class="action-btn" href="%url%"><i class="fa fa-pencil" aria-hidden="true"></i></a>'
                    var newRows = '';
                    var newColumns = '';
                    container.html('');
                    // Agrupar a todos los schedules que empiezan y terminan a la misma hora
                    _.each(self.schedules, function(r) {
                        var start = moment(r.dates.data[0].begin_at).format('h:mm a');
                        var end = moment(r.dates.data[0].finish_at).format('h:mm a');
                        // Validar si los schedulesGrouped tienen la propiedad (start + ' - ' + end)
                        if (!self.schedulesGrouped.hasOwnProperty(start + ' - ' + end)) {
                            // El objeto no tiene la propiedad, entonces hay que crearla y asignarle un arreglo vacio
                            self.schedulesGrouped[start + ' - ' + end] = [];
                        }
                        // Agregar el schedule en su grupo correspondiente
                        self.schedulesGrouped[start + ' - ' + end].push(r);
                    });

                    _.each(self.schedulesGrouped, function(schedules, index) {
                        tpl = template;
                        newRows = '';
                        extraRows = 1;
                        tpl = tpl.replace('%TIME%', index);
                        areSameDate = false;
                        _.each(schedules, function(s, index) {
                            newColumns = '';
                            var startDate = moment(s.dates.data[0].begin_at).format('DD MMMM YYYY');
                            var endDate = moment(s.dates.data[s.dates.data.length - 1].finish_at).format('DD MMMM YYYY');
                            if (index == 0) {
                                tpl = tpl.replace('%DATE%', startDate == endDate? startDate : startDate + ' to ' + endDate);
                            } else if (!(oldStartDate === startDate && oldEndDate === endDate)) {
                                newColumns += (colTemplate.replace('%colContent%', '<b>' + (startDate == endDate? startDate : startDate + ' to ' + endDate) + '</b>')).replace('%colspan%', 8);
                                newRows += rowTemplate.replace('%rowContent%', newColumns);
                                areSameDate = false;
                                newColumns = '';
                                extraRows++;
                            }
                            if (!areSameDate) {
                                oldStartDate = startDate;
                                oldEndDate = endDate;
                                areSameDate = true;
                            }

                            newColumns += (colTemplate.replace('%colContent%', s.subject)).replace('%colspan%', 1);
                            var level = 'S' + s.schedulable.data.level;
                            newColumns += (colTemplate.replace('%colContent%', level)).replace('%colspan%', 1);
                            newColumns += (colTemplate.replace('%colContent%', s.place.data.name)).replace('%colspan%', 1);
                            newColumns += (colTemplate.replace('%colContent%', s.responsible.data.full_name)).replace('%colspan%', 1);
                            var dataStudents = [];
                            _.each(s.schedulable.data.clients.data, function(c, i) {
                                dataStudents.push(c.first_name + ' ' + c.last_name);
                            });
                            newColumns += (colTemplate.replace('%colContent%', dataStudents.join(', '))).replace('%colspan%', 1);
                            newColumns += colTemplate.replace(/%colContent%|%colspan%|%classes%/gi, function(term) {
                                if (term === '%colContent%') {
                                    return editBtnTemplate.replace('%url%', '/admin/schedules/' + s.id + '/edit');
                                } else if (term === '%colspan%') {
                                    return 1;
                                } else if (term === '%classes%') {
                                    return 'text-center'
                                }
                            });
                            newRows += rowTemplate.replace('%rowContent%', newColumns);
                        });
                        tpl = tpl.replace('%newRows%', newRows);
                        tpl = tpl.replace('%rowspan%', schedules.length + extraRows);
                        container.append(tpl);
                    });
                    if (self.schedules.length > 0) {
                        let btnPrint = '<button type="button" class="btn btn-primary btn-generate-pdf">Generate PDF</button>';
                        container.append(btnPrint);
                        initExportToPdf();
                    }
                };

                if (this.config.url === void 0) {
                    showData();
                }
                else {
                    // Mostrar loading mask
                    this.ele.ploading({
                        action: 'show',
                        spinnerHTML: '<i></i>',
                        spinnerClass: 'fa fa-spinner fa-spin p-loading-fontawesome'
                    });
                    $.ajax({
                        url: this.config.url,
                        type: 'GET',
                        dataType: 'json',
                        data: {
                            from: this.current.firstDate.format('YYYY-MM-DD'),
                            to: this.current.lastDate.format('YYYY-MM-DD')
                        },
                        success: function(r) {
                            if (r.success) {
                                self.schedules = r.data;
                                showData();
                            }
                            self.ele.ploading({action: 'hide'});
                        }
                    });
                }
            };
            this.init = function() {
                this.ele.addClass('schw');
                this.ele.append('<div class="schw-header"></div>');
                this.ele.append('<div class="schw-class-container">');
                // Mostrar datos
                this.renderNav();
                this.render();
            };
            this.render = function() {
                // Mostrar fechas
                this.ele.find('.schw-header .schw-dates').html(this.current.firstDate.format('MMMM DD') + ' - ' + this.current.lastDate.format('MMMM DD'));
                this.ele.find('.schw-class-container').html(
                    '<div class="schw-class-data">' +
                        '<div class="body">' +
                            '<table class="table table-bordered" style="margin-bottom: 0;">' +
                                '<thead>' +
                                    '<tr>' +
                                        '<th style="width: 160px;">Time</th>' +
                                        '<th style="width: 150px;">Theme</th>' +
                                        '<th style="width: 100px;">Level</th>' +
                                        '<th style="width: 150px;">Place</th>' +
                                        '<th style="width: 150px;">Responsable</th>' +
                                        '<th>Students</th>' +
                                        '<th style="width: 50px;">Edit</th>' +
                                    '</tr>' +
                                '</thead>' +
                                '<tbody id="data-container">' +
                                '</tbody>' +
                            '</table>' +
                        '</div>' +
                    '</div>'
                );
                this.renderData();
            };
            this.init();
        };

        EXP.initConfirmButton({
            selector: 'button.table-btn-destroy',
            title: "Delete schedule",
            msg: "This action is going to delete the selected schedule.",
            callback: function(btn) {
                $(btn).parent().submit();
            }
        });
        $('.list-view1').on('change', function (e) {
            if (this.checked) {
                location.reload();
            }
        });
        $('.calendar-view1').on('change', function (e) {
            if (this.checked) {
                EXP.sch.config.url = window.location;
                $('#schedules-calendar').removeClass('hidden');
                $('#schedules-list').addClass('hidden');
            }
        });
        EXP.sch = new EXP.ClassScheduler({
            selector: '#calendar',
            url: "{{ route('schedules.index') }}"
        });
    });
</script>
@endsection

@section('scripts')
    @parent
    <script type="text/javascript">
        function initExportToPdf() {
            EXP.Downloader({
                el: '.btn-generate-pdf',
                url: '{{ route('schedules.print') }}',
                filename: 'schedule.pdf',
                mimetype: 'application/pdf',
            }, function(request) {
                if (EXP.sch.schedules.length > 0) {
                    let dataJson = JSON.stringify(formatData(EXP.sch.schedulesGrouped));
                    let currentWeek = EXP.sch.current.firstDate.format('MMMM DD') + ' - ' + EXP.sch.current.lastDate.format('MMMM DD');
                    request.send('scheduleData=' + dataJson + '&_token={{ csrf_token() }}&week=' + currentWeek);
                }
            });
        }

        function initExportToCsv() {
            EXP.Downloader({
                el: '#btn-export-to-csv',
                url: '{{ route('schedules.csv') }}',
                filename: 'schedule.csv',
                mimetype: 'application/csv',
            }, function(request) {
                request.send("_token={{ csrf_token() }}");
            });
        }

        initExportToCsv();

        function formatData(data) {
            var dataArray = {};
            var start = '';
            var end = '';
            var startDate = '';
            var endDate = '';
            var students = [];
            _.each(data, function(schedules, index) {
                dataArray[index] = [];
                _.each(schedules, function(s) {
                    start = moment(s.dates.data[0].begin_at).format('h:mm a');
                    end = moment(s.dates.data[0].finish_at).format('h:mm a');
                    startDate = moment(s.dates.data[0].begin_at).format('DD MMMM YYYY');
                    endDate = moment(s.dates.data[s.dates.data.length - 1].finish_at).format('DD MMMM YYYY');
                    students = [];
                    _.each(s.schedulable.data.clients.data, function(c, i) {
                        students.push(c.first_name + ' ' + c.last_name);
                    });
                    dataArray[index].push({'start': start, 'end': end, 'start_date': startDate, 'end_date': endDate, 'place': s.place.data.name, 'responsible': s.responsible.data.full_name, 'subject': s.subject, 'level': 'S' + s.schedulable.data.level, 'students': students.join(', ')});
                });
            });
            return dataArray;
        }
    </script>
@endsection
