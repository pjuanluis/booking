@extends('layouts.app')

@if ($formType === Constant::FORM_CREATE)
    @section('section-title', 'Schedule / Create')
@elseif ($formType === Constant::FORM_EDIT)
    @section('section-title', 'Schedule / Edit')
@elseif ($formType === Constant::FORM_SHOW)
    @section('section-title', 'Schedule / Show')
@endif

@section('content-header')
    <h1>
        @if ($formType === Constant::FORM_CREATE)
            Create
        @elseif ($formType === Constant::FORM_EDIT)
            Edit
        @elseif ($formType === Constant::FORM_SHOW)
            View
        @endif
        Schedule
    </h1>
@endsection

<?php
function getData($field, $group, $formType) {
    if (old($field)) {
        return old($field);
    }
    if ($formType === Constant::FORM_EDIT || $formType === Constant::FORM_SHOW) {
        error_log("Field: $field");
        return $group->$field;
    }
    return '';
}
?>
@section('content')
    <style type="text/css">
        .close {
            position: absolute;
            right: 10px;
            top: 5px;
            color: #ff0000;
        }
        .close:focus, .close:hover {
            color: #ff0000;
        }
        #clients-list {
            min-height: 150px;
            max-height: 500px;
            overflow-y: auto;
        }
        #client-filters {
            border-bottom: 1px solid #eee;
            padding-bottom: 10px;
            margin-bottom: 10px;
        }
        .form-control {
            padding: 6px 12px !important;
        }
    </style>
    <form id="form-schedule" action="{{ $formAction  }}" method="post">
        {{ csrf_field() }}
        @if($formType === Constant::FORM_EDIT)
            {{ method_field('PUT') }}
            <input type="hidden" name="updated_at" value="{{ old('updated_at') ? old('updated_at') : $schedule->updated_at }}">
        @endif
        <div class="box" id="app">
            <div class="box-header">
                <div class="panel panel-fit">
                    <div id="experiences">
                        <div class="panel-heading">
                            <h4>Experiences</h4>
                        </div>
                        <div class="panel-body">
                            @php
                                $experienceSelected = old('experiences', isset($schedule)? $schedule->experiences->data[0]->id : '');
                                $isPrivate = old('is_private', isset($schedule)? $schedule->is_private : '');
                            @endphp
                            <ul class="list-inline">
                                @foreach($experiences as $e)
                                    <li>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" class="iCheck" name="experiences"
                                                value="{{ $e->id }}" {{ $experienceSelected == $e->id? 'checked' : '' }}> {{ $e->name }}
                                            </label>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                            {{--  Private experience  --}}
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="iCheck" name="is_private" id="experience-private"
                                    v-model="experiencePrivate" value="1" {{ $isPrivate == 1 ? 'checked' : '' }}> Private
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box-body">
                <div id="step-1">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group {{ $errors->has('level')? 'has-error' : '' }}">
                                <label clasS="control-label">Level</label>
                                <input class="form-control" type="number" min="1" name="level" value="{{ old('level') ? old('level') : (isset($schedule->schedulable->data->level) ? $schedule->schedulable->data->level : '') }}">
                                <span class="help-block">{{ $errors->first('level') }}</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            {{--  Group  --}}
                            <div class="form-group {{ $errors->has('group') ? 'has-error' : '' }}">
                                <label  class="control-label">Group</label>
                                @php
                                    $groupId = old('group') ? old('group') :
                                        (isset($schedule) && $schedule->schedulable_type == 'App\\Group' ? $schedule->schedulable_id : '');
                                @endphp
                                <input class="form-control" type="text" name="group" value="{{ old('group') ? old('group') : (isset($schedule->schedulable->data->name) ? $schedule->schedulable->data->name : '') }}">
                                <span class="help-block">{{ $errors->first('group') }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            {{--  Level  --}}
                            <div class="form-group {{ $errors->has('subject') ? 'has-error' : '' }}">
                                <label  class="control-label">Theme</label>
                                <input class="form-control" type="text" name="subject" value="{{ old('subject') ? old('subject') : (isset($schedule->subject) ? $schedule->subject : '') }}">
                                <span class="help-block">{{ str_replace('subject', 'theme', $errors->first('subject')) }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            {{--  Date  --}}
                            <div class="form-group {{ $errors->has('schedule_time')? 'has-error' : '' }}">
                                <label  class="control-label">Select dates</label>
                                <div class="v-dpinline">
                                    <div class="dp-inline__calendar" data-date-format="yyyy-mm-dd" data-multidate="true"></div>
                                    @php
                                    $dates = [];
                                    if (isset($schedule->dates->data)) {
                                        foreach ($schedule->dates->data as $date) {
                                            $dates[] = \Carbon\Carbon::parse($date->begin_at)->format('Y-m-d');
                                        }
                                    }
                                    @endphp
                                    <input type="hidden" class="dp-inline__input" name="begin_date" value="{{ old('begin_date') ? old('begin_date') : (isset($schedule->dates->data) ? implode(',', $dates) : '') }}">
                                </div>
                                <span class="help-block">{{ str_replace('schedule time', 'start date', $errors->first('schedule_time')) }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            {{--  Start time  --}}
                            <div class="form-group {{ $errors->has('schedule_time') ? 'has-error' : '' }}">
                                <label  class="control-label">Start time</label>
                                <div class="input-group">
                                    <input type="text" class="form-control datetimepicker" id="start-time" data-format="LT" :value="getSlugOfExperience(this.experience) == 'spanish' ? '09:00': ''"
                                        name="begin_time" value="{{ old('begin_time') ? old('begin_time') : (isset($schedule->dates->data) ? \Carbon\Carbon::parse($schedule->dates->data[0]->begin_at)->format('h:i A') : '9:00 AM') }}">
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-time"></span>
                                    </span>
                                </div>
                                <span class="help-block">{{ str_replace('schedule time', 'start time', $errors->first('schedule_time')) }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            {{-- Duration --}}
                            <div class="form-group {{ $errors->has('schedule_time') ? 'has-error' : '' }}">
                                <label class="control-label">Duration</label>
                                <div class="input-group">
                                    <input class="form-control" type="number" name="duration" min="1" max="144" step="0.5"
                                           value="{{ old('duration') ? old('duration') : (isset($schedule->dates->data) ? \Carbon\Carbon::parse($schedule->dates->data[0]->finish_at)->diffInSeconds(\Carbon\Carbon::parse($schedule->dates->data[0]->begin_at)) / 3600 : '3') }}">
                                    <span class="input-group-addon">Hours</span>
                                </div>
                                <span class="help-block">{{ str_replace('schedule time', 'duration', $errors->first('schedule_time')) }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="step-2" style="display: none;">
                    <div class="row">
                        @if ($formType !== Constant::FORM_SHOW)
                        <div class="col-sm-6" style="border-right: 5px dashed #eee;">
                            <div class="panel panel-default" style="margin-bottom: 0;">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Listing of students</h3>
                                </div>
                                <div class="panel-body">
                                    <div id="client-filters">
                                        <div class="form-group">
                                            <label for="filter-name" class="control-label">Experiences</label><br />
                                            @foreach($experiences as $e)
                                                <label class="checkbox-inline" {!! $loop->first? "style='padding-left: 0;'" : '' !!}>
                                                    <input type="checkbox" id="check-{{ $e->slug }}" class="iCheck" value="1" data-experience="{{ $e->slug }}"> {{ $e->name }}
                                                </label>
                                            @endforeach
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-sm-6">
                                                <label for="filter-name">Name</label>
                                                <input type="text" class="form-control" id="filter-name" placeholder="Jane Doe">
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="filter-date">Courses begin date</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control dpicker-local" id="filter-date" placeholder="{{ \Carbon\Carbon::now()->format('d-m-Y') }}">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-sm-6">
                                                <label for="filter-country">Country</label>
                                                <select class="form-control" id="filter-country" name="country_id">
                                                    <option value=""></option>
                                                    @foreach ($countries as $country)
                                                    <option value="{{ $country->id }}">{{ $country->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="filter-agency">Agency</label>
                                                <select class="form-control" id="filter-agency" name="agency_id">
                                                    <option value=""></option>
                                                    @foreach ($agencies as $agency)
                                                    <option value="{{ $agency->id }}">{{ $agency->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <button id="btn-search-clients" type="button" class="btn btn-primary pull-right"><i class="fa fa-search"></i> Search</button>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="clients-list" class="list-group"></div>
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="col-sm-6">
                            {{--  Place  --}}
                            <div class="form-group {{ $errors->has('place_id') ? 'has-error' : '' }}">
                                <label class="control-label">Place</label>
                                <select name="place_id" class="select2 form-control" style="width: 100%;">
                                    <option value="">- Select -</option>
                                    @php
                                        $placeId = old('place_id') ? old('place_id') : (isset($schedule->place->data->id) ? $schedule->place->data->id : '');
                                    @endphp
                                    @foreach($places as $p)
                                        <option value="{{ $p->id }}" {{ $placeId == $p->id ? 'selected' : '' }}>
                                            {{ $p->name }}
                                        </option>
                                    @endforeach
                                </select>
                                <span class="help-block">{{ $errors->first('place_id') }}</span>
                            </div>
                            {{--  Responsible  --}}
                            <div class="form-group {{ $errors->has('responsible_id') ? 'has-error' : '' }}">
                                <label  class="control-label">Responsible</label>
                                <select name="responsible_id" class="select2 form-control" style="width: 100%;">
                                    <option value="">- Select -</option>
                                    @php
                                        $responsibleId = old('responsible_id') ? old('responsible_id') : (isset($schedule->responsible->data->id) ? $schedule->responsible->data->id : '');
                                    @endphp
                                    @foreach($peoples as $p)
                                        <option value="{{ $p->id }}" {{ $responsibleId == $p->id ? 'selected' : '' }}>
                                            {{ $p->first_name . ' ' . $p->last_name }}
                                        </option>
                                    @endforeach
                                </select>
                                <span class="help-block">{{ $errors->first('responsible_id') }}</span>
                            </div>
                            <div class="form-group {{ $errors->has('clients') ? 'has-error' : '' }}">
                                <label class="control-label">Students</label>
                                <input id="hidden-clients" type="hidden" name="clients" />
                                <input id="hidden-clientnames" type="hidden" name="client_names" />
                                <input id="hidden-clientcourses" type="hidden" name="client_courses" />
                                <div id="selected-clients" class="list-group">
                                    <?php
                                    $clients = getData('clients', $group, $formType);
                                    $clientNames = [];
                                    $clientCourses = [];
                                    if (old('clients') != null) {
                                        $clientNames = explode('|', getData('client_names', $group, $formType));
                                        $clientCourses = explode('|', getData('client_courses', $group, $formType));
                                        $clients = explode(',', $clients);
                                    }
                                    else if ($formType !== Constant::FORM_CREATE) {
                                        $clients = $clients->data;
                                    }
                                    else {
                                        $clients = [];
                                    }
                                    $id = $name = $courses = $studentExperiences = null;
                                    foreach ($clients as $i => $c) {
                                        $id = $c;
                                        if (old('clients') == null && $formType !== Constant::FORM_CREATE) {
                                            $id = $c->id;
                                            $name = $c->first_name . ' ' . $c->last_name;
                                            $courses = [];
                                            foreach ($c->commands->data as $cc) {
                                                foreach ($cc->commandCourses->data as $ccc) {
                                                    $studentExperiences = [];
                                                    foreach ($ccc->package->data->experiences->data as $ex) {
                                                        $studentExperiences[] = $ex->name;
                                                    }
                                                    $courses[] = implode('& ', $studentExperiences) . ' - ' . $ccc->package->data->name;
                                                }
                                            }
                                            $courses = implode(', ', $courses);
                                        }
                                        else {
                                            $name = $clientNames[$i];
                                            $courses = $clientCourses[$i];
                                        }
                                        ?>
                                        <a href="#" class="list-group-item" data-id="{{ $id }}">
                                            <h4 class="list-group-item-heading">{{ $name }}</h4>
                                            <p class="list-group-item-text">{{ $courses }}</p>
                                            @if ($formType !== Constant::FORM_SHOW)
                                            <button type="button" class="close pull-right" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            @endif
                                        </a>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <span class="help-block">{{ str_replace('clients', 'students', $errors->first('clients')) }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box-footer">
                <a class="btn btn-default" href="{{ url()->previous() }}">
                    @if ($formType !== Constant::FORM_SHOW)
                        Cancel
                    @else
                        Back
                    @endif
                </a>
                <button id="btn-back" class="btn btn-default" type="button">Back</button>
                <button id="btn-primary" class="btn btn-primary" type="button">Save</button>
            </div>
        </div>
    </form>
@endsection

@section('scripts')
    @parent
    <script src="{{ asset('js/vue.min.js') }}"></script>
    @php
        $experienceId = old('experiences') ? old('experiences') : (isset($schedule->experiences->data[0]->id) ? $schedule->experiences->data[0]->id : null);
        $clientId = old('schedulable_client_id') ? old('schedulable_client_id') :
            (isset($schedule) && $schedule->schedulable_type == 'App\\Client' ? $schedule->schedulable_id : '\'\'');
    @endphp
    <script type="text/javascript">
        var form = $('#form-schedule');
        var button = form.find('#btn-primary');
        var onHashChange = function() {
            var hash = window.location.hash.trim();
            if (hash === '' || hash === '#step-1') {
                $('#step-2').hide();
                $('#step-1').show();
                $('#experiences').show();
                button.text('Next');
                button.attr('target', 'step-2');
                form.find('#btn-back').hide();
            }
            else if (hash === '#step-2') {
                $('#step-1').hide();
                $('#experiences').hide();
                $('#step-2').show();
                button.text('Save');
                button.attr('target', 'create');
                form.find('#btn-back').attr('target', 'step-1').show();
            }
        };
        window.onhashchange = onHashChange;
        onHashChange();
        function showStudentsInSelect(filters) {
            $.ajax({
                url: "{{ route('clients.index') }}?experiences=" + filters.join(),
                dataType: 'json',
                cache: false
            }).done(function (result) {
                var $students = $(document.querySelector('#students'));
                $students.html('');
                $students.append($('<option>', {
                    value: '',
                    text: '- Select -'
                }));
                for (var i = 0, size = result.data.length; i < size; i++) {
                    var $tmpOption = $('<option>', {
                        value: result.data[i].id,
                        text: result.data[i].full_name
                    });
                    if (result.data[i].id === {!! $clientId !!}) {
                        $tmpOption.attr('selected', 'selected');
                    }
                    $students.append($tmpOption);
                }
            });
        }

        function getSlugOfExperience(xp) {
            var experiences = {!! json_encode($experiences) !!},
                slug = '';
            // Get slug of current experience
            for (var i = 0,  size = experiences.length; i < size; i++) {
                if (experiences[i].id == xp) {
                    slug = experiences[i].slug;
                    break;
                }
            }
            return slug;
        }

        function init() {
            form = $('#form-schedule');
            button = form.find('#btn-primary');
            button.click(function(evt) {
                evt.preventDefault();
                var target = button.attr('target');
                if (target === 'create') {
                    // Enviar al api
                    $('#form-schedule').submit();
                }
                else {
                    window.location.hash = target;
                }
            });
            form.find('#btn-back').click(function(evt) {
                evt.preventDefault();
                var target = $(this).attr('target');
                window.location.hash = target;
            });
            var radios = $('input[name=experiences]:checked');
            if (radios.length <= 0) {
                radios = $('input[name=experiences]');
                if (radios.length > 0) {
                    radios.first().iCheck('check');
                }
            }
        };


        window.addEventListener('load', function() {
            init();
            $('.v-dpinline').dPickerInline();
            @if ($formType !== Constant::FORM_SHOW)
            $('#selected-clients .list-group-item button.close').click(function(evt) {
                evt.preventDefault();
                $(this).closest('.list-group-item').remove();
            });
            var typingTimer;
            var doneTypingInterval = 500;
            //on keyup, start the countdown
            $('#filter-name').keyup(function(){
                clearTimeout(typingTimer);
                if ($('#filter-name').val()) {
                    typingTimer = setTimeout(searchClients, doneTypingInterval);
                }
            });
            $('#btn-search-clients').click(function(evt) {
                evt.preventDefault();
                searchClients();
            });
            $('#filter-country').select2({
                placeholder: "Select country",
                allowClear: true
            });
            $('#filter-agency').select2({
                placeholder: "Select agency",
                allowClear: true
            });
            @endif
            var searchClients = function() {
                // Mostrar loading
                $('#clients-list').ploading({
                    action: 'show',
                    spinnerHTML: '<i></i>',
                    spinnerClass: 'fa fa-spinner fa-spin p-loading-fontawesome'
                });
                var name = $('#filter-name').val();
                var countryId = $('#filter-country').val();
                var agencyId = $('#filter-agency').val();
                var begin = $('#filter-date').val() === ''? $('#filter-date').val() : moment($('#filter-date').val(), 'DD-MM-YYYY').format('YYYY-MM-DD');;
                var experiences = [];
                $('#client-filters input[type=checkbox]').each(function(i, checkbox) {
                    if (checkbox.checked) {
                        experiences.push($(checkbox).data('experience'));
                    }
                });
                // Buscar clientes
                EXP.request.get('/admin/clients?include=scheduleDates.schedule.experiences,commands.commandCourses.package.experiences,commands.commandCourses:from_date(' + begin + ')' +
                        '&name=' + name + '&country_id=' + countryId + '&agency_id=' + agencyId + '&course_from=' + begin + '&experiences=' + experiences.join(',') + '&no-paginate=1', function(resp) {
                    var list = $('#clients-list');
                    list.html('');
                    // Mostrar listado de clientes
                    let now = moment();
                    _.each(resp.data, function(r) {
                        // Buscar si ha tomado clases de surf
                        let scheduleDates = r.scheduleDates.data;
                        let counterSurfClass = 0;
                        for (let i = 0; i < scheduleDates.length; i++) {
                            let beginAt = moment(scheduleDates[i].begin_at);
                            let tooksurfLessons = false;
                            if (beginAt.isBefore(now)) {
                                let experiences = scheduleDates[i].schedule.data.experiences.data;
                                for (let j = 0; j < experiences.length; j++) {
                                    if (experiences[j].slug === 'surf') {
                                        tooksurfLessons = true;
                                        break;
                                    }
                                }
                                if (tooksurfLessons) {
                                    counterSurfClass++;
                                }
                            }
                        }
                        var item = $('<a href="#" class="list-group-item" data-id="' + r.id + '"></a>');
                        item.append('<h4 class="list-group-item-heading">' + r.full_name + (counterSurfClass? '<small>(Surf class: ' + counterSurfClass + ')</small>' : '') + '</h4>');
                        var details = $('<p class="list-group-item-text"></p>');
                        var packs = [];
                        _.each(r.commands.data, function(c) {
                            _.each(c.commandCourses.data, function(cc) {
                                var experiences = _.map(cc.package.data.experiences.data, function(e) {
                                    return e.name;
                                });
                                packs.push(experiences.join('& ') + ' - ' + cc.package.data.name);
                            });
                        });
                        details.html(packs.join(', '));
                        item.append(details);
                        list.append(item);
                    });
                    initListItemsEvents();
                    $('#clients-list').ploading({action: 'hide'});
                });
            };
            var initListItemsEvents = function() {
                $('#clients-list a.list-group-item').click(function(evt) {
                    evt.preventDefault();
                    var clientId = $(this).data('id');
                    // Verificar que el cliente no haya sido añadido previamente a la lista
                    var ids = getSelectedClientIds();
                    for (var i = 0; i < ids.length; i++) {
                        if (ids[i] == clientId) {
                            return;
                        }
                    }
                    var item = $('<a href="#" class="list-group-item" data-id="' + clientId + '"></a>');
                    item.html($(this).html());
                    var closeBtn = $('<button type="button" class="close pull-right" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
                    closeBtn.click(function(evt) {
                        evt.preventDefault();
                        item.remove();
                    });
                    item.append(closeBtn);
                    $('#selected-clients').append(item);
                });
            };
            var getSelectedClientIds = function() {
                var items = $('#selected-clients a');
                var ids = _.map(items, function(item) {
                    return $(item).data('id');
                });
                return ids;
            };
            $('#form-schedule').submit(function() {
                $('.content-wrapper').ploading({
                    action: 'show',
                    spinnerHTML: '<i></i>',
                    spinnerClass: 'fa fa-spinner fa-spin p-loading-fontawesome'
                });
                $('#hidden-clients').val(getSelectedClientIds().join(','));
                var items = $('#selected-clients a');
                var temp = _.map(items, function(item) {
                    return $(item).find('h4').text();
                });
                $('#hidden-clientnames').val(temp.join('|'));
                var temp = _.map(items, function(item) {
                    return $(item).find('p').text();
                });
                $('#hidden-clientcourses').val(temp.join('|'));
            });
        });
    </script>
@endsection
