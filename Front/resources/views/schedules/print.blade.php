<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Eschedule</title>
        <style type="text/css">
            * {
                margin: 0;
                padding: 0;
            }

            body {
                margin: 15px;
            }

            .week {
                text-align: center;
                margin-bottom: 10px;
                font-size: 18px;
            }

            .table {
                width: 100%;
                color: #212529;
                border-collapse: collapse;
                font-size: 10px;
                border: 1px solid #dee2e6;
            }

            .table, .week {
                font-family: 'helvetica', sans-serif;
            }


            .table td, .table th {
                padding: 1px;
                border: 1px solid #dee2e6;
            }

            .table thead th {
                vertical-align: bottom;
                border-bottom: 1px solid #dee2e6;
            }

            .table td.empty {
                border-top: none;
                border-bottom: none;
            }

            .table td.time {
                border-bottom: none;
            }
        </style>
    </head>
    <body>
        @php
            $scheduleData = json_decode($scheduleData, true);
        @endphp
        <h1 class="week">{{ $week }}</h1>
        <div id="box-container">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="width: 120px;">Time</th>
                        <th style="width: 120px;">Theme</th>
                        <th style="width: 35px;">Level</th>
                        <th style="width: 100px;">Place</th>
                        <th style="width: 100px;">Responsable</th>
                        <th style="width: 265px;">Students</th>
                    </tr>
                </thead>
                <tbody id="data-container">
                    @foreach ($scheduleData as $key => $schedules)
                        {!! transformData($schedules, $key) !!}
                    @endforeach
                </tbody>
            </table>
        </div>
        @php
        function transformData($schedules, $time) {
            $oldStartDate = '';
            $oldEndDate = '';
            $rowTemplate = '<tr>%rowContent%</tr>';
            $colTemplate = '<td colspan="%colspan%" class="%classes%">%colContent%</td>';
            $tpl = '<tr><td class="time"><b>%TIME%</b></td><td colspan="8"><b>%DATE%</b></td></tr>%newRows%';
            $newRows = '';
            $newColumns = '';
            $tpl = str_replace('%TIME%', $time, $tpl);
            $areSameDate = false;
            foreach($schedules as $index => $s) {
                $newColumns = '';
                $startDate = $s['start_date'];
                $endDate = $s['end_date'];
                if ($index == 0) {
                    $tpl = str_replace('%DATE%', $startDate == $endDate? $startDate : "{$startDate} to {$endDate}", $tpl);
                } else if (!($oldStartDate === $startDate && $oldEndDate === $endDate)) {
                    $newColumns .= str_replace('%classes%', 'empty', str_replace('%colspan%', 1, str_replace('%colContent%', "", $colTemplate)));
                    $newColumns .= str_replace('%colspan%', 8, str_replace('%colContent%', "<b>" . ($startDate == $endDate? $startDate : "{$startDate} to {$endDate}") . " </b>", $colTemplate));
                    $newRows .= str_replace('%rowContent%', $newColumns, $rowTemplate);
                    $areSameDate = false;
                    $newColumns = '';
                }
                if (!$areSameDate) {
                    $oldStartDate = $startDate;
                    $oldEndDate = $endDate;
                    $areSameDate = true;
                }

                $newColumns .= str_replace('%classes%', 'empty', str_replace('%colspan%', 1, str_replace('%colContent%', "", $colTemplate)));
                $newColumns .= str_replace('%colspan%', 1, str_replace('%colContent%', $s['subject'], $colTemplate));
                $newColumns .= str_replace('%colspan%', 1, str_replace('%colContent%', $s['level'], $colTemplate));
                $newColumns .= str_replace('%colspan%', 1, str_replace('%colContent%', $s['place'], $colTemplate));
                $newColumns .= str_replace('%colspan%', 1, str_replace('%colContent%', $s['responsible'], $colTemplate));
                $newColumns .= str_replace('%colspan%', 1, str_replace('%colContent%', $s['students'], $colTemplate));
                $newRows .= str_replace('%rowContent%', $newColumns, $rowTemplate);
            }
            $tpl = str_replace('%newRows%', $newRows, $tpl);
            return $tpl;
        }
        @endphp
    </body>
</html>
