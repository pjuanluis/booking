@extends('layouts.app')

@section('title', $title)

@section('content-header')
    <h1>{{ $title }}</h1>
@endsection

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <a href="{{ route('users.create') }}" class="btn btn-primary pull-right">New user</a>
        </div>
        <div class="box-body">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>User</th>
                        <th>Type</th>
                        <th>Email</th>
                        <th class="text-center fit">Edit</th>
                        <th class="text-center fit">Delete</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($users->data as $user)
                        <tr>
                            <td>{{ $user->first_name . ' ' . $user->last_name }}</td>
                            <td>
                                @if ($user->authenticatable_type === 'App\Agency')
                                    Agency
                                @elseif ($user->authenticatable_type === 'App\Client')
                                    Client
                                @elseif (!empty($user->role->data))
                                    {{ $user->role->data->name }}
                                @else
                                    Administrator
                                @endif
                            </td>
                            <td>{{ $user->email }}</td>
                            <td class="text-center fit">
                                <a class="action-btn"
                                    @if ($user->authenticatable_type === 'App\Agency')
                                        href="{{ route('agencies.edit', ['id' => $user->authenticatable_id]) }}"
                                    @elseif ($user->authenticatable_type === 'App\Client')
                                        href="{{ route('clients.edit', ['id' => $user->authenticatable_id]) }}"
                                    @else
                                        href="{{ route('users.edit', ['id' => $user->id]) }}"
                                    @endif>
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </a>
                            </td>
                            <td class="text-center fit">
                                <form
                                    @if ($user->authenticatable_type === 'App\Agency')
                                        action="{{ route('agencies.destroy', ['id' => $user->authenticatable_id]) }}"
                                    @elseif ($user->authenticatable_type === 'App\Client')
                                        action="{{ route('clients.destroy', ['id' => $user->authenticatable_id]) }}"
                                    @else
                                        action="{{ route('users.destroy', [ 'id' => $user->id]) }}"
                                    @endif method="post">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button type="submit" class="action-btn table-btn-destroy">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="100">No data available</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
            {!! Component::pagination(
                10,
                $users->meta->pagination->current_page,
                $users->meta->pagination->total_pages,
                $params
            ) !!}
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script>
            $(function() {
                EXP.initConfirmButton({
                    selector: 'button.table-btn-destroy',
                    title: "Delete user",
                    msg: "This action is going to delete the current user.",
                    callback: function(btn) {
                        $(btn).parent().submit();
                    }
                });
            });
    </script>
@endsection
