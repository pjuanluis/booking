@extends('layouts.app')

@section('title', $title)

@section('content-header')
    <h1>{{ $title }}</h1>
@endsection

@section('content')
    <form action="{{ $formAction }}" method="post" autocomplete="off" id="create-user-form">
        {{ csrf_field() }}
        @if ($formType === Constant::FORM_EDIT)
            {{ method_field('PUT') }}
            <input type="hidden" name="updated_at" value="{{ $user->data->updated_at }}" />
        @endif

        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                            <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                                <label for="first_name" class="control-label">First name</label>
                                <input class="form-control" type="text" name="first_name" id="first_name"
                                    value="{{ old('first_name') ? old('first_name') : (isset($user->data->first_name) ? $user->data->first_name : '') }}"
                                    required>
                                <span class="help-block">{{ $errors->first('first_name') }}</span>
                            </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                            <label for="last_name" class="control-label">Last name</label>
                            <input class="form-control" type="text" name="last_name" id="last_name"
                                value="{{ old('last_name') ? old('last_name') : (isset($user->data->last_name) ? $user->data->last_name : '') }}"
                                required>
                            <span class="help-block">{{ $errors->first('last_name') }}</span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                            <label for="email" class="control-label">Email</label>
                            <input class="form-control" type="email" name="email" id="email"
                                value="{{ old('email') ? old('email') : (isset($user->data->email) ? $user->data->email : '') }}"
                                required>
                            <span class="help-block">{{ $errors->first('email') }}</span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                            <label for="password" class="control-label">Password</label>
                            <input class="form-control password" type="text" name="password" id="password"
                                required>
                            <span class="help-block">{{ $errors->first('password') }}</span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('role_id') ? 'has-error' : '' }}">
                            <label for="role_id" class="control-label">Role</label>
                            @php
                                $roleId = old('role_id') ? old('role_id') : (isset($user->data->role->data->id) ? $user->data->role->data->id : '');
                            @endphp
                            <select name="role_id" id="role_id" class="select2 form-control">
                                <option value="">- Select -</option>
                                @foreach ($roles->data as $role)
                                    @php
                                        if ($role->slug === 'agency' || $role->slug === 'client') {
                                            continue;
                                        }
                                    @endphp
                                    <option value="{{ $role->id }}" {{ $roleId === $role->id ? 'selected' : '' }}>
                                        {{ $role->name }}
                                    </option>
                                @endforeach
                            </select>
                            <span class="help-block">{{ $errors->first('role_id') }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <a href="{{ url()->previous() }}" class="btn btn-default">Cancel</a>
        <button type="submit" class="btn btn-primary">Save</button>
    </form>
@endsection

@section('scripts')
    @parent
    <script src="https://terrylinooo.github.io/jquery.disableAutoFill/assets/js/jquery.disableAutoFill.min.js"></script>
    <script type="text/javascript">
        $(function() {
            // Deshabitar el autollenado del navegador cuando se habilita la opción de guardar usuario y contraseña
            $('#create-user-form').disableAutoFill({
                passwordFiled: '.password',
                debugMode: false,
                randomizeInputName: true
            });
        });
    </script>
@endsection
