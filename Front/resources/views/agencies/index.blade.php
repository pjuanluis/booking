@extends('layouts.app')

@section('title', 'Agencies')

@section('content-header')
    <h1>Agencies</h1>
@endsection

@section('content')
    @php
        $user = Auth::user();
    @endphp
    <div class="box">
        <div class="box-header with-border">
            @empty ($user->authenticatable_type)
                <a href="{{ route('agencies.create') }}" class="btn btn-primary pull-right">New agency</a>
            @endempty
        </div>
        <div class="box-body table-responsive no-padding">

            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Code</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th class="fit">Edit</th>
                        <th class="fit">Estatus</th>
                        @empty ($user->authenticatable_type)
                            <th class="fit">Delete</th>
                        @endempty
                    </tr>
                </thead>
                <tbody>
                    @forelse ($agencies->data as $a)
                        <tr>
                            <td>{{ $a->code }}</td>
                            <td>{{ $a->name }}</td>
                            <td>{{ $a->email }}</td>
                            <td class="text-center">
                                <a href="{{ route('agencies.edit', ['id' => $a->id]) }}" class="action-btn">
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </a>
                            </td>
                            <td class="text-center">
                                <i class="fa fa-flag {{ $a->is_active == 1 ? 'on' : 'off' }}" aria-hidden="true"></i>
                            </td>
                            @empty ($user->authenticatable_type)
                                <td class="text-center">
                                    <form action="{{ route('agencies.destroy', [ 'id' => $a->id]) }}" method="post">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button type="submit" class="action-btn table-btn-destroy">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                                        </button>
                                    </form>
                                </td>
                            @endempty
                        </tr>
                    @empty
                        <tr>
                            <td colspan="40">No data</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
            @if (isset($agencies->meta))
                {!! Component::pagination(
                    10,
                    $agencies->meta->pagination->current_page,
                    $agencies->meta->pagination->total_pages,
                    $params
                ) !!}
            @endif

        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script type="text/javascript">
        window.addEventListener('load', function() {
            EXP.initConfirmButton({
                selector: 'button.table-btn-destroy',
                title: "Delete agency",
                msg: "This action is going to delete the current agency.",
                callback: function(btn) {
                    $(btn).parent().submit();
                }
            });
        });
    </script>
@endsection
