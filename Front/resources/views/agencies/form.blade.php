@extends('layouts.app')

@if ($formType === Constant::FORM_CREATE)
    @section('title', 'Create agency')
@elseif($formType === Constant::FORM_EDIT)
    @section('title', 'Edit agency')
@elseif ($formType === Constant::FORM_SHOW)
    @section('title', 'View agency')
@endif

@section('content-header')
    @if ($formType === Constant::FORM_CREATE)
        <h1>Create agency</h1>
    @elseif ($formType === Constant::FORM_EDIT)
        <h1>Edit agency</h1>
    @elseif ($formType === Constant::FORM_SHOW)
        <h1>View agency</h1>
    @endif
@endsection

@section('content')
    <form action="{{ isset($formAction) ? $formAction : '#' }}" method="post" role="form">
        {{ csrf_field() }}
        @if ($formType === Constant::FORM_EDIT)
            {{ method_field('PUT') }}
            <input type="hidden" name="updated_at" value="{{ $agency->updated_at }}">
        @endif
        <div class="box">
            <div class="box-body">

                <div class="form-group {{ $errors->has('code') ? 'has-error' : '' }}">
                    <label for="code" class="control-label">Code*</label>
                    <input type="text" name="code" id="code" class="form-control" required value="{{ old('code', isset($agency)? $agency->code : '') }}" style="text-transform: uppercase;" maxlength="3">
                    <span class="help-block">{{ $errors->first('code') }}</span>
                </div>

                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    <label for="name" class="control-label">Name*</label>
                    <input type="text"name="name" id="name" class="form-control"
                        aria-describedby="help-name" required value="{{ old('name') !== null ?
                            old('name') :
                            (isset($agency->name) ? $agency->name : '') }}">
                    <span id="help-name" class="help-block">{{ $errors->first('name') }}</span>
                </div>

                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                    <label for="email" class="control-label">Email*</label>
                    <input type="email"name="email" id="email" class="form-control"
                        aria-describedby="help-email" required value="{{ old('email') !== null ?
                            old('email') :
                            (isset($agency->email) ? $agency->email : '') }}">
                    <span id="help-email" class="help-block">{{ $errors->first('email') }}</span>
                </div>

                <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                    <label for="password" class="control-label">Password*</label>
                    <input type="password" name="password" id="password" class="form-control"
                        aria-describedby="help-password" {{ $formType === Constant::FORM_EDIT ? '' : 'required' }}
                        value="{{ old('password') !== null ? old('password') : '' }}">
                    <span id="help-password" class="help-block">{{ $errors->first('password') }}</span>
                </div>

                <div class="form-group {{ $errors->has('logo') ? 'has-error' : '' }}">
                    <label for="phone" class="control-label">Logo*</label>
                    <div>
                        @if (isset($agency->file->data->path))
                            <img src="{{ $agency->file->data->path }}" id="logo" width="300" height="300">
                        @endif
                        <div id="croppie-image" class="hidden"></div>
                        <div class="input-group ">
                            <input type="file" id="file" value="Choose an image" accept="image/*">
                            <input type="hidden" id="file-content" name="logo[contents]" />
                            <input type="hidden" id="file-name" name="logo[name]" />
                        </div>
                    </div>
                    <span class="help-block">{{ empty($errors->first('logo')) ? 'Please, use image(JPG, PNG or GIF) less than 1 MB.' : $errors->first('logo') }}</span>
                </div>

            </div>
            <div class="box-footer">
                <a class="btn btn-default" href="{{ url()->previous() }}">
                    @if ($formType !== Constant::FORM_SHOW)
                        Cancel
                    @else
                        Back
                    @endif
                </a>
                @if ($formType === Constant::FORM_SHOW) {
                    <a class="btn btn-primary" href="{{ route('agencies.edit', ['agency' => $agency->id]) }}">
                        Edit
                    </a>
                @else
                    <button type="submit" class="btn btn-primary">Save</button>
                @endif
            </div>
        </div>
    </form>
@endsection

@section('scripts')
    @parent
    <script>
        var $file = $('#file'),
            $fileContent = $('#file-content'),
            $fileName = $('#file-name'),
            $croppie = $("#croppie-image").croppie({
                viewport: { width: 300, height: 300 },
                boundary: { width: 308, height: 308 },
                showZoomer: false,
                url: ''
            });

        $file.on('change', function(e) {
            var file = this.files[0];

            if (this.files && file) {
                var reader = new FileReader();

                reader.onload = function(ev) {
                    $croppie.croppie('bind', {
                        url: ev.target.result
                    });
                    $fileName.val(file.name);
                    $fileContent.val(ev.target.result.split(',')[1]);
                    $('#logo').addClass('hidden');
                    $('#croppie-image').removeClass('hidden');
                }

                reader.readAsDataURL(file);
            }
        });
        $croppie.on('update', function(e, data) {
            $croppie.croppie('result', {
                type: 'base64'
            }).then(function(d) {
                $fileContent.val(d.split(',')[1]);
            });
        });
    </script>
@endsection
