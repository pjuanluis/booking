@extends('layouts.app')

@section('title', 'Currencies')

@section('content-header')
    <h1>Currencies</h1>
@endsection

@section('content')
    @php
        $user = Auth::user();
    @endphp
    <div class="box">
        <div class="box-header with-border">
            @empty ($user->authenticatable_type)
                <a href="{{ route('currencies.create') }}" class="btn btn-primary pull-right">New currency</a>
            @endempty
        </div>
        <div class="box-body table-responsive">

            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Code</th>
                        @empty ($user->authenticatable_type)
                            <th class="fit">Edit</th>
                            <th class="fit">Delete</th>
                        @endempty
                    </tr>
                </thead>
                <tbody>
                    @forelse ($currencies->data as $currency)
                        <tr>
                            <td>{{ $currency->name }}</td>
                            <td>{{ $currency->code }}</td>
                            @empty ($user->authenticatable_type)
                                <td class="text-center">
                                    <a href="{{ route('currencies.edit', ['id' => $currency->id]) }}" class="action-btn">
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                    </a>
                                </td>
                                <td class="text-center">
                                    <form action="{{ route('currencies.destroy', [ 'id' => $currency->id]) }}" method="post">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button type="submit" class="action-btn table-btn-destroy">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                                        </button>
                                    </form>
                                </td>
                            @endempty
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4">No data</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
            @if (isset($currencies->meta))
                {!! Component::pagination(
                    20,
                    $currencies->meta->pagination->current_page,
                    $currencies->meta->pagination->total_pages,
                    $params
                ) !!}
            @endif

        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script type="text/javascript">
        window.addEventListener('load', function() {
            EXP.initConfirmButton({
                selector: 'button.table-btn-destroy',
                title: "Delete currency",
                msg: "This action is going to delete the current currency.",
                callback: function(btn) {
                    $(btn).parent().submit();
                }
            });
        });
    </script>
@endsection
