@extends('layouts.app')

@if ($formType === Constant::FORM_CREATE)
    @section('title', 'Create currency')
@elseif($formType === Constant::FORM_EDIT)
    @section('title', 'Edit currency')
@elseif ($formType === Constant::FORM_SHOW)
    @section('title', 'View currency')
@endif

@section('content-header')
    @if ($formType === Constant::FORM_CREATE)
        <h1>Create currency</h1>
    @elseif ($formType === Constant::FORM_EDIT)
        <h1>Edit currency</h1>
    @elseif ($formType === Constant::FORM_SHOW)
        <h1>View currency</h1>
    @endif
@endsection

@section('content')
    <form action="{{ $formAction }}" method="POST" class="">
        {{ csrf_field() }}
        @if ($formType === Constant::FORM_EDIT)
            {{ method_field('PUT') }}
        @endif
        <div class="box">
            <div class="box-body">
                <input type="hidden" name="currency_id" value="{{ !empty($currency) ? $currency->id : '' }}">
                {{--  Name  --}}
                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    <label for="name" class="control-label label-line">Name</label>
                    <input type="text" class="form-control" name="name" id="name"
                    value="{{ old('name', !empty($currency) ? $currency->name : '') }}">
                    <span class="help-block">{{ $errors->first('name') }}</span>
                </div>

                {{--  Code  --}}
                <div class="form-group {{ $errors->has('code') ? 'has-error' : '' }}">
                    <label for="code" class="control-label label-line">Code</label>
                    <input type="text" class="form-control" name="code" id="code"
                    value="{{ old('code', !empty($currency) ? $currency->code : '') }}" minlength="3" maxlength="3">
                    <span class="help-block">{{ $errors->first('code') }}</span>
                </div>

                <strong class="title-block">Exchange rates</strong>
                {{--  Exchange rates  --}}
                <div class="form-group {{ $errors->has('exchange_rates') ? 'has-error' : '' }}">
                    @foreach ($currencies as $currency2)
                        @php
                        $exchangeR = "";
                        @endphp
                        @if (isset($currency->exchangeRates))
                            @if($currency->id == $currency2->id)
                                @continue
                            @endif
                            @foreach ($currency->exchangeRates->data as $cur)
                                @if ($cur->exchangeCurrency->data->id == $currency2->id)
                                    @php
                                    $exchangeR = $cur->amount;
                                    break;
                                    @endphp
                                @endif
                            @endforeach
                        @endif
                        <div class="form-group">
                            <label for="curr-{{ $currency2->id }}" class="control-label label-line">{{ $currency2->name }}</label>
                            <input type="number" class="form-control xrate" name="exchangeRate[{{ $currency2->id }}]" id="curr-{{ $currency2->id }}"
                            value="{{ !empty(old('exchangeRate'))? (old('exchangeRate')[$currency2->id] != null? old('exchangeRate')[$currency2->id] : '') : $exchangeR }}" step="any" min="0">
                        </div>
                    @endforeach
                    <span class="help-block">{{ $errors->first('exchange_rates') }}</span>
                </div>
            </div>

            <div class="box-footer">
                <a class="btn btn-default" href="{{ url()->current() != url()->previous() ? url()->previous() : route('currencies.index') }}">Cancel</a>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </div>
        {{--  Form buttons  --}}
    </form>
@endsection

@section('scripts')
    @parent
    <script>

    </script>
@endsection
