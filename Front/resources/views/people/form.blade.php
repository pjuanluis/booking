@extends('layouts.app')

@section('title')
    {{ $title }}
@endsection

@section('content-header')
    <h1>{{ $title }}</h1>
@endsection

@php
    $readOnly = ($formType === Constant::FORM_SHOW) ? 'readonly' : '';
@endphp

@section('content')
<form id="client-form" action="{{ $formType === Constant::FORM_SHOW ? '#' : $formAction }}" method="POST">
    {{ csrf_field() }}
    @if ($formType === Constant::FORM_EDIT)
        {{ method_field('PUT') }}
        <input type="hidden" name="updated_at" value="{{ $person->updated_at }}" />
    @endif
    <div class="box" style="">
        <div class="box-body">
            <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                <label for="first_name" class="control-label">First name*</label>
                <input id="first_name" type="text" class="form-control"
                    name="first_name" value="{{ old('first_name', isset($person)? $person->first_name : '') }}" {{ $readOnly }} required>
                <span class="help-block">{{ $errors->first('first_name') }}</span>
            </div>
            <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                <label for="last_name" class="control-label">Last name*</label>
                <input id="last_name" type="text" class="form-control"
                    name="last_name" value="{{ old('last_name', isset($person)? $person->last_name : '') }}" {{ $readOnly }} required>
                <span class="help-block">{{ $errors->first('last_name') }}</span>
            </div>
            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                <label for="email" class="control-label">Email*</label>
                <input id="email" type="email" class="form-control"
                    name="email" value="{{ old('email', isset($person)? $person->email : '') }}" {{ $readOnly }} required>
                <span class="help-block">{{ $errors->first('email') }}</span>
            </div>
            <div class="form-group {{ $errors->has('position') ? 'has-error' : '' }}">
                <label for="position" class="control-label">Position</label>
                <input id="position" type="text" class="form-control"
                    name="position" value="{{ old('position', isset($person)? $person->position : '') }}" {{ $readOnly }} >
                <span class="help-block">{{ $errors->first('position') }}</span>
            </div>
            <div class="form-group {{ $errors->has('abbreviation') ? 'has-error' : '' }}">
                <label for="abbreviation" class="control-label">Abbreviation</label>
                <input id="abbreviation" type="text" class="form-control" name="abbreviation" value="{{ old('abbreviation', isset($person)? $person->abbreviation : '') }}" {{ $readOnly }} />
                <span class="help-block">{{ $errors->first('abbreviation') }}</span>
            </div>
            @php
                $types = [
                    [
                        'name' => 'Spanish teacher',
                        'slug' => 'spanish_teacher'
                    ],
                    [
                        'name' => 'Surf instructor',
                        'slug' => 'surf_instructor'
                    ],
                    [
                        'name' => 'Volunteer responsible',
                        'slug' => 'volunteer_responsible'
                    ],
                ];
                $selectedType = old('type', isset($person)? $person->type : '');
            @endphp
            <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
                <label for="type" class="control-label">Type</label>
                <select id="type" class="form-control select2" name="type" {{ $readOnly == 'readonly'? 'disabled' : '' }} />
                    <option value="">--Select--</option>
                    @foreach ($types as $type)
                        <option value="{{ $type['slug'] }}" {{ $selectedType == $type['slug']? 'selected' : '' }}>{{ $type['name'] }}</option>
                    @endforeach
                </select>
                <span class="help-block">{{ $errors->first('type') }}</span>
            </div>
        </div>
        <div class="box-footer">
            <a class="btn btn-default" href="{{ url()->previous() }}">
                @if ($formType !== Constant::FORM_SHOW)
                    Cancel
                @else
                    Back
                @endif
            </a>
            @if ($formType !== Constant::FORM_SHOW)
                <button class="btn btn-primary" type="submit" name="action">Save</button>
            @else
                <a class="btn btn-primary" href="{{ route('people.edit', ['id' => $person->id]) }}">
                    Edit
                </a>
            @endif
        </div>
    </div>
</form>

@endsection
