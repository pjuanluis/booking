@extends('layouts.app')

@section('title')
    People
@endsection

@section('content-header')
    <h1>People</h1>
@endsection

@section('content')
<div class="box">
    <div class="box-header with-border">
        <a href="{{ route('people.create') }}" class="btn btn-primary pull-right">New people</a>
    </div>
    <div class="box-body">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Type</th>
                    <th class="text-center">View</th>
                    <th class="text-center">Edit</th>
                    <th class="text-center">Delete</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($people->data as $person)
                <tr>
                    <td>{{ $person->full_name }}</td>
                    <td>{{ $person->email }}</td>
                    <td>{{ ucfirst(str_replace('_', ' ', $person->type)) }}</td>
                    <td class="text-center fit">
                        <a class="action-btn" href="{{ route('people.show', ['id' => $person->id]) }}">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </a>
                    </td>
                    <td class="text-center fit">
                        <a class="action-btn" href="{{ route('people.edit', ['id' => $person->id]) }}">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                    </td>
                    <td class="text-center fit">
                        <form action="{{ route('people.destroy', [ 'id' => $person->id]) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="action-btn table-btn-destroy">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </button>
                        </form>
                    </td>
                </tr>
                @empty
                    <tr>
                        <td colspan="40">No data</td>
                    </tr>
                @endforelse
            </tbody>
        </table>

        {!! Component::pagination(
            10,
            $people->meta->pagination->current_page,
            $people->meta->pagination->total_pages,
            $params
            ) !!}
    </div>
</div>
<script type="text/javascript">
    window.addEventListener('load', function() {
        EXP.initConfirmButton({
            selector: 'button.table-btn-destroy',
            title: "Delete person",
            msg: "This action is going to delete the current person.",
            callback: function(btn) {
                $(btn).parent().submit();
            }
        });

        EXP.initConfirmButton({
            selector: 'button.table-btn-activate',
            title: "Activate/deactivate room type",
            msg: "This action is going to activate/deactivate the current room type.",
            callback: function(btn) {
                $(btn).parent().submit();
            }
        });
    });
</script>
@endsection
