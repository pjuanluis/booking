@extends('layouts.app')
@section('title') {{ $title }}
@endsection

@section('content-header')
<h1>{{ $title }}</h1>
@endsection
@php
    $readOnly = ($formType === Constant::FORM_SHOW) ? 'readonly' : '';
@endphp
@section('content')
<form id="client-form" action="{{ $formType === Constant::FORM_SHOW ? '#' : $formAction }}" method="POST">
    {{ csrf_field() }} @if ($formType === Constant::FORM_EDIT) {{ method_field('PUT') }}
    <input type="hidden" name="updated_at" value="{{ $residence->updated_at }}" /> @endif
    <div class="box" style="">
        <div class="box-body">
            @php
                $selectedHousing = old('housing_id', isset($residence)? $residence->housing->data->id : '');
            @endphp
            <div class="form-group {{ $errors->has('housing_id') ? 'has-error' : '' }}">
                <label for="housing_id" class="control-label">Housing*</label>
                <select name="housing_id" id="housing_id" class="form-control select2" {{ $readOnly == 'readonly'? 'disabled' : '' }}>
                    @foreach ( $housing->data as $h)
                        <option value="{{ $h->id }}" {{ $selectedHousing == $h->id? 'selected' : '' }}>{{ $h->name }}</option>
                    @endforeach
                </select>
                <span class="help-block">{{ $errors->first('housing_id') }}</span>
            </div>
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name" class="control-label">Name*</label>
                <input id="name" type="text" class="form-control" name="name" value="{{ old('name', isset($residence)? $residence->name : '') }}"
                    {{ $readOnly }} required>
                <span class="help-block">{{ $errors->first('name') }}</span>
            </div>
            <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                <label for="address" class="control-label">Address*</label>
                <input id="level" type="text" class="form-control" name="address" value="{{ old('address', isset($residence)? $residence->address : '') }}"
                    {{ $readOnly }} />
                <span class="help-block">{{ $errors->first('address') }}</span>
            </div>
        </div>
        <div class="box-footer">
            <a class="btn btn-default" href="{{ url()->previous() }}">
                @if ($formType !== Constant::FORM_SHOW)
                    Cancel
                @else
                    Back
                @endif
            </a>
            @if ($formType !== Constant::FORM_SHOW)
                <button class="btn btn-primary" type="submit" name="action">Save</button>
            @else
                <a href="{{ route('residences.edit', ['id' => $residence->id]) }}" class="btn btn-primary">Edit</a>
            @endif
        </div>
    </div>
</form>
@endsection
