@extends('layouts.app')

@section('title')
    Residences
@endsection

@section('content-header')
    <h1>Residences</h1>
@endsection

@section('content')
<div class="box">
    <div class="box-header with-border">
        <a href="{{ route('residences.create') }}" class="btn btn-primary pull-right">New residence</a>
    </div>
    {{-- Este formulario sirver para ordenar los registros, no borrar --}}
    <form id="form-sort" action="index.html" method="post">
        {{ csrf_field() }}
    </form>
    <div class="box-body">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Address</th>
                    <th class="text-center">Rooms</th>
                    <th class="text-center">View</th>
                    <th class="text-center">Edit</th>
                    <th class="text-center">Delete</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($residences->data as $residence)
                <tr>
                    <td>{{ $residence->name }}</td>
                    <td>{{ $residence->address }}</td>
                    <td class="text-center">
                        <a class="action-btn" href="{{ route('rooms.index', ['residence' => $residence->id]) }}">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </a>
                    </td>
                    <td class="text-center fit">
                        <a class="action-btn" href="{{ route('residences.show', ['id' => $residence->id]) }}">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </a>
                    </td>
                    <td class="text-center fit">
                        <a class="action-btn" href="{{ route('residences.edit', ['id' => $residence->id]) }}">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                    </td>
                    <td class="text-center fit">
                        <form action="{{ route('residences.destroy', [ 'id' => $residence->id]) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="action-btn table-btn-destroy">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </button>
                        </form>
                    </td>

                </tr>
                @empty
                    <tr>
                        <td colspan="40">No data</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    @if (Request::get('housing'))
        <div class="box-footer">
            <a href="{{ route('housing.index') }}" class="btn btn-default">Back</a>
        </div>
    @endif
</div>
<script type="text/javascript">
    window.addEventListener('load', function() {
        EXP.initConfirmButton({
            selector: 'button.table-btn-destroy',
            title: "Delete residence",
            msg: "This action is going to delete the current residence.",
            callback: function(btn) {
                $(btn).parent().submit();
            }
        });
    });
</script>
@endsection
