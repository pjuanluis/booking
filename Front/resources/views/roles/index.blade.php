@extends('layouts.app')

@section('title', $title)

@section('content-header')
    <h1>{{ $title }}</h1>
@endsection

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <a href="{{ route('roles.create') }}" class="btn btn-primary pull-right">New role</a>
        </div>
        <div class="box-body">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Role</th>
                        <th class="text-center fit">Status</th>
                        <th class="text-center fit">Edit</th>
                        <th class="text-center fit">Delete</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($roles->data as $role)
                        <tr>
                            <td>{{ $role->name }}</td>
                            <td class="text-center fit">
                                <form action="{{ route('roles.activate', [ 'id' => $role->id]) }}" method="post">
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}
                                    <button type="submit" class="action-btn table-btn-activate" style="opacity: 1;" title="{{ $role->is_active == 1? 'Deactivate' : 'Activate' }}">
                                        <i class="fa fa-flag {{ $role->is_active == 1 ? 'on' : 'off' }}" aria-hidden="true"></i>
                                    </button>
                                </form>
                            </td>
                            <td class="text-center fit">
                                <a class="action-btn" href="{{ route('roles.edit', ['id' => $role->id]) }}">
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </a>
                            </td>
                            <td class="text-center fit">
                                <form action="{{ route('roles.destroy', [ 'id' => $role->id]) }}" method="post">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button type="submit" class="action-btn table-btn-destroy">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="100">No data available</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script>
            $(function() {
                EXP.initConfirmButton({
                    selector: 'button.table-btn-activate',
                    title: "Activate/deactivate role",
                    msg: "This action is going to activate/deactivate the current role.",
                    callback: function(btn) {
                        $(btn).parent().submit();
                    }
                });
                
                EXP.initConfirmButton({
                    selector: 'button.table-btn-destroy',
                    title: "Delete role",
                    msg: "This action is going to delete the current role.",
                    callback: function(btn) {
                        $(btn).parent().submit();
                    }
                });
            });
    </script>
@endsection