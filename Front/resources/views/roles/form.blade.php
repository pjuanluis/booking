@extends('layouts.app')

@section('title', $title)

@section('content-header')
    <h1>{{ $title }}</h1>
@endsection

@section('content')
    <form action="{{ $formAction }}" method="post">
        {{ csrf_field() }}
        @if ($formType === Constant::FORM_EDIT)
            {{ method_field('PUT') }}
            <input type="hidden" name="updated_at" value="{{ $role->data->updated_at }}" />
        @endif
        <div class="box">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label for="name" class="control-label">Role name</label>
                            <input type="text" name="name" id="name" class="form-control" required
                                value="{{ old('name') ? old('name') : (isset($role->data->name) ? $role->data->name : '') }}">
                            <span class="help-block">{{ $errors->first('name') }}</span>
                        </div>
                    </div>
                </div>
                <hr>
            </div>

            <div class="box-body">
                <div class="row">
                    <div class="col-xs-12">
                        @php
                            if (old('accesses')) {
                                $accessesTo = old('accesses');
                            }
                            $moduleExist = false;
                        @endphp
                        @forelse ($modules->data as $module)
                            @php
                                $moduleExist = array_key_exists($module->id, $accessesTo);
                            @endphp
                            <div>
                                <strong>{{ $module->name }}</strong>
                                <ul class="list-inline">
                                    @foreach ($module->permissions->data as $permission)
                                        <li>
                                            <div class="checkbox iCheck">
                                                <label>
                                                    <input type="checkbox" id="{{ $permission->id }}" name="accesses[{{ $module->id }}][]" value="{{ $permission->id }}"
                                                        {{ $moduleExist ? (in_array($permission->id, $accessesTo[$module->id]) ? 'checked' : '') : '' }}>
                                                    {{ $permission->name }}
                                                </label>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @empty
                            <span class="no-data">No data available.</span>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>

        <a href="{{ url()->previous() }}" type="cancel" class="btn btn-default">Cancel</a>
        <button type="submit" class="btn btn-primary">Save</button>
</form>
 @endsection