@extends('layouts.app')

@section('title')
    Petty cash
@endsection

@section('content-header')
    <h1>Petty Cash</h1>
@endsection

@section('content')

<div class="box" style="">
    <div class="box-header with-border">
        <a href="{{ route('consumption-centers.create') }}" class="btn btn-primary pull-right">New Petty cash</a>
    </div>
    <div class="box-body table-responsive no-padding">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Petty cash</th>
                    <th>Balance</th>
                    <th class="text-center fit">View</th>
                    <th class="text-center fit">Edit</th>
                    <th class="text-center fit">Delete</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($consumptionCenters->data as $consumptionCenter)
                <tr>
                    <td>{{ $consumptionCenter->name }}</td>
                    <td>{{ $consumptionCenter->balance }}</td>
                    <td class="text-center">
                        <a href="{{ route('petty-cashes.index', ['petty-cash' => $consumptionCenter->id]) }}" class="action-btn">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a href="{{ route('consumption-centers.edit', ['id' => $consumptionCenter->id]) }}" class="action-btn">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <form action="{{ route('consumption-centers.destroy', [ 'id' => $consumptionCenter->id]) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="action-btn table-btn-destroy">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {!! Component::pagination(
            10,
            $consumptionCenters->meta->pagination->current_page,
            $consumptionCenters->meta->pagination->total_pages,
            $params
        ) !!}
    </div>
</div>
<script type="text/javascript">
    window.addEventListener('load', function() {
        EXP.initConfirmButton({
            selector: 'button.table-btn-destroy',
            title: "Delete petty cash",
            msg: "This action is going to delete the current petty cash.",
            callback: function(btn) {
                $(btn).parent().submit();
            }
        });
    });
</script>
@endsection
