@extends('layouts.app')

@section('title')
    {{ getTitle($formType, $isDeposit) }}
@endsection

@section('content-header')
    <h1>
        {{ getTitle($formType, $isDeposit) }}
    </h1>
@endsection

@php
    function getTitle($formType, $isDeposit) {
        if ($formType === Constant::FORM_CREATE) {
            return "New " . ($isDeposit == 1? 'Deposit' : 'Withdrawal');
        } elseif ($formType === Constant::FORM_EDIT) {
            return "Edit " . ($isDeposit == 1? 'Deposit' : 'Withdrawal');
        } elseif ($formType === Constant::FORM_SHOW) {
            return "Show " . ($isDeposit == 1? 'Deposit' : 'Withdrawal');
        }
    }

    if ($formType === Constant::FORM_CREATE) {
        $user = Auth::user();
        $responsible = $user->first_name . ' ' . $user->last_name;
    } else {
        $responsible = !empty($move->user->data->full_name)? $move->user->data->full_name : '--';
    }
@endphp

@php
    $readOnly = ($formType === Constant::FORM_SHOW) ? 'readonly' : '';
@endphp

@section('content')
<form action="{{ $formType === Constant::FORM_SHOW ? '#' : $formAction }}" method="POST" enctype="multipart/form-data">
    {{ csrf_field() }}
    @if ($formType === Constant::FORM_EDIT)
        {{ method_field('PUT') }}
        <input type="hidden" name="updated_at" value="{{ $move->updated_at }}" />
    @endif
    <input type="hidden" name="is_deposit" value="{{ $isDeposit }}">
    <div class="box" style="">
        <div class="box-body">
            <div class="form-group {{ $errors->has('consumption_center_id') ? 'has-error' : '' }}">
                <label class="control-label">Petty cash</label>
                <input type="text" class="form-control" value="{{ $pettyCash->data->name }}" readonly style="max-width: 500px;">
                <input type="hidden" name="consumption_center_id" value="{{ $pettyCash->data->id }}">
                <span class="help-block">{{ $errors->first('consumption_center_id') }}</span>
            </div>
            <div class="form-group">
                <label for="responsible" class="control-label">Responsible</label>
                <input id="responsible" type="text" class="form-control" value="{{ $responsible }}" readonly style="max-width: 500px;">
            </div>
            <div class="form-group {{ $errors->has('amount') ? 'has-error' : '' }}">
                <label for="amount" class="control-label">Amount*</label>
                <input id="amount" type="text" class="form-control"
                    name="amount" value="{{ old('amount', isset($move)? $move->amount : '') }}" {{ $readOnly }} style="max-width: 500px;">
                <span class="help-block">{{ $errors->first('amount') }}</span>
            </div>
            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="description" class="control-label">Description*</label>
                <textarea name="description" class="form-control" rows="3" cols="80" {{ $readOnly }} style="max-width: 500px;">{{ old('description', isset($move)? $move->description : '') }}</textarea>
                <span class="help-block">{{ $errors->first('description') }}</span>
            </div>
            @if ($isDeposit == 1)
                <div class="form-group {{ $errors->has('file') ? 'has-error' : '' }}">
                    @isset($move->file->data->path)
                        <a href="{{ $move->file->data->path }}" target="_blank" class="btn btn-default">
                            <i class="fa fa-file-image-o" aria-hidden="true"></i>
                        </a>
                    @endisset
                    @if ($formType !== Constant::FORM_SHOW)
                        <label for="file">
                            <span class="btn btn-primary">Upload voucher</span>
                            <span></span>
                            <input type="file" name="file" id="file" style="display: none;" accept="image/*">
                        </label>
                        <span class="help-block">
                            @if ($errors->has('file'))
                                {{ str_replace('file', 'voucher', $errors->first('file')) }}
                            @else
                                Please, use image(JPG, PNG or GIF) less than 1 MB.
                            @endif
                        </span>
                    @endif
                </div>
            @endif
        </div>
        <div class="box-footer">
            <a class="btn btn-default" href="{{ url()->previous() }}">
                @if ($formType !== Constant::FORM_SHOW)
                    Cancel
                @else
                    Back
                @endif
            </a>
            @if ($formType !== Constant::FORM_SHOW)
                <button class="btn btn-primary" type="submit" name="action">Save</button>
            @endif
        </div>
    </div>
</form>
@endsection

@section('scripts')
    @parent
    <script type="text/javascript">
        $(function() {
            // Mostrar el nombre del archivo seleccionado
            function archivo(evt) {
                var file = evt.target.files[0];
                console.log(file);
                $(this).prev().text(file.name);
            }
            $.each($('input[type=file]'), function(i, element) {
                element.addEventListener('change', archivo, false);
            });
        })
    </script>
@endsection
