@extends('layouts.app')

@section('title')
    Petty cash
@endsection

@section('content-header')
    <h1>Petty cash</h1>
@endsection

@section('content')
@php
    $id = "";
    $title = "List petty cash";
    $moves = null;
    if (isset($consumptionCenter->data->pettyCashes)) {
        $moves = $consumptionCenter->data->pettyCashes;
        $id = $consumptionCenter->data->id;
        $title = "{$consumptionCenter->data->name} petty cash";
    } else {
        $moves = $consumptionCenter;
    }
@endphp
<h2 style="font-size: 20px;">{{ $title }}</h2>
<div class="box" style="">
        <div class="box-header with-border">
            @if ($id)
                <a href="{{ route('petty-cashes.create', ['is-deposit' => 1, 'petty-cash' => $id]) }}" class="btn btn-primary pull-right" style="margin-left: 5px;">New Deposit</a>
                <a href="{{ route('petty-cashes.create', ['is-deposit' => 0, 'petty-cash' => $id]) }}" class="btn btn-primary pull-right" style="margin-left: 5px;">New Withdrawal</a>
            @endif
            @if ($moves->data)
                <button type="button" class="btn btn-primary pull-right" id="btn-export-to-csv">Export to CSV</button>
            @endif
        </div>
    <div class="box-body table-responsive no-padding">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Description</th>
                    <th>Responsible</th>
                    <th class="text-center fit">File</th>
                    <th class="text-center fit">Deposit</th>
                    <th class="text-center fit">Withdrawal</th>
                    <th class="text-center fit">Balance</th>
                    <th class="text-center fit">View</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($moves->data as $move)
                    <tr>
                        @php
                            $date = new Carbon\Carbon($move->date);
                            // Cambiar la zona horaria
                            $date->tz = 'America/Mexico_City';
                        @endphp
                        <td>{{ $date->format('d-m-Y / H:i:s') }}</td>
                        <td>{{ $move->description }}</td>
                        <td>{{ !empty($move->user->data->full_name)? $move->user->data->full_name : '--' }}</td>
                        <td class="text-center">
                            @isset($move->file->data->path)
                                <a href="{{ $move->file->data->path }}" target="_blank">
                                    <i class="fa fa-file-image-o" aria-hidden="true"></i>
                                </a>
                            @else
                                --
                            @endisset
                        </td>
                        <td class="text-center">{{ $move->is_deposit === 1? $move->amount : '' }}</td>
                        <td class="text-center">{{ $move->is_deposit === 0? $move->amount : '' }}</td>
                        <td class="text-center">{{ $move->balance }}</td>
                        <td class="text-center">
                            <a href="{{ route('petty-cashes.show', ['id' => $move->id]) }}" class="action-btn">
                                <i class="fa fa-search" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="8">No data</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
        {!! Component::pagination(
            10,
            $moves->meta->pagination->current_page,
            $moves->meta->pagination->total_pages,
            $params
        ) !!}
    </div>
    <div class="box-footer">
        <a href="{{ route('petty-cashes.index') }}?show=consumption-centers" class="btn btn-primary">Back</a>
    </div>
</div>
@endsection
@if ($moves->data)
    @section('scripts')
        @parent
        <script>
        $(function() {
            function initExportToCsv() {
                EXP.Downloader({
                    el: '#btn-export-to-csv',
                    url: '{{ route('petty-cashes.csv', ['petty-cash' => $id]) }}',
                    filename: 'petty-cash.csv',
                    mimetype: 'application/csv',
                }, function(request) {
                    request.send("_token={{ csrf_token() }}");
                });
            }

            initExportToCsv();
        });
        </script>
    @endsection
@endif
