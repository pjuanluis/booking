@extends('layouts.app')

@section('title')
    {{ $title }}
@endsection

@section('content-header')
    <h1>{{ $title }}</h1>
@endsection

@php
    $readOnly = ($formType === Constant::FORM_SHOW) ? 'readonly' : '';
@endphp

@section('content')
<form id="client-form" action="{{ $formType === Constant::FORM_SHOW ? '#' : $formAction }}" method="POST">
    {{ csrf_field() }}
    @if ($formType === Constant::FORM_EDIT)
        {{ method_field('PUT') }}
        <input type="hidden" name="updated_at" value="{{ $consumptionCenter->updated_at }}" />
    @endif
    <div class="box" style="">
        <div class="box-body">
            <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                <label for="name" class="control-label">Name*</label>
                <input id="name" type="text" class="form-control"
                    name="name" value="{{ old('name', isset($consumptionCenter)? $consumptionCenter->name : '') }}" {{ $readOnly }} required>
                <span class="help-block">{{ str_replace('slug', 'name', $errors->first('slug')) }}</span>
            </div>
        </div>
        <div class="box-footer">
            <a class="btn btn-default" href="{{ url()->previous() }}">
                @if ($formType !== Constant::FORM_SHOW)
                    Cancel
                @else
                    Back
                @endif
            </a>
            @if ($formType !== Constant::FORM_SHOW)
                <button class="btn btn-primary" type="submit" name="action">Save</button>
            @else
                <a class="btn btn-primary" href="{{ route('consumption-centers.edit', ['id' => $consumptionCenter->id]) }}">
                    Edit
                </a>
            @endif
        </div>
    </div>

</form>

@endsection
