@extends('layouts.app')

@section('title')
    Consumption centers
@endsection

@section('content-header')
    <h1>Consumption centers</h1>
@endsection

@section('content')
<div class="box">
    <div class="box-header with-border">
        <a href="{{ route('consumption-centers.create') }}" class="btn btn-primary pull-right">New consumption center</a>
    </div>
    <div class="box-body">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Name</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">View</th>
                    <th class="text-center">Edit</th>
                    <th class="text-center">Delete</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($consumptionCenters->data as $consumptionCenter)
                <tr>
                    <td>{{ $consumptionCenter->name }}</td>
                    <td class="text-center fit">
                        <form action="{{ route('consumption-centers.activate', [ 'id' => $consumptionCenter->id]) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <button type="submit" class="action-btn table-btn-activate" style="opacity: 1;" title="{{ $consumptionCenter->is_active == 1? 'Deactivate' : 'Activate' }}">
                                <i class="fa fa-flag {{ $consumptionCenter->is_active == 1 ? 'on' : 'off' }}" aria-hidden="true"></i>
                            </button>
                        </form>
                    </td>
                    <td class="text-center fit">
                        <a class="action-btn" href="{{ route('consumption-centers.show', ['id' => $consumptionCenter->id]) }}">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </a>
                    </td>
                    <td class="text-center fit">
                        <a class="action-btn" href="{{ route('consumption-centers.edit', ['id' => $consumptionCenter->id]) }}">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                    </td>
                    <td class="text-center fit">
                        <form action="{{ route('consumption-centers.destroy', [ 'id' => $consumptionCenter->id]) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="action-btn table-btn-destroy">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </button>
                        </form>
                    </td>
                </tr>
                @empty
                    <tr>
                        <td colspan="40">No data</td>
                    </tr>
                @endforelse
            </tbody>
        </table>

        {!! Component::pagination(
            10,
            $consumptionCenters->meta->pagination->current_page,
            $consumptionCenters->meta->pagination->total_pages,
            $params
            ) !!}
    </div>
</div>
<script type="text/javascript">
    window.addEventListener('load', function() {
        EXP.initConfirmButton({
            selector: 'button.table-btn-destroy',
            title: "Delete consumption center",
            msg: "This action is going to delete the current consumption center.",
            callback: function(btn) {
                $(btn).parent().submit();
            }
        });

        EXP.initConfirmButton({
            selector: 'button.table-btn-activate',
            title: "Activate/deactivate consumption center",
            msg: "This action is going to activate/deactivate the current consumption center.",
            callback: function(btn) {
                $(btn).parent().submit();
            }
        });
    });
</script>
@endsection
