@extends('layouts.app')

@section('title')
    Countries
@endsection

@section('content-header')
    <h1>Countries</h1>
@endsection

@section('content')
<div class="box">
    <div class="box-header with-border">
        <a href="{{ route('countries.create') }}" class="btn btn-primary pull-right">New country</a>
    </div>
    {{-- Este formulario sirver para ordenar los registros, no borrar --}}
    <form id="form-sort" action="index.html" method="post">
        {{ csrf_field() }}
    </form>
    <div class="box-body">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Name</th>
                    <th class="text-center" colspan="2">Order</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">View</th>
                    <th class="text-center">Edit</th>
                    <th class="text-center">Delete</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($countries->data as $country)
                <tr>
                    <td>{{ $country->name }}</td>
                    <td class="text-center fit">
                        <button type="button" class="action-btn table-btn btn-sort {{ $loop->first ? 'disabled' : '' }}"
                        @if(!$loop->first)
                        data-form-target="#form-sort" data-form-method="PUT"
                        data-form-action="{{ route('countries.sort', ['country' => $country->id]) }}?dir=up"
                        @endif>
                        <i class="fa fa-arrow-up" aria-hidden="true"></i>
                        </button>
                        </td>
                    <td class="text-center fit">
                        <button type="button" class="action-btn table-btn btn-sort {{ $loop->last ? 'disabled' : '' }}"
                        @if(!$loop->last)
                        data-form-target="#form-sort" data-form-method="PUT"
                        data-form-action="{{ route('countries.sort', ['country' => $country->id]) }}?dir=down"
                        @endif>
                        <i class="fa fa-arrow-down" aria-hidden="true"></i>
                        </button>
                    </td>
                    <td class="text-center fit">
                        <form action="{{ route('countries.activate', [ 'id' => $country->id]) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <button type="submit" class="action-btn table-btn-activate" style="opacity: 1;" title="{{ $country->is_active == 1? 'Deactivate' : 'Activate' }}">
                                <i class="fa fa-flag {{ $country->is_active == 1 ? 'on' : 'off' }}" aria-hidden="true"></i>
                            </button>
                        </form>
                    </td>
                    <td class="text-center fit">
                        <a class="action-btn" href="{{ route('countries.show', ['id' => $country->id]) }}">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </a>
                    </td>
                    <td class="text-center fit">
                        <a class="action-btn" href="{{ route('countries.edit', ['id' => $country->id]) }}">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                    </td>
                    <td class="text-center fit">
                        <form action="{{ route('countries.destroy', [ 'id' => $country->id]) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="action-btn table-btn-destroy">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </button>
                        </form>
                    </td>

                </tr>
                @empty
                    <tr>
                        <td colspan="40">No data</td>
                    </tr>
                @endforelse
            </tbody>
        </table>

        {!! Component::pagination(
            10,
            $countries->meta->pagination->current_page,
            $countries->meta->pagination->total_pages,
            $params
            ) !!}
    </div>
</div>
<script type="text/javascript">
    window.addEventListener('load', function() {
        EXP.initConfirmButton({
            selector: 'button.table-btn-destroy',
            title: "Delete country",
            msg: "This action is going to delete the current country.",
            callback: function(btn) {
                $(btn).parent().submit();
            }
        });

        EXP.initConfirmButton({
            selector: 'button.table-btn-activate',
            title: "Activate/deactivate country",
            msg: "This action is going to activate/deactivate the current country.",
            callback: function(btn) {
                $(btn).parent().submit();
            }
        });

        $('.btn-sort').click(function(e) {
            var form = $(e.currentTarget.getAttribute('data-form-target'));
            form.attr('action', e.currentTarget.getAttribute('data-form-action'));
            if (e.currentTarget.getAttribute('data-form-method') && e.currentTarget.getAttribute('data-form-action')) {
            form.append($('<input>', {
            name: '_method',
            type: 'hidden',
            value: e.currentTarget.getAttribute('data-form-method')
            }));
            }
            form.submit();
        });
    });
</script>
@endsection
