<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Report from {{ $from }} to {{ $to }}</title>
        <style type="text/css">
            * {
                padding: 0;
                margin: 0;
            }
            .main {
                margin: 15px;
                padding: 10px;
            }
            .img {
                max-width: 100%;
                display: block;
            }
            .graph-container {
                margin-top: 15px;
            }
        </style>
    </head>
    <body>
        <div class="main">
            <h1>Report from {{ $from }} to {{ $to }}</h1>
            <div class="graph-container">
                <h2>Income per month</h2>
                <img class="img" src="{{ $g1 }}" alt="Income per month">
            </div>
            <div class="graph-container">
                <h2>Bookings per month</h2>
                <img class="img" src="{{ $g2 }}" alt="Bookings per month">
            </div>
        </div>
    </body>
</html>
