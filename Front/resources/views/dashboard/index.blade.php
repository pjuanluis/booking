@extends('layouts.app')

@section('title')
    Dashboard
@endsection

@section('content-header')
    <h1 class="">DASHBOARD</h1>
@endsection

@section('content')
    <style media="screen">
        .max-width-70 {
            max-width: 70%;
        }

        .mb-20 {
            margin-bottom: 20px;
        }

        .data-container {
            margin-bottom: 20px;
        }

        .data-title, .data-value {
            text-align: center;
            display: block;
            width: 100%;
            font-weight: bold;
        }

        .data-title {
            font-size: 2rem;
            text-transform: uppercase;
            color: #6978AA;
            border-bottom: 2px solid #e3e7f2;
        }

        .data-value {
            font-size: 4rem;
            color: #3f5ab0;
        }

        .data-value__small {
            font-size: 3rem;
            display: inline-block;
        }

        @media screen and (max-width: 767px) {
            .max-width-70 {
                max-width: 100%;
            }
        }
    </style>
    <div class="box">
        <div class="box-header mb-20">
            <h2 class="h3">INCOME</h2>
        </div>
        <div class="box-body">
            <div class="container-fluid max-width-70" style="margin-bottom: 15px;">
                <div class="row data-container">
                    <div class="col-md-3">
                        <p>
                            @php
                                $amountLastMonth = $dataDashboard->data->income->last_month? $dataDashboard->data->income->last_month->amount : 0;
                            @endphp
                            <span class="data-title">last month (mxn)</span>
                            <span class="data-value">
                                {{ '$' . ($amountLastMonth !== 0 ? number_format($amountLastMonth, 2) : $amountLastMonth) }}
                            </span>
                        </p>
                    </div>
                    <div class="col-md-offset-6 col-md-3">
                        <p>
                            <span class="data-title">average (mxn)</span>
                            <span class="data-value">
                                @php
                                    $average = $dataDashboard->data->income->average;
                                @endphp
                                {{ '$' . ($average !== 0 ? number_format($average, 2) : $average) }}
                            </span>
                        </p>
                    </div>
                </div>
                <canvas id="myChart"></canvas>
            </div>
        </div>
    </div>
    <div class="box">
        <div class="box-header mb-20">
            <h2 class="h3">BOOKINGS THIS MONTH</h2>
        </div>
        <div class="box-body">
            <div class="container-fluid max-width-70">
                <div class="row data-container">
                    <div class="col-md-3">
                        <p>
                            @php
                                $online = $dataDashboard->data->bookings->current_month? $dataDashboard->data->bookings->current_month->data->origin->online : 0;
                                $onlineLM = $dataDashboard->data->bookings->last_month? $dataDashboard->data->bookings->last_month->data->origin->online : 0;
                            @endphp
                            <span class="data-title">online</span>
                            <span class="data-value">
                                {{ $online }} <span class="data-value__small">({{ $onlineLM }})</span>
                            </span>
                        </p>
                    </div>
                    <div class="col-md-3">
                        <p>
                            @php
                                $paid = $dataDashboard->data->bookings->current_month? $dataDashboard->data->bookings->current_month->data->paid_fee : 0;
                                $paidLM = $dataDashboard->data->bookings->last_month? $dataDashboard->data->bookings->last_month->data->paid_fee : 0;
                            @endphp
                            <span class="data-title">Fee and payment</span>
                            <span class="data-value">
                                {{ $paid }} <span class="data-value__small">({{ $paidLM }})</span>
                            </span>
                        </p>
                    </div>
                    <div class="col-md-3">
                        <p>
                            @php
                                $agency = $dataDashboard->data->bookings->current_month? $dataDashboard->data->bookings->current_month->data->origin->agency : 0;
                                $agencyLM = $dataDashboard->data->bookings->last_month? $dataDashboard->data->bookings->last_month->data->origin->agency : 0;
                            @endphp
                            <span class="data-title">agency</span>
                            <span class="data-value">
                                {{ $agency }} <span class="data-value__small">({{ $agencyLM }})</span>
                            </span>
                        </p>
                    </div>
                    <div class="col-md-3">
                        <p>
                            @php
                                $walkIn = $dataDashboard->data->bookings->current_month? $dataDashboard->data->bookings->current_month->data->origin->walk_in : 0;
                                $walkInLM = $dataDashboard->data->bookings->last_month? $dataDashboard->data->bookings->last_month->data->origin->walk_in : 0;
                            @endphp
                            <span class="data-title">walk-in</span>
                            <p class="data-value">
                                {{ $walkIn }}
                                <span class="data-value__small">({{ $walkInLM }})</span>
                            </p>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script type="text/javascript">
    var ctx = $("#myChart");
    var ctx2 = $("#myChart2");
    var labels = ['J', 'F', 'M', 'A', 'M', 'J', 'J', 'A', 'S', 'O', 'N', 'D'];
    var income = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    var totalBooking = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    var paidBooking = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

    @foreach ($dataDashboard->data->income->by_month as $key => $data)
        @php
            $numberMonth = $data->month->number
        @endphp
        income[{{ $numberMonth - 1 }}] = {{ $data->amount }};
    @endforeach

    var scales = {
        yAxes: [{
            gridLines: {
                display: true,
                color: 'rgba(0, 0, 0, 0.06)',
            }
        }],
        xAxes: [{
            gridLines: {
                display: false,
            }
        }],
    };


    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: labels,
            datasets: [
                {
                    label: 'Income(MXN)',
                    data: income,
                    backgroundColor: '#3F5AB0',

                }
            ]
        },
        options: {
            legend: {
                display: false,
            },
            scales: scales,
        }
    });
    </script>
@endsection
