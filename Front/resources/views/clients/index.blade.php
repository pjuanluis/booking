@extends('layouts.app')

@section('title')
    Clients
@endsection

@section('content-header')
    <h1>Clients</h1>
@endsection

@section('content')

<div class="box" style="">
    @php
        $user = Auth::user();
    @endphp
    <div class="box-header with-border">
    @empty ($user->authenticatable_type)
        <a href="{{ route('clients.create') }}" class="btn btn-primary pull-right">New client</a>
    @endempty
    </div>
    <div class="box-body">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2 class="panel-title h3">Filter by</h2>
            </div>
            <div class="panel-body">
                <form action="{{ url()->full() }}" method="get">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            @php
                                $clientSelected = Request::get('name', '');
                            @endphp
                            <label>Full name</label>
                            <select class="form-control select2" name="name">
                                <option value="">-- Select client name --</option>
                                @foreach ($clientsWithoutPagination->data as $client)
                                    <option value="{{ $client->full_name }}" {{ $clientSelected == $client->full_name? 'selected' : '' }}>{{ $client->full_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary" name="is_filter" value="true"><i class="fa fa-filter"></i> Filter</button>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
        <div class="" style="margin: 10px 0;">
            <button type="button" class="btn btn-primary pull-right" id="btn-export-to-csv">
                <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                Export to CSV
            </button>
            <div class="clearfix"></div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-hover datatable" data-url-booking-index="{{ route('bookings.index') }}">
                <thead>
                    <tr>
                        <th>First name</th>
                        <th>Last name</th>
                        <th>Email</th>
                        <th class="text-center fit">Commands</th>
                        <th class="text-center fit">Credential</th>
                        <th class="text-center fit">View</th>
                        <th class="text-center fit">Edit</th>
                        @if ($user->authenticatable_type !== 'App\Client')
                            <th class="text-center fit">Delete</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach ($clients->data as $client)
                    <tr data-client-id="{{ $client->id }}" data-url-ajax="{{ route('bookings.index') }}">
                        <td>{{ $client->first_name }}</td>
                        <td>{{ $client->last_name }}</td>
                        <td>{{ $client->email }}</td>
                        <td class="text-center details-control action-btn fit" style="cursor: pointer;">
                            <i class="fa fa-address-card" aria-hidden="true"></i>
                        </td>
                        <td class="text-center" style="cursor: pointer;">
                            <a href="{{ route('clients.credential', ['id' => $client->id]) }}" class="action-btn">
                                <i class="fa fa-id-card-o" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a href="{{ route('clients.show', ['id' => $client->id]) }}" class="action-btn">
                                <i class="fa fa-search" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td class="text-center">
                            <a href="{{ route('clients.edit', ['id' => $client->id]) }}" class="action-btn">
                                <i class="fa fa-pencil" aria-hidden="true"></i>
                            </a>
                        </td>
                        @if ($user->authenticatable_type !== 'App\Client')
                            <td class="text-center">
                                <form action="{{ route('clients.destroy', [ 'id' => $client->id]) }}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button type="submit" class="action-btn table-btn-destroy">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </button>
                                </form>
                            </td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        {!! Component::pagination(
            10,
            $clients->meta->pagination->current_page,
            $clients->meta->pagination->total_pages,
            $params
        ) !!}
    </div>
</div>
@endsection

@section('scripts')
    @parent
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/sprintf.min.js') }}"></script>
    <script>
        function format(commands, urlBookingIndex) {
            var ret = '<div style="padding: 16px;">';
            var totalCommands = commands.data.length;

            if (totalCommands == 0) {
                ret += 'No data available!'
            }

            // Add packages name
            _.forEach(commands.data, function(command, k) {
                ret += sprintf(
                    '<a href="%s/%s/edit" target="_blank" class="text-color-black">- Command: %s</a>%s',
                    urlBookingIndex,
                    command.id,
                    command.booking_code,
                    (k < (commands.data.length - 1)) ? '<hr>' : ''
                );
            });

            return ret + '</div>';
        }

        $(function() {
            var $dt = $('.datatable');
            var dt = $dt.DataTable({
                paging: false,
                searching: false,
                ordering: false,
                info: false
            });
            $dt.find('tbody').on( 'click', 'tr td.details-control', function () {
                var tr = $(this).closest('tr');
                var row = dt.row(tr);
                var clientId = 0;
                var urlAjax = '';
                var urlBookingIndex = '';
                var commands = null;

                if ( row.child.isShown() ) {
                    tr.removeClass( 'details' );
                    row.child.hide();
                } else {
                    clientId = tr.data('clientId');
                    urlAjax = tr.data('urlAjax');
                    urlBookingIndex = tr.closest('table').data('urlBookingIndex');
                    $.get(sprintf('%s?include=commandCourses&client_id=%s', urlAjax, clientId), function(res) {
                        tr.addClass('details');
                        row.child( format(res, urlBookingIndex) ).show();
                    });
                }
            });

            EXP.initConfirmButton({
                selector: 'button.table-btn-destroy',
                title: "Delete client",
                msg: "This action is going to delete the current client.",
                callback: function(btn) {
                    $(btn).parent().submit();
                }
            });

            EXP.Downloader({
                el: '#btn-export-to-csv',
                url: '{{ route('clients.csv') }}',
                filename: 'clients.csv',
                mimetype: 'application/csv',
            }, function(request) {
                request.send("_token={{ csrf_token() }}");
            });
        });
    </script>
@endsection
