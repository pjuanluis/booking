@extends('layouts.app')

@section('title')
    @if ($formType === Constant::FORM_CREATE)
        Create client
    @elseif ($formType === Constant::FORM_EDIT)
        Edit client
    @elseif ($formType === Constant::FORM_SHOW)
        View client
    @endif
@endsection

@section('content-header')
    @if ($formType === Constant::FORM_CREATE)
        <h1>Create client</h1>
    @elseif ($formType === Constant::FORM_EDIT)
        <h1>Edit client</h1>
    @elseif ($formType === Constant::FORM_SHOW)
        <h1>View client</h1>
    @endif
@endsection

@php
$readOnly = ($formType === Constant::FORM_SHOW) ? 'readonly' : '';
$disabled = ($formType === Constant::FORM_SHOW) ? 'disabled' : '';
$imageURL = '';
if ($formType !== Constant::FORM_CREATE) {
    if (!empty($client->file->data)) {
        $imageURL = $client->file->data->path;
    }
}
@endphp

@section('content')
<style type="text/css">
    .normal-weight {
        font-weight: normal;
    }
    .select2-container--disabled .select2-selection__placeholder {
        display: none;
    }

    .table-first-row-no-border tr:first-child td {
        border: none;
    }
    .collapsing {
        -webkit-transition: none !important;
        transition: none !important;
        display: none;
    }
    .btn-toggle-travelinformation {
        background: none;
    }
    .btn-toggle-travelinformation .fa::before {
        content: "\f0d8";
    }
    .btn-toggle-travelinformation.collapsed .fa::before {
        content: "\f0d7";
    }
</style>
<?php
function getData($field, $client, $formType) {
    if ($field === 'has_alergies' || $field === 'has_diseases' || $field === 'is_smoker' || $field === 'has_volunteer_experience') {
        if (old($field) === '0') {
            return '0';
        }
    }
    if (old($field)) {
        return old($field);
    }
    if ($formType === Constant::FORM_EDIT || $formType === Constant::FORM_SHOW) {
        if ($field === 'gender_id') {
            return isset($client->gender->data->id) ? $client->gender->data->id : '';
        }
        return isset($client->$field) ? $client->$field : '';
    }
    return '';
}
?>
<form id="client-form" action="{{ $formType === Constant::FORM_SHOW ? '#' : $formAction }}" method="POST">
    {{ csrf_field() }}
    @if ($formType === Constant::FORM_EDIT)
        {{ method_field('PUT') }}
        <input type="hidden" name="updated_at" value="{{ $client->updated_at }}" />
    @endif
    <div class="box" style="">
        <div class="box-body no-padding">
            {{--  Personal info  --}}
            <div class="panel panel-primary no-margin no-border">
                <div class="panel-heading">
                    <h3 class="panel-title">Personal information</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="form-group col-sm-6 {{ $errors->has('first_name') ? 'has-error' : '' }}">
                            @php
                            $val = getData('first_name', $client, $formType);
                            @endphp
                            <label for="firstname" class="control-label">First name*</label>
                            <input id="firstname" type="text" class="form-control"
                                name="first_name" value="{{ $val }}" {{ $readOnly }} required>
                            <span class="help-block">{{ $errors->first('first_name') }}</span>
                        </div>
                        <div class="form-group col-sm-6 {{ $errors->has('last_name') ? 'has-error' : '' }}">
                            @php
                            $val = getData('last_name', $client, $formType);
                            @endphp
                            <label for="lastname" class="control-label">Last name*</label>
                            <input id="lastname" type="text" class="form-control"
                                name="last_name" value="{{ $val }}" {{ $readOnly }} required>
                            <span class="help-block">{{ $errors->first('last_name') }}</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6 {{ $errors->has('birth_date') ? 'has-error' : '' }}">
                            @php
                            $val = getData('birth_date', $client, $formType);
                            if (!empty($val)) {
                                try {$val = (new Carbon\Carbon($val))->format('d-m-Y');} catch (\Exception $e) {}
                            }
                            @endphp
                            <label for="birthdate" class="control-label">Date of birth</label>
                            <div class="input-group">
                                <input type="text" id="birthdate" name="birth_date" class="form-control birthdate-picker"
                                    value="{{ $val }}" {{ $disabled }} autocomplete="off"/>
                                <span class="input-group-addon {{ $disabled ? 'hide' : '' }}">
                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                </span>
                            </div>
                            <input type="text" id="client_age" class="form-control" readonly style="opacity: .6;"/>
                            <span class="help-block">{{ str_replace('Y-m-d', 'd-m-Y', $errors->first('birth_date')) }}</span>
                        </div>
                        <div class="form-group col-sm-6 {{ $errors->has('country_id') ? 'has-error' : '' }}">
                            <label for="select-country" class="control-label">Country</label>
                            <select name="country_id" id="select-country" {{ $disabled }} class="select2 form-control" style="width: 100%;">
                                @php
                                    $val = getData('country_id', $client, $formType);
                                    if (empty($val)) {
                                        $val = $countries[0]->id;
                                    }
                                @endphp
                                @foreach ($countries as $country)
                                    <option value="{{ $country->id }}" {{ $val == $country->id ? 'selected' : '' }}>{{ $country->name }}</option>
                                @endforeach
                            </select>
                            <span class="help-block">{{ $errors->first('country_id') }}</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6 {{ $errors->has('email') ? 'has-error' : '' }}">
                            @php
                            $val = getData('email', $client, $formType);
                            @endphp
                            <label for="email" class="control-label">E-mail*</label>
                            <input id="email" type="text" class="form-control"
                                name="email" value="{{ $val }}" {{ $readOnly }} required>
                            <span class="help-block">{{ $errors->first('email') }}</span>
                        </div>
                        <div class="form-group col-sm-6 {{ $errors->has('phone') ? 'has-error' : '' }}">
                            @php
                            $val = getData('phone', $client, $formType);
                            @endphp
                            <label for="phone" class="control-label">Phone</label>
                            <input id="phone" type="text" class="form-control"
                                name="phone" value="{{ $val }}" {{ $readOnly }}>
                            <span class="help-block">{{ $errors->first('phone') }}</span>
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('file') ? 'has-error' : '' }}">
                        <label for="phone" class="control-label">Image</label>
                        <div>
                            <img id="client-image" src="{{ $imageURL }}" alt="" width="300" height="300" style="{{ empty($imageURL) ? 'display: none;' : '' }}" />
                            <div id="croppie-image" style="width: 300px; height: 300px; {{ !empty($imageURL) ? 'display: none;' : '' }}"></div>
                            <div class="input-group  {{ $disabled }}">
                                <input type="file" id="file" value="Choose an image" accept="image/*" {{ $disabled }}
                                    class="{{ $disabled ? 'hide' : '' }}" />
                                <input type="hidden" id="file-data" name="file_data" />
                                <input type="hidden" id="file-name" name="file_name" />
                            </div>
                        </div>
                        <span class="help-block">{{ $errors->first('file') }}</span>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6 {{ $errors->has('emergency_phone') ? 'has-error' : '' }}">
                            @php
                            $val = getData('emergency_phone', $client, $formType);
                            @endphp
                            <label for="emergency-phone" class="control-label">Emergency phone</label>
                            <input id="emergency-phone" type="text" class="form-control"
                                name="emergency_phone" value="{{ $val }}" {{ $readOnly }}>
                            <span class="help-block">{{ $errors->first('emergency_phone') }}</span>
                        </div>
                        <div class="form-group col-sm-6 {{ $errors->has('gender_id') ? 'has-error' : '' }}">
                            <label class="control-label">Gender</label><br />
                            @php
                            $val = getData('gender_id', $client, $formType);
                            @endphp
                            @foreach ($genders->data as $gender)
                                <label class="radio-inline" style="padding-left: 0;">
                                    <input type="radio" class="iCheck" value="{{ $gender->id }}" name="gender_id" {{ $val == $gender->id? 'checked' : '' }} {{ $disabled }}> {{ $gender->name }}
                                </label>
                            @endforeach
                            <span class="help-block">{{ $errors->first('gender_id') }}</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6 {{ $errors->has('has_alergies') ? 'has-error' : '' }}">
                            @php
                            $val = getData('has_alergies', $client, $formType);
                            @endphp
                            <label class="control-label">Alergies</label>
                            <div>
                                <label class="normal-weight">
                                    <input name="has_alergies" type="radio" class="iCheck" id="aly" value="1" {{ $val=='1' ? 'checked' : '' }} {{ $disabled }} /> Yes
                                </label>
                            </div>
                            <div>
                                <label class="normal-weight">
                                    <input name="has_alergies" type="radio" class="iCheck" id="aln" value="0" {{ $val=='0' ? 'checked' : '' }} {{ $disabled }} /> No
                                </label>
                            </div>
                            <span class="help-block">{{ $errors->first('has_alergies') }}</span>
                        </div>
                        <div class="form-group col-sm-6 {{ $errors->has('alergies') ? 'has-error' : '' }}">
                            @php
                            $val = getData('alergies', $client, $formType);
                            @endphp
                            <label for="alergies" class="control-label">Specify</label>
                            <input id="alergies" type="text" class="form-control"
                                name="alergies" value="{{ $val }}" {{ $readOnly }}>
                            <span class="help-block">{{ $errors->first('alergies') }}</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6 {{ $errors->has('has_diseases') ? 'has-error' : '' }}">
                            @php
                            $val = getData('has_diseases', $client, $formType);
                            @endphp
                            <label class="control-label">Diseases</label>
                            <div>
                                <label class="normal-weight">
                                    <input name="has_diseases" type="radio" class="iCheck" id="disy" value="1" {{ $val=='1' ? 'checked' : '' }} {{ $disabled }} /> Yes
                                </label>
                            </div>
                            <div>
                                <label class="normal-weight">
                                    <input name="has_diseases" type="radio" class="iCheck" id="disn" value="0" {{ $val=='0' ? 'checked' : '' }} {{ $disabled }} /> No
                                </label>
                            </div>
                            <span class="help-block">{{ $errors->first('has_diseases') }}</span>
                        </div>
                        <div class="form-group col-sm-6 {{ $errors->has('diseases') ? 'has-error' : '' }}">
                            @php
                            $val = getData('diseases', $client, $formType);
                            @endphp
                            <label for="diseases" class="control-label">Specify</label>
                            <input id="diseases" type="text" class="form-control"
                                name="diseases" value="{{ $val }}" {{ $readOnly }}>
                            <span class="help-block">{{ $errors->first('diseases') }}</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6 {{ $errors->has('is_smoker') ? 'has-error' : '' }}">
                            @php
                            $val = getData('is_smoker', $client, $formType);
                            @endphp
                            <label class="control-label">Is smoker?</label>
                            <div>
                                <label class="normal-weight">
                                    <input name="is_smoker" type="radio" class="iCheck" id="smoky" value="1" {{ $val=='1' ? 'checked' : '' }} {{ $disabled }} /> Yes
                                </label>
                            </div>
                            <div>
                                <label class="normal-weight">
                                    <input name="is_smoker" type="radio" class="iCheck" id="smokn" value="0" {{ $val=='0' ? 'checked' : '' }} {{ $disabled }} /> No
                                </label>
                            </div>
                            <span class="help-block">{{ $errors->first('is_smoker') }}</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6 {{ $errors->has('fb_profile') ? 'has-error' : '' }}">
                            @php
                            $val = getData('fb_profile', $client, $formType);
                            @endphp
                            <label for="fb-profile" class="control-label">Facebook URL or profile</label>
                            <input id="fb-profile" type="text" class="form-control"
                                name="fb_profile" value="{{ $val }}" {{ $readOnly }}>
                            <span class="help-block">{{ $errors->first('fb_profile') }}</span>
                        </div>
                    </div>
                </div> {{-- panel-body --}}
            </div>

            {{--  Commands  --}}
            <div class="panel panel-primary no-margin no-border">
                <div class="panel-heading">
                    <h3 class="panel-title">Commands</h3>
                </div>
                <div class="panel-body">
                    @if ($formType === Constant::FORM_EDIT)
                        <div class="clearfix">
                            <a href="{{ route('bookings.create') }}" class="btn btn-primary pull-right {{ $disabled ? 'hide' : ''}}"
                                target="_blank" style="margin-bottom: 15px;" id="btn-add-command"
                                data-goto-url="{{ route('bookings.create') }}{{ isset($client->id) ? '?client_id=' . $client->id : '' }}">
                                Add Command
                            </a>
                        </div>
                    @endif
                    <table class="table table-first-row-no-border table-vertical-align-middle">
                        <tbody>
                            @forelse (
                                isset($client->commands->data) ?
                                $client->commands->data : []
                                as $k => $v
                            )
                                <tr>
                                    <td class="fit" style="width: 50px">
                                        <a href="{{ route('bookings.edit', ['command' => $v->id]) }}?client_id={{ $client->id }}" target="_blank"
                                            class="text-color-black">
                                            {{ $v->booking_code }}
                                        </a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td>No data available!</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>

            {{--  Travel information  --}}
            <div class="panel panel-primary no-margin no-border">
                <div class="panel-heading">
                    <h3 class="panel-title">Travel information</h3>
                </div>
                <div class="panel-body">
                    <table class="table table-first-row-no-border table-vertical-align-middle">
                        <tbody>
                            @forelse (
                                isset($client->commands->data) ?
                                $client->commands->data : []
                                as $k => $v
                            )
                                <tr data-cti="row-command-{{ $v->id }}">
                                    <td class="fit" title="Travel information" style="width: 50px">
                                        <button class="btn btn-toggle-travelinformation collapsed" type="button" data-toggle="collapse"
                                            data-target="#command-{{ $v->id }}" expanded="false"
                                            aria-controls="command-{{ $v->id }}">
                                            <i class="fa" aria-hidden="true"></i>
                                        </button>
                                    </td>
                                    <td>
                                        {{ $v->booking_code }}
                                    </td>
                                </tr>
                                <tr class="collapse" id="command-{{ $v->id }}">
                                    <td colspan="2">
                                        <div class="row">
                                            <div class="form-group col-sm-6 {{ $errors->has('travel_information.' . $k . '.travel_by') ? 'has-error' : '' }}">
                                                <input type="hidden" name="travel_information[{{ $k }}][id]"
                                                    value="{{ isset($v->travelInformation->data->id) ? $v->travelInformation->data->id : 0 }}">
                                                @php
                                                    $val = old('travel_information.' . $k . '.travel_by');

                                                    if ($val === null && isset($v->travelInformation->data->travel_by)) {
                                                        $val = $v->travelInformation->data->travel_by;
                                                    }
                                                @endphp
                                                <label class="control-label">Travel by</label>
                                                <div>
                                                    <label class="normal-weight">
                                                        <input name="travel_information[{{ $k }}][travel_by]" type="radio" class="iCheck" id="travelbyplane-c{{ $v->id }}" value="plane" {{ $val==='plane' ? 'checked' : '' }} {{ $disabled }} /> Plane
                                                    </label>
                                                </div>
                                                <div>
                                                    <label class="normal-weight">
                                                        <input name="travel_information[{{ $k }}][travel_by]" type="radio" class="iCheck" id="travelbybus-c{{ $v->id }}" value="bus" {{ $val==='bus' ? 'checked' : '' }} {{ $disabled }} /> Bus
                                                    </label>
                                                </div>
                                                <span class="help-block">{{ str_replace('travel_information.' . $k .'.', '', $errors->first('travel_information.' . $k . '.travel_by')) }}</span>
                                            </div>
                                        </div>
                                        @php
                                            $airports = ['Puerto Escondido', 'Huatulco'];
                                        @endphp
                                        <div class="row">
                                            <div class="form-group col-sm-6 {{ $errors->has('travel_information.' . $k . '.arrival_place') ? 'has-error' : '' }}">
                                                <label class="control-label">From CDMX to*</label>
                                                <select id="select-arrival-place-c{{ $v->id }}" name="travel_information[{{ $k }}][arrival_place]"
                                                    class="cselect2 form-control" {{ $disabled }} style="width:100%;">
                                                    <option value="" selected></option>
                                                    @php
                                                        $val = old('travel_information.' . $k . '.arrival_place');

                                                        if ($val === null && isset($v->travelInformation->data->arrival_place)) {
                                                            $val = $v->travelInformation->data->arrival_place;
                                                        }
                                                    @endphp
                                                    @foreach ($airports as $airport)
                                                        <option value="{{ $airport }}" {{ $val == $airport ? 'selected' : '' }}>{{ $airport }}</option>}
                                                    @endforeach
                                                </select>
                                                <span class="help-block">{{ str_replace('travel_information.' . $k .'.', '', $errors->first('travel_information.' . $k . '.arrival_place')) }}</span>
                                            </div>

                                            <div class="form-group col-sm-6 {{ $errors->has('travel_information.' . $k . '.arrival_carrier') ? 'has-error' : '' }}">
                                                @php
                                                    $val = old('travel_information.' . $k . '.arrival_carrier');

                                                    if ($val === null && isset($v->travelInformation->data->arrival_carrier)) {
                                                        $val = $v->travelInformation->data->arrival_carrier;
                                                    }
                                                @endphp
                                                <label for="arrival-carrier" class="control-label txt-change-airline">Airline</label>
                                                <input id="arrival-carrier-c{{ $v->id }}" type="text" class="form-control"
                                                    name="travel_information[{{ $k }}][arrival_carrier]" value="{{ $val }}" {{ $readOnly }}>
                                                <span class="help-block">{{ str_replace('travel_information.' . $k .'.', '', $errors->first('travel_information.' . $k . '.arrival_carrier')) }}</span>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-sm-4 {{ $errors->has('travel_information.' . $k . '.arrival_date') ? 'has-error' : '' }}">
                                                @php
                                                    $arrivalAt = null;
                                                    $val = old('travel_information.' . $k . '.arrival_date');

                                                    if ($val === null && !empty($v->travelInformation->data->arrival_at)) {
                                                        $arrivalAt = \Carbon\Carbon::parse($v->travelInformation->data->arrival_at);
                                                        $val = $arrivalAt->format('d-m-Y');
                                                    }
                                                @endphp
                                                <label for="arrival-date" class="control-label">Arrival day*</label>
                                                <div class="input-group">
                                                    <input type="text" id="arrival-date-c{{ $v->id }}" name="travel_information[{{ $k }}][arrival_date]" class="dpicker-local form-control"
                                                        value="{{ $val }}" {{ $disabled }}/>
                                                    <span class="input-group-addon {{ $disabled ? 'hide' : '' }}">
                                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                                <span class="help-block">{{ str_replace('travel_information.' . $k .'.', '', $errors->first('travel_information.' . $k . '.arrival_date')) }}</span>
                                            </div>

                                            <div class="bootstrap-timepicker form-group col-sm-4 {{ $errors->has('travel_information.' . $k . '.arrival_time') ? 'has-error' : '' }}">
                                                @php
                                                    $val = old('travel_information.' . $k . '.arrival_time');

                                                    if ($val === null && !empty($arrivalAt)) {
                                                        $val = $arrivalAt->format('H:i');
                                                    }
                                                @endphp
                                                <label for="arrival-time" class="control-label">Arrival hour</label>
                                                <div class="input-group">
                                                    <input type="text" id="arrival-time-c{{ $v->id }}" name="travel_information[{{ $k }}][arrival_time]" class="form-control"
                                                        value="{{ $val }}" {{ $disabled }} />
                                                    <span class="input-group-addon {{ $disabled ? 'hide' : '' }}">
                                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                                <span class="help-block">{{ str_replace('travel_information.' . $k .'.', '', $errors->first('travel_information.' . $k . '.arrival_time')) }}</span>
                                            </div>

                                            <div class="form-group col-sm-4 {{ $errors->has('travel_information.' . $k . '.arrival_travel_number') ? 'has-error' : '' }}">
                                                @php
                                                    $val = old('travel_information.' . $k . '.arrival_travel_number');

                                                    if ($val === null && isset($v->travelInformation->data->arrival_travel_number)) {
                                                        $val = $v->travelInformation->data->arrival_travel_number;
                                                    }
                                                @endphp
                                                <label for="arrival-number" class="control-label txt-change-flight-number">Flight number</label>
                                                <input id="arrival-number-c{{ $v->id }}" type="text" class="form-control"
                                                    name="travel_information[{{ $k }}][arrival_travel_number]" value="{{ $val }}" {{ $readOnly }}>
                                                <span class="help-block">{{ str_replace('travel_information.' . $k .'.', '', $errors->first('travel_information.' . $k . '.arrival_travel_number')) }}</span>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-sm-6 {{ $errors->has('travel_information.' . $k . '.departure_place') ? 'has-error' : '' }}">
                                                <label class="control-label">Departure from*</label>
                                                <select name="travel_information[{{ $k }}][departure_place]" class="cselect2 form-control"
                                                    {{ $disabled }} style="width:100%;">
                                                    <option value="" selected></option>
                                                    @php
                                                        $val = old('travel_information.' . $k . '.departure_place');

                                                        if ($val === null && isset($v->travelInformation->data->departure_place)) {
                                                            $val = $v->travelInformation->data->departure_place;
                                                        }
                                                    @endphp
                                                    @foreach ($airports as $airport)
                                                        <option value="{{ $airport }}" {{ $val == $airport ? 'selected' : '' }}>{{ $airport }}</option>
                                                    @endforeach
                                                </select>
                                                <span class="help-block">{{ str_replace('travel_information.' . $k .'.', '', $errors->first('travel_information.' . $k . '.departure_place')) }}</span>
                                            </div>

                                            <div class="form-group col-sm-6 {{ $errors->has('travel_information.' . $k . '.departure_carrier') ? 'has-error' : '' }}">
                                                @php
                                                    $val = old('travel_information.' . $k . '.departure_carrier');

                                                    if ($val === null && isset($v->travelInformation->data->departure_carrier)) {
                                                        $val = $v->travelInformation->data->departure_carrier;
                                                    }
                                                @endphp
                                                <label for="departure-carrier" class="control-label txt-change-airline">Airline</label>
                                                <input id="departure-carrier-c{{ $v->id }}" type="text" class="form-control"
                                                    name="travel_information[{{ $k }}][departure_carrier]" value="{{ $val }}" {{ $readOnly }}>
                                                <span class="help-block">{{ str_replace('travel_information.' . $k .'.', '', $errors->first('travel_information.' . $k . '.departure_carrier')) }}</span>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group col-sm-4 {{ $errors->has('travel_information.' . $k . '.departure_date') ? 'has-error' : '' }}">
                                                @php
                                                    $departureAt = null;
                                                    $val = old('travel_information.' . $k . '.departure_date');

                                                    if ($val === null && !empty($v->travelInformation->data->departure_at)) {
                                                        $departureAt = \Carbon\Carbon::parse($v->travelInformation->data->departure_at);
                                                        $val = $departureAt->format('d-m-Y');
                                                    }
                                                @endphp
                                                <label for="departure-date" class="control-label">Departure day*</label>
                                                <div class="input-group">
                                                    <input type="text" id="departure-date-c{{ $v->id }}" name="travel_information[{{ $k }}][departure_date]" class="dpicker-local form-control"
                                                        value="{{ $val }}" {{ $disabled }}/>
                                                    <span class="input-group-addon {{ $disabled ? 'hide' : '' }}">
                                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                                <span class="help-block">{{ str_replace('travel_information.' . $k .'.', '', $errors->first('travel_information.' . $k . '.departure_date')) }}</span>
                                            </div>

                                            <div class="bootstrap-timepicker form-group col-sm-4 {{ $errors->has('travel_information.' . $k . '.departure_time') ? 'has-error' : '' }}">
                                                @php
                                                    $val = old('travel_information.' . $k . '.departure_time');

                                                    if ($val === null && !empty($departureAt)) {
                                                        $val = $departureAt->format('H:i');
                                                    }
                                                @endphp
                                                <label for="departure-time" class="control-label">Departure hour</label>
                                                <div class="input-group">
                                                    <input type="text" id="departure-time-c{{ $v->id }}" name="travel_information[{{ $k }}][departure_time]" class="form-control"
                                                        value="{{ $val }}" {{ $disabled }} />
                                                    <span class="input-group-addon {{ $disabled ? 'hide' : '' }}">
                                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                                <span class="help-block">{{ str_replace('travel_information.' . $k .'.', '', $errors->first('travel_information.' . $k . '.departure_time')) }}</span>
                                            </div>

                                            <div class="form-group col-sm-4 {{ $errors->has('travel_information.' . $k . '.departure_travel_number') ? 'has-error' : '' }}">
                                                @php
                                                    $val = old('travel_information.' . $k . '.departure_travel_number');

                                                    if ($val === null && isset($v->travelInformation->data->departure_carrier)) {
                                                        $val = $v->travelInformation->data->departure_travel_number;
                                                    }
                                                @endphp
                                                <label for="departure-number" class="control-label txt-change-flight-number">Flight number</label>
                                                <input id="departure-number-c{{ $v->id }}" type="text" class="form-control"
                                                    name="travel_information[{{ $k }}][departure_travel_number]" value="{{ $val }}" {{ $readOnly }}>
                                                <span class="help-block">{{ str_replace('travel_information.' . $k .'.', '', $errors->first('travel_information.' . $k . '.departure_travel_number')) }}</span>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td>No data available!</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>

            {{--  Academic information --}}
            <div class="panel panel-primary no-margin no-border">
                <div class="panel-heading">
                    <h3 class="panel-title">Academic information</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group {{ $errors->has('spanish_level') ? 'has-error' : '' }}">
                        <label class="control-label">Spanish knowledge</label>
                        @php
                        $val = getData('spanish_level', $client, $formType);
                        @endphp
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="splev0">
                                    <input id="splev0" type="radio" class="iCheck opt-spanish-level" name="spanish_level" value="none" {{ (empty($val) || $val == 'none') ? 'checked' : '' }} {{ $disabled }}> None
                                </label>
                            </div>
                            <div class="col-sm-3">
                                <label for="splev1">
                                    <input id="splev1" type="radio" class="iCheck opt-spanish-level" name="spanish_level" value="begginer" {{ $val == 'begginer' ? 'checked' : '' }} {{ $disabled }}> Begginer
                                </label>
                            </div>
                            <div class="col-sm-3">
                                <label for="splev2">
                                    <input id="splev2" type="radio" class="iCheck opt-spanish-level" name="spanish_level" value="middle"  {{ $val=='middle' ? 'checked' : '' }} {{ $disabled }}> Middle
                                </label>
                            </div>
                            <div class="col-sm-3">
                                <label for="splev3">
                                    <input id="splev3" type="radio" class="iCheck opt-spanish-level" name="spanish_level" value="advanced" {{ $val=='advanced' ? 'checked' : '' }} {{ $disabled }}> Advanced
                                </label>
                            </div>
                        </div>
                        <span class="help-block">{{ $errors->first('spanish_level') }}</span>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            @php
                                $tmp = null;
                                $val = old('spanish_knowledge_number') ? old('spanish_knowledge_number') : '';
                                if (empty($val)) {
                                    $val = getData('spanish_knowledge_duration', $client, $formType);
                                    $tmp = !empty($val) ? explode(' ', $val) : ['', ''];
                                    $val = $tmp[0];
                                }
                            @endphp
                            <div class="form-group col-sm-3 {{ $errors->has('spanish_knowledge_duration') ? 'has-error' : '' }}">
                                <label class="control-label">How long</label>
                                <input type="number" name="spanish_knowledge_number" class="form-control" value="{{ !empty($val) ? $val : 0 }}" {{ $readOnly }}>
                            </div>
                            <div class="form-group col-sm-3">
                                <label class="control-label">&nbsp;</label>
                                <select name="spanish_knowledge_type" {{ $disabled }} class="cselect2 form-control">
                                    @php
                                        $val = old('spanish_knowledge_type') ? old('spanish_knowledge_type') : '';
                                        if (empty($val)) {
                                            $val = $tmp[1];
                                        }
                                    @endphp
                                    <option value="month" {{ $val == 'month' ? 'selected' : '' }}>Months</option>
                                    <option value="year" {{ $val == 'year' ? 'selected' : '' }}>Years</option>
                                </select>
                            </div>
                            <input type="hidden" name="spanish_knowledge_duration" value="0 months" />
                        </div>
                        <span class="help-block">{{ $errors->first('spanish_knowledge_duration') }}</span>
                    </div>
                    @if ($formType !== Constant::FORM_CREATE)
                        <div class="form-group">
                            <div class="row">
                                <div class="form-group col-sm-3">
                                    <a href="https://www.experienciamexico.mx/forms/" target="_blank" class="btn btn-primary">Spanish Test</a>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="form-group {{ $errors->has('has_surf_experience') ? 'has-error' : '' }}">
                        <label class="control-label">Surf Experience</label>
                        @php
                        $val = getData('has_surf_experience', $client, $formType);
                        @endphp
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="sexpy">
                                    <input id="sexpy" type="radio" class="iCheck opt-surf-experience with-gap" name="has_surf_experience" value="1" {{ $val == '1' ? 'checked' : '' }} {{ $disabled }}> Yes
                                </label>
                            </div>
                            <div class="col-sm-3">
                                <label for="sexpn">
                                    <input id="sexpn" type="radio" class="iCheck opt-surf-experience with-gap" name="has_surf_experience" value="0" {{ empty($val) ? 'checked' : '' }} {{ $disabled }}> No
                                </label>
                            </div>
                        </div>
                        <span class="help-block">{{ $errors->first('has_surf_experience') }}</span>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            @php
                                $tmp = null;
                                $val = old('surf_experience_number') ? old('surf_experience_number') : '';
                                if (empty($val)) {
                                    $val = getData('surf_experience_duration', $client, $formType);
                                    $tmp = !empty($val) ? explode(' ', $val) : ['', ''];
                                    $val = $tmp[0];
                                }
                            @endphp
                            <div class="form-group col-sm-3 {{ $errors->has('surf_experience_duration') ? 'has-error' : '' }}">
                                <label class="control-label">How long</label>
                                <input type="number" name="surf_experience_number" class="form-control" value="{{ !empty($val) ? $val : 0 }}" {{ $readOnly }}>
                            </div>
                            <div class="form-group col-sm-3">
                                <label class="control-label">&nbsp;</label>
                                <select name="surf_experience_type" {{ $disabled }} class="cselect2 form-control">
                                    @php
                                        $val = old('surf_experience_type') ? old('surf_experience_type') : '';
                                        if (empty($val)) {
                                            $val = $tmp[1];
                                        }
                                    @endphp
                                    <option value="month" {{ $val == 'month' ? 'selected' : '' }}>Months</option>
                                    <option value="year" {{ $val == 'year' ? 'selected' : '' }}>Years</option>
                                </select>
                            </div>
                            <input type="hidden" name="surf_experience_duration" value="0 months" />
                        </div>
                        <span class="help-block">{{ $errors->first('surf_experience_duration') }}</span>
                    </div>
                    <div class="form-group {{ $errors->has('has_volunteer_experience') ? 'has-error' : '' }}">
                        <label class="control-label">Volunteer experience</label>
                        @php
                        $val = getData('has_volunteer_experience', $client, $formType);
                        @endphp
                        <div class="row">
                            <div class="col-sm-3">
                                <input id="vexpy" type="radio" class="iCheck opt-volunteer-experience with-gap" name="has_volunteer_experience" value="1" {{ $val=='1' ? 'checked' : '' }} {{ $disabled }}>
                                <label for="vexpy">Yes</label>
                            </div>
                            <div class="col-sm-3">
                                <input id="vexpn" type="radio" class="iCheck opt-volunteer-experience with-gap" name="has_volunteer_experience" value="0" {{ empty($val) ? 'checked' : '' }} {{ $disabled }}>
                                <label for="vexpn">No</label>
                            </div>
                            <div class="form-group col-sm-6 {{ $errors->has('volunteer_experience') ? 'has-error' : '' }}" style="margin-top: 0;">
                                @php
                                $val = getData('volunteer_experience', $client, $formType);
                                @endphp
                                <label class="control-label">Specify*</label>
                                <textarea class="form-control" name="volunteer_experience" rows="4" {{ $readOnly }}>{{ $val }}</textarea>
                            </div>
                        </div>
                        <span class="help-block">{{ $errors->first('has_volunteer_experience') }}</span>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            @php
                                $tmp = null;
                                $val = old('volunteer_experience_number') ? old('volunteer_experience_number') : '';
                                if (empty($val)) {
                                    $val = getData('volunteer_experience_duration', $client, $formType);
                                    $tmp = !empty($val) ? explode(' ', $val) : ['', ''];
                                    $val = $tmp[0];
                                }
                            @endphp
                            <div class="form-group col-sm-3 {{ $errors->has('volunteer_experience_duration') ? 'has-error' : '' }}">
                                <label class="control-label">How long</label>
                                <input type="number" name="volunteer_experience_number" class="form-control"
                                    value="{{ !empty($val) ? $val : 0 }}" {{ $readOnly }}>
                            </div>
                            <div class="form-group col-sm-3">
                                <label class="control-label">&nbsp;</label>
                                <select name="volunteer_experience_type" {{ $disabled }} class="cselect2 form-control">
                                    @php
                                        $val = old('volunteer_experience_type') ? old('volunteer_experience_type') : '';
                                        if (empty($val)) {
                                            $val = $tmp[1];
                                        }
                                    @endphp
                                    <option value="month" {{ $val == 'month' ? 'selected' : '' }}>Months</option>
                                    <option value="year" {{ $val == 'year' ? 'selected' : '' }}>Years</option>
                                </select>
                            </div>
                            <input type="hidden" name="volunteer_experience_duration" value="0 months" />
                        </div>
                        <span class="help-block">{{ $errors->first('volunteer_experience_duration') }}</span>
                    </div>
                </div>
            </div>

            {{-- Acount information --}}
            <div class="panel panel-primary no-margin no-border">
                <div class="panel-heading">
                    <h3 class="panel-title">Acount information</h3>
                </div>
                <div class="panel-body">

                    <div class="row">
                        <div class="col-sm-6 col-md-4 col-lg-3">
                            <div class="form-group">
                                <label for="account-user" class="control-label">User</label>
                                <input type="email" class="form-control" id="account-user" required readonly>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6 col-md-4 col-lg-3">
                            <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                                <label for="account-password" class="control-label">Password</label>
                                <input type="password" class="form-control" id="account-password" {{ $readOnly }}
                                    name="password" value="{{ old('password') ? old('password') : '' }}">
                                <span class="help-block">{{ $errors->first('password') }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a class="btn btn-default" href="{{ url()->previous() }}">
        @if ($formType !== Constant::FORM_SHOW)
            Cancel
        @else
            Back
        @endif
    </a>
    @if ($formType !== Constant::FORM_SHOW)
        <button class="btn btn-primary" type="submit" name="action">Save</button>
    @else
        <a class="btn btn-primary" href="{{ route('clients.edit', ['client' => $client->id]) }}">
            Edit
        </a>
    @endif
</form>

<script type="text/javascript">
    /**
     * Open collapse if fields has error
     */
    function openCollapsedWithClass(clas =  '.has-error') {
        var $collapse = $('.collapse');

        if (clas === void 0) {
            clas = '.has-error';
        }

        _.forEach($collapse, function(col) {
            var $col = $(col);
            var $hasHerror = $col.find(clas);

            if ($hasHerror.length > 0) {
                $('[data-target="#' + $col.attr('id') + '"]').removeClass('collapsed');
                $col.addClass('in');
            }
        });
    }

    function calculateClientAge(date, inputFormat) {
        let years = "";
        if (date && inputFormat) {
            years = moment().diff(moment(date, inputFormat), 'years');
        }
        if (years) {
            years = " " + years + " years old";
        } else {
            years = "";
        }
        $('#client_age').val(years)
    }

    function addActiveRowTravelInformation(dataCTI) {
        console.log('[data-cti="' + dataCTI + '"]');
        $('[data-cti="' + dataCTI + '"]').addClass('active');
    }

    function removeActiveRowTravelInformation(dataCTI) {
        $('[data-cti="' + dataCTI + '"]').removeClass('active');
    }

    function setActiveRowsTravelInformation() {
        var $rowCollapseIn = $('.collapse.in');

        $('[data-cti]').removeClass('active');

        _.forEach($rowCollapseIn, function(cIn) {
            addActiveRowTravelInformation('row-' + cIn.id);
        });
    }

    function duplicateEmailInAccountInformation() {
        var $email = $('#email');
        var $accountUser = $('#account-user');

        $email.change(function(e) {
            $accountUser.val($email.val());
        }).keyup(function(e) {
            $accountUser.val($email.val());
        });

        $accountUser.val($email.val());
    }

    window.addEventListener('load', function() {
        duplicateEmailInAccountInformation();
        calculateClientAge($('#birthdate').val(), 'DD-MM-YYYY');
        var $uploadCrop;
        var fileSelected = false;
        $('#birthdate').on('change', (e) => {
            calculateClientAge($('#birthdate').val(), 'DD-MM-YYYY');
        });
        function readFile(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#client-image').hide();
                    $('#croppie-image').show();
                    $uploadCrop.croppie('bind', {
                        url: e.target.result
                    }).then(function(){
                        console.log('jQuery bind complete');
                    });
                };
                reader.readAsDataURL(input.files[0]);
                $('#file-name').val(input.files[0].name);
            }
            else {
                swal("Sorry - you're browser doesn't support the FileReader API");
            }
        }
        $uploadCrop = $('#croppie-image').croppie({
            viewport: {
                width: 250,
                height: 250,
                type: 'square'
            },
            showZoomer: false,
            enableOrientation: true,
            enableExif: true
        });
        $('#file').on('change', function () {
            fileSelected = true;
            readFile(this);
        });
        setTimeout(function() {
            $('.cselect2').select2({
                placeholder: "Select an option",
                allowClear: true
            });
        }, 200);
        $('form#client-form').submit(function() {
            if (fileSelected) {
                $uploadCrop.croppie('result', {size: {width: 400, height: 400}}).then(function(img) {
                    $('#file-data').val(img);
                });
            }
        }).find('[type="submit"]').on('click', function(e) {
            var $collapsedNotIn = $('.collapse:hidden');

            _.forEach($collapsedNotIn.find(':required').first(), function(v) {
                var $required = $(v);

                if (!$required.val()) {
                    $required.addClass('open-parent-collapsed');
                }

                return false;
            });

            openCollapsedWithClass('.open-parent-collapsed');
        });
        $('#firstname').focus();

        @if($formType === Constant::FORM_CREATE)
            // Alerges initial checked
            if ($('[name=has_alergies]:checked').length == 0) {
                $('#aln').iCheck('check');
            }
            // Diseases initial checked
            if ($('[name=has_diseases]:checked').length == 0) {
                $('#disn').iCheck('check');
            }
            // Smoker initial checked
            if ($('[name=is_smoker]:checked').length == 0) {
                $('#smokn').iCheck('check');
            }
            // Trabel by initial checked
            if ($('[name=travel_by]:checked').length == 0) {
                $('#travelbyplane').iCheck('check');
            }
            // Spanish level initial checked
            if ($('[name=spanish_level]:checked').length == 0) {
                $('#splev0').iCheck('check');
            }
            // Surf Experiencia initial checked
            if ($('[name=has_surf_experience]:checked').length == 0) {
                $('#sexpn').iCheck('check');
            }
            // has_volunteer_experience initial checked
            if ($('[name=has_volunteer_experience]:checked').length == 0) {
                $('#vexpn').iCheck('check');
            }
        @endif

        // Change text flight number
        $('[name="travel_by"]').on('ifChanged', function(e) {
            var travelValue = e.currentTarget.value;
            if (travelValue == 'bus') {
                $('.txt-change-flight-number').text('Bus number');
                $('.txt-change-airline').text('Busline');
            } else if (travelValue == 'plane') {
                $('.txt-change-flight-number').text('Flight number');
                $('.txt-change-airline').text('Airline');
            }
        });

        // Collapses of travel information
        $.support.transition = false
        openCollapsedWithClass();
        setActiveRowsTravelInformation();
        $('.collapse').on('hidden.bs.collapse', function () {
            removeActiveRowTravelInformation('row-' + this.id);
        }).on('show.bs.collapse', function() {
            addActiveRowTravelInformation('row-' + this.id);
        });

        clients_btnAddCommand(); // clients_btn-add-command.js
    });
</script>

@endsection
