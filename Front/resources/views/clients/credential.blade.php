<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Credential {{ $fullName }}</title>
        <style type="text/css">
            * {
                padding: 0;
                margin: 0;
                box-sizing: border-box;
            }

            .main {
                margin: 30px 15px;
                padding: 10px;
            }

            .credential__background {
                width: 8.6cm;
                height: 5.4cm;
                display: block;
                position: absolute;
                left: 0;
                top: 0;
            }

            .credential__back {
                transform: rotate(180deg);
            }

            .credential__front, .credential__back {
                width: 8.6cm;
                height: 5.4cm;
                position: relative;
            }

            .credential__info {
                position: absolute;
                z-index: 100;
            }

            .credential__info--name, .credential__info--validity {
                left: 122px;
                font-family: sans-serif;
            }

            .credential__info--name {
                font-size: .8em;
                top: 100px;
                color: #3F5AB0;
                font-weight: bold;
            }

            .credential__info--validity {
                top: 147px;
                font-size: .6em;
            }

            .credential__info--photo {
                top: 45px;
                left: 16px;
                width: 90px;
                height: 95px;
                display: block;
            }
        </style>
    </head>
    <body>
        <div class="main">
            <div class="credential">
                <div class="credential__front">
                    <img class="credential__background" src="{{ asset('img/admin/credential_front.jpg') }}" alt="Credential front">
                    @if (!empty($file))
                        <img class="credential__info credential__info--photo" src="{{ $file }}" alt="Client photo">
                    @endif
                    <p class="credential__info credential__info--name">{{ $fullName }}</p>
                    <p class="credential__info credential__info--validity">{{ $validity }}</p>
                </div>
                <div class="credential__back">
                    <img class="credential__background" src="{{ asset('img/admin/credential_back.jpg') }}" alt="Credential back">
                </div>
            </div>
        </div>
    </body>
</html>
