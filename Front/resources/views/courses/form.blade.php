@extends('layouts.app')

@section('title')
    {{ $title }}
@endsection

@section('content-header')
    <h1>{{ $title }}</h1>
@endsection

@php
    $readOnly = ($formType === Constant::FORM_SHOW) ? 'readonly' : '';
@endphp

@section('content')
<form id="client-form" action="{{ $formType === Constant::FORM_SHOW ? '#' : $formAction }}" method="POST">
    {{ csrf_field() }}
    @if ($formType === Constant::FORM_EDIT)
        {{ method_field('PUT') }}
        <input type="hidden" name="updated_at" value="{{ $package->updated_at }}" />
    @endif
    <div class="box" style="">
        <div class="box-body">
            @php
                $units = [
                    [
                        'name' => 'Class',
                        'slug' => 'class'
                    ],
                    [
                        'name' => 'Project',
                        'slug' => 'project'
                    ],
                ];
                $selectedExperiences = explode(',', old('experiences', ''));
                $selectedUnit = old('unit', isset($package)? $package->unit : '');
                if (isset($package->experiences)) {
                    $selectedExperiences = array_map(function($el) {
                        return $el->id;
                    }, $package->experiences->data);
                }
            @endphp
            <div class="form-group {{ $errors->has('experiences.0') ? 'has-error' : '' }}" style="overflow-x: hidden;">
                <label for="" class="control-label">Experiences</label>
                <input type="hidden" name="experiences" id="experiences" value="{{ implode(',', $selectedExperiences) }}">
                <select class="form-control select2-c" multiple="multiple" {{ $readOnly === 'readonly'? 'disabled' : '' }} style="max-width: 100%; width: 100%; background-color: transparent;">
                    @foreach ($experiences->data as $experience)
                        <option value="{{ $experience->id }}" {{ in_array($experience->id, $selectedExperiences)? 'selected' : '' }}>{{ $experience->name }}</option>
                    @endforeach
                </select>
                <span class="help-block">{{ $errors->first('experiences.0') }}</span>
            </div>
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name" class="control-label">Name*</label>
                <input type="text" class="form-control" name="name" {{ $readOnly }} required id="name" value="{{ old('name', isset($package->name)? $package->name : '') }}">
                <span class="help-block">{{ $errors->first('name') }}</span>
            </div>
            <div class="form-group {{ $errors->has('unit') ? 'has-error' : '' }}">
                <label for="unit" class="control-label">Unit*</label>
                <select class="form-control select2" name="unit" {{ $readOnly === 'readonly'? 'disabled' : '' }} required id="unit" style="max-width: 100%; width: 100%;">
                    @foreach ($units as $unit)
                        <option value="{{ $unit['slug'] }}" {{ $selectedUnit == $unit['slug']? 'selected' : '' }}>{{ $unit['name'] }}</option>
                    @endforeach
                </select>
                <span class="help-block">{{ $errors->first('unit') }}</span>
            </div>
            <div class="form-group {{ $errors->has('quantity') ? 'has-error' : '' }}">
                <label for="quantity" class="control-label">Quantity*</label>
                <input type="number" class="form-control" name="quantity" {{ $readOnly }} required id="quantity" value="{{ old('quantity', isset($package)? $package->quantity : '') }}" min="0" step="any">
                <span class="help-block">{{ $errors->first('quantity') }}</span>
            </div>
            <div class="form-group {{ $errors->has('cost') ? 'has-error' : '' }}">
                <label for="cost" class="control-label">Cost*</label>
                <input type="number" class="form-control" name="cost" {{ $readOnly }} required id="cost" value="{{ old('cost', isset($package)? $package->cost : '') }}" min="0" step="any">
                <span class="help-block">{{ $errors->first('cost') }}</span>
            </div>
            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="description" class="control-label">Description*</label>
                <textarea class="form-control" name="description" {{ $readOnly }} required id="description" rows="5">{{ old('description', isset($package->description)? $package->description : '') }}</textarea>
                <span class="help-block">{{ $errors->first('description') }}</span>
            </div>
            <div>
                <b>Rates</b>
                <hr style="margin-top: 0;">
            </div>
            @if(isset($package->rates->data))
                @php
                    $rates = $package->rates->data;
                    $newRates = [];
                    for ($i = 0; $i < count($rates); $i++) {
                        $newRates[$i]['from'] = $rates[$i]->quantity;
                        $newRates[$i]['to'] = $rates[$i]->to_quantity;
                        $newRates[$i]['unit_cost'] = $rates[$i]->unit_cost;
                    }
                    $ratesJson = json_encode($newRates);
                @endphp
            @endif
            <div id="app">
                <input type="hidden" id="rates_hidden" name="course_rates" :value="JSON.stringify(rates)" value="{{ old('course_rates', isset($package->rates->data) ? $ratesJson : '[]') }}">
                <div class="row" v-for="(rate, i) in rates">
                    <div class="col-sm-11" style="padding-left: 0;">
                        <div class="col-sm-4">
                            <div class="form-group" :class="{ 'has-error': (showErrorMessage(i, 'from') != '') }">
                                <label class="control-label label-line">From*</label>
                                <input type="number" class="form-control" step="any" v-model="rate.from" {{ $readOnly }} min="1">
                                <span class="help-block">
                                    @{{ showErrorMessage(i, 'from') }}
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" :class="{ 'has-error': (showErrorMessage(i, 'to') != '') }">
                                <label class="control-label label-line">To</label>
                                <input type="number" class="form-control" step="any" v-model="rate.to" {{ $readOnly }} min="1">
                                <span class="help-block">
                                    @{{ showErrorMessage(i, 'to') }}
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" :class="{ 'has-error': (showErrorMessage(i, 'unit_cost') != '') }">
                                <label class="control-label label-line" for="">Unit cost*</label>
                                <input type="number" class="form-control" value="" step="any" v-model="rate.unit_cost" {{ $readOnly }} min="1">
                                <span class="help-block">
                                    @{{ showErrorMessage(i, 'unit_cost') }}
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1">
                        @if($readOnly != 'readonly')
                        <div style="display: flex; align-items: center; height: 76px;">
                            <button type="button" class="btn table-btn table-btn-destroy" @click="deleteItemDynamicList(rate, rates, null, {keep: 1, itemBase: { 'from': '', 'to': '', 'unit_cost': '' }})">
                                <i class="fa fa-trash-o fa--size-1-3" aria-hidden="true"></i>
                            </button>
                        </div>
                        @endif
                    </div>
                </div>
                @if($readOnly != 'readonly')
                <button id="add-rate" type="button" class="btn btn-primary" style="margin-bottom: 10px;" @click="addItemDynamicList({ 'from': '', 'to': '', 'unit_cost': '' }, rates)">Add rate</button>
                @endif
            </div>
        </div>
        <div class="box-footer">
            <a class="btn btn-default" href="{{ url()->previous() }}">
                @if ($formType !== Constant::FORM_SHOW)
                    Cancel
                @else
                    Back
                @endif
            </a>
            @if ($formType !== Constant::FORM_SHOW)
                <button class="btn btn-primary" type="submit" name="action">Save</button>
            @else
                <a class="btn btn-primary" href="{{ route('courses.edit', ['id' => $package->id]) }}">
                    Edit
                </a>
            @endif
        </div>
    </div>
</form>
@endsection

@section('scripts')
    @parent
    <script src="{{ asset('js/vue.min.js') }}"></script>
    <script type="text/javascript">
        $(function() {
            var $select2C = $(".select2-c");
            $select2C.select2({
                tags: true,
                tokenSeparators: [',', ' ']
            });
            $select2C.on("change", function(e) {
                $('#experiences').val($(this).val());
            });
        });
        new Vue({
            el: '#app',
            data: {
                rates: JSON.parse(document.querySelector('#rates_hidden').value || '[]'),
                errors: {!! json_encode($errors->get('course_rates.*')) !!}
            },
            methods: {
                /**
                * Delete item from dynamic list.
                * @item element to delete
                * @items array with all items
                * @itemsDeleted array of deleted items [{ id: 1 }, {id: 9}]
                * @keepElements If a item will never deleted but clear content: {keep: 1, itemBase: {from: '', to: '', pax: ''}}
                */
                deleteItemDynamicList: function(item, items, itemsDeleted, keepElements) {
                    swal({
                        title: "Delete rate",
                        text: "Are you sure to delete the selected rate",
                        type: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, do it!'
                    }).then(function () {
                        var index = items.indexOf(item);
                        // Log id of items deleted
                        if (itemsDeleted) {
                            var itemId = items[index].id || null;
                            if (itemId) {
                                itemsDeleted.push({ id: itemId });
                            }
                        }
                        // Keep item but clear contents
                        if (keepElements) {
                            var keep = keepElements.keep || null,
                            itemsTotal = items.length;
                            if (keep && (itemsTotal <= keep)) {
                                items.splice(index, 1, keepElements. itemBase);
                                return;
                            }
                        }
                        // Delete item
                        items.splice(index, 1);
                    }).catch(swal.noop);
                },
                addItemDynamicList: function(itemBase, items) {
                    items.push(itemBase);
                },
                showErrorMessage: function(index, field) {
                    return this.errors['course_rates.' + index + "." + field]? this.errors['course_rates.' + index + "." + field][0] : '';
                }
            },
            mounted: function() {
                @if (empty(old('course_rates')))
                    @if($formType !== Constant::FORM_EDIT && $formType !== Constant::FORM_SHOW)
                        $('#add-rate').trigger('click');
                    @endif
                @endif
            }
        });
    </script>
@endsection
