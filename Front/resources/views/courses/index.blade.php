@extends('layouts.app')

@section('title')
    Courses
@endsection

@section('content-header')
    <h1>Courses</h1>
@endsection

@section('content')
<div class="box">
    <div class="box-header with-border">
        <div class="row">
            <div class="col-sm-8">
                @php
                    $selectedExperiences = !empty(Request::get('experience'))? explode(',', Request::get('experience')) : [];
                @endphp
                <form class="form-horizontal" action="{{ route('courses.index') }}" method="get">
                    {{-- {{ csrf_field() }} --}}
                    <div class="form-group" style="width: 100%;">
                        <label for="" class="control-label col-sm-3">Filter by experience</label>
                        <div class="col-sm-7">
                            <input type="hidden" name="experience" id="experience" value="">
                            <select class="form-control select2-c" multiple="multiple" style="width: 100%;">
                                @foreach ($experiences->data as $experience)
                                    <option value="{{ $experience->slug }}" {{ in_array($experience->slug, $selectedExperiences)? 'selected' : '' }}>{{ $experience->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" class="btn btn-success col-sm-2">Filter</button>
                    </div>
                </form>
            </div>
            <div class="col-sm-4">
                <a href="{{ route('courses.create') }}" class="btn btn-primary pull-right">New course</a>
                <a href="{{ route('experiences.create') }}" class="btn btn-primary pull-right" style="margin-right: 5px;">New experience</a>
            </div>
        </div>
    </div>
    <div class="box-body">
        <div class="panel no-margin no-border">
            <div class="panel-body">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Unit</th>
                            <th>Quantity</th>
                            <th>Cost</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">View</th>
                            <th class="text-center">Edit</th>
                            <th class="text-center">Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($packages->data as $p)
                        <tr>
                            <td>{{ $p->name }}</td>
                            <td>{{ $p->unit }}</td>
                            <td>{{ $p->quantity }}</td>
                            <td>{{ $p->cost }}</td>
                            <td class="text-center fit">
                                <form action="{{ route('courses.activate', ['id' => $p->id]) }}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}
                                    <button type="submit" class="action-btn table-btn-activate" style="opacity: 1;" title="{{ $p->is_active == 1? 'Deactivate' : 'Activate' }}">
                                        <i class="fa fa-flag {{ $p->is_active == 1 ? 'on' : 'off' }}" aria-hidden="true"></i>
                                    </button>
                                </form>
                            </td>
                            <td class="text-center fit">
                                <a class="action-btn" href="{{ route('courses.show', ['id' => $p->id, ]) }}">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </a>
                            </td>
                            <td class="text-center fit">
                                <a class="action-btn" href="{{ route('courses.edit', ['id' => $p->id]) }}">
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </a>
                            </td>
                            <td class="text-center fit">
                                <form action="{{ route('courses.destroy', ['id' => $p->id]) }}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button type="submit" class="action-btn table-btn-destroy">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @empty
                            <tr><td colspan="6">No data</td></tr>
                        @endforelse
                    </tbody>
                </table>
                {!! Component::pagination(
                    10,
                    $packages->meta->pagination->current_page,
                    $packages->meta->pagination->total_pages,
                    $params
                ) !!}
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    @parent
    <script type="text/javascript">
    window.addEventListener('load', function() {
        $select2C = $(".select2-c");
        $select2C.select2({
            tags: true,
            tokenSeparators: [',', ' ']
        });
        $select2C.on("change", function(e) {
            var values = $(this).val();
            $('#experience').val(values.join());
        });


        EXP.initConfirmButton({
            selector: 'button.table-btn-destroy',
            title: "Delete course",
            msg: "This action is going to delete the current course.",
            callback: function(btn) {
                $(btn).parent().submit();
            }
        });

        EXP.initConfirmButton({
            selector: 'button.table-btn-activate',
            title: "Activate/deactivate course",
            msg: "This action is going to activate/deactivate the current course.",
            callback: function(btn) {
                $(btn).parent().submit();
            }
        });
    });
    </script>
@endsection
