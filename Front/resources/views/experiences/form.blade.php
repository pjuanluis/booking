@extends('layouts.app')

@section('title')
    {{ $title }}
@endsection

@section('content-header')
    <h1>{{ $title }}</h1>
@endsection

@php
    $readOnly = ($formType === Constant::FORM_SHOW) ? 'readonly' : '';
@endphp

@section('content')
<form id="client-form" action="{{ $formType === Constant::FORM_SHOW ? '#' : $formAction }}" method="POST">
    {{ csrf_field() }}
    @if ($formType === Constant::FORM_EDIT)
        {{ method_field('PUT') }}
        <input type="hidden" name="updated_at" value="{{ $experience->updated_at }}" />
    @endif
    <div class="box" style="">
        <div class="box-body">
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name" class="control-label">Name*</label>
                <input id="name" type="text" class="form-control"
                    name="name" value="{{ old('name', isset($experience)? $experience->name : '') }}" {{ $readOnly }} required>
                <span class="help-block">{{ $errors->first('name') }}</span>
            </div>
            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="description" class="control-label">Description</label>
                <textarea id="description" class="form-control"
                    name="description" {{ $readOnly }}>{{ old('description', isset($experience)? $experience->description : '') }}</textarea>
                <span class="help-block">{{ $errors->first('description') }}</span>
            </div>
        </div>
        <div class="box-footer">
            <a class="btn btn-default" href="{{ url()->previous() }}">
                @if ($formType !== Constant::FORM_SHOW)
                    Cancel
                @else
                    Back
                @endif
            </a>
            @if ($formType !== Constant::FORM_SHOW)
                <button class="btn btn-primary" type="submit" name="action">Save</button>
            @endif
        </div>
    </div>
</form>

@endsection
