@extends('layouts.app')

@section('title')
    {{ $title }}
@endsection

@section('content-header')
    <h1>{{ $title }}</h1>
@endsection

@php
    $readOnly = ($formType === Constant::FORM_SHOW) ? 'readonly' : '';
@endphp

@section('content')
<form id="client-form" action="{{ $formType === Constant::FORM_SHOW ? '#' : $formAction }}" method="POST">
    {{ csrf_field() }}
    @if ($formType === Constant::FORM_EDIT)
        {{ method_field('PUT') }}
        <input type="hidden" name="updated_at" value="{{ $package->housingRoomPackage->updated_at }}" />
    @endif
    <div class="box" style="">
        <div class="box-body">
            @php
                $units = [
                    [
                        'name' => 'Week',
                        'slug' => 'week'
                    ],
                    [
                        'name' => 'Night',
                        'slug' => 'night'
                    ],
                    [
                        'name' => 'Month',
                        'slug' => 'month'
                    ],
                ];
            @endphp
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name" class="control-label">Name*</label>
                <input type="text" class="form-control" name="name" {{ $readOnly }} required id="name" value="{{ old('name', isset($package->name)? $package->name : '') }}">
                <span class="help-block">{{ $errors->first('name') }}</span>
            </div>
            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="description" class="control-label">Description*</label>
                <textarea class="form-control" name="description" {{ $readOnly }} required id="description">{{ old('description', isset($package->description)? $package->description : '') }}</textarea>
                <span class="help-block">{{ $errors->first('description') }}</span>
            </div>
            <div>
                <b>Rates</b>
                <hr style="margin-top: 0;">
            </div>
            @if(isset($package->rates->data))
                @php
                    $rates = $package->rates->data;
                    $newRates = [];
                    for ($i = 0; $i < count($rates); $i++) {
                        $newRates[$i]['quantity'] = $rates[$i]->quantity;
                        $newRates[$i]['from'] = $rates[$i]->from_date ? (new Carbon\Carbon($rates[$i]->from_date))->format('d-m-Y') : $rates[$i]->from_date;
                        $newRates[$i]['to'] = $rates[$i]->to_date? (new Carbon\Carbon($rates[$i]->to_date))->format('d-m-Y') : $rates[$i]->to_date;
                        $newRates[$i]['unit'] = $rates[$i]->unit;
                        $newRates[$i]['unit_cost'] = $rates[$i]->unit_cost;
                    }
                    $ratesJson = json_encode($newRates);
                @endphp
            @endif
            <div id="app">
                <input type="hidden" id="rates_hidden" name="housing_rates" :value="JSON.stringify(rates)" value="{{ old('housing_rates', isset($package->rates->data) ? $ratesJson : '[]') }}">
                <div class="row" v-for="(rate, i) in rates">
                    <div class="col-sm-11" style="padding-left: 0;">
                        <div class="col-sm-2">
                            <div class="form-group" :class="{ 'has-error': (showErrorMessage(i, 'quantity') != '') }">
                                <label class="control-label label-line">Quantity*</label>
                                <input type="number" class="form-control" step="any" v-model="rate.quantity" {{ $readOnly }}>
                                <span class="help-block">
                                    <span class="help-block">
                                        @{{ showErrorMessage(i, 'quantity') }}
                                    </span>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group" :class="{ 'has-error': (showErrorMessage(i, 'unit') != '') }">
                                <label class="control-label label-line">Unit*</label>
                                <select class="form-control select2-v" v-model="rate.unit" {{ $readOnly == 'readonly'? 'disabled' : '' }}>
                                    @foreach ($units as $unit)
                                    <option value="{{ $unit['slug'] }}">{{ $unit['name'] }}</option>
                                    @endforeach
                                </select>
                                <span class="help-block">
                                    @{{ showErrorMessage(i, 'unit') }}
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" :class="{ 'has-error': (showErrorMessage(i, 'from') != '') }">
                                <label class="control-label label-line" for="from">From</label>
                                <div class="input-group">
                                    <date-picker v-model="rate.from" :config="datePickerConfig" {{ $readOnly }}></date-picker>
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                    </span>
                                </div>
                                <span class="help-block">
                                    @{{ showErrorMessage(i, 'from') }}
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group" :class="{ 'has-error': (showErrorMessage(i, 'to') != '') }">
                                <label class="control-label label-line" for="to">To</label>
                                <div class="input-group">
                                    <date-picker v-model="rate.to" :config="datePickerConfig" {{ $readOnly }}></date-picker>
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                    </span>
                                </div>
                                <span class="help-block">
                                    @{{ showErrorMessage(i, 'to') }}
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group" :class="{ 'has-error': (showErrorMessage(i, 'unit_cost') != '') }">
                                <label class="control-label label-line" for="">Unit cost*</label>
                                <input type="number" class="form-control" value="" step="any" v-model="rate.unit_cost" {{ $readOnly }}>
                                <span class="help-block">
                                    @{{ showErrorMessage(i, 'unit_cost') }}
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1">
                        @if($readOnly != 'readonly')
                        <div style="display: flex; align-items: center; height: 76px;">
                            <button type="button" class="btn table-btn table-btn-destroy" @click="deleteItemDynamicList(rate, rates, null, {keep: 1, itemBase: { 'quantity': '', 'unit': '', 'from': '', 'to': '', 'unit_cost': '' }})">
                                <i class="fa fa-trash-o fa--size-1-3" aria-hidden="true"></i>
                            </button>
                        </div>
                        @endif
                    </div>
                </div>
                @if($readOnly != 'readonly')
                <button id="add-rate" type="button" class="btn btn-primary" style="margin-bottom: 10px;" @click="addItemDynamicList({ 'quantity': '', 'unit': '', 'from': '', 'to': '', 'unit_cost': '' }, rates)">Add rate</button>
                @endif
            </div>
        </div>
        <div class="box-footer">
            <a class="btn btn-default" href="{{ url()->previous() }}">
                @if ($formType !== Constant::FORM_SHOW)
                    Cancel
                @else
                    Back
                @endif
            </a>
            @if ($formType !== Constant::FORM_SHOW)
                <button class="btn btn-primary" type="submit" name="action">Save</button>
            @else
                <a href="{{ route('housing.rooms.packages.edit', ['id' => $package->housingRoomPackage->id, 'housing-room' => $housingRoom]) }}" class="btn btn-primary">Edit</a>
            @endif
        </div>
    </div>
</form>
@endsection

@section('scripts')
    @parent
    <script src="{{ asset('js/vue-bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('js/vue.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(document).on('select2:select', '.select2-v', function(e) {
                $(e.target).trigger2('change');
            });

            Vue.component('date-picker', VueBootstrapDatetimePicker.default);
            new Vue({
                el: '#app',
                data: {
                    rates: JSON.parse(document.querySelector('#rates_hidden').value || '[]'),
                    datePickerConfig: {
                        format: 'DD-MM-YYYY',
                        useCurrent: false,
                        showClear: false,
                        showClose: false,
                    },
                    errors: {!! json_encode($errors->get('housing_rates.*')) !!}
                },
                methods: {
                    /**
                    * Delete item from dynamic list.
                    * @item element to delete
                    * @items array with all items
                    * @itemsDeleted array of deleted items [{ id: 1 }, {id: 9}]
                    * @keepElements If a item will never deleted but clear content: {keep: 1, itemBase: {from: '', to: '', pax: ''}}
                    */
                    deleteItemDynamicList: function(item, items, itemsDeleted, keepElements) {
                        swal({
                            title: "Delete rate",
                            text: "Are you sure to delete the selected rate",
                            type: 'question',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, do it!'
                        }).then(function () {
                            var index = items.indexOf(item);
                            // Log id of items deleted
                            if (itemsDeleted) {
                                var itemId = items[index].id || null;
                                if (itemId) {
                                    itemsDeleted.push({ id: itemId });
                                }
                            }
                            // Keep item but clear contents
                            if (keepElements) {
                                var keep = keepElements.keep || null,
                                    itemsTotal = items.length;
                                if (keep && (itemsTotal <= keep)) {
                                    items.splice(index, 1, keepElements. itemBase);
                                    return;
                                }
                            }
                            // Delete item
                            items.splice(index, 1);
                        }).catch(swal.noop);
                    },
                    addItemDynamicList: function(itemBase, items) {
                        items.push(itemBase);
                    },
                    showErrorMessage: function(index, field) {
                        return this.errors['housing_rates.' + index + "." + field]? this.errors['housing_rates.' + index + "." + field][0] : '';
                    }
                },
                watch: {
                    rates: function() {
                        var self = this;
                        setTimeout(function() {
                            $('.select2-v').select2();
                        });
                    }
                },
                mounted: function() {
                    $('.select2-v').select2();
                    @if (empty(old('housing_rates')))
                        @if($formType !== Constant::FORM_EDIT && $formType !== Constant::FORM_SHOW)
                            $('#add-rate').trigger('click');
                        @endif
                    @endif
                }
            });
        });
    </script>
@endsection
