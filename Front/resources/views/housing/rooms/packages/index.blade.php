@extends('layouts.app')

@section('title')
    Housing room options
@endsection

@section('content-header')
    <h1>Housing / Room options</h1>
@endsection

@section('content')
<div class="box">
    <div class="box-header with-border">
        <a href="{{ route('housing.rooms.packages.create', ['id' => $housingRoom->id]) }}" class="btn btn-primary pull-right">New room option</a>
    </div>
    <div class="box-body">
        <div class="panel panel-primary no-margin no-border">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Housing room:
                    <span class="text-capitalize">{{ $housingRoom->name }}</span>
                </h3>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">View</th>
                            <th class="text-center">Edit</th>
                            <th class="text-center">Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($housingRoom->packages->data as $p)
                        <tr>
                            <td>{{ $p->name }}</td>
                            <td class="text-center fit">
                                <form action="{{ route('housing.rooms.packages.activate', ['id' => $p->housingRoomPackage->id]) }}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}
                                    <button type="submit" class="action-btn table-btn-activate" style="opacity: 1;" title="{{ $p->housingRoomPackage->is_active == 1? 'Deactivate' : 'Activate' }}">
                                        <i class="fa fa-flag {{ $p->housingRoomPackage->is_active == 1 ? 'on' : 'off' }}" aria-hidden="true"></i>
                                    </button>
                                </form>
                            </td>
                            <td class="text-center fit">
                                <a class="action-btn" href="{{ route('housing.rooms.packages.show', ['id' => $p->housingRoomPackage->id, 'housing-room' => $housingRoom->id]) }}">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </a>
                            </td>
                            <td class="text-center fit">
                                <a class="action-btn" href="{{ route('housing.rooms.packages.edit', ['id' => $p->housingRoomPackage->id, 'housing-room' => $housingRoom->id]) }}">
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </a>
                            </td>
                            <td class="text-center fit">
                                <form action="{{ route('housing.rooms.packages.destroy', ['id' => $p->housingRoomPackage->id]) }}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button type="submit" class="action-btn table-btn-destroy">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @empty
                            <tr><td colspan="6">No data</td></tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <a href="{{ route('housing.rooms.index', ['housing' => $housingId]) }}"class="btn btn-primary">Back</a>
    </div>
</div>
<script type="text/javascript">
    window.addEventListener('load', function() {
        EXP.initConfirmButton({
            selector: 'button.table-btn-destroy',
            title: "Delete housing room package",
            msg: "This action is going to delete the current housing room package.",
            callback: function(btn) {
                $(btn).parent().submit();
            }
        });

        EXP.initConfirmButton({
            selector: 'button.table-btn-activate',
            title: "Activate/deactivate housing room package",
            msg: "This action is going to activate/deactivate the current housing room package.",
            callback: function(btn) {
                $(btn).parent().submit();
            }
        });
    });
</script>
@endsection
