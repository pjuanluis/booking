@extends('layouts.app')

@section('title')
    {{ $title }}
@endsection

@section('content-header')
    <h1>{{ $title }}</h1>
@endsection

@php
    $readOnly = ($formType === Constant::FORM_SHOW) ? 'readonly' : '';
@endphp

@section('content')
<form id="client-form" action="{{ $formType === Constant::FORM_SHOW ? '#' : $formAction }}" method="POST">
    {{ csrf_field() }}
    @if ($formType === Constant::FORM_EDIT)
        {{ method_field('PUT') }}
        <input type="hidden" name="updated_at" value="{{ $housingRoom->updated_at }}" />
    @endif
    <div class="box" style="">
        <div class="box-body">
            @php
                $currentRoomTypeId = old('room_type_id', isset($housingRoom)? $housingRoom->roomType->data->id : '');
                $costType = old('cost_type', isset($housingRoom)? $housingRoom->cost_type : '');
            @endphp
            <div class="form-group {{ $errors->has('room_type_id') ? 'has-error' : '' }}">
                <label for="room_type_id" class="control-label">Room type*</label>
                <select class="form-control select2" name="room_type_id" {{ $readOnly == 'readonly'? 'disabled' : '' }}>
                    @foreach ($roomTypes->data as $key => $roomType)
                        <option value="{{ $roomType->id }}" {{ $roomType->id == $currentRoomTypeId? 'selected' : '' }}>{{ $roomType->name }}</option>
                    @endforeach
                </select>
                <span class="help-block">{{ $errors->first('room_type_id') }}</span>
            </div>
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name" class="control-label">Name*</label>
                <input id="name" type="text" class="form-control"
                    name="name" value="{{ old('name', isset($housingRoom)? $housingRoom->name : '') }}" {{ $readOnly }} required>
                <span class="help-block">{{ $errors->first('name') }}</span>
            </div>
            <div class="form-group {{ $errors->has('beds') ? 'has-error' : '' }}">
                <label for="beds" class="control-label">Beds*</label>
                <input id="beds" type="number" class="form-control"
                    name="beds" min="1" value="{{ old('beds', isset($housingRoom)? $housingRoom->beds : '1') }}" {{ $readOnly }} required>
                <span class="help-block">{{ $errors->first('beds') }}</span>
            </div>
            <div class="form-group {{ $errors->has('cost_type') ? 'has-error' : '' }}">
                <label for="cost_type" class="control-label" style="display: block;">Cost type</label>
                <label class="radio-inline">
                    <input type="radio" name="cost_type" class="iCheck" checked value="bed" {{ $readOnly == 'readonly'? 'disabled' : '' }}> Bed
                </label>
                <label class="radio-inline">
                    <input type="radio" name="cost_type" class="iCheck" {{ $costType == 'room'? 'checked' : '' }} value="room"  {{ $readOnly == 'readonly'? 'disabled' : '' }}> Room
                </label>
                <span class="help-block">{{ $errors->first('cost_type') }}</span>
            </div>
            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="description" class="control-label">Description</label>
                <textarea name="description" id="description" class="form-control" rows="8" cols="80" {{ $readOnly }}>{{ old('description', isset($housingRoom)? $housingRoom->description : '') }}</textarea>
                <span class="help-block">{{ $errors->first('description') }}</span>
            </div>
        </div>
        <div class="box-footer">
            <a class="btn btn-default" href="{{ url()->previous() }}">
                @if ($formType !== Constant::FORM_SHOW)
                    Cancel
                @else
                    Back
                @endif
            </a>
            @if ($formType !== Constant::FORM_SHOW)
                <button class="btn btn-primary" type="submit" name="action">Save</button>
            @else
                <a href="{{ route('housing.rooms.edit', ['id' => $housingRoom->id, 'housing' => $housing]) }}" class="btn btn-primary">Edit</a>
            @endif
        </div>
    </div>
</form>

@endsection
