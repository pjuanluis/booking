@extends('layouts.app')

@section('title')
    Housing rooms
@endsection

@section('content-header')
    <h1>Housing / Room types</h1>
@endsection

@section('content')
<div class="box">
    <div class="box-header with-border">
        <a href="{{ route('housing.rooms.create', ['id' => $housing->id]) }}" class="btn btn-primary pull-right">New room type</a>
    </div>
    <div class="box-body">
        <div class="panel panel-primary no-margin no-border">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Housing:
                    <span class="text-capitalize">{{ $housing->name }}</span>
                </h3>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Beds</th>
                            <th>Cost type</th>
                            <th class="text-center">Room options</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">View</th>
                            <th class="text-center">Edit</th>
                            <th class="text-center">Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($housing->housingRooms->data as $hr)
                        <tr>
                            <td>{{ $hr->name }}</td>
                            <td>{{ $hr->beds }}</td>
                            <td>{{ $hr->cost_type }}</td>
                            <td class="text-center">
                                <a class="action-btn" href="{{ route('housing.rooms.packages.index', ['id' => $hr->id, 'housing' => $housing->id]) }}">
                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                </a>
                            </td>
                            <td class="text-center fit">
                                <form action="{{ route('housing.rooms.activate', ['id' => $hr->id]) }}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}
                                    <button type="submit" class="action-btn table-btn-activate" style="opacity: 1;" title="{{ $hr->is_active == 1? 'Deactivate' : 'Activate' }}">
                                        <i class="fa fa-flag {{ $hr->is_active == 1 ? 'on' : 'off' }}" aria-hidden="true"></i>
                                    </button>
                                </form>
                            </td>
                            <td class="text-center fit">
                                <a class="action-btn" href="{{ route('housing.rooms.show', ['id' => $hr->id, 'housing' => $housing->id]) }}">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </a>
                            </td>
                            <td class="text-center fit">
                                <a class="action-btn" href="{{ route('housing.rooms.edit', ['id' => $hr->id, 'housing' => $housing->id]) }}">
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </a>
                            </td>
                            <td class="text-center fit">
                                <form action="{{ route('housing.rooms.destroy', ['id' => $hr->id]) }}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button type="submit" class="action-btn table-btn-destroy">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @empty
                            <tr><td colspan="6">No data</td></tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <a href="{{ route('housing.index') }}"class="btn btn-primary">Back</a>
    </div>
</div>
<script type="text/javascript">
    window.addEventListener('load', function() {
        EXP.initConfirmButton({
            selector: 'button.table-btn-destroy',
            title: "Delete housing room",
            msg: "This action is going to delete the current housing room.",
            callback: function(btn) {
                $(btn).parent().submit();
            }
        });

        EXP.initConfirmButton({
            selector: 'button.table-btn-activate',
            title: "Activate/deactivate housing room",
            msg: "This action is going to activate/deactivate the current housing room.",
            callback: function(btn) {
                $(btn).parent().submit();
            }
        });
    });
</script>
@endsection
