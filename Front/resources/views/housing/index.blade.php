@extends('layouts.app')

@section('title')
    Housing Residences
@endsection

@section('content-header')
    <h1>Housing types</h1>
@endsection

@section('content')
<div class="box">
    <div class="box-header with-border">
        <a href="{{ route('housing.create') }}" class="btn btn-primary pull-right">New Housing</a>
    </div>
    <div class="box-body">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Name</th>
                    <th class="text-center">Residences</th>
                    <th class="text-center">Room types</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">View</th>
                    <th class="text-center">Edit</th>
                    <th class="text-center">Delete</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($housing->data as $h)
                <tr>
                    <td>{{ $h->name }}</td>
                    <td class="text-center">
                        <a class="action-btn" href="{{ route('residences.index', ['housing' => $h->id]) }}">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="action-btn" href="{{ route('housing.rooms.index', ['id' => $h->id]) }}">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </a>
                    </td>
                    <td class="text-center fit">
                        <form action="{{ route('housing.activate', [ 'id' => $h->id]) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <button type="submit" class="action-btn table-btn-activate" style="opacity: 1;" title="{{ $h->is_active == 1? 'Deactivate' : 'Activate' }}">
                                <i class="fa fa-flag {{ $h->is_active == 1 ? 'on' : 'off' }}" aria-hidden="true"></i>
                            </button>
                        </form>
                    </td>
                    <td class="text-center fit">
                        <a class="action-btn" href="{{ route('housing.show', ['id' => $h->id]) }}">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </a>
                    </td>
                    <td class="text-center fit">
                        <a class="action-btn" href="{{ route('housing.edit', ['id' => $h->id]) }}">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                    </td>
                    <td class="text-center fit">
                        <form action="{{ route('housing.destroy', [ 'id' => $h->id]) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="action-btn table-btn-destroy">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
    window.addEventListener('load', function() {
        EXP.initConfirmButton({
            selector: 'button.table-btn-destroy',
            title: "Delete housing",
            msg: "This action is going to delete the current housing.",
            callback: function(btn) {
                $(btn).parent().submit();
            }
        });

        EXP.initConfirmButton({
            selector: 'button.table-btn-activate',
            title: "Activate/deactivate housing",
            msg: "This action is going to activate/deactivate the current housing.",
            callback: function(btn) {
                $(btn).parent().submit();
            }
        });
    });
</script>
@endsection
