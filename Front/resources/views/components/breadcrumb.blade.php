@if(!empty($items))
    <ol class="breadcrumb">
        @foreach($items as $item)
            <li class="{{ empty($item['href']) ? 'active' : '' }}">
                @if(empty($item['href']))
                    {{ $item['label'] }}
                @else
                    <a href="{{ $item['href'] }}">{{ $item['label'] }}</a>
                @endif
            </li>
        @endforeach
    </ol>
@endif