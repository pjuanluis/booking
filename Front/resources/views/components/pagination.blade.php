@if (!empty($paginate))
    <div class="box-footer clearfix">
        <ul class="pagination no-margin pull-right">
            @foreach($paginate as $item)
                <li class="page-item{{ empty($item['href']) ? ' disabled' : '' }}{{ !empty($item['active']) ? ' ' . $item['active'] : '' }}">
                    @if (empty($item['href']))
                        <span class="page-link">{{ $item['text'] }}</span>
                    @else
                        <a class="page-link" href="{{ $item['href'] }}">{{ $item['text'] }}</a>
                    @endif
                </li>
            @endforeach
        </ul>
    </div>
@endif