<textarea 
    @if(!empty($attributes))
        @foreach($attributes as $k => $v)
            {{ $k }}="{{ $v }}"
        @endforeach
    @endif
>{{ $value }}</textarea>