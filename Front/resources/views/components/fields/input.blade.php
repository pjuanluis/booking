<input 
    @if(!empty($attributes))
        @foreach($attributes as $k => $v)
            {{ $k }}="{{ $v }}"
        @endforeach
    @endif
>