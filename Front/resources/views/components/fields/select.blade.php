<select 
    @if(!empty($attributes))
        @foreach ($attributes as $k => $v)
            {{ $k }}="{{ $v }}"
        @endforeach
    @endif
>
    @foreach ($options as $v)
        <option value="$v['value']">{{ $v['label'] }}</option>
    @endforeach
</select>