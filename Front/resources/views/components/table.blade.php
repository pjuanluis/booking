<div class="box">
    <div class="box-header">
        <h3 class="box-title">{{ $tableTitle }}</h3>
    </div>
    <div class="box-body">
        <div class="dataTables_wrapper form-inline dt-bootstrap">
            <div class="row">
                <div class="col-xs-12">
                    <table id="{{ $tableId }}" class="table table-bordered table-striped dataTable crud-table" role="grid">
                        <thead>
                            <tr>
                                @foreach($titles as $title)
                                    <th class="sorting" aria-controls="{{ $tableId }}">{{ $title }}</th>
                                @endforeach
                                @if(!empty($actions))
                                    <th>Actions</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($records as $record)
                                <tr>
                                    @foreach($record as $data)
                                        <td>{{ $data }}</td>
                                    @endforeach
                                    @if(!empty($actions))
                                        <td>
                                            @foreach($actions as $action)
                                                {{ $action }}
                                            @endforeach
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                        </tfoot>
                            <tr>
                                @foreach($titles as $title)
                                    <th class="sorting">{{ $title }}</th>
                                @endforeach
                                @if(!empty($actions))
                                    <th>Actions</th>
                                @endif
                            </tr>
                        </tfoot>
                    </table>
                </div> {{-- table /.col-xs-12 --}}
            </div> {{-- table /.row --}}
        </div> {{-- /.dataTables_wrapper --}}
    </div> {{-- /.box-body --}}
</div> {{-- /.box --}}