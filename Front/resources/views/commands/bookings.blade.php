@extends('layouts.app')

@section('title')
    New Bookings
@endsection

@section('content-header')
    <h1>
        @if (Request::get('command-type') === 'booking')
            New Bookings
        @elseif (Request::get('command-type') === 'confirmed')
            Confirmed Bookings
        @else
            Bookings
        @endif
    </h1>
@endsection

@php
    function getParam($field, $default)
    {
        return Request::get($field, $default);
    }
@endphp

@section('content')

<div class="box" style="min-height: 570px;">
    <div class="box-header">
        @php
            $user = Auth::user();
        @endphp
        @if (empty($user->authenticatable_type) || $user->authenticatable_type === "App\Agency")
            <a href="{{ route('bookings.create') }}" class="btn btn-primary pull-right">New booking</a>
        @endif
    </div>
    <div class="box-body">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2 class="panel-title h3">Filter by</h2>
            </div>
            <div class="panel-body">
                <form action="{{ url()->full() }}" method="get">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Booking Reference</label><br />
                                <input type="text" class="form-control" placeholder="WWW-1807-00000" name="booking_code" value="{{ getParam('booking_code', '') }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Experiences</label><br />
                        @php
                            $experienceSelected = getParam('experiences', []);
                            if (!is_array($experienceSelected) && $experienceSelected) {
                                $experienceSelected = explode(',', $experienceSelected);
                            }
                        @endphp
                        @foreach ($experiences->data as $exp)
                            <label class="checkbox-inline" style="padding-left: 0;">
                                <input type="checkbox" class="iCheck" value="{{ $exp->slug }}" name="experiences[]" {{ in_array($exp->slug, $experienceSelected)? 'checked' : '' }}> {{ $exp->name }}
                            </label>
                        @endforeach
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            @php
                                $clientSelected = getParam('client_id', '');
                                $clientName = getParam('client_name', '');
                            @endphp
                            <label>Client</label>
                            <select class="form-control select2" name="client_id" id="client_id">
                                @if ($clientSelected && $clientName)
                                    <option value="{{ $clientSelected }}">{{ $clientName }}</option>
                                @endif
                            </select>
                            <input type="hidden" name="client_name" id="client_name" value="{{ $clientName }}">
                        </div>
                        <div class="form-group col-sm-6">
                            @php
                                $registrationFee = getParam('registration_fee', '');
                            @endphp
                            <label class="control-label">Registration fee</label><br />
                            <label class="checkbox-inline" style="padding-left: 0;">
                                <input type="radio" class="iCheck" value="paid" name="registration_fee" {{ $registrationFee === 'paid'? 'checked' : '' }}> Paid
                            </label>
                            <label class="checkbox-inline" style="padding-left: 0;">
                                <input type="radio" class="iCheck" value="not-paid" name="registration_fee" {{ $registrationFee === 'not-paid'? 'checked' : '' }}> Not paid
                            </label>
                            <label class="checkbox-inline" style="padding-left: 0;">
                                <input type="radio" class="iCheck" value="" name="registration_fee" {{ !$registrationFee? 'checked' : '' }}> None
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label>Command start</label>
                            <div class="input-group">
                                <input type="text" class="form-control dpicker-local" name="from_date" placeholder="{{ \Carbon\Carbon::now()->format('d-m-Y') }}" value="{{ getParam('from_date', '') }}">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Command end</label>
                            <div class="input-group">
                                <input type="text" class="form-control dpicker-local" name="to_date" placeholder="{{ \Carbon\Carbon::now()->format('d-m-Y') }}" value="{{ getParam('to_date', '') }}">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="filter-date">Arrival date</label>
                            <div class="input-group">
                                <input type="text" class="form-control dpicker-local" name="arrival_at" placeholder="{{ \Carbon\Carbon::now()->format('d-m-Y') }}" value="{{ getParam('arrival_at', '') }}">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Departure date</label>
                            <div class="input-group">
                                <input type="text" class="form-control dpicker-local" name="departure_at" placeholder="{{ \Carbon\Carbon::now()->format('d-m-Y') }}" value="{{ getParam('departure_at', '') }}">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary" name="is_filter" value="true"><i class="fa fa-filter"></i> Filter</button>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
        @if ($commands->data)
            <div class="">
                <form class="" action="{{ route('bookings.export-excel') }}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="data" value="{{ json_encode($commands->data) }}">
                    <input type="hidden" name="command-type" value="{{ Request::get('command-type', 'filtered') }}">
                    <button type="submit" class="btn btn-primary pull-right" style="margin-left: 5px;">
                        <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                        Export to Excel
                    </button>
                    @if (!Request::has('is_filter'))
                        <button type="button" class="btn btn-primary pull-right" id="btn-export-to-csv">
                            <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                            Export to CSV
                        </button>
                    @endif
                    <div class="clearfix"></div>
                </form>
            </div>
        @endif
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Booking Reference</th>
                        <th>Responsible</th>
                        <th>Client/lead name</th>
                        <th>Reg. date</th>
                        <th>Command start</th>
                        <th>Command end</th>
                        <th class="text-center fit">Payment</th>
                        <th class="text-center fit">Client connected</th>
                        <th class="text-center fit">View</th>
                        @if (empty($user->authenticatable_type) || $user->authenticatable_type === "App\Agency")
                            <th class="text-center fit">Edit</th>
                            <th class="text-center fit">Delete</th>
                            @if (empty($user->authenticatable_type))
                                <th class="text-center fit">Logs</th>
                            @endif
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach ($commands->data as $com)
                        <tr>
                            <td>{{ $com->booking_code }}</td>
                            <td>{{ $com->purchase->data->user->data->full_name ?? '--' }}</td>
                            <td>{{ $com->client_name }}</td>
                            <td>{{ (new Carbon\Carbon(explode(' ', $com->created_at)[0]))->format('d-m-Y') }}</td>
                            <td>{{ $com->from_date? (new Carbon\Carbon($com->from_date))->format('d-m-Y') : '' }}</td>
                            <td>{{ $com->to_date? (new Carbon\Carbon($com->to_date))->format('d-m-Y') : '' }}</td>
                            <td class="text-center">
                                <span class="table-icon">
                                    @php
                                        $classStatus = 'fa-check text-green';
                                        $text = 'Paid';
                                        if ($com->payment_status === 'partially_paid') {
                                            $classStatus = 'fa-check text-yellow';
                                            $text = 'Partially paid';
                                        } elseif ($com->payment_status === 'not_payed') {
                                            $classStatus = 'fa-times text-red';
                                            $text = 'Not payed';
                                        }
                                    @endphp
                                    <i class="fa {{ $classStatus }}" aria-hidden="true" title="{{ $text }}"></i>
                                </span>
                            </td>
                            <td class="text-center"><span class="table-icon"><i class="fa {{ $com->hasClientData ? 'fa-check text-green' : 'fa-times text-red' }}" aria-hidden="true"></i></span></td>
                            <td class="text-center">
                                <a href="{{ route('bookings.show', ['id' => $com->id]) }}" class="action-btn">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </a>
                            </td>
                            @if (empty($user->authenticatable_type) || $user->authenticatable_type === "App\Agency")
                                <td class="text-center">
                                    <a href="{{ route('bookings.edit', ['id' => $com->id]) }}" class="action-btn">
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                    </a>
                                </td>
                                <td class="text-center">
                                    <form action="{{ route('bookings.destroy', [ 'id' => $com->id]) }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button type="submit" class="action-btn table-btn-destroy">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                                        </button>
                                    </form>
                                </td>
                                @if (empty($user->authenticatable_type))
                                    <td class="text-center">
                                        <a href="{{ route('bookings.logs', ['id' => $com->id]) }}" class="action-btn">
                                            <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                @endif
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {!! Component::pagination(
                10,
                $commands->meta->pagination->current_page,
                $commands->meta->pagination->total_pages,
                $params
                ) !!}
        </div>
    </div>
</div>
@endsection

@section('scripts')
    @parent
    <script type="text/javascript">
        $(function() {
            EXP.initConfirmButton({
                selector: 'button.table-btn-destroy',
                title: "Delete booking",
                msg: "This action is going to delete the current booking.",
                callback: function(btn) {
                    $(btn).parent().submit();
                }
            });
            EXP.initAutocomplete(document.getElementById('client_id'), {
                url: '/admin/clients',
                paramName: 'name',
                displayField: 'full_name',
                disabled: false
            });
            $('#client_id').on('select2:select', function() {
                $('#client_name').val($('#select2-client_id-container').text());
            });
            @if ($commands->data && !Request::has('is_filter'))
            EXP.Downloader({
                el: '#btn-export-to-csv',
                url: '{{ route('bookings.export-csv') }}',
                filename: 'bookings.csv',
                mimetype: 'application/csv',
            }, function(request) {
                let params = location.search.substring(1);
                request.send("_token={{ csrf_token() }}" + (params != ''? '&' + params : ''));
            });
            @endif
        });
    </script>
@endsection
