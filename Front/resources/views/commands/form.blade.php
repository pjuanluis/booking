@extends('layouts.app')

@section('title')
    @if ($formType === Constant::FORM_CREATE)
        Create booking
    @elseif ($formType === Constant::FORM_EDIT)
        Edit booking
    @elseif ($formType === Constant::FORM_SHOW)
        View booking
    @endif
@endsection

@section('content-header')
    @if ($formType === Constant::FORM_CREATE)
        <h1>Create booking</h1>
    @elseif ($formType === Constant::FORM_EDIT)
        <h1>Edit booking</h1>
    @elseif ($formType === Constant::FORM_SHOW)
        <h1>View booking</h1>
    @endif
@endsection

@section('content')
<style>
    .grid-container table i.fa {
        margin-right: 10px;
        cursor: pointer;
    }
    .cmd-record-info.has-error .help-block {
        display: block;
        color: #dd4b39;
        padding: 0 24px;
    }
    .cmd-record-info.has-error .help-block > ul {
        list-style-type: none;
    }
    .cmd-record-info.has-error .col-xs-12 .divider {
        border-top: solid 1px #dd4b39 !important;
    }
    .cmd-record-info.has-error table tbody tr.has-error {
        color: #dd4b39;
        background-color: #f9dede;
    }
    .total {
        margin: 0 10px 0;
        display: inline-block;
    }
    .total_currency {
        margin-right: 2px;
    }
</style>
<?php
function getClientData($field, $client, $formType) {
    if ($field === 'has_alergies' || $field === 'has_diseases' || $field === 'is_smoker' || $field === 'has_volunteer_experience') {
        if (old($field) === '0') {
            return '0';
        }
    }
    if (old($field)) {
        return old($field);
    }
    if ($formType === Constant::FORM_EDIT || $formType === Constant::FORM_SHOW) {
        if ($field === 'gender_id') {
            return isset($client->gender->data->id) ? $client->gender->data->id : '';
        }
        return isset($client->$field) ? $client->$field : '';
    }
    return '';
}

function getCommandData($field, $command) {
    if (($field === 'client_id' || $field === 'client_name' || $field === 'client_first_name' || $field === 'client_last_name'
            || $field === 'client_country_id' || $field === 'client_country_name' || $field === 'client_email') && !empty($command->data->client->data)) {
        if ($field === 'client_id') {
            return $command->data->client->data->id;
        }
        else if ($field === 'client_name') {
            return $command->data->client->data->full_name;
        }
        else if ($field === 'client_first_name') {
            return $command->data->client->data->first_name;
        }
        else if ($field === 'client_last_name') {
            return $command->data->client->data->last_name;
        }
        else if ($field === 'client_email') {
            return $command->data->client->data->email;
        }
        else if ($field === 'client_country_id') {
            return $command->data->client->data->country_id;
        }
        else if ($field === 'client_country_name') {
            return $command->data->client->data->country;
        }
    }
    else if ($field === 'agency_id') {
        if (!empty($command->data->agency->data)) {
            return $command->data->agency->data->id;
        }
    }
    else if ($field === 'agency_name') {
        if (!empty($command->data->agency->data)) {
            return $command->data->agency->data->name;
        }
    }
    else if ($field === 'pickup_id') {
        if (!empty($command->data->pickup->data)) {
            return $command->data->pickup->data->id;
        }
    }
    else if ($field === 'pickup_name') {
        if (!empty($command->data->pickup->data)) {
            return $command->data->pickup->data->name;
        }
    }
    else if ($field === 'pickup_cost') {
        if (!empty($command->data->pickup->data)) {
            return $command->data->pickup_cost;
        }
    }
    else if ($field === 'pickup_currency_id') {
        if (!empty($command->data->purchase->data)) {
            return $command->data->purchase->data->currency_id;
        }
    }
    else if ($field === 'pickup_discount') {
        if (!empty($command->data->purchase->data)) {
            return $command->data->purchase->data->discount;
        }
    }
    else if ($field === 'pickup_exchange_rates') {
        if (!empty($command->data->purchase->data)) {
            return json_encode($command->data->purchase->data->exchange_rates);
        }
        return '';
    }
    else if ($field === 'current_pickup_exchange_rates') {
        if (!empty($command->data->pickup->data) && !empty($command->data->purchase->data)) {
            return json_encode($command->data->purchase->data->exchange_rates);
        }
        return '';
    }
    else if ($field === 'courses') {
        $data = "[";
        foreach ($command->data->commandCourses->data as $i => $cc) {
            $data .= '' .
                '{' .
                    '"id": ' . $cc->id . ',' .
                    '"experiences": [';
            foreach ($cc->package->data->experiences->data as $j => $e) {
                $data .= "'" . $e->slug . "'";
                if (($j + 1) < count($cc->package->data->experiences->data)) {
                    $data .= ",";
                }
            }
            $data .= "]," .
                    '"package": "' . $cc->package->data->name . '",' .
                    '"package_id": ' . $cc->package->data->id . ',' .
                    '"from_date": "' . $cc->from_date . '",' .
                    '"to_date": "' . $cc->to_date . '",' .
                    '"quantity": ' . $cc->quantity . ',' .
                    '"currency": "' . $cc->purchase->data->currency->data->code . '",' .
                    '"currency_id": ' . $cc->purchase->data->currency->data->id . ',' .
                    '"discount": ' . $cc->purchase->data->discount . ',' .
                    '"exchange_rates": ' . json_encode($cc->purchase->data->exchange_rates) . ',' .
                    '"current_exchange_rates": ' . json_encode($cc->purchase->data->exchange_rates) . ',' .
                    '"cost": ' . $cc->cost .
                '}';
            if ($i < (count($command->data->commandCourses->data) - 1)) {
                $data .= ',';
            }
        }
        $data .= "]";
        return $data;
    }
    else if ($field === 'housing') {
        $data = "[";
        foreach ($command->data->commandHousing->data as $i => $cc) {
            $data .= '' .
                '{' .
                    '"id": ' . $cc->id . ',' .
                    '"command_housing_id": ' . $cc->id . ',' .
                    '"housing_room_package_id": ' . $cc->housing_room_package_id . ',' .
                    '"housing_id": ' . $cc->housing->data->id . ',' .
                    '"housing": "' . $cc->housing->data->name . '",' .
                    '"housing_room_id": ' . $cc->housingRoom->data->id . ',' .
                    '"housing_room": "' . $cc->housingRoom->data->name . '",' .
                    '"package": "' . $cc->package->data->name . '",' .
                    '"package_id": ' . $cc->package->data->id . ',' .
                    '"from_date": "' . $cc->from_date . '",' .
                    '"to_date": "' . $cc->to_date . '",' .
                    '"quantity": ' . $cc->quantity . ',' .
                    '"unit": "' . $cc->unit . '",' .
                    '"cost": ' . $cc->cost . ',' .
                    '"currency": "' . $cc->purchase->data->currency->data->code . '",' .
                    '"currency_id": ' . $cc->purchase->data->currency->data->id . ',' .
                    '"discount": ' . $cc->purchase->data->discount . ',' .
                    '"exchange_rates": ' . json_encode($cc->purchase->data->exchange_rates) . ',' .
                    '"current_exchange_rates": ' . json_encode($cc->purchase->data->exchange_rates) . ',' .
                    '"people": ' . $cc->people . ',' .
                    '"room_id": ' . (!empty($cc->room->data) ? $cc->room->data->id : 'null') . ',' .
                    '"room_number": ' . (!empty($cc->room->data) ? '"' . $cc->room->data->name . '"' : 'null') . ',' .
                    '"residence_id": ' . (!empty($cc->room->data) ? $cc->room->data->residence->data->id : 'null') . ',' .
                    '"residence": ' . (!empty($cc->room->data) ? '"' . $cc->room->data->residence->data->name . '"' : 'null') .
                '}';
            if ($i < (count($command->data->commandHousing->data) - 1)) {
                $data .= ',';
            }
        }
        $data .= "]";
        return $data;
    }
    else if ($field === 'payments' || $field === 'all_payments') {
        $data = "[";
        foreach ($command->data->payments->data as $i => $cc) {
            if ($cc->type === 'registration_fee' || $field === 'all_payments') {
                $data .= '' .
                '{' .
                    '"id": ' . $cc->id . ',' .
                    '"concept": "' . $cc->concept . '",' .
                    '"invoiced_at": "' . $cc->invoiced_at . '",' .
                    '"type": "' . $cc->type . '",' .
                    '"reference_id": "' . $cc->reference_id . '",' .
                    '"paid_at": "' . $cc->paid_at . '",' .
                    '"vendor": "' . $cc->vendor . '",' .
                    '"amount": ' . $cc->amount . ',' .
                    '"currency": "' . $cc->currency->data->code . '",' .
                    '"currency_id": ' . $cc->currency->data->id . ',' .
                    '"payment_method": "' . $cc->paymentMethod->data->name . '",' .
                    '"payment_method_id": ' . $cc->paymentMethod->data->id . ',' .
                    '"exchange_rates": ' . json_encode($cc->exchange_rates) . ',' .
                    '"current_exchange_rates": ' . json_encode($cc->exchange_rates) . ',' .
                '}';
                if ($i < (count($command->data->payments->data) - 1)) {
                    $data .= ',';
                }
            }
        }
        $data .= "]";
        return $data;
    }
    else if ($field === 'purchases') {
        $data = "[";
        foreach ($command->data->extraPurchases->data as $i => $ep) {
            $data .= '' .
                '{' .
                    '"id": ' . $ep->id . ',' .
                    '"reference_number": "' . $ep->reference_number . '",' .
                    '"concept": ' . json_encode($ep->concept) . ',' .
                    '"amount": ' . $ep->amount . ',' .
                    '"currency_id": ' . $ep->purchasable->data->currency->data->id . ',' .
                    '"currency": "' . $ep->purchasable->data->currency->data->code . '",' .
                    '"discount": ' . $ep->purchasable->data->discount . ',' .
                    '"exchange_rates": ' . json_encode($ep->purchasable->data->exchange_rates) . ',' .
                    '"current_exchange_rates": ' . json_encode($ep->purchasable->data->exchange_rates) . ',' .
                    '"consumption_center_id": ' . $ep->purchasable->data->consumptionCenter->data->id . ',' .
                    '"consumption_center": "' . $ep->purchasable->data->consumptionCenter->data->name . '",' .
                    '"date": "' . $ep->date . '",' .
                '}';
            if ($i < (count($command->data->extraPurchases->data) - 1)) {
                $data .= ',';
            }
        }
        $data .= "]";
        return $data;
    }
    else {
        if (isset($command->data->$field)) {
            return $command->data->$field;
        }
    }
    return null;
}
function getData($field, $formType, $command) {
    if (old($field)) {
        return old($field);
    }
    if ($formType === Constant::FORM_CREATE && ($field === 'courses' || $field === 'housing'
            || $field === 'payments' || $field === 'all_payments' || $field === 'purchases')) {
        return '[]';
    }
    if ($formType === Constant::FORM_EDIT || $formType === Constant::FORM_SHOW) {
        return getCommandData($field, $command);
    }
    return null;
}
$client = empty($command->data->client->data)? "" : $command->data->client->data;
$readOnly = ($formType === Constant::FORM_SHOW) ? 'readonly' : '';
$disabled = ($formType === Constant::FORM_SHOW) ? 'disabled' : '';
$imageURL = '';
if (!empty($client->file->data->path)) {
    $imageURL = $client->file->data->path;
}
?>
<form id="command-form" action="{{ $formType === Constant::FORM_SHOW ? '#' : $formAction }}" class="{{ $readOnly }}" method="POST">
    {{ csrf_field() }}
    @if ($formType === Constant::FORM_EDIT)
        {{ method_field('PUT') }}
        <input type="hidden" name="updated_at" value="{{ $command->data->updated_at }}" />
    @endif
    <input type="hidden" id="input-command-courses" name="courses" />
    <input type="hidden" id="input-command-housing" name="housing" />
    <input type="hidden" id="input-command-purchases" name="purchases" />
    <input type="hidden" id="input-command-payments" name="payments" />
    @php
    if ($formType === Constant::FORM_CREATE) {
        $command = null;
    }
    @endphp
    @if (empty($user->authenticatable_type))
    <div class="box" style="">
        <div class="box-header with-border">
            <h3 class="box-title">Booking Information</h3>
        </div>
        <div class="box-body">
            @if ($formType !== Constant::FORM_CREATE)
                <div class="row">
                    <div class="form-group col-sm-6 {{ $errors->has('booking_code') ? 'has-error' : '' }}">
                        <label for="booking-code" class="col-sm-3 control-label label-line">Booking code</label>
                        <div class="col-sm-9">
                            @php
                            $val = getData('booking_code', $formType, $command);
                            @endphp
                            <input type="text" class="form-control" id="booking-code" name="booking_code"
                                    value="{{ $val }}" {{ $readOnly }} {{ ($formType === Constant::FORM_EDIT) ? 'readonly' : '' }}>
                            <span class="help-block">{{ $errors->first('booking_code') }}</span>
                        </div>
                    </div>
                </div>
            @endif

            @if ($formType === Constant::FORM_SHOW)
                <div class="row">
                    <div class="form-group col-sm-6 {{ $errors->has('from_date') ? 'has-error' : '' }}">
                        <label for="begin-date" class="col-sm-3 control-label label-line">Begin date</label>
                        <div class="col-sm-9">
                            @php
                            $val = getData('from_date', $formType, $command);
                            @endphp
                            <input type="text" class="form-control" id="begin-date" name="from_date"
                                    value="{{ empty($val)? $val : (new Carbon\Carbon($val))->format('d-m-Y') }}" readOnly>
                            <span class="help-block">{{ $errors->first('from_date') }}</span>
                        </div>
                    </div>
                    <div class="form-group col-sm-6 {{ $errors->has('to_date') ? 'has-error' : '' }}">
                        <label for="end-date" class="col-sm-3 control-label label-line">End date</label>
                        <div class="col-sm-9">
                            @php
                            $val = getData('to_date', $formType, $command);
                            @endphp
                            <input type="text" class="form-control" id="end-date" name="to_date"
                                    value="{{ empty($val)? $val : (new Carbon\Carbon($val))->format('d-m-Y') }}" readOnly>
                            <span class="help-block">{{ $errors->first('to_date') }}</span>
                        </div>
                    </div>
                </div>
            @endif
            @if (empty($user->authenticatable_type))
            <div class="row">
                <div class="form-group col-sm-6 {{ $errors->has('agency_id') ? 'has-error' : '' }}">
                    <label for="select-agency" class="col-sm-3 control-label label-line">Agency</label>
                    <div class="col-sm-9">
                        <select class="form-control" id="select-agency" name="agency_id" {{ $disabled }}
                                style="width: 100%;">
                            @php
                            $val = getData('agency_id', $formType, $command);
                            $val1 = getData('agency_name', $formType, $command);
                            @endphp
                            <option value=""></option>
                            @foreach ($agencies as $agency)
                                <option value="{{ $agency->id }}" {{ $val == $agency->id ? 'selected' : '' }}>{{ $agency->name }}</option>
                            @endforeach
                        </select>
                        <input type="text" name="agency_name" id="agency_name" value="{{ $val1 }}" style="display: none;"/>
                        <span class="help-block">{{ $errors->first('agency_id') }}</span>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
    @endif

    <div class="box" style="">
        <div class="box-header with-border">
            <h3 class="box-title">Client general information</h3>
        </div>
        <div class="box-body">
            @if ($formType !== Constant::FORM_SHOW)
            <div class="row">
                <div class="form-group col-sm-6 {{ $errors->has('client_id') ? 'has-error' : '' }}">
                    <label for="select-client" class="col-sm-3 control-label label-line">Client</label>
                    <div class="col-sm-9">
                        <select class="form-control" id="select-client" name="client_id" style="width: 100%;">
                            @php
                            $val = getData('client_id', $formType, $command);
                            $clientId = $val;
                            $val1 = getData('client_name', $formType, $command);
                            $val2 = getData('client_email', $formType, $command);
                            $val3 = getData('client_first_name', $formType, $command);
                            $val4 = getData('client_last_name', $formType, $command);
                            $val5 = getData('client_country_id', $formType, $command);
                            $val6 = getClientData('birth_date', $client, $formType);
                            $val7 = getClientData('phone', $client, $formType);
                            $val8 = getClientData('emergency_phone', $client, $formType);
                            $val9 = getClientData('has_alergies', $client, $formType);
                            $val10 = getClientData('alergies', $client, $formType);
                            $val11 = getClientData('has_diseases', $client, $formType);
                            $val12 = getClientData('diseases', $client, $formType);
                            $val13 = getClientData('is_smoker', $client, $formType);
                            $val14 = getClientData('fb_profile', $client, $formType);
                            $val15 = getClientData('spanish_level', $client, $formType);
                            $val16 = getClientData('spanish_knowledge_duration', $client, $formType);
                            $val17 = getClientData('has_surf_experience', $client, $formType);
                            $val18 = getClientData('surf_experience_duration', $client, $formType);
                            $val19 = getClientData('has_volunteer_experience', $client, $formType);
                            $val20 = getClientData('volunteer_experience', $client, $formType);
                            $val21 = getClientData('volunteer_experience_duration', $client, $formType);
                            $val22 = getClientData('gender_id', $client, $formType);
                            @endphp
                            @if (!empty($val))
                                <option value="{{ $val }}" data-email="{{ $val2 }}" data-first_name="{{ $val3 }}" data-last_name="{{ $val4 }}" data-country_id="{{ $val5 }}" data-birth_date="{{ $val6 }}" data-phone="{{ $val7 }}"data-emergency_phone="{{ $val8 }}" data-has_alergies="{{ $val9 }}" data-alergies="{{ $val10 }}" data-has_diseases="{{ $val11 }}" data-diseases="{{ $val12 }}" data-is_smoker="{{ $val13 }}" data-fb_profile="{{ $val14 }}" data-spanish_level="{{ $val15 }}" data-spanish_knowledge_duration="{{ $val16 }}" data-has_surf_experience="{{ $val17 }}" data-surf_experience_duration="{{ $val18 }}" data-has_volunteer_experience="{{ $val19 }}" data-volunteer_experience="{{ $val20 }}" data-volunteer_experience_duration="{{ $val21 }}" data-gender_id="{{ $val22 }}" data-image="{{ $imageURL }}" selected>
                                    {{ $val1 }}
                                </option>
                            @endif
                        </select>
                        <input id="hidden-clientname" type="hidden" name="client_name" value="{{ $val1 }}" />
                        <span class="help-block">{{ $errors->first('client_id') }}</span>
                    </div>
                </div>
                <div class="form-group col-sm-6">
                    <label style="padding-left: 15px;">
                        <input id="chk-newclient" type="checkbox" class="iCheck" {{ empty($val) ? 'checked' : '' }}> New client
                        <input id="hidden-newclient" type="hidden" name="new_client" value="{{ empty($val) ? '1' : '0' }}" />
                    </label>
                </div>
            </div>
            @endif
            <div class="row">
                <div class="form-group col-sm-6 {{ $errors->has('client_first_name') ? 'has-error' : '' }}">
                    <label for="client-firstname" class="col-sm-3 control-label label-line">First name</label>
                    <div class="col-sm-9">
                        @php
                        $val = getData('client_first_name', $formType, $command);
                        @endphp
                        <input type="text" class="form-control" id="client-firstname" name="client_first_name"
                               value="{{ $val }}" {{ !empty($clientId) || $formType === Constant::FORM_SHOW ? 'readOnly' : '' }}>
                        <span class="help-block">{{ $errors->first('client_first_name') }}</span>
                    </div>
                </div>
                <div class="form-group col-sm-6 {{ $errors->has('client_last_name') ? 'has-error' : '' }}">
                    <label for="client-lastname" class="col-sm-3 control-label label-line">Last name</label>
                    <div class="col-sm-9">
                        @php
                        $val = getData('client_last_name', $formType, $command);
                        @endphp
                        <input type="text" class="form-control" id="client-lastname" name="client_last_name"
                                value="{{ $val }}" {{ !empty($clientId) || $formType === Constant::FORM_SHOW ? 'readOnly' : '' }}>
                        <span class="help-block">{{ $errors->first('client_last_name') }}</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-sm-6 {{ $errors->has('client_email') ? 'has-error' : '' }}">
                    <label for="client-email" class="col-sm-3 control-label label-line">Email</label>
                    <div class="col-sm-9">
                        @php
                        $val = getData('client_email', $formType, $command);
                        @endphp
                        <input type="text" class="form-control" id="client-email" name="client_email"
                                value="{{ $val }}" {{ !empty($clientId) || $formType === Constant::FORM_SHOW ? 'readOnly' : '' }}>
                        <span class="help-block">{{ $errors->first('client_email') }}</span>
                    </div>
                </div>
                <div class="form-group col-sm-6 {{ $errors->has('country_id') ? 'has-error' : '' }}">
                    <label for="select-country" class="col-sm-3 control-label">Country</label>
                    <div class="col-sm-9">
                        <select name="country_id" id="select-country" {{ !empty($clientId) || $formType === Constant::FORM_SHOW ? 'disabled' : '' }}
                                class="select2 form-control" style="width: 100%;">
                            @php
                            $val = getData('client_country_id', $formType, $command);
                            $val1 = getData('client_country_name', $formType, $command);
                            @endphp
                            <option value="">- Select a country -</option>
                            @foreach ($countries as $country)
                            <option value="{{ $country->id }}" {{ $val == $country->id ? 'selected' : '' }}>{{ $country->name }}</option>
                            @endforeach
                        </select>
                        <input type="hidden" name="country_name" id="country_name" value="{{ $val1 }}">
                        <span class="help-block">{{ $errors->first('country_id') }}</span>
                    </div>
                </div>
            </div>
            <div id="client_info">
                <div class="row">
                    <div class="form-group col-sm-6">
                        @php
                        $val = getClientData('birth_date', $client, $formType);
                        @endphp
                        <label for="birthdate" class="col-sm-3 control-label label-line">Date of birth</label>
                        <div class="col-sm-9">
                            <input type="text" id="birthdate" class="form-control" value="{{ empty($val)? $val : (new \Carbon\Carbon($val))->format('d-m-Y') }}" readonly/>
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                        @php
                        $val = getClientData('phone', $client, $formType);
                        @endphp
                        <label for="phone" class="col-sm-3 control-label label-line">Phone</label>
                        <div class="col-sm-9">
                            <input type="text" id="phone" class="form-control" value="{{ $val }}" readonly/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-sm-6">
                        <label for="client-image" class="col-sm-3 control-label label-line">Image</label>
                        <div class="col-sm-9">
                            <img id="client-image" src="{{ $imageURL }}" alt="" style="{{ empty($imageURL) ? 'display: none;' : 'max-width: 120px; display: block;' }}" />
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                        @php
                        $val = getClientData('emergency_phone', $client, $formType);
                        @endphp
                        <label for="emergency-phone" class="col-sm-3 control-label label-line">Emergency phone</label>
                        <div class="col-sm-9">
                            <input id="emergency-phone" type="text" class="form-control" value="{{ $val }}" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-sm-6">
                        @php
                        $val = getClientData('has_alergies', $client, $formType);
                        @endphp
                        <label class="col-sm-3 control-label label-line">Alergies</label>
                        <div class="col-sm-9">
                            <label class="normal-weight">
                                <input type="radio" name="has_alergies" class="iCheck" id="aly" value="1" {{ $val=='1' ? 'checked' : '' }} disabled/> Yes
                            </label>
                            <label class="normal-weight">
                                <input type="radio" name="has_alergies" class="iCheck" id="aln" value="0" {{ $val=='0' ? 'checked' : '' }} disabled/> No
                            </label>
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                        @php
                            $val = getClientData('alergies', $client, $formType);
                        @endphp
                        <label for="alergies" class="col-sm-3 control-label label-line">Specify</label>
                        <div class="col-sm-9">
                            <input id="alergies" type="text" class="form-control" value="{{ $val }}" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-sm-6">
                        @php
                            $val = getClientData('has_diseases', $client, $formType);
                        @endphp
                        <label class="col-sm-3 control-label label-line">Diseases</label>
                        <div class="col-sm-9">
                            <label class="normal-weight">
                                <input type="radio" name="has_diseases" class="iCheck" id="disy" value="1" {{ $val=='1' ? 'checked' : '' }} disabled/> Yes
                            </label>
                            <label class="normal-weight">
                                <input type="radio" name="has_diseases" class="iCheck" id="disn" value="0" {{ $val=='0' ? 'checked' : '' }} disabled/> No
                            </label>
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                        @php
                            $val = getClientData('diseases', $client, $formType);
                        @endphp
                        <label for="diseases" class="col-sm-3 control-label label-line">Specify</label>
                        <div class="col-sm-9">
                            <input id="diseases" type="text" class="form-control" value="{{ $val }}" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-sm-6">
                        @php
                            $val = getClientData('is_smoker', $client, $formType);
                        @endphp
                        <label class="col-sm-3 control-label label-line">Is smoker?</label>
                        <div class="col-sm-9">
                            <label class="normal-weight">
                                <input type="radio" name="is_smoker" class="iCheck" id="smoky" value="1" {{ $val=='1' ? 'checked' : '' }} disabled/> Yes
                            </label>
                            <label class="normal-weight">
                                <input type="radio" name="is_smoker" class="iCheck" id="smokn" value="0" {{ $val=='0' ? 'checked' : '' }} disabled/> No
                            </label>
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                        @php
                            $val = getClientData('fb_profile', $client, $formType);
                        @endphp
                        <label for="fb-profile" class="col-sm-3 control-label label-line">Facebook URL or profile</label>
                        <div class="col-sm-9">
                            <input id="fb-profile" type="text" class="form-control" value="{{ $val }}" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-sm-6">
                        @php
                            $val = getClientData('gender_id', $client, $formType);
                        @endphp
                        <label class="col-sm-3 control-label label-line">Gender</label>
                        <div class="col-sm-9">
                            @foreach ($genders->data as $gender)
                                <label class="normal-weight" >
                                    <input type="radio" class="iCheck" value="{{ $gender->id }}" name="gender_id" {{ $val == $gender->id? 'checked' : '' }} disabled> {{ $gender->name }}
                                </label>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-12 control-label label-line">Spanish knowledge</label>
                    @php
                        $val = getClientData('spanish_level', $client, $formType);
                    @endphp
                    <div class="col-sm-12">
                        <div class="col-sm-3">
                            <label for="splev0">
                                <input id="splev0" type="radio" name="spanish_level" class="iCheck opt-spanish-level" value="none" {{ (empty($val) || $val == 'none') ? 'checked' : '' }} disabled> None
                            </label>
                        </div>
                        <div class="col-sm-3">
                            <label for="splev1">
                                <input id="splev1" type="radio" name="spanish_level" class="iCheck opt-spanish-level" value="begginer" {{ $val == 'begginer' ? 'checked' : '' }} disabled> Begginer
                            </label>
                        </div>
                        <div class="col-sm-3">
                            <label for="splev2">
                                <input id="splev2" type="radio" name="spanish_level" class="iCheck opt-spanish-level" value="middle"  {{ $val=='middle' ? 'checked' : '' }} disabled> Middle
                            </label>
                        </div>
                        <div class="col-sm-3">
                            <label for="splev3">
                                <input id="splev3" type="radio" name="spanish_level" class="iCheck opt-spanish-level" value="advanced" {{ $val=='advanced' ? 'checked' : '' }} disabled> Advanced
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        @php
                            $val = getClientData('spanish_knowledge_duration', $client, $formType);
                            $tmp = !empty($val) ? explode(' ', $val) : ['', ''];
                            $val = $tmp[0];
                        @endphp
                        <label class="col-sm-12 control-label label-line">How long</label>
                        <div class="col-sm-6">
                            <div class="col-sm-6">
                                <input id="spanish_knowledge_duration_number" type="number" class="form-control" value="{{ !empty($val) ? $val : 0 }}" readonly>
                            </div>
                            <div class="col-sm-6">
                                <select id="spanish_knowledge_duration_unit" disabled class="select2 form-control">
                                    @php
                                        $val = $tmp[1];
                                    @endphp
                                    <option value="month" {{ $val == 'month' ? 'selected' : '' }}>Months</option>
                                    <option value="year" {{ $val == 'year' ? 'selected' : '' }}>Years</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-12 control-label label-line">Surf Experience</label>
                    @php
                        $val = getClientData('has_surf_experience', $client, $formType);
                    @endphp
                    <div class="col-sm-12">
                        <div class="col-sm-3">
                            <label for="sexpy">
                                <input id="sexpy" type="radio" name="has_surf_experience" class="iCheck opt-surf-experience with-gap" value="1" {{ $val == '1' ? 'checked' : '' }} disabled> Yes
                            </label>
                        </div>
                        <div class="col-sm-3">
                            <label for="sexpn">
                                <input id="sexpn" type="radio" name="has_surf_experience" class="iCheck opt-surf-experience with-gap" value="0" {{ empty($val) ? 'checked' : '' }} disabled> No
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        @php
                            $val = getClientData('surf_experience_duration', $client, $formType);
                            $tmp = !empty($val) ? explode(' ', $val) : ['', ''];
                            $val = $tmp[0];
                        @endphp
                        <label class="col-sm-12 control-label label-line">How long</label>
                        <div class="col-sm-6">
                            <div class="col-sm-6">
                                <input type="number" id="surf_experience_duration_number" class="form-control" value="{{ !empty($val) ? $val : 0 }}" readonly>
                            </div>
                            <div class="col-sm-6">
                                <select id="surf_experience_duration_unit" disabled class="select2 form-control">
                                    @php
                                        $val = $tmp[1];
                                    @endphp
                                    <option value="month" {{ $val == 'month' ? 'selected' : '' }}>Months</option>
                                    <option value="year" {{ $val == 'year' ? 'selected' : '' }}>Years</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-12 control-label label-line">Volunteer experience</label>
                    @php
                        $val = getClientData('has_volunteer_experience', $client, $formType);
                    @endphp
                    <div class="col-sm-12">
                        <div class="col-sm-3">
                            <input id="vexpy" type="radio" name="has_volunteer_experience" class="iCheck opt-volunteer-experience with-gap" value="1" {{ $val=='1' ? 'checked' : '' }} disabled>
                            <label for="vexpy">Yes</label>
                        </div>
                        <div class="col-sm-3">
                            <input id="vexpn" type="radio" name="has_volunteer_experience" class="iCheck opt-volunteer-experience with-gap" value="0" {{ empty($val) ? 'checked' : '' }} disabled>
                            <label for="vexpn">No</label>
                        </div>
                        <div class="form-group col-sm-6" style="margin-top: 0;">
                            @php
                                $val = getClientData('volunteer_experience', $client, $formType);
                            @endphp
                            <label class="control-label">Specify</label>
                            <textarea id="volunteer_experience" class="form-control" rows="4" readonly>{{ $val }}</textarea>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        @php
                            $val = getClientData('volunteer_experience_duration', $client, $formType);
                            $tmp = !empty($val) ? explode(' ', $val) : ['', ''];
                            $val = $tmp[0];
                        @endphp
                        <label class="col-sm-12 control-label label-line">How long</label>
                        <div class="col-sm-6">
                            <div class="col-sm-6">
                                <input id="volunteer_experience_duration_number" type="number" class="form-control" value="{{ !empty($val) ? $val : 0 }}" readonly>
                            </div>
                            <div class="col-sm-6">
                                <select id="volunteer_experience_duration_unit" disabled class="select2 form-control">
                                    @php
                                        $val = $tmp[1];
                                    @endphp
                                    <option value="month" {{ $val == 'month' ? 'selected' : '' }}>Months</option>
                                    <option value="year" {{ $val == 'year' ? 'selected' : '' }}>Years</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Travel partner</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="form-group col-sm-6">
                    <label for="travel_partner" class="col-sm-3 control-label label-line">Full name</label>
                    <div class="col-sm-9">
                        @php
                            $val = getData('travel_partner', $formType, $command);
                        @endphp
                        <input class="form-control" type="text" id="travel_partner" name="travel_partner" value="{{ $val }}" {{ $readOnly }}/>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Experiences</h3>
            @if ($formType !== Constant::FORM_SHOW)
            <button type="button" class="btn btn-primary add pull-right"><i class="fa fa-plus-circle"></i><span> Add</span></button>
            @endif
        </div>
        <div id="command-courses" class="cmd-record-info box-body">
            <div class="grid-container"></div>
            <span class="help-block"><ul></ul></span>
        </div>
    </div>
    @if (!empty($command->data->client->data))
    <div id="client-classes" class="box">
        <?php
        $passed = [];
        $incomming = [];
        $today = \Carbon\Carbon::now();
        foreach ($command->data->client->data->scheduleDates->data as $sch) {
            if ((new \Carbon\Carbon($sch->begin_at))->lessThan($today)) {
                $passed[] = $sch;
            }
            else {
                $incomming[] = $sch;
            }
        }
        ?>
        <div class="box-header with-border">
            <h3 class="box-title">Upcoming classes <span class="badge">{{ count($incomming) }}</span></h3>
        </div>
        <div class="box-body">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Theme</th>
                        <th>Group</th>
                        <th>Responsible</th>
                        <th>Date / time</th>
                        <th class="text-center">Canceled</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($incomming as $sch)
                    <tr data-cid="{{ $command->data->client->data->id }}" data-sid="{{ $sch->id }}" data-canceled="{{ $sch->canceled_at == null ? '0' : '1' }}">
                        <td>{{ $sch->subject }}</td>
                        <td>{{ $sch->schedulable_type == 'App\Group' ? $sch->schedulable->data->name : '' }}</td>
                        <td>{{ $sch->schedule->data? $sch->schedule->data->responsible->data->full_name : '' }}</td>
                        <td>{{ (new \Carbon\Carbon($sch->begin_at))->format('d-m-Y / H:i') }}</td>
                        <td class="text-center"><button type="button" class="action-btn cancelcl"><i class="fa {{ $sch->canceled_at == null ? 'fa-times' : 'fa-check' }}" aria-hidden="true"></i></button></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="box-footer"></div>
        <div class="box-header with-border">
            <h3 class="box-title">Past classes <span class="badge">{{ count($passed) }}</span></h3>
        </div>
        <div class="box-body">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Theme</th>
                        <th>Group</th>
                        <th>Responsible</th>
                        <th>Date / time</th>
                        <th class="text-center">Taken</th>
                        <th class="text-center">Canceled</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($passed as $sch)
                    <tr data-cid="{{ $command->data->client->data->id }}" data-sid="{{ $sch->id }}">
                        <td>{{ $sch->subject }}</td>
                        <td>{{ $sch->schedulable_type == 'App\Group' ? $sch->schedulable->data->name : '' }}</td>
                        <td>{{ $sch->schedule->data? $sch->schedule->data->responsible->data->full_name : '' }}</td>
                        <td>{{ (new \Carbon\Carbon($sch->begin_at))->format('d-m-Y / H:i') }}</td>
                        <td class="text-center"><input type="checkbox" class="iCheck statuscl" {{ $sch->taken == 1 ? 'checked' : '' }} /></td>
                        <td class="text-center"><span class="table-icon"><i class="fa {{ $sch->canceled_at == null ? 'fa-times' : 'fa-check' }}" aria-hidden="true"></i></span></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @endif
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Housing</h3>
            @if ($formType !== Constant::FORM_SHOW)
            <button type="button" class="btn btn-primary add pull-right"><i class="fa fa-plus-circle"></i><span> Add</span></button>
            @endif
        </div>
        <div id="command-housing" class="cmd-record-info box-body">
            <div class="grid-container"></div>
            <span class="help-block"><ul></ul></span>
        </div>
    </div>

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Extra purchases</h3>
            @if ($formType !== Constant::FORM_SHOW)
            <button type="button" class="btn btn-primary add pull-right"><i class="fa fa-plus-circle"></i><span> Add</span></button>
            @endif
        </div>
        <div id="command-purchases" class="cmd-record-info box-body">
            <div class="grid-container"></div>
            <span class="help-block"><ul></ul></span>
        </div>
    </div>

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Travel information</h3>
        </div>
        <div class="box-body travel-information">
            <input type="hidden" name="id_travel_information"
                value="{{ isset($command->data->travelInformation->data->id) ? $command->data->travelInformation->data->id : '' }}">
            @php
                $travelTextOnEmpty = ($formType === Constant::FORM_SHOW) ? 'No data' : '' ;
            @endphp

            <div class="row hidden" id="msg-error-t-inf">
                <div class="col-xs-12">
                    <div class="form-group has-error">
                        <span class="help-block" style="padding-left: 15px;">
                            You will need to fill in all of you travel information if you have them at this time.
                            If you don' t have complete travel information, please
                            <a href="#" id="reset-travel-information">click here</a>
                            to reset this section and continue.
                        </span>
                    </div>
                </div>
            </div>

            <div class="travel-information__client">
                <div class="row">
                    <div class="form-group col-sm-6 {{ $errors->has('travel_by') ? 'has-error' : '' }}">
                        <label class="control-label col-sm-3">Travel by</label>
                        <div class="col-sm-9">
                            @if ($formType === Constant::FORM_SHOW)
                                <input type="text" class="form-control" readonly
                                    value="{{ isset($command->data->travelInformation->data->travel_by) ? $command->data->travelInformation->data->travel_by : 'No data' }}">
                            @else
                                @php
                                    $val = old('travel_by');
                                    if ($val === null && isset($command->data->travelInformation->data->travel_by)) {
                                        $val =  $command->data->travelInformation->data->travel_by;
                                    }
                                @endphp
                                <label>
                                    <input type="radio" name="travel_by" id="travel_by_plane" class="iCheck" value="plane" {{ $val === 'plane' ? 'checked' : '' }}>
                                        Plane
                                </label>
                                <label>
                                    <input type="radio" name="travel_by" id="travel_by_bus" class="iCheck" value="bus"
                                    {{ $val === 'bus' ? 'checked' : '' }}>
                                    Bus
                                </label>
                            @endif
                            <span class="help-block">{{ $errors->first('travel_by') }}</span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    @php
                        $airports = ['Puerto Escondido', 'Huatulco'];
                    @endphp
                    <div class="form-group col-sm-6 {{ $errors->has('arrival_place') ? 'has-error' : '' }}">
                        <label for="arrival_place" class="control-label col-sm-3">Arrival place</label>
                        <div class="col-sm-9">
                            @if ($formType === Constant::FORM_SHOW)
                                <input class="form-control" type="text" id="arrival_place" {{ $readOnly }}
                                value="{{ $command->data->travelInformation->data->arrival_place ?? $travelTextOnEmpty }}">
                            @else
                                <select name="arrival_place" class="form-control select2">
                                    <option value="">Select an option</option>
                                    @foreach ($airports as $airport)
                                        @php
                                            $val = old('arrival_place', $command->data->travelInformation->data->arrival_place ?? null);
                                        @endphp
                                        <option value="{{ $airport }}" {{ $airport === $val ? 'selected' : '' }}>
                                            {{ $airport }}
                                        </option>
                                    @endforeach
                                </select>
                            @endif
                            <span class="help-block" >{{ $errors->first('arrival_place') }}</span>
                        </div>
                    </div>
                    <div class="form-group col-sm-6 {{ $errors->has('departure_place') ? 'has-error' : '' }}">
                        <label for="departure_place" class="control-labe col-sm-3">Departure place</label>
                        <div class="col-sm-9">
                            @if ($formType === Constant::FORM_SHOW)
                                <input class="form-control" id="departure_place" type="text" {{ $readOnly }}
                                value="{{ $command->data->travelInformation->data->departure_place ?? $travelTextOnEmpty }}">
                            @else
                                <select name="departure_place" class="form-control select2">
                                    <option value="">Select an option</option>
                                    @foreach ($airports as $airport)
                                        @php
                                            $val = old('departure_place', $command->data->travelInformation->data->departure_place?? null);
                                        @endphp
                                        <option value="{{ $airport }}" {{ $airport === $val ? 'selected' : '' }}>
                                            {{ $airport }}
                                        </option>
                                    @endforeach
                                </select>
                            @endif
                            <span class="help-block">{{ $errors->first('departure_place') }}</span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-sm-6 {{ $errors->has('arrival_carrier') ? 'has-error' : '' }}">
                        <label for="arrival_carrier" class="control-label txt-change-airline col-sm-3">Airline</label>
                        <div class="col-sm-9">
                            @php
                                $val = old('arrival_carrier');
                                if ($val === null && isset($command->data->travelInformation->data->arrival_carrier)) {
                                    $val = $command->data->travelInformation->data->arrival_carrier;
                                }
                            @endphp
                            <input id="arrival_carrier" name="arrival_carrier" type="text" class="form-control" {{ $readOnly }}
                            value="{{ isset($val) ? $val : $travelTextOnEmpty }}">
                            <span class="help-block">{{ $errors->first('arrival_carrier') }}</span>
                        </div>
                    </div>
                    <div class="form-group col-sm-6 {{ $errors->has('departure_carrier') ? 'has-error' : '' }}">
                        <label for="departure_carrier" class="control-label txt-change-airline col-sm-3">Airline</label>
                        <div class="col-sm-9">
                            @php
                                $val = old('departure_carrier');
                                if ($val === null && isset($command->data->travelInformation->data->departure_carrier)) {
                                    $val = $command->data->travelInformation->data->departure_carrier;
                                }
                            @endphp
                            <input id="departure_carrier" name="departure_carrier" type="text" class="form-control" {{ $readOnly }}
                            value="{{ isset($val) ? $val : $travelTextOnEmpty }}">
                            <span class="help-block">{{ $errors->first('departure_carrier') }}</span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-sm-6 {{ $errors->has('arrival_date') ? 'has-error' : '' }}">
                        <label for="arrival_at_date" class="control-label col-sm-3">Arrival date</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                @php
                                    $val = old('arrival_date');
                                    if ($val === null && !empty($command->data->travelInformation->data->arrival_at)) {
                                        $val = \Carbon\Carbon::parse($command->data->travelInformation->data->arrival_at)->format('d-m-Y');
                                    }
                                @endphp
                                <input type="text" id="arrival_at_date_date" name="arrival_date" class="dpicker-local form-control" {{ $readOnly }}
                                value="{{ isset($val) ? $val : $travelTextOnEmpty }}"/>
                                <span class="input-group-addon {{ $disabled ? 'hide' : '' }}">
                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                </span>
                            </div>
                            <span class="help-block">{{ $errors->first('arrival_date') }}</span>
                        </div>
                    </div>
                    <div class="form-group col-sm-6 {{ $errors->has('departure_date') ? 'has-error' : '' }}">
                        <label for="departure_at_date" class="control-label col-sm-3">Departure date</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                @php
                                    $val = old('departure_date');
                                    if ($val === null && !empty($command->data->travelInformation->data->departure_at)) {
                                        $val = \Carbon\Carbon::parse($command->data->travelInformation->data->departure_at)->format('d-m-Y');
                                    }
                                @endphp
                                <input type="text" id="departure_at_date" name="departure_date" class="dpicker-local form-control" {{ $readOnly }}
                                value="{{ isset($val) ? $val : $travelTextOnEmpty }}"/>
                                <span class="input-group-addon {{ $disabled ? 'hide' : '' }}">
                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                </span>
                            </div>
                            <span class="help-block">{{ $errors->first('departure_date') }}</span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-sm-6 {{ $errors->has('arrival_time') ? 'has-error' : '' }}">
                        <label for="arrival_at_time" class="control-label col-sm-3">Arrival time</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                @php
                                    $val = old('arrival_time');
                                    if ($val === null && !empty($command->data->travelInformation->data->arrival_at)) {
                                        $val = \Carbon\Carbon::parse($command->data->travelInformation->data->arrival_at)->format('H:i');
                                    }
                                @endphp
                                <input type="text" id="arrival_at_time" name="arrival_time" class="form-control" {{ $readOnly }}
                                value="{{ isset($val) ? $val : $travelTextOnEmpty }}"/>
                                <span class="input-group-addon {{ $disabled ? 'hide' : '' }}">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                </span>
                            </div>
                            <span class="help-block">{{ $errors->first('arrival_time') }}</span>
                        </div>
                    </div>
                    <div class="form-group col-sm-6 {{ $errors->has('departure_time') ? 'has-error' : '' }}">
                        <label for="departure_at_time" class="control-label col-sm-3">Departure time</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                @php
                                    $val = old('departure_time');
                                    if ($val === null && !empty($command->data->travelInformation->data->departure_at)) {
                                        $val = \Carbon\Carbon::parse($command->data->travelInformation->data->departure_at)->format('H:i');
                                    }
                                @endphp
                                <input type="text" id="departure_at_time" name="departure_time" class="form-control" {{ $readOnly }}
                                value="{{ isset($val) ? $val : $travelTextOnEmpty }}"/>
                                <span class="input-group-addon {{ $disabled ? 'hide' : '' }}">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                </span>
                            </div>
                            <span class="help-block">{{ $errors->first('departure_time') }}</span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-sm-6 {{ $errors->has('arrival_travel_number') ? 'has-error' : '' }}">
                        <label for="arrival_travel_number" class="control-label txt-change-flight-number col-sm-3">Flight number</label>
                        <div class="col-sm-9">
                            @php
                                $val = old('arrival_travel_number');
                                if ($val === null && isset($command->data->travelInformation->data->arrival_travel_number)) {
                                    $val = $command->data->travelInformation->data->arrival_travel_number;
                                }
                            @endphp
                            <input id="arrival_travel_number" name="arrival_travel_number" type="text" class="form-control" {{ $readOnly }}
                            value="{{ isset($val) ? $val : $travelTextOnEmpty }}">
                        </div>
                        <span class="help-block">{{ $errors->first('arrival_travel_number') }}</span>
                    </div>
                    <div class="form-group col-sm-6 {{ $errors->has('departure_travel_number') ? 'has-error' : '' }}">
                        <label for="departure_travel_number" class="control-label txt-change-flight-number col-sm-3">Flight number</label>
                        <div class="col-sm-9">
                            @php
                                $val = old('departure_travel_number');
                                if ($val === null && isset($command->data->travelInformation->data->departure_travel_number)) {
                                    $val = $command->data->travelInformation->data->departure_travel_number;
                                }
                            @endphp
                            <input id="departure_travel_number" name="departure_travel_number" type="text" class="form-control" {{ $readOnly }}
                            value="{{ isset($val) ? $val : $travelTextOnEmpty }}">
                            <span class="help-block">{{ $errors->first('departure_travel_number') }}</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-sm-6 {{ $errors->has('pickup_id') ? 'has-error' : '' }}">
                    <label for="select-pickup" class="col-sm-3 control-label label-line">Transfer services</label>
                    <div class="col-sm-9">
                        @php
                        $val = getData('pickup_id', $formType, $command);
                        $val1 = getData('pickup_name', $formType, $command);
                        @endphp
                        @if ($formType === Constant::FORM_SHOW)
                            <input type="text" class="form-control" value="{{ $val1 ?? 'No data' }}" readonly>
                        @else
                            <select name="pickup_id" id="select-pickup" class="form-control" {{ $disabled }}  style="width: 100%;">
                                @if ($formType !== Constant::FORM_SHOW)
                                    <option value=""></option>
                                    @foreach ($pickup as $p)
                                        <option value="{{ $p->id }}" data-cost="{{ $p->cost }}" @if ($val == $p->id) selected @endif>{{ $p->name }}</option>
                                    @endforeach
                                @else
                                    <option value="{{ $val }}" selected>
                                        {{ $val1 }}
                                    </option>
                                @endif
                            </select>
                            <input type="hidden" name="pickup_name" id="pickup_name" value="{{ $val1 }}" />
                        @endif
                        <span class="help-block">{{ $errors->first('pickup_id') }}</span>
                    </div>
                </div>
                <div class="form-group col-sm-6 {{ $errors->has('pickup_cost') ? 'has-error' : '' }}">
                    <label for="pickup-cost" class="col-sm-3 control-label label-line">Transfer cost</label>
                    <div class="col-sm-9">
                        @php
                        $defaultCurrency = collect($currencies)->where('slug', 'us_dollar')->first();
                        $val = getData('pickup_cost', $formType, $command);
                        $selectedCurrency = getData('pickup_currency_id', $formType, $command);
                        $pickupExchangeRates = getData('pickup_exchange_rates', $formType, $command);
                        $currentPickupExchangeRates = getData('current_pickup_exchange_rates', $formType, $command);
                        if (!$selectedCurrency) {
                            $selectedCurrency = $defaultCurrency->id;
                        }
                        @endphp
                        <div class="row">
                            <input type="hidden" name="pickup_exchange_rates" id="pickup_exchange_rates" value="{{ $pickupExchangeRates }}">
                            <input type="hidden" id="current_pickup_exchange_rates" value="{{ $currentPickupExchangeRates }}">
                            <div class="col-xs-7 col-sm-8">
                                <input type="number" class="form-control" id="pickup-cost" name="pickup_cost" value="{{ $val }}" {{ $readOnly }}>
                            </div>
                            <div class="col-xs-5 col-sm-4" style="padding-left: 0;">
                                @if ($formType === Constant::FORM_SHOW)
                                    @foreach ($currencies as $key => $currency)
                                        @if ($currency->id == $selectedCurrency)
                                            <input type="hidden" id="pickup_currency_id" value="{{ $currency->id }}">
                                            <input type="text" class="form-control" value="{{ $currency->code }}" readonly>
                                            @break
                                        @endif
                                    @endforeach
                                @else
                                    <select class="form-control select2" id="pickup_currency_id" style="width: 100%;" name="pickup_currency_id" required>
                                        @foreach ($currencies as $key => $currency)
                                            <option value="{{ $currency->id }}" {{ $currency->id == $selectedCurrency ? 'selected' : '' }}>{{ $currency->code }}</option>
                                        @endforeach
                                    </select>
                                    <input type="hidden" name="pickup_currency" id="pickup_currency" value="{{ collect($currencies)->where('id', $selectedCurrency)->first()->code }}">
                                @endif
                            </div>
                        </div>
                        <span class="help-block">{{ $errors->first('pickup_cost') }}</span>
                    </div>
                </div>
                <div class="col-sm-offset-6 col-sm-6">
                    <div class="form-group {{ $errors->has('pickup_discount') ? 'has-error' : '' }}">
                        <label for="pickup_discount" class="col-sm-3 control-label label-line">Transfer discount %</label>
                        <div class="col-sm-9">
                            @php
                            $val = getData('pickup_discount', $formType, $command);
                            @endphp
                            <input type="number" class="form-control" id="pickup_discount" name="pickup_discount" value="{{ $val }}" {{ $readOnly }}>
                            <span class="help-block">{{ $errors->first('pickup_discount') }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if ($formType !== Constant::FORM_CREATE)
        {{-- Solo estoy ocultando este bloque porque el cliente así lo pidió --}}
        <div class="box" style="display: none;">
            <div class="box-header with-border">
                <h3 class="box-title">Fee payment</h3>
            </div>
            <div id="command-payments" class="cmd-record-info box-body">
                <div class="grid-container"></div>
            </div>
        </div>
    @endif
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Payments <small>(Make sure to save changes for your payment to be registered)</small></h3>
            @if ($formType !== Constant::FORM_SHOW)
                <button type="button" class="btn btn-primary add pull-right" id="add-payment">
                    <i class="fa fa-plus-circle"></i>
                    <span> Add</span>
                </button>
            @endif
        </div>
        <div class="box-body">
            <div class="panel panel-default" style="margin-bottom: 0;">
                <div class="panel-heading">
                    <h2 class="panel-title h3 pull-left">Totals:</h2>
                    <div id="totals" class="pull-left">

                    </div>
                    <div class="clearfix"></div>
                </div>
                <div id="command-all-payments" class="panel-body cmd-record-info">
                    <div class="table-responsive grid-container">

                    </div>
                    <span class="help-block"><ul></ul></span>
                </div>
                <div class="panel-footer">
                    <div id="paidTotals" class="pull-left">

                    </div>
                    <div class="clearfix"></div>
                    <div id="balance">

                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
            <div class="box-footer">
                <div class="row">
                        <div class="col-md-7">
                            @if ($formType !== Constant::FORM_SHOW)
                                <button class="btn btn-primary btn-voiced" data-pay_status="1">Save selected as invoiced</button>
                                <button class="btn btn-primary btn-voiced" data-pay_status="0">Save selected as not invoiced</button>
                            @endif
                        </div>
                    <div class="col-md-5">
                        <div class="form-group {{ $errors->has('payment_status') ? 'has-error' : '' }}">
                            @php
                            $val = getData('payment_status', $formType, $command);
                            @endphp
                            <label for="">Payment status</label>
                            <select class="form-control" name="payment_status" {{ $disabled }}>
                                <option value="paid" {{ $val === 'paid' ? 'selected' : '' }}>Paid</option>
                                <option value="partially_paid" {{ $val === 'partially_paid' ? 'selected' : '' }}>Partially paid</option>
                                <option value="not_payed" {{ $val === 'not_payed' || $formType === Constant::FORM_CREATE ? 'selected' : '' }}>Not payed</option>
                            </select>
                            <span class="help-block">{{ $errors->first('payment_status') }}</span>
                        </div>
                    </div>
                </div>
            </div>
    </div>

    <div class="box" id="app" style="display: none;">
        <div class="box-header with-border">
            <h3 class="box-title">Billing Reference</h3>
            @if ($formType !== Constant::FORM_SHOW)
                <button id="add-bill" type="button" class="btn btn-primary pull-right" @click="addItemDynamicList({'id': 0, 'reference_number': '', 'amount': '', 'currency_id': '', 'currency': ''}, bills)"><i class="fa fa-plus-circle"></i><span> Add</span></button>
            @endif
        </div>
        <div id="" class="box-body">
            @if(isset($command->data->bills->data))
                @php
                    $bills = $command->data->bills->data;
                    $newBills = [];
                    for ($i = 0; $i < count($bills); $i++) {
                        $newBills[$i]['id'] = $bills[$i]->id;
                        $newBills[$i]['reference_number'] = $bills[$i]->reference_number;
                        $newBills[$i]['amount'] = $bills[$i]->amount;
                        $newBills[$i]['currency_id'] = $bills[$i]->currency->data->id;
                        $newBills[$i]['currency'] = $bills[$i]->currency->data->code;
                    }
                    $billsJson = json_encode($newBills);
                @endphp
            @endif
            <div>
                <input type="hidden" id="bills_hidden" name="command_bills" :value="JSON.stringify(bills)" value="{{ old('command_bills', isset($command->data->bills->data) ? $billsJson : '[]') }}">
                <div class="row" v-for="(bill, i) in bills">
                    <div class="col-sm-6" style="padding-left: 0;">
                        <div class="col-sm-4">
                            <div class="form-group" :class="{ 'has-error': (showErrorMessage(i, 'reference_number') != '') }">
                                <label class="control-label label-line">Reference number</label>
                                <input type="text" class="form-control" v-model="bill.reference_number" {{ $readOnly }}>
                                <span class="help-block">
                                    <span class="help-block">
                                        @{{ showErrorMessage(i, 'reference_number') }}
                                    </span>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" :class="{ 'has-error': (showErrorMessage(i, 'amount') != '') }">
                                <label class="control-label label-line">Amount</label>
                                <input type="number" class="form-control" step="any" min="0" v-model="bill.amount" {{ $readOnly }}>
                                <span class="help-block">
                                    @{{ showErrorMessage(i, 'amount') }}
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" :class="{ 'has-error': (showErrorMessage(i, 'currency_id') != '') }">
                                <label class="control-label label-line" for="from">Currency</label>
                                <select class="form-control select2-v" v-model="bill.currency_id" {{ $disabled }} @change="handleCurrencyChange(bill, $event)">
                                    @foreach ($currencies as $key => $currency)
                                        <option value="{{ $currency->id }}">{{ $currency->code }}</option>
                                    @endforeach
                                </select>
                                <span class="help-block">
                                    @{{ showErrorMessage(i, 'currency_id') }}
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1">
                        @if($readOnly != 'readonly')
                        <div style="display: flex; align-items: center; height: 76px;">
                            <button type="button" class="btn table-btn table-btn-destroy" @click="deleteItemDynamicList(bill, bills, null, {keep: 1, itemBase: { 'id': 0, 'reference_number': '', 'amount': '', 'currency_id': '', 'currency': ''}})">
                                <i class="fa fa-trash-o fa--size-1-3" aria-hidden="true"></i>
                            </button>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="box" id="app">
        <div class="box-header with-border">
            <h3 class="box-title">Comments</h3>
        </div>
        <div id="" class="box-body">
            <div class="form-group">
                <textarea name="comments" rows="10" class="form-control" {{ $readOnly }}>{{ getData('comments', $formType, $command) }}</textarea>
            </div>
        </div>
    </div>

    @if ($formType === Constant::FORM_EDIT)
        <div class="box" id="app">
            <div class="box-header with-border">
                <h3 class="box-title">Send email?</h3>
            </div>
            <div id="" class="box-body">
                <div class="form-group">
                    <label class="checkbox">
                        <input type="checkbox" class="iCheck" name="send_email" value="1" {{ $formType == Constant::FORM_CREATE ? 'checked' : '' }}> Yes
                    </label>
                </div>
            </div>
        </div>
    @endif

    <div class="form-group">
        <a class="btn btn-default" href="{{ url()->previous() }}">
            @if ($formType !== Constant::FORM_SHOW)
                Cancel
            @else
                Back
            @endif
        </a>
        @if ($formType !== Constant::FORM_SHOW)
            <button id="btn-save-command" type="submit" class="btn btn-primary">Save</button>
        @else
            <a class="btn btn-primary" href="{{ route('bookings.edit', ['client' => $command->data->id]) }}">
                Edit
            </a>
        @endif
    </div>
</form>

@if ($formType !== Constant::FORM_SHOW)
    @include('commands.modals')
@endif
<script type="text/javascript">
    function loadClientAsDefault(url = null) {
        if(url === null) {
            console.error('Missing client URL');
            return false;
        }

        $('#chk-newclient').prop('checked', false)
            .prop('disabled' , true)
            .iCheck('update')
            .trigger('change');

        $.getJSON(url, function(response) {
            var $selectClient = $('#select-client');
            var selectClientOptions = $selectClient.data('select2').options.options;

            response.data.text = response.data.full_name;
            selectClientOptions.data = [response.data];

            $selectClient.select2(selectClientOptions)
                .trigger('change');

            $selectClient.after($('<input>', {
                'type': 'hidden',
                'name': 'client_id',
                'value': $selectClient.val()
            })).prop('disabled', true)
        });
    }

    function changeTextOnTravelType() {
        var $travelBy = $('[name="travel_by"]:checked'),
            $airline = $('.txt-change-airline'),
            $flightNumber = $(".txt-change-flight-number")
            dataValue = 'plane';

        if ($travelBy.val() !== void 0) {
            dataValue = $travelBy.val();
        }

        if (dataValue == "plane") {
            $flightNumber.text('Flight number');
            $airline.text('Airline');
        } else {
            $flightNumber.text('Bus number');
            $airline.text('Busline');
        }
    }

    function onNewClientInputChanges(event) {
        if (this.checked) {
            $('#client-firstname').prop('readOnly', false).focus();
            $('#client-lastname').prop('readOnly', false);
            $('#client-email').prop('readOnly', false);
            $('#select-country').prop('disabled', false);
            $('#hidden-newclient').val(1);
        }
        else {
            $('#client-firstname').prop('readOnly', true);
            $('#client-lastname').prop('readOnly', true);
            $('#client-email').prop('readOnly', true);
            $('#select-country').prop('disabled', true);
            $('#hidden-newclient').val(0);
        }
        showClientInformation(!($('#select-client').val() == void 0) && !(this.checked));
    }


    function showClientInformation(show) {
        if (show || Boolean({{ $formType === Constant::FORM_SHOW }})) {
            $('#client_info').show();
        } else {
            $('#client_info').hide();
        }
    }

    window.addEventListener('load', function() {
        showClientInformation(!($('#select-client').val() == void 0));
        changeTextOnTravelType();
        var getSelectedIds = function(table) {
            var ids = [];
            var id = 0;
            $(table + ' input.iCheckInvoiced').each(function(i, chk) {
                if (chk.checked) {
                    id = $(chk).closest('tr').data('id');
                    if (id > 0) {
                        ids.push(id);
                    }
                }
            });
            return ids;
        };
        var updatePayStatus = function(status) {
            var data = {
                payments: getSelectedIds('#command-all-payments table'),
                is_paid: status,
                command_id: EXP.command.id,
            };
            if (data.payments.length === 0) {
                return;
            }
            $('#command-all-payments').ploading({
                action: 'show',
                spinnerHTML: '<i></i>',
                spinnerClass: 'fa fa-spinner fa-spin p-loading-fontawesome'
            });
            EXP.request.post("{{ route('bookings.payments.mark_as_invoiced') }}", data, function(resp) {
                if (resp.status_code == 200 && resp.success) {
                    $.each(EXP.command.allPayments.component.grid.data, function(index, row) {
                        if (data.payments.indexOf(parseInt(row.id)) !== -1) {
                            // Modificar la fecha de facturación
                            EXP.command.allPayments.component.grid.data[index].invoiced_at = resp.invoiced_at? resp.invoiced_at : "";
                        }
                    });
                    // Desmarcar los checkboxs
                    if (data.is_paid == 0) {
                        $('#command-all-payments table' + ' input.iCheckInvoiced').each(function(i, chk) {
                            if (chk.checked) {
                                $(chk).iCheck('uncheck');
                            }
                        });
                    }
                    $('#command-all-payments').ploading({action: 'hide'});
                }
            });
        };
        EXP.initConfirmButton({
                selector: '.btn-voiced',
                type: 'question',
                title: 'Change status',
                msg: function(btn) {
                    if (btn.data('pay_status') == 1) {
                        return 'Mark payments as invoiced';
                    }
                    return 'Mark payments as not invoiced';
                },
                callback: function(btn, value) {
                    var status = btn.data('pay_status') == 1 ? 1 : 0;
                    updatePayStatus(status);
                },
                validator: function() {
                    return getSelectedIds('#command-all-payments table').length > 0;
                }
            });
        $('[name="travel_by"]').on('ifChanged', changeTextOnTravelType)
            .on('change', changeTextOnTravelType);

        @if ($formType !== Constant::FORM_CREATE)
            EXP.command.id = {{ $command->data->id }};
        @else
            EXP.command.id = -1;
        @endif
        $('#pickup-cost').on('keyup', function() {
            EXP.paymentObject.calculateTotals();
        });
        $('#pickup-cost').on('change', function() {
            EXP.paymentObject.calculateTotals();
        });
        $('#pickup_discount').on('keyup', function() {
            EXP.paymentObject.calculateTotals();
        });
        $('#pickup_discount').on('change', function() {
            EXP.paymentObject.calculateTotals();
        });
        $('form#command-form #select-country').on('change', function() {
            var selected = $("option:selected", this);
            $('form#command-form #country_name').val(selected.text());
        });
        $('form#command-form #select-agency').on('change', function() {
            var selected = $("option:selected", this);
            $('form#command-form #agency_name').val(selected.text());
        });
        $('form#command-form #pickup_currency_id').on('change', function() {
            var selected = $("option:selected", this);
            var currentExchangeRates = $('form#command-form #current_pickup_exchange_rates').val() || '[]';
            var exchangeRates = EXP.command.getExchangeRates(parseInt(selected.val()), JSON.parse(currentExchangeRates));
            $('form#command-form #pickup_exchange_rates').val(JSON.stringify(exchangeRates));
            $('form#command-form #pickup_currency').val(selected.text());
            EXP.paymentObject.calculateTotals();
        });
        $('#chk-newclient').on('ifChanged', onNewClientInputChanges)
            .on('change', onNewClientInputChanges);
        $('input[type=checkbox].statuscl').on('ifChanged', function(event) {
            // Mostrar loading
            $('#client-classes').ploading({
                action: 'show',
                spinnerHTML: '<i></i>',
                spinnerClass: 'fa fa-spinner fa-spin p-loading-fontawesome'
            });
            var tr = $(this).closest('tr');
            var data = {
                schedule_date_id: tr.data('sid'),
                client_id: tr.data('cid'),
                taken: this.checked ? 1 : 0,
                canceled: 0
            };
            EXP.request.post('/admin/bookings/class-status', data, function(resp) {
                $('#client-classes').ploading({action: 'hide'});
            });
        });
        EXP.initConfirmButton({
            selector: 'button.cancelcl',
            title: "Update class status",
            msg: "This action is going to change the selected class status.",
            callback: function(btn) {
                // Mostrar loading
                $('#client-classes').ploading({
                    action: 'show',
                    spinnerHTML: '<i></i>',
                    spinnerClass: 'fa fa-spinner fa-spin p-loading-fontawesome'
                });
                var tr = $(btn).closest('tr');
                var data = {
                    schedule_date_id: tr.data('sid'),
                    client_id: tr.data('cid'),
                    canceled: tr.data('canceled') == 1 ? 0 : 1
                };
                EXP.request.post('/admin/bookings/class-status', data, function(resp) {
                    var i = $(btn).find('i.fa');
                    if (i.hasClass('fa-times')) {
                        i.removeClass('fa-times');
                        i.addClass('fa-check');
                    }
                    else {
                        i.removeClass('fa-check');
                        i.addClass('fa-times');
                    }
                    $('#client-classes').ploading({action: 'hide'});
                });
            }
        });

        setTimeout(function() {
            $('form#command-form #select-pickup').select2({
                placeholder: "Select an option",
                allowClear: true
            });
            EXP.initAutocomplete(document.getElementById('select-client'), {
                url: '/admin/clients?include=file,gender',
                paramName: 'name',
                displayField: 'full_name',
                disabled: @if ($formType === Constant::FORM_SHOW) true @else false @endif
            });
        }, 200);
        $('form#command-form #select-client').on('change', function() {
            var data = $(this).select2('data')[0];
            var email = data.email;
            var fullName = data.full_name;
            var firstName = data.first_name;
            var lastName = data.last_name;
            var countryId = data.country_id;
            var birth_date = data.birth_date;
            var phone = data.phone;
            var emergency_phone = data.emergency_phone;
            var has_alergies = data.has_alergies;
            var alergies = data.alergies;
            var has_diseases = data.has_diseases;
            var diseases = data.diseases;
            var is_smoker = data.is_smoker;
            var fb_profile = data.fb_profile;
            var spanish_level = data.spanish_level;
            var spanish_knowledge_duration = data.spanish_knowledge_duration;
            var has_surf_experience = data.has_surf_experience;
            var surf_experience_duration = data.surf_experience_duration;
            var has_volunteer_experience = data.has_volunteer_experience;
            var volunteer_experience = data.volunteer_experience;
            var volunteer_experience_duration = data.volunteer_experience_duration;
            var gender_id = data.gender? (data.gender.data? data.gender.data.id : '') : undefined;
            var client_image = data.file != undefined? data.file.data.path : undefined;
            if (email === void 0) {
                email = $('option:selected', this).attr('data-email');
            }
            if (firstName === void 0) {
                firstName = $('option:selected', this).attr('data-first_name');
            }
            if (lastName === void 0) {
                lastName = $('option:selected', this).attr('data-last_name');
            }
            if (countryId === void 0) {
                countryId = $('option:selected', this).attr('data-country_id');
            }
            if (birth_date === void 0) {
                birth_date = $('option:selected', this).attr('data-birth_date');
            }
            if (phone === void 0) {
                phone = $('option:selected', this).attr('data-phone');
            }
            if (emergency_phone === void 0) {
                emergency_phone = $('option:selected', this).attr('data-emergency_phone');
            }
            if (has_alergies === void 0) {
                has_alergies = $('option:selected', this).attr('data-has_alergies');
            }
            if (alergies === void 0) {
                alergies = $('option:selected', this).attr('data-alergies');
            }
            if (has_diseases === void 0) {
                has_diseases = $('option:selected', this).attr('data-has_diseases');
            }
            if (diseases === void 0) {
                diseases = $('option:selected', this).attr('data-diseases');
            }
            if (is_smoker === void 0) {
                is_smoker = $('option:selected', this).attr('data-is_smoker');
            }
            if (fb_profile === void 0) {
                fb_profile = $('option:selected', this).attr('data-fb_profile');
            }
            if (spanish_level === void 0) {
                spanish_level = $('option:selected', this).attr('data-spanish_level');
            }
            if (spanish_knowledge_duration === void 0) {
                spanish_knowledge_duration = $('option:selected', this).attr('data-spanish_knowledge_duration');
            }
            if (has_surf_experience === void 0) {
                has_surf_experience = $('option:selected', this).attr('data-has_surf_experience');
            }
            if (surf_experience_duration === void 0) {
                surf_experience_duration = $('option:selected', this).attr('data-surf_experience_duration');
            }
            if (has_volunteer_experience === void 0) {
                has_volunteer_experience = $('option:selected', this).attr('data-has_volunteer_experience');
            }
            if (volunteer_experience === void 0) {
                volunteer_experience = $('option:selected', this).attr('data-volunteer_experience');
            }
            if (volunteer_experience_duration === void 0) {
                volunteer_experience_duration = $('option:selected', this).attr('data-volunteer_experience_duration');
            }
            if (gender_id === void 0) {
                gender_id = $('option:selected', this).attr('data-gender_id');
            }
            if (client_image === void 0) {
                client_image = $('option:selected', this).attr('data-image');
            }
            setTimeout(function() {
                $('form#command-form #hidden-clientname').val(fullName);
            }, 200);
            $('form#command-form #client-email').val(email);
            $('form#command-form #client-firstname').val(firstName);
            $('form#command-form #client-lastname').val(lastName);
            $('form#command-form #select-country').val(countryId);
            $('form#command-form #birthdate').val(birth_date == void 0? birth_date : moment(birth_date, 'YYYY-MM-DD').format('DD-MM-YYYY'));
            $('form#command-form #phone').val(phone);
            $('form#command-form #client-image').attr('src', client_image);
            $('form#command-form #emergency-phone').val(emergency_phone);
            $('form#command-form input[type=radio][name=has_alergies][value=' + has_alergies + ']').iCheck('check');
            $('form#command-form #alergies').val(alergies);
            $('form#command-form input[type=radio][name=has_diseases][value=' + has_diseases + ']').iCheck('check');
            $('form#command-form #diseases').val(diseases);
            $('form#command-form input[type=radio][name=is_smoker][value=' + is_smoker + ']').iCheck('check');
            $('form#command-form #fb-profile').val(fb_profile);
            $('form#command-form input[type=radio][name=spanish_level][value=' + (spanish_level === void 0? "none" : spanish_level) + ']').iCheck('check');
            $('form#command-form #spanish_knowledge_duration_number').val(spanish_knowledge_duration == void 0? 0 : spanish_knowledge_duration.split(" ")[0]);
            $('form#command-form #spanish_knowledge_duration_unit').val(spanish_knowledge_duration == void 0? "month" : spanish_knowledge_duration.split(" ")[1]).trigger('change');
            $('form#command-form input[type=radio][name=has_surf_experience][value=' + has_surf_experience + ']').iCheck('check');
            $('form#command-form #surf_experience_duration_number').val(surf_experience_duration == void 0? 0 : surf_experience_duration.split(" ")[0]);
            $('form#command-form #surf_experience_duration_unit').val(surf_experience_duration == void 0? "month" : surf_experience_duration.split(" ")[1]).trigger('change');
            $('form#command-form input[type=radio][name=has_volunteer_experience][value=' + has_volunteer_experience + ']').iCheck('check');
            $('form#command-form #volunteer_experience').val(volunteer_experience);
            $('form#command-form #volunteer_experience_duration_number').val(volunteer_experience_duration == void 0? 0 : volunteer_experience_duration.split(" ")[0]);
            $('form#command-form #volunteer_experience_duration_unit').val(volunteer_experience_duration == void 0? "month" : volunteer_experience_duration.split(" ")[1]).trigger('change');
            $('form#command-form input[type=radio][name=gender_id]').iCheck('uncheck');
            $('form#command-form input[type=radio][name=gender_id][value=' + gender_id + ']').iCheck('check');
            if (client_image === void 0) {
                $('form#command-form #client-image').css('display', 'none');
            } else {
                $('form#command-form #client-image').css({
                    'display': 'block',
                    'max-width': '120px'
                });
            }
            var country = $('#select-country').select2('data')[0].text;
            $('#select2-select-country-container').text(country).attr('title', country);
            showClientInformation(!($('#select-client').val() == void 0) && !($('#chk-newclient').is(':checked')));
        });
        // Botón guardar comanda
        $('#btn-save-command').click(function(evt) {
            evt.preventDefault();
            var data = EXP.command.courses.component.getData();
            $('#input-command-courses').val(JSON.stringify(data));
            data = EXP.command.housing.component.getData();
            $('#input-command-housing').val(JSON.stringify(data));
            data = EXP.command.purchases.component.getData();
            $('#input-command-purchases').val(JSON.stringify(data));
            data = EXP.command.allPayments.component.getData();
            $('#input-command-payments').val(JSON.stringify(data));
            $('#command-form').submit();
        });
        // Guardar nombre del elemento seleccionado en el campo hidden correspondiente
        $('form#command-form select').each(function(i) {
            $(this).on('change', function() {
                var selected = $("option:selected", this);
                $(this).parent().find('input[type=hidden]').val($(this).parent().find('.select2-selection__rendered').text().trim());
            });
        });
        $('form#command-form #select-pickup').on('change', function() {
            var selected = $("option:selected", this);
            $('form#command-form #pickup-cost').val(selected.attr('data-cost'));
            $('form#command-form #pickup_name').val(selected.text());
            EXP.paymentObject.calculateTotals();
        });
        EXP.command.courses.component = new EXP.CommandComponent({!! getData('courses', $formType, $command) !!}, {
            init: EXP.command.courses.initComponent,
            gridSelector: '#command-courses .grid-container',
            containerSelector: '#command-courses',
            modalSelector: '#modal-course',
            readOnly: @if ($formType === Constant::FORM_SHOW) true @else false @endif
        });
        EXP.command.purchases.component = new EXP.CommandComponent({!! getData('purchases', $formType, $command) !!}, {
            init: EXP.command.purchases.initComponent,
            gridSelector: '#command-purchases .grid-container',
            containerSelector: '#command-purchases',
            modalSelector: '#modal-purchase',
            readOnly: @if ($formType === Constant::FORM_SHOW) true @else false @endif
        });

        EXP.command.housing.component = new EXP.CommandComponent({!! getData('housing', $formType, $command) !!}, {
            init: EXP.command.housing.initComponent,
            gridSelector: '#command-housing .grid-container',
            containerSelector: '#command-housing',
            modalSelector: '#modal-housing',
            readOnly: @if ($formType === Constant::FORM_SHOW) true @else false @endif
        });
        @if ($formType !== Constant::FORM_CREATE)
            EXP.command.housing.component.modalScheduler = {
                ele: $('#modal-scheduler'),
                scheduler: null
            };
            EXP.command.housing.component.modalScheduler.ele.find('#select-residences').on('change', function() {
                var data = EXP.command.housing.component.grid.data[EXP.command.housing.component.grid.selectedIndex];
                var selectRoom = EXP.command.housing.component.modalScheduler.ele.find('#select-room');
                selectRoom.html('');
                _.each(EXP.residences.data, function(r) {
                    if (r.housing.data.id == data.housing_id && $('#select-residences').val() == r.id) {
                        var beginDateMoment = moment(data.from_date, 'YYYY-MM-DD');
                        var endDateMoment = moment(data.to_date, 'YYYY-MM-DD');

                        _.each(r.rooms.data, function(r) {
                            var sizeBLockedDates = r.blockedDates.data.length;
                            var fromAt = null;
                            var toAt = null;

                            for (var x = 0; x < sizeBLockedDates; x++) {
                                fromAt = r.blockedDates.data[x].from_at;
                                toAt = r.blockedDates.data[x].to_at;
                                if (toAt === void 0 || toAt === null) {
                                    toAt = r.blockedDates.data[x].from_at;
                                }

                                fromAt = moment(fromAt, 'YYYY-MM-DD');
                                toAt = moment(toAt, 'YYYY-MM-DD');

                                // Return void if busy dates.
                                if (
                                    beginDateMoment.isBetween(fromAt, toAt, null, '[]') ||
                                    endDateMoment.isBetween(fromAt, toAt, null, '[]')
                                ) {
                                    return;
                                }
                            }
                            for (var i = 0; i < r.housingRooms.data.length; i++) {
                                if (r.housingRooms.data[i].id == data.housing_room_id) {
                                    selectRoom.append('<option value="' + r.id + '">' + r.name + '</option>');
                                }
                            }
                        });
                    }
                });
            });
            EXP.command.housing.component.modalScheduler.ele.find('.box-footer button.add').click(function() {
                var data = EXP.command.housing.component.grid.data[EXP.command.housing.component.grid.selectedIndex];
                var selectedResidence = EXP.command.housing.component.modalScheduler.ele.find('#select-residences option:selected');
                var selectedRoom = EXP.command.housing.component.modalScheduler.ele.find('#select-room option:selected');
                var exchange = EXP.command.housing.component.modalScheduler.ele.find('#exchange').is(':checked');
                var commandHousingExchange = $(".sch-event[data-sch-event-selected=1]").data('ch-id');
                if (exchange && !commandHousingExchange) {
                    swal(
                        'Exchange accommodation',
                        'Select an item in the calendar',
                        'info'
                    );
                    return;
                }
                $.ajax({
                    url: '/admin/commands/' + EXP.command.id + '/accommodation',
                    type: 'PUT',
                    dataType: 'json',
                    data: {
                        command_housing_id: data.id,
                        room_id: selectedRoom.val(),
                        exchange: exchange ? 1 : 0,
                        command_housing_exchange: exchange ? commandHousingExchange : ''
                    },
                    success: function(r) {
                        // Mostrar mensaje de éxito
                        swal(
                            (exchange ? 'Exchange' : 'Store') + ' accommodation',
                            r.message,
                            'success'
                        ).then((result) => {
                            if (exchange) {
                                location.reload();
                            }
                        }).catch(() => {
                            if (exchange) {
                                location.reload();
                            }
                        });
                        // Actualizar datos en el grid
                        data.residence_id = selectedResidence.val();
                        data.residence = selectedResidence.text();
                        data.room_id = selectedRoom.val();
                        data.room_number = selectedRoom.text();
                        EXP.command.housing.component.grid.render();
                        EXP.command.housing.component.modalScheduler.ele.modal('hide');
                    },
                    error: function(r) {
                        var response = r.responseJSON;
                        // Mostrar mensaje de error
                        swal(
                            (exchange ? 'Exchange' : 'Store') + ' accommodation',
                            response.message,
                            'error'
                        );
                        // Mostrar errores
                        var items = '';
                        var selectRoom = EXP.command.housing.component.modalScheduler.ele.find('#select-room');
                        if (response.errors !== void 0) {
                            if (response.errors.room_id !== void 0) {
                                _.each(response.errors.room_id, function(e) {
                                    items += '<li style="display: block">' + e + '</li>';
                                });
                                selectRoom.closest('.form-group').addClass('has-error').find('.help-block').html('<ul>' + items + '</ul>');
                            }
                        }
                    }
                });
            });
            @php
            $roomsTmp = [];
            foreach ($rooms as $r) {
                $roomTypes = [];
                $housingRooms = $r->housingRooms->data;
                for ($i = 0; $i < count($housingRooms); $i++) {
                    array_push($roomTypes, $housingRooms[$i]->roomType->data->name);
                }
                $roomsTmp[] = [
                    'id' => $r->id,
                    'name' => $r->name,
                    'residence' => $r->residence->data->name,
                    'type' => $roomTypes,
                    'beds' => $r->beds,
                    'housing_slug' => $r->residence->data->housing->data->slug,
                    'blockedDates' => $r->blockedDates ?? [],
                ];
            }

            /**
             * Ordena los registros de forma ascendente(asc) o descendente(desc)
             */
            function sortRecords($rooms, $dir = 'asc') {
                $roomsCollection = groupRecords($rooms);
                $roomsOrdered = [];
                // Odenar por nombre de housing
                if ($dir === 'asc') {
                    $roomsCollection = $roomsCollection->sortBy(function ($item, $key) {
                        return $key;
                    });
                } else {
                    $roomsCollection = $roomsCollection->sortByDesc(function ($item, $key) {
                        return $key;
                    });
                }
                // Odenar por nombre de residencia
                foreach ($roomsCollection as $key => $rCollection) {
                    if ($dir === 'asc') {
                        $roomsCollection[$key] = $rCollection->sortBy(function ($item, $key) {
                            return str_slug($key, '_');
                        });
                    } else {
                        $roomsCollection[$key] = $rCollection->sortByDesc(function ($item, $key) {
                            return str_slug($key, '_');
                        });
                    }
                    // Ordenar por nombre de cuarto
                    foreach($roomsCollection[$key] as $key2 => $collection) {
                        if ($dir === 'asc') {
                            $roomsCollection[$key][$key2] = $collection->sortBy('name');
                        } else {
                            $roomsCollection[$key][$key2] = $collection->sortByDesc('name');
                        }
                        // Guardar los datos ordenados en el array que se retornará
                        $roomsOrdered = array_merge($roomsOrdered, $roomsCollection[$key][$key2]->toArray());
                    }
                }

                return $roomsOrdered;
            }

            /**
             * Agrupa los registros por tipo de housing y tipo de residencia
             */
            function groupRecords($rooms) {
                $roomsCollection = collect($rooms);
                // Agrupar por tipo de housing
                $roomsCollection = $roomsCollection->groupBy('housing_slug');
                // Agrupar por residencia
                foreach ($roomsCollection as $key => $rCollection) {
                    $roomsCollection[$key] = $rCollection->groupBy('residence');
                }

                return $roomsCollection;
            }

            $roomsTmp = sortRecords($roomsTmp, 'asc');
            @endphp
            EXP.command.housing.component.modalScheduler.ele.on('shown.bs.modal', function (e) {
                // Inicializar scheduler
                if (EXP.command.housing.component.modalScheduler.scheduler !== null) {
                    EXP.command.housing.component.modalScheduler.scheduler.ele.html('');
                }
                var data = EXP.command.housing.component.grid.data[EXP.command.housing.component.grid.selectedIndex];
                var date = moment(data.from_date);
                EXP.command.housing.component.modalScheduler.scheduler = new EXP.Scheduler({
                    selector: '#modal-scheduler #scheduler',
                    currentMonth: date.month() + 1,
                    currentYear: date.year(),
                    rooms: {!! json_encode($roomsTmp) !!},
                    url: '/admin/accommodations'
                });
            });
            EXP.command.payments.component = new EXP.CommandComponent({!! getData('payments', $formType, $command) !!}, {
                init: EXP.command.payments.initComponent,
                gridSelector: '#command-payments .grid-container',
                containerSelector: '#command-payments',
                modalSelector: '#a'
            });
        @endif
        EXP.currencies = {!!  json_encode($currencies) !!};
        EXP.command.allPayments.component = new EXP.CommandComponent({!! getData('all_payments', $formType, $command) !!}, {
            init: EXP.command.allPayments.initComponent,
            gridSelector: '#command-all-payments .grid-container',
            containerSelector: '#command-all-payments',
            modalSelector: '#modal-payments',
            readOnly: @if ($formType === Constant::FORM_SHOW) true @else false @endif
        });
        EXP.paymentObject = new EXP.command.allPayments.Object();
        EXP.paymentObject.init();
        EXP.residences = {data: []};
        // Mostrar errores
        @php
        if ($formType !== Constant::FORM_SHOW) {
            $keys = $errors->keys();
            foreach ($keys as $key) {
                $arrKey = explode('.', $key);
                $commandItemError = false;
                // Mostrar errores de cursos
                if (strpos($key, 'courses') !== false) {
                    @endphp
                    item = EXP.command.courses.component;
                    @php
                    $commandItemError = true;
                }
                if ($commandItemError) {
                    @endphp
                    item.ele.addClass('has-error');
                    @php
                    foreach ($errors->get($key) as $msg) {
                        @endphp
                        item.ele.find('.help-block > ul').append('<li>{{ $msg }}</li>');
                        @php
                    }
                    if (count($arrKey) > 1) {
                        @endphp
                        $(item.grid.ele.find('table tbody tr')[{{ $arrKey[1] }}]).addClass('danger');
                        @php
                    }
                }
            }
            foreach ($keys as $key) {
                $arrKey = explode('.', $key);
                $commandItemError = false;
                // Mostrar errores de housing
                if (strpos($key, 'housing') !== false) {
                    @endphp
                    item = EXP.command.housing.component;
                    @php
                    $commandItemError = true;
                }
                if ($commandItemError) {
                    @endphp
                    item.ele.addClass('has-error');
                    @php
                    foreach ($errors->get($key) as $msg) {
                        @endphp
                        item.ele.find('.help-block > ul').append('<li>{{ $msg }}</li>');
                        @php
                    }
                    if (count($arrKey) > 1) {
                        @endphp
                        $(item.grid.ele.find('table tbody tr')[{{ $arrKey[1] }}]).addClass('danger');
                        @php
                    }
                }
            }
            foreach ($keys as $key) {
                $arrKey = explode('.', $key);
                $commandItemError = false;
                // Mostrar errores de purchases
                if (strpos($key, 'purchases') !== false) {
                    @endphp
                    item = EXP.command.purchases.component;
                    @php
                    $commandItemError = true;
                }
                if ($commandItemError) {
                    @endphp
                    item.ele.addClass('has-error');
                    @php
                    foreach ($errors->get($key) as $msg) {
                        @endphp
                        item.ele.find('.help-block > ul').append('<li>{{ $msg }}</li>');
                        @php
                    }
                    if (count($arrKey) > 1) {
                        @endphp
                        $(item.grid.ele.find('table tbody tr')[{{ $arrKey[1] }}]).addClass('danger');
                        @php
                    }
                }
            }
            @endphp
            EXP.packages.data = {!! json_encode($packages) !!};
            EXP.housing.data = {!! json_encode($housing) !!};
            @php
        }
        if ($formType !== Constant::FORM_CREATE) {
            @endphp
            EXP.residences = {data: {!! json_encode($residences) !!}};
            @php
        }
        @endphp

        @if (request()->has('client_id'))
            loadClientAsDefault("{{ route('clients.show', ['id' => request()->get('client_id')]) }}");
        @endif
    });
</script>
@endsection
@section('scripts')
    @parent
    <script src="{{ asset('js/vue.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(document).on('select2:select', '.select2-v', function(e) {
                $(e.target).trigger2('change');
            });

            $('#select-agency').select2({
                placeholder: "Select an agency",
                allowClear: true
            });

            function clearTravelInformation($travelInformationC) {
                if ($travelInformationC === void 0) {
                    $travelInformationC = $('.travel-information__client');
                }

                $travelInformationC.find('#travel_by_bus, #travel_by_plane')
                    .prop('checked', false)
                    .iCheck('update');

                $travelInformationC.find('select')
                    .val('')
                    .trigger('change.select2');

                $travelInformationC.find('input, textarea')
                    .val('');
            }

            new Vue({
                el: '#app',
                data: {
                    bills: JSON.parse(document.querySelector('#bills_hidden').value || '[]'),
                    errors: {!! json_encode($errors->get('command_bills.*')) !!}
                },
                methods: {
                    /**
                    * Delete item from dynamic list.
                    * @item element to delete
                    * @items array with all items
                    * @itemsDeleted array of deleted items [{ id: 1 }, {id: 9}]
                    * @keepElements If a item will never deleted but clear content: {keep: 1, itemBase: {from: '', to: '', pax: ''}}
                    */
                    deleteItemDynamicList: function(item, items, itemsDeleted, keepElements) {
                        swal({
                            title: "Delete bill",
                            text: "Are you sure to delete the selected bill",
                            type: 'question',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, do it!'
                        }).then(function () {
                            var index = items.indexOf(item);
                            // Log id of items deleted
                            if (itemsDeleted) {
                                var itemId = items[index].id || null;
                                if (itemId) {
                                    itemsDeleted.push({ id: itemId });
                                }
                            }
                            // Keep item but clear contents
                            if (keepElements) {
                                var keep = keepElements.keep || null,
                                    itemsTotal = items.length;
                                if (keep && (itemsTotal <= keep)) {
                                    items.splice(index, 1, keepElements. itemBase);
                                    return;
                                }
                            }
                            // Delete item
                            items.splice(index, 1);
                        }).catch(swal.noop);
                    },
                    addItemDynamicList: function(itemBase, items) {
                        items.push(itemBase);
                    },
                    showErrorMessage: function(index, field) {
                        return this.errors['command_bills.' + index + "." + field]? this.errors['command_bills.' + index + "." + field][0] : '';
                    },
                    handleCurrencyChange(bill, e) {
                        if (e.target.selectedIndex >= 0) {
                            bill.currency = e.target.options[e.target.selectedIndex].innerText;
                        }
                    }
                },
                watch: {
                    bills: function() {
                        var self = this;
                        setTimeout(function() {
                            $('.select2-v').select2();
                        });
                    }
                },
                mounted: function() {
                    var $msgErrorTInfo = $('#msg-error-t-inf');
                    var $travelInformationC = $('.travel-information__client');
                    var $resetTravelInformation = $('#reset-travel-information');

                    $('.select2-v').select2();
                    @if (empty(old('command_bills')))
                        @if($formType !== Constant::FORM_EDIT && $formType !== Constant::FORM_SHOW)
                            $('#add-bill').trigger('click');
                        @endif
                    @endif

                    if (
                        $msgErrorTInfo.length > 0 &&
                        $travelInformationC.find('.has-error').length > 0
                    ) {
                        $msgErrorTInfo.removeClass('hidden');
                    }

                    $resetTravelInformation.click(function(e) {
                        e.preventDefault();

                        clearTravelInformation($travelInformationC);
                    });
                }
            });
        });
    </script>
@endsection
