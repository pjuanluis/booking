<style type="text/css">
    .modal .help-block ul {
        margin: 0;
        padding: 0;
        list-style-type: none;
    }
    .modal .help-block ul li {
        display: none;
    }
</style>
@php
$messages = \Illuminate\Support\Arr::dot((array) trans('validation.custom'));
@endphp

<div class="modal fade" id="modal-course" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Add experience</h4>
            </div>
            <div class="modal-body">
                <form action="#!">
                    <input type="hidden" name="id" id="id">
                    <div class="box-body">
                        <div class="form-group col-sm">
                            <label class="control-label">Experiences</label>
                            <div>
                                @foreach ($experiences as $exp)
                                    <label style="margin-right: 10px; font-weight: normal;">
                                        <input type="checkbox" class="iCheck minimal experiences" data-experience="{{ $exp->slug }}">
                                        {{ $exp->name }}
                                    </label>
                                @endforeach
                                <div>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-sm">
                            <label for="select-package" class="control-label">Package</label>
                            <div>
                                <select class="form-control select2" id="select-package-course" style="width: 100%;" name="package_id" required></select>
                                <span class="help-block">
                                    <ul>
                                        <li rule="required">{{ $messages['courses.*.package_id.required'] }}</li>
                                    </ul>
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="course-quantity">Quantity</label>
                                <input type="number" class="form-control" id="course-quantity" name="quantity" required rule="integer|min:1" />
                                <span class="help-block">
                                    <ul>
                                        <li rule="required">{{ $messages['courses.*.quantity.required'] }}</li>
                                        <li rule="integer">{{ $messages['courses.*.quantity.integer'] }}</li>
                                        <li rule="min">{{ str_replace(':min', '1', $messages['courses.*.quantity.min']) }}</li>
                                    </ul>
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="course-begin">Begin</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="course-begin" name="from_date" rule="date" required />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                    </span>
                                </div>
                                <span class="help-block">
                                    <ul>
                                        <li rule="required">{{ $messages['courses.*.from_date.required'] }}</li>
                                        <li rule="date">{{ str_replace(':format', 'dd-mm-yyyy', $messages['courses.*.from_date.date_format']) }}</li>
                                    </ul>
                                </span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="course-end">End</label>
                                <input type="text" class="form-control" id="course-end" name="to_date" rule="date|after:from_date" required />
                                <span class="help-block">
                                    <ul>
                                        <li rule="required">{{ $messages['courses.*.to_date.required'] }}</li>
                                        <li rule="date">{{ str_replace(':format', 'dd/mm/yyyy', $messages['courses.*.to_date.date_format']) }}</li>
                                        <li rule="after">{{ $messages['courses.*.to_date.after'] }}</li>
                                    </ul>
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="course-cost">Cost</label>
                                <input type="number" class="form-control" id="course-cost" name="cost" required />
                                <span class="help-block">
                                    <ul>
                                        <li rule="required">{{ $messages['courses.*.cost.required'] }}</li>
                                        <li rule="numeric">{{ $messages['courses.*.cost.numeric'] }}</li>
                                    </ul>
                                </span>
                            </div>
                            <div class="form-group col-sm-6">
                                <input type="hidden" id="course-current-exchange-rates">
                                <label for="select-currency-course" class="control-label">Currency</label>
                                <div>
                                    <select class="form-control select2" id="select-currency-course" style="width: 100%;" name="currency_id" required>
                                        @foreach ($currencies as $key => $currency)
                                            <option value="{{ $currency->id }}">{{ $currency->code }}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-block">
                                        <ul>
                                            <li rule="required">{{ $messages['currency_id.required'] }}</li>
                                        </ul>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="course-discount">Discount(%)</label>
                                <input type="number" class="form-control" id="course-discount" name="discount" min="0" step="1" required rule="integer|min:0" value="0"/>
                                <span class="help-block">
                                    <ul>
                                        <li rule="required">{{ $messages['courses.*.discount.required'] }}</li>
                                        <li rule="numeric">{{ $messages['courses.*.discount.numeric'] }}</li>
                                        <li rule="min">{{ str_replace(':min', '0', $messages['courses.*.discount.min']) }}</li>
                                    </ul>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="button" class="btn btn-success add">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-purchase" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Add purchase</h4>
            </div>
            <div class="modal-body">
                <form action="#!">
                    <input type="hidden" name="id" id="id">
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="purchase_date">Date</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="purchase_date" name="purchase_date" rule="date" required />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                    </span>
                                </div>
                                <span class="help-block">
                                    <ul>
                                        <li rule="required">{{ $messages['purchase.*.date.required'] }}</li>
                                        <li rule="date">{{ str_replace(':format', 'dd-mm-yyyy', $messages['purchase.*.date.date_format']) }}</li>
                                    </ul>
                                </span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="purchase_reference_number" class="control-label">Reference Number</label>
                                <input type="text" class="form-control" id="purchase_reference_number" name="purchase_reference_number">
                                <span class="help-block">

                                </span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-xs-12">
                                <label for="purchase_concept" class="control-label">Concept</label>
                                <textarea name="purchase_concept" id="purchase_concept" rows="6" class="form-control" required></textarea>
                                <span class="help-block">
                                    <ul>
                                        <li rule="required">{{ $messages['purchase.*.concept.required'] }}</li>
                                    </ul>
                                </span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="purchase_amount">Amount</label>
                                <input type="number" class="form-control" id="purchase_amount" name="purchase_amount" min="0" step="any" required>
                                <span class="help-block">
                                    <ul>
                                        <li rule="required">{{ $messages['purchase.*.amount.required'] }}</li>
                                        <li rule="numeric">{{ $messages['purchase.*.amount.numeric'] }}</li>
                                    </ul>
                                </span>
                            </div>
                            <div class="form-group col-sm-6">
                                <input type="hidden" id="purchase-current-exchange-rates">
                                <label for="purchase_currency" class="control-label">Currency</label>
                                <div>
                                    <select class="form-control select2" id="purchase_currency" style="width: 100%;" name="purchase_currency" required>
                                        @foreach ($currencies as $key => $currency)
                                            <option value="{{ $currency->id }}">{{ $currency->code }}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-block">
                                        <ul>
                                            <li rule="required">{{ $messages['currency_id.required'] }}</li>
                                        </ul>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="purchase_discount">Discount(%)</label>
                                <input type="number" class="form-control" id="purchase_discount" name="purchase_discount" min="0" step="1" required rule="integer|min:0" value="0"/>
                                <span class="help-block">
                                    <ul>
                                        <li rule="required">{{ $messages['purchase.*.discount.required'] }}</li>
                                        <li rule="numeric">{{ $messages['purchase.*.discount.numeric'] }}</li>
                                        <li rule="min">{{ str_replace(':min', '0', $messages['purchase.*.discount.min']) }}</li>
                                    </ul>
                                </span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="purchase_consumption_center" class="control-label">Consumption Center</label>
                                <div>
                                    <select name="purchase_consumption_center" id="purchase_consumption_center" class="form-control select2" required style="width: 100%;">
                                        @foreach ($consumptionCenters->data as $cc)
                                            <option value="{{ $cc->id }}">
                                                {{ $cc->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <span class="help-block">
                                        <ul>
                                            <li rule="required">{{ $messages['consumption_center.required'] }}</li>
                                        </ul>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="button" class="btn btn-success add">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-payments" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Add payment</h4>
            </div>
            <div class="modal-body">
                <form action="#!">
                    <input type="hidden" name="id" id="id">
                    <input type="hidden" name="invoiced_at" id="invoiced_at">
                    <input type="hidden" name="paid_at" id="paid_at">
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="payment_amount">Amount</label>
                                <input type="number" class="form-control" id="payment_amount" name="payment_amount" min="1" step="any" required/>
                                <span class="help-block">
                                    <ul>
                                        <li rule="required">{{ $messages['payment.*.amount.required'] }}</li>
                                        <li rule="numeric">{{ $messages['payment.*.amount.numeric'] }}</li>
                                    </ul>
                                </span>
                            </div>
                            <div class="form-group col-sm-6">
                                <input type="hidden" id="payment-current-exchange-rates">
                                <label for="payment_currency" class="control-label">Currency</label>
                                <div>
                                    <select class="form-control select2" id="payment_currency" style="width: 100%;" name="payment_currency" required>
                                        @foreach ($currencies as $key => $currency)
                                            <option value="{{ $currency->id }}">{{ $currency->code }}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-block">
                                        <ul>
                                            <li rule="required">{{ $messages['currency_id.required'] }}</li>
                                        </ul>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="payment_reference_number" class="control-label">Reference Number</label>
                                <input type="text" class="form-control" id="payment_reference_number" name="payment_reference_number">
                                <span class="help-block">

                                </span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="payment_payment_method" class="control-label">Payment method</label>
                                <div>
                                    <select name="payment_payment_method" id="payment_payment_method" class="form-control select2" required style="width: 100%;">
                                        @foreach ($methodsPayment as $mp)
                                            <option value="{{ $mp->id }}" {{ $mp->slug === 'cash'? 'selected' : ''}}>
                                                {{ $mp->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <span class="help-block">
                                        <ul>
                                            <li rule="required">{{ $messages['payment_method.required'] }}</li>
                                        </ul>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="payment_vendor" class="control-label">Vendor</label>
                                <input type="text" class="form-control" id="payment_vendor" name="payment_vendor">
                                <span class="help-block">

                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="button" class="btn btn-success add">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-housing" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Add housing</h4>
            </div>
            <div class="modal-body">
                <form action="#!">
                    <input type="hidden" name="id" id="id">
                    <input type="hidden" name="h-room" id="h-room">
                    <input type="hidden" name="h-residence" id="h-residence">
                    <input type="hidden" name="id" id="id">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="select-housing" class="control-label">Housing</label>
                            <div>
                                <select class="form-control select2" id="select-housing" style="width: 100%;" name="housing_id" required>
                                    @foreach ($housing as $h)
                                        <option value="{{ $h->id }}">{{ $h->name }}</option>
                                    @endforeach
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="housing-select-room" class="control-label">Room type</label>
                            <div>
                                <select class="form-control select2" id="housing-select-room" style="width: 100%;" name="housing_room_id" required>
                                    @foreach ($housing[0]->housingRooms->data as $room)
                                        <option value="{{ $room->id }}">{{ $room->name }}</option>
                                    @endforeach
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="select-package" class="control-label">Options</label>
                            <div>
                                <select class="form-control select2" id="select-package" style="width: 100%;" name="housing_room_package_id" required>
                                    @foreach ($housing[0]->housingRooms->data[0]->packages->data as $package)
                                        <option value="{{ $package->housing_room_package_id }}" data-id="{{ $package->id }}">{{ $package->name }}</option>
                                    @endforeach
                                </select>
                                <span class="help-block">
                                    <ul>
                                        <li rule="required">{{ $messages['housing.*.housing_room_package_id.required'] }}</li>
                                    </ul>
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="housing-quantity">Quantity</label>
                                <input type="number" class="form-control" id="housing-quantity" name="quantity" required rule="integer|min:1" />
                                <span class="help-block">
                                    <ul>
                                        <li rule="required">{{ $messages['housing.*.quantity.required'] }}</li>
                                        <li rule="integer">{{ $messages['housing.*.quantity.integer'] }}</li>
                                        <li rule="min">{{ str_replace(':min', '1', $messages['housing.*.quantity.min']) }}</li>
                                    </ul>
                                </span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="select-housing-unit" class="control-label">Unit</label>
                                <div>
                                    <select class="form-control select2" id="select-housing-unit" style="width: 100%;" name="unit" required>
                                        <option value="night">Night</option>
                                        <option value="week">Week</option>
                                        <option value="month">Month</option>
                                    </select>
                                    <span class="help-block">
                                        <ul>
                                            <li rule="required">{{ $messages['housing.*.unit.required'] }}</li>
                                        </ul>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="housing-begin">Begin</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="housing-begin" name="from_date" rule="date" required />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                    </span>
                                </div>
                                <span class="help-block">
                                    <ul>
                                        <li rule="required">{{ $messages['housing.*.from_date.required'] }}</li>
                                        <li rule="date">{{ str_replace(':format', 'dd-mm-yyyy', $messages['housing.*.from_date.date_format']) }}</li>
                                    </ul>
                                </span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="housing-end">End</label>
                                <input type="text" class="form-control" id="housing-end" name="to_date" rule="date|after:from_date" required readonly/>
                                <span class="help-block">
                                    <ul>
                                        <li rule="required">{{ $messages['housing.*.to_date.required'] }}</li>
                                        <li rule="date">{{ str_replace(':format', 'dd/mm/yyyy', $messages['housing.*.to_date.date_format']) }}</li>
                                        <li rule="after">{{ $messages['housing.*.to_date.after'] }}</li>
                                    </ul>
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="housing-cost">Cost</label>
                                <input type="number" class="form-control" id="housing-cost" name="cost" required/>
                                <span class="help-block">
                                    <ul>
                                        <li rule="required">{{ $messages['housing.*.cost.required'] }}</li>
                                        <li rule="numeric">{{ $messages['housing.*.cost.numeric'] }}</li>
                                    </ul>
                                </span>
                            </div>
                            <div class="form-group col-sm-6">
                                <input type="hidden" id="housing-current-exchange-rates">
                                <label for="select-currency-housing" class="control-label">Currency</label>
                                <div>
                                    <select class="form-control select2" id="select-currency-housing" style="width: 100%;" name="housing_currency" required>
                                        @foreach ($currencies as $key => $currency)
                                            <option value="{{ $currency->id }}">{{ $currency->code }}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-block">
                                        <ul>
                                            <li rule="required">{{ $messages['currency_id.required'] }}</li>
                                        </ul>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="housing-people">People</label>
                                <select class="form-control select2" id="select-housing-people" style="width: 100%;" name="people" required rule="integer|min:1">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                </select>
                                <span class="help-block">
                                    <ul>
                                        <li rule="required">{{ $messages['housing.*.people.required'] }}</li>
                                        <li rule="integer">{{ $messages['housing.*.people.integer'] }}</li>
                                        <li rule="min">{{ str_replace(':min', '1', $messages['housing.*.people.min']) }}</li>
                                    </ul>
                                </span>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="housing-discount">Discount(%)</label>
                                <input type="number" class="form-control" id="housing-discount" name="housing-discount" min="0" step="1" required rule="integer|min:0" value="0"/>
                                <span class="help-block">
                                    <ul>
                                        <li rule="required">{{ $messages['housing.*.discount.required'] }}</li>
                                        <li rule="numeric">{{ $messages['housing.*.discount.numeric'] }}</li>
                                        <li rule="min">{{ str_replace(':min', '0', $messages['housing.*.discount.min']) }}</li>
                                    </ul>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="button" class="btn btn-success add">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-scheduler" role="dialog" style="">
    <div class="modal-dialog" role="document" style="width: 95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Select room</h4>
            </div>
            <div class="modal-body">
                <div id="housing-data">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="housing">Housing</label>
                            <input type="text" class="form-control" id="housing" readOnly />
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="begin-date">Begin</label>
                            <input id="begin-date" type="text" class="form-control" readOnly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="housing-room">Room type</label>
                            <input id="housing-room" type="text" class="form-control" readOnly>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="end-date">End</label>
                            <input id="end-date" type="text" class="form-control" readOnly>
                        </div>
                    </div>
                </div>
                <form action="#!">
                    <input type="hidden" name="id" id="id">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="select-residences" class="control-label">Residence</label>
                            <select id="select-residences" style="width: 100%;" class="form-control select2" name="residence_id" required></select>
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="select-room" class="control-label">Room</label>
                            <div class="row">
                                <div class="col-md-8">
                                    <select id="select-room" style="width: 100%;" name="room_id" class="form-control select2" required></select>
                                    <span class="help-block"></span>
                                </div>
                                <div class="col-md-4">
                                    <label>
                                        <input type="checkbox" id="exchange" class="iCheck"> Exchange
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div id="scheduler" class="sch-scheduler" style="padding: 0 10px;"></div>
            </div>
            <div class="box-footer">
                <button type="button" class="btn btn-success add">Update</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function updateHousingEndDate($housingBegin, $housingEnd, $housingQuantity, $housingUnit) {
        var beginDate = null;
        var quantity = null;
        var endDate = null;
        var unit = null;

        if ($housingBegin.val().trim().length > 0) {
            beginDate = moment($housingBegin.val(), 'DD-MM-YYYY');
        }

        if ($housingQuantity.val().trim().length > 0) {
            quantity = $housingQuantity.val();
        }

        if ($housingUnit.val().trim().length > 0) {
            unit = $housingUnit.val();
        }

        if (beginDate && quantity && unit && quantity > 0) {
            if (unit.trim().toUpperCase() === 'WEEK') {
                quantity *= 7; // 7 days in a week
            } else if (unit.trim().toUpperCase() === 'MONTH') {
                quantity *= 28; // 1 mes = 4 semanas, 1 semana = 7 días, 7 * 4 = 28 días del mes
            }

            endDate = beginDate.clone()
                .startOf('day')
                .add(quantity, 'days');
            $housingEnd.val(endDate.format('DD-MM-YYYY'));
        } else {
            $housingEnd.val('');
        }
    }

    window.addEventListener('load', function() {
        var $housingBegin = $('#housing-begin');
        var $housingEnd = $('#housing-end');
        var $housingQuantity = $('#housing-quantity');
        var $housingUnit = $('#select-housing-unit');

        $housingEnd.click(function(e) {
            $housingEnd.datepicker('destroy');
        });

        $housingBegin.change(function(e) {
            updateHousingEndDate($housingBegin, $housingEnd, $housingQuantity, $housingUnit);
        });

        $housingQuantity.change(function() {
            updateHousingEndDate($housingBegin, $housingEnd, $housingQuantity, $housingUnit);
        }).keyup(function() {
            updateHousingEndDate($housingBegin, $housingEnd, $housingQuantity, $housingUnit);
        });

        $housingUnit.change(function(e) {
            updateHousingEndDate($housingBegin, $housingEnd, $housingQuantity, $housingUnit);
        });
    });
</script>
