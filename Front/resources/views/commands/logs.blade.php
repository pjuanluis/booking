@extends('layouts.app')
use Carbon\Carbon;

@section('title')
    Bookings logs
@endsection

@section('content-header')
    <h1>
        Bookings logs
    </h1>
@endsection

@section('content')
<style>
    .code-exp {
        background-color: #f5f5f5;
        padding: 5px 5px 5px 10px;
        border-radius: 3px;
        font-family: monospace;
    }
    .code-exp .tab {
        padding-left: 16px;
    }
    .code-exp span {
        display: block;
    }
    .logs-accommodation {
        position: relative;
    }
    .logs-accommodation .icon {
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
    }
</style>

<div class="box" style="min-height: 570px;">
    <div class="box-body">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            @php
                $date = null;
            @endphp
            @foreach ($logs->data as $log)
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            @php
                                $date = new Carbon\Carbon($log->created_at);
                                $date->tz = 'America/Mexico_City';
                            @endphp
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#log-{{ $log->id }}" aria-expanded="true">
                                {{ $date->format('d-m-Y H:i:s') }} / {{ $log->user->data->full_name }} / {{ $log->action }}
                            </a>
                        </h4>
                    </div>
                    <div id="log-{{ $log->id }}" class="panel-collapse collapse" role="tabpanel">
                        <div class="panel-body">
                            @php
                                $data = $log->data;
                            @endphp
                            @if ($log->action === 'Store accommodation' || $log->action === 'Exchange accommodation')
                                <div class="row logs-accommodation">
                                    <div class="col-md-5">
                                        <div class="code-exp">
                                            <span>Housing:</span>
                                            <span class="tab">{{ $data->command_housing->name }}</span>
                                            <span>Before:</span>
                                            <span class="tab">Room: {{ $data->command_housing->before->room }}</span>
                                            <span>After:</span>
                                            <span class="tab">Room: {{ $data->command_housing->after->room }}</span>
                                        </div>
                                    </div>
                                    @if ($log->action === 'Exchange accommodation')
                                        <span class="icon">
                                            <i class="fa fa-exchange" aria-hidden="true"></i>
                                        </span>
                                        <div class="col-md-2 text-center" style="padding: 15px 0;">
                                        </div>
                                        <div class="col-md-5">
                                            <div class="code-exp">
                                                <span>Housing:</span>
                                                <span class="tab">{{ $data->command_housing_exchange->name }}</span>
                                                <span>Before:</span>
                                                <span class="tab">Room: {{ $data->command_housing_exchange->before->room }}</span>
                                                <span>After:</span>
                                                <span class="tab">Room: {{ $data->command_housing_exchange->after->room }}</span>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            @elseif ($log->action === 'mark payments as invoiced')
                                <div class="code-exp">
                                    <div class="box">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Payments</h3>
                                        </div>
                                        <div class="box-body">
                                            <div class="table-responsive grid-container">
                                                <table class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>Invoiced</th>
                                                            <th>Reference number</th>
                                                            <th>Concept</th>
                                                            <th>Amount</th>
                                                            <th>Currency</th>
                                                            <th>Payment method</th>
                                                            <th>Paid at</th>
                                                            <th>Vendor</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="8"><b>Before</b></td>
                                                        </tr>
                                                        @foreach ($data->payments->before as $payment)
                                                            <tr>
                                                                <td>{{ $payment->invoiced_at ? 'yes' : 'no' }}</td>
                                                                <td>{{ $payment->reference_id }}</td>
                                                                <td>{{ $payment->concept }}</td>
                                                                <td>{{ $payment->amount }}</td>
                                                                <td>{{ $payment->currency->code }}</td>
                                                                <td>{{ $payment->payment_method->name }}</td>
                                                                <td>{{ (new Carbon\Carbon($payment->paid_at))->format('d-m-Y H:i:s') }}</td>
                                                                <td>{{ $payment->vendor }}</td>
                                                            </tr>
                                                        @endforeach
                                                        <tr>
                                                            <td colspan="8"><b>After</b></td>
                                                        </tr>
                                                        @foreach ($data->payments->after as $payment)
                                                            <tr>
                                                                <td>{{ $payment->invoiced_at ? 'yes' : 'no' }}</td>
                                                                <td>{{ $payment->reference_id }}</td>
                                                                <td>{{ $payment->concept }}</td>
                                                                <td>{{ $payment->amount }}</td>
                                                                <td>{{ $payment->currency->code }}</td>
                                                                <td>{{ $payment->payment_method->name }}</td>
                                                                <td>{{ (new Carbon\Carbon($payment->paid_at))->format('d-m-Y H:i:s') }}</td>
                                                                <td>{{ $payment->vendor }}</td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="code-exp">
                                    <span>Booking Information:</span>
                                    <span class="tab">Booking code: {{ $data->booking_code ?? '' }}</span>
                                    <span class="tab">Agency: {{ $data->agency_name ?? '' }}</span>
                                    <span>Client:</span>
                                    <span class="tab">New client: {{ $data->new_client == 1 ? 'Yes' : 'No' }}</span>
                                    <span class="tab">Full name: {{ $data->client_name }}</span>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <span class="tab">First name: {{ $data->client_first_name ?? '' }}</span>
                                        </div>
                                        <div class="col-md-4">
                                            <span class="tab">Last name: {{ $data->client_last_name ?? '' }}</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <span class="tab">Email: {{ $data->client_email ?? '' }}</span>
                                        </div>
                                        <div class="col-md-4">
                                            <span class="tab">Country: {{ $data->country_name ?? ''  }}</span>
                                        </div>
                                    </div>
                                    <span>Travel partner:</span>
                                    <span class="tab">Full name: {{ $data->travel_partner }}</span>
                                    @if (!empty($data->courses))
                                        <div class="box">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">Experiences</h3>
                                            </div>
                                            <div class="box-body">
                                                <div class="box-body table-responsive no-padding">
                                                    <table class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>Experience</th>
                                                                <th>Package</th>
                                                                <th>Q</th>
                                                                <th>Cost</th>
                                                                <th>Currency</th>
                                                                <th>Discount</th>
                                                                <th>Begin</th>
                                                                <th>End</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($data->courses as $course)
                                                                <tr>
                                                                    <td>{{ implode(',', $course->experiences) }}</td>
                                                                    <td>{{ $course->package }}</td>
                                                                    <td>{{ $course->quantity }}</td>
                                                                    <td>{{ $course->cost }}</td>
                                                                    <td>{{ $course->currency }}</td>
                                                                    <td>{{ $course->discount }}</td>
                                                                    <td>{{ (new Carbon\Carbon($course->from_date))->format('d-m-Y') }}</td>
                                                                    <td>{{ (new Carbon\Carbon($course->to_date))->format('d-m-Y') }}</td>
                                                                    <td>{{ $course->id >= 1 ? 'Update' : 'Create' }}</td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if (!empty($data->housing))
                                        <div class="box">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">Housing</h3>
                                            </div>
                                            <div class="box-body">
                                                <div class="box-body table-responsive no-padding">
                                                    <table class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>Housing</th>
                                                                <th>Room type</th>
                                                                <th>Package</th>
                                                                <th>Room</th>
                                                                <th>Residence</th>
                                                                <th>Q</th>
                                                                <th>U</th>
                                                                <th>Cost</th>
                                                                <th>Currency</th>
                                                                <th>Discount</th>
                                                                <th>Begin</th>
                                                                <th>End</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($data->housing as $housing)
                                                                <tr>
                                                                    <td>{{ $housing->housing }}</td>
                                                                    <td>{{ $housing->housing_room }}</td>
                                                                    <td>{{ $housing->package }}</td>
                                                                    <td>{{ $housing->room_number }}</td>
                                                                    <td>{{ $housing->residence }}</td>
                                                                    <td>{{ $housing->quantity }}</td>
                                                                    <td>{{ $housing->unit }}</td>
                                                                    <td>{{ $housing->cost }}</td>
                                                                    <td>{{ $housing->currency }}</td>
                                                                    <td>{{ $housing->discount }}</td>
                                                                    <td>{{ (new Carbon\Carbon($housing->from_date))->format('d-m-Y') }}</td>
                                                                    <td>{{ (new Carbon\Carbon($housing->to_date))->format('d-m-Y') }}</td>
                                                                    <td>{{ $housing->id >= 1 ? 'Update' : 'Create' }}</td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if (!empty($data->purchases))
                                        <div class="box">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">Extra purchases</h3>
                                            </div>
                                            <div class="box-body">
                                                <div class="box-body table-responsive no-padding">
                                                    <table class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>Date</th>
                                                                <th>Reference number</th>
                                                                <th>Concept</th>
                                                                <th>Amount</th>
                                                                <th>Currency</th>
                                                                <th>Discount</th>
                                                                <th>Consumption center</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($data->purchases as $purchase)
                                                                <tr>
                                                                    <td>{{ (new Carbon\Carbon($purchase->date))->format('d-m-Y') }}</td>
                                                                    <td>{{ $purchase->reference_number }}</td>
                                                                    <td>{{ $purchase->concept }}</td>
                                                                    <td>{{ $purchase->amount }}</td>
                                                                    <td>{{ $purchase->currency }}</td>
                                                                    <td>{{ $purchase->discount }}</td>
                                                                    <td>{{ $purchase->consumption_center }}</td>
                                                                    <td>{{ $purchase->id >= 1 ? 'Update' : 'Create' }}</td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <span>Travel information:</span>
                                    <span class="tab">Action: {{ $data->id_travel_information >= 1 ? 'Update' : (!empty($data->travel_by) ? 'Create' : '') }}</span>
                                    <span class="tab">Travel by: {{ $data->travel_by ?? '' }}</span>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <span class="tab">Arrival place: {{ $data->arrival_place }}</span>
                                            <span class="tab">Airline/Busline: {{ $data->arrival_carrier }}</span>
                                            <span class="tab">Arrival date: {{ $data->arrival_date }}</span>
                                            <span class="tab">Arrival time: {{ $data->arrival_time }}</span>
                                            <span class="tab">Flight number/Bus number: {{ $data->arrival_travel_number }}</span>
                                        </div>
                                        <div class="col-md-6">
                                            <span class="tab">Departure place: {{ $data->departure_place }}</span>
                                            <span class="tab">Airline/Busline: {{ $data->departure_carrier }}</span>
                                            <span class="tab">Departure date: {{ $data->departure_date }}</span>
                                            <span class="tab">Departure time: {{ $data->departure_time }}</span>
                                            <span class="tab">Flight number/Bus number: {{ $data->departure_travel_number }}</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <span class="tab">Transfer services: {{ $data->pickup_name }}</span>
                                        </div>
                                        <div class="col-md-6">
                                            <span class="tab">Transfer cost: {{ $data->pickup_cost }} {{ $data->pickup_id ? $data->pickup_currency : '' }}</span>
                                            <span class="tab">Transfer discount %: {{ $data->pickup_id ? $data->pickup_discount : '' }}</span>
                                        </div>
                                    </div>
                                    @if (!empty($data->payments))
                                        <div class="box">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">Payments</h3>
                                            </div>
                                            <div class="box-body">
                                                <div class="table-responsive grid-container">
                                                    <table class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>Reference number</th>
                                                                <th>Concept</th>
                                                                <th>Amount</th>
                                                                <th>Currency</th>
                                                                <th>Payment method</th>
                                                                <th>Paid at</th>
                                                                <th>Vendor</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($data->payments as $payment)
                                                                <tr>
                                                                    <td>{{ $payment->reference_id }}</td>
                                                                    <td>{{ $payment->concept }}</td>
                                                                    <td>{{ $payment->amount }}</td>
                                                                    <td>{{ $payment->currency }}</td>
                                                                    <td>{{ $payment->payment_method }}</td>
                                                                    <td>{{ (new Carbon\Carbon($payment->paid_at))->format('d-m-Y H:i:s') }}</td>
                                                                    <td>{{ $payment->vendor }}</td>
                                                                    <td>{{ $payment->id >= 1 ? 'Update' : 'Create' }}</td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="code-exp">
                                        <span>Payment status:</span>
                                        <span class="tab">
                                            {{ str_replace('_', ' ', $data->payment_status) }}
                                        </span>
                                        <span>Comments:</span>
                                        <span class="tab">
                                            {!! nl2br($data->comments) !!}
                                        </span>
                                        <span>Send email?:</span>
                                        <span class="tab">
                                            {{ !empty($data->send_email) && $data->send_email == '1' ? 'Yes' : 'N0' }}
                                        </span>
                                    </div>
                                    @if (!empty($data->command_bills))
                                        <div id="app" class="box">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">Billing Reference</h3>
                                            </div>
                                            <div class="box-body">
                                                <div class="table-responsive">
                                                    <table class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>Reference number</th>
                                                                <th>Amount</th>
                                                                <th>Currency</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($data->command_bills as $bill)
                                                                <tr>
                                                                    <td>{{ $bill->reference_number }}</td>
                                                                    <td>{{ $bill->amount }}</td>
                                                                    <td>{{ $bill->currency }}</td>
                                                                    <td>{{ $bill->id >= 1 ? 'Update' : 'Create' }}</td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        @if (!empty($logs->data))
            {!! Component::pagination(
                10,
                $logs->meta->pagination->current_page,
                $logs->meta->pagination->total_pages,
                $params
                ) !!}
        @endif
    </div>
</div>
<script type="text/javascript">
</script>
@endsection
