@extends('layouts.app')

@section('title')
    Importations
@endsection

@section('content-header')
    <h1>Importations</h1>
@endsection

@section('content')
<style type="text/css">
    .modal .help-block ul {
        margin: 0;
        padding: 0;
        list-style-type: none;
    }
    .modal .help-block ul li {
        display: none;
    }
</style>
<div class="box" style="">
    <div class="box-header with-border">
        <button id="btn-import" type="button" class="btn btn-primary pull-right"><i class="fa fa-file-excel-o"></i> Import file</button>
    </div>
    <div class="box-body table-responsive no-padding">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>File name</th>
                    <th>Description</th>
                    <th>Started at</th>
                    <th>Finished at</th>
                    <th>Details</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($imports->data as $import)
                @php
                $class = '';
                if ($import->status === 'invalid_excel') {
                    $class = 'danger';
                }
                else if ($import->status !== 'imported') {
                    $class = 'info';
                }
                @endphp
                <tr class="{{ $class }}">
                    <td>{{ $import->filename }}</td>
                    <td>{{ $import->description }}</td>
                    <td>{{ (new Carbon\Carbon($import->created_at))->format('d-m-Y') }}</td>
                    <td>{{ (new Carbon\Carbon($import->finished_at))->format('d-m-Y') }}</td>
                    <td>{{ $import->details }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="modal-import" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Import excel file</h4>
            </div>
            <div class="modal-body">
                <form action="#!">
                    <input type="hidden" name="id" id="id">
                    <div class="form-group">
                        <label for="file" class="control-label">File</label>
                        <input type="file" class="form-control" id="file" name="file"
                               accept="application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/xls" required/>
                        <span class="help-block">
                            <ul>
                                <li rule="required">File is required</li>
                            </ul>
                        </span>
                    </div>
                    <div class="form-group">
                        <label for="description" class="control-label">Description</label>
                        <textarea class="form-control" id="description" name="description"></textarea>
                    </div>
                </form>
                <div id="progress-info" style="display: none;">
                    <div class="progress active" style="margin-bottom: 0;">
                        <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                            <span class="sr-only">0% Complete</span>
                        </div>
                    </div>
                    <p id="progress-msg"></p>
                </div>
            </div>
            <div class="modal-footer">
                <button id="btn-startimport" type="button" class="btn btn-success">Start importation</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    window.addEventListener('load', function() {
        var modal = $('#modal-import');
        var reload = false;
        modal.modal({
            backdrop: false,
            keyboard: false,
            show: false
        });
        modal.on('hide.bs.modal', function() {
            if (reload) {
                window.location.reload();
            }
        });
        $('button#btn-import').click(function() {
            modal.modal('show');
        });
        modal.find('button#btn-startimport').click(function() {
            reload = true;
            modal.find('.modal-content').ploading({
                action: 'show',
                spinnerHTML: '<i></i>',
                spinnerClass: 'fa fa-spinner fa-spin p-loading-fontawesome'
            });
            var btn = $(this);
            var file = $('#file').prop('files')[0];
            var formData = new FormData();
            formData.append('file', file);
            formData.append('description', modal.find('#description').val());
            $.ajax({
                url: '{{ route("import.store") }}',
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                data: formData,
                type: 'POST',
                success: function(response) {
                    btn.prop('disabled', true);
                    modal.find('.modal-header button.close').prop('disabled', true);
                    showProgress(response.import.id);
                },
                error: function(response) {
                    swal('Import file', response.responseJSON.message, 'error');
                },
                complete: function() {
                    modal.find('.modal-content').ploading({action: 'hide'});
                }
             });
        });
    });
    var showProgress = function(id) {
        var modal = $('#modal-import');
        var progressPanel = modal.find('#progress-info');
        var progressBar = progressPanel.find('.progress .progress-bar');
        progressPanel.show();
        var interval = setInterval(function() {
            EXP.request.get('/admin/import/' + id, function(response) {
                progressBar.css('width', response.data.percent_completed + '%');
                var p = progressPanel.find('#progress-msg');
                var pClass = 'text-light-blue';
                if (response.data.status === 'invalid_excel') {
                    pClass = 'text-red';
                }
                else if (response.data.status === 'imported') {
                    pClass = 'text-green';
                }
                p.removeClass();
                p.addClass(pClass);
                p.text(response.data.details);
                if ((response.data.status !== 'loading_excel' && response.data.status !== 'initialized') || response.data.percent_completed == 100) {
                    modal.find('.modal-header button.close').prop('disabled', false);
                    clearInterval(interval);
                }
            });
        }, 1000);
    };
</script>
@endsection
