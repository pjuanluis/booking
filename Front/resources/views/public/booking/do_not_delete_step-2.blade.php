<h3 class="title-step">Confirm your Experiencia
    <span class="title-step__description">Step 2 of 3</span>
</h3>
<div class="block">
    <div class="block-header">
        <div class="content-fluid">
            This is the Experiencia that you are going to have with us:
        </div>
    </div>
    <div class="block-body">
        <ul class="list-group info">
            <li class="list-group-item">Spanish courses, Standard 15 clases per week</li>
        </ul>
    </div>
</div>
<div class="block">
    <div class="block-header">
        <div class="content-fluid">
            This is the time your Experiencia is going to be:
        </div>
    </div>
    <div class="block-body">
        <ul class="list-group info">
            <li class="list-group-item">
                2 weeks
                <span class="badge">
                    50 USD
                </span>
            </li>
        </ul>
    </div>
</div>
<div class="block">
    <div class="block-header">
        <div class="content-fluid">
            Here is where you are going to stay:
        </div>
    </div>
    <div class="block-body">
        <ul class="list-group info">
            <li class="list-group-item">
                School Residence, Private Room for 2 weeks
                <span class="badge">
                    120 USD
                </span>
            </li>
        </ul>
    </div>
</div>
<div class="block">
    <div class="block-header">
        <div class="content-fluid">
            We are going to pick you up at:
        </div>
    </div>
    <div class="block-body">
        <ul class="list-group info">
            <li class="list-group-item">
                Puerto Escondido Airport
                <span class="badge">
                    20 USD
                </span>
            </li>
        </ul>
    </div>
</div>
<div class="block">
    <div class="block-header">
        <div class="content-fluid">
            Your payment
        </div>
    </div>
    <div class="block-body">
        <ul class="list-group info">
            <li class="list-group-item">
                Total charge
                <span class="badge">
                    <strong>190 USD</strong>
                </span>
            </li>
        </ul>
    </div>
</div>