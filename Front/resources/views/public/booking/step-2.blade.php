<h3 class="title-step">Let's plan your EXPERIENCIA
    <span class="title-step__description">Step 2 of 3</span>
</h3>

<div class="block">
    <div class="block-header">
        <div class="content-fluid">
            <strong>This is the EXPERIENCIA that you want to have with us:</strong>
        </div>
    </div>
    <div class="block-body">
        <ul class="list-group info"></ul>
        <p class="total-to-pay text-right">
            <span style="font-size: 20px; border-bottom: 2px solid black; padding: 0 15px 5px">
                Total to pay: <strong class="amount"></strong>
            </span>
        </p>
    </div>

    <div class="block-header show-on-fligth-tickets-no">
        <div class="content-fluid">
            <strong>Choose your dates:</strong>
        </div>
    </div>
    <div class="block-body show-on-fligth-tickets-no">
        <strong>This are the avilable dates that match with your request. Please drag your mouse to choose. If you are not good with this dates, please try changing your residence or room options</strong>
        <div class="choose-your-dates" data-busy-dates="['2017-09-20', '2017-09-23', '2017-09-29']"></div>
    </div>
</div> {{-- ./block --}}
