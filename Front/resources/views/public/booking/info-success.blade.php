@extends('layouts.public-booking-base')

@section('content')

<div class="block">
    <div class="block-header">Client information successfully saved</div>
    <div class="block-body block-separation text-left">
    Your request has been sent<br>
    You will receive an email with your request details, and we will contact you within next 24 hours.
    </div>
</div>
@endsection