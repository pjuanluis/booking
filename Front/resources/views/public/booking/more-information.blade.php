@extends('layouts.public-booking-base')

@section('content')
    @if (Session::has('response'))
    <div class="alert alert-warning" role="alert">
        UPS!, {{ Session::get('response')['message'] }}
    </div>
    @endif
    @php
    Session::forget('response');
    @endphp
    <div class="container-fluid section" id="app">
        <div>
            <h1>Thank you for chossing us!</h1>
            <p>
                <strong>Dear {{ $command->client_first_name . ' ' . $command->client_last_name }},
                we know you want to
                {{
                    !empty($command->commandCourses->data) ? (' study ' . $command->commandCourses->data[0]->quantity .
                            ' of ' . $command->commandCourses->data[0]->package->data->name) .
                            ' with us' : ''
                }}
                {{
                    !empty($command->commandHousing->data) ? (', and stay in ' . $command->commandHousing->data[0]->housing->data->name .
                            ' from ' . $command->commandHousing->data[0]->from_date . ' to ' . $command->commandHousing->data[0]->to_date) : ''
                }}
                . If there's a wrong information above, please contact us.</strong>
            </p>
            <p>
                <strong>We now need more info to proceed with your booking and make sure everything
                is going to be perfect for your arrive and experience. Please fill the form below:</strong>
            </p>
        </div>

        <form id="form-client" action="{{ $formAction }}" method="POST">
            {{ csrf_field() }}
            {{-- Personal information --}}
            <input type="hidden" name="first_name" value="{{ $command->client_first_name }}" />
            <input type="hidden" name="last_name" value="{{ $command->client_last_name }}" />
            <input type="hidden" name="email" value="{{ $command->client_email }}" />
            <input type="hidden" name="token" value="{{ $command->token }}" />
            <div class="block">
                <div class="block-header">
                    <strong>Personal infromation</strong>
                </div>
                <div class="block-body">
                    <div class="row">
                        <div class="col-md-6">
                            {{--  Date of birth  --}}
                            <div class="field-inline field-inline--labels-align form-group {{ $errors->has('birth_date') ? 'has-error' : '' }}">
                                <label class="field-inline-label control-label">Date of birth</label>
                                <div class="field-inline-field">
                                    <div class="birth-date input-group input-calendar--blue">
                                        <ul class="list-inline">
                                            <li><select class="year"></select></li>
                                            <li><select class="month"></select></li>
                                            <li><select class="day"></select></li>
                                        </ul>
                                        <input type="hidden" class="datepicker-year input-calendar__input form-control"
                                                name="birth_date" id="birth-date" value="{{ old('birth_date') ? old('birth_date') : '' }}">
                                    </div>
                                </div>
                                <span class="help-block">{{ $errors->first('birth_date') }}</span>
                            </div>

                            {{--  Your phone  --}}
                            <div class="field-inline field-inline--labels-align form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
                                <label class="field-inline-label control-label">Your phone</label>
                                <div class="field-inline-field">
                                    <input type="text" class="form-control" name="phone" value="{{ old('phone') ? old('phone') : '' }}">
                                    <span class="help-block">{{ $errors->first('phone') }}</span>
                                </div>
                            </div>

                            {{--  Photo  --}}
                            <div class="croppie-image {{ $errors->has('file') ? 'has-error' : '' }}" style="min-height: 337.5px;">
                                <input id="file" type="file" class="croppie-image__file hide">
                                <input type="hidden" class="photo_data" name="photo_data" value="">
                                <input type="hidden" class="photo_name" name="photo_name" value="">
                                <div class="croppie-image__view-container">
                                    <span class="croppie-image__caption">Photo</span>
                                    <div class="croppie-image__view"></div>
                                </div>
                                <div class="croppie-image__actions">
                                    <button type="button" class="btn croppie-image__upload">Upload file</button>
                                    <button type="button" class="btn croppie-image__cancel">Cancel</button>
                                </div>
                                <span class="help-block">{{ $errors->first('file') }}</span>
                            </div>
                        </div>

                        <div class="col-md-6">
                            {{--  Recidence Country  --}}

                            <div class="field-inline field-inline--flex form-group {{ $errors->has('country_id') ? 'has-error' : '' }}">
                                <label class="field-inline-label control-label">Residence Country</label>
                                <div class="field-inline-field field-triangle" style="width: 70%;">
                                    <span class="field-triangle__triangle"></span>
                                    <select class="select2" name="country_id">
                                        @php
                                        $val = old('country_id') ? old('country_id') : '';
                                        @endphp
                                        @foreach ($countries as $country)
                                        <option value="{{ $country->id }}" {{ $val == $country->id ? 'selected' : '' }}>{{ $country->name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-block">{{ $errors->first('country_id') }}</span>
                                </div>
                            </div>

                            {{--  Recidence phone  --}}
                            <div class="field-inline field-inline--labels-align form-group {{ $errors->has('emergency_phone') ? 'has-error' : '' }}">
                                <label class="field-inline-label control-label">Emergency phone</label>
                                <div class="field-inline-field">
                                    <input type="text" class="form-control" name="emergency_phone" value="{{ old('emergency_phone') ? old('emergency_phone') : '' }}">
                                    <span class="help-block">{{ $errors->first('emergency_phone') }}</span>
                                </div>
                            </div>

                            <div class="form-group field-inline field-inline--labels-align {{ $errors->has('gender_id') ? 'has-error' : '' }}">
                                <label class="field-inline-label control-label">Gender</label><br />
                                <div class="field-inline-field">
                                    <div class="row">
                                        @php
                                        $val = old('gender_id', '');
                                        @endphp
                                        @foreach ($genders->data as $gender)
                                            <div class="col-sm-3 col-xs-6">
                                                <label>
                                                    <input type="radio" class="iCheck" value="{{ $gender->id }}" name="gender_id" {{ $val == $gender->id? 'checked' : '' }}> {{ $gender->name }}
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                    <span class="help-block">{{ $errors->first('gender_id') }}</span>
                                </div>
                            </div>

                            {{--  Allergies  --}}
                            <div class="field-inline field-inline--labels-align form-group {{ $errors->has('has_alergies') ? 'has-error' : '' }}">
                                <label class="field-inline-label control-label">Allergies</label>
                                <div class="field-inline-field">
                                    <div class="row">
                                        @php
                                        $val = old('has_alergies') ? old('has_alergies') : (old('has_alergies') === '0' ? '0' : '');
                                        @endphp
                                        <div class="col-sm-2 col-xs-6">
                                            <label>
                                                <input type="radio" class="opt-allergies" name="has_alergies" v-model="hasAllergies" value="1" {{ $val === '1' ? 'checked' : '' }}> Yes
                                            </label>
                                        </div>
                                        <div class="col-sm-2 col-xs-6">
                                            <label>
                                                <input type="radio" class="opt-allergies" name="has_alergies" v-model="hasAllergies" value="0" {{ $val === '0' ? 'checked' : '' }}> No
                                            </label>
                                        </div>
                                    </div>
                                    <span class="help-block">{{ $errors->first('has_alergies') }}</span>
                                </div>
                            </div>

                            {{--  Specify  --}}
                            <div id="allergies-details" class="field-inline field-inline--labels-align form-group {{ $errors->has('alergies') ? 'has-error' : '' }}" :class="{ hidden: hasAllergies === 0  }">
                                <label class="field-inline-label control-label">Specify</label>
                                <div class="field-inline-field">
                                    <input type="text" class="form-control" name="alergies" value="{{ old('alergies') ? old('alergies') : '' }}">
                                    <span class="help-block">{{ $errors->first('alergies') }}</span>
                                </div>
                            </div>

                            {{--  Diseases  --}}
                            <div class="field-inline field-inline--labels-align form-group {{ $errors->has('has_diseases') ? 'has-error' : '' }}">
                                <label class="field-inline-label control-label">Diseases</label>
                                <div class="field-inline-field">
                                    <div class="row">
                                        @php
                                        $val = old('has_diseases') ? old('has_diseases') : (old('has_diseases') === '0' ? '0' : '');
                                        @endphp
                                        <div class="col-sm-2 col-xs-6">
                                            <label>
                                                <input type="radio" class="opt-diseases" name="has_diseases" v-model="hasDiseases" value="1" {{ $val === '1' ? 'checked' : '' }}> Yes
                                            </label>
                                        </div>
                                        <div class="col-sm-2 col-xs-6">
                                            <label>
                                                <input type="radio" class="opt-diseases" name="has_diseases" v-model="hasDiseases" value="0" {{ $val === '0' ? 'checked' : '' }}> No
                                            </label>
                                        </div>
                                    </div>
                                    <span class="help-block">{{ $errors->first('has_diseases') }}</span>
                                </div>
                            </div>

                            {{--  Specify  --}}
                            <div id="diseases-details" class="field-inline field-inline--labels-align form-group {{ $errors->has('diseases') ? 'has-error' : '' }}" :class="{ hidden: hasDiseases === 0 }">
                                <label class="field-inline-label control-label">Specify</label>
                                <div class="field-inline-field">
                                    <input type="text" class="form-control" name="diseases" value="{{ old('diseases') ? old('diseases') : '' }}">
                                    <span class="help-block">{{ $errors->first('diseases') }}</span>
                                </div>
                            </div>

                            {{--  Do you smoke?  --}}
                            <div class="field-inline field-inline--labels-align form-group {{ $errors->has('is_smoker') ? 'has-error' : '' }}">
                                <label class="field-inline-label control-label">Do you smoke?</label>
                                <div class="field-inline-field">
                                    <div class="row">
                                        @php
                                        $val = old('is_smoker') ? old('is_smoker') : (old('is_smoker') === '0' ? '0' : '');
                                        @endphp
                                        <div class="col-sm-2 col-xs-6">
                                            <label>
                                                <input type="radio" name="is_smoker" value="1" {{ $val === '1' ? 'checked' : '' }}> Yes
                                            </label>
                                        </div>
                                        <div class="col-sm-2 col-xs-6">
                                            <label>
                                                <input type="radio" name="is_smoker" value="0" {{ $val === '0' ? 'checked' : '' }}> No
                                            </label>
                                        </div>
                                    </div>
                                    <span class="help-block">{{ $errors->first('is_smoker') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            {{-- Facebook URL --}}
                            <div class="field-inline form-group {{ $errors->has('fb_profile') ? 'has-error' : '' }}">
                                <label class="field-inline-label control-label">Facebook URL or profile</label>
                                <div class="field-inline-field">
                                    <input type="text" class="form-control" name="fb_profile" value="{{ old('fb_profile') ? old('fb_profile') : '' }}">
                                    <span class="help-block">{{ $errors->first('fb_profile') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{--  Travel information  --}}
            <div class="block">
                <div class="block-header">
                    <strong>Travel information (If you already have it)</strong>
                </div>
                <div class="block-body">
                    <div class="row hidden" id="msg-error-t-inf">
                        <div class="col-sm-12">
                            <div class="form-group has-error">
                                <span class="help-block">
                                    You will need to fill in all of you travel information if you have them at this time.
                                    If you don' t have complete travel information, please
                                    <a href="#" id="reset-travel-information">click here</a>
                                    to reset this section and continue.
                                </span>
                            </div>
                        </div>
                    </div>
                    <div id="travel-information">
                        <div class="row">
                            <div class="col-sm-6">
                                {{-- My trip would be by  --}}
                                <div class="field-inline form-group mrg-0 {{ $errors->has('travel_by') ? 'has-error' : '' }}">
                                    <label class="field-inline-label control-label">My trip would be by </label>
                                    <div class="field-inline-field">
                                        <div class="row">
                                            @php
                                            $val = old('travel_by') ? old('travel_by') : '';
                                            @endphp
                                            <div class="col-sm-4 col-xs-6">
                                                <label>
                                                    <input type="radio" name="travel_by" id="travel_by_plane"
                                                    value="plane" v-model="travelBy" {{ $val == 'plane' ? 'checked' : '' }}> Plane
                                                </label>
                                            </div>
                                            <div class="col-sm-4 col-xs-6">
                                                <label>
                                                    <input type="radio" name="travel_by" id="travel_by_bus"
                                                    value="bus" v-model="travelBy" {{ $val == 'bus' ? 'checked' : '' }}> Bus
                                                </label>
                                            </div>
                                        </div>
                                        <span class="help-block">{{ $errors->first('travel_by') }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="divider--tiny divider--margin-tb"></div>

                        <strong>Arrival information</strong>
                        @php
                            $airports = ['Puerto Escondido', 'Huatulco'];
                        @endphp
                        <div class="row">
                            <div class="col-md-6">
                                {{-- From  --}}
                                <div class="field-inline field-inline--flex form-group {{ $errors->has('arrival_place') ? 'has-error' : '' }}">
                                    <label class="field-inline-label control-label">From CDMX to </label>
                                    <div class="field-inline-field field-triangle">
                                        <span class="field-triangle__triangle"></span>
                                        <select id="select-arrival-place" name="arrival_place">
                                            <option value="">Select an option</option>
                                            @php
                                                $val = old('arrival_place') ? old('arrival_place') : '';
                                            @endphp
                                            @foreach ($airports as $airport)
                                                <option value="{{ $airport }}" {{ $airport == $val ? 'selected' : '' }}>
                                                    {{ $airport }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <span class="help-block">{{ $errors->first('arrival_place') }}</span>
                                    </div>
                                </div>

                                {{-- Airline  --}}
                                <div class="field-inline form-group {{ $errors->has('arrival_carrier') ? 'has-error' : '' }}">
                                    <label class="field-inline-label control-label">@{{ txtTravel.line }}</label>
                                    <div class="field-inline-field">
                                        <input type="text" class="form-control" name="arrival_carrier" value="{{ old('arrival_carrier') ? old('arrival_carrier') : '' }}">
                                        <span class="help-block">{{ $errors->first('arrival_carrier') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        {{-- My arrival day  --}}
                                        <div class="field-inline form-group {{ $errors->has('arrival_date') ? 'has-error' : '' }}">
                                            <label class="field-inline-label control-label">My arrival day </label>
                                            <div class="field-inline-field">
                                                <div class="input-group input-calendar--blue">
                                                    <input type="text"  class="datepicker input-calendar__input form-control"
                                                            accept=""id="arrival-day_arrival" name="arrival_date"
                                                            value="{{ old('arrival_date') ? old('arrival_date') : '' }}" autocomplete="off">
                                                    <label class="input-group-addon input-calendar__icon" for="arrival-day_arrival">
                                                        <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <span class="help-block">{{ $errors->first('arrival_date') }}</span>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        {{-- Arrival hour  --}}
                                        <div class="field-inline form-group {{ $errors->has('arrival_time') ? 'has-error' : '' }}">
                                            <label class="field-inline-label control-label">Arrival hour </label>
                                            <div class="field-inline-field">
                                                <input type="text" class="form-control timepicker" name="arrival_time"
                                                        value="{{ old('arrival_time') ? old('arrival_time') : '' }}" autocomplete="off">
                                            </div>
                                            <span class="help-block">{{ $errors->first('arrival_time') }}</span>
                                        </div>
                                    </div>
                                </div>

                                {{-- Flight number  --}}
                                <div class="field-inline form-group {{ $errors->has('arrival_travel_number') ? 'has-error' : '' }}">
                                    <label class="field-inline-label control-label">@{{ txtTravel.number  }}</label>
                                    <div class="field-inline-field">
                                        <input type="text" class="form-control" name="arrival_travel_number"
                                                value="{{ old('arrival_travel_number') ? old('arrival_travel_number') : '' }}">
                                        <span class="help-block">{{ $errors->first('arrival_travel_number') }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <strong>Departure information</strong>
                        <div class="row">
                            <div class="col-md-6">
                                {{-- From  --}}
                                <div class="field-inline field-inline--flex form-group {{ $errors->has('departure_place') ? 'has-error' : '' }}">
                                    <label class="field-inline-labe control-labell">From&nbsp;&nbsp;&nbsp;</label>
                                    <div class="field-inline-field field-triangle">
                                        <span class="field-triangle__triangle"></span>
                                        <select name="departure_place">
                                            <option value="">Select an option</option>
                                            @php
                                                $val = old('departure_place') ? old('departure_place') : '';
                                            @endphp
                                            @foreach ($airports as $airport)
                                                <option value="{{ $airport }}" {{ $airport == $val ? 'selected' : '' }}>
                                                    {{ $airport }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <span class="help-block">{{ $errors->first('departure_place') }}</span>
                                    </div>
                                    <label class="field-inline-label control-label">to CDMX</label>
                                </div>

                                {{-- Airline  --}}
                                <div class="field-inline form-group {{ $errors->has('departure_carrier') ? 'has-error' : '' }}">
                                    <label class="field-inline-label control-label">@{{ txtTravel.line }}</label>
                                    <div class="field-inline-field">
                                        <input type="text" class="form-control" name="departure_carrier"
                                                value="{{ old('departure_carrier') ? old('departure_carrier') : '' }}">
                                        <span class="help-block">{{ $errors->first('departure_carrier') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        {{-- My arrival day  --}}
                                        <div class="field-inline form-group {{ $errors->has('departure_date') ? 'has-error' : '' }}">
                                            <label class="field-inline-label control-label">My departure day </label>
                                            <div class="field-inline-field">
                                                <div class="input-group input-calendar--blue">
                                                    <input type="text" class="datepicker input-calendar__input form-control"
                                                            id="departure-day_departure" name="departure_date"
                                                            value="{{ old('departure_date') ? old('departure_date') : '' }}" autocomplete="off">
                                                    <label class="input-group-addon input-calendar__icon" for="departure-day_departure">
                                                        <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <span class="help-block">{{ $errors->first('departure_date') }}</span>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        {{-- Arrival hour  --}}
                                        <div class="field-inline form-group {{ $errors->has('departure_time') ? 'has-error' : '' }}">
                                            <label class="field-inline-label control-label">Departure hour </label>
                                            <div class="field-inline-field">
                                                <input type="text" class="form-control timepicker" name="departure_time"
                                                        value="{{ old('departure_time') ? old('departure_time') : '' }}" autocomplete="off">
                                            </div>
                                            <span class="help-block">{{ $errors->first('departure_time') }}</span>
                                        </div>
                                    </div>
                                </div>

                                {{-- Flight number  --}}
                                <div class="field-inline form-group {{ $errors->has('departure_travel_number') ? 'has-error' : '' }}">
                                    <label class="field-inline-label control-label">@{{ txtTravel.number }}</label>
                                    <div class="field-inline-field">
                                        <input type="text" class="form-control" name="departure_travel_number"
                                                value="{{ old('departure_travel_number') ? old('departure_travel_number') : '' }}">
                                        <span class="help-block">{{ $errors->first('departure_travel_number') }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{--  Academic information  --}}
            <div class="block">
                <div class="block-header">
                    <strong>Academic information</strong>
                </div>
                <div class="block-body">
                    {{-- Spanish --}}
                    <strong>Spanish</strong>
                    <div class="row">
                        <div class="field-inline form-group {{ $errors->has('spanish_level') ? 'has-error' : '' }}">
                            <div class="col-md-4">
                                <label class="field-inline-label control-label">did you study Spanish before?</label>
                            </div>
                            <div class="field-inline-field col-md-8">
                                <div class="row">
                                    @php
                                    $val = old('spanish_level') ? old('spanish_level') : '';
                                    @endphp
                                    <div class="col-xs-6 col-sm-3">
                                        <label>
                                            <input type="radio" class="opt-spanish-level" name="spanish_level" v-model="spanishLevel" value="none" {{ $val == 'none' ? 'checked' : '' }}> No
                                        </label>
                                    </div>
                                    <div class="col-xs-6 col-sm-3">
                                        <label>
                                            <input type="radio" class="opt-spanish-level" name="spanish_level" v-model="spanishLevel" value="begginer" {{ $val == 'begginer' ? 'checked' : '' }}> Beginer
                                        </label>
                                    </div>
                                    <div class="col-xs-6 col-sm-3">
                                        <label>
                                            <input type="radio" class="opt-spanish-level" name="spanish_level" v-model="spanishLevel" value="middle" {{ $val == 'middle' ? 'checked' : '' }}> Middle
                                        </label>
                                    </div>
                                    <div class="col-xs-6 col-sm-3">
                                        <label>
                                            <input type="radio" class="opt-spanish-level" name="spanish_level" v-model="spanishLevel" value="advanced" {{ $val == 'advanced' ? 'checked' : '' }}> Advanced
                                        </label>
                                    </div>
                                </div>
                                <span class="help-block">{{ $errors->first('spanish_level') }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div id="spanish-duration" class="col-md-5" :class="{ invisible: spanishLevel === 'none' }">
                            {{-- How long --}}
                            <div class="field-inline form-group {{ $errors->has('spanish_knowledge_duration') ? 'has-error' : '' }}">
                                <label class="field-inline-label control-label">How long?</label>
                                <div class="field-inline-field">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="field-triangle form-group">
                                                <span class="field-triangle__triangle"></span>
                                                <select name="spanish_knowlege_number">
                                                    @php
                                                    $val = old('spanish_knowlege_number') ? old('spanish_knowlege_number') : '';
                                                    @endphp
                                                    @for ($i = 0; $i < 12; $i++)
                                                    <option value="{{ $i }}" {{ $val == $i ? 'selected' : '' }}>{{ $i }}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="field-triangle form-group">
                                                <span class="field-triangle__triangle"></span>
                                                <select name="spanish_knowlege_type">
                                                    @php
                                                    $val = old('spanish_knowlege_type') ? old('spanish_knowlege_type') : '';
                                                    @endphp
                                                    <option value="month" {{ $val == 'month' ? 'selected' : '' }}>Months</option>
                                                    <option value="year" {{ $val == 'year' ? 'selected' : '' }}>Years</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <span class="help-block">{{ $errors->first('spanish_knowledge_duration') }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-md-offset-1">
                            {{-- Apply quiz --}}
                            <div class="field-inline form-group">
                                <div class="row">
                                    @if (false)
                                        <label class="field-inline-label col-md-6">Please apply the following test</label>
                                        <div class="field-inline-field col-md-6">
                                            <button type="button" class="btn full-width">GO TO QUIZ</button>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="divider--tiny divider--margin-tb"></div>

                    {{--  Surf experience  --}}
                    <strong>Surf</strong>
                    <div class="field-inline form-group {{ $errors->has('has_surf_experience') ? 'has-error' : '' }}">
                        <div class="row">
                            <div class="col-md-4">
                                <label class="field-inline-label control-label">Do you have previous Surf experience?</label>
                            </div>
                            <div class="field-inline-field col-md-8">
                                <div class="row">
                                    @php
                                    $val = old('has_surf_experience') ? old('has_surf_experience') : (old('has_surf_experience') === '0' ? '0' : '');
                                    @endphp
                                    <div class="col-md-3 col-xs-6">
                                        <label>
                                            <input type="radio" class="opt-surf-experience" name="has_surf_experience" v-model="hasSurfExperience" value="0" {{ $val === '0' ? 'checked' : '' }}> No
                                        </label>
                                    </div>
                                    <div class="col-md-3 col-xs-6">
                                        <label>
                                            <input type="radio" class="opt-surf-experience" name="has_surf_experience" v-model="hasSurfExperience" value="1" {{ $val === '1' ? 'checked' : '' }}> Yes
                                        </label>
                                    </div>
                                </div>
                                <span class="help-block">{{ $errors->first('has_surf_experience') }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div id="surf-duration" class="col-md-5" :class="{ hidden: hasSurfExperience === 0 }">
                            {{-- How long --}}
                            <div class="field-inline form-group {{ $errors->has('surf_experience_duration') ? 'has-error' : '' }}">
                                <label class="field-inline-label">How long?</label>
                                <div class="field-inline-field">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="field-triangle form-group">
                                                <span class="field-triangle__triangle"></span>
                                                <select name="surf_experience_number">
                                                    @php
                                                    $val = old('surf_experience_number') ? old('surf_experience_number') : '';
                                                    @endphp
                                                    @for ($i = 0; $i < 12; $i++)
                                                    <option value="{{ $i }}" {{ $val == $i ? 'selected' : '' }}>{{ $i }}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="field-triangle form-group">
                                                <span class="field-triangle__triangle"></span>
                                                <select name="surf_experience_type">
                                                    @php
                                                    $val = old('surf_experience_type') ? old('surf_experience_type') : '';
                                                    @endphp
                                                    <option value="month" {{ $val == 'month' ? 'selected' : '' }}>Months</option>
                                                    <option value="year" {{ $val == 'year' ? 'selected' : '' }}>Years</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <span class="help-block">{{ $errors->first('surf_experience_duration') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="divider--tiny divider--margin-tb"></div>

                    <strong>Volunteer</strong>
                    <div class="row">
                        {{--  Volunteer experience  --}}
                        <div class="field-inline form-group {{ $errors->has('has_volunteer_experience') ? 'has-error' : '' }}">
                            <div class="col-md-4">
                                <label class="field-inline-label control-label">Do you have previous volunteer experience?</label>
                            </div>
                            <div class="field-inline-field col-md-8">
                                <div class="row">
                                    @php
                                    $val = old('has_volunteer_experience') ? old('has_volunteer_experience') : (old('has_volunteer_experience') === '0' ? '0' : '');
                                    @endphp
                                    <div class="col-md-3 col-xs-6">
                                        <label>
                                            <input type="radio" class="opt-volunteer-experience" name="has_volunteer_experience" v-model="hasVolunteerExperience" value="0" {{ $val === '0' ? 'checked' : '' }}> No
                                        </label>
                                    </div>
                                    <div class="col-md-3 col-xs-6">
                                        <label>
                                            <input type="radio" class="opt-volunteer-experience" name="has_volunteer_experience" v-model="hasVolunteerExperience" value="1" {{ $val === '1' ? 'checked' : '' }}> Yes
                                        </label>
                                    </div>
                                </div>
                                <span class="help-block">{{ $errors->first('has_volunteer_experience') }}</span>
                            </div>
                        </div>
                    </div>
                    <div id="volunteer-duration"class="row" :class="{ hidden: hasVolunteerExperience === 0 }">
                        <div class="col-md-5">
                            {{-- How long --}}
                            <div class="field-inline form-group {{ $errors->has('volunteer_experience_duration') ? 'has-error' : '' }}">
                                <label class="field-inline-label control-label">How long?</label>
                                <div class="field-inline-field">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="field-triangle form-group">
                                                <span class="field-triangle__triangle"></span>
                                                <select name="volunteer_experience_number">
                                                    @php
                                                    $val = old('volunteer_experience_number') ? old('volunteer_experience_number') : '';
                                                    @endphp
                                                    @for ($i = 0; $i < 12; $i++)
                                                    <option value="{{ $i }}" {{ $val == $i ? 'selected' : '' }}>{{ $i }}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="field-triangle form-group">
                                                <span class="field-triangle__triangle"></span>
                                                <select name="volunteer_experience_type">
                                                    @php
                                                    $val = old('volunteer_experience_type') ? old('volunteer_experience_type') : '';
                                                    @endphp
                                                    <option value="month" {{ $val == 'month' ? 'selected' : '' }}>Months</option>
                                                    <option value="year" {{ $val == 'year' ? 'selected' : '' }}>Years</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <span class="help-block">{{ $errors->first('volunteer_experience_duration') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-offset-1">
                            {{--  Specify  --}}
                            <div class="field-inline form-group {{ $errors->has('volunteer_experience') ? 'has-error' : '' }}">
                                <label class="field-inline-label control-label">Please specify</label>
                                <div class="field-inline-field">
                                    <textarea class="form-control" name="volunteer_experience" rows="4">{{ old('volunteer_experience') ? old('volunteer_experience') : '' }}</textarea>
                                    <span class="help-block">{{ $errors->first('volunteer_experience') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="divider--tiny divider--margin-tb"></div>
                </div>
            </div>

            {{--  Important information  --}}
            <div class="block">
                <div class="block-header">
                    <strong>Important information</strong>
                </div>
                <div class="block-body">
                    <div class="row">
                        {{-- Cancellation policies  --}}
                        <label class="col-sm-6 col-md-3">Please read our cancellation policies </label>
                        <div class="col-sm-6 col-md-3 form-group">
                            <a href="{{ route('cancellation-policies.index') }}" target="_blank" class="btn full-width" style="background-color: rgb(221, 221, 221); color: rgb(51, 51, 51);">CANCELLATION POLICIES</a>
                        </div>
                    </div>
                    @if (false)
                        <div class="row">
                            {{--  Class Schedule --}}
                            <label class="col-sm-6 col-md-3">Class Schedule </label>
                            <div class="col-sm-6 col-md-3 form-group">
                                <button type="button" class="btn full-width">CLASS SCHEDULE</button>
                            </div>
                        </div>
                    @endif
                </div>
            </div>

            {{--  Form buttons  --}}
            <div class="block">
                <div class="block-header">&nbsp;</div>
                <div class="block-body block-separation text-right">
                    <button type="reset" class="btn bg-secondary-color text-white text-white-in-hover">CLEAR</button>
                    <button type="submit" class="btn">SAVE</button>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    @parent
    <script>
        function clearTravelInformation($travelInformation) {
            $travelInformation.find('#travel_by_bus, #travel_by_plane')
                .prop('checked', false)
                .iCheck('update');

            $travelInformation.find('select')
                .val('')
                .trigger('change.select2');

            $travelInformation.find('input, textarea')
                .val('');
        }

        function addHelperTextTravelInformation() {
            var $msgErrorTInfo = $('#msg-error-t-inf');
            var $travelInformation = $('#travel-information');
            var $resetTravelInformation = $('#reset-travel-information');

            if (
                $msgErrorTInfo.length > 0 &&
                $travelInformation.find('.has-error').length > 0
            ) {
                $msgErrorTInfo.removeClass('hidden');
            }

            $resetTravelInformation.click(function(e) {
                e.preventDefault();

                clearTravelInformation($travelInformation);
            });
        }

        new Vue({
            el: '#app',
            data: {
                hasAllergies: ($('input[name="has_alergies"]:checked').val()),
                hasDiseases: ($('input[name="has_diseases"]:checked').val()),
                spanishLevel: $('input[name="spanish_level"]:checked').val(),
                hasSurfExperience: ($('input[name="has_surf_experience"]:checked').val()),
                hasVolunteerExperience: ($('input[name="has_volunteer_experience"]:checked').val()),
                travelBy: '',
                txtTravel: {
                    number: '',
                    line: ''
                }
            },
            watch: {
                travelBy: function() {
                    if (this.travelBy == 'plane') {
                        this.txtTravel.number = 'Flight number';
                        this.txtTravel.line = 'Airline';
                    } else if(this.travelBy == 'bus') {
                        this.txtTravel.number = 'Bus number';
                        this.txtTravel.line = 'Busline';
                    }
                }
            },
            mounted: function() {
                var vm = this;

                vm.travelBy = "{{ old('travel_by') }}";

                $('#form-client').on('reset', function(e) {
                    var $form = $(e.target);
                    setTimeout((function() {
                        this.find("select").trigger("change");
                        this.iCheck('update');
                    }).bind($form), 0);
                });
                $('.opt-allergies').on('ifChecked', function(event) {
                    vm.hasAllergies = parseInt(this.value);
                });
                $('.opt-diseases').on('ifChecked', function(event) {
                    vm.hasDiseases = parseInt(this.value);
                });
                $('.opt-spanish-level').on('ifChecked', function(event) {
                    vm.spanishLevel = this.value;
                });
                $('.opt-surf-experience').on('ifChecked', function(event) {
                    vm.hasSurfExperience = parseInt(this.value);
                });
                $('.opt-volunteer-experience').on('ifChecked', function(event) {
                    vm.hasVolunteerExperience = parseInt(this.value);
                });
                $('input[name="travel_by"]').on('ifChecked', function(event) {
                    vm.travelBy = this.value;
                });
                $('#form-client').submit(function() {
                    var $form = $(this);
                    $('body').ploading({action: 'show'});
                    $('html').scrollTop($('.p-loading-spinner').offset().top - (window.innerHeight / 2) + 22);
                    // Prevents double clic
                    $form.find("button").prop("disabled", true);
                    $form.get(0).submit();
                });
            }
        });
        $(function() {
            addHelperTextTravelInformation();

            setTimeout(function() {
                hiddenPreviewImage($('img.cr-image'));
            }, 500);

            $('img.cr-image').on('change', function() {
                hiddenPreviewImage($('img.cr-image'));
            });

            function hiddenPreviewImage(img) {
                if (img.attr('src') === '' || img.attr('src') === undefined) {
                    img.hide();
                }
            }
        });
    </script>
@endsection
