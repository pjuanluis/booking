@extends('layouts.public-booking-base')

@section('content')
@php
$display = 'none';
if (Session::has('paypal_error')) {
    $display = 'block';
}
@endphp
<div class="alert alert-warning" role="alert" style="display: {{$display}};">
    {{ Session::get('paypal_error') }}
    @php
    Session::forget('paypal_error');
    @endphp
</div>

<div class="title-step separation__up">
    <input id="accept-terms" type="checkbox">
    <label for="accept-terms" class="inline">
        I have already read and agree with the <a href=" http://www.experienciamexico.mx/cancellation-policy/" target="_blank">terms and conditions</a>
    </label>
</div>

{{-- Paypal --}}
<div class="block">
    <div class="block-header">Choose your payment gateway</div>
    <div class="block-body block-separation text-left">
        <div class="container-fluid">
            <ul class="list-group--light info">
                <li class="list-group-item list-group-item--point">
                    Pay with PayPal
                    <form id="paypal-form" action="{{ route('paypal.payment') }}" method="POST" class="pull-right">
                        {{ csrf_field() }}
                        <div class="panel panel-default">
                            <div class="panel-body text-center">
                                <img src="{{ asset('img/public/logo_paypal.jpg') }}" alt="PayPal Acceptance Mark" class="img-responsive">
                                <input type="hidden" name="bk_token" value="{{ $command->token }}" />
                                <button id="btn-paypal" type="submit" class="btn bg-primary btn-sm">PAY WITH PAYPAL</button>
                            </div>
                        </div>
                    </form>
                </li>
            </ul>
        </div>
    </div>
</div> {{-- ./block paypal --}}

{{-- Conekta --}}
@if (false)
    <div class="block">
        <div class="divider--light"></div>
        <div class="block-body block-separation">
            <ul class="list-group--light info">
                <li class="list-group-item list-group-item--point">
                    Pay with Conekta
                </li>
            </ul>

            <form id="conekta-form" action="{{ $formAction }}" method="POST">
                {{ csrf_field() }}
                <span class="card-errors"></span>
                <div class="form-group">
                    <label for="">Name and last name (as it appears on your credit card)</label>
                    <input type="text" class="form-control" data-conekta="card[name]" required>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <label for="full-name">Credit card number</label>
                        <input type="text" id="full-name" class="form-control" data-conekta="card[number]" required>
                    </div>
                    <div class="col-sm-2" class="fomr-control">
                        <label for="security">security code</label>
                        <input type="text" id="security" class="form-control" data-conekta="card[cvc]" required>
                    </div>

                    {{-- Months --}}
                    <div class="col-sm-3" class="form-control">
                        <label for="due-date">Due date</label>
                        <select type="text" id="due-date" class="full-width" data-conekta="card[exp_month]" required>
                            <option value="">Month</option>
                            @for ($i = 1; $i <= 12; $i++)
                                <option value="{{ str_pad($i, 2, '0', STR_PAD_LEFT) }}">{{ date("F", mktime(0, 0, 0, $i)) }}</option>
                            @endfor
                        </select>
                    </div>

                    {{-- Years --}}
                    @php
                    $currentYear = (int) \Carbon\Carbon::now()->toDateTimeString('Y');
                    @endphp
                    <div class="col-sm-3" class="form-control">
                        <label for="due-year">&nbsp;</label>
                        <select type="text" id="due-year" data-conekta="card[exp_year]" required>
                            <option value="">Year</option>
                            @for ($i = 0; $i < 10; $i++)
                                <option value="{{ $currentYear + $i }}">{{ $currentYear + $i }}</option>
                            @endfor
                        </select>
                    </div>
                </div>
                <input type="hidden" name="bk_token" value="{{ $command->token }}" />
                <div class="block-separation text-right">
                    <button id='btn-conekta' type="submit" class="btn btn-sm">PAY WITH CONEKTA</button>
                </div>
            </form>
        </div>
    </div> {{-- ./block conekta --}}
@endif
<script type="text/javascript">
    window.addEventListener('load', function() {
        // Se obtiene desde el admin de conekta en la sección "API Keys"
        Conekta.setPublishableKey("{{ config('services.conekta.public_key') }}");
        var conektaSuccessResponseHandler = function (token) {
            var $form = $("#conekta-form");
            //Add the token_id in the form
            $form.append($('<input type="hidden" name="card_token" id="conektaTokenId">').val(token.id));
            $form.get(0).submit(); //Submit
        };
        var conektaErrorResponseHandler = function (response) {
            var $form = $("#card-form");
            $form.find(".card-errors").text(response.message_to_purchaser);
            $form.find("button").prop("disabled", false);
            $('body').ploading({action: 'hide'});
        };
        $("#conekta-form").submit(function (event) {
            var $form = $(this);
            if ($('#accept-terms')[0].checked) {
                $('body').ploading({action: 'show'});
                // Prevents double clic
                $form.find("button").prop("disabled", true);
                $("button#btn-paypal").prop("disabled", true);
                Conekta.token.create($form, conektaSuccessResponseHandler, conektaErrorResponseHandler);
            }
            else {
                $('.alert').show().html("UPS!, you must accept the terms and conditions.");
                $('html').scrollTop($('.alert').offset().top);
            }
            return false;
        });
        $("#paypal-form").submit(function (event) {
            var $form = $(this);
            if ($('#accept-terms')[0].checked) {
                // Prevents double clic
                $form.find("button").prop("disabled", true);
                $("button#btn-conekta").prop("disabled", true);
                $form.get(0).submit();
            }
            else {
                $('.alert').show().html("UPS!, you must accept the terms and conditions.");
                $('html').scrollTop($('.alert').offset().top);
            }
            return false;
        });
    });
</script>
<script type="text/javascript" src="https://conektaapi.s3.amazonaws.com/v0.3.2/js/conekta.js"></script>
@endsection
