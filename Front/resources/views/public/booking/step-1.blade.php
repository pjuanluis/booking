@php
    $today = \Carbon\Carbon::today();
@endphp

<div id="app-step1">
    <h3 class="title-step">Let's plan your EXPERIENCIA
        <span class="title-step__description">Step 1 of 3</span>
    </h3>

    {{--  When it's going to happen?  --}}
    <div class="block">
        <div class="block-header">
            <strong>When are you planning your trip?</strong>
        </div>
        <div class="block-body">
            <div class="block-ask">
                @if (false)
                    <div class="block-ask__title">
                        <label class="btn-collapse">
                            <input type="radio" name="flight-tickets-already" v-model="flightTicketsAlready" value="1"
                            accept=""data-target="#i-have-tickets" class="flight-tickets-already">
                            I have already my flight tickets to Puerto Escondido
                        </label>
                    </div>
                @endif
                <div id="i-have-tickets" class="collapse block-ask__content" :class="{ in: flightTicketsAlready }">
                    <div class="content group-content">
                        {{--   My arrival day --}}
                        <div class="camp-inline group-content__element group-content__element--xs-full camp-inline--half">
                            <label class="camp-inline__left camp-inline--half">My arrival day</label>
                            <div class="input-group input-calendar--blue">
                                <input type="text" class="datepicker input-calendar__input form-control" id="arrival-day">
                                <label class="input-group-addon input-calendar__icon" for="arrival-day">
                                    <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
                                </label>
                            </div>
                        </div>

                        {{--  My departure day  --}}
                        <div class="camp-inline group-content__element group-content__element--right group-content__element--xs-full camp-inline--half">
                            <label class="camp-inline__left camp-inline--half">My departure day</label>
                            <span class="camp-inline__container">
                                <div class="input-group input-calendar--blue">
                                    <input type="text" class="datepicker input-calendar__input form-control" id="departure-day" disabled>
                                    <label class="input-group-addon input-calendar__icon" for="departure-day">
                                        <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
                                    </label>
                                </div>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            @if (false)
                <div class="block-ask">
                    <div class="block-ask__title">
                        <label class="btn-collapse">
                            <input type="radio" id="flight-tickets-no" name="flight-tickets-already" v-model="flightTicketsAlready" value="0"
                            accept=""data-target="#i-dont-have-tickets" class="flight-tickets-already">
                            I don't have my tickets flight yet
                        </label>
                    </div>
                    <div id="i-dont-have-tickets" class="collapse block-ask__content" :class="{ in: flightTicketsAlready == '0' }">
                        <div class="content group-content">
                            <label>I'm planning to be there in</label>
                            <div class="group-content__element group-content__element--xs-block">
                                <select name="" id="ticket-flight-month">
                                    @for ($monthNumber = (int) $today->format('m'); $monthNumber <= 12; $monthNumber++)
                                        <option value="{{ $monthNumber }}" data-info="Info A">{{ date("F", mktime(0, 0, 0, $monthNumber, 1)) }}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="group-content__element group-content__element--xs-block">
                                <select id="ticket-flight-year" class="year-onchange" data-target="#ticket-flight-month" data-current-year="{{ $today->format('Y')  }}" data-current-month="{{ $today->format('m')  }}"  name="" id="">
                                    @for ($year = (int) $today->format('Y'); $year < (int) $today->format('Y') + 2; $year++)
                                        <option value="{{ $year }}" data-info="Info A">{{ $year }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>

    {{--  Which EXPERIENCIA  do you want to have with us?  --}}
    <div class="block">
        <div class="block-header">
            <strong>Which EXPERIENCIA do you want to have with us?</strong>
        </div>
        <div class="block-body">
            @foreach ($packages as $r)
                {{-- TODO: quitar estas condicines cuando lo pida el cliente --}}
                {{-- No mostrar las experiencias de yoga y valunteer --}}
                @if (count($r->experience) === 1 && ($r->experience[0] === 'yoga' || $r->experience[0] === 'volunteer'))
                    @continue
                @endif
                {{-- No mostrar la experiencia surf & volunteer --}}
                @if (count($r->experience) === 2 && ($r->experience[0] === 'surf' && $r->experience[1] === 'volunteer'))
                    @continue
                @endif
            <div class="block-ask">
                <div class="block-ask__title">
                    <label class="btn-collapse">
                        <input type="checkbox" name="want-to-exp" data-course-id="{{ implode('-', $r->experience) }}"
                            data-target="#course-{{ implode('-', $r->experience) }}" class="choose-course"> {{ implode(' & ', $r->experience) }}
                    </label>
                </div>
                <div id="course-{{ implode('-', $r->experience) }}" class="collapse block-ask__content">
                    <div class="content group-content">
                        <a tabindex="0" role="button" data-toggle="popover" id="shower__course-{{ implode('-', $r->experience) }}" data-content=""
                            class="popover-information btn btn-info group-content__element--right">info</a>
                        <div class="group-content__element group-content__element--xs-block">
                            <select name="package_id" id="select-course-package-{{ implode('-', $r->experience) }}"
                            class="select-course-package shower__action"
                            data-target="#shower__course-{{ implode('-', $r->experience) }}"
                            data-course-id="{{ implode('-', $r->experience) }}">
                                @foreach ($r->packages as $p)
                                    {{-- No mostrar las opciones que inicien con PKW --}}
                                    {{-- TODO: quitar esta condición cuando lo pida el cliente --}}
                                    @if (strpos($p->name, 'PKW') > 0 || strpos($p->name, 'PKW') === false)
                                        <option value="{{$p->id}}" data-info="{{$p->description}}">{{$p->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        <span class="group-content__element--xs-block text-right ">
                            <span class="group-content__element text-right">
                                <label>How many?</label>
                            </span>
                            <div class="group-content__element mrg-left" style="width: 6.2em">
                                <input type="number" id="course-quantity-{{implode('-', $r->experience)}}" name="quantity"
                                    data-course-id="{{implode('-', $r->experience)}}" class="text-left course-quantity" style="width: 100%;">
                            </div>
                        </span>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>

    {{--  Where do you want to stay  --}}
    <div class="block">
        <div class="block-header">
            <strong>Where do you want to stay?</strong>
        </div>
        <div class="block-help" style="display: none; padding: 10px 0 10px 38px; background-color: #a94442; color: #f2dede;"></div>
        <div class="block-body">
            @foreach ($housing as $r)
                <div class="block-ask">
                    <div class="block-ask__title">
                        <label class="btn-collapse">
                            <input type="radio" id="choose-housing-{{$r->id}}" name="want-to-stay" v-model="wantToStay" data-housing-id="{{$r->id}}" data-target="#stay-{{ str_slug($r->name) }}" class="want-to-stay" value="{{$r->id}}"> {{ $r->name }}
                        </label>
                    </div>
                    <div id="stay-{{ str_slug($r->name) }}" class="collapse block-ask__content" :class="{ in: wantToStay == {{ $r->id }} }">
                        <div class="content group-content">
                            <div class="content group-content">
                                <a tabindex="0" role="button" data-toggle="popover" id="shower__housing-{{$r->id}}" data-content=""
                                    class="popover-information btn btn-info group-content__element--right visible-xs-block">info</a>
                                <div class="group-content__element group-content__element--xs-block">
                                    <select name="housing_room_id" id="select-housing-room-{{$r->id}}" data-housing-id="{{$r->id}}"
                                    class="select-housing-room shower__action"
                                    data-target="#shower__housing-{{$r->id}}">
                                        @foreach ($r->housingRooms->data as $hr)
                                            <option value="{{ $hr->id }}" data-cost_type="{{ $hr->cost_type }}" data-beds="{{ $hr->beds }}" data-info="{{ $hr->description }}">{{ $hr->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <a tabindex="0" role="button" data-toggle="popover" id="shower__housing-package-{{$r->id}}" data-content=""
                                    class="hide-xs popover-information btn btn-info group-content__element--right">info</a>
                                <div class="group-content__element group-content__element--xs-block">
                                    <select name="housing_room_package_id" id="select-housing-package-{{$r->id}}" data-housing-id="{{$r->id}}"
                                    class="select-housing-package shower__action"
                                    data-target="#shower__housing-package-{{$r->id}}">
                                        @php
                                            $packagesTmp = [];
                                            if (isset($r->housingRooms->data[0]->packages->data)) {
                                                $packagesTmp = $r->housingRooms->data[0]->packages->data;
                                            }
                                        @endphp
                                        @foreach ($packagesTmp as $p)
                                            {{-- No mostrar las opciones que tengan la palabra PKW --}}
                                            {{-- TODO: quitar esta condición cuando lo pida el cliente --}}
                                            @if (strpos($p->name, 'PKW') === false)
                                                <option value="{{ $p->housing_room_package_id }}" data-id="{{ $p->id }}" data-info="{{ $p->description }}">{{ $p->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>

                                <span class="group-content__element--xs-block text-right ">
                                    <span class="group-content__element text-right">
                                        <label>How long?</label>
                                    </span>
                                    <div class="group-content__element mrg-left" style="width: 6.2em">
                                        <input type="number" id="housing-quantity-{{$r->id}}" name="quantity" data-housing-id="{{$r->id}}" class="text-left housing-quantity" style="width: 100%;">
                                    </div>
                                    <div class="group-content__element">
                                        <select id="select-housing-unit-{{$r->id}}" name="unit" data-housing-id="{{$r->id}}" class="text-left select-housing-unit">
                                            <option value="night">Nights</option>
                                            <option value="week">Weeks</option>
                                            <option value="month">Month</option>
                                        </select>
                                    </div>
                                    <span class="group-content__element text-right">
                                        <label>Guests</label>
                                    </span>
                                    <div class="group-content__element mrg-left" style="width: 6.2em">
                                        <select id="select-housing-people-{{$r->id}}" name="people" data-housing-id="{{$r->id}}" class="text-left select-housing-people">
                                            @php
                                                $bedsTmp = 0;
                                                if (isset($r->housingRooms->data[0]->beds)) {
                                                    $bedsTmp = $r->housingRooms->data[0]->beds;
                                                }
                                            @endphp
                                            @for($i = 1; $i <= $bedsTmp; $i++)
                                            <option value="{{ $i }}">{{ $i }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

            <div class="block-ask">
                <div class="block-ask__title">
                    <label class="btn-collapse">
                        <input type="radio" name="want-to-stay" data-target="#stay-no-housing"
                            data-housing-id="0" class="want-to-stay"> No housing
                    </label>
                </div>
                <div id="stay-no-housing" class="collapse block-ask__content">
                    <div class="content group-content"></div>
                </div>
            </div>
        </div>
    </div>

    {{-- Airport pick up? --}}
    <div class="block">
        <div class="block-header">
            <strong>Transfer service?</strong>
        </div>
        <div class="block-body">
            <div class="block-ask">
                <div class="block-ask__title">
                    <label class="btn-collapse">
                        <input type="radio" name="pickme-up" v-model="pickmeUp" value="yes" data-target="#pickmeup-airport"
                            class="choose-pickup">
                        Yes, please
                    </label>
                </div>
                <div id="pickmeup-airport" class="collapse block-ask__content" :class="{ in: pickmeUp.toLowerCase() == 'yes' }">
                    <div class="content group-content">
                        <div class="group-content__element group-content__element--xs-block">
                            <select id="select-pickup" name="pickup_id">
                                @foreach ($pickup as $r)
                                    {{-- No mostrar las opciones que tengan la palabra PKW --}}
                                    {{-- TODO: quitar esta condición cuando lo pida el cliente --}}
                                    @if (strpos($r->name, 'PKW') === false)
                                        <option value="{{ $r->id }}" data-cost="{{ $r->cost }}">{{ $r->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                            </div>
                        </div>
                    </div>
                </div>

            <div class="block-ask">
                <div class="block-ask__title">
                    <label class="btn-collapse">
                        <input type="radio" name="pickme-up" value="no" data-target="#pickmeup-nopick"
                            class="choose-pickup">
                        I don't need it
                    </label>
                </div>
                <div id="pickmeup-nopick" class="collapse block-ask__content">
                    <div class="content group-content"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="block">
        <div class="block-header">
            <strong>Travel partner</strong>
        </div>
        <div class="block-body">
            <div class="row">
                <div class="col-sm-6 field-inline">
                    <div class="field-inline-label">
                        <label for="travel_partner">Full name</label>
                    </div>
                    <div class="field-inline-field form-camp">
                        <input class="form-control" type="text" id="travel_partner" name="travel_partner"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    /*var EXP = {
        courses: {},
        housing: {}
    };*/
    window.addEventListener('load', function () {
        @php
        $packTmp = [];
        foreach ($packages as $p0) {
            foreach ($p0->packages as $p) {
                $packTmp[] = $p;
            }
        }
        @endphp
        EXP.packages.data = {!! json_encode($packTmp) !!};
        EXP.housing.data = {!! json_encode($housing) !!};
        EXP.selected = {
            from: null,
            to: null,
            monthYear: null,
            courses: {},
            housing: {},
            pickup: {}
        };
        $('.want-to-stay').on('ifChecked', function(event) {
            var housingId = $(this).attr('data-housing-id');
            var housing = null;
            appStep1.$root.wantToStay = this.value;
            EXP.selected.housing = {};
            if (housingId == 0) {
                $(this).closest('.block').find('.block-help').hide();
                return;
            }
            housing = EXP.housing.findById(housingId);
            EXP.selected.housing[housing.id] = {
                housing: housing.name,
                housing_room: $('select#select-housing-room-' + housing.id + ' option:selected').text(),
                housing_room_id: $('select#select-housing-room-' + housing.id + ' option:selected').val(),
                package: $('select#select-housing-package-' + housing.id + ' option:selected').text(),
                housing_room_package_id: $('select#select-housing-package-' + housing.id + ' option:selected').val(),
                quantity: EXP.toInteger($('#housing-quantity-' + housing.id).val()),
                unit: $('select#select-housing-unit-' + housing.id + ' option:selected').val(),
                cost: 0,
                people: $('select#select-housing-people-' + housing.id + ' option:selected').val()
            };
            var data = EXP.housing.getInputData(housing.id);
            EXP.housing.addUnits(data);
            EXP.housing.checkAvailability();
        });
        $('.choose-course').on('ifChanged', function(event) {
            var courseId = $(this).attr('data-course-id');
            $(this.getAttribute("data-target")).collapse('toggle');
            if (courseId == 0) {
                return;
            }
            if (!$(this)[0].checked) {
                delete EXP.selected.courses[courseId];
                return;
            }
            EXP.selected.courses[courseId] = {
                course: courseId.split('-').join(' & '),
                package: $('select#select-course-package-' + courseId + ' option:selected').text(),
                package_id: $('select#select-course-package-' + courseId + ' option:selected').val(),
                quantity: EXP.toInteger($('#course-quantity-' + courseId).val()),
                cost: 0
            };
        });
        $('.choose-pickup').on('ifChecked', function(event) {
            appStep1.$root.pickmeUp = this.value;
            EXP.selected.pickup = {};
            if ($(this).val() === 'yes') {
                EXP.selected.pickup = {
                    id: $('select#select-pickup option:selected').val(),
                    pickup: $('select#select-pickup option:selected').text(),
                    cost: EXP.toFloat($('select#select-pickup option:selected').attr('data-cost'))
                };
            }
        });
        $('.flight-tickets-already').on('ifChecked', function(event) {
            var date = '';
            appStep1.$root.flightTicketsAlready = parseInt(this.value);
            if ($(this).val() == 0) {
                EXP.selected.from = null;
                EXP.selected.to = null;
                EXP.selected.monthYear = $('#ticket-flight-month option:selected').val().leftPad(2, '0') + '-' +
                        $('#ticket-flight-year option:selected').val();
            }
            else {
                date = $('#arrival-day').val();
                EXP.selected.from = date !== '' ? moment(date, 'DD-MM-YYYY').format('YYYY-MM-DD') : null;
                date = $('#departure-day').val();
                EXP.selected.to = date !== '' ? moment(date, 'DD-MM-YYYY').format('YYYY-MM-DD') : null;
                EXP.selected.monthYear = null;
            }
        });
        EXP.housing.addUnits = function(data) {
            var housing = EXP.housing.findById(data.housing_id);
            var housingRoom = EXP.housing.findHousingRoomById(data.housing_id, data.housing_room_id);
            // Obtener unidades
            var package = EXP.housing.findPackageById(housing.id, housingRoom.id, data.package_id);
            var units = EXP.housing.getPackageUnits(package);
            // Agregar unidades
            var selectUnit = $('select#select-housing-unit-' + data.housing_id);
            selectUnit.html('');
            _.each(units, function(u) {
                selectUnit.append('<option value="' + u.value + '">' + u.name + '</option>');
            });
        };
        EXP.housing.calculateCost = function(data) {
            if (EXP.selected.from === null) {
                //return;
            }
            var housing = EXP.housing.findById(data.housing_id);
            var housingRoom = EXP.housing.findHousingRoomById(data.housing_id, data.housing_room_id);
            var package = EXP.housing.findPackageById(housing.id, housingRoom.id, data.package_id);
            var rate;
            var cost = 0;
            var unit = $('select#select-housing-unit-' + data.housing_id + ' option:selected').val();
            if (data.quantity > 0) {
                // Buscar tarifa
                rate = EXP.housing.findPackageRate(package, EXP.selected.from, unit);
                if (rate !== null) {
                    // Calcular costo
                    cost = EXP.toFloat(rate.unit_cost) * data.quantity;
                    if (housingRoom.cost_type == 'bed') {
                        cost *= data.people;
                    }
                }
            }
            if (EXP.selected.housing[housing.id] !== void 0) {
                EXP.selected.housing[housing.id].cost = cost;
            }
        };
        EXP.housing.showPackages = function(data) {
            var selectPackage = $('select#select-housing-package-' + data.housing_id);
            selectPackage.html('');
            // Obtener curso
            var room = EXP.housing.findHousingRoomById(data.housing_id, data.housing_room_id);
            // Agregar paquetes del curso
            _.each(room.packages.data, function(p) {
                // No mostrar las opciones que tenga la palabra PKW
                // TODO: quitar esta condición cuando lo pida el cliente
                if (p.name.indexOf('PKW') < 0) {
                    selectPackage.append('<option value="' + p.housing_room_package_id + '" data-id="' + p.id + '" data-info="' + p.description + '">' + p.name + '</option>');
                }
            });
            selectPackage.trigger('change');
        };
        EXP.housing.getInputData = function(housingId) {
            var data = {
                housing_id: housingId,
                housing_room_id: $('select#select-housing-room-' + housingId).find('option:selected').val(),
                housing_room_package_id: $('select#select-housing-package-' + housingId).find('option:selected').val(),
                package_id: $('select#select-housing-package-' + housingId).find('option:selected').attr('data-id'),
                quantity: EXP.toInteger($('#housing-quantity-' + housingId).val()),
                unit: $('select#select-housing-unit-' + housingId + ' option:selected').val(),
                people: $('select#select-housing-people-' + housingId + ' option:selected').val()
            };
            if (EXP.selected.housing[data.housing_id] === void 0) {
                EXP.selected.housing[data.housing_id] = {};
            }
            var housing = EXP.housing.findById(housingId);
            EXP.selected.housing[data.housing_id].housing = housing.name;
            EXP.selected.housing[data.housing_id].housing_room = $('select#select-housing-room-' + housingId + ' option:selected').text();
            EXP.selected.housing[data.housing_id].package = $('select#select-housing-package-' + housingId + ' option:selected').text();
            EXP.selected.housing[data.housing_id].housing_room_package_id = data.housing_room_package_id;
            EXP.selected.housing[data.housing_id].housing_room_id = data.housing_room_id;
            EXP.selected.housing[data.housing_id].unit = data.unit;
            EXP.selected.housing[data.housing_id].quantity = data.quantity;
            EXP.selected.housing[data.housing_id].people = data.people;
            return data;
        };
        EXP.housing.checkAvailability = function() {
            var housingId = 0;
            for (var id in EXP.selected.housing) {
                housingId = id;
            }
            if (housingId == 0) {
                return;
            }
            if (EXP.selected.from == null || EXP.selected.to == null) {
                return;
            }
            if (EXP.selected.housing[housingId].people === '') {
                return;
            }
            var housingRoomId = $('select#select-housing-room-' + housingId).find('option:selected').val();
            EXP.request.get(
                '/housing/availability?from_date=' + EXP.selected.from + '&to_date=' +
                    EXP.selected.to + '&housing_room_id=' + housingRoomId + '&people=' + EXP.selected.housing[housingId].people,
                function(r) {
                    var showError = false;
                    if (!(Object.keys(r.data) === 0)) {
                        if (r.data.availability < 0) {
                            showError = true;
                        }
                        if (showError) {
                            $('#choose-housing-' + housingId).closest('.block-ask')
                                    .addClass('alert-danger')
                                    .closest('.block').find('.block-help')
                                    .show()
                                    .html('The selected housing room is unavailable in the specified dates.');
                            EXP.selected.housing = {};
                            // Mostrar modal de ayuda
                            swal({
                                title: '',
                                text: "There is full occupancy on the selected dates. Anyways, don't worry, just contact us so we can help with your dates or try to select other housing option.",
                                type: 'error',
                                confirmButtonText: 'Contact us',
                                confirmButtonColor: '#3085d6',
                                showCancelButton: true,
                                cancelButtonColor: '#d33',
                                reverseButtons: true,
                                allowOutsideClick: false,
                            })
                            .then((result) => {
                                if (result) {
                                    let win = window.open('https://www.experienciamexico.mx/contact/', '_blank');
                                    win.focus();
                                }
                            })
                            .catch(swal.noop);;
                        }
                        else {
                            $('#choose-housing-' + housingId).closest('.block-ask')
                                    .removeClass('alert-danger')
                                    .closest('.block').find('.block-help')
                                    .hide();
                        }
                    }
                }
            );
        };
        // Mostrar paquetes al seleccionar un cuarto de hospedaje
        $('select.select-housing-room').on('change', function() {
            var data = EXP.housing.getInputData($(this).attr('data-housing-id'));
            var selected = $(this).find('option:selected');
            // Mostrar numero de personas permitidas para el tipo de cuarto seleccionado
            var selectPeople = $('#select-housing-people-' + data.housing_id);
            selectPeople.html('');
            for (var i = 1; i <= selected.data('beds'); i++) {
                selectPeople.append('<option value="' + i + '">' + i + '</option>');
            }
            EXP.housing.showPackages(data);
            data = EXP.housing.getInputData(data.housing_id);
            EXP.housing.addUnits(data);
            data = EXP.housing.getInputData(data.housing_id);
            EXP.housing.calculateCost(data);
            EXP.housing.checkAvailability();
        });
        // Mostrar unidades al seleccionar un paquete
        $('select.select-housing-package').on('change', function() {
            var data = EXP.housing.getInputData($(this).attr('data-housing-id'));
            EXP.housing.addUnits(data);
            data = EXP.housing.getInputData(data.housing_id);
            EXP.housing.calculateCost(data);
        });
        // Realizar calculos al ingresar la cantidad
        $('input.housing-quantity').change(function() {
            var data = EXP.housing.getInputData($(this).attr('data-housing-id'));
            EXP.housing.calculateCost(data);
        });
        // Realizar calculos al seleccionar la unidad
        $('select.select-housing-unit').on('change', function() {
            var data = EXP.housing.getInputData($(this).attr('data-housing-id'));
            EXP.housing.calculateCost(data);
        });
        // Realizar calculos cuando el número de personas cambia
        $('select.select-housing-people').on('change', function() {
            var data = EXP.housing.getInputData($(this).attr('data-housing-id'));
            EXP.housing.calculateCost(data);
            EXP.housing.checkAvailability();
        });

        $('#arrival-day').change(function(evt) {
            var departureDP = $('#departure-day');
            departureDP.removeAttr('disabled');
            var date = moment($(this).val(), 'DD-MM-YYYY');
            EXP.selected.from = date.format('YYYY-MM-DD');
            departureDP.datepicker('setStartDate', date.toDate());
        });
        $('#departure-day').change(function(evt) {
            var date = moment($(this).val(), 'DD-MM-YYYY');
            EXP.selected.to = date.format('YYYY-MM-DD');
            EXP.housing.checkAvailability();
        });
        $('#ticket-flight-month').on('change', function() {
            EXP.selected.monthYear = $(this).find('option:selected').val().leftPad(2, '0') + '-' +
                    $('#ticket-flight-year option:selected').val();
        });
        $('#ticket-flight-year').on('change', function() {
            setTimeout(function() {
                EXP.selected.monthYear = $('#ticket-flight-month option:selected').val().leftPad(2, '0') + '-' +
                        $('#ticket-flight-year option:selected').val();
            }, 500);
        });
        // Cursos
        EXP.packages.calculateCost = function(data) {
            var package = EXP.packages.findById(data.package_id);
            var rate;
            var cost = 0;
            if (data.quantity > 0) {
                // Buscar tarifa
                rate = EXP.packages.findPackageRate(package, data.quantity);
                if (rate !== null) {
                    // Calcular costo
                    cost = EXP.toFloat(rate.unit_cost) * data.quantity;
                }
            }
            if (EXP.selected.courses[data.course_id] !== void 0) {
                EXP.selected.courses[data.course_id].cost = cost;
            }
        };
        EXP.packages.showPackages= function(data) {
            var selectPackage = $('#select-course-package-' + data.course_id);
            selectPackage.html('');
            // Obtener curso
            var course = EXP.packages.findById(data.course_id);
            // Agregar paquetes del curso
            _.each(course.packages.data, function(p) {
                selectPackage.append('<option value="' + p.id + '">' + p.name + '</option>');
            });
        };
        EXP.packages.getInputData = function(courseId) {
            var data = {
                course_id: courseId,
                package_id: $('select#select-course-package-' + courseId).find('option:selected').val(),
                quantity: EXP.toInteger($('#course-quantity-' + courseId).val()),
                unit: $('select#select-course-unit-' + courseId + ' option:selected').val()
            };
            if (EXP.selected.courses[data.course_id] !== void 0) {
                EXP.selected.courses[data.course_id].package = $('select#select-course-package-' + courseId + ' option:selected').text();
                EXP.selected.courses[data.course_id].package_id = data.package_id;
                EXP.selected.courses[data.course_id].quantity = data.quantity;
            }
            return data;
        };
        // Realizar calculos al ingresar la cantidad
        $('input.course-quantity').change(function() {
            var data = EXP.packages.getInputData($(this).attr('data-course-id'));
            EXP.packages.calculateCost(data);
        });
        // Realizar calculos al seleccionar la unidad
        $('select.select-course-unit').on('change', function() {
            var data = EXP.packages.getInputData($(this).attr('data-course-id'));
            EXP.packages.calculateCost(data);
        });
        // Mostrar unidades al seleccionar un paquete
        $('select.select-course-package').on('change', function() {
            var data = EXP.packages.getInputData($(this).attr('data-course-id'));
            data = EXP.packages.getInputData($(this).attr('data-course-id'));
            EXP.packages.calculateCost(data);
        });

        // Seleccionar pickup
        $('select#select-pickup').on('change', function() {
            EXP.selected.pickup = {
                id: $(this).find('option:selected').val(),
                pickup: $(this).find('option:selected').text(),
                cost: $(this).find('option:selected').attr('data-cost')
            };
        });
        $('#next-step').click(function(evt) {
            evt.preventDefault();
            var hash = $(this).attr('href');
            if (EXP.isValidBooking()) {
                window.location.hash = hash;
            }
        });
        EXP.isValidBooking = function() {
            var housing = [];
            var courses = [];
            var id;
            var tmp;
            var $alert = $('.alert');
            var hash = window.location.hash;
            if (hash === '#step-1') {
                if ((EXP.selected.from == null || EXP.selected.to == null) && EXP.selected.monthYear == null) {
                    $alert.show().html("UPS!, you forgot to select the travel dates.");
                    $('html').scrollTop($alert.offset().top);
                    return false;
                }
            }
            else {
                if (EXP.selected.from == null || EXP.selected.to == null) {
                    $alert.show().html("UPS!, you forgot to select the travel dates.");
                    $('html').scrollTop($alert.offset().top);
                    return false;
                }
            }
            for (id in EXP.selected.housing) {
                housing.push(id);
                if (EXP.selected.housing[id].quantity == 0) {
                    $alert.show().html("UPS!, you forgot to select the housing duration.");
                    $('html').scrollTop($alert.offset().top);
                    return false;
                }
                if (EXP.selected.from != null && EXP.selected.to != null) {
                    tmp = moment(EXP.selected.from, 'YYYY-MM-DD');
                    var unit = EXP.selected.housing[id].unit;
                    var quantity = EXP.selected.housing[id].quantity;
                    if (unit === 'week') {
                        quantity *= 7;
                    } else if (unit === 'month') {
                        quantity *= 28; // 1 mes = 4 semanas, 1 semana = 7 días, 7 * 4 = 28 días del mes
                    }
                    if (tmp.add(quantity, 'days').isAfter(moment(EXP.selected.to, 'YYYY-MM-DD'))) {
                        $alert.show().html("UPS!, the housing duration is invalid for the selected dates.");
                        $('html').scrollTop($alert.offset().top);
                        return false;
                    }
                }
            }
            var days = 0;
            for (id in EXP.selected.courses) {
                courses.push(id);
                if (EXP.selected.courses[id].quantity == 0) {
                    $alert.show().html("UPS!, you forgot to select the course quantity.");
                    $('html').scrollTop($alert.offset().top);
                    return false;
                }
            }
            if (housing.length == 0 && courses.length == 0) {
                $alert.show().html("UPS!, you must to select the housing / course that you prefer.");
                $('html').scrollTop($alert.offset().top);
                return false;
            }
            if (hash === '#step-3') {
                if (EXP.selected.client_first_name === '' || EXP.selected.client_last_name === '') {
                    $alert.show().html("UPS!, you must to enter your first name and last name.");
                    $('html').scrollTop($alert.offset().top);
                    return false;
                }
                if (EXP.selected.client_email === '') {
                    $alert.show().html("UPS!, you must to enter your email.");
                    $('html').scrollTop($alert.offset().top);
                    return false;
                }
                tmp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if (!tmp.test(EXP.selected.client_email)) {
                    $alert.show().html("UPS!, you must to enter a valid email.");
                    $('html').scrollTop($alert.offset().top);
                    return false;
                }
            }
            $alert.hide();
            return true;
        };
        var hash = window.location.hash;
        if (hash !== '' && hash !== '#step-1') {
            if (!EXP.isValidBooking()) {
                window.location.hash = '#step-1';
            }
        }
    });
    window.addEventListener('hashchange', function() {
        var hash = window.location.hash;
        var container;
        var containerAmountToPay;
        var tmp;
        var total;
        if (hash === '#step-2') {
            container = $('#step-2 .block .block-body ul.list-group');
            containerAmountToPay = $('#step-2 .block .block-body .total-to-pay .amount');
            container.html('');
            total = 0;
            // Mostrar fecha
            tmp = EXP.selected.from == null ?
                    moment('01-' + EXP.selected.monthYear, 'DD-MM-YYYY') :
                    moment(EXP.selected.from, 'YYYY-MM-DD');
            container.append('' +
               '<li class="list-group-item list-group-item--point">' +
                    '<strong>When: </strong> ' + moment(EXP.selected.from, 'YYYY-MM-DD').format('DD-MM-YYYY') + ' to ' + moment(EXP.selected.to, 'YYYY-MM-DD').format('DD-MM-YYYY') +
                '</li>'
            );
            // Mostrar hospedaje
            for (var id in EXP.selected.housing) {
                if (!EXP.selected.housing.hasOwnProperty(id)) {
                    continue;
                }
                container.append('' +
                   '<li class="list-group-item list-group-item--point">' +
                        '<strong>Where: </strong> ' +
                        EXP.selected.housing[id].housing + ' / ' +
                        EXP.selected.housing[id].housing_room +
                        '<span class="badge">' + EXP.selected.housing[id].cost + ' USD</span>' +
                    '</li>'
                );
                total += parseFloat(EXP.selected.housing[id].cost);
            }
            // Mostrar cursos
            for (var id in EXP.selected.courses) {
                if (!EXP.selected.courses.hasOwnProperty(id)) {
                    continue;
                }
                container.append('' +
                   '<li class="list-group-item list-group-item--point">' +
                        '<strong>Course</strong> ' +
                        EXP.selected.courses[id].course + ' / ' +
                        EXP.selected.courses[id].package +
                    '</li>'
                );
                container.append('' +
                   '<li class="list-group-item list-group-item--point">' +
                        '<strong>How many: </strong> ' +
                        EXP.selected.courses[id].quantity +
                        '<span class="badge">' + EXP.selected.courses[id].cost + ' USD</span>' +
                    '</li>'
                );
                total += parseFloat(EXP.selected.courses[id].cost);
            }
            // Mostrar extras
            if (EXP.selected.pickup.id !== void 0) {
                container.append('' +
                    '<li class="list-group-item list-group-item--point">' +
                        '<strong>Extra</strong> Pick you up at ' +
                        EXP.selected.pickup.pickup +
                        '<span class="badge">' + EXP.selected.pickup.cost + ' USD</span>' +
                    '</li>'
                );
                total += parseFloat(EXP.selected.pickup.cost);
            }
            containerAmountToPay.text(total + ' USD');
        }
    });
</script>
