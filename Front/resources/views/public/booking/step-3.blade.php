<h3 class="title-step">To finish, fill in with your info
    <span class="title-step__description">Step 3 of 3</span>
</h3>

<div class="row">
    {{-- Name --}}
    <div class="col-sm-6 field-inline">
        <div class="field-inline-label">
            <label for="name">Name:</label>
        </div>
        <div class="field-inline-field form-camp">
            <div class="required">
                <input class="form-control" type="text" id="first-name" required="required" placeholder="*">
            </div>
        </div>
    </div>

    {{-- Last name --}}
    <div class="col-sm-6 field-inline">
        <div class="field-inline-label">
            <label for="last-name" id="width-to">Last name:</label>
        </div>
        <div class="field-inline-field form-camp">
            <div class="required">
                <input class="form-control" type="text" id="last-name" required="required" placeholder="*">
            </div>
        </div>
    </div>
</div>

<div class="row">
    {{--  Email  --}}
    <div class="col-sm-6 field-inline">
        <div class="field-inline-label">
            <label for="email">E-mail:</label>
        </div>
        <div class="field-inline-field form-camp">
            <div class="required">
                <input class="form-control" type="email" id="email" required="required" placeholder="*">
            </div>
        </div>
    </div>

    <div class="col-sm-6 field-inline">
        <div class="field-inline-label">
            <label for="phone">Phone:</label>
        </div>
        <div class="field-inline-field form-camp">
            <input class="form-control" type="tel" id="phone" value="">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 form-camp">
        <textarea class="form-control" id="comments" name="comments" cols="30" rows="10" placeholder="Comments"></textarea>
    </div>
</div>

<p class="text-right">* Obligatory fields</p>

{{-- Payment --}}
<div class="block">
    <div class="block-header">
        <div class="content-fluid">&nbsp;</div>
    </div>
    <div class="block-body">
        <div class="block-separation">
            <div class="block-separation text-center">
                <div class="text-center">
                    <label for="check-privacy-policy">
                        <input type="checkbox" id="check-privacy-policy">
                        I agree with <a href="http://www.experienciamexico.mx/privacy-policy" target="_blank">Privacy Policy</a>
                    </label>
                </div>
                <button id="btn-book" class="btn bg-primary btn-book" type="submit">Send request</button>
                <button id="btn-book-pay" class="btn bg-primary btn-book center-xs-block my-xs-5" type="submit">Confirm the reservation</button>
                {{--  <button class="btn" type="button">info</button>  --}}
                <a tabindex="0" role="button" data-toggle="popover" data-content="Información de ayuda, este botón muestra texto para tener una idea general de lo que está pasando o pasará"
                    class="popover-information btn btn-gray">info</a>
            </div>
        </div>
    </div>
</div>
