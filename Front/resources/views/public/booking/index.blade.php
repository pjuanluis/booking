@extends('layouts.public-booking-base')

@section('content')
    <div class="alert alert-warning" role="alert" style="display: none;">
        UPS!, you forgot to tell us where do you want to stay!
    </div>

    <form id="form-booking" action="" method="post">
        {{ csrf_field() }}
        <div class="container-fluid section">
            <div id="step-1" data-after="#step-2" class="step-section">
                @include('public.booking.step-1')
            </div>
            <div id="step-2" data-before="#step-1" data-after="#step-3" class="step-section">
                @include('public.booking.step-2')
            </div>
            <div id="step-3" data-before="#step-2" class="step-section">
                @include('public.booking.step-3')
            </div>
            <div class="block">
                <div class="block-header">&nbsp;</div>
                <div class="block-body block-separation">
                    <button type="reset" id="reset-step" class="btn bg-secondary-color text-white text-white-in-hover">CLEAR</button>
                    <a href="#" id="back-step" class="btn bg-primary">BACK STEP</a>
                    <a href="#" id="next-step" class="btn bg-primary pull-right">NEXT STEP</a>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('scripts')
    @parent
    <script>
        var appStep1 = new Vue({
            el: '#app-step1',
            data: {
                flightTicketsAlready: parseInt($('input[name="flight-tickets-already"]:checked').val()) || 1,
                wantToStay: parseInt($('input[name="want-to-stay"]:checked').val()) || '',
                pickmeUp: $('input[name="want-to-stay"]:checked').val() || ''
            }
        });
    </script>
    <script type="text/javascript">
        window.addEventListener('load', function() {
            inititalizeOnUrlHash();
            window.onhashchange = function() {
                inititalizeOnUrlHash();
            };
            $('#form-booking').on('reset', function(e) {
                var $form = $(e.target);
                setTimeout((function() {
                    this.find("select").trigger("change");
                    this.iCheck('update');
                    $('.collapse.block-ask__content').removeClass('in');
                }).bind($form), 0);
                setTimeout(function() {
                    EXP.selected = {
                        from: null,
                        to: null,
                        monthYear: null,
                        courses: {},
                        housing: {},
                        pickup: {}
                    };
                }, 500);
                window.location.hash = '#step-1';
            });
            $('button.btn-book').click(function(evt) {
                evt.preventDefault();
                if (!$('#check-privacy-policy').is(':checked')) {
                    $('.alert').show().html("UPS!, you must agree with the privacy policies.");
                    $('html').scrollTop($('.alert').offset().top);
                    return;
                }
                $('.btn-book').prop('disabled', true);
                // TODO: Agregar loading mask
                var btnId = $(this).attr('id');
                EXP.selected.client_first_name = $('#step-3 input#first-name').val();
                EXP.selected.client_last_name = $('#step-3 input#last-name').val();
                EXP.selected.client_email = $('#step-3 input#email').val();
                EXP.selected.client_phone = $('#step-3 input#phone').val();
                EXP.selected.comments = $('#step-3 #comments').val();
                EXP.selected.travel_partner = $("#step-1 #travel_partner").val();
                if (!EXP.isValidBooking()) {
                    $('#btn-book-pay').prop('disabled', false);
                    $('#btn-book').prop('disabled', false);
                    return;
                }
                var id, days;
                for (id in EXP.selected.housing) {
                    EXP.selected.housing[id].from = EXP.selected.from;
                    var unit = EXP.selected.housing[id].unit;
                    days = EXP.selected.housing[id].quantity;
                    if (unit === 'week') {
                        days *= 7;
                    } else if (unit === 'month') {
                        days *= 28; // 1 mes = 4 semanas, 1 semana = 7 días, 7 * 4 = 28 días del mes
                    }
                    EXP.selected.housing[id].to = moment(EXP.selected.from).add(days, 'days').format('YYYY-MM-DD');
                }
                EXP.selected.pay = (btnId === 'btn-book-pay' ? 1 : 0);
                $('body').ploading({action: 'show'});
                EXP.request.post('/booking', EXP.selected, function(r) {
                    var fields;
                    var $alert = $('.alert');
                    var msg;
                    if (r.status_code == 201 || r.status_code == 200) {
                        if (EXP.selected.pay == 1) {
                            window.location.href = '/booking/payment?token=' + r.data.token;
                        }
                        else {
                            swal("Your request has been sent.", "You will receive an email to confirm that we've received it. Thank you for your interest in Experiencia.", "success")
                            .then(() => {
                                let win = window.open('https://www.experienciamexico.mx/welcome-to-experiencia/', '_blank');
                                win.focus();
                                window.location.href = '/booking/';
                            });
                        }
                    }
                    else if (r.status_code == 422) {
                        msg = [r.message];
                        if (r.errors !== void 0) {
                            fields = _.filter(_.keys(r.errors), function(f) {
                                return f.indexOf('courses') == 0 || f.indexOf('housing') == 0 || f.indexOf('pickup_id') == 0;
                            });
                            if (fields.length > 0) {
                                _.each(fields, function(f) {
                                    msg.push(r.errors[f][0]);
                                });
                                window.location.hash = '#step-1';
                            }
                            else {
                                _.each(_.keys(r.errors), function(f) {
                                    msg.push(r.errors[f][0]);
                                });
                                window.location.hash = '#step-3';
                            }
                        }
                        $alert.show().html(msg.join('<br />'));
                        $('html').scrollTop($alert.offset().top);
                        $('#btn-book-pay').prop('disabled', false);
                        $('#btn-book').prop('disabled', false);
                    }
                    else {
                        $alert.show().html(r.message);
                        $('html').scrollTop($alert.offset().top);
                        $('#btn-book-pay').prop('disabled', false);
                        $('#btn-book').prop('disabled', false);
                    }
                    $('body').ploading({action: 'hide'});
                }, function(r) {
                    // Cuando sucede un error en la petición
                    $('body').ploading({action: 'hide'});
                    var $alert = $('.alert');
                    $alert.show().html("Unexpected error, try again.<br/>If the error persists please repeat the whole process again, <a href='/'>click here to do it<a>.");
                    $('html').scrollTop($alert.offset().top);
                    $('#btn-book-pay').prop('disabled', false);
                    $('#btn-book').prop('disabled', false);
                });
            });
        });
    </script>
@endsection
