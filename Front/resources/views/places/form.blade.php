@extends('layouts.app')

@section('title')
    {{ $title }}
@endsection

@section('content-header')
    <h1>{{ $title }}</h1>
@endsection

@php
    $readOnly = ($formType === Constant::FORM_SHOW) ? 'readonly' : '';
@endphp

@section('content')
<form id="client-form" action="{{ $formType === Constant::FORM_SHOW ? '#' : $formAction }}" method="POST">
    {{ csrf_field() }}
    @if ($formType === Constant::FORM_EDIT)
        {{ method_field('PUT') }}
        <input type="hidden" name="updated_at" value="{{ $place->updated_at }}" />
    @endif
    <div class="box" style="">
        <div class="box-body">
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name" class="control-label">Name*</label>
                <input id="name" type="text" class="form-control"
                    name="name" value="{{ old('name', isset($place)? $place->name : '') }}" {{ $readOnly }} required>
                <span class="help-block">{{ $errors->first('name') }}</span>
            </div>
            @php
                $address = old('address', isset($place)? $place->address : '');
                $latitude = old('latitude', isset($place)? $place->latitude : '');
                $longitude = old('longitude', isset($place)? $place->longitude : '');
                $errorMessage = "";
            @endphp
            <input id="longitude" type="hidden" class="form-control" name="longitude" value="{{ $longitude }}">
            <input id="latitude" type="hidden" class="form-control" name="latitude" value="{{ $latitude }}">
            <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                <label for="address" class="control-label">Address</label>
                <input id="address" type="text" class="form-control" name="address" value="{{ $address }}" {{ $readOnly }} placeholder="">
                <span class="help-block">{{ $errors->first('address') }}</span>
            </div>
            <div class="form-group {{ $errors->has('latitude') || $errors->has('longitude') ? 'has-error' : '' }}">
                <div id="map" style="width: 100%; height: 400px;">

                </div>
                @if (!empty($errors->first('longitude')))
                    @php
                        $errorMessage = $errors->first('longitude');
                    @endphp
                @elseif(!empty($errors->first('latitude')))
                    @php
                        $errorMessage = $errors->first('latitude');
                    @endphp
                @endif
                <span class="help-block">{{ $errorMessage }}</span>
            </div>
        </div>
        <div class="box-footer">
            <a class="btn btn-default" href="{{ url()->previous() }}">
                @if ($formType !== Constant::FORM_SHOW)
                    Cancel
                @else
                    Back
                @endif
            </a>
            @if ($formType !== Constant::FORM_SHOW)
                <button class="btn btn-primary" type="submit" name="action">Save</button>
            @else
                <a href="{{ route('places.edit', ['id' => $place->id]) }}" class="btn btn-primary">Edit</a>
            @endif
        </div>
    </div>
</form>

@endsection

@section('scripts')
    <script type="text/javascript" src="//maps.google.com/maps/api/js?key={{ env('GMAP_KEY') }}&libraries=geometry,places"></script>
    @parent
    <script type="text/javascript">
        $(function() {
            var marker =  null;
            var latLngInit = {lat: {{ !empty($latitude)? $latitude : 15.872308066355158 }}, lng: {{ !empty($longitude)? $longitude : -97.0754584106445 }}}
            var placeLatLng = new google.maps.LatLng(latLngInit);
            @if ($formType != Constant::FORM_SHOW)
                var autocomplete = new google.maps.places.Autocomplete(document.getElementById('address'));
            @endif

            var map = new google.maps.Map(document.getElementById('map'), {
                center: placeLatLng,
                zoom: 11
            });

            var geocoder = new google.maps.Geocoder;
            @if ($formType != Constant::FORM_CREATE)
                addMarker(map, placeLatLng);
            @endif

            map.addListener('click', function(e) {
                addMarker(map, e.latLng);
            });

            function geocodeLatLng(geocoder, map, latlng, input) {
                geocoder.geocode({'location': latlng}, function(results, status) {
                    if (status === 'OK') {
                        if (results[1]) {
                            input.val(results[1].formatted_address);
                        } else {
                            window.alert('No results found');
                        }
                    } else {
                        window.alert('Geocoder failed due to: ' + status);
                    }
                });
            }

            function addMarker(map, latLng) {
                map.setZoom(11);
                if (marker != void 0) {
                    marker.setMap(null);
                }
                marker = new google.maps.Marker({
                    position: latLng,
                    map: map
                });
                geocodeLatLng(geocoder, map, latLng, $("#address"));
                showLatitudeAndLongitude(latLng);
                map.setCenter(latLng);
                map.setZoom(17);
            }

            function showLatitudeAndLongitude(latLng) {
                $('#latitude').val(latLng.lat());
                $('#longitude').val(latLng.lng());
            }

            autocomplete.addListener('place_changed', function() {
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    // User entered the name of a Place that was not suggested and
                    // pressed the Enter key, or the Place Details request failed.
                    window.alert("No details available for input: '" + place.name + "'");
                    return;
                }
                addMarker(map, place.geometry.location);
            });
        });
    </script>
@endsection
