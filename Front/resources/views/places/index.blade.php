@extends('layouts.app')

@section('title')
    Places
@endsection

@section('content-header')
    <h1>Places</h1>
@endsection

@section('content')
<div class="box">
    <div class="box-header with-border">
        <a href="{{ route('places.create') }}" class="btn btn-primary pull-right">New place</a>
    </div>
    <div class="box-body">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Address</th>
                    <th class="text-center">View</th>
                    <th class="text-center">Edit</th>
                    <th class="text-center">Delete</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($places->data as $place)
                <tr>
                    <td>{{ $place->name }}</td>
                    <td>{{ $place->address }}</td>
                    <td class="text-center fit">
                        <a class="action-btn" href="{{ route('places.show', ['id' => $place->id]) }}">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </a>
                    </td>
                    <td class="text-center fit">
                        <a class="action-btn" href="{{ route('places.edit', ['id' => $place->id]) }}">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                    </td>
                    <td class="text-center fit">
                        <form action="{{ route('places.destroy', [ 'id' => $place->id]) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="action-btn table-btn-destroy">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </button>
                        </form>
                    </td>
                </tr>
                @empty
                    <tr>
                        <td colspan="40">No data</td>
                    </tr>
                @endforelse
            </tbody>
        </table>

        {!! Component::pagination(
            10,
            $places->meta->pagination->current_page,
            $places->meta->pagination->total_pages,
            $params
            ) !!}
    </div>
</div>
<script type="text/javascript">
    window.addEventListener('load', function() {
        EXP.initConfirmButton({
            selector: 'button.table-btn-destroy',
            title: "Delete place",
            msg: "This action is going to delete the current place.",
            callback: function(btn) {
                $(btn).parent().submit();
            }
        });

        EXP.initConfirmButton({
            selector: 'button.table-btn-activate',
            title: "Activate/deactivate room type",
            msg: "This action is going to activate/deactivate the current place.",
            callback: function(btn) {
                $(btn).parent().submit();
            }
        });
    });
</script>
@endsection
