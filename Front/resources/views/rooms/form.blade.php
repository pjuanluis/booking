@extends('layouts.app')
@section('title') {{ $title }}
@endsection

@section('content-header')
<h1>{{ $title }}</h1>
@endsection
@php
    $readOnly = ($formType === Constant::FORM_SHOW) ? 'readonly' : '';
@endphp
@section('content')
<form id="client-form" action="{{ $formType === Constant::FORM_SHOW ? '#' : $formAction }}" method="POST">
    {{ csrf_field() }} @if ($formType === Constant::FORM_EDIT) {{ method_field('PUT') }}
    <input type="hidden" name="updated_at" value="{{ $room->updated_at }}" /> @endif
    <div class="box" style="">
        <div class="box-body">
            @php
                $selectedHousingRoomsTmp = [];
                if (isset($room)) {
                    foreach ($room->housingRooms->data as $hr) {
                        array_push($selectedHousingRoomsTmp, $hr->id);
                    }
                }
                $selectedHousingRooms = old('housing_rooms', $selectedHousingRoomsTmp);
            @endphp
            <div class="form-group {{ $errors->has('residence_id') ? 'has-error' : '' }}">
                <label for="residence_id" class="control-label">Residence*</label>
                <input id="residence_id" type="text" class="form-control" value="{{ $residence->data->name }}"readonly>
                <span class="help-block">{{ $errors->first('residence_id') }}</span>
            </div>
            <div class="form-group {{ $errors->has('housing_rooms') ? 'has-error' : '' }}">
                <label for="housing_rooms" class="control-label">Room type*</label>
                <select name="housing_rooms[]" id="housing_rooms" class="form-control select2" multiple="multiple" {{ $readOnly == 'readonly'? 'disabled' : '' }}>
                    @foreach ( $residence->data->housing->data->housingRooms->data as $hr)
                        <option value="{{ $hr->id }}" {{ in_array($hr->id, $selectedHousingRooms)? 'selected' : '' }}>{{ $hr->name }}</option>
                    @endforeach
                </select>
                <span class="help-block">{{ str_replace('Housing room', 'Room type', $errors->first('housing_rooms')) }}</span>
            </div>
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name" class="control-label">Name*</label>
                <input id="name" type="text" class="form-control" name="name" value="{{ old('name', isset($room)? $room->name : '') }}"
                    {{ $readOnly }} required>
                <span class="help-block">{{ $errors->first('name') }}</span>
            </div>
            <div class="form-group {{ $errors->has('beds') ? 'has-error' : '' }}">
                <label for="beds" class="control-label">Beds*</label>
                <input id="level" type="number" min="1" class="form-control" name="beds" value="{{ old('beds', isset($room)? $room->beds : '') }}"
                    {{ $readOnly }} />
                <span class="help-block">{{ $errors->first('beds') }}</span>
            </div>

            {{-- BLock room --}}
            <div class="block-room row" id="block-room"
            data-ranges-dates="{{ isset($room->blockedDates) ? json_encode($room->blockedDates->data) : '[]' }}">
                <div class="col-sm-4 form-inline">
                    <div class="form-group">
                        <label>Out of service</label>
                        @if ($readOnly !== 'readonly')
                        <div class="d-flex">
                            <div class="input-group">
                                <input type="text" class="form-control from"
                                v-model="range.from" data-date-format="dd-mm-yyyy">
                                <div class="input-group-addon">to</div>
                                <input type="text" class="form-control to"
                                v-model="range.to" data-date-format="dd-mm-yyyy">
                            </div>
                            <button type="button" class="btn btn-primary" :disabled="range.from === '' || range.to === ''"
                            @click="addDateRange()" style="margin-left: 1rem;">Add</button>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12">
                    <input type="hidden" name="ranges_date_deleted" :value="JSON.stringify(rangesDateDeleted)">
                    <ul class="ranges-list list-group">
                        <li v-if="rangesList.length === 0"
                        class="list-group-item">No data available!</li>
                        <li v-for="(range, rangeK) in rangesList" v-key="rangeK"
                        class="list-group-item">
                            <input type="hidden" :name="'ranges-list[' + rangeK + '][id]'" :value="range.id">
                            <input type="hidden" :name="'ranges-list[' + rangeK + '][from_at]'" :value="formatHumanDate(range.from)">
                            <input type="hidden" :name="'ranges-list[' + rangeK + '][to_at]'" :value="formatHumanDate(range.to)">

                            @if ($readOnly !== 'readonly')
                            <button type="button" class="btn btn-default"
                            @click="deleteDateRange(rangeK)">X</button>
                            @endif
                            <span v-if="range.to !== null">From</span>
                            @{{ formatHumanDate(range.from) }}
                            <span v-if="range.to !== null">
                            to @{{ formatHumanDate(range.to) }}
                            </span>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
        <div class="box-footer">
            <a class="btn btn-default" href="{{ url()->previous() }}">
                @if ($formType !== Constant::FORM_SHOW)
                    Cancel
                @else
                    Back
                @endif
            </a>
            @if ($formType !== Constant::FORM_SHOW)
                <button class="btn btn-primary" type="submit" name="action">Save</button>
            @else
                <a href="{{ route('rooms.edit', ['residence' => $residenceId, 'id' => $room->id]) }}" class="btn btn-primary">Edit</a>
            @endif
        </div>
    </div>
</form>
@endsection

@section('scripts')
    @parent
    <script src="{{ asset('js/vue.min.js') }}"></script>
    <script>
        $(function(){
            window.obj = new BlockRoom(document.getElementById('block-room'));
        });
    </script>
@endsection
