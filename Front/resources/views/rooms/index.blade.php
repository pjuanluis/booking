@extends('layouts.app')

@section('title')
    Rooms
@endsection

@section('content-header')
    <h1>Rooms</h1>
@endsection

@section('content')
<div class="box">
    <div class="box-header with-border">
        <a href="{{ route('rooms.create', ['residence' => $residence->data->id]) }}" class="btn btn-primary pull-right">New room</a>
    </div>
    <div class="box-body">
        <div class="panel panel-primary no-margin no-border">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Residence:
                    <span class="text-capitalize">{{ $residence->data->name }}</span>
                </h3>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Beds</th>
                            <th class="text-center">View</th>
                            <th class="text-center">Edit</th>
                            <th class="text-center">Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($residence->data->rooms->data as $room)
                        <tr>
                            <td>{{ $room->name }}</td>
                            <td>{{ $room->beds }}</td>
                            <td class="text-center fit">
                                <a class="action-btn" href="{{ route('rooms.show', ['residence' => $residence->data->id, 'id' => $room->id]) }}">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </a>
                            </td>
                            <td class="text-center fit">
                                <a class="action-btn" href="{{ route('rooms.edit', ['residence' => $residence->data->id, 'id' => $room->id]) }}">
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </a>
                            </td>
                            <td class="text-center fit">
                                <form action="{{ route('rooms.destroy', ['residence' => $residence->data->id, 'id' => $room->id]) }}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button type="submit" class="action-btn table-btn-destroy">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @empty
                            <tr>
                                <td colspan="40">No data</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <a href="{{ route('residences.index') }}" class="btn btn-primary">Back</a>
    </div>
</div>
<script type="text/javascript">
    window.addEventListener('load', function() {
        EXP.initConfirmButton({
            selector: 'button.table-btn-destroy',
            title: "Delete room",
            msg: "This action is going to delete the current room.",
            callback: function(btn) {
                $(btn).parent().submit();
            }
        });
    });
</script>
@endsection
