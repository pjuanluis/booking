@extends('layouts.app')

@section('styles')
    @parent
    <style>
        .scheduler-container {
            max-height: 100vh;
        }
        .sch-scheduler {
            padding: 0 10px 16px 10px;
            margin-top: 16px;
            margin-bottom: 0;
            min-width: 1024px;
        }
    </style>
@endsection

@section('content')
    <div class="scheduler-container" id="scheduler-container">
        <div id="scheduler" class="sch-scheduler"
            data-selector="#scheduler"
            data-current-month="{{ $now->month }}"
            data-current-year="{{ $now->year }}"
            data-url="/admin/accommodations"
            data-rooms="{{ json_encode($accommodationData['rooms']) }}"></div>
    </div>
@endsection

@section('scripts')
    @parent
    <script>
        $(function(){
            var $scheduler = $('#scheduler');
            var schedulerData = $scheduler.data();
            new EXP.Scheduler(schedulerData);
        });
</script>
@endsection
