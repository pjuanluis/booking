/**
 * Class BlockRoom
 */
var BlockRoom = (function() {
    var thisClass = null;

    function BlockRoom(el) {
        thisClass = this;
        thisClass.el = el;
        thisClass.$from = null;
        thisClass.$to = null;
        thisClass.now = moment();
        
        thisClass.humanDateFormat = 'DD-MM-YYYY';
        
        thisClass.vm = null;

        initializeVue();
    }

    function initializeVue() {
        thisClass.vm = new Vue({
            data: function() {
                return {
                    range: {
                        from: '',
                        to: ''
                    },
                    rangesDateDeleted: [],
                    rangesList: []
                };
            },
            watch: {
                rangesList: watchRangesList
            },
            methods: {
                addDateRange: addDateRange,
                deleteDateRange: deleteDateRange,
                formatHumanDate: formatHumanDate
            },
            mounted: function() {
                initializeDatepickers.call(this);
                initializeRangesList.call(this);
            }
        }).$mount(thisClass.el);
    }

    function initializeRangesList() {
        var vm = this;
        var dataRangesDates = JSON.parse(vm.$el.getAttribute('data-ranges-dates') || '[]');

        for (var i=0, size=dataRangesDates.length; i < size; i++) {
            dataRangesDates[i].from = getDateAsMoment(dataRangesDates[i].from);
            dataRangesDates[i].to = getDateAsMoment(dataRangesDates[i].to);
        }

        vm.rangesList = dataRangesDates;
    }

    /**
     * 
     */
    function initializeDatepickers() {
        var vm = this;
        
        thisClass.$from = $(vm.$el).find('input.from');
        thisClass.$to = $(vm.$el).find('input.to');

        thisClass.$from.datepicker();
        thisClass.$to.datepicker();

        thisClass.$from.datepicker(
            'setStartDate',
            thisClass.now.format(thisClass.humanDateFormat)
        );
        thisClass.$to.datepicker(
            'setStartDate',
            thisClass.now.format(thisClass.humanDateFormat)
        );
        
        // Events
        thisClass.$from.on('changeDate', function(evt) {
            var value = thisClass.$from.val();
            var newDate = moment(value, thisClass.humanDateFormat);
            thisClass.$to.datepicker(
                'setStartDate',
                newDate.format(thisClass.humanDateFormat)
            );  
            vm.range.from = value;
        });
        thisClass.$to.on('changeDate', function(evt) {
            vm.range.to = thisClass.$to.val();
        });
    }

    /**
     * 
     */
    function watchRangesList() {
        var vm = this;
        var disableDates = [];
        _.each(vm.rangesList, function(range) {
            var tmpDate = null;
            
            if (range === void 0 || range === null) {
                return;
            }

            if (range.to !== void 0 && range.to !== null) {
                tmpDate = moment(range.from).clone();
                while (range.to.diff(tmpDate, 'days') >= 0) {
                    disableDates.push(tmpDate.format(thisClass.humanDateFormat));
                    tmpDate.add(1, 'day');
                }
            }
        });

        thisClass.$from.datepicker('setDatesDisabled', disableDates);
    }

    /**
     * 
     * @param {moment} date 
     * @returns {String|void}
     */
    function formatHumanDate(date) {
        if (date === null || date === void 0) {
            return;
        }

        return date.format(thisClass.humanDateFormat);
    }

    /**
     * 
     * @param {String} d
     * @returns {moment|null} 
     */
    function getDateAsMoment(d) {
        var date = null;

        if (d === void 0 || d === null) {
            d = '';
        }

        date = moment(d, thisClass.humanDateFormat);

        return date.isValid() ? date : null;
    }

    /**
     * 
     */
    function addDateRange() {
        var vm = this;
        var from = getDateAsMoment(vm.range.from);
        var to = getDateAsMoment(vm.range.to);

        if (from === null) {
            return;
        }
        
        vm.range.from = '';
        vm.range.to = '';

        vm.rangesList.push({
            id: 0,
            from: from,
            to: to
        });
    }

    /**
     * 
     * @param {int} index 
     */
    function deleteDateRange(index) {
        var vm = this;
        var range = vm.rangesList[index];

        if (range.id > 0) {
            vm.rangesDateDeleted.push(range.id);
        }

        vm.$delete(vm.rangesList, index);
    }
    
    return BlockRoom;
})();