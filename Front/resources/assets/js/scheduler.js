
/* global moment */

if (window.EXP === void 0) {
    window.EXP = {
        courses: {},
        housing: {}
    };
}

EXP.Scheduler = function(config) {
    this.config = config;
    this.ele = $(config.selector);
    this.events = this.config.events || [];
    this.events = _.sortBy(this.events, 'start');
    var date = moment(new Date(this.config.currentYear, this.config.currentMonth - 1, 1));
    this.current = {
        month: this.config.currentMonth,
        year: this.config.currentYear,
        firstDate: date,
        lastDate: moment(new Date(this.config.currentYear, this.config.currentMonth - 1, date.daysInMonth()))
    };
    var self = this;
    this.setCurrent = function(date) {
        this.current.month = date.month() + 1;
        this.current.year = date.year();
        this.current.firstDate = moment(new Date(date.year(), date.month(), 1));
        this.current.lastDate = moment(new Date(this.current.year, this.current.month - 1, date.daysInMonth()));
    };
    this.nextMonth = function() {
        var date = this.current.firstDate.add(1, 'M');
        this.setCurrent(date);
        this.render();
    };
    this.previousMonth = function() {
        var date = this.current.firstDate.subtract(1, 'M');
        this.setCurrent(date);
        this.render();
    };
    this.goto = function(date) {
        if (!(date instanceof moment)) {
            date = moment(date);
            if (!date.isValid()) {
                return;
            }
        }
        if (this.current.month === (date.month() + 1) && this.current.year === date.year()) {
            return;
        }
        this.setCurrent(date);
        this.render();
    };
    this.renderNav = function() {
        var content = '' +
            '<div class="sch-buttons">' +
            '<div class="sch-next-button"><i class="fa fa-chevron-right"></i></div>' +
            '<div class="sch-prev-button"><i class="fa fa-chevron-left"></i></div>' +
            '<div class="sch-today-button"><span>Today</span></div>' +
            '<div class="clearfix"></div>' +
            '</div>' +
            '<div class="sch-date"></div>';
        this.ele.find('.sch-navline').html(content);
        // Inicializar botones
        this.ele.find('.sch-navline .sch-next-button > i').click(function() {
            self.nextMonth();
        });
        this.ele.find('.sch-navline .sch-prev-button > i').click(function() {
            self.previousMonth();
        });
        this.ele.find('.sch-navline .sch-today-button > span').click(function() {
            self.goto(moment());
        });
    };
    this.renderCalendar = function() {
        var content = '' +
            '<div class="sch-axis-x">' +
            '<table>' +
            '<tr>' +
            '<td style="width: 105px;">Room</td>' +
            '<td>Type</td>' +
            '</tr>' +
            '</table>' +
            '</div>' +
            '<div class="sch-axis-y">' +
            '<div class="sch-current-month"></div>' +
            '<table class="sch-month-days">' +
            '<tr></tr>' +
            '</table>' +
            '</div>' +
            '<div class="clearfix"></div>';
        this.ele.find('.sch-header').html(content);
        var w = this.ele.find('.sch-header').width() - this.ele.find('.sch-header .sch-axis-x').width();
        this.ele.find('.sch-header .sch-axis-y').width(w);
    };
    this.groupData = function(room) {
        var rows = [];
        // Evitar encimamiento de reservaciones
        // Checar si hay fechas que se entrelazen para un mismo cuarto
        _.each(self.events, function(e) {
            if (e.room_id != room.id) {
                return;
            }
            var start = moment(e.start);
            var end = moment(e.end);
            if (
                !(
                    (start.month() + 1 == self.current.month && start.year() == self.current.year) ||
                    (end.month() + 1 == self.current.month && end.year() == self.current.year) ||
                    (self.current.firstDate.isAfter(start) && self.current.lastDate.isBefore(end))
                )
            ) {
                return;
            }
            var entry;
            var addTo = 0;
            var placed = false;
            var i, j;
            for (i = 0; i < rows.length; i++) {
                for (j = 0; j < rows[i].length; j++) {
                    entry = rows[i][j];
                    if (start.isSameOrAfter(entry.end) || end.isSameOrBefore(entry.start)) {
                        placed = true;
                        continue;
                    }
                    placed = false;
                    break;
                }
                if (placed) {
                    break;
                }
            }
            addTo = i;
            if (rows.length === 0) {
                rows.push([]);
            }
            if (rows.length < (addTo + 1)) {
                rows.push([]);
            }
            rows[addTo].push({
                command_id: e.command_id,
                start: start,
                end: end,
                text: e.text,
                housing_room_name: e.housing_room_name,
                room_id: e.room_id,
                booking_code: e.booking_code,
                command_housing_id: e.command_housing_id ? e.command_housing_id : 0,
                people: e.people,
            });
        });
        return rows;
    };
    this.renderData = function() {
        var daysWidth = this.ele.find('.sch-header .sch-axis-y').width();
        var template = '' +
            '<div class="sch-entry">' +
            '<div class="sch-axis-x">' +
            '<table>' +
            '<tr>' +
            '<td style="width: 105px;">%ROOM%</td>' +
            '<td>%TYPE%</td>' +
            '</tr>' +
            '</table>' +
            '</div>' +
            '<div class="sch-axis-y" style="width: ' + daysWidth + 'px;">' +
            '<table>' +
            '<tr>%DAYS%</tr>' +
            '</table>' +
            '%EVENTS%' +
            '</div>' +
            '<div class="clearfix"></div>' +
            '</div>';
        var monthDates = [];
        var days = self.current.firstDate.daysInMonth();
        var style = 'style="width: calc(100% / ' + days + ');"';
        var classes = '';
        var width = (daysWidth / days);
        // Obtener fines de semana
        var weekends = [];
        var dt = moment(new Date(self.current.year, self.current.month - 1, 1));
        for (var i = 1; i <= days; i++) {
            if (dt.day() == 6 || dt.day() == 0) {
                weekends.push(i);
            }
            monthDates.push(dt.clone());
            dt.add(1, 'd');
        }
        var showData = function() {
            self.ele.find('.sch-data .sch-entry-container').html('');
            var url = location.protocol + '//' +  location.hostname + '/admin/bookings/{id}/edit';
            _.each(self.config.rooms, function(r, index) {
                var tpl = template;
                tpl = tpl.replace('%ROOM%', r.residence + ' ' + r.name);
                tpl = tpl.replace('%TYPE%', r.type.join(',') + '<br />' + r.beds + ' beds');
                // Mostrar días del mes actual
                var content = '';
                var fromAt = null;
                var toAt = null;
                var sizeBlockedDates = 0;
                for (var i = 1; i <= days; i++) {
                    classes = '';
                    // Pintar fines de semana
                    if (weekends.indexOf(i) !== -1) {
                        classes = 'sch-weekend';
                    }

                    if (r.blockedDates.data === void 0 || r.blockedDates.data === null) {
                        sizeBlockedDates = 0;
                    } else {
                        sizeBlockedDates = r.blockedDates.data.length;
                    }

                    // Add class to mark room blocked days
                    for (var j = 0; j < sizeBlockedDates; j++) {
                        fromAt = moment(r.blockedDates.data[j].from_at, 'YYYY-MM-DD');
                        toAt = null;

                        if (
                            r.blockedDates.data[j].to_at === void 0 ||
                            r.blockedDates.data[j].to_at === null
                        ) {
                            toAt = fromAt.clone();
                        } else {
                            toAt = moment(r.blockedDates.data[j].to_at, 'YYYY-MM-DD');
                        }

                        // console.log(monthDates[i - 1].toString(), r.blockedDates.data[j].from_at, r.blockedDates.data[j].to_at);
                        if (monthDates[i - 1].isBetween(fromAt, toAt, 'days', '[]')) {
                            classes += ' sch-room-blocked-day';
                        }
                    }

                    content += '<td ' + style + ' class="' + classes + '"></td>';
                }
                tpl = tpl.replace('%DAYS%', content);
                content = '';
                var rows = self.groupData(r);
                // Pintar reservaciones
                _.each(rows, function(row, i) {
                    _.each(row, function(e, j) {
                        if (e.room_id != r.id) {
                            return;
                        }
                        var days;
                        var left;
                        var w;
                        // TODO: Checar si inicia en otro mes pero termina en el mes actual
                        if (e.start.isBefore(self.current.firstDate)) {
                            if (e.end.month() + 1 == self.current.month) {
                                days = e.end.diff(self.current.firstDate, 'd');
                                left = 0;
                                w = (width * days) - 12 + (width / 2);
                            }
                            else {
                                days = self.current.firstDate.daysInMonth();
                                left = 0;
                                w = daysWidth - 12;
                            }
                        }
                        else if (e.end.isAfter(self.current.lastDate)) {
                            days = self.current.lastDate.diff(e.start, 'd');
                            left = (e.start.date() * width) - (width / 2) + 2;
                            w = (width * days) - 14 + (width / 2);
                        }
                        else {
                            days = e.end.diff(e.start, 'd');
                            left = (e.start.date() * width) - (width / 2) + 2;
                            w = (width * days) - 14;
                        }
                        // TODO: Checar si termina en otro mes
                        var top = (i * 60) + 2;
                        content += '' +
                            '<div class="sch-event" data-sch-event-selected="0" data-ch-id="' + e.command_housing_id + '" data-left="' + left + '" style="left: ' + left + 'px; top: ' + top + 'px; width: ' + w + 'px;">' +
                            '<span>' +
                                '<a href="' + url.replace('{id}', e.command_id)  + '" style="color: white; text-decoration: underline;" target="_blank">' +
                                    e.booking_code +
                                '</a> / ' +
                                e.start.format('DD MMM YYYY') + ' - ' + e.end.format('DD MMM YYYY') +
                            '</span>' +
                            '<span>' + e.text + ' / People: ' + e.people + '</span>' +
                            '<span>' + e.housing_room_name + '</span>' +
                            '</div>';
                    });
                });
                tpl = tpl.replace('%EVENTS%', content);
                self.ele.find('.sch-data .sch-entry-container').append(tpl);
                var elem;
                if (rows.length > 1) {
                    elem = $(self.ele.find('.sch-data .sch-entry-container .sch-entry')[index]);
                    elem.find('.sch-axis-x').height(rows.length * 60)
                        .find('table').height(rows.length * 60);
                    elem.find('.sch-axis-y').height(rows.length * 60)
                        .find('table').height(rows.length * 60);
                }
                // Poner color a las filas
                var lastRow = $(self.ele.find('.sch-data .sch-entry-container .sch-entry')[index]);
                if (lastRow) {
                    switch (r.housing_slug) {
                        case 'school_residence':
                            lastRow.css('background-color', '#F0F8FF');
                            break;
                        case 'surf_camp':
                            lastRow.css('background-color', '#E6E6FA');
                            break;
                        case 'mexican_family':
                            lastRow.css('background-color', '#FFFFE0');
                            break;
                        default:
                    }
                }
            });
            initEventListener();
            setNewHeight();
        };

        var setNewHeight = function() {
            $.each($('.sch-entry'), function(i, element) {
                $(element).find('.sch-axis-y, .sch-axis-y table').height($(element).height());
            });
        }

        var initEventListener = function() {
            // Muestra la información completa de un evento del calendario, al dar click
            $('.sch-event').on('click', function() {
                // Deseleccionar el evento anterior
                $(".sch-event[data-sch-event-selected=1]").attr('data-sch-event-selected', 0);
                // Seleccionar el nuevo evento
                $(this).attr('data-sch-event-selected', 1);
                var rowWidth = $(this).closest('.sch-axis-y').width();
                var leftPosition = $(this).data('left');
                var diff = rowWidth - leftPosition;
                var thisWidthBefore = $(this).width();
                $(this).css('left', 'auto');
                $(this).toggleClass('show'); // Agrega el nuevo tamaño
                var thisWidthAfter = $(this).width();
                if (thisWidthAfter < thisWidthBefore) {
                    // El tamaño original del evento es más grande que el nuevo tamaño, entonces no tiene caso modificarlo, solo se debe modificar en el caso contrario
                    $(this).removeClass('show');
                }
                if (diff < $(this).width()) {
                    $(this).css('left', rowWidth - $(this).width());
                } else {
                    $(this).css('left', leftPosition);
                }
            });
        };
        if (this.config.url === void 0) {
            showData();
        }
        else {
            // Mostrar loading mask
            this.ele.ploading({
                action: 'show',
                spinnerHTML: '<i></i>',
                spinnerClass: 'fa fa-spinner fa-spin p-loading-fontawesome'
            });
            $.ajax({
                url: this.config.url,
                type: 'GET',
                dataType: 'json',
                data: {
                    monthYear: this.current.month + '-' + this.current.year
                },
                success: function(r) {
                    if (r.success) {
                        self.events = r.data;
                        showData();
                    }
                    self.ele.ploading({ action: 'hide' });
                }
            });
        }
    };
    this.init = function() {
        this.ele.addClass('sch-container');
        var width = self.ele.width() - 20;
        this.ele.append('<div class="sch-navline"></div>');
        this.ele.append('<div class="sch-header" style="width: ' + width + 'px;"></div>');
        this.ele.append('<div class="sch-data"><div class="sch-entry-container" style="width: ' + width + 'px;"></div></div>');
        // Mostrar datos
        this.renderNav();
        this.renderCalendar();
        this.render();
    };
    this.render = function() {
        // Mostrar fechas
        this.ele.find('.sch-navline .sch-date').html(this.current.firstDate.format('DD MMM YYYY') + ' - ' + this.current.lastDate.format('DD MMM YYYY'));
        // Mostrar mes actual
        this.ele.find('.sch-header .sch-axis-y .sch-current-month').html(this.current.firstDate.format('MMMM YYYY'));
        // Mostrar días del mes actual
        var elem = this.ele.find('.sch-header .sch-axis-y .sch-month-days tr');
        var days = this.current.firstDate.daysInMonth();
        var extra = 'style="width: calc(100% / ' + days + ');"';
        elem.html('');
        for (var i = 1; i <= days; i++) {
            elem.append('<td ' + extra + '>' + (i + '').leftPad(2, '0') + '</td>');
        }
        self.renderData();
    };
    this.init();
};
