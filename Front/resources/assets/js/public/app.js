require("slick-carousel");
require("bootstrap-datepicker");
require("./../../libraries/select2/js/select2.min");
require("./../../../../node_modules/admin-lte/plugins/iCheck/icheck");
require("./../../../../node_modules/p-loading/dist/js/p-loading.min.js");
window.moment = require("./../../../../node_modules/moment/moment");
require("./../components/exp-spinner");
require("./../experiencia");
require('./../components/croppie-image');
require('./../components/birth-date');
require('bootstrap-timepicker');
window.swal = require('sweetalert2');

import Book from './book';
import ChooseYourDates from './choose-your-date';

/**
 * Initialize buttons using url hash
 */
window.inititalizeOnUrlHash = function() {
    var hash,
        hrefAfter,
        hrefBefore,
        $sectionStep,
        $btnResetStep,
        $btnNextStep,
        $btnBackStep;

    hash = window.location.hash;
    // Recharge page with hash if it doesn't exist
    if (hash.trim() == '') {
        window.location.hash = 'step-1';
        return false;
    }

    $sectionStep = $(hash);

    hrefAfter = $sectionStep.attr("data-after");
    hrefAfter = (hrefAfter == undefined) ? '' : hrefAfter.trim();
    hrefBefore = $sectionStep.attr("data-before");
    hrefBefore = (hrefBefore == undefined) ? '' : hrefBefore.trim();

    $btnResetStep = $("#reset-step");

    $btnNextStep = $("#next-step");
    $btnNextStep.attr("href", hrefAfter);
    if (hrefAfter == '') {
        $btnNextStep.addClass('hidden');
    } else {
        $btnNextStep.removeClass('hidden');
    }

    $btnBackStep = $("#back-step");
    $btnBackStep.attr("href", hrefBefore);
    if (hrefBefore == '') {
        $btnBackStep.addClass('hidden');
        $btnResetStep.removeClass('hidden');
    } else {
        $btnBackStep.removeClass('hidden');
        $btnResetStep.addClass('hidden');
    }
};

/**
 * Document ready
 */
$(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.birth-date').birthDate();
    $('.timepicker').timepicker({
        defaultTime: false,
        showMeridian: false,
        minuteStep: 5
    });
    $('.croppie-image').croppieImage();
    $('.slick').slick({
        dots: false,
        infinite: true,
        speed: 500,
        fade: false,
        arrows: false,
        autoplay: true
    });

    $.each($("select"), function(i, v) {
        var $el = $(v);

        $el.select2({
            minimumResultsForSearch: $el.hasClass('select2') ? 0 : -1
        }).data('select2')
            .$container
            .addClass($el.attr("class").replace("select2-hidden-accessible", ''));
    });

    // When year is changed on select for ticket flight
    $(document).on('change', '.year-onchange', function(e) {
        var $month = $(e.target.getAttribute('data-target'));
        var currentYear = parseInt(e.target.getAttribute('data-current-year'));
        var currentMonth = parseInt(e.target.getAttribute('data-current-month'));
        var months = ["January", "February", "March", "April", "May", "June", "July", "August",
            "September", "October", "November", "December"];
        var monthStart = parseInt(e.target.value) == currentYear ? currentMonth : 1;
        var tmpMonths = new Array();

        $month.select2().empty();
        for (var month = monthStart; month <= 12; month++) {
            tmpMonths.push({
                id: month,
                text: months[month - 1]
            });
        }
        $month.select2({
            minimumResultsForSearch: -1,
            data: tmpMonths
        });
    });

    $('input').iCheck({
        checkboxClass: 'icheckbox_minimal-aero',
        radioClass: 'iradio_minimal-aero',
        increaseArea: '20%' // optional
    });

    // $('.block-ask .choose-course').click(function(e) {
    //     var $self = $(e.target);
    //     if (!$self.find('input').prop('disabled')) {
    //         $($self.find('input').attr("data-target")).collapse('toggle');
    //     }
    // });
    $("input.spinner").expSpinner();
    $(".datepicker").datepicker({
        format: 'dd-mm-yyyy'
    });

    // Information popover
    $(".popover-information").popover({
        container: "body",
        placement: "bottom",
        title: "<button type='button' class='close' aria-label='Close'>&times;</button>",
        html: true
    }).on('show.bs.popover', function(e) {
        var $popover = $(e.target);
        var $blockItemDsk = $popover;

        while (!$blockItemDsk.is("body")) {
            if ($blockItemDsk.hasClass("block-item_dsk")) {
                $blockItemDsk.height('');
                $blockItemDsk.height($blockItemDsk.height() + $popover.height());
            }
            $blockItemDsk = $blockItemDsk.parent();
        }
    }).on('hidden.bs.popover', function(e) {
        $(e.target).data("bs.popover").inState.click = false;
    });
    // close Information popover
    $(document).on("click", ".close", function(e) {
        e.preventDefault();
        var $tag = $(e.target);

        while (!$tag.is("body")) {
            if ($tag.hasClass("popover")) {
                $tag.popover("hide");
            }
            $tag = $tag.parent();
        }
    });
    // close popover on click outside
    $('body').on('click', function(e) {
        var $clicked = $(e.target);
        if ($clicked.data('toggle') !== 'popover' && $clicked.parents('.popover.in').length === 0) {
            $('[data-toggle="popover"]').popover('hide');
        }
    });

    // Update data-info with select on change event
    // Info shower
    var $selectShowerAction = $('select.shower__action');
    $selectShowerAction.on('change', function(e) {
        $(this.getAttribute('data-target')).attr(
            'data-content',
            $(this).find('option:selected').attr('data-info')
        );
    });
    // info shower on initial
    $.each($selectShowerAction, function() {
        $(this.getAttribute('data-target')).attr(
            'data-content',
            $(this).find('option:selected').attr('data-info')
        );
    });
});
