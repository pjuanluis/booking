/**
 * All code here are only to control the next and back step (show and hide contents)
 */
export default class Book {
    constructor () {
        var self = this;
        //configuration DOM
        self.objects = {
            steps: $(".step"),
            btn: {
                nextStep: $("#next-step"),
                backStep: $("#back-step")
            }
        };

        //Configuration book
        self.step = {
            prefix: "step-",
            currentPos: 0,
            total: this.objects.steps.length
        };

        //Initialize
        self.detectStep();
        window.onhashchange = function() {
            self.detectStep();
        };
        self.createEvents();
    }

    changeHash(step) {
        var self = this;
        window.location.hash = self.step.prefix + step;
    }

    detectStep() {
        var self = this;
        var newStep = parseInt(
            window.location.hash.replace("#" + self.step.prefix, '')
        ) - 1;
        if (isNaN(newStep)) {
            self.changeHash((self.step.currentPos + 1));
        } else {
            self.step.currentPos = newStep;
            self.showCurrent();
        }
    }

    showCurrent() {
        var self = this;

        //Show or hide content step
        $(self.objects.steps).addClass("hidden");
        for (var i = 0; i < self.step.total; i++) {
            if (self.step.currentPos == i) {
                $(self.objects.steps[i]).removeClass("hidden");
            }
        }

        //Show or hide buttons next-step and back-step
        self.objects.btn.nextStep.removeClass("no-visible");
        self.objects.btn.backStep.removeClass("no-visible");
        if (self.step.currentPos >= self.step.total - 1) {
            self.objects.btn.nextStep.addClass("no-visible");
        } else if (self.step.currentPos <= 0) {
            self.objects.btn.backStep.addClass("no-visible");
        }
    }

    createEvents() {
        var self = this;

        // Next-step button
        self.objects.btn.nextStep.click(function(e) {
            e.preventDefault();
            self.step.currentPos++;
            self.changeHash(self.step.currentPos + 1);
        });

        // Back-step button
        self.objects.btn.backStep.click(function(e) {
            e.preventDefault();
            self.step.currentPos--;
            self.changeHash(self.step.currentPos + 1);
        });
    }
};