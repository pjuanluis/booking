// requires and imports
require("fullcalendar");
var moment = require("./../../../../node_modules/moment/moment");

// initializations
var $calendar = null;
var busyDates = [];
const today = moment(new Date());
const dateFormat = "YYYY-MM-DD";

$(window).on('hashchange', function(e) {
    init();
});

// document ready
$(function() {
    init();
});

function init() {
    var hash = window.location.hash;
    if (hash !== '#step-2') {
        return;
    }
    var iHaveDate = !$('#flight-tickets-no').is(':checked');
    var defaultMonth = parseInt($('#ticket-flight-month').val());
    var defaultYear = $('#ticket-flight-year').val();
    var defaultDate = '';

    defaultMonth = defaultMonth < 10 ? '0' + defaultMonth : defaultMonth;
    defaultDate = defaultYear + '-' + defaultMonth + '-01';

    if (iHaveDate) {
        if ($calendar !== null) {
            $calendar.hide();
        }
        $(".show-on-fligth-tickets-no").addClass('hide');
        return false;
    }

    $(".show-on-fligth-tickets-no").removeClass('hide');
    if ($calendar === null) {
        $calendar =  $(".choose-your-dates");
        inititalizeCalendar(defaultDate);
        $calendar.find('.fc-toolbar .fc-left').prepend(
            $('<button>', {
                "type": "button",
                "class": "fc-button fc-state-default",
                "text": "Clear selection"
            }).on('click', function() {
                $calendar.fullCalendar("removeEvents");
                EXP.selected.from = null;
                EXP.selected.to = null;
            })
        );
    } else {
        $calendar.show();
        if ($calendar.is(':visible')) {
            if ($calendar.fullCalendar('getView').intervalStart.format('YYYY-MM-DD') == defaultDate) {
                renderOccupancy($calendar.fullCalendar('getView'));
            }
            else {
                $calendar.fullCalendar('gotoDate', moment(defaultDate));
            }
        }
    }
}

/**
 * Initialize calendar
 */
function inititalizeCalendar(defDate) {
    $calendar.fullCalendar({
        defaultDate: moment(defDate),
        defaultView: "month",
        editable: true,
        selectable: true,
        fixedWeekCount: false,
        aspectRatio: 1.85,
        validRange: {
            start: '2017-10-01',
            end: '2017-11-01'
        },
        header: {
            left: "",
            center: 'title',
            right: 'prev today, next'
        },
        columnFormat: {
            month: 'ddd'
        },
        viewRender: function(currentView) {
            renderOccupancy(currentView);
        },
        dayRender: function(date, cell) {
            cell.html(
                $("<span>", {
                    "class": "fc-date-name--top",
                    "html": date.format("DD MMM YYYY")
                })
            );
            // ignore if day is older than today(current day)
            if (date.diff(today) <= 0) {
                return false;
            }
            // read tag attributes and mark day as free or bussy
            markDayAs(date, cell);
        },
        select: function (start, end, jsEvent, view) {
            // Only select days bigger than today and non busy dates
            var tmp, id, days = 0;
            try {
                if(start.diff(today) < 0) {
                    throw "Selection before than today";
                }
                for (var i = 0; i < busyDates.length; i++) {
                    if (moment(busyDates[i]).isBetween(start, end)) {
                        throw "Selection of invalid date";
                    }
                }
                for (id in EXP.selected.housing) {
                    tmp = EXP.selected.housing[id].unit === 'week' ? 7 : 1;
                    days += (EXP.selected.housing[id].quantity * tmp);
                }
                tmp = moment(start.format('YYYY-MM-DD'));
                if (tmp.add(days, 'day').isAfter(end)) {
                    alert("The range of dates must be " + (days + 1) + " days");
                    return;
                }
                EXP.selected.from = start.format('YYYY-MM-DD');
                EXP.selected.to = end.format('YYYY-MM-DD');
                $calendar.fullCalendar('removeEvents');
                renderDate(start, end);
            } catch(err) {
                $calendar.fullCalendar('unselect');
                alert("An unavailable date was selected: " + err + ".");
            }
            $calendar.fullCalendar('unselect');
        },
        selectOverlap: false
    });
}

function renderOccupancy(currentView) {
    var id = 0;
    for (id in EXP.selected.housing) {
        if (!EXP.selected.housing.hasOwnProperty(id)) {
            id = 0;
        }
    }
    // Obtener ocupacion del tipo de cuarto seleccionado en el mes actual
    EXP.request.get(
        '/housing/occupancy?month=' + currentView.intervalStart.format('MM-YYYY') + '&housing_room_id=' + EXP.selected.housing[id].housing_room_id,
        function(r) {
            var i;
            var dates = [];
            for (i = 0; i < r.data.length; i++) {
                if (r.data[i].is_full) {
                    dates.push(r.data[i].date);
                }
            }
            busyDates = dates;
            var date = currentView.start.clone();
            // Marcar dias segun ocupación
            var cells = $calendar.find('.fc-view-container .fc-widget-content .fc-day-grid-container .fc-day');
            for (i = 0; i < cells.length; i++) {
                markDayAs(date, $(cells[i]));
                date.add(1, 'day');
            }
        }
    );
    disableMovMonthBtn(currentView);
}

/**
 * Disable back button when month is less than current month
 * @param {*} currentView
 */
function disableMovMonthBtn(currentView) {
    var $prevMonthBtn = $(".fc-prev-button");
    var $nextMonthBtn = $(".fc-next-button");
    var disabled = false;

    // Disable prev
    disabled = (currentView.start.diff(today) < 0) && (currentView.end.diff(today) > 0);
    $prevMonthBtn.prop('disabled', disabled);
    if (disabled) {
        $prevMonthBtn.addClass('fc-state-disabled');
    } else {
        $prevMonthBtn.removeClass('fc-state-disabled');
    }

    // Disable next
    disabled = (currentView.start.diff(today.clone().add(2, 'years')) < 0) &&
            (currentView.end.diff(today.clone().add(2, 'years')) > 0);
    $nextMonthBtn.prop('disabled', disabled);
    if (disabled) {
        $nextMonthBtn.addClass('fc-state-disabled');
    } else {
        $nextMonthBtn.removeClass('fc-state-disabled');
    }
}

/**
 * Mark the day as busy or free, with an icon.
 * @param {*moment} date
 * @param {*jQuery} cell
 */
function markDayAs(date, cell) {
    var icon = busyDates.indexOf(date.format(dateFormat)) != -1 ? "fa-times" : "fa-check";
    cell.addClass('disabled');
    cell.find('i.fc__icon').remove();
    cell.append($("<i>", {
        "class": "fc__icon fa " + icon,
        "aria-hidden": "true"
    }));
}

/**
 * Render a date
 * @param {*moment} start
 * @param {*moment} end
 */
function renderDate(start, end) {
    $calendar.fullCalendar('renderEvent', {
        start: start,
        end: end,
        color: "blue",
        rendering: 'background',
        block: true
    }, true);
}

export default $calendar;
