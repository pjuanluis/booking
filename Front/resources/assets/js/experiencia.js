/* global validator, moment, swal */

if (window.EXP === void 0) {
    window.EXP = {
        packages: {},
        housing: {}
    };
}

EXP.Grid = function(container, config) {
    this.container = container;
    this.config = config;
    this.genId = function() {
        return 'cmp-' + Math.floor(Math.random() * 10000);
    };
    if (this.config.readOnly === void 0) {
        this.config.readOnly = false;
    }
    this.id = this.genId();
    this.fields = this.config.fields;
    this.data = this.config.data;
    this.columns = this.config.columns;
    this.ele = null;
    this.template = '' +
        '<div id="%ID%">' +
        (!this.config.readOnly ?
            '<div class="btn-toolbar" role="toolbar" aria-label="...">' +
            '<div class="btn-group pull-right" role="group" aria-label="..."></div>' +
            '</div>' : '') +
        '<div class="box-body table-responsive no-padding">' +
        '<table class="table table-striped table-hover">' +
        '<thead>' +
        '<tr></tr>' +
        '</thead>' +
        '<tbody></tbody>' +
        '</table>' +
        '</div>' +
        '</div>';
    this.html = function() {
        return this.template.replace('%ID%', this.id);
    };
    this.render = function() {
        var grid = this;
        this.container.html(this.html());
        if (this.columns === void 0) {
            return;
        }
        var items = '';
        // Agregar toolbar
        var e = $('#' + this.id + ' .btn-toolbar > .btn-group');
        if (this.config.toolbar !== void 0) {
            // Renderizar botones del toolbar
            _.each(this.config.toolbar, function(btn) {
                items += '<button type="button" class="btn btn-default">' + btn.text + '</button>';
            });
            $(e).html(items);
            // Inicializar acciones de los botones
            $(e).find('button').each(function(i) {
                $(this).click(function() {
                    if (grid.config.toolbar[i].handler !== void 0) {
                        if (_.isFunction(grid.config.toolbar[i].handler)) {
                            grid.config.toolbar[i].handler(grid);
                        }
                    }
                });
            });
        }
        // Agregar encabezado de la tabla
        e = $('#' + this.id + ' table.table > thead > tr');
        $(e).html('');
        var columns = [];
        items = '';
        _.each(this.columns, function(c) {
            if (c.field === 'actionColumn' && grid.config.readOnly) {
                return;
            }
            items += '<th ' + (c.width !== void 0 ? ('width="' + c.width + '"') : '') + '>' + c.title + '</th>';
            columns.push(c);
        });
        this.columns = columns;
        $(e).html(items);
        // Mostrar datos
        e = $('#' + this.id + ' table.table > tbody');
        $(e).html('');
        items = '';
        var buttons = [];
        _.each(grid.data, function(r) {
            items += '<tr data-id="' + (r.id ? r.id : '0') + '">';
            _.each(grid.columns, function(c) {
                var value = r[c.field];
                if (c.field !== 'actionColumn') {
                    if (value === null) {
                        value = '';
                    }
                    else if (c.renderer !== void 0) {
                        if (_.isFunction(c.renderer)) {
                            value = c.renderer(value);
                        }
                    }
                    items += '<td>' + value + '</td>';
                }
                // Agregar botones
                else {
                    if (!grid.config.readOnly) {
                        items += '<td>';
                        buttons = c.buttons;
                        _.each(c.buttons, function(btn) {
                            items += '<button type="button" class="action-btn"><i class="fa ' + btn.icon + '" aria-hidden="true"></i></button>';
                        });
                        items += '</td>';
                    }
                }
            });
            items += '</tr>';
            if (r.nested !== void 0 && grid.config.nested !== void 0) {
                items += '<tr><td colspan="4">';
                _.each(grid.config.nested, function(n) {
                    items += '<div><strong>' + n.title + '</strong>: ' + r.nested[n.field] + '</div>';
                });
                items += '</td></tr>';
            }
        });
        $(e).html(items);
        // Inicializar botones de acción del grid
        _.each(buttons, function(b) {
            $('#' + grid.id).find('.' + b.icon).each(function(i) {
                $(this).click(function() {
                    if (b.handler !== void 0) {
                        if (_.isFunction(b.handler)) {
                            b.handler(grid, i);
                        }
                    }
                });
            });
        });
        this.ele = $('#' + this.id);
        if (this.config.onAfterRender !== void 0) {
            if (_.isFunction(this.config.onAfterRender)) {
                this.config.onAfterRender(this);
            }
        }
    };
    this.add = function(data) {
        this.data.push(data);
        this.render();
        EXP.paymentObject.calculateTotals();
    };
    this.set = function(index, data) {
        this.data[index] = data;
        this.render();
        EXP.paymentObject.calculateTotals();
    };
    this.remove = function(index) {
        this.data.splice(index, 1);
        this.render();
        EXP.paymentObject.calculateTotals();
    };
    this.removeAll = function() {
        this.data = [];
        this.render();
        EXP.paymentObject.calculateTotals();
    };
    this.findColumnIndexByFieldName = function(name) {
        var i;
        for (i = 0; i < this.columns.length; i++) {
            if (this.columns[i].field === name) {
                return i;
            }
        }
        return -1;
    };
    this.getData = function() {
        var data = [];
        var self = this;
        _.each(this.data, function(d) {
            r = {};
            _.each(self.fields, function(field) {
                r[field] = d[field];
            });
            data.push(r);
        });
        return data;
    };
    this.render();
};

EXP.toFloat = function(number) {
    var n = 0;
    n = parseFloat(number);
    if (isNaN(n)) {
        n = 0;
    }
    else if (!isFinite(n)) {
        n = 0;
    }
    return n;
};
EXP.toInteger = function(number) {
    var n = 0;
    n = parseInt(number);
    if (isNaN(n)) {
        n = 0;
    }
    else if (!isFinite(n)) {
        n = 0;
    }
    return n;
};

EXP.toDate = function(date, inputFormat, outputFormat) {
    if (validator.isEmpty(date)) {
        return '';
    }
    date = moment(date, validator.isEmpty(inputFormat) ? 'YYYY-MM-DD' : inputFormat, true);
    return date.isValid() ? date.format(validator.isEmpty(outputFormat) ? 'YYYY-MM-DD' : outputFormat) : '';
};

EXP.isValidForm = function(form) {
    // Obtener campos del formulario
    var fields = form.find('select,input[type=text],input[type=email],input[type=number],textarea');
    var hasErrors = false;
    // Mostrar mensaje de error
    var showErrorMsg = function($ele, rule) {
        // Agregar clase has-error y obtener los elementos de los mensajes
        var items = $ele.closest('.form-group').addClass('has-error').find('.help-block > ul > li');
        var i;
        // Buscar mensaje del rule y mostrarlo
        for (i = 0; i < items.length; i++) {
            if (items[i].getAttribute('rule') === rule) {
                $(items[i]).show();
            }
        }
        hasErrors = true;
    };
    // Ocultar mensaje de eror
    var hideErrorMsg = function($ele, rule) {
        // Obtener elementos de los mensajes
        var items = $ele.closest('.form-group').find('.help-block > ul > li');
        var i;
        var visibles = 0;
        // Buscar mensaje del rule y ocultarlo
        for (i = 0; i < items.length; i++) {
            if (items[i].getAttribute('rule') === rule) {
                $(items[i]).hide();
            }
            if ($(items[i]).is(':visible')) {
                visibles++;
            }
        }
        // Si todos los mensajes de error están ocultos
        if (visibles === 0) {
            // Remover clase has-error
            $ele.closest('.form-group').removeClass('has-error');
        }
    };
    // Buscar campo segun su nombre
    var findFieldByName = function(name) {
        var i;
        for (i = 0; i < fields.length; i++) {
            if (fields[i].getAttribute('name') === name) {
                return fields[i];
            }
        }
        return null;
    };
    // Validar los campos del formulario
    _.each(fields, function(ele) {
        var $ele = $(ele);
        var rules;
        var type;
        var param;
        var paramValue;
        var i;
        // Validar rule required
        if (ele.hasAttribute('required')) {
            if (validator.isEmpty(ele.value)) {
                showErrorMsg($ele, 'required');
            }
            else {
                hideErrorMsg($ele, 'required');
            }
        }
        // Validar input
        if (ele.hasAttribute('type')) {
            type = ele.getAttribute('type');
            // Validar input number
            if (type === 'number') {
                // Si existe el atributo rule
                if (ele.hasAttribute('rule')) {
                    // Obtener rules del campo
                    rules = ele.getAttribute('rule').split('|');
                    _.each(rules, function(rule) {
                        // Validar rule integer
                        if (rule === 'integer') {
                            if (!validator.isInt(ele.value)) {
                                showErrorMsg($ele, 'integer');
                            }
                            else {
                                hideErrorMsg($ele, 'integer');
                            }
                        }
                        // Validar rule min
                        else if (rule.indexOf('min') === 0) {
                            // Obtener valor minimo
                            paramValue = rule.split(':')[1];
                            if (!validator.isInt(ele.value, { min: paramValue })) {
                                showErrorMsg($ele, 'min');
                            }
                            else {
                                hideErrorMsg($ele, 'min');
                            }
                        }

                    });
                }
                // Validar rule numeric
                else {
                    if (!validator.isDecimal(ele.value)) {
                        showErrorMsg($ele, 'numeric');
                    }
                    else {
                        hideErrorMsg($ele, 'numeric');
                    }
                }
            }
        }
        var dateFormat = 'DD-MM-YYYY';
        if (ele.hasAttribute('rule')) {
            // Obtener rules del campo
            rules = ele.getAttribute('rule').split('|');
            _.each(rules, function(rule) {
                // Validar rule date
                if (rule === 'date') {
                    if (!moment(ele.value, dateFormat, true).isValid()) {
                        showErrorMsg($ele, 'date');
                    }
                    else {
                        hideErrorMsg($ele, 'date');
                    }
                }
                // Validar rule date_format
                if (rule.indexOf('date_format') === 0) {
                    // Obtener formato
                    paramValue = rule.substr(rule.indexOf(':') + 1);
                    if (paramValue === '') {
                        showErrorMsg($ele, 'date_format');
                    }
                    else if (!moment(ele.value, paramValue, true).isValid()) {
                        showErrorMsg($ele, 'date_format');
                    }
                    else {
                        hideErrorMsg($ele, 'date_format');
                    }
                }
                // Validar rule after
                if (rule.indexOf('after') === 0) {
                    // Obtener dato contra el cual validar el rule
                    param = rule.split(':')[1];
                    paramValue = findFieldByName(param);
                    if (paramValue !== null) {
                        paramValue = paramValue.value;
                        // Buscar formato
                        for (i = 0; i < rules.length; i++) {
                            if (rules[i].indexOf('date_format:') === 0) {
                                dateFormat = rules[i].substr(rules[i].indexOf(':') + 1);
                                break;
                            }
                        }
                        if (paramValue === '') {
                            showErrorMsg($ele, 'after');
                        }
                        else if (!moment(ele.value, dateFormat, true).isSameOrAfter(moment(paramValue, dateFormat, true))) {
                            showErrorMsg($ele, 'after');
                        }
                        else {
                            hideErrorMsg($ele, 'after');
                        }
                    }
                }
            });
        }
    });
    return !hasErrors;
};

EXP.packages.findById = function(id) {
    var c = _.find(EXP.packages.data, function(c) {
        return c.id == id;
    });
    if (c === void 0) {
        return null;
    }
    return c;
};
EXP.packages.findByExperiences = function(experiences) {
    var i = j = 0;
    var exists = false;
    var packages = [];
    _.each(EXP.packages.data, function(p) {
        if (p.experiences.data.length != experiences.length) {
            return;
        }
        for (i = 0; i < experiences.length; i++) {
            exists = false;
            for (j = 0; j < p.experiences.data.length; j++) {
                if (experiences[i] == p.experiences.data[j].slug) {
                    exists = true;
                    break;
                }
            }
            if (!exists) {
                break;
            }
        }
        if (exists) {
            packages.push(p);
        }
    });
    return packages;
};
EXP.packages.findPackageRate = function(pack, quantity) {
    var rate;
    for (var i = 0; i < pack.rates.data.length; i++) {
        rate = pack.rates.data[i];
        if (rate.to_quantity !== null) {
            if (quantity >= rate.quantity && quantity <= rate.to_quantity) {
                return rate;
            }
        }
        else if (quantity >= rate.quantity) {
            return rate;
        }
    }
    return null;
};

EXP.housing.findById = function(id) {
    var h = _.find(EXP.housing.data, function(h) {
        return h.id == id;
    });
    if (h === void 0) {
        return null;
    }
    return h;
};
EXP.housing.findHousingRoomById = function(housingId, id) {
    var housing = EXP.housing.findById(housingId);
    if (housing === null) {
        return null;
    }
    var r = _.find(housing.housingRooms.data, function(r) {
        return r.id == id;
    });
    if (r === void 0) {
        return null;
    }
    return r;
};
EXP.housing.findPackageById = function(housingId, housingRoomId, id) {
    var housingRoom = EXP.housing.findHousingRoomById(housingId, housingRoomId);
    if (housingRoom === null) {
        return null;
    }
    var p = _.find(housingRoom.packages.data, function(p) {
        return p.id == id;
    });
    if (p === void 0) {
        return null;
    }
    return p;
};
EXP.housing.getPackageUnits = function(pack) {
    var units = [];
    _.each(pack.rates.data, function(r) {
        var u = _.find(units, function(u) {
            return u.value === r.unit;
        });
        if (u === void 0) {
            units.push({
                name: r.unit.charAt(0).toUpperCase() + r.unit.substr(1),
                value: r.unit
            });
        }
    });
    if (units.length === 0) {
        units.push({ name: 'Week', value: 'week' });
    }
    return units;
};
EXP.housing.findPackageRate = function(pack, date, unit) {
    var rate;
    for (var i = 0; i < pack.rates.data.length; i++) {
        rate = pack.rates.data[i];
        if (rate.unit === unit) {
            if (rate.from_date !== null) {
                if (moment(date).isSameOrAfter(moment(rate.from_date)) && moment(date).isSameOrBefore(moment(rate.to_date))) {
                    return rate;
                }
            }
        }
    }
    for (var i = 0; i < pack.rates.data.length; i++) {
        rate = pack.rates.data[i];
        if (rate.unit === unit) {
            if (rate.from_date === null) {
                return rate;
            }
        }
    }
    return null;
};

EXP.initConfirmButton = function(config) {
    $(config.selector).click(function(e) {
        e.preventDefault();
        if (config.validator !== void 0) {
            if (_.isFunction(config.validator)) {
                if (!config.validator()) {
                    return false;
                }
            }
        }
        var btn = $(this);
        swal({
            title: _.isFunction(config.title) ? config.title(btn) : config.title,
            text: _.isFunction(config.msg) ? config.msg(btn) : config.msg,
            type: config.type === void 0 ? 'warning' : config.type,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, do it!'
        }).then(function() {
            config.callback(btn);
        }).catch(swal.noop);
    });
};

EXP.initAutocomplete = function(ele, config) {
    $(ele).select2({
        disabled: config.disabled !== void 0 ? config.disabled : false,
        ajax: {
            url: config.url,
            dataType: 'json',
            delay: 250,
            data: function(params) {
                var data = {
                    page: params.page
                };
                data[config.paramName] = params.term;
                return data;
            },
            processResults: function(data, params) {
                params.page = params.page || 1;
                return {
                    results: data.data,
                    pagination: {
                        more: (params.page * 20) < data.meta.pagination.total
                    }
                };
            },
            cache: true
        },
        escapeMarkup: function(markup) {
            return markup;
        },
        minimumInputLength: 1,
        templateResult: function(repo) {
            if (repo.loading) {
                return repo.text;
            }
            return repo[config.displayField];
        },
        templateSelection: function(repo) {
            return repo[config.displayField] || repo.text;
        }
    });
};

EXP.request = {
    get: function(url, onSuccess, onError) {
        $.ajax({
            async: true,
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function(r) {
                if (onSuccess !== void 0) {
                    if (_.isFunction(onSuccess)) {
                        onSuccess(r);
                    }
                }
            },
            error: function(r) {
                if (onError !== void 0) {
                    if (_.isFunction(onError)) {
                        onError(r);
                    }
                }
            }
        });
    },
    post: function(url, data, onSuccess, onError) {
        $.ajax({
            async: true,
            url: url,
            data: data,
            type: 'POST',
            dataType: 'json',
            success: function(r) {
                if (onSuccess !== void 0) {
                    if (_.isFunction(onSuccess)) {
                        onSuccess(r);
                    }
                }
            },
            error: function(r) {
                if (onError !== void 0) {
                    if (_.isFunction(onError)) {
                        onError(r);
                    }
                }
            }
        });
    }
};

String.prototype.leftPad = function(padLength, padStr) {
    var str = '';
    for (var i = 0; i < padLength; i++) {
        str += padStr;
    }
    str += this;
    return str.slice(-(padStr.length * padLength));
};

EXP.initICheck = function(selector) {
    $(selector).iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });
}
