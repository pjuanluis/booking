$.fn.expCheckbox = function() {
    return this.each(function() {
        var $self,
            checkboxId;
        
        $self = $(this);
        checkboxId = $self.attr("id");
        if (checkboxId == undefined || checkboxId.trim() == '') {
            checkboxId = createId();
            $self.attr("id", checkboxId);
        }

        eventListener(
            $self,
            createHtml($self, checkboxId)
        );

        $self.addClass("hidden");
    });

    /**
     * Generate an id of 5 chars;
     */
    function createId() {
        var id = "";
        var source = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        var sourceSize = source.length;

        for (var i = 0; i < 5; i++) {
            id += source.charAt(Math.floor(Math.random() * sourceSize));
        }

        return id;
    }

    /**
     * Get class from element
     * @param {*element} JQuery object  to get class
     */
    function getClasses($element) {
        var clas = $element.attr("class");
        return clas == undefined ? '' : clas;
    }

    /**
     * Create the html span to simulate the new checkbox
     * @param {*} $checkbox JQuery object tomanipulate  
     * @param {*} id Identifier 
     */
    function createHtml($checkbox, id) {
        var clas,
            $newCheckbox;

        clas = getClasses($checkbox) + " exp-checkbox";
        if ($checkbox.is(":checked")) {
            clas += " active";
        }

        $newCheckbox = $("<span>", {
            class: clas.trim(),
            id: "exp-" +id
        });
        
        $checkbox.after(
            $newCheckbox
        );
        
        return $newCheckbox;
    }

    /**
     * Add events listener
     * @param {*} checkbox JQuery object  
     * @param {*}  customCheckbox the span on JQuery object
     */
    function eventListener($checkbox, $customCheckbox) {
        $checkbox.change(function() {
            $customCheckbox.removeClass("active");
            if ($checkbox.is(":checked")) {
                $customCheckbox.addClass("active");
            }
        });
    }
};