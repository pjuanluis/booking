var moment = require('moment');

$.fn.birthDate = function() {
    var $self,
        $year,
        $moth,
        $day,
        $birthDate;

    return this.each(function() {
        $self = $(this);
        $year = $self.find('.year');
        $month = $self.find('.month');
        $day = $self.find('.day');
        $birthDate = $self.find('#birth-date');

        $year.append($('<option>').val('').html('Year'));
        for (i = new Date().getFullYear(); i > 1900; i--) {
            $year.append($('<option>').val(i).html(i));
        }
        $month.append($('<option>').val('').html('Month'));
        for (i = 1; i < 13; i++){
            $month.append($('<option>').val(i).html(i < 10 ? '0' + i : i));
        }

        var birthDate = $birthDate.val();
        var year,
            month,
            day;
        if (birthDate) {
            year = parseInt(moment(birthDate).format("YYYY"));
            month = parseInt(moment(birthDate).format("MM"));
            day = parseInt(moment(birthDate).format("DD"));
        }

        $year.val('');
        $year.find('option[value="'+ year +'"]').prop('selected', true);
        if ($year.val() == null) {
            $year.find('option').first().prop('selected', true);
        }
        $year.trigger('change.select2');

        $month.val('');
        $month.find('option[value="'+ month +'"]').prop('selected', true);
        if ($month.val() == null) {
            $month.find('option').first().prop('selected', true);
        }
        $month.trigger('change.select2');

        updateNumberOfDays(); 
        $day.find('option[value="'+ day +'"]').prop('selected', true);
        if ($day.val() == null) {
            $day.find('option').first().prop('selected', true);
        }

        $self.find('.year, .month').change(function(){
            updateNumberOfDays(); 
        });

        // update #birth-date:hidden
        $self.find('.year, .month, .day').change(function(){
            var year = $year.val() || '';
            var month = $month.val() || '';
            var day = $day.val() || '';
            $birthDate.val(year + '-' + (month < 10 ? '0' + month : month) + '-' + (day < 10 ? '0' + day : day));
        });
    });
    
    function updateNumberOfDays(){
        $day.html('');
        month = $month.val();
        year = $year.val();
        days = daysInMonth(month, year);
    
        $day.append($('<option>').val('').html('Day'));
        for(i=1; i < days+1 ; i++){
            $day.append($('<option>').val(i).html(i < 10 ? '0' + i : i));
        }

        $day.val('');
        $day.find('option').first().prop('selected', true);
        $day.trigger('change.select2');
    }
    
    function daysInMonth(month, year) {
        return new Date(year, month, 0).getDate();
    }
}