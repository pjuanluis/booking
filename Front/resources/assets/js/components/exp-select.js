$.fn.expSelect = function() {
    return this.each(function() {
        var $currentSelect = $(this);
        var selectId = $currentSelect.attr("id");

        selectId = (selectId == undefined  || selectId.trim()) ? createId() : selectId;
        $currentSelect.attr("id", selectId);
        
        addEvents(
            $currentSelect,
            createHtml($currentSelect, selectId)
        );

        $currentSelect.addClass("hidden");
    });

    /**
     * Generate an id of 5 chars;
     */
    function createId() {
        var id = "";
        var source = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        var sourceSize = source.length;

        for (var i = 0; i < 5; i++) {
            id += source.charAt(Math.floor(Math.random() * sourceSize));
        }

        return id;
    }

    function createHtml(select, id) {
        var $optionTmp,
            $liTmp,
            $newSelect,
            $options,
            $ul,
            // dataInfo,
            selectClass;
        
        selectClass = select.attr("class");
        if (selectClass == undefined) {
            selectClass = '';
        }
        $options = select.find("option");
        $ul = $("<ul>", {
            class: "options-list"
        });

        // Create all options to <li>
        for (var i=0; i < $options.length; i++) {
            $optionTmp = $($options[i]);
            $liTmp = $("<li>", {
                "class": "option",
                "data-value": $optionTmp.val(),
            }).append(
                $("<span>", {
                    "class": "text",
                    "text": $optionTmp.text()
                })
            );
            
            // dataInfo = $optionTmp.attr("data-info");
            // if (dataInfo != undefined &&  dataInfo != null && dataInfo.trim() != '') {
            //     $liTmp.append(
            //         $("<button>", {
            //             "type": "button",
            //             "class": "btn btn-info",
            //             "text": "Info",
            //             "data-info": dataInfo
            //         })
            //     );
            // }
            $ul.append($liTmp);
        }

      $newSelect =  $("<div>", {
            "class":  selectClass == '' ? "custom-select" : selectClass + " " + "custom-select",
            "data-select-id": id
        }).append(
            $("<span>", {
                class: "current-value",
                text: select.find("option:selected").text()
            })
        ).append($ul);

        $(select).after($newSelect);

        return $newSelect;
    }

    /**
     * Action listener
     * @param {*} select the current select.
     * @param {*} customSelect the new select, simulated with html.
     */
    function addEvents(select, customSelect) {
        var customOptions = customSelect.find(".option");

        // Toggle options list on custom-select clicked.
        customSelect.find(".options-list").toggle();
        customSelect.click(function(e) {
            e.preventDefault();
            customSelect.find(".options-list").toggle();
        });

        // Clicked over an option
        customOptions.click(function(e) {
            var $self;

            e.preventDefault();
            $self = $(this);
            customOptions.removeClass('selected');
            $self.addClass("selected");
            select.val($self.attr("data-value"));
            customSelect.find(".current-value").text($self.find(".text").text());
        });

        // Clicked on info .btn-info element
        customOptions.find(".btn-info").click(function(e) {
            var $card = customSelect.parent().find(".card");
            
            e.preventDefault();
            $card.find(".card-body p").text(
                this.getAttribute("data-info")
            );
            $card.addClass("show");
            return false;
        });
    }
};