const croppie = require("./../../../../node_modules/croppie/croppie");

const clas = {
    view: '.croppie-image__view',
    btnUpload: '.croppie-image__upload',
    btnCancel: '.croppie-image__cancel',
    inputFile: '.croppie-image__file',
    inputPhotoData: '.photo_data',
    inputPhotoName: '.photo_name'
};

$.fn.croppieImage = function() {
    var $self,
        $view,
        $croppie,
        $inputFile;
    var fileSelected = false;

    return this.each(function() {
        $self = $(this);
        initialize();
        initializeEvents();
    });

    /**
     * Initialize croppie
     */
    function initialize() {
        $inputFile = $self.find(clas.inputFile);
        $view = $self.find(clas.view);

        $croppie = $view.croppie({
            enableExif: true,
            showZoomer: false,
            // enableZoom: false,
            viewport: {
                width: 200,
                height: 200
            },
            boundary: {
                width: $view.width,
                height: $view.outerHeight(true)
            }
        });
    }

    /**
     * initialize events
     */
    function initializeEvents() {
        // Click upload button
        $self.find(clas.btnUpload).click(function(e) {
            e.preventDefault();
            $inputFile.trigger('click');
        });

        // Click cancel button
        $self.find(clas.btnCancel).click(function(e) {
            e.preventDefault();
            clearFile();
        });

        // Change input file
        $inputFile.change(function(e) {
            readFile(e.target);
            fileSelected = true;
        });

        // Submit
        $('form').submit(function(e) {
            var $form = $(this);
            // Validar seleccion de imagen
            if ($inputFile[0].files.length > 0) {
                $croppie.croppie('result', {size: {width: 400, height: 400}}).then(function(img) {
                    if (fileSelected) {
                        $self.find(clas.inputPhotoData).val(img);
                        $form.get(0).submit();
                    }
                });
            }
            return false;
        });

        // Reset form
        $self.closest("form").on('reset', function() {
            clearFile();
        });
    }

    /**
     * Read and show file
     * @param {*} input input tag
     */
    function readFile(input) {
        if (input.files && input.files[0]) {
          var reader = new FileReader();
      
          reader.onload = function (e) {
            $croppie.croppie('bind', {
              url: e.target.result
            });
          };
          reader.readAsDataURL(input.files[0]);

          $self.find(clas.inputPhotoName).val(input.files[0].name);
        }
    }

    /**
     * Clear file from croppie
     */
    function clearFile() {
        $inputFile.val('');
        $self.find(clas.inputPhotoName).val('');
        $self.find(clas.inputPhotoData).val('');

        $croppie.croppie('bind', {
            url: ''
        });
    }
};