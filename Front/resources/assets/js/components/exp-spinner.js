$.fn.expSpinner = function() {
    return this.each(function() {
        var $self,
            spinnerId;
        
        $self = $(this);
        $self.val(
            ($self.val() == undefined || $self.val().trim() == '') ? 0 : $self.val()
        )
        spinnerId = $self.attr("id");
        if (spinnerId == undefined || spinnerId.trim() == '') {
            spinnerId = createId();
            $self.attr("id", spinnerId);
        }

        eventListener(
            $self,
            createHtml($self, spinnerId)
        );

        $self.addClass("hidden");
        $self.trigger("change");
    });

    /**
     * Generate an id of 5 chars;
     */
    function createId() {
        var id = "";
        var source = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        var sourceSize = source.length;

        for (var i = 0; i < 5; i++) {
            id += source.charAt(Math.floor(Math.random() * sourceSize));
        }

        return id;
    }

    /**
     * Get class from element
     * @param {*element} JQuery object  to get class
     */
    function getClasses($element) {
        var clas = $element.attr("class");
        return clas == undefined ? '' : clas;
    }

    /**
     * Create the html span to simulate the new checkbox
     * @param {*} $spinner JQuery object tomanipulate  
     * @param {*} id Identifier 
     */
    function createHtml($spinner, id) {
        var clas,
            $newSpinner,
            $value,
            $plusButton,
            $minusButton;

        clas = getClasses($spinner) + " exp-spinner";

        $value = $("<span>", {
            class: "value",
        });

        $plusButton = $('<button>', {
            type: "button",
            class: "btn btn-default plus"
        }).append(
            $("<span>", {
                "class": "glyphicon glyphicon-plus",
                "aria-hidden": "true"
            })
        );

        $minusButton = $('<button>', {
            type: "button",
            class: "btn btn-default minus"
        }).append(
            $("<span>", {
                "class": "glyphicon glyphicon-minus",
                "aria-hidden": "true"
            })
        );

        $newSpinner = $("<span>", {
            class: clas.trim(),
            id: "exp-" + id
        })
        .append(
            $('<div>', {
                class: "buttons"
            })
            .append($plusButton)
            .append($minusButton)
        )
        .append($value);
        
        $spinner.after(
            $newSpinner
        );
        
        return $newSpinner;
    }

    /**
     * Add events listener
     * @param {*} spinner JQuery object  
     * @param {*}  customspinner the span on JQuery object
     */
    function eventListener($spinner, $customSpinner) {
        $spinner.change(function() {
            var staticText = $spinner.attr("data-static-text");

            staticText = (staticText == undefined) ? '' : ' ' + staticText;
            $($customSpinner).find(".value").text(
                $spinner.val() + staticText
            );
        });

        // Plus button
        $customSpinner.find(".plus").click(function(e) {
            var value = parseInt($spinner.val());

            e.preventDefault();
            $spinner.val(value + 1);

           $spinner.trigger("change"); 
        });

        // Minus button
        $customSpinner.find(".minus").click(function(e) {
            var value = parseInt($spinner.val());

            e.preventDefault();
            $spinner.val(value - 1);
            
           $spinner.trigger("change");
        });
    }
};