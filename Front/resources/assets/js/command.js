
/* global moment */

EXP.command = {
    courses: {},
    housing: {},
    purchases: {},
    payments: {},
    allPayments: {},
    getExchangeRates: function(currencyID, currentExchangeRates) {
        if (currentExchangeRates.length > 0 && currencyID == currentExchangeRates[0].base_currency_id) {
            // currentExchangeRates es lo que regresó el API, y no cambia hasta que se guarde el nuevo exchange rates
            return currentExchangeRates;
        }
        var currency = EXP.currencies.find(function(c) {
            return c.id == currencyID;
        });
        var exchangeRates = [];
        if (!currency) {
            return exchangeRates;
        }
        var exchangeRatesTmp = currency.exchangeRates ? currency.exchangeRates.data : [];
        for (var i = 0; i < exchangeRatesTmp.length; i++) {
            exchangeRates.push({
                amount: parseFloat(exchangeRatesTmp[i].amount),
                base_amount: exchangeRatesTmp[i].base_amount,
                to_currency_id: exchangeRatesTmp[i].exchangeCurrency.data.id,
                base_currency_id: currency.id,
            });
        }
        return exchangeRates;
    },
};

EXP.CommandComponent = function(data, config) {
    this.config = config;
    this.ele = $(config.containerSelector);
    this.window = {
        ele: $(config.modalSelector),
        form: {
            ele: $(config.modalSelector + ' form'),
            action: 'add',
            clear: function() { },
            setData: function() { },
            getData: function() { },
            isValid: function() {
                return EXP.isValidForm(this.ele);
            }
        }
    },
        this.edit = function(index) {
            this.window.form.setData(this.items[index].getData());
            this.window.ele.modal('show');
            this.selectedIndex = index;
        };
    this.getData = function() {
        return this.grid.getData();
    };
    this.config.init(this, data, this.config);
};

EXP.command.courses.initComponent = function(cmp, data, config) {
    cmp.grid = new EXP.Grid($(config.gridSelector), {
        fields: [
            'id', 'experiences', 'experience_id', 'package', 'package_id', 'quantity',
            'cost', 'currency', 'currency_id', 'discount', 'from_date', 'to_date', 'exchange_rates', 'current_exchange_rates'
        ],
        data: data,
        columns: [
            {
                field: 'experiences', title: "Experience",
                renderer: function(val) {
                    return val.join(', ');
                }
            },
            { field: 'package', title: "Package" },
            { field: 'quantity', title: "Q" },
            {
                field: 'cost', title: "Cost",
                renderer: function(val) {
                    if (val === null || val === '') {
                        return '';
                    }
                    return '$' + val;
                }
            },
            { field: 'currency', title: "Currency" },
            {
                field: 'discount', title: "Discount",
                renderer: function(val) {
                    if (val === null || val === '') {
                        return '';
                    }
                    return val + '%';
                }
            },
            {
                field: 'from_date', title: "Begin",
                renderer: function(val) {
                    if (val === null || val === '') {
                        return '';
                    }
                    return moment(val, 'YYYY-MM-DD').format('DD-MM-YYYY');
                }
            },
            {
                field: 'to_date', title: "End",
                renderer: function(val) {
                    if (val === null || val === '') {
                        return '';
                    }
                    return moment(val, 'YYYY-MM-DD').format('DD-MM-YYYY');
                }
            },
            {
                field: 'actionColumn',
                title: 'Actions',
                buttons: [
                    {
                        icon: 'fa-pencil',
                        handler: function(grid, index) {
                            var data = grid.data[index];
                            grid.selectedIndex = index;
                            cmp.window.form.ele.find('#course-begin').datepicker();
                            cmp.window.form.ele.find('#course-end').datepicker();
                            cmp.window.form.setData(data);
                            cmp.window.ele.find('.modal-content .modal-header .modal-title').text('Edit experience');
                            cmp.window.form.ele.find('button.add').text('Update');
                            cmp.window.form.action = 'edit';
                            cmp.window.ele.modal('show');
                        }
                    },
                    {
                        icon: 'fa-trash-o',
                        handler: function(grid, index) {
                            swal({
                                title: "Delete experience",
                                text: "Are you sure of deleting the selected experience",
                                type: 'question',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes, do it!'
                            }).then(function() {
                                grid.remove(index);
                            }).catch(swal.noop);
                        }
                    }
                ]
            }
        ],
        toolbar: [],
        readOnly: config.readOnly !== void 0 ? config.readOnly : false,
        onAfterRender: function(grid) {

        }
    });
    cmp.ele.parent().find('.box-header button.add').click(function(evt) {
        evt.preventDefault();
        cmp.window.form.ele.find('#course-begin').datepicker();
        cmp.window.form.ele.find('#course-end').datepicker();
        cmp.window.ele.modal('show');
    });

    // Inicializar datepickers
    cmp.window.form.ele.find('#course-begin').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy'
    });
    cmp.window.form.ele.find("#course-begin").datepicker().on('hide.bs.modal', function(event) {
        event.stopPropagation();
    });
    cmp.window.form.ele.find('#course-end').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy'
    });
    cmp.window.form.ele.find("#course-end").datepicker().on('hide.bs.modal', function(event) {
        event.stopPropagation();
    });
    var calculateEndDate = function(data) {
        if (data.from_date == '') {
            return;
        }
        var begin = moment(data.from_date, 'YYYY-MM-DD');
        var end;
        if (data.quantity > 0 && (data.unit === 'day' || data.unit === 'week')) {
            end = begin.add(data.quantity, data.unit).subtract(1, 'day');
            // Si la fecha de termino es un sábado o domingo, cambiar fecha de término al viernes previo
            if (end.day() === 6) {
                end.subtract(1, 'day');
            }
            else if (end.day() === 0) {
                end.subtract(2, 'day');
            }
            cmp.window.form.ele.find('#course-end').datepicker('setDate', end.format('DD-MM-YYYY'));
        }
    };
    var calculateCost = function(data) {
        var package = EXP.packages.findById(data.package_id);
        cmp.window.form.ele.find('#course-end').val('');
        var rate;
        var cost = 0;
        if (data.quantity > 0 && package != null) {
            // Buscar tarifa
            rate = EXP.packages.findPackageRate(package, data.quantity);
            if (rate !== null) {
                // Calcular costo
                cost = EXP.toFloat(rate.unit_cost) * data.quantity;
            }
        }
        cmp.window.form.ele.find('#course-cost').val(cost);
    };
    var activateEndDateField = function() {
        var unit = cmp.window.form.ele.find('#select-course-unit option:selected').val();
        if (unit === 'package') {
            //cmp.window.form.ele.find('#course-end').removeAttr('readOnly');
        }
        else {
            //cmp.window.form.ele.find('#course-end').attr('readOnly', 'readOnly');
        }
    };
    // Realizar calculos al ingresar la fecha de inicio
    cmp.window.form.ele.find('#course-begin').change(function() {
        var data = cmp.window.form.getData();
        // Calcular fecha de termino
        calculateEndDate(data);
    });
    // Realizar calculos al ingresar la cantidad
    cmp.window.form.ele.find('#course-quantity').change(function() {
        var data = cmp.window.form.getData();
        calculateCost(data);
        // Calcular fecha de termino
        calculateEndDate(data);
    });
    cmp.window.form.init = function(data) {

    };
    var showPackages = function() {
        var checkExperiences = cmp.window.form.ele.find('input.experiences');
        var experiences = [];
        _.each(checkExperiences, function(chk) {
            if (chk.checked) {
                experiences.push($(chk).data('experience'));
            }
        });
        var selectPackage = cmp.window.form.ele.find('#select-package-course');
        selectPackage.html('');
        // Obtener paquetes
        var packages = EXP.packages.findByExperiences(experiences);
        // Agregar paquetes
        _.each(packages, function(p) {
            selectPackage.append('<option value="' + p.id + '">' + p.name + '</option>');
        });
    };
    // Mostrar paquetes al seleccionar un curso
    cmp.window.form.ele.find('input.experiences').on('ifChanged', function(event) {
        showPackages();
        var data = cmp.window.form.getData();
        activateEndDateField();
        calculateCost(data);
        // Calcular fecha de termino
        calculateEndDate(data);
    });
    // Mostrar unidades al seleccionar un paquete
    cmp.window.form.ele.find('#select-package-course').on('change', function() {
        var data = cmp.window.form.getData();
        activateEndDateField();
        calculateCost(data);
        // Calcular fecha de termino
        calculateEndDate(data);
    });
    cmp.window.form.getData = function() {
        var checkExperiences = cmp.window.form.ele.find('input.experiences');
        var experiences = [];
        _.each(checkExperiences, function(chk) {
            if (chk.checked) {
                experiences.push($(chk).data('experience'));
            }
        });
        var selectedPackage = cmp.window.form.ele.find('#select-package-course option:selected');
        var cost = EXP.toFloat(cmp.window.form.ele.find('#course-cost').val());
        var quantity = EXP.toInteger(cmp.window.form.ele.find('#course-quantity').val());
        var selectedCurrency = cmp.window.form.ele.find('#select-currency-course option:selected');
        var discount = cmp.window.form.ele.find('#course-discount').val();
        var currentExchangeRates = JSON.parse(cmp.window.form.ele.find('#course-current-exchange-rates').val() || '[]');
        var exchangeRates = EXP.command.getExchangeRates(parseInt(selectedCurrency.val()), currentExchangeRates);
        return {
            id: cmp.window.form.ele.find('#id').val(),
            experiences: experiences,
            package_id: selectedPackage.val(),
            package: selectedPackage.text(),
            currency_id: parseInt(selectedCurrency.val()),
            currency: selectedCurrency.text(),
            quantity: quantity,
            cost: cost,
            discount: discount,
            exchange_rates: exchangeRates,
            current_exchange_rates: currentExchangeRates,
            from_date: EXP.toDate(cmp.window.form.ele.find('#course-begin').val(), 'DD-MM-YYYY', 'YYYY-MM-DD'),
            to_date: EXP.toDate(cmp.window.form.ele.find('#course-end').val(), 'DD-MM-YYYY', 'YYYY-MM-DD')
        };
    };
    cmp.window.form.clear = function() {
        cmp.window.form.ele[0].reset();
        cmp.window.form.ele.find('#course-current-exchange-rates').val('');
        // resetear los selects
        var selects = cmp.window.form.ele.find('select');
        selects.find('option:first-child').prop('selected', true);
        selects.trigger('change');
        // Desmarcar casillas de experiencias
        var checkExperiences = cmp.window.form.ele.find('input.experiences');
        for (var i = 0; i < checkExperiences.length; i++) {
            $(checkExperiences[i]).iCheck('check');
            $(checkExperiences[i]).iCheck('uncheck');
        }
    };
    cmp.window.form.setData = function(data) {
        // Seleccionar experiencias
        var checkExperiences = cmp.window.form.ele.find('input.experiences');
        _.each(data.experiences, function(e) {
            _.each(checkExperiences, function(chk) {
                if (e == $(chk).data('experience')) {
                    $(chk).iCheck('check');
                }
            });
        });
        showPackages();
        // Seleccionar paquete
        select = cmp.window.form.ele.find('#select-package-course')[0];
        for (i = 0; i < select.children.length; i++) {
            if (select.children[i].value == data.package_id) {
                $(select.children[i]).attr('selected', 'selected');
                $(select).val(data.package_id);
                cmp.window.form.ele.find('#select2-select-package-course-container').text($(select.children[i]).text());
                break;
            }
        }
        cmp.window.form.ele.find('#id').val(data.id);
        cmp.window.form.ele.find('#course-quantity').val(data.quantity);
        cmp.window.form.ele.find('#course-begin').datepicker('setDate', EXP.toDate(data.from_date, 'YYYY-MM-DD', 'DD-MM-YYYY'));
        cmp.window.form.ele.find('#course-end').datepicker('setDate', EXP.toDate(data.to_date, 'YYYY-MM-DD', 'DD-MM-YYYY'));
        cmp.window.form.ele.find('#course-cost').val(data.cost);
        cmp.window.form.ele.find('#course-discount').val(data.discount);
        cmp.window.form.ele.find('#select-currency-course').val(data.currency_id).trigger('change');
        cmp.window.form.ele.find('#course-current-exchange-rates').val(JSON.stringify(data.current_exchange_rates));
    };
    cmp.window.form.ele.find('button.add').click(function(evt) {
        evt.preventDefault();
        if (!cmp.window.form.isValid()) {
            return;
        }
        var data = cmp.window.form.getData();
        if (cmp.window.form.action === 'add') {
            data.id = 0;
            cmp.grid.add(data);
        }
        else {
            cmp.grid.set(cmp.grid.selectedIndex, data);
        }
        cmp.window.ele.modal('hide');
    });
    cmp.window.ele.on('hide.bs.modal', function(e) {
        cmp.window.ele.find('.modal-content .modal-header .modal-title').text('Add course / program');
        cmp.window.form.ele.find('button.add').text('Add');
        cmp.window.form.clear();
        cmp.window.form.action = 'add';
    });
};

EXP.command.housing.initComponent = function(cmp, data, config) {
    var buttons = [
        {
            icon: 'fa-pencil',
            handler: function(grid, index) {
                var data = grid.data[index];
                grid.selectedIndex = index;
                cmp.window.form.ele.find('#housing-begin').datepicker();
                cmp.window.form.ele.find('#housing-end').datepicker();
                cmp.window.form.setData(data);
                cmp.window.ele.find('.modal-content .modal-header .modal-title').text('Edit housing');
                cmp.window.form.ele.find('button.add').text('Update');
                cmp.window.form.action = 'edit';
                cmp.window.ele.modal('show');
            }
        },
        {
            icon: 'fa-trash-o',
            handler: function(grid, index) {
                swal({
                    title: "Delete housing",
                    text: "Are you sure of deleting the selected housing",
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, do it!'
                }).then(function() {
                    grid.remove(index);
                }).catch(swal.noop);
            }
        }
    ];
    if (EXP.command.id != -1) {
        buttons.push({
            icon: 'fa-bed',
            handler: function(grid, index) {
                grid.selectedIndex = index;
                cmp.modalScheduler.ele.modal('show');
                // Mostrar datos de hospedaje
                var data = grid.data[index];
                EXP.command.housing.component.modalScheduler.ele.find('#housing-data #housing').val(data.housing);
                EXP.command.housing.component.modalScheduler.ele.find('#housing-data #housing-room').val(data.housing_room);
                EXP.command.housing.component.modalScheduler.ele.find('#housing-data #begin-date').val(EXP.toDate(data.from_date, 'YYYY-MM-DD', 'DD-MM-YYYY'));
                EXP.command.housing.component.modalScheduler.ele.find('#housing-data #end-date').val(EXP.toDate(data.to_date, 'YYYY-MM-DD', 'DD-MM-YYYY'));
                // Inicializar selects de residencias y cuartos
                var selectResidence = cmp.modalScheduler.ele.find('#select-residences');
                selectResidence.html('');
                // Agregar residencias
                var rooms = null;
                _.each(EXP.residences.data, function(r) {
                    if (r.housing.data.id == data.housing_id) {
                        selectResidence.append('<option value="' + r.id + '" ' +
                            (r.id == data.residence_id ? 'selected' : '') + '>' +
                            r.name + '</option>');
                        if (rooms == null && r.id == data.residence_id) {
                            rooms = r.rooms.data;
                        }
                    }
                });
                var selectRooms = cmp.modalScheduler.ele.find('#select-room');
                selectRooms.html('');

                var beginDateMoment = moment(data.from_date, 'YYYY-MM-DD');
                var endDateMoment = moment(data.to_date, 'YYYY-MM-DD');
                _.each(rooms, function(r) {
                    var sizeBLockedDates = r.blockedDates.data.length;
                    var fromAt = null;
                    var toAt = null;

                    for (var x = 0; x < sizeBLockedDates; x++) {
                        fromAt = r.blockedDates.data[x].from_at;
                        toAt = r.blockedDates.data[x].to_at;
                        if (toAt === void 0 || toAt === null) {
                            toAt = r.blockedDates.data[x].from_at;
                        }

                        fromAt = moment(fromAt, 'YYYY-MM-DD');
                        toAt = moment(toAt, 'YYYY-MM-DD');

                        // Return void if busy dates.
                        if (
                            beginDateMoment.isBetween(fromAt, toAt, null, '[]') ||
                            endDateMoment.isBetween(fromAt, toAt, null, '[]')
                        ) {
                            return;
                        }
                    }
                    for (var i = 0; i < r.housingRooms.data.length; i++) {
                        if (r.housingRooms.data[i].id == data.housing_room_id) {
                            selectRooms.append('<option value="' + r.id + '" ' +
                                (r.id == data.room_id ? 'selected' : '') +
                                '>' + r.name + '</option>');
                        }
                    }
                });
            }
        });
    }
    cmp.grid = new EXP.Grid($(config.gridSelector), {
        fields: [
            'id', 'housing_room_package_id', 'housing', 'housing_id', 'housing_room',
            'housing_room_id', 'package', 'package_id', 'quantity', 'unit',
            'cost', 'currency', 'currency_id', 'discount', 'from_date', 'to_date', 'room_id', 'room_number', 'residence_id', 'residence', 'people', 'exchange_rates', 'current_exchange_rates'
        ],
        data: data,
        columns: [
            { field: 'housing', title: "Housing" },
            { field: 'housing_room', title: "Room type" },
            { field: 'package', title: "Package" },
            { field: 'room_number', title: "Room" },
            { field: 'residence', title: "Residence" },
            { field: 'quantity', title: "Q" },
            { field: 'unit', title: "U" },
            {
                field: 'cost', title: "Cost",
                renderer: function(val) {
                    if (val === null || val === '') {
                        return '';
                    }
                    return '$' + val;
                }
            },
            { field: 'currency', title: "Currency" },
            {
                field: 'discount', title: "Discount",
                renderer: function(val) {
                    if (val === null || val === '') {
                        return '';
                    }
                    return val + '%';
                }
            },
            { field: 'people', title: "People" },
            {
                field: 'from_date', title: "Begin",
                renderer: function(val) {
                    if (val === null || val === '') {
                        return '';
                    }
                    return moment(val, 'YYYY-MM-DD').format('DD-MM-YYYY');
                }
            },
            {
                field: 'to_date', title: "End",
                renderer: function(val) {
                    if (val === null || val === '') {
                        return '';
                    }
                    return moment(val, 'YYYY-MM-DD').format('DD-MM-YYYY');
                }
            },
            {
                field: 'actionColumn',
                title: 'Actions',
                width: 105,
                buttons: buttons
            }
        ],
        toolbar: [],
        readOnly: config.readOnly !== void 0 ? config.readOnly : false,
        onAfterRender: function(grid) {

        }
    });
    cmp.ele.parent().find('.box-header button.add').click(function(evt) {
        cmp.window.form.ele.find('#housing-begin').datepicker();
        cmp.window.form.ele.find('#housing-end').datepicker();
        cmp.window.ele.modal('show');
        evt.preventDefault();
    });

    // Inicializar datepickers
    cmp.window.form.ele.find('#housing-begin').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy'
    });
    cmp.window.form.ele.find("#housing-begin").datepicker().on('hide.bs.modal', function(event) {
        event.stopPropagation();
    });
    cmp.window.form.ele.find('#housing-end').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy'
    });
    cmp.window.form.ele.find("#housing-end").datepicker().on('hide.bs.modal', function(event) {
        event.stopPropagation();
    });
    var calculateEndDate = function(data) {
        if (data.from_date == '') {
            return;
        }
        var begin = moment(data.from_date, 'YYYY-MM-DD');
        var end;
        if (data.quantity > 0 && (data.unit === 'night' || data.unit === 'week')) {
            end = begin.add(data.quantity, data.unit === 'night' ? 'day' : data.unit);
            // Si la fecha de termino es un sábado o domingo, cambiar fecha de término al viernes previo
            /*if (end.day() === 6) {
                end.subtract(1, 'day');
            }
            else if (end.day() === 0) {
                end.subtract(2, 'day');
            }*/
            cmp.window.form.ele.find('#housing-end').datepicker('setDate', end.format('DD-MM-YYYY'));
        }
    };
    var addUnits = function(data) {
        var housing = EXP.housing.findById(data.housing_id);
        var housingRoom = EXP.housing.findHousingRoomById(data.housing_id, data.housing_room_id);
        cmp.window.form.ele.find('#housing-cost').val('');
        // Obtener unidades
        var package = EXP.housing.findPackageById(housing.id, housingRoom.id, data.package_id);
        var units = EXP.housing.getPackageUnits(package);
        // Agregar unidades
        var selectUnit = cmp.window.form.ele.find('#select-housing-unit');
        selectUnit.html('');
        _.each(units, function(u) {
            selectUnit.append('<option value="' + u.value + '">' + u.name + '</option>');
        });
    };
    var calculateCost = function(data) {
        var housing = EXP.housing.findById(data.housing_id);
        var housingRoom = EXP.housing.findHousingRoomById(data.housing_id, data.housing_room_id);
        var package = EXP.housing.findPackageById(housing.id, housingRoom.id, data.package_id);
        var rate;
        var cost = 0;
        var unit = cmp.window.form.ele.find('#select-housing-unit option:selected').val();
        if (data.quantity > 0) {
            // Buscar tarifa
            rate = EXP.housing.findPackageRate(package, data.from_date, unit);
            if (rate !== null) {
                // Calcular costo
                cost = EXP.toFloat(rate.unit_cost) * data.quantity;
                if (housingRoom.cost_type == 'bed') {
                    cost *= data.people;
                }
            }
        }
        cmp.window.form.ele.find('#housing-cost').val(cost);
    };
    // Realizar calculos al ingresar la fecha de inicio
    cmp.window.form.ele.find('#housing-begin').change(function() {
        var data = cmp.window.form.getData();
        calculateCost(data);
        // Calcular fecha de termino
        calculateEndDate(data);
    });
    // Realizar calculos al ingresar la cantidad
    cmp.window.form.ele.find('#housing-quantity').change(function() {
        var data = cmp.window.form.getData();
        calculateCost(data);
        // Calcular fecha de termino
        calculateEndDate(data);
    });
    // Realizar calculos al seleccionar la unidad
    cmp.window.form.ele.find('#select-housing-unit').change(function() {
        var data = cmp.window.form.getData();
        calculateCost(data);
        // Calcular fecha de termino
        calculateEndDate(data);
    });
    cmp.window.form.init = function(data) {

    };
    var showHousingRooms = function() {
        var selected = cmp.window.form.ele.find('#select-housing option:selected');
        var selectRoom = cmp.window.form.ele.find('#housing-select-room');
        selectRoom.html('');
        // Obtener hospedaje
        var housing = EXP.housing.findById(selected.val());
        // Agregar cuartos del hospedaje
        _.each(housing.housingRooms.data, function(h) {
            selectRoom.append('<option value="' + h.id + '" data-cost_type="' + h.cost_type + '" data-beds="' + h.beds + '">' + h.name + '</option>');
        });
        showPackages();
    };
    var showPackages = function() {
        var selectHousing = cmp.window.form.ele.find('#select-housing option:selected');
        var selectHousingRoom = cmp.window.form.ele.find('#housing-select-room option:selected');
        var selectPackage = cmp.window.form.ele.find('#select-package');
        selectPackage.html('');
        // Obtener curso
        var room = EXP.housing.findHousingRoomById(selectHousing.val(), selectHousingRoom.val());
        // Agregar paquetes del curso
        _.each(room.packages.data, function(p) {
            selectPackage.append('<option value="' + p.housing_room_package_id + '" data-id="' + p.id + '">' + p.name + '</option>');
        });
        // Mostrar numero de personas permitidas para el tipo de cuarto seleccionado
        var selectPeople = cmp.window.form.ele.find('#select-housing-people');
        selectPeople.html('');
        for (var i = 1; i <= selectHousingRoom.data('beds'); i++) {
            selectPeople.append('<option value="' + i + '">' + i + '</option>');
        }
    };
    // Mostrar tipos de cuartos al seleccionar un hospedaje
    cmp.window.form.ele.find('#select-housing').on('change', function() {
        showHousingRooms();
        var data = cmp.window.form.getData();
        addUnits(data);
        calculateCost(data);
        // Calcular fecha de termino
        calculateEndDate(data);
    });
    // Mostrar paquetes al seleccionar un cuarto de hospedaje
    cmp.window.form.ele.find('#housing-select-room').on('change', function() {
        showPackages();
        var data = cmp.window.form.getData();
        addUnits(data);
        calculateCost(data);
        // Calcular fecha de termino
        calculateEndDate(data);
    });
    // Mostrar unidades al seleccionar un paquete
    cmp.window.form.ele.find('#select-package').on('change', function() {
        var data = cmp.window.form.getData();
        addUnits(data);
        calculateCost(data);
        // Calcular fecha de termino
        calculateEndDate(data);
    });
    cmp.window.form.ele.find('#select-housing-people').on('change', function() {
        var data = cmp.window.form.getData();
        calculateCost(data);
    });
    cmp.window.form.getData = function() {
        var selectedHousing = cmp.window.form.ele.find('#select-housing option:selected');
        var selectedHousingRoom = cmp.window.form.ele.find('#housing-select-room option:selected');
        var selectedPackage = cmp.window.form.ele.find('#select-package option:selected');
        var selectedUnit = cmp.window.form.ele.find('#select-housing-unit option:selected');
        var cost = EXP.toFloat(cmp.window.form.ele.find('#housing-cost').val());
        var quantity = EXP.toInteger(cmp.window.form.ele.find('#housing-quantity').val());
        var selectedPeople = cmp.window.form.ele.find('#select-housing-people option:selected');
        var discount = cmp.window.form.ele.find('#housing-discount').val();
        var room = cmp.window.form.ele.find('#h-room').val();
        var residence = cmp.window.form.ele.find('#h-residence').val();
        var selectedCurrency = cmp.window.form.ele.find('#select-currency-housing option:selected');
        var currentExchangeRates = JSON.parse(cmp.window.form.ele.find('#housing-current-exchange-rates').val() || '[]');
        var exchangeRates = EXP.command.getExchangeRates(parseInt(selectedCurrency.val()), currentExchangeRates);
        return {
            id: cmp.window.form.ele.find('#id').val(),
            housing_id: selectedHousing.val(),
            housing: selectedHousing.text(),
            housing_room_id: selectedHousingRoom.val(),
            housing_room: selectedHousingRoom.text(),
            housing_room_package_id: selectedPackage.val(),
            package_id: selectedPackage.attr('data-id'),
            package: selectedPackage.text(),
            unit: selectedUnit.val(),
            quantity: quantity,
            cost: cost,
            discount: discount,
            from_date: EXP.toDate(cmp.window.form.ele.find('#housing-begin').val(), 'DD-MM-YYYY', 'YYYY-MM-DD'),
            to_date: EXP.toDate(cmp.window.form.ele.find('#housing-end').val(), 'DD-MM-YYYY', 'YYYY-MM-DD'),
            people: EXP.toInteger(selectedPeople.val()),
            currency_id: parseInt(selectedCurrency.val()),
            currency: selectedCurrency.text(),
            room_number: room,
            residence: residence,
            exchange_rates: exchangeRates,
            current_exchange_rates: currentExchangeRates,
        };
    };
    cmp.window.form.clear = function() {
        cmp.window.form.ele[0].reset();
        cmp.window.form.ele.find('#housing-select-room').html('');
        cmp.window.form.ele.find('#select-package').html('');
        cmp.window.form.ele.find('#housing-current-exchange-rates').val('');
        showHousingRooms();
        // Seleccionar la primer moneda
        cmp.window.form.ele.find('#select-currency-housing option:first-child').prop('selected', true).trigger('change');
    };
    cmp.window.form.setData = function(data) {
        // Seleccionar hospedaje
        var select = cmp.window.form.ele.find('#select-housing')[0];
        var i;
        for (i = 0; i < select.children.length; i++) {
            if (select.children[i].value == data.housing_id) {
                $(select.children[i]).attr('selected', 'selected');
                $(select).val(data.housing_id);
                cmp.window.form.ele.find('#select2-select-housing-container').text($(select.children[i]).text());
                break;
            }
        }
        showHousingRooms();
        // Seleccionar cuarto
        select = cmp.window.form.ele.find('#housing-select-room')[0];
        for (i = 0; i < select.children.length; i++) {
            if (select.children[i].value == data.housing_room_id) {
                $(select.children[i]).attr('selected', 'selected');
                $(select).val(data.housing_room_id);
                cmp.window.form.ele.find('#select2-housing-select-room-container').text($(select.children[i]).text());
                break;
            }
        }
        showPackages();
        // Seleccionar paquete
        select = cmp.window.form.ele.find('#select-package')[0];
        for (i = 0; i < select.children.length; i++) {
            if (select.children[i].value == data.housing_room_package_id) {
                $(select.children[i]).attr('selected', 'selected');
                $(select).val(data.housing_room_package_id);
                cmp.window.form.ele.find('#select2-select-package-container').text($(select.children[i]).text());
                break;
            }
        }
        // Seleccionar unidad
        select = cmp.window.form.ele.find('#select-housing-unit')[0];
        for (i = 0; i < select.children.length; i++) {
            if (select.children[i].value == data.unit) {
                $(select.children[i]).attr('selected', 'selected');
                $(select).val(data.unit);
                cmp.window.form.ele.find('#select2-select-housing-unit-container').text($(select.children[i]).text());
                break;
            }
        }
        // Seleccionar numero de personas
        select = cmp.window.form.ele.find('#select-housing-people')[0];
        for (i = 0; i < select.children.length; i++) {
            if (select.children[i].value == data.people) {
                $(select.children[i]).attr('selected', 'selected');
                $(select).val(data.people);
                cmp.window.form.ele.find('#select2-select-housing-people-container').text($(select.children[i]).text());
                break;
            }
        }
        cmp.window.form.ele.find('#id').val(data.id);
        cmp.window.form.ele.find('#housing-quantity').val(data.quantity);
        cmp.window.form.ele.find('#housing-begin').datepicker('setDate', EXP.toDate(data.from_date, 'YYYY-MM-DD', 'DD-MM-YYYY'));
        cmp.window.form.ele.find('#housing-end').datepicker('setDate', EXP.toDate(data.to_date, 'YYYY-MM-DD', 'DD-MM-YYYY'));
        cmp.window.form.ele.find('#housing-cost').val(data.cost);
        cmp.window.form.ele.find('#housing-discount').val(data.discount);
        cmp.window.form.ele.find('#h-room').val(data.room_number);
        cmp.window.form.ele.find('#h-residence').val(data.residence);
        cmp.window.form.ele.find('#select-currency-housing').val(data.currency_id).trigger('change');
        cmp.window.form.ele.find('#housing-current-exchange-rates').val(JSON.stringify(data.current_exchange_rates));
    };
    cmp.window.form.ele.find('button.add').click(function(evt) {
        evt.preventDefault();
        if (!cmp.window.form.isValid()) {
            console.log('not valid');
            return;
        }
        var data = cmp.window.form.getData();
        if (cmp.window.form.action === 'add') {
            data.id = 0;
            data.room_number = null;
            data.residence = null;
            cmp.grid.add(data);
            // Eliminar del dom el botón de agregar, esto se hace para que solo se pueda guardar un nuevo registro a la vez
            // cmp.ele.parent().find('.box-header button.add').remove();
        }
        else {
            cmp.grid.set(cmp.grid.selectedIndex, data);
        }
        cmp.window.ele.modal('hide');
    });
    cmp.window.ele.on('hide.bs.modal', function(e) {
        cmp.window.ele.find('.modal-content .modal-header .modal-title').text('Add housing');
        cmp.window.form.ele.find('button.add').text('Add');
        cmp.window.form.clear();
        cmp.window.form.action = 'add';
    });
};

EXP.command.payments.initComponent = function(cmp, data, config) {
    cmp.grid = new EXP.Grid($(config.gridSelector), {
        fields: [
            'id', 'concept', 'amount', 'currency', 'paid_at', 'vendor'
        ],
        data: data,
        columns: [
            { field: 'concept', title: "Concept" },
            {
                field: 'amount', title: "Amount",
                renderer: function(val) {
                    if (val === null || val === '') {
                        return '';
                    }
                    return '$' + val;
                }
            },
            { field: 'currency', title: "Currency" },
            {
                field: 'paid_at', title: "Paid at",
                renderer: function(val) {
                    if (val === null || val === '') {
                        return '';
                    }
                    return moment(val, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY');
                }
            },
            { field: 'vendor', title: "Vendor" }
        ]
    });
};

EXP.command.purchases.initComponent = function(cmp, data, config) {
    cmp.grid = new EXP.Grid($(config.gridSelector), {
        fields: [
            'id', 'date', 'reference_number', 'concept', 'amount', 'discount', 'currency', 'currency_id', 'consumption_center', 'consumption_center_id', 'exchange_rates', 'current_exchange_rates'
        ],
        data: data,
        columns: [
            {
                field: 'date', title: "Date",
                renderer: function(val) {
                    if (val === null || val === '') {
                        return '';
                    }
                    return moment(val, 'YYYY-MM-DD').format('DD-MM-YYYY');
                }
            },
            { field: 'reference_number', title: "Reference number" },
            { field: 'concept', title: "Concept" },
            {
                field: 'amount', title: "Amount",
                renderer: function(val) {
                    if (val === null || val === '') {
                        return '';
                    }
                    return '$' + val;
                }
            },
            { field: 'currency', title: "Currency" },
            {
                field: 'discount', title: "Discount",
                renderer: function(val) {
                    if (val === null || val === '') {
                        return '';
                    }
                    return val + '%';
                }
            },
            { field: 'consumption_center', title: "Consumption center" },
            {
                field: 'actionColumn',
                title: 'Actions',
                buttons: [
                    {
                        icon: 'fa-pencil',
                        handler: function(grid, index) {
                            var data = grid.data[index];
                            grid.selectedIndex = index;
                            cmp.window.form.setData(data);
                            cmp.window.ele.find('.modal-content .modal-header .modal-title').text('Edit purchase');
                            cmp.window.form.ele.find('button.add').text('Update');
                            cmp.window.form.action = 'edit';
                            cmp.window.ele.modal('show');
                        }
                    },
                    {
                        icon: 'fa-trash-o',
                        handler: function(grid, index) {
                            swal({
                                title: "Delete purchase",
                                text: "Are you sure of deleting the selected purchase",
                                type: 'question',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes, do it!'
                            }).then(function() {
                                grid.remove(index);
                            }).catch(swal.noop);
                        }
                    }
                ]
            }
        ],
        toolbar: [],
        readOnly: config.readOnly !== void 0 ? config.readOnly : false,
        onAfterRender: function(grid) {

        }
    });
    cmp.ele.parent().find('.box-header button.add').click(function(evt) {
        evt.preventDefault();
        cmp.window.ele.modal('show');
    });

    // Inicializar datepickers
    cmp.window.form.ele.find('#purchase_date').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy'
    });

    cmp.window.form.ele.find("#purchase_date").datepicker().on('hide.bs.modal', function(event) {
        event.stopPropagation();
    });

    cmp.window.form.getData = function() {
        var amount = EXP.toFloat(cmp.window.form.ele.find('#purchase_amount').val());
        var referenceNumber = cmp.window.form.ele.find('#purchase_reference_number').val();
        var selectedCurrency = cmp.window.form.ele.find('#purchase_currency option:selected');
        var selectedComsumptionCenter = cmp.window.form.ele.find('#purchase_consumption_center option:selected');
        var discount = cmp.window.form.ele.find('#purchase_discount').val();
        var concept = cmp.window.form.ele.find('#purchase_concept').val();
        var currentExchangeRates = JSON.parse(cmp.window.form.ele.find('#purchase-current-exchange-rates').val() || '[]');
        var exchangeRates = EXP.command.getExchangeRates(parseInt(selectedCurrency.val()), currentExchangeRates);
        return {
            id: cmp.window.form.ele.find('#id').val(),
            reference_number: referenceNumber,
            concept: concept,
            amount: amount,
            currency_id: parseInt(selectedCurrency.val()),
            currency: selectedCurrency.text(),
            discount: discount,
            consumption_center_id: selectedComsumptionCenter.val(),
            consumption_center: selectedComsumptionCenter.text(),
            exchange_rates: exchangeRates,
            current_exchange_rates: currentExchangeRates,
            date: EXP.toDate(cmp.window.form.ele.find('#purchase_date').val(), 'DD-MM-YYYY', 'YYYY-MM-DD'),
        };
    };
    cmp.window.form.clear = function() {
        cmp.window.form.ele[0].reset();
        cmp.window.form.ele.find('#purchase-current-exchange-rates').val('');
        // resetear los selects
        var selects = cmp.window.form.ele.find('select');
        selects.find('option:first-child').prop('selected', true);
        selects.trigger('change');
    };
    cmp.window.form.setData = function(data) {
        cmp.window.form.ele.find('#id').val(data.id);
        cmp.window.form.ele.find('#purchase_amount').val(data.amount);
        cmp.window.form.ele.find('#purchase_reference_number').val(data.reference_number);
        cmp.window.form.ele.find('#purchase_currency').val(data.currency_id).trigger('change');
        cmp.window.form.ele.find('#purchase_consumption_center').val(data.consumption_center_id).trigger('change');
        cmp.window.form.ele.find('#purchase_discount').val(data.discount);
        cmp.window.form.ele.find('#purchase_concept').val(data.concept);
        cmp.window.form.ele.find('#purchase_date').val(EXP.toDate(data.date, 'YYYY-MM-DD', 'DD-MM-YYYY'));
        cmp.window.form.ele.find('#purchase-current-exchange-rates').val(JSON.stringify(data.current_exchange_rates));
    };
    cmp.window.form.ele.find('button.add').click(function(evt) {
        evt.preventDefault();
        if (!cmp.window.form.isValid()) {
            return;
        }
        var data = cmp.window.form.getData();
        if (cmp.window.form.action === 'add') {
            data.id = 0;
            cmp.grid.add(data);
        }
        else {
            cmp.grid.set(cmp.grid.selectedIndex, data);
        }
        cmp.window.ele.modal('hide');
    });
    cmp.window.ele.on('hide.bs.modal', function(e) {
        cmp.window.ele.find('.modal-content .modal-header .modal-title').text('Add purchase');
        cmp.window.form.ele.find('button.add').text('Add');
        cmp.window.form.clear();
        cmp.window.form.action = 'add';
    });
};

EXP.command.allPayments.initComponent = function(cmp, data, config) {
    cmp.grid = new EXP.Grid($(config.gridSelector), {
        fields: [
            'id', 'invoiced_at', 'reference_id', 'concept', 'type', 'amount', 'currency', 'currency_id', 'payment_method', 'payment_method_id', 'paid_at', 'vendor', 'exchange_rates', 'current_exchange_rates'
        ],
        data: data,
        columns: [
            {
                field: 'invoiced_at', title: "Invoiced",
                renderer: function(val) {
                    var checked = 'checked';
                    if (val === null || val === '') {
                        checked = '';
                    }
                    return '<input type="checkbox" class="iCheckInvoiced" ' + checked + '>';
                }
            },
            { field: 'reference_id', title: "Reference number" },
            { field: 'concept', title: "Concept" },
            {
                field: 'amount', title: "Amount",
                renderer: function(val) {
                    if (val === null || val === '') {
                        return '';
                    }
                    return '$' + val;
                }
            },
            { field: 'currency', title: "Currency" },
            { field: 'payment_method', title: "Payment method" },
            {
                field: 'paid_at', title: "Paid at",
                renderer: function(val) {
                    if (val === null || val === '') {
                        return '';
                    }
                    return EXP.toDate(val, 'YYYY-MM-DD HH:mm:ss', 'DD-MM-YYYY');
                }
            },
            { field: 'vendor', title: "Vendor" },
            {
                field: 'actionColumn',
                title: 'Actions',
                buttons: [
                    {
                        icon: 'fa-pencil',
                        handler: function(grid, index) {
                            var data = grid.data[index];
                            if (data.type === 'registration_fee' || data.type === 'reservation_payment') {
                                swal("Edit payment", "This type of payment can not be edited", "warning");
                                return;
                            }
                            grid.selectedIndex = index;
                            cmp.window.form.setData(data);
                            cmp.window.ele.find('.modal-content .modal-header .modal-title').text('Edit payment');
                            cmp.window.form.ele.find('button.add').text('Update');
                            cmp.window.form.action = 'edit';
                            cmp.window.ele.modal('show');
                        }
                    },
                    {
                        icon: 'fa-trash-o',
                        handler: function(grid, index) {
                            var data = grid.data[index];
                            if (data.type === 'registration_fee' || data.type === 'reservation_payment') {
                                swal("Delete payment", "This type of payment can not be deleted", "warning");
                                return;
                            }
                            swal({
                                title: "Delete payment",
                                text: "Are you sure of deleting the selected payment",
                                type: 'question',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes, do it!'
                            }).then(function() {
                                grid.remove(index);
                            }).catch(swal.noop);
                        }
                    }
                ]
            }
        ],
        toolbar: [],
        readOnly: config.readOnly !== void 0 ? config.readOnly : false,
        onAfterRender: function(grid) {
            EXP.initICheck('.iCheckInvoiced');
            // Quitar los checkbox a los nuevos pagos, solo se dejan a los pagos que ya han sido guardados
            $.each(cmp.ele.find('.iCheckInvoiced'), function(index, value) {
                var id = $(value).closest('tr').data('id');
                if (id == 0) {
                    $(value).closest('.icheckbox_square-blue').remove();
                }
            });
        }
    });

    cmp.ele.parent().parent().parent().find('.box-header button.add').click(function(evt) {
        evt.preventDefault();
        cmp.window.ele.modal('show');
    });

    cmp.window.form.getData = function() {
        var amount = EXP.toFloat(cmp.window.form.ele.find('#payment_amount').val());
        var referenceNumber = cmp.window.form.ele.find('#payment_reference_number').val();
        var selectedCurrency = cmp.window.form.ele.find('#payment_currency option:selected');
        var selectedPaymentMethod = cmp.window.form.ele.find('#payment_payment_method option:selected');
        var invoicedAt = cmp.window.form.ele.find('#invoiced_at').val();
        var paidAt = cmp.window.form.ele.find('#paid_at').val();
        var vendor = cmp.window.form.ele.find('#payment_vendor').val();
        var currentExchangeRates = JSON.parse(cmp.window.form.ele.find('#payment-current-exchange-rates').val() || '[]');
        var exchangeRates = EXP.command.getExchangeRates(parseInt(selectedCurrency.val()), currentExchangeRates);
        return {
            id: cmp.window.form.ele.find('#id').val(),
            invoiced_at: invoicedAt,
            reference_id: referenceNumber,
            concept: 'Purchase payment',
            type: 'purchase_payment',
            vendor: vendor,
            amount: amount,
            currency_id: parseInt(selectedCurrency.val()),
            currency: selectedCurrency.text(),
            payment_method_id: selectedPaymentMethod.val(),
            payment_method: selectedPaymentMethod.text(),
            paid_at: paidAt ? paidAt : moment().format('YYYY-MM-DD HH:mm:ss'),
            exchange_rates: exchangeRates,
            current_exchange_rates: currentExchangeRates,
        };
    };

    cmp.window.form.clear = function() {
        cmp.window.form.ele[0].reset();
        cmp.window.form.ele.find('#invoiced_at, #paid_at, #payment-current-exchange-rates').val('');
        // resetear los selects
        var selects = cmp.window.form.ele.find('select');
        selects.find('option:first-child').prop('selected', true);
        selects.trigger('change');
    };
    cmp.window.form.setData = function(data) {
        cmp.window.form.ele.find('#id').val(data.id);
        cmp.window.form.ele.find('#invoiced_at').val(data.invoiced_at);
        cmp.window.form.ele.find('#paid_at').val(data.paid_at);
        cmp.window.form.ele.find('#payment_amount').val(data.amount);
        cmp.window.form.ele.find('#payment_reference_number').val(data.reference_id);
        cmp.window.form.ele.find('#payment_currency').val(data.currency_id).trigger('change');
        cmp.window.form.ele.find('#payment_payment_method').val(data.payment_method_id).trigger('change');
        cmp.window.form.ele.find('#payment_vendor').val(data.vendor);
        cmp.window.form.ele.find('#payment-current-exchange-rates').val(JSON.stringify(data.current_exchange_rates));
    };
    cmp.window.form.ele.find('button.add').click(function(evt) {
        evt.preventDefault();
        if (!cmp.window.form.isValid()) {
            return;
        }
        var data = cmp.window.form.getData();
        if (cmp.window.form.action === 'add') {
            data.id = 0;
            cmp.grid.add(data);
        }
        else {
            cmp.grid.set(cmp.grid.selectedIndex, data);
        }
        cmp.window.ele.modal('hide');
    });
    cmp.window.ele.on('hide.bs.modal', function(e) {
        cmp.window.ele.find('.modal-content .modal-header .modal-title').text('Add payment');
        cmp.window.form.ele.find('button.add').text('Add');
        cmp.window.form.clear();
        cmp.window.form.action = 'add';
    });
};

EXP.command.allPayments.Object = function() {
    this.totals = [],
    this.amounts = [];
    this.paidTotals = [];
    this.defaultCurrency = 0;
    this.init = function() {
        // La moneda por default es el dolar
        this.defaultCurrency = this.findDefaultCurrency('USD', EXP.currencies);
        this.calculateTotals();
    }
    this.findCurrencyById = function(id, currencies) {
        return currencies.find(function(c) {
            return c.id === id;
        });
    };
    this.showTotals = function(totals, selector) {
        var html = '';
        totals.forEach(function(total, index) {
            var currency = this.findCurrencyById(index, EXP.currencies);
            if (currency) {
                html += '' +
                    '<p class="total">' +
                    '<span class="total_currency">' + currency.code + ':</span>' +
                    '<span class="total_amount">$' + total.toFixed(2) + '</span>' +
                    '</p>';
            }
        }, this);
        $(selector).html(html);
    }
    this.showBalance = function(grandTotal, amountPaid, balanceDue) {
        var html =
        '<div class="pull-right" style="min-width: 200px;">' +
            '<p><b>Grand total: </b>' + grandTotal.toFixed(2) + ' MXN</p>' +
            '<p><b>Amount paid: </b>' + amountPaid.toFixed(2) + ' MXN</p>' +
            '<p><b>Balance due: </b>' + balanceDue.toFixed(2) + ' MXN</p>' +
        '</div>';
        $('#balance').html(html);
    }
    this.initTotals = function(currencies) {
        for (var i = 0; i < currencies.length; i++) {
            // Cada posición del array va ser una moneda
            this.totals[currencies[i].id] = 0;
            this.paidTotals[currencies[i].id] = 0;
            this.amounts[currencies[i].id] = [];
        }
    };
    this.findDefaultCurrency = function(currency, currencies) {
        var c = currencies.find(function(c) {
            return c.code === currency;
        });
        if (c.id) {
            return c.id;
        }
        return 0;
    };
    this.calculateTotalByCurrency = function(data, currencies, defaultCurrency, field, isPaidTotals, hasCurrency) {
        var amount = 0;
        for (var i = 0; i < data.length; i++) {
            var currency = defaultCurrency;
            if (hasCurrency) {
                currency = data[i].currency_id;
            }
            if (isPaidTotals) {
                // Acumula el total de lo que se ha pagado en las diferentes monedas
                this.paidTotals[currency] += EXP.toFloat(data[i][field]);
            } else {
                // Acumula el total de lo que se ha comprado en las diferentes monedas
                amount = this.applyDiscount(data[i][field], data[i]['discount'])
                this.totals[currency] += amount;
                // Las cantidades que se han comprado
                this.amounts[currency].push({'amount': amount, 'exchange_rates': data[i].exchange_rates});
            }
        }
    };
    this.calculateTotalCourses = function() {
        var data = [];
        // calcular el total de los cursos
        data = EXP.command.courses.component.grid.data;
        this.calculateTotalByCurrency(data, EXP.currencies, this.defaultCurrency, 'cost', false, true);
    };
    this.calculateTotalHousing = function() {
        var data = [];
        // calcular el total de los hospedajes
        data = EXP.command.housing.component.grid.data;
        this.calculateTotalByCurrency(data, EXP.currencies, this.defaultCurrency, 'cost', false, true);
    };
    this.calculateTotalExtraPurchases = function() {
        var data = [];
        // calcular el total de las compras extras
        data = EXP.command.purchases.component.grid.data;
        this.calculateTotalByCurrency(data, EXP.currencies, this.defaultCurrency, 'amount', false, true);
    };
    this.calculateBalance = function() {
        var data = [];
        // calcular el total de los pagos
        data = EXP.command.allPayments.component.grid.data;
        var currency = null;
        var grandTotal = 0;
        var amountPaid = 0;
        var balanceDue = 0;
        // Convertir a peso mexicano todo lo que se ha pagado
        for(var i = 0; i < data.length; i++) {
            currency = this.findCurrencyById(parseInt(data[i].currency_id), EXP.currencies);
            amountPaid += this.convertToMxn(currency, data[i].amount, data[i].exchange_rates);
        }
        // Convertir a peso mexicano todo lo que sea comprado
        this.amounts.forEach(function(amountsByCurrency, index) {
            amountsByCurrency.forEach(function(data) {
                currency = this.findCurrencyById(index, EXP.currencies);
                grandTotal += this.convertToMxn(currency, data.amount, data.exchange_rates);
            }, this);
        }, this);
        balanceDue = parseFloat(grandTotal.toFixed(2)) - parseFloat(amountPaid.toFixed(2));
        this.showBalance(grandTotal, amountPaid, balanceDue);
        this.calculateTotalByCurrency(data, EXP.currencies, this.defaultCurrency, 'amount', true, true);
    };
    this.convertToMxn = function(currency, amountToConvert, exchangeRates) {
        var totalInMxn = 0;
        if (!currency) {
            return 0;
        }
        if (currency.code === 'MXN') {
            // La moneda es peso mexicano, no hay nada que convertir;
            return amountToConvert;
        }
        var toCurrency = null;
        for(var i = 0; i < exchangeRates.length; i++) {
            // Convertir la cantidad a peso mexicano
            toCurrency = this.findCurrencyById(exchangeRates[i].to_currency_id, EXP.currencies);
            if (toCurrency && exchangeRates[i].base_currency_id === currency.id && toCurrency.code === 'MXN') {
                totalInMxn = amountToConvert * exchangeRates[i].amount;
                break;
            }
        }
        return totalInMxn;
    };
    this.calculateTotalPickupCost = function() {
        var data = [];
        // Sumar al total el costo del pickup
        data.push({
            amount: $('#pickup-cost').val() ? $('#pickup-cost').val() : 0,
            currency_id: $('#pickup_currency_id').val() ? $('#pickup_currency_id').val() : 0,
            discount: $('#pickup_discount').val() ? $('#pickup_discount').val() : 0,
            exchange_rates: JSON.parse($('#pickup_exchange_rates').val() || '[]'),
        });
        this.calculateTotalByCurrency(data, EXP.currencies, this.defaultCurrency, 'amount', false, true);
    };
    this.applyDiscount = function(value, discount) {
        var valueFloat = EXP.toFloat(value);
        var dis = 0;
        if (discount) {
            dis = valueFloat * (EXP.toFloat(discount) / 100);
        }
        return valueFloat - dis;
    };
    this.calculateTotals = function() {
        this.initTotals(EXP.currencies);
        this.calculateTotalCourses();
        this.calculateTotalHousing();
        this.calculateTotalExtraPurchases();
        this.calculateTotalPickupCost();
        this.calculateBalance();
        this.showTotals(this.totals, '#totals');
        this.showTotals(this.paidTotals, '#paidTotals');
    };
};
