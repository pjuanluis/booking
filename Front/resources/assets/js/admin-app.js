$(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.dpinline').dPickerInline();
    $('.btn-checkbox').btnCheckbox();
    $('.iCheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });
    $(".select2").select2();
    $('.dpicker:not(:disabled):not([readonly])').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
    });
    $('.dpicker-local:not(:disabled):not([readonly])').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy'
    });
    $('.timepicker').timepicker({
        showInputs: false,
        showMeridian: false
    });
    $('.btn-delete').on('click', function(e) {
        var self = this;
        e.preventDefault();
        swal({
            title: '¿Quieres eliminar el registro?',
            text: '¡No podrás recuperarlo!',
            type: 'warning',
            showCancelButton: true,
            cancelButtonText: '¡No, cancelar!',
            confirmButtonText: '¡Sí, eliminar!',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonClass: 'btn btn-success',
            buttonsStyling: false
        }).then(function(result) {
            if (result) {
                actionWhenOk(self)
            }
        }).catch(swal.noop);
    });
    // Datetimpicker
    $.each($('.datetimepicker'), function(k, dtp) {
        var dataFormat = dtp.getAttribute('data-format'),
            options = {};

        if (dataFormat) {
            options['format'] = dataFormat
        }

        $(dtp).datetimepicker(options);
    });
    //Full calendar in schedules
    var $calendarDateFilter = $('<input>', {
        id: 'calendar-date-filter',
        type: 'text',
        style: 'width: 1px; border: none;'
    });
    $('.list-view').on('change', function (e) {
        if (this.checked) {
            $calendarDateFilter.val('');
            $('#schedules-calendar').empty();
            $('#schedules-list').removeClass('hidden');
        }
    });
    $('.calendar-view').on('change', function (e) {
        if (this.checked) {
            var $scheduleCalendar = $('#schedules-calendar'),
                $calendar = $('<div>', {
                    id: 'calendar',
                    class: 'fc-day-cursor-pointer'
                });
            // filter date change event
            $calendarDateFilter.on('change', function(e) {
                $calendar.fullCalendar('gotoDate', e.currentTarget.value);
                $(this).datepicker('hide');
            });
            $scheduleCalendar.empty();
            // Add calendar
            $scheduleCalendar.append($calendar);
            $('#schedules-list').addClass('hidden');

            $calendar.fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'agendaWeek,agendaDay,listWeek'
                },
                firstDay: 1,
                defaultDate: moment(),
                defaultView: 'agendaWeek',
                slotEventOverlap: true,
                allDaySlot: false,
                navLinks: true, // can click day/week names to navigate views
                editable: false,
                eventLimit: true, // allow "more" link when too many events
                dayClick: function (date, allDay, jsEvent, view) {
                    if (allDay) {
                        $calendar.fullCalendar('changeView', 'agendaDay');
                        $calendar.fullCalendar('gotoDate', date.format());
                    }
                },
                events: function (start, end, timezone, callback) {
                    $('#schedules-calendar #calendar').ploading({
                        action: 'show',
                        spinnerHTML: '<i></i>',
                        spinnerClass: 'fa fa-spinner fa-spin p-loading-fontawesome'
                    });
                    $.ajax({
                        url: $scheduleCalendar.attr("data-url"),
                        dataType: 'json',
                        data: {
                            from: start.format(),
                            to: end.format()
                        },
                        success: function (result) {
                            var events = [],
                                resultSize = result.data.length;

                            for (var i = 0; i < resultSize; i++) {
                                var title = result.data[i].place.data.name + '\n';
                                title += result.data[i].responsible.data.full_name + '\n';
                                title += result.data[i].subject;

                                events.push({
                                    title:  title,
                                    start: result.data[i].begin_at,
                                    end: result.data[i].finish_at,
                                    description: createTemplateScheduleDescription(result.data[i], ($scheduleCalendar.attr('data-url-edit') || ''))
                                });
                            }

                            callback(events);
                            $('#schedules-calendar #calendar').ploading({action: 'hide'});
                        }
                    });
                },
                eventRender: function(event, element) {
                    element.qtip({
                        prerender: true,
                        content: {
                            text: event.description,
                            button: 'Close'
                        },
                        show: {
                            event: 'click',
                            solo: true
                         },
                         hide: {
                            event: 'unfocus click',
                            // inactive: 6000,
                            effect: function(offset) {
                               $(this).hide('fast');
                            }
                         },
                        position: {
                            my: 'bottom center',
                            at: 'top center',
                            // target: element
                            viewport: $(document.body)
                        },
                        style: {
                            classes: 'qtip-light qtip-shadow'
                        },
                    });
                }
            });
            // Add button go to date in calendar
            var $btnGoToDate = $('<button>', {
                class: "fc-button fc-state-default fc-corner-left fc-corner-right"
            });
            $calendarDateFilter.datepicker();
            $btnGoToDate.append($calendarDateFilter)
                .append($('<i>', {
                    class: 'fa fa-calendar'
                }));
            $btnGoToDate.on('click', function(e) {
                $calendarDateFilter.datepicker('show');
            });
            $calendar.find('.fc-toolbar .fc-left')
                .append($btnGoToDate);
        }
    });

    // Mark position in menu
    var $sidebarMenu = $('.sidebar-menu');
    var currentUrl = $sidebarMenu.data('currentUrl');
    $sidebarMenu.find('li a').each(function() {
        var $this = $(this);
        var thisHref = $this.attr('href');
        var thisHrefSize = 0;

        thisHrefSize = thisHref.indexOf('?');
        if (thisHrefSize < 0) {
            thisHrefSize = thisHref.length;
        }
        // Obtener los parámetros
        var thisHrefParams = $this[0].search.substr(1);
        thisHref = thisHref.substring(0, thisHrefSize);

        if (thisHref.startsWith(currentUrl) || currentUrl.startsWith(thisHref)) {
            if (thisHrefParams.trim() === "") {
                $this.parents('li').addClass('active');
            } else if (location.href.indexOf(thisHrefParams) >= 0) {
                $this.parents('li').addClass('active');
            } else {
                $this.parents('li.treeview').addClass('active');
            }
        }
    });
});

/**
 * Create form and submit
 */
function actionWhenOk(self) {
    var $form = $('<form>', {
        action: self.getAttribute('href') || '',
        method: 'post'
    }).append($('<input>', {
        type: 'hidden',
        name: '_method',
        value: self.getAttribute('data-method') || ''
    })).append($('<input>', {
        type: 'hidden',
        name: '_token',
        value: $('meta[name="csrf-token"]').attr('content')
    }));
    $('body').append($form);
    $form.submit();
}

// Trigger2 events
$.fn.trigger2 = function (eventName) {
    return this.each(function () {
        var el = $(this).get(0);
        triggerNativeEvent(el, eventName);
    });
};

function triggerNativeEvent(el, eventName) {
    if (el.fireEvent) {
        el.fireEvent('on' + eventName);
    } else {
        var evt = document.createEvent('Events');
        evt.initEvent(eventName, true, false);
        el.dispatchEvent(evt);
    }
}

// Checkbox as button
$.fn.btnCheckbox = function () {
    return this.each(function () {
        var $checkbox = $(this).find('input');

        setActiveClass($checkbox.get(0));
        // Change event
        $checkbox.on('change', function (e) {
            setActiveClass(this);
        });
    });

    function setActiveClass(tag) {
        var $parent = $(tag).parent();
        if (tag.checked) {
            $parent.addClass('active');
        } else {
            $parent.removeClass('active');
        }
    }
};

// Datepicker inline
$.fn.dPickerInline = function () {
    return this.each(function () {
        var $dpCalendar = $(this.querySelector('.dp-inline__calendar')),
            dpInput = this.querySelector('.dp-inline__input'),
            options = {
                weekStart: 1 // monday on 'english' or 'default'
            },
            multidate = ($dpCalendar.attr('data-multidate') || false) == "true",
            initialDates = (dpInput.value || '').split(",");

        if (multidate) {
            options['multidate'] = multidate;
        }

        $dpCalendar.datepicker(options);
        $dpCalendar.datepicker('setDates', initialDates);
        $dpCalendar.on('changeDate', function () {
            dpInput.value = $dpCalendar.datepicker('getFormattedDate');
        });
    });
};

// Vue
window.VUE_HOOKS = {
    mounted: function () {
        $('input').on('ifChecked', function (event) {
            $(event.target).trigger2('change');
        });
        $('input').on('ifUnchecked', function (event) {
            $(event.target).trigger2('change');
        });
    }
};

function createTemplateScheduleDescription(data, urlEdit) {
    var description = '',
        clients = '',
        clientsSize = 0;

    description = '<div class="qtip-content__header"> \
        <h3 class="qtip-content__title">' + data.subject + '</h3> \
        <a class="btn qtip-button__circle qtip-button--edit" href="' + urlEdit + '/' + data.id + '/edit"> \
            <i class="fa fa-pencil" aria-hidden="true"></i> \
        </a> \
    </div> \
    <div class="qtip-content__body"> \
        <p>' + moment(data.begin_at).format('dddd, MMM Do, YYYY h:mm:ss a') + '</p>';

        if (data.schedulable_type == 'App\\Client') {
            description += 'Client <span class="qtip-data">' + data.schedulable.data.full_name + '</span> \
            Email <span class="qtip-data">' + data.schedulable.data.email + '</span>';
        } else if (data.schedulable_type == 'App\\Group') {
            description += 'Group <span class="qtip-data">' + data.schedulable.data.name + '</span>';
            // Get clients
            clientsSize = data.schedulable.data.clients.data.length;
            if (clientsSize > 0 ) {
                clients += 'Clients <span class="qtip-data"><ul class="list-group list-group--clients">';
                for (var j = 0; j < clientsSize; j++) {
                    clients += '<li class="list-group-item">' +
                    data.schedulable.data.clients.data[j].first_name + ' ' +
                    data.schedulable.data.clients.data[j].last_name + '</li>';
                }
                clients += '</ul></span>';
            }
        }
        description += 'Responsible <span class="qtip-data">' + data.responsible.data.full_name + '</span> \
        Place <span class="qtip-data">' + data.place.data.name + '</span>' + clients +
    '</div>'; // .qtip-content__body

    return description;
}

// Starts with polyfill
if (!String.prototype.startsWith) {
    Object.defineProperty(String.prototype, 'startsWith', {
        enumerable: false,
        configurable: false,
        writable: false,
        value: function (searchString, position) {
            position = position || 0;
            return this.lastIndexOf(searchString, position) === position;
        }
    });
}


// BirthDayPicker
function addHTML($datepicker, lang) {
    var $picker = $datepicker.data('datepicker').picker,
        $pickerHead = $picker.find('thead'),
        $years = null,
        $months = null,
        newHTML = '',
        now = moment(),
        currentDate = null;

        if ($datepicker.val() != '') {
            currentDate = moment($datepicker.val(), 'DD-MM-YYYY');
        } else {
            currentDate = moment();
        }

        newHTML = '\
            <tr>\
                <th colspan="100">\
                    <select id="select-year" class="select2"></select>\
                    <select id="select-month"></select>\
                </th>\
            </tr>\
        ';
    $pickerHead.find('tr:eq(1)')
        .after(newHTML);

    // Add years
    $years = $picker.find('#select-year');
    for (var i = 1850; i <= now.year(); i++) {
        var attr = { text: i };

        if (i === currentDate.year()) {
            attr['selected'] = 'selected';
        }

        $years.append($('<option>', attr));
    }

    // Add months
    $months = $picker.find('#select-month');
    for (var i = 0; i < 12; i++) {
        var attr = {
            value: i + 1,
            text: $.fn.datepicker.dates[lang]['months'][i]
         };

        if (i === currentDate.month()) {
            attr['selected'] = 'selected';
        }

        $months.append($('<option>', attr));
    }

    // console.log(moment());
}

function addEvents($datepicker) {
    var $picker = $datepicker.data('datepicker').picker,
        $years = null,
        $months = null;

    $years = $picker.find('#select-year');
    $months = $picker.find('#select-month');

    $years.on('change', function(e) {
        var newDate = moment();

        newDate.year(this.value);
        newDate.month($months.val() - 1);
        newDate.day(1);

        $datepicker.datepicker('update', newDate.format('DD-MM-YYYY'));
    });

    $months.on('change', function(e) {
        var newDate = moment();

        newDate.year($years.val());
        newDate.month(this.value - 1);
        newDate.day(1);

        $datepicker.datepicker('update', newDate.format('DD-MM-YYYY'));
    });
}

var $bdPicker = $('.birthdate-picker');
if ($bdPicker.length > 0) {
    $bdPicker.datepicker({
        format: 'dd-mm-yyyy',
        forceParse: false
    });
    addHTML($bdPicker, 'en');
    addEvents($bdPicker);
    $bdPicker.datepicker().on('show', function(e) {
        var $picker = $bdPicker.data('datepicker').picker;

        $picker.off();

        $(document).on('click', '.datepicker .day', function(e) {
            var $target = $(e.currentTarget);
            var date = new Date($target.data('date'));
            var configDate = {
                year: date.getUTCFullYear(),
                month: date.getUTCMonth(),
                date: date.getUTCDate()
            };
            var $years = $picker.find('#select-year');
            var $months = $picker.find('#select-month');

            $years.val(configDate.year);
            $months.val(configDate.month + 1);

            date = moment(configDate);
            $bdPicker.datepicker('update', date.format('DD-MM-YYYY'));
        });
    });
}
