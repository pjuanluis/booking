function clients_btnAddCommand() {
    /**
     * 
     * @param {*} title 
     * @param {*} message 
     */
    function showMessageInformation(title, message) {
        return swal({
            title: title,
            type: 'info',
            html: message,
            allowOutsideClick: false,
            showCloseButton: true,
            showCancelButton: false,
            focusConfirm: true,
            confirmButtonText: '<i class="fa fa-check-circle"></i> Ok!',
        });
    }

    /**
     * 
     * @param {*} selector 
     */
    function scrollToFirst(selector) {
        var offset = $(selector).first().offset();

        if (offset == void 0) {
            return;
        }

        $([document.documentElement, document.body]).animate({
            scrollTop: offset.top - 50
        }, 'fast');
    }

    function showError(name, error) {
        var $field = null;
        var $formGroup = null;

        $field = $('[name="' + name + '"]');
        $formGroup = $field.closest('.form-group');

        $formGroup.addClass('has-error');
        $formGroup.find('.help-block').text(error);
    }

    /**
     * 
     */
    function showErrorInTravelInformation(v, k) {
        var splited = k.split('.');
        var selector = splited[0] + '[' + splited[1] + '][' + splited[2] +']';
        var error = v[0].replace(splited[0] + '.' + splited[1] + '.', '');

        showError(selector, error);
    }

    /**
     * 
     */
    function openCollapsedIfHasErrors() {
        _.forEach($('.collapse:not(.in)'), function(v) {
            var selector = '.btn-toggle-travelinformation[data-target="#' + v.id + '"]';
            $(selector).trigger('click');
        });
    }

    /**
     * 
     * @param {*} response 
     */
    function showErrors(response) {
        $('.has-error').removeClass('has-error');
        _.forEach(response.errors, function(v, k) {
            if (k.startsWith('travel_information.')) {
                showErrorInTravelInformation(v, k);
                return true;
            }
            showError(k, v[0]);
        });
        openCollapsedIfHasErrors();
    }

    /**
     * Main
     */
    $(function() {
        var $btAddCommand = $('#btn-add-command');
        
        $btAddCommand.click(function(e) {
            var $clientForm = $('#client-form');

            e.preventDefault();
            e.stopPropagation();

            $.post($clientForm.attr('action'), $clientForm.serialize(), function(response) {
                if (response.status_code != 200 && response.status_code != 201) {
                    showErrors(response);
                    showMessageInformation(
                        'Missing data',
                        'To create a command is necessary the client information.'
                    ).then(function(result){
                        scrollToFirst('.has-error');
                    });
                } else {
                    window.location = e.currentTarget.getAttribute('data-goto-url');
                }
            }, 'json');
        });
    });
}