if (window.EXP === void 0) {
    window.EXP = {};
}

EXP.Downloader = function(config, callback, errorHandler) {
    var init = function(config, callback, errorHandler) {
        var response = validateConfig(config);
        if (!response[0]) {
            alert(response[1]);
            return;
        }
        initEventHandler(config, callback, errorHandler);
    };

    var validateConfig = function(config) {
        var errorMessage = '';
        if (!config.el) {
            errorMessage = 'El componente Downloader necesita la propiedad "el"';
        } else if (!config.url) {
            errorMessage = 'El componente Downloader necesita la propiedad "url"';
        } else if (!config.filename) {
            errorMessage = 'El componente Downloader necesita la propiedad "filename"';
        } else if (!config.mimetype) {
            errorMessage = 'El componente Downloader necesita la propiedad "mimetype"';
        }
        if (errorMessage) {
            return [false, errorMessage];
        }
        return [true];
    };

    var initEventHandler = function(config, callback, errorHandler) {
        $(config.el).on('click', function(e) {
            e.preventDefault();
            // Abrir loader
            $('body').ploading({ containerClass: 'p-loading-downloader', action: 'show' });
            $('.p-loading-downloader').css({
                'position': 'fixed',
                'left': 0,
                'right': 0,
                'top': 0,
                'bottom': 0,
                'z-index': 10000000
            });
            var request = new XMLHttpRequest();
            request.open('POST', config.url, true);
            request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
            request.responseType = 'blob';
            request.onload = function() {
                // Only handle status code 200
                if (request.status === 200) {
                    // Try to find out the filename from the content disposition `filename` value
                    var disposition = request.getResponseHeader('content-disposition');
                    var matches = /"([^"]*)"/.exec(disposition);
                    var filename = (matches != null && matches[1] ? matches[1] : (_.isFunction(config.filename) ? config.filename() : config.filename));

                    // The actual download
                    var blob = new Blob([request.response], { type: config.mimetype });
                    var link = document.createElement('a');
                    link.href = window.URL.createObjectURL(blob);
                    link.download = filename;

                    document.body.appendChild(link);

                    link.click();

                    document.body.removeChild(link);
                } else if (errorHandler && _.isFunction(errorHandler)) {
                    errorHandler();
                } else {
                    alert('A ocurrido un error al descargar el archivo.');
                }

                // Ocultar el loader
                $('body').ploading({ containerClass: 'p-loading-downloader', action: 'hide' });
            };
            if (callback && _.isFunction(callback)) {
                // Hacer la petición con datos extras en el callback
                callback(request);
            } else {
                // Hacer la petición
                request.send();
            }
        });
    }

    init(config, callback, errorHandler);
};
