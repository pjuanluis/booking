<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style type="text/css" rel="stylesheet" media="all">
        /* Media Queries */
        @media only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }
        body {font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 0; padding: 0; width: 100%; background-color: #F2F4F6;}
        .email-wrapper {width: 100%; margin: 0; padding: 0; background-color: #F2F4F6;}
        .email-masthead {padding: 25px 0; text-align: center;}
        .email-masthead_name {font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 16px; font-weight: bold; color: #2F3133; text-decoration: none; text-shadow: 0 1px 0 white;}
        .email-body {width: 100%; margin: 0; padding: 0; border-top: 1px solid #EDEFF2; border-bottom: 1px solid #EDEFF2; background-color: #FFF;}
        .email-body_inner {width: auto; max-width: 570px; margin: 0 auto; padding: 0;}
        .email-body_cell {font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; padding: 35px;}
        .email-footer {width: auto; max-width: 570px; margin: 0 auto; padding: 0; text-align: center;}
        .email-footer_cell {font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; color: #AEAEAE; padding: 35px; text-align: center;}
        .body_action {width: 100%; margin: 30px auto; padding: 0; text-align: center;}
        .body_sub {margin-top: 25px; padding-top: 25px; border-top: 1px solid #EDEFF2;}
        .anchor {color: #3869D4;}
        .header-1 {margin-top: 0; color: #2F3133; font-size: 19px; font-weight: bold; text-align: left;}
        .paragraph {margin-top: 0; color: #74787E; font-size: 16px; line-height: 1.5em;}
        .paragraph-sub {margin-top: 0; color: #74787E; font-size: 12px; line-height: 1.5em;}
        .paragraph-center {text-align: center;}
        .button {display: block; display: inline-block; width: 200px; min-height: 20px; padding: 10px;
                 background-color: #3869D4; border-radius: 3px; color: #ffffff; font-size: 15px; line-height: 25px;
                 text-align: center; text-decoration: none; -webkit-text-size-adjust: none; margin-bottom: 10px;}

        .button--green {background-color: #22BC66;}
        .button--red {background-color: #dc4d2f;}
        .button--blue {background-color: #3869D4;}
    </style>
</head>
<body>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="email-wrapper" align="center">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <!-- Logo -->
                    <tr>
                        <td class="email-masthead">
                            <a class="email-masthead_name" href="{{ url('/') }}" target="_blank">
                                {{ config('app.name') }}
                            </a>
                        </td>
                    </tr>

                    <!-- Email Body -->
                    <tr>
                        <td class="email-body" width="100%">
                            <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="email-body_cell">
                                        <!-- Greeting -->
                                        @if ($toClient)
                                            <h1 class="header-1">
                                                Hello {{ $command->client_first_name }} {{ $command->client_last_name }}!
                                            </h1>
                                        @else
                                            <h1 class="header-1">
                                                Hello!
                                            </h1>
                                        @endif

                                        @php
                                        $preferences = "";
                                        $totals = [
                                            'MXN' => 0,
                                            'USD' => 0,
                                            'EUR' => 0
                                        ];
                                        if (!empty($command->commandCourses)) {
                                            $preferences .= '<strong>Courses:</strong><br />';
                                            foreach ($command->commandCourses as $r) {
                                                $currencyCode = $r->purchase->currency->code;
                                                // Aplicar el descuento
                                                $cost = $r->cost - ($r->cost * ($r->purchase->discount / 100));
                                                $preferences .= $r->quantity . ' of ' . $r->package->name . ': ' . $cost . " {$currencyCode}" . '<br />';
                                                $totals[$currencyCode] += $cost;
                                            }
                                            $preferences .= '<br />';
                                        }
                                        if (!empty($command->commandHousing)) {
                                            $preferences .= '<strong>Housing:</strong><br />';
                                            foreach ($command->commandHousing as $r) {
                                                $currencyCode = $r->purchase->currency->code;
                                                // Aplicar el descuento
                                                $cost = $r->cost - ($r->cost * ($r->purchase->discount / 100));
                                                $preferences .= $r->housing->name . " - {$r->housingRoom->name}" . ' from ' . (new \Carbon\Carbon($r->from_date))->format('d-m-Y') . ' to ' . (new \Carbon\Carbon($r->to_date))->format('d-m-Y') . ': ' . $cost . " {$currencyCode}" . '<br />';
                                                $totals[$currencyCode] += $cost;
                                            }
                                            $preferences .= '<br />';
                                        }
                                        if ($command->extraPurchases()->get()->count() > 0) {
                                            $preferences .= '<strong>Extra purchases:</strong><br />';
                                            foreach ($command->extraPurchases()->get() as $ep) {
                                                $currencyCode = $ep->purchasable->currency->code;
                                                // Aplicar el descuento
                                                $cost = $ep->amount - ($ep->amount * ($ep->purchasable->discount / 100));
                                                $preferences .= "{$ep->concept}: {$cost} {$currencyCode}<br />";
                                                $totals[$currencyCode] += $cost;
                                            }
                                            $preferences .= '<br />';
                                        }
                                        if (!empty($command->pickup_id)) {
                                            $currencyCode = $command->purchase->currency->code;
                                            $cost = $command->pickup_cost - ($command->pickup_cost * ($command->purchase->discount / 100));
                                            $preferences .= '<strong>Pickup at:</strong><br />';
                                            $preferences .= $command->pickup->name . ': ' . $cost . " {$currencyCode}";
                                            $totals[$currencyCode] += $cost;
                                            $preferences .= '<br /><br />';
                                        }
                                        $preferences .= '<strong>Total: </strong>';
                                        $totalsTmp = [];
                                        foreach ($totals as $code => $amount) {
                                            // Dejar unicamente los totales mayores a cero
                                            if ($amount > 0) {
                                                $totalsTmp[$code] = $amount;
                                            }
                                        }
                                        $totals = $totalsTmp;
                                        @endphp
                                        @foreach ($totals as $code => $amount)
                                            @php
                                            $preferences .= "{$amount} {$code}";
                                            if (!$loop->last) {
                                                $preferences .= " / ";
                                            }
                                            @endphp
                                        @endforeach

                                        @if ($toClient)
                                            <div>
                                                @if ($isUpdateCommand)
                                                    <p>Your information has been updated.</p>
                                                @else
                                                    <p>Your information has been registered.</p>
                                                @endif
                                                {{-- <table class="body_action" align="center" width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td align="center">
                                                            <a href="{{ $infoLink['url'] }}" class="button button--blue" target="_blank">{{ $infoLink['text'] }}</a>
                                                        </td>
                                                    </tr>
                                                </table> --}}
                                                <br />
                                                <b>Dates:</b> {{ " from " . (new \Carbon\Carbon($command->from_date))->format('d-m-Y') . " to " . (new \Carbon\Carbon($command->to_date))->format('d-m-Y') }}<br /><br />
                                                {!! $preferences !!}
                                            </div>
                                            @if (!$isUpdateCommand)
                                                <div>
                                                    <p>To reserve your place, please pay the total or a deposit here:</p>
                                                    <table class="body_action" align="center" width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td align="center">
                                                                <a href="{{ $paymentLink['url'] }}" class="button button--blue" target="_blank" style="color: white;">{{ $paymentLink['text'] }}</a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            @endif
                                        @else
                                            <div>
                                                @php
                                                    $message = "We have received a new booking from the " . ($command->registration_from === 'admin_site' ? 'admin' : 'site');
                                                    if ($isUpdateCommand) {
                                                        $message = "A booking has been updated";
                                                    }
                                                @endphp
                                                <p>{{ $message }}, to see details please click in the following link <a href="{{ config('services.app_experiencia.url') . '/admin/bookings/' . $command->id }}">See booking</a>
                                                </p><br />
                                                <p><b>Client data:</b></p>
                                                <p>Name: {{ $command->client_first_name . ' ' . $command->client_last_name }}</p>
                                                <p>Email: {{ $command->client_email }}</p>
                                                <p>Comments: {{ $command->client_comments }}</p>
                                                <p>Dates: {{ " from " . (new \Carbon\Carbon($command->from_date))->format('d-m-Y') . " to " . (new \Carbon\Carbon($command->to_date))->format('d-m-Y') }}</p>
                                                <br />
                                                {!! $preferences !!}
                                            </div>
                                        @endif

                                        <!-- Salutation -->
                                        <p class="paragraph">
                                            Regards,<br>{{ config('app.name') }}
                                        </p>

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <!-- Footer -->
                    <tr>
                        <td>
                            <table class="email-footer" align="center" width="570" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="email-footer_cell">
                                        <p class="paragraph-sub">
                                            &copy; {{ date('Y') }}
                                            <a class="anchor" href="{{ url('/') }}" target="_blank">{{ config('app.name') }}</a>.
                                            All rights reserved.
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
