<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The :attribute must be accepted.',
    'active_url'           => 'The :attribute is not a valid URL.',
    'after'                => 'The :attribute must be a date after :date.',
    'after_or_equal'       => 'The :attribute must be a date after or equal to :date.',
    'alpha'                => 'The :attribute may only contain letters.',
    'alpha_dash'           => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num'            => 'The :attribute may only contain letters and numbers.',
    'array'                => 'The :attribute must be an array.',
    'before'               => 'The :attribute must be a date before :date.',
    'before_or_equal'      => 'The :attribute must be a date before or equal to :date.',
    'between'              => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
        'string'  => 'The :attribute must be between :min and :max characters.',
        'array'   => 'The :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'The :attribute field must be true or false.',
    'confirmed'            => 'The :attribute confirmation does not match.',
    'date'                 => 'The :attribute is not a valid date.',
    'date_format'          => 'The :attribute does not match the format :format.',
    'different'            => 'The :attribute and :other must be different.',
    'digits'               => 'The :attribute must be :digits digits.',
    'digits_between'       => 'The :attribute must be between :min and :max digits.',
    'dimensions'           => 'The :attribute has invalid image dimensions.',
    'distinct'             => 'The :attribute field has a duplicate value.',
    'email'                => 'The :attribute must be a valid email address.',
    'exists'               => 'The selected :attribute is invalid.',
    'file'                 => 'The :attribute must be a file.',
    'filled'               => 'The :attribute field must have a value.',
    'image'                => 'The :attribute must be an image.',
    'in'                   => 'The selected :attribute is invalid.',
    'in_array'             => 'The :attribute field does not exist in :other.',
    'integer'              => 'The :attribute must be an integer.',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'ipv4'                 => 'The :attribute must be a valid IPv4 address.',
    'ipv6'                 => 'The :attribute must be a valid IPv6 address.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
        'string'  => 'The :attribute may not be greater than :max characters.',
        'array'   => 'The :attribute may not have more than :max items.',
    ],
    'mimes'                => 'The :attribute must be a file of type: :values.',
    'mimetypes'            => 'The :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => 'The :attribute must be at least :min.',
        'file'    => 'The :attribute must be at least :min kilobytes.',
        'string'  => 'The :attribute must be at least :min characters.',
        'array'   => 'The :attribute must have at least :min items.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'numeric'              => 'The :attribute must be a number.',
    'present'              => 'The :attribute field must be present.',
    'regex'                => 'The :attribute format is invalid.',
    'required'             => 'The :attribute field is required.',
    'required_if'          => 'The :attribute field is required when :other is :value.',
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'The :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => 'The :attribute must be a string.',
    'timezone'             => 'The :attribute must be a valid zone.',
    'unique'               => 'The :attribute has already been taken.',
    'uploaded'             => 'The :attribute failed to upload.',
    'url'                  => 'The :attribute format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'courses.*.package_id' => [
            'required' => 'The course package is required.',
            'exists' => 'The selected course package is invalid.',
        ],
        'courses.*.quantity' => [
            'required' => 'The quantity of the selected course is required.',
            'integer' => 'The quantity of the selected course must be an integer.',
            'min' => 'The quantity of the selected course must be at least :min.',
        ],
        'courses.*.cost' => [
            'required' => 'The cost of the selected course is required.',
            'numeric' => 'The cost of the selected course must be a number.',
            'min' => 'The cost of the selected course must be at least :min.',
        ],
        'courses.*.unit' => [
            'required' => 'The unit of the selected course is required.',
            'in' => 'The unit of the selected course is invalid.',
        ],
        'courses.*.from_date' => [
            'required' => 'The begin date of the course is required.',
            'date_format' => 'The begin date of the course does not match the format :format.',
        ],
        'courses.*.to_date' => [
            'required' => 'The end date of the course is required.',
            'date_format' => 'The end date of the course does not match the format :format.',
            'after_or_equal' => 'The end date of the course must be a date after or equal to begin date.',
            'val' => 'The end date of the course must be :quantity :unit after to begin date.',
        ],

        'housing.*.housing_room_package_id' => [
            'required' => 'The housing room package is required.',
            'exists' => 'The selected housing room package is invalid.',
            'unavailable' => 'The selected housing room has full ocupancy in the selected dates.',
        ],
        'housing.*.quantity' => [
            'required' => 'The quantity of the selected housing room package is required.',
            'integer' => 'The quantity of the selected housing room package must be an integer.',
            'min' => 'The quantity of the selected housing room package must be at least :min.',
        ],
        'housing.*.cost' => [
            'required' => 'The cost of the selected housing room package is required.',
            'numeric' => 'The cost of the selected housing room package must be a number.',
            'min' => 'The cost of the selected housing room package must be at least :min.',
        ],
        'housing.*.unit' => [
            'required' => 'The unit of the selected housing room package is required.',
            'in' => 'The unit of the selected housing room package is invalid.',
            'exists' => 'The unit of the selected housing room package is invalid.',
        ],
        'housing.*.from_date' => [
            'required' => 'The begin date of the housing is required.',
            'date_format' => 'The begin date of the housing does not match the format :format.',
        ],
        'housing.*.to_date' => [
            'required' => 'The end date of the housing is required.',
            'date_format' => 'The end date of the housing does not match the format :format.',
            'after' => 'The end date of the housing must be a date after the begin date.',
            'val' => 'The end date of the housing must be :quantity :unit after the begin date.',
        ],
        'housing.*.people' => [
            'required' => 'The quantity of people is required.',
            'integer' => 'The quantity of people must be an integer.',
            'min' => 'The quantity of people must be at least :min.',
        ],

        'file' => [
            'image' => 'The file must be an image.',
            'excel' => 'The file must be an excel file',
            'size' => 'The file must be less than :size'
        ],
        'accommodation' => [
            'room_id' => [
                'required' => 'Room is required',
                'in' => 'The selected room is invalid for the specified command housing.',
                'taken' => 'The selected room is taken between the dates of the specified command housing.',
            ]
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'client_first_name' => "Client first name",
        'client_last_name' => "Client last name",
        'client_email' => "Client email",
        'pickup_at_id' => "Transfer service",
        'pickup_id' => "Transfer service",
        'package_id' => "Package",
        'housing_room_package_id' => "Package of the housing room",
        'unit' => 'Unit',
        'quantity' => 'Quantity',
        'client_id' => 'Client',
        'agency_id' => 'Agency',
        'from_date' => 'Begin date',
        'to_date' => 'End date',
        'day' => 'Day|Days',
        'week' => 'Week|Weeks',
        'month' => 'month|months',
        'housing_room_id' => 'Housing room',
        'housing_rooms' => 'Housing room',
        'command_housing_id' => 'Command housing',
        'currency_id' => 'currency',
        'method_payment_id' => 'payment method',
        'consumption_center_id' => 'consumption center',
        'status_id' => 'status',
        'payment_status_id' => 'payment status',
        'room_type_id' => 'room type',
        'housing_rates.*.quantity' => 'quantity',
        'housing_rates.*.unit' => 'unit',
        'housing_rates.*.unit_cost' => 'unit cost',
        'housing_rates.*.from' => 'from',
        'housing_rates.*.to' => 'to',
        'course_rates.*.unit_cost' => 'unit cost',
        'course_rates.*.from' => 'from',
        'course_rates.*.to' => 'to',
        'experiences.*' => 'experience',
        'night' => 'Night|Nights',
        'place_id' => 'place',
        'responsible_id' => 'responsible',
        'command_bills.*.reference_number' => 'reference number',
        'command_bills.*.amount' => 'amount',
        'command_bills.*.currency_id' => 'currency',
        'gender_id' => 'gender',
        'purchases.*.date' => 'date',
        'purchases.*.reference_number' => 'reference number',
        'purchases.*.concept' => 'concept',
        'purchases.*.amount' => 'amount',
        'purchases.*.discount' => 'discount',
        'purchases.*.currency_id' => 'currency',
        'purchases.*.consumption_center_id' => 'consumption center',
        'travel_information.*.arrival_time' => 'arrival hour',
        'travel_information.*.arrival_date' => 'arrival day',
        'travel_information.*.departure_time' => 'departure hour',
        'travel_information.*.departure_date' => 'departure day',
        'travel_information.*.arrival_place' => 'arrival place',
        'travel_information.*.departure_place' => 'departure place',
        'travel_information.*.arrival_carrier' => 'arrival carrier',
        'travel_information.*.departure_carrier' => 'departure carrier',
        'travel_information.*.arrival_travel_number' => 'arrival travel number',
        'travel_information.*.departure_travel_number' => 'departure travel number',
        'pickup_cost' => 'transfer cost',
        'pickup_discount' => 'transfer discount',
        'country_id' => 'Country',
    ],

];
