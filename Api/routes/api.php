<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$api = app('Dingo\Api\Routing\Router');
$api->version('v1', ['middleware' => 'auth_client'], function ($api) {
    $api->post('bookings', ['as' => 'bookings.store', 'uses' => 'App\Http\Controllers\CommandController@book']);
    $api->get('packages', ['as' => 'packages', 'uses' => 'App\Http\Controllers\PackageController@index']);
    $api->get('packages/groupbyexperiences', ['as' => 'packages.groupbyexperiences', 'uses' => 'App\Http\Controllers\PackageController@groupByExperiences']);
    $api->get('packages/{id}', ['as' => 'packages.show', 'uses' => 'App\Http\Controllers\PackageController@show']);
    $api->get('housing/occupancy', ['as' => 'housing.occupancy', 'uses' => 'App\Http\Controllers\HousingController@occupancyDates']);
    $api->get('housing/availability', ['as' => 'housing.availability', 'uses' => 'App\Http\Controllers\HousingController@availability']);
    $api->get('commands/getbytoken/{token}', ['as' => 'commands.getbytoken', 'uses' => 'App\Http\Controllers\CommandController@getByToken']);
    $api->post('commands/{command}/payments', ['as' => 'commands.payments.store', 'uses' => 'App\Http\Controllers\CommandController@storePayment']);
    // Catálogos
    $api->get('catalogues/{catalogue}', ['as' => 'catalogues', 'uses' => 'App\Http\Controllers\CatalogueController@index']);
    $api->get('catalogues/{catalogue}/{id}', ['as' => 'catalogues', 'uses' => 'App\Http\Controllers\CatalogueController@show']);
    $api->post('payments/conekta', ['as' => 'payments.conekta', 'uses' => 'App\Http\Controllers\PaymentController@conekta']);

    // Clientes públicos
    $api->post('public-clients', ['as' => 'clients.storePublic', 'uses' => 'App\Http\Controllers\ClientController@storePublic']);
});
$api->version('v1', ['middleware' => 'auth:api'], function ($api) {
    $api->get('commands', ['as' => 'commands.index', 'uses' => 'App\Http\Controllers\CommandController@index']);
    $api->get('commands/count', ['as' => 'commands.count', 'uses' => 'App\Http\Controllers\CommandController@index']);
    $api->get('commands/generate-csv', ['as' => 'commands.generatecsv', 'uses' => 'App\Http\Controllers\CommandController@generateCSV']);
    $api->get('commands/{command}', ['as' => 'commands.show', 'uses' => 'App\Http\Controllers\CommandController@show']);
    $api->get('commands/{command}/logs', ['as' => 'commands.logs', 'uses' => 'App\Http\Controllers\CommandController@logs']);
    $api->post('commands', ['as' => 'commands.store', 'uses' => 'App\Http\Controllers\CommandController@store']);
    $api->put('commands/{command}', ['as' => 'commands.update', 'uses' => 'App\Http\Controllers\CommandController@update']);
    $api->delete('commands/{command}', ['as' => 'commands.destroy', 'uses' => 'App\Http\Controllers\CommandController@destroy']);
    $api->put('commands/{command}/accommodations', ['as' => 'commands.accommodations.store', 'uses' => 'App\Http\Controllers\CommandController@storeAccommodation']);
    $api->put('commands/payments/mark-as-invoiced', ['as' => 'commands.payments.mark_as_invoiced', 'uses' => 'App\Http\Controllers\CommandController@markPaymentsAsInvoiced']);
    // Usuarios
    $api->get('users', ['as' => 'users', 'uses' => 'App\Http\Controllers\UserController@index']);
    $api->get('users/me', ['as' => 'users.me', 'uses' => 'App\Http\Controllers\UserController@me']);
    $api->get('users/{user}', ['as' => 'users.show', 'uses' => 'App\Http\Controllers\UserController@show']);
    $api->get('users/{user}/show', ['as' => 'users.showUser', 'uses' => 'App\Http\Controllers\UserController@showUser']);
    $api->post('users', ['as' => 'users.store', 'uses' => 'App\Http\Controllers\UserController@store']);
    $api->put('users/{user}', ['as' => 'users.update', 'uses' => 'App\Http\Controllers\UserController@update']);
    $api->put('users/{user}/remember-token', ['as' => 'users.remember_token.update', 'uses' => 'App\Http\Controllers\UserController@updateRememberToken']);
    $api->delete('users/{user}', ['as' => 'users.destroy', 'uses' => 'App\Http\Controllers\UserController@destroy']);
    // Clientes
    $api->get('clients', ['as' => 'clients', 'uses' => 'App\Http\Controllers\ClientController@index']);
    $api->post('clients', ['as' => 'clients.store', 'uses' => 'App\Http\Controllers\ClientController@store']);
    $api->get('clients/{client}', ['as' => 'clients.show', 'uses' => 'App\Http\Controllers\ClientController@show']);
    $api->put('clients/{client}', ['as' => 'clients.update', 'uses' => 'App\Http\Controllers\ClientController@update']);
    $api->delete('clients/{client}', ['as' => 'clients.destroy', 'uses' => 'App\Http\Controllers\ClientController@destroy']);
    // Residencias
    $api->get('residences', ['as' => 'residences', 'uses' => 'App\Http\Controllers\ResidenceController@index']);
    $api->get('residences/{residence}', ['as' => 'residences.show', 'uses' => 'App\Http\Controllers\ResidenceController@show']);
    $api->post('residences', ['as' => 'residences.store', 'uses' => 'App\Http\Controllers\ResidenceController@store']);
    $api->put('residences/{residence}', ['as' => 'residences.update', 'uses' => 'App\Http\Controllers\ResidenceController@update']);
    $api->delete('residences/{residence}', ['as' => 'residences.destroy', 'uses' => 'App\Http\Controllers\ResidenceController@destroy']);
    // Cuartos
    $api->get('rooms', ['as' => 'rooms', 'uses' => 'App\Http\Controllers\RoomController@index']);
    $api->get('rooms/accommodations', ['as' => 'rooms.accommodations', 'uses' => 'App\Http\Controllers\RoomController@showAccommodations']);
    $api->get('rooms/{room}', ['as' => 'rooms.show', 'uses' => 'App\Http\Controllers\RoomController@show']);
    $api->post('rooms', ['as' => 'rooms.store', 'uses' => 'App\Http\Controllers\RoomController@store']);
    $api->put('rooms/{room}', ['as' => 'rooms.update', 'uses' => 'App\Http\Controllers\RoomController@update']);
    $api->delete('rooms/{room}', ['as' => 'rooms.destroy', 'uses' => 'App\Http\Controllers\RoomController@destroy']);
    // Grupos
    $api->get('groups', ['as' => 'groups', 'uses' => 'App\Http\Controllers\GroupController@index']);
    $api->get('groups/{id}', ['as' => 'groups.show', 'uses' => 'App\Http\Controllers\GroupController@show']);
    $api->post('groups', ['as' => 'groups.store', 'uses' => 'App\Http\Controllers\GroupController@store']);
    $api->put('groups/{id}', ['as' => 'groups.update', 'uses' => 'App\Http\Controllers\GroupController@update']);
    $api->delete('groups/{id}', ['as' => 'groups.destroy', 'uses' => 'App\Http\Controllers\GroupController@destroy']);
    // Horarios
    $api->get('schedules', ['as' => 'schedules', 'uses' => 'App\Http\Controllers\ScheduleController@index']);
    $api->get('schedules/{id}', ['as' => 'schedules.show', 'uses' => 'App\Http\Controllers\ScheduleController@show']);
    $api->post('schedules', ['as' => 'schedules.store', 'uses' => 'App\Http\Controllers\ScheduleController@store']);
    $api->put('schedules/{id}', ['as' => 'schedules.update', 'uses' => 'App\Http\Controllers\ScheduleController@update']);
    $api->put('schedule-date/{id}/class-status', ['as' => 'schedule-date.update.class-status', 'uses' => 'App\Http\Controllers\ScheduleController@updateClassStatus']);
    $api->delete('schedules/{id}', ['as' => 'schedules.destroy', 'uses' => 'App\Http\Controllers\ScheduleController@destroy']);
    // Personas
    $api->get('people', ['as' => 'people', 'uses' => 'App\Http\Controllers\PersonController@index']);
    $api->get('people/{id}', ['as' => 'people.show', 'uses' => 'App\Http\Controllers\PersonController@show']);
    $api->post('people', ['as' => 'people.store', 'uses' => 'App\Http\Controllers\PersonController@store']);
    $api->put('people/{id}', ['as' => 'people.update', 'uses' => 'App\Http\Controllers\PersonController@update']);
    $api->delete('people/{id}', ['as' => 'people.destroy', 'uses' => 'App\Http\Controllers\PersonController@destroy']);


    // Lugares
    $api->get('places', ['as' => 'places', 'uses' => 'App\Http\Controllers\PlaceController@index']);
    $api->get('places/{id}', ['as' => 'places.show', 'uses' => 'App\Http\Controllers\PlaceController@show']);
    $api->post('places', ['as' => 'places.store', 'uses' => 'App\Http\Controllers\PlaceController@store']);
    $api->put('places/{id}', ['as' => 'places.update', 'uses' => 'App\Http\Controllers\PlaceController@update']);
    $api->delete('places/{id}', ['as' => 'places.destroy', 'uses' => 'App\Http\Controllers\PlaceController@destroy']);

    // Importaciones
    $api->get('imports', ['as' => 'imports', 'uses' => 'App\Http\Controllers\ImportController@index']);
    $api->post('imports', ['as' => 'imports.store', 'uses' => 'App\Http\Controllers\ImportController@store']);
    $api->get('imports/{id}', ['as' => 'imports.show', 'uses' => 'App\Http\Controllers\ImportController@show']);
    // Agencias
    $api->get('agencies', ['as' => 'agencies', 'uses' => 'App\Http\Controllers\AgencyController@index']);
    $api->post('agencies', ['as' => 'agencies.store', 'uses' => 'App\Http\Controllers\AgencyController@store']);
    $api->get('agencies/{id}', ['as' => 'agencies.show', 'uses' => 'App\Http\Controllers\AgencyController@show']);
    $api->put('agencies/{id}', ['as' => 'agencies.update', 'uses' => 'App\Http\Controllers\AgencyController@update']);
    $api->delete('agencies/{id}', ['as' => 'agencies.destroy', 'uses' => 'App\Http\Controllers\AgencyController@destroy']);
    // Pagos
    $api->post('payments', ['as' => 'payments.store', 'uses' => 'App\Http\Controllers\PaymentController@store']);
    // Moneda
    $api->get('currencies', ['as' => 'currencies.index', 'uses' => 'App\Http\Controllers\CurrencyController@index']);
    $api->post('currencies', ['as' => 'currencies.store', 'uses' => 'App\Http\Controllers\CurrencyController@store']);
    $api->get('currencies/{id}', ['as' => 'currencies.show', 'uses' => 'App\Http\Controllers\CurrencyController@show']);
    $api->put('currencies/{id}', ['as' => 'currencies.update', 'uses' => 'App\Http\Controllers\CurrencyController@update']);
    $api->delete('currencies/{id}', ['as' => 'currencies.destroy', 'uses' => 'App\Http\Controllers\CurrencyController@destroy']);
    // Métodos de pago
    $api->get('methods-payment', ['as' => 'methods-payment.index', 'uses' => 'App\Http\Controllers\MethodPaymentController@index']);
    // Statuses
    $api->get('statuses', ['as' => 'statuses.index', 'uses' => 'App\Http\Controllers\StatusController@index']);
    // Centros de consumo
    $api->get('consumption-centers', ['as' => 'consumption-centers.index', 'uses' => 'App\Http\Controllers\ConsumptionCenterController@index']);
    $api->post('consumption-centers', ['as' => 'consumption-centers.store', 'uses' => 'App\Http\Controllers\ConsumptionCenterController@store']);
    $api->get('consumption-centers/{consumptionCenter}', ['as' => 'consumption-centers.show', 'uses' => 'App\Http\Controllers\ConsumptionCenterController@show']);
    $api->put('consumption-centers/{consumptionCenter}', ['as' => 'consumption-centers.update', 'uses' => 'App\Http\Controllers\ConsumptionCenterController@update']);
    $api->delete('consumption-centers/{consumptionCenter}', ['as' => 'consumption-centers.destroy', 'uses' => 'App\Http\Controllers\ConsumptionCenterController@destroy']);
    $api->put('consumption-centers/{consumptionCenter}/activate', ['as' => 'consumption-centers.activate', 'uses' => 'App\Http\Controllers\ConsumptionCenterController@activate']);

    // Caja chica
    $api->get('petty-cashes', ['as' => 'petty-cashes.index', 'uses' => 'App\Http\Controllers\PettyCashController@index']);
    $api->post('petty-cashes', ['as' => 'petty-cashes.store', 'uses' => 'App\Http\Controllers\PettyCashController@store']);
    $api->put('petty-cashes/{pettycash}', ['as' => 'petty-cashes.update', 'uses' => 'App\Http\Controllers\PettyCashController@update']);
    $api->get('petty-cashes/{pettycash}', ['as' => 'petty-cashes.show', 'uses' => 'App\Http\Controllers\PettyCashController@show']);
    $api->delete('petty-cashes/{pettycash}', ['as' => 'petty-cashes.destroy', 'uses' => 'App\Http\Controllers\PettyCashController@destroy']);

    // Catálogo de hospedaje
    $api->get('housing', ['as' => 'housing', 'uses' => 'App\Http\Controllers\HousingController@index']);
    $api->post('housing', ['as' => 'housing.store', 'uses' => 'App\Http\Controllers\HousingController@store']);
    $api->put('housing/{housing}', ['as' => 'housing.update', 'uses' => 'App\Http\Controllers\HousingController@update']);
    $api->get('housing/{housing}', ['as' => 'housing.show', 'uses' => 'App\Http\Controllers\HousingController@show']);
    $api->delete('housing/{housing}', ['as' => 'housing.destroy', 'uses' => 'App\Http\Controllers\HousingController@destroy']);
    $api->put('housing/{housing}/activate', ['as' => 'housing.activate', 'uses' => 'App\Http\Controllers\HousingController@activate']);

    // Housing rooms
    $api->post('housing/{housing}/rooms',['as' => 'housing.rooms.store', 'uses' => 'App\Http\Controllers\HousingRoomController@store']);
    $api->get('housing-rooms/{room}',['as' => 'housing.rooms.show', 'uses' => 'App\Http\Controllers\HousingRoomController@show']);
    $api->put('housing-rooms/{room}',['as' => 'housing.rooms.update', 'uses' => 'App\Http\Controllers\HousingRoomController@update']);
    $api->delete('housing-rooms/{room}',['as' => 'housing.rooms.destroy', 'uses' => 'App\Http\Controllers\HousingRoomController@destroy']);
    $api->put('housing-rooms/{room}/activate',['as' => 'housing.rooms.activate', 'uses' => 'App\Http\Controllers\HousingRoomController@activate']);

    // Room type
    $api->get('room-types', ['as' => 'room-types.index', 'uses' => 'App\Http\Controllers\RoomTypeController@index']);
    $api->post('room-types', ['as' => 'room-types.store', 'uses' => 'App\Http\Controllers\RoomTypeController@store']);
    $api->get('room-types/{roomType}', ['as' => 'room-types.show', 'uses' => 'App\Http\Controllers\RoomTypeController@show']);
    $api->put('room-types/{roomType}', ['as' => 'room-types.update', 'uses' => 'App\Http\Controllers\RoomTypeController@update']);
    $api->delete('room-types/{roomType}', ['as' => 'room-types.destroy', 'uses' => 'App\Http\Controllers\RoomTypeController@destroy']);
    $api->put('room-types/{roomType}/activate', ['as' => 'room-types.activate', 'uses' => 'App\Http\Controllers\RoomTypeController@activate']);

    // Housing room packages
    $api->post('housing-rooms/{room}/packages',['as' => 'housing.rooms.packages.store', 'uses' => 'App\Http\Controllers\HousingRoomPackageController@store']);
    $api->get('housing-rooms-packages/{housingRoomPackage}',['as' => 'housing.rooms.packages.show', 'uses' => 'App\Http\Controllers\HousingRoomPackageController@show']);
    $api->put('housing-rooms-packages/{housingRoomPackage}',['as' => 'housing.rooms.packages.update', 'uses' => 'App\Http\Controllers\HousingRoomPackageController@update']);
    $api->delete('housing-rooms-packages/{housingRoomPackage}',['as' => 'housing.rooms.packages.destroy', 'uses' => 'App\Http\Controllers\HousingRoomPackageController@destroy']);
    $api->put('housing-rooms-packages/{housingRoomPackage}/activate',['as' => 'housing.rooms.packages.activate', 'uses' => 'App\Http\Controllers\HousingRoomPackageController@activate']);

    // Packages
    $api->post('packages', ['as' => 'packages.store', 'uses' => 'App\Http\Controllers\PackageController@store']);
    $api->put('packages/{id}', ['as' => 'packages.update', 'uses' => 'App\Http\Controllers\PackageController@update']);
    $api->delete('packages/{id}', ['as' => 'packages.destroy', 'uses' => 'App\Http\Controllers\PackageController@destroy']);
    $api->put('packages/{id}/activate', ['as' => 'packages.activate', 'uses' => 'App\Http\Controllers\PackageController@activate']);

    // Countries
    $api->get('countries', ['as' => 'countries.index', 'uses' => 'App\Http\Controllers\CountryController@index']);
    $api->post('countries', ['as' => 'countries.store', 'uses' => 'App\Http\Controllers\CountryController@store']);
    $api->get('countries/{country}', ['as' => 'countries.show', 'uses' => 'App\Http\Controllers\CountryController@show']);
    $api->put('countries/{country}', ['as' => 'countries.update', 'uses' => 'App\Http\Controllers\CountryController@update']);
    $api->delete('countries/{country}', ['as' => 'countries.destroy', 'uses' => 'App\Http\Controllers\CountryController@destroy']);
    $api->put('countries/{country}/activate', ['as' => 'countries.activate', 'uses' => 'App\Http\Controllers\CountryController@activate']);
    $api->put('countries/{country}/sort', ['as' => 'countries.sort', 'uses' => 'App\Http\Controllers\CountryController@sort']);

    // Experiences
    $api->get('experiences', ['as' => 'experiences', 'uses' => 'App\Http\Controllers\ExperienceController@index']);
    $api->post('experiences', ['as' => 'experiences.store', 'uses' => 'App\Http\Controllers\ExperienceController@store']);
    $api->get('experiences/{id}', ['as' => 'experiences.show', 'uses' => 'App\Http\Controllers\ExperienceController@show']);

    // Courses
    $api->get('courses', ['as' => 'courses.index', 'uses' => 'App\Http\Controllers\CourseController@index']);
    $api->post('courses', ['as' => 'courses.store', 'uses' => 'App\Http\Controllers\CourseController@store']);
    $api->get('courses/{package}', ['as' => 'courses.show', 'uses' => 'App\Http\Controllers\CourseController@show']);
    $api->put('courses/{package}', ['as' => 'courses.update', 'uses' => 'App\Http\Controllers\CourseController@update']);
    $api->delete('courses/{package}', ['as' => 'courses.destroy', 'uses' => 'App\Http\Controllers\CourseController@destroy']);
    $api->put('courses/{package}/activate', ['as' => 'courses.activate', 'uses' => 'App\Http\Controllers\CourseController@activate']);

    // Roles
    $api->get('roles', ['as' => 'roles.index', 'uses' => 'App\Http\Controllers\RoleController@index']);
    $api->get('roles/{role}', ['as' => 'roles.show', 'uses' => 'App\Http\Controllers\RoleController@show']);
    $api->post('roles', ['as' => 'roles.store', 'uses' => 'App\Http\Controllers\RoleController@store']);
    $api->put('roles/{role}', ['as' => 'roles.update', 'uses' => 'App\Http\Controllers\RoleController@update']);
    $api->delete('roles/{role}', ['as' => 'roles.destroy', 'uses' => 'App\Http\Controllers\RoleController@destroy']);
    $api->put('roles/{role}/activate', ['as' => 'roles.activate', 'uses' => 'App\Http\Controllers\RoleController@activate']);

    // System modules
    $api->get('system-modules', ['as' => 'system-modules', 'uses' => 'App\Http\Controllers\SystemModuleController@index']);
    $api->get('system-modules/{module}', ['as' => 'system-modules.show', 'uses' => 'App\Http\Controllers\SystemModuleController@show']);

    // Dashboard
    $api->get('dashboard', ['as' => 'dashboard.index', 'uses' => 'App\Http\Controllers\DashboardController@index']);

    // Reports
    $api->get('reports/{report}', ['as' => 'reports.index', 'uses' => 'App\Http\Controllers\ReportController@index']);
});
