<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Purchase extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date',
        'reference_number',
        'concept',
        'amount',
        'command_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'date'];

    public function consumptionCenter() {
        return $this->belongsTo('App\ConsumptionCenter');
    }

    public function currency() {
        return $this->belongsTo('App\Currency');
    }

    public function methodPayment() {
        return $this->belongsTo('App\MethodPayment');
    }

    public function status() {
        return $this->belongsTo('App\Status');
    }

    public function client() {
        return $this->belongsTo('App\Client');
    }

    public function command() {
        return $this->belongsTo('App\command');
    }

    public function purchasable()
    {
        return $this->morphOne('App\Purchasable', 'purchasable');
    }

    /**
     * Delete the current purchase
     */
    public function delete()
    {
        $this->purchasable()->delete();
        parent::delete();
    }
}
