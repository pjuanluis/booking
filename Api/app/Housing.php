<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

class Housing extends BaseModel
{
    use SoftDeletes;

    protected $table = 'housing';

    protected $fillable = [
    	'name', 'slug', 'is_active',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at',];

    /**
     * Get related housing rooms
     */
    public function housingRooms()
    {
        return $this->hasMany('App\HousingRoom');
    }

    /**
     * Get related residences
     */
    public function residences()
    {
        return $this->hasMany('App\Residence');
    }
}
