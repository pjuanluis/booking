<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CommandHousing extends Model
{
    use SoftDeletes;

    protected $fillable = [
    	'command_id', 'housing_room_package_id', 'from_date', 'to_date', 'quantity',
        'unit', 'cost', 'room_id', 'people'
    ];

    public $table = "command_housing";

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at',];

    /**
     * Get the related command.
     */
    public function command()
    {
        return $this->belongsTo('App\Command');
    }

    /**
     * Get the related room.
     */
    public function room()
    {
        return $this->belongsTo('App\Room');
    }

    /**
     * Get the related housing room package.
     */
    public function housingRoomPackage()
    {
        return $this->belongsTo('App\HousingRoomPackage');
    }

    /**
     * Get the related package.
     */
    public function package()
    {
        return $this->housingRoomPackage->package();
    }

    /**
     * Get the related housing room.
     */
    public function housingRoom()
    {
        return $this->housingRoomPackage->housingRoom();
    }

    /**
     * Get the related housing.
     */
    public function housing()
    {
        return $this->housingRoomPackage->housingRoom->housing();
    }

    public function delete()
    {
        // TODO: Borrar relaciones
        $this->purchase()->delete();
        parent::delete();
    }

    /**
     * Get the course's purchase
     */
    public function purchase()
    {
        return $this->morphOne('App\Purchasable', 'purchasable');
    }
}
