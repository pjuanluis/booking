<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TravelInformation extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'travel_information';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'command_id', 'travel_by', 'arrival_place', 'arrival_carrier', 'arrival_at', 'arrival_travel_number',
        'departure_place', 'departure_carrier', 'departure_at', 'departure_travel_number'
    ];


    /**
     * The attributes that should be mutated to dates.
     * 
     * @var array
     */
    protected $dates = [
        'arrival_at', 'departure_at', 'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * Get related command
     */
    public function command()
    {
        return $this->belongsTo(App\Command::class);
    }    
}