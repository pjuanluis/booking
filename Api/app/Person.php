<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Person extends BaseModel
{
    use SoftDeletes;
    
    protected $fillable = [
    	'first_name', 'last_name', 'email', 'position', 'abbreviation', 'type'
    ];
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at',];
}
