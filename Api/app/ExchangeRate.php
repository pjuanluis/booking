<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExchangeRate extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "base_currency_id", "to_currency_id", "base_amount", "amount"
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'amount' => 'float',
    ];

    /**
     * Get the related base currency.
     */
    public function baseCurrency()
    {
        return $this->belongsTo('App\Currency', 'base_currency_id');
    }

    /**
     * Get the related exchange currency.
     */
    public function exchangeCurrency()
    {
        return $this->belongsTo('App\Currency', 'to_currency_id');
    }
}
