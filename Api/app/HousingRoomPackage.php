<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HousingRoomPackage extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
    	'package_id', 'housing_room_id', 'is_active',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at',];

    /**
     * Get the related package.
     */
    public function package()
    {
        return $this->belongsTo('App\Package');
    }

    /**
     * Get the related housing room.
     */
    public function housingRoom()
    {
        return $this->belongsTo('App\HousingRoom');
    }

    public function housingRates()
    {
        return $this->hasMany('App\HousingRate');
    }
}
