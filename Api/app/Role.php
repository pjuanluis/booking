<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends BaseModel
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "name", "slug", "is_active"
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * Get the related users.
     */
    public function users()
    {
        return $this->hasMany('App\User');
    }

    /**
     * Check if the role has the specified permission over the specified system module.
     * @param type $moduleId The module id
     * @param type $permissionId The permission id to check
     * @return boolean true if the role has permission, false otherwise.
     */
    public function hasPermission($moduleId, $permissionId)
    {
        $count = DB::table('system_access_control')
                ->where('role_id', $this->id)
                ->where('system_module_id', $moduleId)
                ->where('system_permission_id', $permissionId)
                ->count();
        if ($count === 0) {
            return false;
        }
        return true;
    }

    public function accesses()
    {
        $result = DB::table('system_access_control')
                ->select(DB::raw('system_modules.name as module_name'), DB::raw('system_modules.id as module_id'),
                        DB::raw('system_permissions.name as permission_name'), DB::raw('system_permissions.id as permission_id'))
                ->leftJoin('system_modules', 'system_access_control.system_module_id', '=', 'system_modules.id')
                ->leftJoin('system_permissions', 'system_access_control.system_permission_id', '=', 'system_permissions.id')
                ->where('role_id', $this->id)
                ->orderBy('system_access_control.role_id')
                ->orderBy('system_access_control.system_module_id')
                ->get();
        $accesses = [];
        $permissions = [];
        $module = ['id' => 0];
        foreach ($result as $i => $r) {
            if ($r->module_id != $module['id']) {
                if ($i > 0) {
                    $accesses[] = [
                        'module' => ['data' => $module],
                        'permissions' => ['data' => $permissions]
                    ];
                }
                $module['id'] = $r->module_id;
                $module['name'] = $r->module_name;
                $permissions = [];
            }
            $permissions[] = [
                'id' => $r->permission_id,
                'name' => $r->permission_name,
            ];
        }
        if (count($result) > 0) {
            $accesses[] = [
                'module' => ['data' => $module],
                'permissions' => ['data' => $permissions]
            ];
        }
        return $accesses;
    }
}
