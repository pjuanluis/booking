<?php

namespace App\Traits;

use App\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Collection;
use GuzzleHttp\Client;

trait FileAction {

    private function uploadFileToExternalDrive($file, $uploadDir)
    {
        $host = config('services.fileserver.url');
        $client = new Client();
        $res = $client->request('POST', $host . '/files/store', [
            'multipart' => [
                [
                    'name' => 'file',
                    'contents' => base64_decode($file['contents']),
                    'filename' => $file['name'],
                ],
            ],
            'headers' => [
                'filedir' => $uploadDir,
            ],
        ]);
        $data = json_decode($res->getBody(), true);
        return $data['url'];
    }

    private function replaceFileToExternalDrive($oldFile, array $newFile)
    {
        $host = config('services.fileserver.url');
        $client = new Client();
        $res = $client->request('PUT', $host . '/files/replace', [
            'multipart' => [
                [
                    'name' => 'file',
                    'contents' => base64_decode($newFile['contents']),
                    'filename' => $newFile['name'],
                ],
                [
                    'name' => 'oldpath',
                    'contents' => $oldFile->storage_path,
                ],
            ],
            'headers' => [
                'filedir' => $oldFile->storage_path,
            ],
        ]);
        $data = json_decode($res->getBody(), true);
        return $data['url'];
    }

    private function deleteFileToExternalDrive($filePath)
    {
        $host = config('services.fileserver.url');
        $client = new Client();
        $res = $client->request('DELETE', $host . '/files/delete', [
            'json' => [
                'filePath' => $filePath,
            ],
        ]);
        $data = json_decode($res->getBody(), true);
        return $data;
    }

    public function uploadFile(array $file, $dir = "images", $diskType = 'public')
    {
        $contents = base64_decode($file['contents']);
        $uniqueName = uniqid() . '-' . str_slug(pathinfo($file['name'], PATHINFO_FILENAME));
        $uniqueName .= '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
        $path = "$dir/".$uniqueName;
        Storage::disk('public')->put($path, $contents);
        $dir = rtrim($dir, '/') . '/';
        if ($diskType == 'fileserver') {
            return $this->uploadFileToExternalDrive($file, $dir);
        }
        return $path;
    }

    public function getStoragePublicDir()
    {
        return 'storage/';
    }

    public function replaceFile($oldFile, array $newFile, $storageDisk = 'public')
    {
        if ($storageDisk == 'fileserver') {
            return $this->replaceFileToExternalDrive($oldFile, $newFile);
        }
        else {
            $filePath = rtrim($oldFile->storage_path, basename($oldFile->storage_path));
            // Guardar nuevo archivo
            $contents = base64_decode($newFile['contents']);
            $uniqueName = uniqid() . '-' . str_slug(pathinfo($newFile['name'], PATHINFO_FILENAME));
            $uniqueName .= '.' . pathinfo($newFile['name'], PATHINFO_EXTENSION);
            $path = "$filePath".$uniqueName;
            Storage::disk('public')->put($path, $contents);
            Storage::disk($storageDisk)->delete($oldFile->storage_path);
            return $path;
        }
    }

    public function deleteFile($file, $fileDiskType = 'public', $garbageDiskType = 'local', $moveToGarbage = false, $deleteFromDisk = false)
    {
        if ($file instanceof Collection) {
            $this->deleteFileCollection($file, $fileDiskType, $garbageDiskType, $moveToGarbage, $deleteFromDisk);
        }
        else if ($file instanceof File) {
            return $this->deleteSingleFile($file, $fileDiskType, $garbageDiskType, $moveToGarbage, $deleteFromDisk);
        }
        return false;
    }

    private function deleteSingleFile($file, $fileDiskType, $garbageDiskType, $moveToGarbage = false, $deleteFromDisk = false)
    {
        $oldLocation = $file->storage_path;
        if ($fileDiskType == 'fileserver') {
            $this->deleteFileToExternalDrive($file->storage_path);
        }
        else if ($moveToGarbage) {
            $garbageDir = 'garbage';
            $exists = Storage::disk($fileDiskType)->exists($file->storage_path);
            if (!$exists) {
                return false;
            }
            $newLocation = $garbageDir . '/' . $file->storage_path;
            $contents = Storage::disk($fileDiskType)->get($oldLocation);
            Storage::disk($garbageDiskType)->put($newLocation, $contents);
            Storage::disk($fileDiskType)->delete($oldLocation);
            $file->update(['storage_path' => $newLocation]);
        }
        else if ($deleteFromDisk) {
            Storage::disk($fileDiskType)->delete($oldLocation);
        }
        $file->delete();
        return true;
    }

    private function deleteFileCollection($fileCollecion, $fileDiskType, $garbageDiskType, $moveToGarbage = false, $deleteFromDisk = false)
    {
        foreach ($fileCollecion as $file) {
            $this->deleteSingleFile($file, $fileDiskType, $garbageDiskType, $moveToGarbage, $deleteFromDisk);
        }
        return true;
    }

    private function recoverFile()
    {
        // NOTE: if you wish to recover deleted files, just move files from local disk dir 'garbage' to public disk.
        // This method should work for both disk types: public and fileserver
    }
}
