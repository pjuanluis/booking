<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemModule extends Model
{
    protected $fillable = [
    	'name', 'slug', 'is_active'
    ];

    /**
     * Get the related system module group.
     */
    public function systemModuleGroup()
    {
        return $this->belongsTo('App\SystemModuleGroup');
    }

    /**
     * The permissions that belong to the module.
     */
    public function permissions()
    {
        return $this->belongsToMany('App\SystemPermission', 'system_module_permissions');
    }
}
