<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseRate extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
    	'package_id', 'quantity', 'to_quantity', 'unit_cost', 'is_active',
    ];
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at',];
    
    /**
     * Get the related package.
     */
    public function package()
    {
        return $this->belongsTo('App\Package');
    }
}
