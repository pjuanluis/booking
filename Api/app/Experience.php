<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Experience extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
    	'name', 'slug', 'description', 'is_active',
    ];
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at',];
    
    /**
     * Get related packages
     */
    public function packages()
    {
        return $this->belongsToMany('App\Package', 'package_experiences');
    }
}
