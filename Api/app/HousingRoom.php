<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HousingRoom extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
    	'housing_id', 'room_type_id', 'name', 'description', 'is_active', 'beds', 'cost_type'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at',];

    /**
     * Get the related housing.
     */
    public function housing()
    {
        return $this->belongsTo('App\Housing');
    }

    /**
     * Get the related room type.
     */
    public function roomType()
    {
        return $this->belongsTo('App\RoomType');
    }

    /**
     * The packages that belong to the housing room.
     */
    public function packages()
    {
        return $this->belongsToMany('App\Package', 'housing_room_packages')
                ->withPivot(['id', 'is_active'])
                ->wherePivot('deleted_at', null);
    }
}
