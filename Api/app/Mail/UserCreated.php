<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserCreated extends Mailable
{
    use Queueable, SerializesModels;

    private $user;
    private $action;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $action = 'create')
    {
        $this->user = $user;
        $this->action = $action;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.user-created')
            ->with([
                'user' => $this->user,
                'action' => $this->action
            ]);
    }
}
