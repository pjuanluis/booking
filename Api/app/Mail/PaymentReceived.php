<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Command;

class PaymentReceived extends Mailable
{
    use Queueable, SerializesModels;

    protected $command;
    protected $sender;
    protected $toClient;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Command $command, $toClient = true, array $from = [])
    {
        $this->command = $command;
        $this->sender = $from;
        $this->toClient = $toClient;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //$this->subject($subject);
        $view = $this->view('emails.payment-received');
        if (!empty($this->sender)) {
            $view = $view->from($this->sender['email'], $this->sender['name']);
        }
        $action = [];
        if (empty($this->command->client)) {
            $action = [
                'url' => config('services.app_experiencia.url') . '/booking/your-info?token=' . $this->command->token,
                'text' => "Personal information form"
            ];
        }
        return $view->with([
            'command' => $this->command,
            'toClient' => $this->toClient,
            'action' => $action
        ]);
    }
}
