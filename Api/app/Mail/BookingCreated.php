<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Command;

class BookingCreated extends Mailable
{
    use Queueable, SerializesModels;

    protected $command;
    protected $sender;
    protected $toClient;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Command $command, $toClient = true, array $from = [])
    {
        $this->command = $command;
        $this->sender = $from;
        $this->toClient = $toClient;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $view = $this->view('emails.booking-created');
        $isUpdateCommand = request()->routeIs('commands.update');
        if (!empty($this->sender)) {
            $view = $view->from($this->sender['email'], $this->sender['name']);
        }
        if ($isUpdateCommand) {
            $view = $view->subject('Booking Updated');
        }
        return $view->with([
            'command' => $this->command,
            'toClient' => $this->toClient,
            'isUpdateCommand' => $isUpdateCommand,
            'paymentLink' => [
                'url' => config('services.app_experiencia.url') . '/booking/payment?token=' . $this->command->token,
                'text' => "Payment options"
            ],
             'infoLink' => [
                'url' => config('services.app_experiencia.url') . '/booking/your-info?token=' . $this->command->token,
                'text' => "Personal information form"
            ]
        ]);
    }
}
