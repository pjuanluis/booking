<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Command;
use App\Client;

class ClientInfoRegistered extends Mailable
{
    use Queueable, SerializesModels;

    protected $command;
    protected $client;
    protected $sender;
    protected $toClient;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Client $client, Command $command, $toClient = true, array $from = [])
    {
        $this->command = $command;
        $this->client = $client;
        $this->sender = $from;
        $this->toClient = $toClient;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //$this->subject($subject);
        $view = $this->view('emails.client-info-registered');
        if (!empty($this->sender)) {
            $view = $view->from($this->sender['email'], $this->sender['name']);
        }
        $action = [];
        $payment = $this->command->payments()->whereIn('type', ['registration_fee', 'reservation_payment'])->first();
        if (empty($payment)) {
            $action = [
                'url' => config('services.app_experiencia.url') . '/booking/payment?token=' . $this->command->token,
                'text' => "Payment options"
            ];
        }
        return $view->with([
            'command' => $this->command,
            'client' => $this->client,
            'toClient' => $this->toClient,
            'hasPayment' => !empty($payment),
            'action' => $action
        ]);
    }
}
