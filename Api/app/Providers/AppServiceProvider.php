<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Observers\CommandHousingObserver;
use App\Observers\CommandCourseObserver;
use App\Observers\CommandObserver;
use App\Observers\UserObserver;
use App\Observers\PaymentObserver;
use App\Observers\ClientObserver;
use App\CommandHousing;
use App\CommandCourse;
use App\Command;
use App\User;
use App\Payment;
use App\Client;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        CommandHousing::observe(CommandHousingObserver::class);
        CommandCourse::observe(CommandCourseObserver::class);
        Command::observe(CommandObserver::class);
        User::observe(UserObserver::class);
        Payment::observe(PaymentObserver::class);
        Client::observe(ClientObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
