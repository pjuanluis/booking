<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HousingRate extends Model
{
    use SoftDeletes;

    protected $fillable = [
    	'housing_room_package_id', 'quantity', 'from_date', 'to_date', 'unit',
        'unit_cost', 'is_active',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at',];

    public function housingRoomPackage()
    {
        return $this->belongsTo('App\HousingRoomPackage');
    }
}
