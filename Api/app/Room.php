<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Room extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'residence_id', 'name', 'beds',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at',];

    /**
     * Get related housing room
     */
    public function housingRooms()
    {
        return $this->belongsToMany('App\HousingRoom', 'housing_rooms_rooms');
    }

    /**
     * Get related residence
     */
    public function residence()
    {
        return $this->belongsTo('App\Residence');
    }

    public function commandHousings()
    {
        return $this->hasMany(\App\CommandHousing::class);
    }

    public function blockedDates()
    {
        return $this->hasMany(\App\BlockedRoom::class);
    }
}
