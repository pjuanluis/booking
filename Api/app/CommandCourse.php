<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CommandCourse extends Model
{
    use SoftDeletes;

    protected $fillable = [
    	'command_id', 'package_id', 'from_date', 'to_date', 'quantity', 'cost',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at',];

    /**
     * Get the related package.
     */
    public function package()
    {
        return $this->belongsTo('App\Package');
    }

    /**
     * Get the related command.
     */
    public function command()
    {
        return $this->belongsTo('App\Command');
    }

    public function delete()
    {
        // TODO: Borrar relaciones
        $this->purchase()->delete();
        parent::delete();
    }

    /**
    * Get the course's purchase
    */
    public function purchase()
    {
        return $this->morphOne('App\Purchasable', 'purchasable');
    }
}
