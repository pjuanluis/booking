<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommandLog extends Model
{

    protected $fillable = [
    	'command_id', 'user_id', 'action', 'url', 'data',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var Object
     */
    protected $casts = [
        'data' => 'Object',
    ];

    /**
     * Get the command that owns the log.
     */
    public function command()
    {
        return $this->belongsTo('App\Command');
    }

    /**
     * Get the user that owns the log.
     */
    public function user()
    {
        return $this->belongsTo('App\User')->withTrashed();
    }
}
