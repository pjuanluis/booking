<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Command extends Model
{
    use SoftDeletes;

    protected $fillable = [
    	'client_id', 'agency_id', 'pickup_id', 'pickup_cost', 'client_first_name',
        'client_last_name', 'client_email', 'from_date', 'to_date', 'status',
        'booking_code', 'token', 'is_active', 'client_comments', 'registration_from', 'travel_partner',
        'payment_status', 'comments'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at',];

    /**
     * Get the related command courses.
     */
    public function commandCourses()
    {
        return $this->hasMany('App\CommandCourse');
    }

    /**
     * Get the related command housing.
     */
    public function commandHousing()
    {
        return $this->hasMany('App\CommandHousing');
    }

    /**
     * Get the related command payments.
     */
    public function payments()
    {
        return $this->hasMany('App\Payment');
    }

    /**
     * Get the related command travel information.
     */
    public function travelInformation()
    {
        return $this->hasMany(TravelInformation::class);
    }

    /**
     * Get the related pickup.
     */
    public function pickup()
    {
        return $this->belongsTo('App\Pickup');
    }

    /**
     * Get the related client.
     */
    public function client()
    {
        return $this->belongsTo('App\Client');
    }

    /**
     * Delete the current command
     */
    public function delete()
    {
        $this->deleteCourses();
        $this->deleteHousing();
        $this->deletePayments();
        $this->purchase()->delete();
        $this->deleteExtraPurchases();
        // Eliminar logs
        $this->commandLogs()->delete();
        // Eliminar la comanda
        parent::delete();
    }

    public function deleteExtraPurchases()
    {
        $extraPurchases = $this->extraPurchases;
        foreach ($extraPurchases as $ep) {
            $ep->delete();
        }
    }

    public function deleteCourses()
    {
        $commandCourses = $this->commandCourses;
        foreach ($commandCourses as $cc) {
            $cc->delete();
        }
    }

    public function deleteHousing()
    {
        $commandHousing = $this->commandHousing;
        foreach ($commandHousing as $ch) {
            $ch->delete();
        }
    }

    public function deletePayments()
    {
        $payments = $this->payments;
        foreach ($payments as $p) {
            $p->delete();
        }
    }

    /**
    * Get the course's purchase
    */
    public function purchase()
    {
        return $this->morphOne('App\Purchasable', 'purchasable');
    }

    /**
     * Get the agency that owns the command.
     */
    public function agency()
    {
        return $this->belongsTo('App\Agency');
    }

    /**
     * Update the booking_code
     *
     * @return void
     */
    public function updateBookingCode()
    {
        $agency = $this->agency;
        $prefix = 'MAN'; // Prefijo para las comandas creadas dede el admin
        if ($agency !== null) {
            $prefix = $agency->code; // Prefijo para las comandas que pertenesen a una agencia
        } elseif (strpos($this->booking_code, 'WWW') !== false) {
            // La comanda fue creada desde el booking público por lo tanto no se puede cambiar su prefijo
            return;
        }
        $this->booking_code = substr_replace($this->booking_code, $prefix, 0, 3);
        $this->save();
    }

    /**
     * Get the related bills.
     */
    public function bills()
    {
        return $this->hasMany('App\Bill');
    }

    /**
     * Get the related extra purchases.
     */
    public function extraPurchases()
    {
        return $this->hasMany('App\Purchase');
    }

    /**
     * Get the related command logs.
     */
    public function commandLogs()
    {
        return $this->hasMany('App\CommandLog');
    }
}
