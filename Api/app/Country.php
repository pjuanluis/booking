<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
    	'name', 'code', 'order', 'is_active'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    public function sort($dir)
    {
        $order = $dir === 'up' ? "desc" : "asc";
        $account = null;
        if ($dir === 'up'){
            $account = $this->getUpperAccount($order);
        }
        else if ($dir === 'down'){
            $account = $this->getLowerAccount($order);
        }
        else {
            return;
        }
        if ($account === null) {
            return;
        }
        $oldValue = $this->order;
        $this->update(['order' => $account->order]);
        $account->update(['order' => $oldValue]);
    }

    public function getUpperAccount($order)
    {
        $account = Country::where('order', '<', $this->order)
                ->orderBy('order', $order)->first();
        return $account;
    }

    public function getLowerAccount($order)
    {
        $account = Country::where('order', '>', $this->order)
                ->orderBy('order', $order)->first();
        return $account;
    }

}
