<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Schedule extends Model
{
    //use SoftDeletes;
    
    protected $fillable = [
    	'schedulable_id', 'schedulable_type', 'responsible_id', 'place_id', 'subject', 
        'begin_at', 'is_private', 'finish_at'
    ];
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];
    
    /**
     * Get the related responsible.
     */
    public function responsible()
    {
        return $this->belongsTo('App\Person', 'responsible_id');
    }
    
    /**
     * Get the related place.
     */
    public function place()
    {
        return $this->belongsTo('App\Place');
    }
    
    /**
     * Get all of the owning schedulable models.
     */
    public function schedulable()
    {
        return $this->morphTo();
    }
    
    /**
     * Get related experiences
     */
    public function experiences()
    {
        return $this->belongsToMany('App\Experience', 'schedule_experiences');
    }
    
    /**
     * The related schedule dates
     */
    public function dates()
    {
        return $this->hasMany('App\ScheduleDate');
    }
}
