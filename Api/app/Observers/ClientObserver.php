<?php

namespace App\Observers;

use Illuminate\Support\Facades\Mail;
use App\Mail\ConfirmClientDataCreated;
use App\Client;

class ClientObserver
{
    private function sendEmailCorroborateInfo(Client $client)
    {
        $authUser = auth()->user();
        $authEmail = isset($authUser->email) ? $authUser->email : '';

        if (strcasecmp($client->email, $authEmail) != 0) {
            Mail::to($client)->send(new ConfirmClientDataCreated($client));
        }
    }

    /**
     * Listen to the Client created event.
     *
     * @param  \App\Client  $client
     * @return void
     */
    public function created(Client $client)
    {
        $this->sendEmailCorroborateInfo($client);
        // Mail::to($user)->send(new UserCreated($user));
    }

    /**
     * Listen to the Client updated event.
     *
     * @param  \App\Client  $client
     * @return void
     */
    public function updated(Client $client)
    {
        $this->sendEmailCorroborateInfo($client);
    }
}