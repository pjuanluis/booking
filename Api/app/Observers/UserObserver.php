<?php

namespace App\Observers;

use Illuminate\Support\Facades\Mail;
use App\Mail\UserCreated;
use App\User;

class UserObserver
{
    /**
     * Listen to the User created event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function created(User $user)
    {
        if (empty($user->p)) {
            $user->p = request()->get('password');
        }
        Mail::to($user)->send(new UserCreated($user));
    }
}
