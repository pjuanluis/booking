<?php

namespace App\Observers;

use App\Role;
use App\User;
use App\Payment;

class PaymentObserver
{

    private function createUser(Payment $payment, $slugRole)
    {
        $u = null;
        $pass = null;
        $role = null;
        $user = User::where('email', $payment->command->client_email);

        if (!$user->exists()) {
            $pass = str_random(8);

            $role = Role::where('slug', $slugRole)->first();

            $u = new User();
            $u->first_name = $payment->command->client_first_name;
            $u->last_name = $payment->command->client_last_name;
            $u->email = $payment->command->client_email;
            $u->password = bcrypt($pass);
            $u->role_id = $role->id;
            $u->p = $pass;
            $u->save();
        }
    }

    /**
     * Listen to the Payment created event.
     *
     * @param  \App\Payment  $payment
     * @return void
     */
    public function created(Payment $payment)
    {
        if (strcasecmp($payment->type, 'registration_fee') == 0) {
            // $this->createUser($payment, 'client');
        }
    }
}
