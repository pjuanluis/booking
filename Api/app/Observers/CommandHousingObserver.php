<?php

namespace App\Observers;

use Auth;
use App\CommandHousing;
use App\Purchasable;
use App\MethodPayment;
use App\ConsumptionCenter;
use App\Status;
use App\Currency;

class CommandHousingObserver
{
    /**
     * Listen to the CommandHousing created event.
     *
     * @param  \App\CommandHousing  $commandHousing
     * @return void
     */
    public function created(CommandHousing $commandHousing)
    {

    }

    /**
     * Listen to the CommandHousing saved event.
     *
     * @param  \App\CommandHousing  $commandHousing
     * @return void
     */
    public function saved(CommandHousing $commandHousing)
    {
        // Valores por default son para cuando la comanda se crea desde el booking público
        $consumptionCenter = ConsumptionCenter::where('slug', 'website');
        $currency = Currency::with('exchangeRates')->where('code', 'USD')->first();
        $discount = 0;
        $exchangeRates = $currency->exchangeRates;
        $exchangeRates = $exchangeRates->map(function($item) {
                    return [
                        'amount' => $item->amount,
                        'base_currency_id' => $item->base_currency_id,
                        'to_currency_id' => $item->to_currency_id,
                        'base_amount' => $item->base_amount
                    ];
                })
                ->toArray();

        if (request()->routeIs('commands.store') || request()->routeIs('commands.update')) {
            // Valores para cuando la comanda se crea o actualiza desde el admin
            $consumptionCenter = ConsumptionCenter::where('slug', 'walk_in');
            $currency = Currency::where('id', request()->input('currency_id'))->first();
            $discount = request()->input('discount');
            $exchangeRates = request()->input('exchange_rates');
        }

        $purchase = $commandHousing->purchase;

        if ($purchase !== null) {
            if (!request()->routeIs('commands.accommodations.store')) {
                // No actualizar el descuento ni la moneda cuando se guarda la acomodación
                $purchase->update([
                    'discount' => $discount,
                    'currency_id' => $currency->id,
                    'exchange_rates' => $exchangeRates,
                ]);
            }
        } else {
            // La comanda no tiene una compra asignada, entonces hay que crearla
            Purchasable::create([
                'discount' => $discount,
                'consumption_center_id' => $consumptionCenter->value('id'),
                'currency_id' => $currency->id,
                'client_id' => $commandHousing->command->client_id,
                'purchasable_id' => $commandHousing->id,
                'purchasable_type' => CommandHousing::class,
                'exchange_rates' => $exchangeRates,
            ]);
        }

        // Valida si se guardó un alojamiento o se hizo un intercambio de alojamiento
        if (request()->routeIs('commands.accommodations.store')) {
            $purchase = $commandHousing->command->purchase;
            // Si la comanda aún no tiene responsable, asignar como responsalbe al usuario que guardó o intercambió el alojamiento
            if (!empty($purchase) && empty($purchase->user_id)) {
                $purchase->update(['user_id' => Auth::user()->id]);
            }
        }
    }
}
