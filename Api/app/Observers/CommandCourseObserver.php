<?php

namespace App\Observers;

use Auth;
use App\CommandCourse;
use App\Purchasable;
use App\MethodPayment;
use App\ConsumptionCenter;
use App\Status;
use App\Currency;

class CommandCourseObserver
{
    /**
     * Listen to the CommandCourse created event.
     *
     * @param  \App\CommandCourse $commandCourse
     * @return void
     */
    public function created(CommandCourse $commandCourse)
    {

    }

    /**
     * Listen to the CommandCourse saved event.
     *
     * @param  \App\CommandCourse  $commandCourse
     * @return void
     */
    public function saved(CommandCourse $commandCourse)
    {
        // Valores por default son para cuando la comanda se crea desde el booking público
        $consumptionCenter = ConsumptionCenter::where('slug', 'website');
        $currency = Currency::with('exchangeRates')->where('code', 'USD')->first();
        $discount = 0;
        $exchangeRates = $currency->exchangeRates;
        $exchangeRates = $exchangeRates->map(function($item) {
                    return [
                        'amount' => $item->amount,
                        'base_currency_id' => $item->base_currency_id,
                        'to_currency_id' => $item->to_currency_id,
                        'base_amount' => $item->base_amount
                    ];
                })
                ->toArray();

        if (request()->routeIs('commands.store') || request()->routeIs('commands.update')) {
            // Valores para cuando la comanda se crea o actualiza desde el admin
            $consumptionCenter = ConsumptionCenter::where('slug', 'walk_in');
            $currency = Currency::where('id', request()->input('currency_id'))->first();
            $discount = request()->input('discount');
            $exchangeRates = request()->input('exchange_rates');
        }

        $purchase = $commandCourse->purchase;

        if ($purchase !== null) {
            // La comanda tiene una compra asignada, entonces hay que actualizarla
            $purchase->update([
                'discount' => $discount,
                'currency_id' => $currency->id,
                'exchange_rates' => $exchangeRates,
            ]);
        } else {
            // La comanda no tiene una compra asignada, entonces hay que crearla
            Purchasable::create([
                'discount' => $discount,
                'consumption_center_id' => $consumptionCenter->value('id'),
                'currency_id' => $currency->id,
                'client_id' => $commandCourse->command->client_id,
                'purchasable_id' => $commandCourse->id,
                'purchasable_type' => CommandCourse::class,
                'exchange_rates' => $exchangeRates,
            ]);
        }
    }
}
