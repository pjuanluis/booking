<?php

namespace App\Observers;

use Auth;
use App\Command;
use App\Purchasable;
use App\MethodPayment;
use App\ConsumptionCenter;
use App\Status;
use App\Currency;

class CommandObserver
{
    /**
     * Listen to the Command created event.
     *
     * @param  \App\Command  $command
     * @return void
     */
    public function created(Command $command)
    {
        // Valores por default son para cuando la comanda se crea desde el booking público
        $consumptionCenter = ConsumptionCenter::where('slug', 'website');
        $currency = Currency::with('exchangeRates')->where('code', 'USD')->first();

        if (request()->routeIs('commands.store')) {
            // Valores para cuando la comanda se crea desde el admin
            $consumptionCenter = ConsumptionCenter::where('slug', 'walk_in');
        }
        $exchangeRates = $currency->exchangeRates;
        $exchangeRates = $exchangeRates->map(function($item) {
                    return [
                        'amount' => $item->amount,
                        'base_currency_id' => $item->base_currency_id,
                        'to_currency_id' => $item->to_currency_id,
                        'base_amount' => $item->base_amount
                    ];
                })
                ->toArray();

        Purchasable::create([
            'discount' => 0,
            'consumption_center_id' => $consumptionCenter->value('id'),
            'currency_id' => $currency->id,
            'client_id' => $command->client_id,
            'purchasable_id' => $command->id,
            'purchasable_type' => Command::class,
            'exchange_rates' => $exchangeRates,
        ]);
    }

    /**
     * Listen to the Command saved event.
     *
     * @param  \App\Command  $command
     * @return void
     */
    public function saved(Command $command)
    {
        // Actualizar la compra de la comanda (el pickup)
        $currency = request()->get('pickup_currency_id');
        $discount = request()->get('pickup_discount');
        $exchangeRates = request()->get('pickup_exchange_rates');
        $purchase = $command->purchase;
        $data = [];
        if (request()->routeIs('commands.store') || request()->routeIs('commands.update')) {
            // Asignar a la comanda un usuario responsable únicamente si no tiene uno
            if (!empty($purchase) && empty($purchase->user_id)) {
                $data['user_id'] = Auth::user()->id;
            }
        }
        if ($currency) {
            $data['currency_id'] = $currency;
            $data['discount'] = $discount == null ? 0 : $discount;
            if (!empty($exchangeRates)) {
                $data['exchange_rates'] = $exchangeRates;
            }
        }
        if ($data && $purchase) {
            // Actualizar la compra únicamente si hay datos
            $purchase->update($data);
        }
    }

    /**
     * Listen to the Command updated event.
     *
     * @param  \App\Command  $command
     * @return void
     */
    public function updated(Command $command)
    {
        if (request()->routeIs('agencies.update')) {
            return;
        }
        $this->updatePurchases($command);
    }

    private function updatePurchases(Command $command) {
        $command->purchase->update(['client_id' => $command->client_id]);
        foreach ($command->commandCourses()->get() as $cc) {
            $cc->purchase->update(['client_id' => $command->client_id]);
        }
        foreach ($command->commandHousing()->get() as $ch) {
            $ch->purchase->update(['client_id' => $command->client_id]);
        }
        foreach ($command->extraPurchases()->get() as $ep) {
            $ep->purchasable->update(['client_id' => $command->client_id]);
        }
    }
}
