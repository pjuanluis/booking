<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlockedRoom extends Model
{
    use SoftDeletes;

    protected $table = "blocked_rooms";

    protected $fillable = [
        'room_id',
        'from_at',
        'to_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'from_at',
        'to_at',
        'created_at',
        'updated_at'
    ];

    /**
     * Get the related room.
     */
    public function room()
    {
        return $this->belongsTo(\App\Room::class);
    }
}
