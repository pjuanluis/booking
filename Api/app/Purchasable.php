<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchasable extends Model
{
    public $timestamps  = false;

    protected $fillable = [
        'discount',
        'status_id',
        'method_payment_id',
        'consumption_center_id',
        'currency_id',
        'client_id',
        'purchasable_id',
        'purchasable_type',
        'user_id',
        'exchange_rates',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var Object
     */
    protected $casts = [
        'exchange_rates' => 'Object',
    ];

     /**
     * Get the related status.
     */
    public function status()
    {
        return $this->belongsTo('App\Status');
    }

    /**
     * Get the related method payment.
     */
    public function methodPayment()
    {
        return $this->belongsTo('App\MethodPayment');
    }

    /**
     * Get the related Consumption center
     */
    public function consumptionCenter()
    {
        return $this->belongsTo('App\ConsumptionCenter');
    }

    /**
     * Get the related currency
     */
    public function currency()
    {
        return $this->belongsTo('App\Currency');
    }

    /**
     * Get the related client
     */
    public function client()
    {
        return $this->belongsTo('App\Client');
    }

    /**
     * Get the related resource
     */
    public function command()
    {
        return $this->belongsTo('App\Command', 'purchasable_id');
    }

    /**
     * Get the related payment
     */
    public function payment()
    {
        return $this->belongsTo('App\Payment', 'purchasable_id');
    }

    /**
     * Get the related resource
     */
    public function commandCourse()
    {
        return $this->belongsTo('App\CommandCourse', 'purchasable_id');
    }

    /**
     * Get the related resource
     */
    public function commandHousing()
    {
        return $this->belongsTo('App\CommandHousing', 'purchasable_id');
    }

    /**
     * Get the related resource
     */
    public function purchase()
    {
        return $this->belongsTo('App\Purchase', 'purchasable_id');
    }

    public function details() {
        return $this->morphTo();
    }

    public function resource(String $model = '')
    {
        return $this->belongsTo($model, 'purchasable_id');
    }

    /**
     * Get the related user
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
