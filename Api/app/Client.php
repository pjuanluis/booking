<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Client extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
    	'first_name', 'last_name', 'email', 'country_id', 'is_active', 'birth_date',
        'phone', 'emergency_phone', 'has_alergies', 'alergies', 'has_diseases',
        'diseases', 'is_smoker', 'fb_profile', 'gender_id',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at',];

    /**
     * Get related country
     */
    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    /**
     * The related file
     */
    public function file()
    {
        return $this->morphOne('App\File', 'fileable');
    }

    /**
     * Get related groups
     */
    public function groups()
    {
        return $this->belongsToMany('App\Group', 'group_students')->withPivot('is_active')->withTimestamps();
    }

    /**
     * Get all of the client's schedules.
     */
    public function schedules()
    {
        return $this->morphMany('App\Schedule', 'schedulable');
    }

    /**
     * Get all of the client group's schedules dates.
     */
    public function groupScheduleDates()
    {
        $groups = $this->groups()->with(['schedules.dates'])->wherePivot('is_active', 1)->get();
        $scheduleDates = collect();
        $gsch = null;
        foreach ($groups as $g) {
            $gsch = $g->schedules;
            foreach ($gsch as $sch) {
                $scheduleDates = $scheduleDates->merge($sch->dates);
            }
        }
        return $scheduleDates;
    }

    public function allScheduleDates($orderBy = [])
    {
        $schedulesDates = $this->groupScheduleDates($orderBy);
        $result = null;
        foreach ($schedulesDates as $i => $schd) {
            $result = DB::table('client_classes')
                    ->where('schedule_date_id', $schd->id)
                    ->where('client_id', $this->id)
                    ->first();
            if (!empty($result)) {
                $schedulesDates[$i]->taken = $result->taken;
                $schedulesDates[$i]->canceled_at = $result->canceled_at;
            }
            else {
                $schedulesDates[$i]->taken = 0;
                $schedulesDates[$i]->canceled_at = null;
            }
        }
        if (!empty($orderBy)) {
            if ($orderBy[1] === 'asc') {
                $schedulesDates = $schedulesDates->sortBy($orderBy[0])->values();
            }
            else {
                $schedulesDates = $schedulesDates->sortByDesc($orderBy[0])->values();
            }
        }
        return $schedulesDates;
    }

    /**
     * The related commands
     */
    public function commands()
    {
        return $this->hasMany('App\Command');
    }

    /**
     * The related purchases
     */
    public function purchases()
    {
        return $this->hasMany('App\Purchasable');
    }

    public function getFullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * Get the related user.
     */
    public function user()
    {
        return $this->morphOne('App\User', 'authenticatable');
    }

    /**
     * Get related gender
     */
    public function gender()
    {
        return $this->belongsTo('App\Gender');
    }
}
