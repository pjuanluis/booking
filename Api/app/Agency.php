<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

class Agency extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
    	'name', 'email', 'is_active', 'code'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * Get the related user.
     */
    public function user()
    {
        return $this->morphOne('App\User', 'authenticatable');
    }

    /**
     * The related file
     */
    public function file()
    {
        return $this->morphOne('App\File', 'fileable');
    }

    /**
     * Get the cammand for the agency.
     */
    public function commands()
    {
        return $this->hasMany('App\Command');
    }

    /**
     * Update the booking_code of the commands that belong to the agency
     *
     * @return void
     */
    public function updateBookingCode()
    {
        foreach ($this->commands as $key => $command) {
            $command->updateBookingCode();
        }
    }
}
