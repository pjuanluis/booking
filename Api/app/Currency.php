<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

class Currency extends BaseModel
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'code', 'slug', 'is_active'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    public function purchases() {
        return $this->hasMeny('App\Purchase');
    }

    /**
     * The related exchange rates
     */
    public function exchangeRates()
    {
        return $this->hasMany('App\ExchangeRate', 'base_currency_id');
    }

    /**
     * Convert the specified amount to the specified currency
     * @param float $amount
     * @param int $currencyId
     * @return int
     */
    public function convertTo($amount, $currencyId, $exchangeRates = [])
    {
        if ($this->id == $currencyId) {
            return $amount;
        }
        if (empty($exchangeRates)) {
            $exchangeRates = $this->exchangeRates;
        }
        foreach ($exchangeRates as $er) {
            if ($er->to_currency_id == $currencyId) {
                return $amount * $er->amount;
            }
        }
        return 0;
    }

    public function delete() {
        // Borrar los tipos de cambios de la moneda
        $this->exchangeRates()->delete();
        // Eliminar la moneda
        parent::delete();
    }
}
