<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class BaseModel extends Model
{

    public function getModelReferences()
    {
        $result = DB::select("" .
                "select R.TABLE_NAME, R.COLUMN_NAME ".
                "from INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE u ".
                "inner join INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS FK ".
                    "on U.CONSTRAINT_CATALOG = FK.UNIQUE_CONSTRAINT_CATALOG ".
                    "and U.CONSTRAINT_SCHEMA = FK.UNIQUE_CONSTRAINT_SCHEMA ".
                    "and U.CONSTRAINT_NAME = FK.UNIQUE_CONSTRAINT_NAME ".
                "inner join INFORMATION_SCHEMA.KEY_COLUMN_USAGE R ".
                    "ON R.CONSTRAINT_CATALOG = FK.CONSTRAINT_CATALOG ".
                    "AND R.CONSTRAINT_SCHEMA = FK.CONSTRAINT_SCHEMA ".
                    "AND R.CONSTRAINT_NAME = FK.CONSTRAINT_NAME ".
                "WHERE U.COLUMN_NAME = '" . $this->primaryKey . "' ".
                    "AND U.TABLE_SCHEMA = 'public' ".
                    "AND U.TABLE_NAME = '" . $this->getTable() . "'");
        return $result;
    }

    /**
     *
     * @param array $extraTables Array of array table - field names of additional tables to look
     * for any others relationships.
     * @param array $ignoreTables Array of table names or arrays with table - name that
     * will be ignored in the searching of relationships.
     * @return boolean
     */
    public function activeReferencedTables($referencedColumn = null, array $extraTables = [], array $ignoreTables = [])
    {
        if (empty($referencedColumn)) {
            $referencedColumn = $this->primaryKey;
        }
        if (!empty($extraTables)) {
            foreach ($extraTables as $r) {
                $count = DB::table($r[0])
                        ->where($r[1], $this->getAttribute($referencedColumn))
                        ->count();
                if ($count > 0) {
                    return false;
                }
            }
        }
        // Obtener referencias al modelo
        $references = $this->getModelReferences();
        // Checar si hay referencias activas al modelo
        $ignore = false;
        $referencedTables = [];
        foreach ($references as $r) {
            $ignore = false;
            foreach ($ignoreTables as $it) {
                if (is_array($it)) {
                    if ($r->table_name === $it[0] && $r->column_name === $it[1]) {
                        $ignore = true;
                        break;
                    }
                }
                else {
                    if ($r->table_name === $it) {
                        $ignore = true;
                        break;
                    }
                }
            }
            if ($ignore) {
                continue;
            }
            $columns = DB::connection()->getSchemaBuilder()->getColumnListing($r->table_name);
            $count = DB::table($r->table_name)
                    ->where($r->column_name, $this->getAttribute($referencedColumn));
            // Si existe la columna deleted_at, checar que su valor sea nulo
            if (in_array('deleted_at', $columns)) {
                $count = $count->whereNull('deleted_at');
            }
            $count = $count->count();
            if ($count > 0) {
                $referencedTables[] = $r->table_name;
            }
        }
        return $referencedTables;
    }

    public function canBeDeleted($referencedColumn = null, array $extraTables = [], array $ignoreTables = [])
    {
        $referencedTables = $this->activeReferencedTables($referencedColumn, $extraTables, $ignoreTables);
        if (count($referencedTables) > 0) {
            return false;
        }
        return true;
    }

    public static function parseTableToModelName($table)
    {
        $modelsDictionary = [
            'File', 'Client', 'Command', 'CommandCourse', 'CommandHousing', 'Country', 'CourseRate', 'Housing', 'HousingRate', 'HousingRoom', 'HousingRoomPackage',
            'Package', 'Payment', 'Pickup', 'Residence', 'Room', 'RoomType', 'User'
        ];
        $class = null;
        foreach ($modelsDictionary as $model) {
            $class = '\App\\' . $model;
            if ((new $class)->getTable() == $table) {
                return $model;
            }
        }
        return null;
    }
}
