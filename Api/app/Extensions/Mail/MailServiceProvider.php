<?php

namespace App\Extensions\Mail;

use Illuminate\Mail\MailServiceProvider as MailProvider;
use App\Extensions\Mail\TransportManager;

class MailServiceProvider extends MailProvider
{
    /**
     * Register the Swift Transport instance.
     *
     * @return void
     */
    protected function registerSwiftTransport()
    {
        $this->app->singleton('swift.transport', function ($app) {
            return new TransportManager($app);
        });
    }
}
