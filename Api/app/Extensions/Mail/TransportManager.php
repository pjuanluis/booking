<?php

namespace App\Extensions\Mail;

use Illuminate\Mail\TransportManager as Manager;
use App\Extensions\Mail\PostalTransport;

class TransportManager extends Manager
{
    /**
     * Create an instance of the Postal Swift Transport driver.
     *
     * @return \App\Extensions\PostalTransport
     */
    protected function createPostalDriver()
    {
        $config = $this->app['config']->get('services.postal', []);

        return new PostalTransport(
            $this->guzzle($config),
            $config['key'], $config['url']
        );
    }
}
