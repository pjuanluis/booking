<?php

namespace App\Extensions\Mail;

use Swift_Mime_Message;
use GuzzleHttp\ClientInterface;
use Illuminate\Mail\Transport\Transport;

class PostalTransport extends Transport
{
    /**
     * Guzzle client instance.
     *
     * @var \GuzzleHttp\ClientInterface
     */
    protected $client;

    /**
     * The Postal API key.
     *
     * @var string
     */
    protected $key;

    /**
     * The Postal API end-point.
     *
     * @var string
     */
    protected $url;

    /**
     * Create a new Postal transport instance.
     *
     * @param  \GuzzleHttp\ClientInterface  $client
     * @param  string  $key
     * @param  string  $url
     * @return void
     */
    public function __construct(ClientInterface $client, $key, $url)
    {
        $this->key = $key;
        $this->client = $client;
        $this->url = $url . '/send/raw';
    }

    public function send(Swift_Mime_Message $message, &$failedRecipients = null): int
    {
        $this->beforeSendPerformed($message);

        $this->client->post($this->url, $this->payload($message));

        $this->sendPerformed($message);

        return $this->numberOfRecipients($message);
    }

    /**
     * Get the HTTP payload for sending the Postal message.
     *
     * @param  \Swift_Mime_SimpleMessage  $message
     * @param  string  $to
     * @return array
     */
    protected function payload(Swift_Mime_Message $message)
    {
        // Get the from field
        $from = $message->getFrom();
        $fromAddress = key($from);
        $data = [
            'mail_from' => $fromAddress,
            'rcpt_to' => $this->getTo($message),
            'data' => base64_encode($message->toString())
        ];
        return [
            'headers' => [
                'X-Server-API-Key' => $this->key,
            ],
            'json' => $data,
        ];
    }

    /**
     * Get the "to" payload field for the API request.
     *
     * @param  \Swift_Mime_SimpleMessage  $message
     * @return string
     */
    protected function getTo(Swift_Mime_Message $message)
    {
        return collect($this->allContacts($message))->map(function ($display, $address) {
            return $address;
        })->values();
    }

    /**
     * Get all of the contacts for the message.
     *
     * @param  \Swift_Mime_SimpleMessage  $message
     * @return array
     */
    protected function allContacts(Swift_Mime_Message $message)
    {
        return array_merge(
            (array) $message->getTo(), (array) $message->getCc(), (array) $message->getBcc()
        );
    }
}
