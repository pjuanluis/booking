<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends BaseModel
{
    use SoftDeletes;
    
    protected $fillable = [
    	'name', 'level', 'is_private', 'level_finish_at'
    ];
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at',];
        
    /**
     * Get related clients
     */
    public function clients()
    {
        return $this->belongsToMany('App\Client', 'group_students')->withPivot('is_active')->withTimestamps();
    }
    
    /**
     * Get all of the group's schedules.
     */
    public function schedules()
    {
        return $this->morphMany('App\Schedule', 'schedulable');
    }
}
