<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Package extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
    	'type', 'name', 'description', 'unit', 'quantity', 'cost', 'is_active',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at',];

    /**
     * Get the related rates
     */
    public function rates()
    {
        return $this->hasMany('App\CourseRate');
    }

    /**
     * Get related experiences
     */
    public function experiences()
    {
        return $this->belongsToMany('App\Experience', 'package_experiences');
    }

    public function findRate($quantity)
    {
        $rates = $this->rates;
        foreach ($rates as $rate) {
            if ($rate->to_quantity !== null) {
                if ($quantity >= $rate->quantity && $quantity <= $rate->to_quantity) {
                    return $rate;
                }
            }
            else if ($quantity >= $rate->quantity) {
                return $rate;
            }
        }
        return null;
    }


    public function findHousingRate($date, $unit)
    {
        $rates = $rates = \App\HousingRate::where('housing_room_package_id', $this->pivot->id)
                ->orderBy('from_date')
                ->get();
        foreach ($rates as $rate) {
            if ($rate->unit === $unit) {
                if ($rate->from_date !== null) {
                    if ((new Carbon($date))->greaterThanOrEqualTo(new Carbon($rate->from_date)) &&
                            (new Carbon($date))->lessThanOrEqualTo(new Carbon($rate->to_date))) {
                        return $rate;
                    }
                }
            }
        }
        foreach ($rates as $rate) {
            if ($rate->unit === $unit) {
                if ($rate->from_date === null) {
                    return $rate;
                }
            }
        }
        return null;
    }
}
