<?php

namespace App\Transformers;

use App\Package;
use League\Fractal\TransformerAbstract;

class PackageTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = ['rates', 'experiences'];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    public function transform(Package $package)
    {
        $data = [
            'id' => (int) $package->id,
            'name' => $package->name,
            'type' => $package->type,
            'description' => $package->description,
            'unit' => $package->unit,
            'quantity' => $package->quantity,
            'cost' => $package->cost,
            'is_active' => $package->is_active,
            'created_at' => $package->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $package->updated_at->format('Y-m-d H:i:s'),
        ];
        if (isset($package->pivot)) {
            $data['housing_room_package_id'] = $package->pivot->id;
            $data['housingRoomPackage'] = [
                'id' => $package->pivot->id,
                'is_active' => $package->pivot->is_active,
                'housing_room_id' => $package->pivot->housing_room_id,
                'created_at' =>  (new \Carbon\Carbon($package->pivot->created_at))->format('Y-m-d H:i:s'),
                'updated_at' =>  (new \Carbon\Carbon($package->pivot->updated_at))->format('Y-m-d H:i:s'),
            ];
        }
        return $data;
    }

    /**
     * Include rates
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeRates(Package $package)
    {
        $rates = null;
        if (isset($package->pivot->id)) {
            $rates = \App\HousingRate::where('housing_room_package_id', $package->pivot->id)->orderBy('from_date')->get();
            return $this->collection($rates, new HousingRateTransformer());
        }
        $rates = $package->rates;
        return $this->collection($rates, new CourseRateTransformer());
    }

    /**
     * Include experiences
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeExperiences(Package $package)
    {
        $experiences = $package->experiences;
        return $this->collection($experiences, new ExperienceTransformer());
    }
}
