<?php

namespace App\Transformers;

use App\SystemModule;
use League\Fractal\TransformerAbstract;

class SystemModuleTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'permissions'
    ];
    
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];
    
    public function transform(SystemModule $systemModule)
    {
        $data = [
            'id' => (int) $systemModule->id,
            'name' => $systemModule->name,
            'action' => $systemModule->action,
            'slot' => $systemModule->slot,
            'is_active' => $systemModule->is_active,
        ];
        return $data;
    }

    /**
     * Include permissions
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includePermissions(SystemModule $systemModule)
    {
        $permissions = $systemModule->permissions;
        return $this->collection($permissions, new SystemPermissionTransformer());
    }
}