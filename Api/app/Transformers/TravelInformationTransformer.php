<?php

namespace App\Transformers;

use App\TravelInformation;
use League\Fractal\TransformerAbstract;

class TravelInformationTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];
    
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];
    
    public function transform(TravelInformation $travelInformation)
    {
        $data = [
            'id' => (int) $travelInformation->id,
            'command_id' => $travelInformation->command_id,
            'travel_by' => $travelInformation->travel_by,
            'arrival_place' => $travelInformation->arrival_place,
            'arrival_carrier' => $travelInformation->arrival_carrier,
            'arrival_at' => (string) $travelInformation->arrival_at,
            'arrival_travel_number' => $travelInformation->arrival_travel_number,
            'departure_place' => $travelInformation->departure_place,
            'authenticatable_id' => $travelInformation->authenticatable_id,
            'departure_carrier' => $travelInformation->departure_carrier,
            'departure_at' => (string) $travelInformation->departure_at,
            'departure_travel_number' => $travelInformation->departure_travel_number,
            'created_at' => (String) $travelInformation->created_at,
            'updated_at' => (String) $travelInformation->updated_at
        ];

        return $data;
    }
}