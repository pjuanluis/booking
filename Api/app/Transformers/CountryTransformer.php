<?php

namespace App\Transformers;

use App\Country;
use League\Fractal\TransformerAbstract;

class CountryTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    public function transform(Country $country)
    {
        $data = [
            'id' => (int) $country->id,
            'name' => $country->name,
            'code' => $country->code,
            'order' => $country->order,
            'is_active' => $country->is_active,
            'created_at' => $country->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $country->updated_at->format('Y-m-d H:i:s'),
        ];
        return $data;
    }
}
