<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Purchasable;

class PurchasableTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'consumptionCenter', 'currency', 'client', 'details', 'user'
    ];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    private $validParams = [];

    public function transform(Purchasable $purchasable)
    {
        $ret = [
            'id' => $purchasable->id,
            'discount' => (float) $purchasable->discount,
            'consumption_center_id' => $purchasable->consumption_center_id,
            'currency_id' => $purchasable->currency_id,
            'client_id' => $purchasable->client_id,
            'purchasable_id' => $purchasable->purchasable_id,
            'purchasable_type' => $purchasable->purchasable_type,
            'exchange_rates' => $purchasable->exchange_rates
        ];

        return $ret;
    }

    /**
     * Include consumption center
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeConsumptionCenter(Purchasable $purchasable)
    {
        $resource = $purchasable->consumptionCenter;
        return $this->item($resource, new ConsumptionCenterTransformer());
    }

    /**
     * Include currency
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeCurrency(Purchasable $purchasable)
    {
        $resource = $purchasable->currency;
        return $this->item($resource, new CurrencyTransformer());
    }

    /**
     * Include client
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeClient(Purchasable $purchasable)
    {
        $resource = $purchasable->client;
        if (isset($resource)) {
            return $this->item($resource, new ClientTransformer());
        }
        return $this->null();
    }

    /**
     * Include pruchaseDetail
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeDetails(Purchasable $purchasable)
    {
        $transformer = null;
        $resource = null;
        switch ($purchasable->purchasable_type) {
            case 'App\Command':
                $resource = $purchasable->command;
                $transformer = new CommandTransformer();
                break;
            case 'App\CommandCourse':
                $resource = $purchasable->commandCourse;
                $transformer = new CommandCourseTransformer();
                break;
            case 'App\CommandHousing':
                $resource = $purchasable->commandHousing;
                $transformer = new CommandHousingTransformer();
                break;
            case 'App\Purchase':
                $resource = $purchasable->purchase;
                $transformer = new PurchaseTransformer();
                break;
            case 'App\Payment':
                $resource = $purchasable->payment;
                $transformer = new PaymentTransformer();
                break;
        }

        if (isset($transformer) && isset($resource)) {
            return $this->item($resource, $transformer);
        }
    }

    /**
     * Include user
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeUser(Purchasable $purchasable)
    {
        $user = $purchasable->user;
        if (isset($user)) {
            return $this->item($user, new UserTransformer());
        }
        return $this->null();
    }
}
