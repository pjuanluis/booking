<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\MethodPayment;

class MethodPaymentTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];
    
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];
    
    private $validParams = [];
    
    public function transform(MethodPayment $methodPayment)
    {
        $ret = [
            'id' => $methodPayment->id,
            'slug' => $methodPayment->slug,
            'name' => $methodPayment->name,
            'is_active' => $methodPayment->is_active
        ];

        return $ret;
    }
}
