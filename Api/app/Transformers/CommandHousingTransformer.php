<?php

namespace App\Transformers;

use App\CommandHousing;
use League\Fractal\TransformerAbstract;

class CommandHousingTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = ['package', 'housingRoom', 'housing', 'room', 'command', 'purchase'];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = ['package', 'housingRoom', 'housing'];

    public function transform(CommandHousing $commandHousing)
    {
        $data = [
            'id' => (int) $commandHousing->id,
            'housing_room_package_id' => (int) $commandHousing->housing_room_package_id,
            'from_date' => $commandHousing->from_date,
            'to_date' => $commandHousing->to_date,
            'quantity' => $commandHousing->quantity,
            'unit' => $commandHousing->unit,
            'cost' => $commandHousing->cost,
            'people' => $commandHousing->people,
            'created_at' => $commandHousing->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $commandHousing->updated_at->format('Y-m-d H:i:s'),
        ];
        return $data;
    }

    /**
     * Include package
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includePackage(CommandHousing $commandHousing)
    {
        $package = $commandHousing->package;
        if ($package === null) {
            return $this->null();
        }
        return $this->item($package, new PackageTransformer());
    }

    /**
     * Include housing room
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeHousingRoom(CommandHousing $commandHousing)
    {
        $housingRoom = $commandHousing->housingRoom;
        return $this->item($housingRoom, new HousingRoomTransformer());
    }

    /**
     * Include housing
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeHousing(CommandHousing $commandHousing)
    {
        $housing = $commandHousing->housing;
        return $this->item($housing, new HousingTransformer());
    }

    /**
     * Include room
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeRoom(CommandHousing $commandHousing)
    {
        $room = $commandHousing->room;
        if ($room === null) {
            return $this->null();
        }
        return $this->item($room, new RoomTransformer());
    }

    /**
     * Include room
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeCommand(CommandHousing $commandHousing)
    {
        $command = $commandHousing->command;
        if ($command === null) {
            return $this->null();
        }
        return $this->item($command, new CommandTransformer());
    }

    /**
     * Include purchase
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includePurchase(CommandHousing $commandHousing)
    {
        $purchase = $commandHousing->purchase;
        if ($purchase === null) {
            return $this->null();
        }
        return $this->item($purchase, new PurchasableTransformer());
    }
}
