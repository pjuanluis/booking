<?php

namespace App\Transformers;

use App\ExchangeRate;
use League\Fractal\TransformerAbstract;

class ExchangeRateTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'baseCurrency', 'exchangeCurrency'
    ];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = ['exchangeCurrency'];

    public function transform(ExchangeRate $exchangeRate)
    {
        $data = [
            'id' => (int) $exchangeRate->id,
            'base_amount' => $exchangeRate->base_amount,
            'amount' => $exchangeRate->amount,
            'created_at' => $exchangeRate->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $exchangeRate->updated_at->format('Y-m-d H:i:s'),
        ];
        return $data;
    }

    /**
     * Include base currency
     *
     * @return \League\Fractal\ItemResource
     */
    public function includeBaseCurrency(ExchangeRate $exchangeRate)
    {
        $currency = $exchangeRate->baseCurrency;
        return $this->item($currency, new CurrencyTransformer());
    }

    /**
     * Include exchange currency
     *
     * @return \League\Fractal\ItemResource
     */
    public function includeExchangeCurrency(ExchangeRate $exchangeRate)
    {
        $currency = $exchangeRate->exchangeCurrency;
        return $this->item($currency, new CurrencyTransformer());
    }
}
