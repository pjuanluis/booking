<?php

namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'role'
    ];
    
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];
    
    public function transform(User $user)
    {
        $data = [
            'id' => (int) $user->id,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'full_name' => $user->first_name . ' ' . $user->last_name,
            'email' => $user->email,
            'password' => $user->password,
            'remember_token' => $user->remember_token,
            'authenticatable_type' => $user->authenticatable_type,
            'authenticatable_id' => $user->authenticatable_id,
            'created_at' => $user->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $user->updated_at->format('Y-m-d H:i:s'),
        ];
        return $data;
    }

    /**
     * Include Role
     *
     * @return \League\Fractal\ItemResource
     */
    public function includeRole(User $user)
    {
        $role = $user->role;
        if ($role === null) {
            return $this->null();
        }
        return $this->item($role, new RoleTransformer());
    }
}