<?php

namespace App\Transformers;

use App\HousingRoom;
use League\Fractal\TransformerAbstract;

class HousingRoomTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = ['packages', 'roomType'];
    
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = ['roomType'];
    
    public function transform(HousingRoom $housingRoom)
    {
        $data = [
            'id' => (int) $housingRoom->id,
            'name' => $housingRoom->name,
            'description' => $housingRoom->description,
            'beds' => $housingRoom->beds,
            'cost_type' => $housingRoom->cost_type,
            'is_active' => $housingRoom->is_active,
            'created_at' => $housingRoom->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $housingRoom->updated_at->format('Y-m-d H:i:s'),
        ];
        if (isset($housingRoom->availability)) {
            $data['availability'] = $housingRoom->availability;
        }
        return $data;
    }
    
    /**
     * Include room type
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeRoomType(HousingRoom $housingRoom)
    {
        $roomType = $housingRoom->roomType;
        if ($roomType === null) {
            return $this->null();
        }
        return $this->item($roomType, new RoomTypeTransformer());
    }
    
    /**
     * Include packages
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includePackages(HousingRoom $housingRoom)
    {
        $packages = $housingRoom->packages;
        if ($packages === null) {
            return $this->null();
        }
        return $this->collection($packages, new PackageTransformer());
    }
}