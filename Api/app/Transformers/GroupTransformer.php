<?php

namespace App\Transformers;

use App\Group;
use League\Fractal\ParamBag;
use League\Fractal\TransformerAbstract;

class GroupTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = ['clients', 'schedules'];
    
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];
    
    private $validParams = ['is_active'];
    
    public function transform(Group $group)
    {
        $data = [
            'id' => (int) $group->id,
            'name' => $group->name,
            'level' => $group->level,
            'number_students' => $group->clients()->wherePivot('is_active', 1)->get()->count(),
            'is_private' => $group->is_private,
            'created_at' => $group->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $group->updated_at->format('Y-m-d H:i:s'),
        ];
        if (isset($group->clientsData)) {
            $data['clients'] = ['data' => $group->clientsData];
        }
        return $data;
    }
    
    /**
     * Include clients
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeClients(Group $group, ParamBag $params = null)
    {
        $isActive = '';
        if ($params !== null) {
            // Optional params validation
            $usedParams = array_keys(iterator_to_array($params));
            if (array_diff($usedParams, $this->validParams)) {
                throw new \Exception(sprintf(
                    'Invalid param(s): "%s". Valid param(s): "%s"', 
                    implode(',', $usedParams), 
                    implode(',', $this->validParams)
                ));
            }
            $isActive = $params->get('is_active')[0];
        }
        $clients = $group->clients()->orderBy('first_name')->orderBy('last_name');
        // Filtrar por status
        if (!empty($isActive)) {
            $clients = $clients->wherePivot('is_active', $isActive);
        }
        $clients = $clients->get();
        return $this->collection($clients, new ClientTransformer());
    }
    
    /**
     * Include schedules
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeSchedules(Group $group)
    {
        $schedules = $group->schedules;
        return $this->collection($schedules, new ScheduleTransformer());
    }
}