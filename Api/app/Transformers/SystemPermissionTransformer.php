<?php

namespace App\Transformers;

use App\SystemPermission;
use League\Fractal\TransformerAbstract;

class SystemPermissionTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];
    
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];
    
    public function transform(SystemPermission $permission)
    {
        $data = [
            'id' => (int) $permission->id,
            'name' => $permission->name,
            'slug' => $permission->slug,
        ];
        return $data;
    }
}