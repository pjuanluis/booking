<?php

namespace App\Transformers;

use App\Residence;
use League\Fractal\TransformerAbstract;

class ResidenceTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = ['housing', 'rooms'];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    public function transform(Residence $residence)
    {
        $data = [
            'id' => (int) $residence->id,
            'name' => $residence->name,
            'address' => $residence->address,
            'created_at' => $residence->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $residence->updated_at->format('Y-m-d H:i:s'),
        ];
        return $data;
    }

    /**
     * Include housing
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeHousing(Residence $residence)
    {
        $housing = $residence->housing;
        if ($housing === null) {
            return $this->null();
        }
        return $this->item($housing, new HousingTransformer());
    }

    /**
     * Include rooms
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeRooms(Residence $residence)
    {
        // Ordenar cuartos
        $sort_by = 'name';
        $dir = 'asc';
        $rooms = $residence->rooms()
            ->orderBy($sort_by, $dir)
            ->get();
        return $this->collection($rooms, new RoomTransformer());
    }
}
