<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Purchase;

class PurchaseTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = ['purchasable', 'command'];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    private $validParams = [];

    public function transform(Purchase $purchase)
    {
        $ret = [
            'id' => $purchase->id,
            'date' => $purchase->date->format('Y-m-d'),
            'reference_number' => $purchase->reference_number,
            'concept' => $purchase->concept,
            'amount' => $purchase->amount,
            'updated_at' => $purchase->updated_at->format('Y-m-d H:i:s'),
            'created_at' => $purchase->created_at->format('Y-m-d H:i:s')
        ];

        return $ret;
    }

    public function includePurchasable(Purchase $purchase)
    {
        $purchasable = $purchase->purchasable;
        if ($purchasable === null) {
            return $this->null();
        }
        return $this->item($purchasable, new PurchasableTransformer());
    }

    public function includeCommand(Purchase $purchase)
    {
        $command = $purchase->command;
        if ($command === null) {
            return $this->null();
        }
        return $this->item($command, new CommandTransformer());
    }
}
