<?php

namespace App\Transformers;

use App\Agency;
use League\Fractal\TransformerAbstract;


class AgencyTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = ['file'];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Agency $agency)
    {
        $data = [
            'id' => $agency->id,
            'name' => $agency->name,
            'email' => $agency->email,
            'code' => $agency->code,
            'is_active' => $agency->is_active,
            'created_at' => $agency->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $agency->updated_at->format('Y-m-d H:i:s'),
        ];
        return $data;
    }

    /**
     * Include file
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeFile(Agency $agency)
    {
        $file = $agency->file;
        if ($file === null) {
            return $this->null();
        }
        return $this->item($file, new FileTransformer());
    }
}
