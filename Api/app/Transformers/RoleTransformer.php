<?php

namespace App\Transformers;

use App\Role;
use League\Fractal\TransformerAbstract;

class RoleTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'accesses'
    ];
    
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];
    
    public function transform(Role $role)
    {
        $data = [
            'id' => (int) $role->id,
            'name' => $role->name,
            'slug' => $role->slug,
            'is_active' => $role->is_active,
            'created_at' => $role->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $role->updated_at->format('Y-m-d H:i:s'),
        ];
        return $data;
    }

    /**
     * Include permissions
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeAccesses(Role $role)
    {
        $access = $role->accesses();
        return $this->collection($access, new ArrayTransformer());
    }
}