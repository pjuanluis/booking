<?php

namespace App\Transformers;

use App\ScheduleDate;
use League\Fractal\TransformerAbstract;


class ScheduleDateTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = ['schedule'];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(ScheduleDate $scheduleDate)
    {
        $data = [
            'id' => $scheduleDate->id,
            'begin_at' => $scheduleDate->begin_at->format('Y-m-d H:i:s'),
            'finish_at' => $scheduleDate->finish_at->format('Y-m-d H:i:s'),
            'subject' => $scheduleDate->schedule->subject,
            'is_private' => $scheduleDate->schedule->is_private,
            'schedulable_type' => $scheduleDate->schedule->schedulable_type,
            'schedulable' => ['data' => $scheduleDate->schedule->schedulable]
        ];
        if (isset($scheduleDate->taken)) {
            $data['taken'] = $scheduleDate->taken;
            $data['canceled_at'] = $scheduleDate->canceled_at;
        }
        return $data;
    }

    /**
     * Include schedule
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeSchedule(ScheduleDate $scheduleDate)
    {
        $schedule = $scheduleDate->schedule;
        if ($schedule === null) {
            return $this->null();
        }
        return $this->item($schedule, new ScheduleTransformer());
    }
}
