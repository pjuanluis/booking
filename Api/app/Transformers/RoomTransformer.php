<?php

namespace App\Transformers;

use App\Room;
use League\Fractal\TransformerAbstract;
use \Carbon\Carbon;

class RoomTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = ['housingRooms', 'residence', 'commandHousings',
        'blockedDates'];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    private $validParams = [];

    public function transform(Room $room)
    {
        $data = [
            'id' => (int) $room->id,
            'name' => $room->name,
            'beds' => $room->beds,
            'created_at' => $room->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $room->updated_at->format('Y-m-d H:i:s'),
        ];
        return $data;
    }

    /**
     * Include housing rooms
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeHousingRooms(Room $room)
    {
        $housingRooms = $room->housingRooms;
        if ($housingRooms === null) {
            return $this->null();
        }
        return $this->collection($housingRooms, new HousingRoomTransformer());
    }

    /**
     * Include residence
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeResidence(Room $room)
    {
        $residence = $room->residence;
        if ($residence === null) {
            return $this->null();
        }
        return $this->item($residence, new ResidenceTransformer());
    }

    public function includeCommandHousings(Room $room)
    {
        $commandHousings = $room->commandHousings;
        if ($commandHousings === null) {
            return $this->null();
        }
        return $this->collection($commandHousings, new CommandHousingTransformer());
    }

    public function includeBlockedDates(Room $room)
    {
        $resource = $room->blockedDates;

        if ($resource === null) {
            return $this->null();
        }

        return $this->collection($resource, new BlockedRoomTransformer());
    }
}
