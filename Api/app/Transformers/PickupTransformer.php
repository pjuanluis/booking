<?php

namespace App\Transformers;

use App\Pickup;
use League\Fractal\TransformerAbstract;

class PickupTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];
    
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];
    
    public function transform(Pickup $pickup)
    {
        $data = [
            'id' => (int) $pickup->id,
            'name' => $pickup->name,
            'cost' => $pickup->cost,
            'is_active' => $pickup->is_active,
            'created_at' => $pickup->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $pickup->updated_at->format('Y-m-d H:i:s'),
        ];
        return $data;
    }
}