<?php

namespace App\Transformers;

use App\File;
use League\Fractal\TransformerAbstract;


class FileTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(File $file)
    {
        $path = $file->storage_path;
        if (!starts_with($path, "http://") && !starts_with($path, "https://")) {
            $path = asset('storage/'.$file->storage_path);
        }
        $data = [
            'id' => $file->id,
            'name' => $file->name,
            'mime' => $file->mime,
            'size' => $file->size,
            'path' => $path,
            'is_active' => $file->is_active,
            'created_at' => $file->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $file->updated_at->format('Y-m-d H:i:s'),
        ];
        return $data;
    }
}