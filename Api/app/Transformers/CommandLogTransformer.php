<?php

namespace App\Transformers;

use App\CommandLog;
use League\Fractal\TransformerAbstract;


class CommandLogTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = ['user', 'command'];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = ['user'];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(CommandLog $commandLog)
    {
        $data = [
            'id' => (int)$commandLog->id,
            'action' => $commandLog->action,
            'url' => $commandLog->url,
            'data' => $commandLog->data,
            'created_at' => $commandLog->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $commandLog->updated_at->format('Y-m-d H:i:s'),
        ];
        return $data;
    }

    /**
     * Include user
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeUser(CommandLog $commandLog)
    {
        $user = $commandLog->user;
        if ($user === null) {
            return $this->null();
        }
        return $this->item($user, new UserTransformer());
    }

    /**
     * Include command
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeCommand(CommandLog $commandLog)
    {
        $command = $commandLog->command;
        if ($command === null) {
            return $this->null();
        }
        return $this->item($command, new CommandTransformer());
    }
}
