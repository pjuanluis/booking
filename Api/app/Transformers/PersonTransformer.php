<?php

namespace App\Transformers;

use App\Person;
use League\Fractal\TransformerAbstract;

class PersonTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    public function transform(Person $person)
    {
        $data = [
            'id' => (int) $person->id,
            'first_name' => $person->first_name,
            'last_name' => $person->last_name,
            'full_name' => $person->first_name . ' ' . $person->last_name,
            'email' => $person->email,
            'position' => $person->position,
            'type' => $person->type,
            'abbreviation' => $person->abbreviation,
            'created_at' => $person->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $person->updated_at->format('Y-m-d H:i:s'),
        ];
        return $data;
    }
}
