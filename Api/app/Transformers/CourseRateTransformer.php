<?php

namespace App\Transformers;

use App\CourseRate;
use League\Fractal\TransformerAbstract;

class CourseRateTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];
    
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];
    
    public function transform(CourseRate $courseRate)
    {
        $data = [
            'id' => (int) $courseRate->id,
            'quantity' => $courseRate->quantity,
            'to_quantity' => $courseRate->to_quantity,
            'unit_cost' => $courseRate->unit_cost,
            'is_active' => $courseRate->is_active,
            'created_at' => $courseRate->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $courseRate->updated_at->format('Y-m-d H:i:s'),
        ];
        return $data;
    }
}