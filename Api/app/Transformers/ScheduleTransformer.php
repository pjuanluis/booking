<?php

namespace App\Transformers;

use App\Schedule;
use League\Fractal\TransformerAbstract;
use League\Fractal\ParamBag;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class ScheduleTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'responsible', 'place', 'schedulable', 'experiences'
    ];
    
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = ['dates'];
    
    private $validParams = ['with_group_clients'];
    
    public function transform(Schedule $schedule)
    {
        $data = [
            'id' => (int) $schedule->id,
            'schedulable_id' => $schedule->schedulable_id,
            'schedulable_type' => $schedule->schedulable_type,
            'subject' => $schedule->subject,
            'is_private' => $schedule->is_private,
            'created_at' => $schedule->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $schedule->updated_at->format('Y-m-d H:i:s'),
        ];
        if (isset($schedule->taken)) {
            $data['taken'] = $schedule->taken;
            $data['canceled_at'] = $schedule->canceled_at;
        }
        return $data;
    }

    /**
     * Include dates
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeDates(Schedule $schedule)
    {
        $dates = $schedule->dates()->orderBy('begin_at')->get();
        return $this->collection($dates, new ScheduleDateTransformer());
    }

    /**
     * Include responsible
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeResponsible(Schedule $schedule)
    {
        $responsible = $schedule->responsible;
        if ($responsible === null) {
            return $this->null();
        }
        return $this->item($responsible, new PersonTransformer());
    }

    /**
     * Include place
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includePlace(Schedule $schedule)
    {
        $place = $schedule->place;
        if ($place === null) {
            return $this->null();
        }
        return $this->item($place, new PlaceTransformer());
    }

    /**
     * Include schedulable
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeSchedulable(Schedule $schedule, ParamBag $params = null)
    {
        $withGroupClients = false;
        if ($params !== null) {
            // Optional params validation
            $usedParams = array_keys(iterator_to_array($params));
            if (array_diff($usedParams, $this->validParams)) {
                throw new \Exception(sprintf(
                    'Invalid param(s): "%s". Valid param(s): "%s"', 
                    implode(',', $usedParams), 
                    implode(',', $this->validParams)
                ));
            }
            $withGroupClients = $params->get('with_group_clients')[0] == 1 ? true : false;
        }
        $schedulable = $schedule->schedulable;
        if ($schedulable === null) {
            return $this->null();
        }
        $clients = null;
        if ($schedule->schedulable_type === 'App\Group') {
            if ($withGroupClients) {
                $clients = $schedulable->clients()
                        ->wherePivot('is_active', 1)
                        ->orderBy('first_name')
                        ->orderBy('last_name')
                        ->get();
                $schedulable->clientsData = $clients;
            }
            return $this->item($schedulable, new GroupTransformer());
        }
        return $this->item($schedulable, new ClientTransformer());
    }
    
    /**
     * Include experiences
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeExperiences(Schedule $schedule)
    {
        $experiences = $schedule->experiences;
        return $this->collection($experiences, new ExperienceTransformer());
    }
}