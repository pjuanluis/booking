<?php

namespace App\Transformers;

use App\Housing;
use League\Fractal\TransformerAbstract;
use League\Fractal\ParamBag;

class HousingTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'packages', 'housingRooms'
    ];

    /**
     * Valid params
     */
    private $validParams = ['is_active'];
    
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];
    
    public function transform(Housing $housing)
    {
        $data = [
            'id' => (int) $housing->id,
            'name' => $housing->name,
            'slug' => $housing->slug,
            'is_active' => $housing->is_active,
            'created_at' => $housing->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $housing->updated_at->format('Y-m-d H:i:s'),
        ];
        return $data;
    }

    /**
     * Include packages
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includePackages(Housing $housing)
    {
        $packages = $housing->packages;
        if ($packages === null) {
            return $this->null();
        }
        return $this->collection($packages, new PackageTransformer());
    }

    private function hasUsedParams($params)
    {
        if ($params === null) {
            return false;
        }

        $usedParams = array_keys(iterator_to_array($params));
        if ($invalidParams = array_diff($usedParams, $this->validParams)) {
            throw new \Exception(sprintf(
                'Invalid param(s): "%s". Valid param(s): "%s"', 
                implode(',', $usedParams), 
                implode(',', $this->validParams)
            ));
        }

        return count($usedParams) > 0;
    }

    /**
     * Include rooms
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeHousingRooms(Housing $housing, ParamBag $params = null)
    {
        $rooms = null;
        
        if ($this->hasUsedParams($params)) {
            $rooms = $housing->housingRooms()
                ->where('is_active', (int) $params->get('is_active')[0])
                ->get();
        }

        if ($rooms === null) {
            $rooms = $housing->housingRooms;
        }

        if ($rooms === null) {
            return $this->null();
        }
        
        return $this->collection($rooms, new HousingRoomTransformer(), 'housing_rooms');
    }
}