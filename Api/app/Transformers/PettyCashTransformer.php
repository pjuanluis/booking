<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\PettyCash;

class PettyCashTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = ['consumptionCenter', 'file', 'user'];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = ['file', 'user'];

    private $validParams = [];

    public function transform(PettyCash $pettyCash)
    {
        $ret = [
            'id' => $pettyCash->id,
            'date' => $pettyCash->created_at->format('Y-m-d H:i:s'),
            'description' => $pettyCash->description,
            'amount' => $pettyCash->amount,
            'balance' => isset($pettyCash->balance)? $pettyCash->balance : '',
            'is_deposit' => $pettyCash->is_deposit,
            'created_at' => $pettyCash->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $pettyCash->updated_at->format('Y-m-d H:i:s'),
        ];
        return $ret;
    }

    /**
     * Include file
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeFile(PettyCash $pettyCash)
    {
        $file = $pettyCash->file;
        if ($file === null) {
            return $this->null();
        }
        return $this->item($file, new FileTransformer());
    }

    /**
     * Include consumption center
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeConsumptionCenter(PettyCash $pettyCash)
    {
        $consumptionCenter = $pettyCash->consumptionCenter;
        return $this->item($consumptionCenter, new ConsumptionCenterTransformer());
    }

    /**
     * Include user
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeUser(PettyCash $pettyCash)
    {
        $user = $pettyCash->user;
        if (!empty($user)) {
            return $this->item($user, new UserTransformer());
        }
        return $this->null();
    }
}
