<?php

namespace App\Transformers;

use App\Experience;
use League\Fractal\TransformerAbstract;

class ExperienceTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'packages',
    ];
    
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];
    
    public function transform(Experience $experience)
    {
        $data = [
            'id' => (int) $experience->id,
            'name' => $experience->name,
            'slug' => $experience->slug,
            'description' => $experience->description,
            'created_at' => $experience->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $experience->updated_at->format('Y-m-d H:i:s'),
        ];
        return $data;
    }

    /**
     * Include packages
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includePackages(Experience $experience)
    {
        $packages = $experience->packages;
        if ($packages === null) {
            return $this->null();
        }
        return $this->collection($packages, new PackageTransformer());
    }
}