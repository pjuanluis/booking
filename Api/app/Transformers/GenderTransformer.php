<?php

namespace App\Transformers;

use App\Gender;
use League\Fractal\TransformerAbstract;


class GenderTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Gender $gender)
    {
        $data = [
            'id' => $gender->id,
            'name' => $gender->name,
            'slug' => $gender->slug,
            'is_active' => $gender->is_active,
            'created_at' => $gender->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $gender->updated_at->format('Y-m-d H:i:s'),
        ];
        return $data;
    }
}
