<?php

namespace App\Transformers;

use App\CommandCourse;
use League\Fractal\TransformerAbstract;

class CommandCourseTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = ['package', 'purchase'];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = ['package'];

    public function transform(CommandCourse $commandCourse)
    {
        $data = [
            'id' => (int) $commandCourse->id,
            'from_date' => $commandCourse->from_date,
            'to_date' => $commandCourse->to_date,
            'quantity' => $commandCourse->quantity,
            'cost' => $commandCourse->cost,
            'created_at' => $commandCourse->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $commandCourse->updated_at->format('Y-m-d H:i:s'),
        ];
        return $data;
    }

    /**
     * Include package
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includePackage(CommandCourse $commandCourse)
    {
        $package = $commandCourse->package;
        return $this->item($package, new PackageTransformer());
    }

    /**
     * Include purchase
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includePurchase(CommandCourse $commandCourse)
    {
        $purchase = $commandCourse->purchase;
        if ($purchase === null) {
            return $this->null();
        }
        return $this->item($purchase, new PurchasableTransformer());
    }
}
