<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\ConsumptionCenter;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class ConsumptionCenterTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = ['pettyCashes'];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    private $validParams = [];

    private $request = null;

    public function transform(ConsumptionCenter $consumptionCenter)
    {
        // Obtener el total depositado
        $totalDeposit = $consumptionCenter
            ->pettyCashes()
            ->where('is_deposit', 1)
            ->pluck('amount')
            ->sum();
        // Obtener el total retirado
        $totalWithdrawal = $consumptionCenter
            ->pettyCashes()
            ->where('is_deposit', 0)
            ->pluck('amount')
            ->sum();
        $ret = [
            'id' => $consumptionCenter->id,
            'slug' => $consumptionCenter->slug,
            'name' => $consumptionCenter->name,
            'balance' => $totalDeposit - $totalWithdrawal,
            'is_active' => $consumptionCenter->is_active,
            'created_at' => $consumptionCenter->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $consumptionCenter->updated_at->format('Y-m-d H:i:s'),
        ];

        return $ret;
    }

    public function setRequest(Request $request) {
        $this->request = $request;
    }

    public function includePettyCashes(ConsumptionCenter $consumptionCenter)
    {
        $paginate = true;
        $sortBy = 'created_at';
        $sortDir = 'DESC';
        if ($this->request !== null && $this->request->get('no-paginate', '0') == 1) {
            $paginate = false;
        }

        $paginator = $consumptionCenter->pettyCashes()->orderBy($sortBy, $sortDir);

        // Aquí agregar los filtros

        // Paginar registros
        if ($paginate) {
            // Todos lo registros (movimientos de una caja chica)
            $fullCollection = $paginator->get();
            // Paginar registros (movimientos de una caja chica)
            $paginator = $paginator->paginate(20);
            $this->addBalanceProperty($fullCollection, $paginator, $sortBy, $sortDir);
            $pettyCashes = $paginator->getCollection();
            $resource = $this->collection($pettyCashes, new PettyCashTransformer());
            return $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));
        }
        // No paginar, retornar todos los registros encontrados
        else {
            $result = $this->addBalanceProperty($paginator->get(), null, $sortBy, $sortDir, false);
            return $this->collection($result, new PettyCashTransformer());
        }
    }

    /**
     * Calcula el balance y crea la propiedad balance para cada objeto PettyCash de la colección
     *
     * @param  Collection  $fullCollection  Todos los movimientos de una caja chica, deben estar ordenados de forma ascendente (ASC), en base a la fecha de creación (created_at)
     * @param  Paginator $paginator Los movimientos de una caja chica ya páginados
     */
    private function addBalanceProperty($fullCollection, $paginator, $sortBy, $sortDir, $paginate = true) {
        if ($paginate) {
            // Obtener los registros paginados(en forma de colección)
            $paginatedCollection = $paginator->getCollection();
        }
        $balance = 0;
        // Para que el algoritmo de calcular el balance funcione, los registros deben estar ordenados de forma ascendente (ASC)
        $fullCollection = $fullCollection->sortBy('created_at')->values(); //Ordena los registros de forma ascendente
        // Recorrer cada uno de los registros y crear la propiedad balance en cada uno de ellos
        $fullCollection->map(function($move, $index) use(&$balance) {
            if ($balance === 0 && $move->is_deposit == 1) {
                // Debe de entrar solo en la primera iteración, ya que siempre el primer movimiento de una caja chica debe ser un depósito
                $balance = $move->amount;
            } elseif ($move->is_deposit == 1) { // Es un deposito
                $balance += $move->amount;
            } elseif ($move->is_deposit == 0) { // Es un retiro
                $balance -= $move->amount;
            }
            $move->balance = $balance; // Crear la propied balance y guardar el balance actual
        });

        if ($paginate) {
            // Solo se obtienen los elementos que estén tanto en $fullCollection como $paginatedCollection, los demás son descartados
            $fullCollection = $fullCollection->intersect($paginatedCollection);
        }

        // Ordenar resultados de forma descendente
        if ($sortDir === 'DESC') {
            $fullCollection = $fullCollection->sortByDesc($sortBy)->values();
        } else {
            // Ordenar resultados de forma ascendente
            $fullCollection = $fullCollection->sortBy($sortBy)->values();
        }

        if ($paginate) {
            // Guardar la nueva colección
            $paginator->setCollection($fullCollection);
        } else {
            return $fullCollection;
        }
    }
}
