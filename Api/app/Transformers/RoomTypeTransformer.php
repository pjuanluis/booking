<?php

namespace App\Transformers;

use App\RoomType;
use League\Fractal\TransformerAbstract;

class RoomTypeTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];
    
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];
    
    public function transform(RoomType $roomType)
    {
        $data = [
            'id' => (int) $roomType->id,
            'name' => $roomType->name,
            'slug' => $roomType->slug,
            'description' => $roomType->description,
            'is_active' => $roomType->is_active,
            'created_at' => $roomType->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $roomType->updated_at->format('Y-m-d H:i:s'),
        ];
        return $data;
    }
}