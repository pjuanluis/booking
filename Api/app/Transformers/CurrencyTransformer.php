<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Currency;

class CurrencyTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = ['exchangeRates'];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    public function transform(Currency $currency)
    {
        $ret = [
            'id' => (int) $currency->id,
            'slug' => $currency->slug,
            'name' => $currency->name,
            'code' => $currency->code,
            'is_active' => $currency->is_active,
            'created_at' => $currency->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $currency->updated_at->format('Y-m-d H:i:s'),
        ];

        return $ret;
    }

    /**
     * Include related exchange rates
     * @param \App\Currency $currency
     * @return \League\Fractal\Resource\Collection
     */
    public function includeExchangeRates(Currency $currency)
    {
        $rates = $currency->exchangeRates;
        return $this->collection($rates, new ExchangeRateTransformer());
    }
}
