<?php

namespace App\Transformers;

use App\Command;
use App\TravelInformation;
use League\Fractal\TransformerAbstract;
use League\Fractal\ParamBag;
use DB;

class CommandTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'commandCourses', 'commandHousing', 'payments', 'pickup', 'client', 'travelInformation', 'agency', 'bills', 'extraPurchases', 'purchase',
    ];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    private $validParams = ['from_date'];

    public function transform(Command $command)
    {
        $payments = $command->payments()->whereIn('type', ['registration_fee', 'reservation_payment'])->get();
        $data = [
            'id' => (int) $command->id,
            'client_id' => $command->client_id,
            'client_first_name' => $command->client_first_name,
            'client_last_name' => $command->client_last_name,
            'client_name' => $command->client_first_name . ' ' . $command->client_last_name,
            'client_email' => $command->client_email,
            'from_date' => $command->from_date,
            'to_date' => $command->to_date,
            'status' => $command->status,
            'booking_code' => $command->booking_code,
            'token' => $command->token,
            'hasRegistrationPayment' => $payments->where('type', 'registration_fee')->count() > 0,
            'hasReservationPayment' => $payments->where('type', 'reservation_payment')->count() > 0,
            'hasClientData' => !empty($command->client),
            'pickup_cost' => $command->pickup_cost,
            'arrival_at' => null,
            'travel_partner' => $command->travel_partner,
            'payment_status' => $command->payment_status,
            'comments' => $command->comments,
            'created_at' => $command->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $command->updated_at->format('Y-m-d H:i:s'),
        ];
        return $data;
    }

    /**
     * Include command courses
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeCommandCourses(Command $command, ParamBag $params = null)
    {
        $fromDate = '';
        if ($params !== null) {
            // Optional params validation
            $usedParams = array_keys(iterator_to_array($params));
            if (array_diff($usedParams, $this->validParams)) {
                throw new \Exception(sprintf(
                    'Invalid param(s): "%s". Valid param(s): "%s"',
                    implode(',', $usedParams),
                    implode(',', $this->validParams)
                ));
            }
            $fromDate = $params->get('from_date')[0];
        }
        $courses = $command->commandCourses()->orderBy('from_date');
        if (!empty($fromDate)) {
            $courses = $courses->where('from_date', '>=', $fromDate);
        }
        $courses = $courses->get();
        return $this->collection($courses, new CommandCourseTransformer());
    }

    /**
     * Include command housing
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeCommandHousing(Command $command)
    {
        $housing = $command->commandHousing()->orderBy('from_date')->get();
        return $this->collection($housing, new CommandHousingTransformer());
    }

    /**
     * Include command payments
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includePayments(Command $command)
    {
        $payments = $command->payments;
        return $this->collection($payments, new PaymentTransformer());
    }

    /**
     * Include pickup
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includePickup(Command $command)
    {
        $pickup = $command->pickup;
        if ($pickup === null) {
            return $this->null();
        }
        return $this->item($pickup, new PickupTransformer());
    }

    /**
     * Include client
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeClient(Command $command)
    {
        $client = $command->client;
        if ($client === null) {
            return $this->null();
        }
        return $this->item($client, new ClientTransformer());
    }

    /**
     * Include agency
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeAgency(Command $command)
    {
        $agency = $command->agency;
        if ($agency === null) {
            return $this->null();
        }
        return $this->item($agency, new AgencyTransformer());
    }

    /**
     * Include travel information
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeTravelInformation(Command $command)
    {
        $travelInformation = $command->travelInformation()->orderBy('updated_at', 'desc')->get();
        if ($travelInformation->isEmpty()) {
            return $this->null();
        }
        return $this->item($travelInformation->first(), new TravelInformationTransformer());
    }

    /**
    * Include bills
    *
    * @return \League\Fractal\Resource\collection
    */
    public function includeBills(Command $command)
    {
        $bills = $command->bills;
        return $this->collection($bills, new BillTransformer());
    }

    /**
    * Include extra purchases
    *
    * @return \League\Fractal\Resource\collection
    */
    public function includeExtraPurchases(Command $command)
    {
        $extraPurchases = $command->extraPurchases;
        return $this->collection($extraPurchases, new PurchaseTransformer());
    }

    /**
     * Include purchase
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includePurchase(Command $command)
    {
        $purchase = $command->purchase;
        if ($purchase === null) {
            return $this->null();
        }
        return $this->item($purchase, new PurchasableTransformer());
    }
}
