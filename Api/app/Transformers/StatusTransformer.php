<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Status;

class StatusTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];
    
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];
    
    private $validParams = [];
    
    public function transform(Status $status)
    {
        $ret = [
            'id' => $status->id,
            'slug' => $status->slug,
            'name' => $status->name,
        ];

        return $ret;
    }
}