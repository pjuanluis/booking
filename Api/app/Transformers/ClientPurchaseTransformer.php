<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Client;

class ClientPurchaseTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'purchases',
    ];
    
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'purchases',
    ];
    
    private $validParams = [];
    
    public function transform(Client $client)
    {
        $ret = [
            'id' => $client->id,
            'name' => $client->getFullName()
        ];

        return $ret;
    }

     /**
     * Include purchasables
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includePurchases(Client $client)
    {
        $purchases = $client->purchases;
        
        foreach ($purchases as $k =>$p) { // TODO: improve if is posible, this filter if has a realted resource
                                                                  // with deleted_at != null
            $resource = $p->resource($p->purchasable_type)
                ->select('id')
                ->get();
            if ($resource->count() == 0) {
                unset($purchases[$k]);
            }
        }
        
        return $this->collection($purchases, new PurchasableTransformer());
    }
}