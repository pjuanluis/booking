<?php

namespace App\Transformers;

use App\HousingRate;
use League\Fractal\TransformerAbstract;

class HousingRateTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];
    
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];
    
    public function transform(HousingRate $housingRate)
    {
        $data = [
            'id' => (int) $housingRate->id,
            'from_date' => $housingRate->from_date,
            'to_date' => $housingRate->to_date,
            'quantity' => $housingRate->quantity,
            'unit' => $housingRate->unit,
            'unit_cost' => $housingRate->unit_cost,
            'is_active' => $housingRate->is_active,
            'created_at' => $housingRate->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $housingRate->updated_at->format('Y-m-d H:i:s'),
        ];
        return $data;
    }
}