<?php

namespace App\Transformers;

use App\Payment;
use League\Fractal\TransformerAbstract;

class PaymentTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = ['currency', 'paymentMethod'];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    public function transform(Payment $payment)
    {
        $data = [
            'id' => (int) $payment->id,
            'concept' => $payment->concept,
            'amount' => $payment->amount,
            'paid_at' => $payment->paid_at,
            'vendor' => $payment->vendor,
            'reference_id' => $payment->reference_id,
            'type' => $payment->type,
            'invoiced_at' => $payment->invoiced_at,
            'exchange_rates' => $payment->exchange_rates,
            'created_at' => $payment->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $payment->updated_at->format('Y-m-d H:i:s'),
        ];
        return $data;
    }

    /**
     * Include currency
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeCurrency(Payment $payment)
    {
        $currency = $payment->currency;
        if ($currency === null) {
            return $this->null();
        }
        return $this->item($currency, new CurrencyTransformer());
    }

    /**
     * Include payment method
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includePaymentMethod(Payment $payment)
    {
        $paymentMethod = $payment->paymentMethod;
        if ($paymentMethod === null) {
            return $this->null();
        }
        return $this->item($paymentMethod, new MethodPaymentTransformer());
    }
}
