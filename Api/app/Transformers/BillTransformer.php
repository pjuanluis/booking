<?php

namespace App\Transformers;

use App\Bill;
use League\Fractal\TransformerAbstract;


class BillTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = ['currency', 'command'];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = ['currency'];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Bill $bill)
    {
        $data = [
            'id' => (int)$bill->id,
            'reference_number' => $bill->reference_number,
            'amount' => (float)$bill->amount,
            'created_at' => $bill->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $bill->updated_at->format('Y-m-d H:i:s'),
        ];
        return $data;
    }

    /**
     * Include currency
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeCurrency(Bill $bill)
    {
        $currency = $bill->currency;
        if ($currency === null) {
            return $this->null();
        }
        return $this->item($currency, new CurrencyTransformer());
    }

    /**
     * Include command
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeCommand(Bill $bill)
    {
        $command = $bill->command;
        if ($command === null) {
            return $this->null();
        }
        return $this->item($command, new CommandTransformer());
    }
}
