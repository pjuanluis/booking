<?php

namespace App\Transformers;

use App\BlockedRoom;
use League\Fractal\TransformerAbstract;


class BlockedRoomTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(BlockedRoom $blockedRoom)
    {
        $data = [
            'id' => (int) $blockedRoom->id,
            'room_id' => $blockedRoom->room_id,
            'from_at' => $blockedRoom->from_at->format('Y-m-d H:i:s'),
            'to_at' => $blockedRoom->to_at->format('Y-m-d H:i:s'),
            'created_at' => $blockedRoom->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $blockedRoom->updated_at->format('Y-m-d H:i:s'),
        ];
        return $data;
    }
}