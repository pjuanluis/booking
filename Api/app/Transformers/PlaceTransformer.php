<?php

namespace App\Transformers;

use App\Place;
use League\Fractal\TransformerAbstract;

class PlaceTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];
    
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];
    
    public function transform(Place $place)
    {
        $data = [
            'id' => (int) $place->id,
            'name' => $place->name,
            'address' => $place->address,
            'latitude' => $place->latitude,
            'longitude' => $place->longitude,
            'created_at' => $place->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $place->updated_at->format('Y-m-d H:i:s'),
        ];
        return $data;
    }
}