<?php

namespace App\Transformers;

use App\Import;
use League\Fractal\TransformerAbstract;

class ImportTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];
    
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];
    
    public function transform(Import $import)
    {
        $data = [
            'id' => (int) $import->id,
            'filename' => $import->filename,
            'storage_path' => $import->storage_path,
            'description' => $import->description,
            'details' => $import->details,
            'status' => $import->status,
            'percent_completed' => $import->percent_completed,
            'created_at' => $import->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $import->updated_at->format('Y-m-d H:i:s'),
            'finished_at' => !empty($import->finished_at) ? $import->finished_at->format('Y-m-d H:i:s') : null,
        ];
        return $data;
    }
}