<?php

namespace App\Transformers;

use App\Client;
use League\Fractal\ParamBag;
use League\Fractal\TransformerAbstract;
use DB;

class ClientTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'country', 'file', 'commands', 'scheduleDates', 'purchases', 'gender'
    ];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    private $validParams = ['order_by'];

    public function transform(Client $client)
    {
        $data = [
            'id' => (int) $client->id,
            'first_name' => $client->first_name,
            'last_name' => $client->last_name,
            'full_name' => $client->first_name . ' ' . $client->last_name,
            'email' => $client->email,
            'birth_date' => $client->birth_date,
            'phone' => $client->phone,
            'emergency_phone' => $client->emergency_phone,
            'has_alergies' => $client->has_alergies,
            'alergies' => $client->alergies,
            'has_diseases' => $client->has_diseases,
            'diseases' => $client->diseases,
            'is_smoker' => $client->is_smoker,
            'fb_profile' => $client->fb_profile,
            'country_id' => $client->country_id,
            'country' => $client->country ? $client->country->name : '',
            'is_active' => $client->is_active,
            'created_at' => $client->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $client->updated_at->format('Y-m-d H:i:s'),
        ];
        $result = DB::table('client_academic_information')->where('client_id', $client->id)->first();
        if (!empty($result)) {
            $data['spanish_level'] = $result->spanish_level;
            $data['spanish_knowledge_duration'] = $result->spanish_knowledge_duration;
            $data['has_surf_experience'] = $result->has_surf_experience;
            $data['surf_experience_duration'] = $result->surf_experience_duration;
            $data['has_volunteer_experience'] = $result->has_volunteer_experience;
            $data['volunteer_experience_duration'] = $result->volunteer_experience_duration;
            $data['volunteer_experience'] = $result->volunteer_experience;
        }
        return $data;
    }

    /**
     * Include commands
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeCommands(Client $client)
    {
        $commands = $client->commands;
        return $this->collection($commands, new CommandTransformer());
    }

    /**
     * Include schedules
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeScheduleDates(Client $client, ParamBag $params = null)
    {
        $orderBy = [];
        if ($params !== null) {
            // Optional params validation
            $usedParams = array_keys(iterator_to_array($params));
            if (array_diff($usedParams, $this->validParams)) {
                throw new \Exception(sprintf(
                    'Invalid param(s): "%s". Valid param(s): "%s"',
                    implode(',', $usedParams),
                    implode(',', $this->validParams)
                ));
            }
            $orderBy = $params->get('order_by');
        }
        $schedules = $client->allScheduleDates($orderBy);
        return $this->collection($schedules, new ScheduleDateTransformer());
    }

    /**
     * Include country
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeCountry(Client $client)
    {
        $country = $client->country;
        if ($country === null) {
            return $this->null();
        }
        return $this->item($country, new CountryTransformer());
    }

    /**
     * Include file
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeFile(Client $client)
    {
        $file = $client->file;
        if ($file === null) {
            return $this->null();
        }
        return $this->item($file, new FileTransformer());
    }

    /**
     * Include purchases
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includePurchases(Client $client)
    {
        $purchases = $client->purchases;
        return $this->collection($purchases, new PurchaseTransformer());
    }

    /**
     * Include gender
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeGender(Client $client)
    {
        $gender = $client->gender;
        if ($gender === null) {
            return $this->null();
        }
        return $this->item($gender, new GenderTransformer());
    }
}
