<?php

namespace App\Http\Controllers;

use Auth;
use Artisan;
use Validator;
use App\Import;
use App\Traits\FileAction;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Transformers\ImportTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ImportController extends Controller
{
    use Helpers, FileAction;

    protected $validationRules = [
        'file' => 'array|required',
        'file.name' => 'required',
        'file.contents' => 'required',
    ];

    public function __construct(Request $request)
    {
        $this->setPermissionAndModule($request, 'import_bookings');
    }

    /**
     * Return a listing of courses.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, ImportTransformer $importTransformer)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        // Obtener cursos
        $result = Import::orderBy('created_at', 'desc')
                ->get();
        return $this->response->collection($result, $importTransformer);
    }

    /**
     * Validate the importation data
     * @param \Illuminate\Http\Request $request The data send by the user
     * @param array $validationRules Importation validation rules
     * @throws StoreResourceFailedException|UpdateResourceFailedException
     */
    private function validateImport(Request $request, $filePath, $validationRules)
    {
        $validator = Validator::make($request->all(), $validationRules);
        if ($filePath !== null) {
            $filePath = $this->getStoragePublicDir() . $filePath;
        }
        $validator->after(function ($validator) use($filePath) {
            if ($filePath !== null) {
                $mime = mime_content_type($filePath);
                // Validar que el archivo sea una excel
                if (!in_array($mime, ['application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/xls'])) {
                    $description = trans('validation.custom.file.excel');
                    $validator->errors()->add("file", $description);
                }
            }
        });
        $pronoun = trans_choice('models.pronouns.import', 1);
        // Si falla la validación
        if ($validator->fails()) {
            if ($filePath !== null) {
                // Borrar archivo
                unlink($filePath);
            }
            // Responder con los errores
            if ($request->isMethod('post')) {
                $description = trans('models.responses.not_created', ['model' => $pronoun]);
                throw new StoreResourceFailedException($description, $validator->errors());
            }
            else {
                $description = trans('models.responses.not_updated', ['model' => $pronoun]);
                throw new UpdateResourceFailedException($description, $validator->errors());
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $filePath = null;
        if ($request->has('file')) {
            $filePath = $this->uploadFile($request->get('file'), 'imports');
        }
        $this->validateImport($request, $filePath, $this->validationRules);
        $file = $request->get('file');
        $import = Import::create([
            'filename' => $file['name'],
            'storage_path' => storage_path('app/public') . '/' . $filePath,
            'status' => 'initialized',
            'description' => $request->get('description'),
            'details' => "Initializing importation",
            'percent_completed' => 0
        ]);
        Artisan::queue('import:bookings', [
            '--id' => $import->id,
        ])->onConnection('beanstalkd');
        //shell_exec('php ' . base_path('artisan') .' import:bookings --id=' . $import->id . ' > /dev/null 2>&1 &');
        $pronoun = trans_choice('models.pronouns.import', 1);
        $description = trans('models.responses.created', ['model' => $pronoun]);
        $response = ['message' => $description, 'import' => $import, 'status_code' => 201];
        return $this->responseOk($response);
    }

    /**
     * Get the specified course.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, ImportTransformer $importTransformer)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $import = null;
        $pronoun = trans_choice('models.pronouns.import', 1);
        try {
            $import = Import::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $description = trans('models.responses.not_found', ['model' => $pronoun]);
            throw new NotFoundHttpException($description);
        }
        return $this->response->item($import, $importTransformer);
    }
}
