<?php

namespace App\Http\Controllers;

use Auth;
use App\Place;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Mail;
use App\Transformers\PlaceTransformer;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class PlaceController extends Controller
{
    use Helpers;

    protected $validationRules = [
        'name' => 'required|string|max:255',
        'address' => 'nullable|string|max:255',
        'latitude' => 'nullable|string|max:255',
        'longitude' => 'nullable|string|max:255', ];

    public function __construct(Request $request)
    {
        $this->setPermissionAndModule($request, 'places');
    }

    /**
     * Display a listing of places.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, PlaceTransformer $placeTransformer)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        // Verificar bandera para no paginar los resultados
        $paginate = true;
        if ($request->get('no-paginate', '0') == 1) {
            $paginate = false;
        }
        // Ordenar por nombre
        $result = Place::orderBy('name', 'asc');
        // Filtrar por nombre
        if (!empty($request->get('name'))) {
            $result = $result->where('name', 'ilike', $request->get('name') . '%');
        }
        // Paginar registros
        if ($paginate) {
            $result = $result->paginate(20);
            return $this->response->paginator($result, $placeTransformer);
        }
        // No paginar, retornar todos los registros encontrados
        else {
            $result = $result->get();
            return $this->response->collection($result, $placeTransformer);
        }
    }

    /**
     * Validate the place
     * @param \Illuminate\Http\Request $request The data send by the user
     * @param array $validationRules Place validation rules
     * @throws StoreResourceFailedException|UpdateResourceFailedException
     */
    private function validatePlace(Request $request, $validationRules)
    {
        $validator = Validator::make($request->all(), $validationRules);
        $pronoun = trans_choice('models.pronouns.place', 1);
        // Si falla la validación
        if ($validator->fails()) {
            // Responder con los errores
            if ($request->isMethod('post')) {
                $description = trans('models.responses.not_created', ['model' => $pronoun]);
                throw new StoreResourceFailedException($description, $validator->errors());
            }
            else {
                $description = trans('models.responses.not_updated', ['model' => $pronoun]);
                throw new UpdateResourceFailedException($description, $validator->errors());
            }
        }
    }
    /**
     * Find the place by a given id
     * @param type $id The place id
     * @return \App\Place
     * @throws NotFoundHttpException
     */
    private function getPlace($id)
    {
        $place = null;
        $pronoun = trans_choice('models.pronouns.place', 1);
        try {
            $place = Place::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $description = trans('models.responses.not_found', ['model' => $pronoun]);
            throw new NotFoundHttpException($description);
        }
        return $place;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $this->validatePlace($request, $this->validationRules);

        // Crear la persona
        Place::create($request->all());

        $pronoun = trans_choice('models.pronouns.place', 1);
        $description = trans('models.responses.created', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 201];
        return $this->response()->created(null, $response);
    }

    /**
     * Get the specified place.
     *
     * @param  int  $id The place id
     * @return \Illuminate\Http\Response
     */
    public function show($id, PlaceTransformer $placeTransformer)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $place = $this->getPlace($id);
        return $this->response->item($place, $placeTransformer);
    }

    public function update(Request $request, $id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $place = $this->getPlace($id);

        $pronoun = trans_choice('models.pronouns.place', 1);

        $this->validationRules['updated_at'] = 'required|date|date_format:Y-m-d H:i:s';

        $this->validatePlace($request, $this->validationRules);

        // Checar que la persona no haya sido modificado previamente
        if ($place->updated_at->greaterThan(new Carbon($request->get('updated_at')))) {
            $description = trans('models.responses.conflict', ['model' => $pronoun]);
            throw new ConflictHttpException($description);
        }

        // Actualizar datos de people
        $place->update($request->all());

        $description = trans('models.responses.updated', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);
        
        $place = $this->getPlace($id);
        $pronoun = trans_choice('models.pronouns.place', 1);
        // Checar si hay referencias activas al package a borrar
        $referencedTables = $place->activeReferencedTables('id');
        $related = null;
        if (count($referencedTables) > 0) {
            $related = \App\BaseModel::parseTableToModelName($referencedTables[0]);
            $related = $related === null ? studly_case($referencedTables[0]) :
                    trans_choice('models.pronouns.' . strtolower($related), 2);
            $description = trans('models.responses.not_deleted', [
                'model' => $pronoun,
                'related' => $related,
            ]);
            throw new DeleteResourceFailedException($description);
        }

        // Eliminar person
        $place->delete();

        $description = trans('models.responses.deleted', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }
}
