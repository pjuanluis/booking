<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Role;
use Validator;
use App\Client;
use App\TravelInformation;
use Carbon\Carbon;
use App\Mail\UserCreated;
use App\Traits\FileAction;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use Illuminate\Validation\Rule;
use App\Mail\ClientInfoRegistered;
use Illuminate\Support\Facades\Mail;
use App\Transformers\ClientTransformer;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ClientController extends Controller
{
    use Helpers, FileAction;

    protected $validationRules = [
        'first_name' => 'required',
        'last_name' => 'required',
        'email' => 'required|email|unique:users,email',
        'country_id' => 'required|exists:countries,id',
        'file' => 'nullable|array',
        'file.name' => 'required_with:file',
        'file.contents' => 'required_with:file',
        'birth_date' => 'nullable|date_format:Y-m-d',
        'has_alergies' => 'required|boolean',
        'has_diseases' => 'required|boolean',
        'is_smoker' => 'required|boolean',
        'fb_profile' => 'nullable|url',
        'password' => 'nullable|min:8',
        'gender_id' => 'required|integer|exists:genders,id',
    ];

    private $requestHasAcademicInformation = false;
    private $requestHasTravelInformation = false;

    public function __construct(Request $request)
    {
        $this->setPermissionAndModule($request, 'clients');
    }

    /**
     * Validate the client data
     * @param \Illuminate\Http\Request $request The data send by the user
     * @param array $validationRules Client validation rules
     * @throws StoreResourceFailedException|UpdateResourceFailedException
     */
    private function validateClient(Request $request, $filePath, $validationRules)
    {
        $validator = Validator::make($request->all(), $validationRules);
        if ($filePath !== null) {
            $filePath = $this->getStoragePublicDir() . $filePath;
        }
        $validator->after(function ($validator) use($filePath) {
            if ($filePath !== null) {
                $mime = mime_content_type($filePath);
                // Validar que el archivo sea una imagen
                if (!in_array($mime, ['image/png', 'image/jpeg', 'image/gif'])) {
                    $description = trans('validation.custom.file.image');
                    $validator->errors()->add("file", $description);
                }
            }
        });
        $pronoun = trans_choice('models.pronouns.client', 1);
        // Si falla la validación
        if ($validator->fails()) {
            if ($filePath !== null) {
                // Borrar archivo
                unlink($filePath);
            }
            // Responder con los errores
            if ($request->isMethod('post')) {
                $description = trans('models.responses.not_created', ['model' => $pronoun]);
                throw new StoreResourceFailedException($description, $validator->errors());
            }
            else {
                $description = trans('models.responses.not_updated', ['model' => $pronoun]);
                throw new UpdateResourceFailedException($description, $validator->errors());
            }
        }
    }

    /**
     * Find the client by a given id
     * @param type $id The client id
     * @return \App\Client
     * @throws NotFoundHttpException
     */
    private function getClient($id)
    {
        $client = null;
        $pronoun = trans_choice('models.pronouns.client', 1);
        try {
            $client = Client::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $description = trans('models.responses.not_found', ['model' => $pronoun]);
            throw new NotFoundHttpException($description);
        }
        return $client;
    }

    /**
     * Return a listing of clients.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, ClientTransformer $clientTransformer)
    {
        $user = Auth::user();
        // Checar permiso
        $this->checkPermission($user, $this->module, $this->permission);

        // Verificar bandera para no paginar los resultados
        $paginate = true;
        $tmp = null;
        if ($request->get('no-paginate', '0') == 1) {
            $paginate = false;
        }
        // Ordenar por fecha de creación
        $result = Client::orderBy('created_at', 'desc');

        // Si el usuario logueado es un cliente, solo puede ver su información
        if ($user->authenticatable_type === "App\Client") {
            $result = $result->where('id', $user->authenticatable_id);
        } elseif ($user->authenticatable_type === "App\Agency") {
            // Si el usuario logueado es una agencia, solo puede ver sus clientes
            $result = $result->whereHas('commands', function($query) use($user) {
                $query->where('agency_id', $user->authenticatable_id);
            });
        }

        // Filtrar por nombre
        if (!empty($request->get('name'))) {
            $result = $result->where(DB::raw("(first_name || ' ' || last_name)"), 'ilike','%' . $request->get('name') . '%');
        }
        // Filtrar por email
        if (!empty($request->get('email'))) {
            $result = $result->where('email', 'ilike', $request->get('email') . '%');
        }
        if (($request->has('is_active'))) {
            $result = $result->where('is_active', $request->get('is_active'));
        }
        // Filtrar por experiencias adquiridas por el cliente
        if (!empty($request->get('experiences'))) {
            $tmp = explode(',', $request->get('experiences'));
            $result = $result->whereHas('commands.commandCourses.package.experiences', function($query) use($tmp) {
                $query->whereIn('slug', $tmp);
            });
        }
        // Filtrar por fecha de inicio de cursos
        if (!empty($request->get('course_from'))) {
            $tmp = $request->get('course_from');
            $result = $result->has('commands.commandCourses', '>=', 1);
            $result = $result->whereHas('commands', function($query) use($tmp) {
                $query->where('from_date', '>=', $tmp);
            });
        }
        // Filtrar por país
        if (!empty($request->get('country_id'))) {
            $result = $result->where('country_id', $request->get('country_id'));
        }
        // Filtrar por agencia
        if (!empty($request->get('agency_id'))) {
            $result = $result->whereHas('commands', function($query) use($request) {
                $query->where('agency_id', $request->get('agency_id'));
            });
        }
        // Deshabilitar el eager loading
        app('api.transformer')->getAdapter()->disableEagerLoading();
        // Paginar registros
        if ($paginate) {
            $result = $result->paginate(20);
            return $this->response->paginator($result, $clientTransformer);
        }
        // No paginar, retornar todos los registros encontrados
        else {
            $result = $result->get();
            return $this->response->collection($result, $clientTransformer);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->createCustomRules($request);

        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $command = null;
        if ($request->has('token')) {
            // Vincular cliente a la comanda
            $pronoun = trans_choice('models.pronouns.booking', 1);
            try {
                $command = \App\Command::where('token', $request->get('token'))->firstOrFail();
            } catch (ModelNotFoundException $e) {
                $description = trans('models.responses.not_found', ['model' => $pronoun]);
                throw new NotFoundHttpException($description);
            }
        }
        $filePath = null;
        if ($request->has('file')) {
            $filePath = $this->uploadFile($request->get('file'), 'clients');
        }
        $this->validateClient($request, $filePath, $this->validationRules);
        $client = Client::create([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'email' => $request->get('email'),
            'country_id' => $request->get('country_id'),
            'birth_date' => empty($request->get('birth_date')) ? null : $request->get('birth_date'),
            'phone' => $request->get('phone'),
            'emergency_phone' => $request->get('emergency_phone'),
            'has_alergies' => $request->get('has_alergies'),
            'alergies' => $request->get('alergies'),
            'has_diseases' => $request->get('has_diseases'),
            'diseases' => $request->get('diseases'),
            'is_smoker' => $request->get('is_smoker'),
            'fb_profile' => $request->get('fb_profile'),
            'gender_id' => $request->get('gender_id'),
        ]);

        if ($request->has('password')) {
            // Create user account
            $userData = $this->createUserData($request);
            $role = Role::where('slug', 'client')->first();
            $userData['role_id'] = $role->id;
            $client->user()->create($userData);
        }

        $arrivalAt = null;
        if (!empty($request->get('arrival_date'))) {
            $arrivalAt = $request->get('arrival_date') . ' ' . $request->get('arrival_time') . ':00';
        }
        $departureAt = null;
        if (!empty($request->get('departure_date'))) {
            $departureAt = $request->get('departure_date') . ' ' . $request->get('departure_time') . ':00';
        }
        // Guardar datos académicos
        DB::table('client_academic_information')->insert([
            'client_id' => $client->id,
            'spanish_level' => $request->get('spanish_level'),
            'spanish_knowledge_duration' => $request->get('spanish_knowledge_duration'),
            'has_surf_experience' => $request->get('has_surf_experience'),
            'surf_experience_duration' => $request->get('surf_experience_duration'),
            'has_volunteer_experience' => $request->get('has_volunteer_experience'),
            'volunteer_experience_duration' => $request->get('volunteer_experience_duration'),
            'volunteer_experience' => $request->get('volunteer_experience'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        // Agregar archivo
        if ($request->has('file')) {
            $file = $request->get('file');
            $client->file()->create([
                'name' => $file['name'],
                'mime' => mime_content_type($this->getStoragePublicDir() . $filePath),
                'size' => filesize($this->getStoragePublicDir() . $filePath),
                'storage_path' => $filePath,
                'description' => array_key_exists('description', $file) ? $file['description'] : null
            ]);
        }
        if (!empty($command)) {
            $command->update(['client_id' => $client->id]);
            $role = Role::where('slug', 'administrator')->first();
            // Enviar email de confirmación de registro de datos del cliente
            $from = [];
            $client->name = $client->getFullName();
            // Enviar correo al cliente
            Mail::to($client)->send(new ClientInfoRegistered($client, $command, true, $from));
            // Enviar correo al admin
            $admins = DB::table('users')
                    ->select('email', DB::raw("first_name || ' ' || last_name as name"))
                    ->where('role_id', $role->id)
                    ->whereNull('deleted_at')
                    ->get();
            Mail::to($admins)->send(new ClientInfoRegistered($client, $command, false, $from));
        }
        $pronoun = trans_choice('models.pronouns.client', 1);
        $description = trans('models.responses.created', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 201];
        return $this->responseOk($response);
    }

    /**
     * Store a newly created resource in storage, when a user is not logged in.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storePublic(Request $request)
    {
        $this->createCustomRules($request);

        $command = null;
        if ($request->has('token')) {
            // Vincular cliente a la comanda
            $pronoun = trans_choice('models.pronouns.booking', 1);
            try {
                $command = \App\Command::where('token', $request->get('token'))->firstOrFail();
            } catch (ModelNotFoundException $e) {
                $description = trans('models.responses.not_found', ['model' => $pronoun]);
                throw new NotFoundHttpException($description);
            }
        }
        $filePath = null;
        if ($request->has('file')) {
            $filePath = $this->uploadFile($request->get('file'), 'clients');
        }
        $this->validateClient($request, $filePath, $this->validationRules);
        $client = Client::create([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'email' => $request->get('email'),
            'country_id' => $request->get('country_id'),
            'birth_date' => empty($request->get('birth_date')) ? null : $request->get('birth_date'),
            'phone' => $request->get('phone'),
            'emergency_phone' => $request->get('emergency_phone'),
            'has_alergies' => $request->get('has_alergies'),
            'alergies' => $request->get('alergies'),
            'has_diseases' => $request->get('has_diseases'),
            'diseases' => $request->get('diseases'),
            'is_smoker' => $request->get('is_smoker'),
            'fb_profile' => $request->get('fb_profile'),
            'gender_id' => $request->get('gender_id'),
        ]);
        // Create user account
        $userData = $this->createUserData($request);
        $role = Role::where('slug', 'client')->first();
        $userData['role_id'] = $role->id;
        $client->user()->create($userData);
        $arrivalAt = null;
        if (!empty($request->get('arrival_date'))) {
            $arrivalAt = $request->get('arrival_date') . ' ' . $request->get('arrival_time') . ':00';
        }
        $departureAt = null;
        if (!empty($request->get('departure_date'))) {
            $departureAt = $request->get('departure_date') . ' ' . $request->get('departure_time') . ':00';
        }

        if ($this->requestHasTravelInformation) {
            // Guardar datos del viaje
            DB::table('travel_information')->insert([
                'command_id' => $command->id,
                'travel_by' => $request->get('travel_by'),
                'arrival_place' => $request->get('arrival_place'),
                'arrival_carrier' => $request->get('arrival_carrier'),
                'arrival_at' => $arrivalAt,
                'arrival_travel_number' => $request->get('arrival_travel_number'),
                'departure_place' => $request->get('departure_place'),
                'departure_carrier' => $request->get('departure_carrier'),
                'departure_at' => $departureAt,
                'departure_travel_number' => $request->get('departure_travel_number'),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
        // Guardar datos académicos
        DB::table('client_academic_information')->insert([
            'client_id' => $client->id,
            'spanish_level' => $request->get('spanish_level'),
            'spanish_knowledge_duration' => $request->get('spanish_knowledge_duration'),
            'has_surf_experience' => $request->get('has_surf_experience'),
            'surf_experience_duration' => $request->get('surf_experience_duration'),
            'has_volunteer_experience' => $request->get('has_volunteer_experience'),
            'volunteer_experience_duration' => $request->get('volunteer_experience_duration'),
            'volunteer_experience' => $request->get('volunteer_experience'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        // Agregar archivo
        if ($request->has('file')) {
            $file = $request->get('file');
            $client->file()->create([
                'name' => $file['name'],
                'mime' => mime_content_type($this->getStoragePublicDir() . $filePath),
                'size' => filesize($this->getStoragePublicDir() . $filePath),
                'storage_path' => $filePath,
                'description' => array_key_exists('description', $file) ? $file['description'] : null
            ]);
        }
        if (!empty($command)) {
            $command->update(['client_id' => $client->id]);
            $role = Role::where('slug', 'administrator')->first();
            // Enviar email de confirmación de registro de datos del cliente
            $from = [];
            $client->name = $client->getFullName();
            // Enviar correo al cliente
            Mail::to($client)->send(new ClientInfoRegistered($client, $command, true, $from));
            // Enviar correo al admin
            $admins = DB::table('users')
                    ->select('email', DB::raw("first_name || ' ' || last_name as name"))
                    ->where('role_id', $role->id)
                    ->whereNull('deleted_at')
                    ->get();
            Mail::to($admins)->send(new ClientInfoRegistered($client, $command, false, $from));
        }
        $pronoun = trans_choice('models.pronouns.client', 1);
        $description = trans('models.responses.created', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 201];
        return $this->responseOk($response);
    }

    /**
     * Get the specified client.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, ClientTransformer $clientTransformer)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $client = null;
        $pronoun = trans_choice('models.pronouns.client', 1);
        try {
            $client = Client::findOrFail($id);
            // Si el usuario logueado es una cliente, solo puede ver su información
            if ($user->authenticatable_type === "App\Client") {
                if ($client->id != $user->authenticatable_id) {
                    throw new AccessDeniedHttpException(trans('models.responses.not_allowed', [
                        'model' => $pronoun
                    ]));
                }
            } elseif ($user->authenticatable_type === "App\Agency") {
                // Si el usuario logueado es una agencia, solo puede ver sus clientes
                $belongToThisAgency = $client->commands()
                    ->where('agency_id', $user->authenticatable_id)
                    ->exists();
                if (!$belongToThisAgency) {
                    throw new AccessDeniedHttpException(trans('models.responses.not_allowed', [
                        'model' => $pronoun
                    ]));
                }
            }
        } catch (ModelNotFoundException $e) {
            $description = trans('models.responses.not_found', ['model' => $pronoun]);
            throw new NotFoundHttpException($description);
        }
        return $this->response->item($client, $clientTransformer, [], function($resource, $fractal) {
            $include = '';
            if (isset($_GET['include'])) {
                $include = $_GET['include'];
            }
            $fractal->parseIncludes($include);
        });
    }

    private function createUserData(Request $request)
    {
        $ret = [
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'email' => $request->get('email'),
        ];
        if (!empty($request->get('password'))) {
            $ret['password'] = bcrypt($request->get('password'));
        }

        return $ret;
    }

    /**
     * Has In Request
     *
     * @param Request $request
     * @param mixed array
     * @return bool
     */
    private function hasInRequest(Request $request, array $columns = [])
    {
        $has = false;
        foreach ($columns as $k => $v) {
            $has = $request->get($v) !== null;
            if ($has) {
                break;
            }
        }
        return $has;
    }

    /**
     * Search in request if has a data of academic information, and save in
     * $this->requestHasAcademicInformation true or false.
     *
     * @param Request $request
     * @return void
     */
    private function hasRequestAcademicInformation(Request $request)
    {
        $columns = [
            'spanish_level',
            'spanish_knowledge_duration',
            'has_surf_experience',
            'surf_experience_duration',
            'has_volunteer_experience',
            'volunteer_experience_duration',
            'volunteer_experience',
        ];

        $this->requestHasAcademicInformation = $this->hasInRequest($request, $columns);
    }

    /**
     * Is clear Array TravelInformation. travel_information.*.
     *
     * @param Request $request
     * @param String $asArrayName
     * @return void
     */
    private function isClearArrayTravelInformation(Request $request, String $asArrayName)
    {
        $ret = true;
        foreach ($request->get($asArrayName) as $tiK => $tiV) {
            foreach ($tiV as $k => $v) {
                $ret = $v !== null;

                if ($ret) {
                    break; // I  don't like to use break 2
                }
            }

            if ($ret) { // This condition is because
                break; // I  don't like to use break 2
            }
        }

        return $ret;
    }

    /**
     * Search in request if has a data of academic information, and save in
     * $this->requestHasTravelInformation true or false.
     *
     * @param Request $request
     * @return void
     */
    private function hasRequestTravelInformation(Request $request)
    {
        $columns = (new TravelInformation)->getFillable();

        $asArrayName = 'travel_information';
        if ($request->get($asArrayName) === null) {
            $this->requestHasTravelInformation = $this->hasInRequest($request, $columns);
        } else {
            $this->requestHasTravelInformation = $this->isClearArrayTravelInformation($request, $asArrayName);
        }
    }

    /**
     * Add rules to $this->validationRules
     */
    private function createRulesAcademicInformation()
    {
        $this->validationRules = array_merge($this->validationRules, [
            'spanish_level' => [
                'required',
                Rule::in(['none', 'begginer', 'middle', 'advanced'])
            ],
            'spanish_knowledge_duration' => 'required',
            'has_surf_experience' => 'required|boolean',
            'surf_experience_duration' => 'required',
            'has_volunteer_experience' => 'required|boolean',
            'volunteer_experience_duration' => 'required'
        ]);
    }

    /**
     * Add rules to $this->validationRule
     *
     *  @param $asArray bool - Default false, if true validate as travel_information.*.key
     * @return void
     */
    private function createRulesTravelInformation(bool $asArray = false)
    {
        if ($asArray) {
            $this->validationRules = array_merge($this->validationRules, [
                'travel_information.*.travel_by' => [ 'required', Rule::in(['plane', 'bus']) ],
                'travel_information.*.arrival_place' => 'required_without:travel_information.*.departure_place|required_with:travel_information.*.arrival_carrier,travel_information.*.arrival_date,travel_information.*.arrival_time,travel_information.*.arrival_travel_number',
                'travel_information.*.arrival_date' => 'nullable|required_with:travel_information.*.arrival_time|date_format:Y-m-d',
                'travel_information.*.arrival_time' => 'nullable|required_with:travel_information.*.arrival_date|date_format:H:i',
                'travel_information.*.arrival_carrier' => 'nullable',
                'travel_information.*.arrival_travel_number' => 'nullable',
                'travel_information.*.departure_place' => 'required_without:travel_information.*.arrival_place|required_with:travel_information.*.departure_carrier,travel_information.*.departure_date,travel_information.*.departure_time,travel_information.*.departure_travel_number',
                'travel_information.*.departure_date' => 'nullable|required_with:travel_information.*.departure_time|date_format:Y-m-d',
                'travel_information.*.departure_time' => 'nullable|required_with:travel_information.*.departure_date|date_format:H:i',
                'travel_information.*.departure_carrier' => 'nullable',
                'travel_information.*.departure_travel_number' => 'nullable',
            ]);
        } else {
            $this->validationRules = array_merge($this->validationRules, [
                'travel_by' => [ 'required', Rule::in(['plane', 'bus']) ],
                'arrival_place' => 'required_without:departure_place|required_with:arrival_carrier,arrival_date,arrival_time,arrival_travel_number',
                'arrival_date' => 'nullable|required_with:arrival_time|date_format:Y-m-d',
                'arrival_time' => 'nullable|required_with:arrival_date|date_format:H:i',
                'departure_place' => 'required_without:arrival_place|required_with:departure_carrier,departure_date,departure_time,departure_travel_number',
                'departure_date' => 'nullable|required_with:departure_time|date_format:Y-m-d',
                'departure_time' => 'nullable|required_with:departure_date|date_format:H:i'
            ]);
        }
    }

    /**
     * Create rules for Academic information and Travel information.
     */
    private function createCustomRules(Request $request)
    {
        $routeActionMethod = strtolower(
            array_last(explode('@', $request->route()->getAction()['uses']))
        );

        if ($routeActionMethod === 'storepublic') {
            $this->createRulesAcademicInformation();

            $this->hasRequestTravelInformation($request);
            if ($this->requestHasTravelInformation) {
                $this->createRulesTravelInformation();
            }
        } else {
            $this->hasRequestAcademicInformation($request);
            if ($this->requestHasAcademicInformation) {
                $this->createRulesAcademicInformation();
            }

            $this->hasRequestTravelInformation($request);
            if ($this->requestHasTravelInformation) {
                $this->createRulesTravelInformation(true);
            }
        }
    }

    /**
     * Update the specified client in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id The client id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->createCustomRules($request);

        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $client = $this->getClient($id);
        $pronoun = trans_choice('models.pronouns.client', 1);

        // Si el usuario logueado es un cliente, solo puede editar sus datos
        if ($user->authenticatable_type === 'App\Client') {
            if ($client->id != $user->authenticatable_id) {
                throw new AccessDeniedHttpException(trans('models.responses.not_allowed', [
                    'model' => $pronoun
                ]));
            }
        } elseif ($user->authenticatable_type === "App\Agency") {
            // Si el usuario logueado es una agencia, solo puede editar sus clientes
            $belongToThisAgency = $client->commands()
                ->where('agency_id', $user->authenticatable_id)
                ->exists();
            if (!$belongToThisAgency) {
                throw new AccessDeniedHttpException(trans('models.responses.not_allowed', [
                    'model' => $pronoun
                ]));
            }
        }

        $user = $client->user;

        $this->validationRules['updated_at'] = 'required|date_format:Y-m-d H:i:s';
        $this->validationRules['file'] = 'array';
        $this->validationRules['file.name'] = '';
        $this->validationRules['file.contents'] = '';
        $filePath = null;
        if ($request->has('file')) {
            $filePath = $this->uploadFile($request->get('file'), 'clients');
        }

         // User validation
         $this->validationRules['email'] = [ 'required' ];
        if (!empty($user->id)) {
            array_push($this->validationRules['email'], "unique:users,email,{$user->id}");
        } else {
            array_push($this->validationRules['email'], "unique:users,email");
        }

        $this->validateClient($request, $filePath, $this->validationRules);
        // Checar que el cliente no haya sido modificado previamente
        if ($client->updated_at->greaterThan(new Carbon($request->get('updated_at')))) {
            if ($filePath !== null) {
                // Borrar archivo
                unlink($this->getStoragePublicDir() . $filePath);
            }
            $description = trans('models.responses.conflict', ['model' => $pronoun]);
            throw new ConflictHttpException($description);
        }
        // Actualizar datos del cliente
        $client->update([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'email' => $request->get('email'),
            'country_id' => $request->get('country_id'),
            'birth_date' => empty($request->get('birth_date')) ? null : $request->get('birth_date'),
            'phone' => $request->get('phone'),
            'emergency_phone' => $request->get('emergency_phone'),
            'has_alergies' => $request->get('has_alergies'),
            'alergies' => $request->get('alergies'),
            'has_diseases' => $request->get('has_diseases'),
            'diseases' => $request->get('diseases'),
            'is_smoker' => $request->get('is_smoker'),
            'fb_profile' => $request->get('fb_profile'),
            'gender_id' => $request->get('gender_id'),
        ]);

        if ($request->has('password')) {
            // Create or edit user account
            $userData = $this->createUserData($request);
            if (empty($user)) {
                $role = Role::where('slug', 'client')->first();
                $userData['role_id'] = $role->id;
                $client->user()->create($userData);
            } else {
                $client->user()->update($userData);
                $us = $client->user;
                if (empty($us->p)) {
                    $us->p = request()->get('password');
                }
                Mail::to($us)->send(new UserCreated($us, 'update'));
            }
        }

        if ($this->requestHasTravelInformation) {
            // Guardar datos del viaje
            foreach ($request->get('travel_information') as $k => $v) {
                $arrivalAt = null;
                if (!empty($v['arrival_date'])) {
                    $arrivalAt = $v['arrival_date'] . ' ' . $v['arrival_time'] . ':00';
                }
                $departureAt = null;
                if (!empty($v['departure_date'])) {
                    $departureAt = $v['departure_date'] . ' ' . $v['departure_time'] . ':00';
                }
                DB::table('travel_information')->where('id', $v['id'])
                    ->update([
                        'travel_by' => $v['travel_by'],
                        'arrival_place' => $v['arrival_place'],
                        'arrival_carrier' => $v['arrival_carrier'],
                        'arrival_at' => $arrivalAt,
                        'arrival_travel_number' => $v['arrival_travel_number'],
                        'departure_place' => $v['departure_place'],
                        'departure_carrier' => $v['departure_carrier'],
                        'departure_at' => $departureAt,
                        'departure_travel_number' => $v['departure_travel_number']
                    ]);
            }
        }

        if ($this->requestHasAcademicInformation) {
            // Guardar datos académicos
            DB::table('client_academic_information')->where('client_id', $client->id)->delete();
            DB::table('client_academic_information')->insert([
                'client_id' => $client->id,
                'spanish_level' => $request->get('spanish_level'),
                'spanish_knowledge_duration' => $request->get('spanish_knowledge_duration'),
                'has_surf_experience' => $request->get('has_surf_experience'),
                'surf_experience_duration' => $request->get('surf_experience_duration'),
                'has_volunteer_experience' => $request->get('has_volunteer_experience'),
                'volunteer_experience_duration' => $request->get('volunteer_experience_duration'),
                'volunteer_experience' => $request->get('volunteer_experience'),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }

        // Actualizar archivo
        if ($request->has('file')) {
            $file = $request->get('file');
            $oldFile = $client->file;
            // Si el cliente tiene un archivo actualmente, reemplazarlo
            if ($oldFile !== null) {
                // Borrar archivo guardado previamente para la validación
                unlink($this->getStoragePublicDir() . $filePath);
                // Reemplazar archivo del cliente
                $filePath = $this->replaceFile($oldFile, $file);
                // Actualizar datos del archivo del cliente
                $client->file->update([
                    'name' => $file['name'],
                    'mime' => mime_content_type($this->getStoragePublicDir() . $filePath),
                    'size' => filesize($this->getStoragePublicDir() . $filePath),
                    'storage_path' => $filePath,
                    'description' => array_key_exists('description', $file) ? $file['description'] : null
                ]);
            }
            else {
                // Guardar nuevo registro del archivo
                $client->file()->create([
                    'name' => $file['name'],
                    'mime' => mime_content_type($this->getStoragePublicDir() . $filePath),
                    'size' => filesize($this->getStoragePublicDir() . $filePath),
                    'storage_path' => $filePath,
                    'description' => array_key_exists('description', $file) ? $file['description'] : null
                ]);
            }
        }
        $description = trans('models.responses.updated', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    /**
     * Remove the specified client from storage.
     *
     * @param  int  $id The client id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $client = $this->getClient($id);
        $pronoun = trans_choice('models.pronouns.client', 1);
        if ($user->authenticatable_type === "App\Agency") {
            // Si el usuario logueado es una agencia, solo puede eliminar sus clientes
            $belongToThisAgency = $client->commands()
                ->where('agency_id', $user->authenticatable_id)
                ->exists();
            if (!$belongToThisAgency) {
                throw new AccessDeniedHttpException(trans('models.responses.not_allowed', [
                    'model' => $pronoun
                ]));
            }
        }
        // Checar si hay referencias activas al cliente a borrar
        $referencedTables = $client->activeReferencedTables('id', [], ['client_academic_information', 'clients', 'files', 'client_classes', 'purchasables']);
        $related = null;
        if (count($referencedTables) > 0) {
            $related = \App\BaseModel::parseTableToModelName($referencedTables[0]);
            $related = $related === null ? studly_case($referencedTables[0]) :
                    trans_choice('models.pronouns.' . strtolower($related), 2);
            $description = trans('models.responses.not_deleted', [
                'model' => $pronoun,
                'related' => $related,
            ]);
            throw new DeleteResourceFailedException($description);
        }
        // Eliminar el usuario del cliente
        $clientUser = $client->user;
        if (!empty($clientUser)) {
            // Un usuario no se puede eliminar así mismo
            if ($clientUser->id == $user->id) {
                $description = 'Could not delete User, the User can not remove himself.';
                throw new DeleteResourceFailedException($description);
            }
            // Antes de eliminar el usuario del cliente hay que cambiar el email para que ese email pueda ser reutilizado con nuevos clientes, ya que este campo tiene la restricción de ser único.
            $clientUser->email = $clientUser->email . "-" . Carbon::now()->format('Y-m-d H:i:s');
            $clientUser->update();
            $clientUser->delete();
        }
        // Borrar cliente
        $client->delete();

        $description = trans('models.responses.deleted', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }
}
