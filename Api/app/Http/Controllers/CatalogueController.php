<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CatalogueController extends Controller
{
    use Helpers;

    protected $availableCatalogues = [
        'pickup', 'countries', 'experiences', 'housing', 'genders'
    ];


    /**
     * Retrieve a listing of the requested catalogue.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($catalogue, Request $request)
    {
        // Validar catalogo
        if (!in_array($catalogue, $this->availableCatalogues)) {
            return $this->response->errorNotFound();
        }
        $transformer = null;
        $collection = null;
        $orderBy = null;
        switch ($catalogue) {
            case 'pickup':
                $transformer = new \App\Transformers\PickupTransformer();
                $collection = \App\Pickup::orderBy('name')->get();
                break;
            case 'countries':
                $transformer = new \App\Transformers\CountryTransformer();
                $orderBy = empty($request->get('order_by')) ? 'order' : $request->get('order_by');
                $collection = \App\Country::orderBy($orderBy)->get();
                break;
            case 'experiences':
                $transformer = new \App\Transformers\ExperienceTransformer();
                $orderBy = empty($request->get('order_by')) ? 'name' : $request->get('order_by');
                $collection = \App\Experience::orderBy($orderBy)->get();
                break;
            case 'housing':
                $transformer = new \App\Transformers\HousingTransformer();
                $orderBy = empty($request->get('order_by')) ? 'name' : $request->get('order_by');
                $collection = \App\Housing::orderBy($orderBy);
                if (!empty($request->get('is_active'))) {
                    $collection = $collection->where('is_active', $request->get('is_active'));
                }
                $collection = $collection->get();
                break;
            case 'genders':
                $transformer = new \App\Transformers\GenderTransformer();
                $orderBy = empty($request->get('order_by')) ? 'name' : $request->get('order_by');
                $collection = \App\Gender::orderBy($orderBy);
                if (!empty($request->get('is_active'))) {
                    $collection = $collection->where('is_active', $request->get('is_active'));
                }
                $collection = $collection->get();
                break;
        }
        return $this->response->collection($collection, $transformer);
    }

    /**
     * Get the specified catalogue.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($catalogue, $id)
    {
        // Validar catalogo
        if (!in_array($catalogue, $this->availableCatalogues)) {
            return $this->response->errorNotFound();
        }
        $transformer = null;
        $item = null;
        $pronoun = '';
        try {
            switch ($catalogue) {
                case 'experiences':
                    $pronoun = trans_choice('models.pronouns.course', 1);
                    $transformer = new \App\Transformers\ExperienceTransformer();
                    $item = \App\Experience::findOrFail($id);
                    break;
                case 'housing':
                    $pronoun = trans_choice('models.pronouns.housing', 1);
                    $transformer = new \App\Transformers\HousingTransformer();
                    $item = \App\Housing::findOrFail($id);
                    break;
            }
        } catch (ModelNotFoundException $e) {
            $description = trans('models.responses.not_found', ['model' => $pronoun]);
            throw new NotFoundHttpException($description);
        }
        return $this->response->item($item, $transformer);
    }
}
