<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use Carbon\Carbon;
use App\ConsumptionCenter;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Transformers\ConsumptionCenterTransformer;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ConsumptionCenterController extends Controller
{
    use Helpers;

    protected $validationRules = [
        'slug' => 'required|max:255|unique:consumption_centers,slug',
    ];

    public function __construct(Request $request)
    {
        $this->setPermissionAndModule($request, 'consumption_centers');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $paginate = true;
        if ($request->get('no-paginate', '0') == 1) {
            $paginate = false;
        }

        $result = ConsumptionCenter::orderBy('created_at', 'DESC');

        // Filtrar por estado
        if (($request->has('is_active'))) {
            $result = $result->where('is_active', $request->get('is_active'));
        }

        // Paginar registros
        if ($paginate) {
            $result = $result->paginate(20);
            return $this->response->paginator($result, new ConsumptionCenterTransformer());
        }
        // No paginar, retornar todos los registros encontrados
        else {
            $result = $result->get();
            return $this->response->collection($result, new ConsumptionCenterTransformer());
        }
    }

    /**
     * Find the consumption center by a given id
     * @param type $id The consumption center id
     * @return \App\ConsumptionCenter
     * @throws NotFoundHttpException
     */
    private function getConsumptionCenter($id)
    {
        $consumptionCenter = null;
        $pronoun = trans_choice('models.pronouns.consumptionCenter', 1);
        try {
            $consumptionCenter = ConsumptionCenter::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $description = trans('models.responses.not_found', ['model' => $pronoun]);
            throw new NotFoundHttpException($description);
        }
        return $consumptionCenter;
    }

    /**
     * Validate the consumption center data
     * @param \Illuminate\Http\Request $request The data send by the user
     * @param array $validationRules Consumption center validation rules
     * @throws StoreResourceFailedException|UpdateResourceFailedException
     */
    private function validateConsumptionCenter(Request $request, $validationRules, $data)
    {
        $validator = Validator::make($data, $validationRules);
        $pronoun = trans_choice('models.pronouns.consumptionCenter', 1);
        // Si falla la validación
        if ($validator->fails()) {
            // Responder con los errores
            if ($request->isMethod('post')) {
                $description = trans('models.responses.not_created', ['model' => $pronoun]);
                throw new StoreResourceFailedException($description, $validator->errors());
            }
            else {
                $description = trans('models.responses.not_updated', ['model' => $pronoun]);
                throw new UpdateResourceFailedException($description, $validator->errors());
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $data = $request->only('name');
        $data['slug'] = str_slug($data['name'], '_');

        $this->validateConsumptionCenter($request, $this->validationRules, $data);

        // Crear el centro de consumo
        ConsumptionCenter::create([
            'name' => $data['name'],
            'slug' => $data['slug'],
            'is_active' => 1
        ]);

        $pronoun = trans_choice('models.pronouns.consumptionCenter', 1);
        $description = trans('models.responses.created', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 201];
        return $this->response()->created(null, $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $consumptionCenter = $this->getConsumptionCenter($id);
        return $this->response->item($consumptionCenter, new ConsumptionCenterTransformer(), [], function($resource, $fractal) {
            $include = '';
            if (isset($_GET['include'])) {
                $include = $_GET['include'];
            }
            $fractal->parseIncludes($include);
        });
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $consumptionCenter = $this->getConsumptionCenter($id);

        $pronoun = trans_choice('models.pronouns.consumptionCenter', 1);

        $this->validationRules['updated_at'] = 'required|date|date_format:Y-m-d H:i:s';
        $this->validationRules['slug'] .= ",{$consumptionCenter->id}";

        $data = $request->only('name', 'updated_at');
        $data['slug'] = str_slug($data['name'], '_');

        $this->validateConsumptionCenter($request, $this->validationRules, $data);

        // Checar que el centro de consumo no haya sido modificado previamente
        if ($consumptionCenter->updated_at->greaterThan(new Carbon($request->get('updated_at')))) {
            $description = trans('models.responses.conflict', ['model' => $pronoun]);
            throw new ConflictHttpException($description);
        }

        // Actualizar datos del centro de consumo
        $consumptionCenter->update([
            'name' => $data['name'],
            'slug' => $data['slug'],
        ]);

        $description = trans('models.responses.updated', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $consumptionCenter = $this->getConsumptionCenter($id);
        $pronoun = trans_choice('models.pronouns.consumptionCenter', 1);
        // Checar si hay referencias activas al centro de consumo a borrar
        $referencedTables = $consumptionCenter->activeReferencedTables('id');
        $related = null;
        if (count($referencedTables) > 0) {
            $related = \App\BaseModel::parseTableToModelName($referencedTables[0]);
            $related = $related === null ? studly_case($referencedTables[0]) :
                    trans_choice('models.pronouns.' . strtolower($related), 2);
            $description = trans('models.responses.not_deleted', [
                'model' => $pronoun,
                'related' => $related,
            ]);
            throw new DeleteResourceFailedException($description);
        }

        // Modificar (agreagar la fecha y hora actual) tanto el nombre como el slug, para poder reutilizar estos datos con nuevos centro de consumo, dado que estos campos tienen la restrinción de ser únicos
        $now = \Carbon\Carbon::now()->format('Y-m-d H:i:s');
        $consumptionCenter->name .= '-' . $now;
        $consumptionCenter->slug .= '-' . $now;
        $consumptionCenter->save();

        // Eliminar el centro de consumo
        $consumptionCenter->delete();

        $description = trans('models.responses.deleted', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    /**
     * Activate / deactivate the specified consumption center.
     *
     * @param  int  $id The consumption center id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $consumptionCenter = $this->getConsumptionCenter($id);
        $pronoun = trans_choice('models.pronouns.consumptionCenter', 1);
        $isActive = $consumptionCenter->is_active == 1 ? 0 : 1;
        $key = $isActive === 1 ? 'activated' : 'deactivated';
        $consumptionCenter->update(['is_active' => $isActive]);
        $description = trans("models.responses.$key", ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }
}
