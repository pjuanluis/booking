<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use App\Country;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Transformers\CountryTransformer;
use Dingo\Api\Exception\ResourceException;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class CountryController extends Controller
{
    use Helpers;

    protected $validationRules = [
        'name' => 'required|string|max:255',
        'code' => 'required|string|size:2|unique:countries,code',
    ];

    public function __construct(Request $request)
    {
        $this->setPermissionAndModule($request, 'countries');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $paginate = true;
        if ($request->get('no-paginate', '0') == 1) {
            $paginate = false;
        }

        // Obtener los parámetros para ordenar los registros
        $sort_by = $request->get('sort_by', 'order');
        $dir = $request->get('dir', 'asc');

        $result = Country::orderBy($sort_by, $dir);

        // Filtrar por estado
        if (($request->has('is_active'))) {
            $result = $result->where('is_active', $request->get('is_active'));
        }

        // Paginar registros
        if ($paginate) {
            $result = $result->paginate(20);
            return $this->response->paginator($result, new CountryTransformer());
        }
        // No paginar, retornar todos los registros encontrados
        else {
            $result = $result->get();
            return $this->response->collection($result, new CountryTransformer());
        }
    }

    /**
     * Validate the country data
     * @param \Illuminate\Http\Request $request The data send by the user
     * @param array $validationRules Country validation rules
     * @throws StoreResourceFailedException|UpdateResourceFailedException
     */
    private function validateCountry(Request $request, $validationRules, $data)
    {
        $validator = Validator::make($data, $validationRules);
        $pronoun = trans_choice('models.pronouns.country', 1);
        // Si falla la validación
        if ($validator->fails()) {
            // Responder con los errores
            if ($request->isMethod('post')) {
                $description = trans('models.responses.not_created', ['model' => $pronoun]);
                throw new StoreResourceFailedException($description, $validator->errors());
            }
            else {
                $description = trans('models.responses.not_updated', ['model' => $pronoun]);
                throw new UpdateResourceFailedException($description, $validator->errors());
            }
        }
    }

    /**
     * Find the country by a given id
     * @param type $id The country id
     * @return \App\Country
     * @throws NotFoundHttpException
     */
    private function getCountry($id)
    {
        $country = null;
        $pronoun = trans_choice('models.pronouns.country', 1);
        try {
            $country = Country::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $description = trans('models.responses.not_found', ['model' => $pronoun]);
            throw new NotFoundHttpException($description);
        }
        return $country;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $data = $request->all();
        // Convertir el código a mayúscula
        $data['code'] = strtoupper($request->get('code', ''));
        $this->validateCountry($request, $this->validationRules, $data);

        // obtener el valor más grande del campo order de todos los registros
        $orderMax = Country::max('order');

        // Crear el country
        Country::create([
            'name' => $data['name'],
            'code' => $data['code'],
            'order' => $orderMax + 1
        ]);

        $pronoun = trans_choice('models.pronouns.country', 1);
        $description = trans('models.responses.created', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 201];
        return $this->response()->created(null, $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, CountryTransformer $countryTransformer)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $country = $this->getCountry($id);
        return $this->response->item($country, $countryTransformer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $country = $this->getCountry($id);

        $pronoun = trans_choice('models.pronouns.country', 1);

        $this->validationRules['updated_at'] = 'required|date|date_format:Y-m-d H:i:s';
        $this->validationRules['code'] .= ",{$country->id}";

        $data = $request->all();
        // Convertir el código a mayúscula
        $data['code'] = strtoupper($request->get('code', ''));

        $this->validateCountry($request, $this->validationRules, $data);

        // Checar que el room type no haya sido modificado previamente
        if ($country->updated_at->greaterThan(new Carbon($request->get('updated_at')))) {
            $description = trans('models.responses.conflict', ['model' => $pronoun]);
            throw new ConflictHttpException($description);
        }

        // Actualizar datos del room type
        $country->update([
            'name' => $data['name'],
            'code' => $data['code']
        ]);

        $description = trans('models.responses.updated', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $country = $this->getCountry($id);
        $pronoun = trans_choice('models.pronouns.country', 1);
        // Checar si hay referencias activas al country a borrar
        $referencedTables = $country->activeReferencedTables('id');
        $related = null;
        if (count($referencedTables) > 0) {
            $related = \App\BaseModel::parseTableToModelName($referencedTables[0]);
            $related = $related === null ? studly_case($referencedTables[0]) :
                    trans_choice('models.pronouns.' . strtolower($related), 2);
            $description = trans('models.responses.not_deleted', [
                'model' => $pronoun,
                'related' => $related,
            ]);
            throw new DeleteResourceFailedException($description);
        }

        // Modificar (agreagar la fecha y hora actual) al code, para poder reutilizarlo con nuevos countries, dado que este campo tiene la restrinción de ser único
        $now = \Carbon\Carbon::now()->format('Y-m-d H:i:s');
        $country->code .= '-' . $now;
        $country->save();

        // Eliminar el country
        $country->delete();

        $description = trans('models.responses.deleted', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    /**
     * Activate / deactivate the specified room type.
     *
     * @param  int  $id The room type id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $country = $this->getCountry($id);
        $pronoun = trans_choice('models.pronouns.country', 1);
        $isActive = $country->is_active == 1 ? 0 : 1;
        $key = $isActive === 1 ? 'activated' : 'deactivated';
        $country->update(['is_active' => $isActive]);
        $description = trans("models.responses.$key", ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    /**
     * Sort the specified country.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id The country id
     * @return \Illuminate\Http\Response
     */
    public function sort(Request $request, $id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $pronoun = trans_choice('models.pronouns.country', 1);
        $country = $this->getCountry($id);
        $country->sort($request->get('dir', 'down'));
        $description = trans("models.responses.sorted", ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }
}
