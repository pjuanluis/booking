<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Illuminate\Http\Request;
use Dingo\Api\Http\Response;
use App\User;
use App\Role;
use App\SystemModule;
use App\SystemPermission;
use Carbon\Carbon;
use DB;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $module;

    protected $permission;

    public function setPermissionAndModule(Request $request, $moduleSlug)
    {
        // Obtener el modulo del sistema
        $this->module = SystemModule::where('slug', $moduleSlug)->first();
        // Obtener el permiso requerido para ejecutar la acción solicitada
        $slug = 'read';
        if ($request->isMethod('post')) {
            $slug = 'add';
        }
        else if ($request->isMethod('put')) {
            $slug = 'edit';
        }
        else if ($request->isMethod('delete')) {
            $slug = 'delete';
        }
        $this->permission = SystemPermission::where('slug', $slug)->first();
    }

    public function responseOk($content)
    {
        $response = new Response($content);
        $response->setStatusCode(200);
        return $response;
    }

    /**
     * Check if the specified user has permission over the specified system module.
     * @param User $user The user logueado
     * @param SystemModule $module The system module being accesed
     * @param SystemPermission $permission The permission to check
     * @throws AccessDeniedHttpException If role doesn't have permission.
     */
    public function checkPermission(User $user, SystemModule $module, SystemPermission $permission)
    {
        $role = $user->role;
        // Si el rol está vació es por que el usuario logueado es administrador
        if (empty($role)) {
            $role = \App\Role::where('slug', 'administrator')->first();
        }
        if (!$role->hasPermission($module->id, $permission->id)) {
            $description = trans('models.responses.not_permission', [
                'permission' => $permission->name,
                'module' => $module->name
            ]);
            throw new AccessDeniedHttpException($description);
        }
    }

    /**
     * Get the total beds of a room type
     *
     * @param int $housing_room_id The housing room id
     */
    public function getTotalBedsOfARoomType($housingRoomId, $fromDate, $toDate) {
        $result0 = DB::select("select sum(tabletmp.total_beds) as total_beds from (
                        select count(rooms.id) * (CASE WHEN housing_rooms.cost_type='room' THEN 1 ELSE rooms.beds END) as total_beds from rooms, housing_rooms_rooms, housing_rooms where rooms.id = housing_rooms_rooms.room_id and housing_rooms_rooms.housing_room_id = housing_rooms.id and housing_rooms.id=:housing_room_id and rooms.deleted_at is null and rooms.id not in (select room_id from blocked_rooms where ((from_at > :from_at and from_at < :to_at) or (to_at < :to_at and to_at > :from_at) or (from_at <= :from_at and to_at >= :to_at)) and deleted_at is null ) group by housing_rooms.id, rooms.id
                    ) as tabletmp
                ", ['housing_room_id' =>  $housingRoomId, 'from_at' => $fromDate, 'to_at' => $toDate]);
        $totalBeds = 0;
        if (!empty($result0)) {
            $totalBeds = $result0[0]->total_beds;
        }
        return $totalBeds;
    }

    public function getReservedHousing($fromDate, $toDate, $housingRoomId, $commandHousingId = null) {
        $commandsHousing = DB::table('command_housing')
                ->select('command_housing.room_id as room_id', 'command_housing.from_date', 'command_housing.to_date', 'housing_rooms.cost_type', 'command_housing.people')
                ->leftJoin('housing_room_packages', 'housing_room_packages.id', '=', 'command_housing.housing_room_package_id')
                ->leftJoin('housing_rooms', 'housing_rooms.id', '=', 'housing_room_packages.housing_room_id')
                ->where(function($query) use($fromDate, $toDate) {
                    $query->where(function($query0) use($fromDate, $toDate) {
                        $query0->where('from_date', '>', $fromDate)
                                ->where('from_date', '<', $toDate);
                    })
                    ->orWhere(function($query0) use($fromDate, $toDate) {
                        $query0->where('to_date', '<', $toDate)
                                ->where('to_date', '>', $fromDate);
                    })
                    ->orWhere(function($query0) use($fromDate, $toDate) {
                        $query0->where('from_date', '<=', $fromDate)
                                ->where('to_date', '>=', $toDate);
                    });
                })
                ->whereNull('command_housing.deleted_at');
        // Excluir un booking en especifico
        if (!empty($commandHousingId)) {
            $commandsHousing = $commandsHousing->where('command_housing.id', '!=', $commandHousingId);
        }
        // Obtener los cuartos del housing_room
        $housingRoomsTmp = DB::raw('select room_id from housing_rooms_rooms, housing_rooms where housing_rooms_rooms.housing_room_id = housing_rooms.id and housing_rooms.id=:housing_room_id');
        // Obtener de los cuartos seleccionados previamente los housing_room diferentes al housing_room recibido en la petición
        $housingRoomsTmp = DB::select('select housing_room_id from housing_rooms_rooms where room_id in (' . $housingRoomsTmp . ') and housing_room_id!=:housing_room_id group by housing_room_id', ['housing_room_id' =>  $housingRoomId]);
        $commandsHousing = $commandsHousing->where(function($query) use($housingRoomsTmp, $housingRoomId){
            $query->where('housing_room_packages.housing_room_id', $housingRoomId);
            foreach ($housingRoomsTmp as $hr) {
                $query->orWhere('housing_room_packages.housing_room_id', $hr->housing_room_id);
            }
        });
        return $commandsHousing->orderBy('command_housing.from_date', 'asc')
                ->get();
    }

    public function getOcuppancy($fromDate, $toDate, $housingRoomId, $people = 0, $commandHousingId = null, $addExtra = true) {
        // Obtener los housing reservados entre las fechas especificadas
        $reservedHousing = $this->getReservedHousing($fromDate, $toDate, $housingRoomId, $commandHousingId);
        $currentRoomId = -1;
        $occupancy = 0;
        $room = null;
        $reservedHousing = $reservedHousing->sortBy('room_id')->values();
        // Obtener los cuarto bloqueados
        $blockedRooms = DB::select("select room_id from blocked_rooms where ((from_at > :from_at and from_at < :to_at) or (to_at < :to_at and to_at > :from_at) or (from_at <= :from_at and to_at >= :to_at)) and deleted_at is null", ['from_at' => $fromDate, 'to_at' => $toDate]);
        $blockedRoomsId = [];
        foreach ($blockedRooms as $br) {
            $blockedRoomsId[] = $br->room_id;
        }
        $arrayBookingsSameRoom = [];
        $hr = DB::table('housing_rooms')
                ->select('cost_type')
                ->where('id', $housingRoomId)
                ->first();
        // Para que este algoritmo funcione los $reservedHousing deben venir ordenados por room_id
        foreach ($reservedHousing as $index => $booking) {
            if (!$this->belongToTheHousingRoom($booking->room_id, $housingRoomId)) {
                continue;
            }
            if ($currentRoomId === -1) { // Debe cumplirse solo en la primera iteración
                $currentRoomId = $booking->room_id;
                $room = DB::table('rooms')->select('beds')
                            ->where('id', $booking->room_id)
                            ->whereNotIn('id', $blockedRoomsId)
                            ->first();
            } elseif ($currentRoomId !== $booking->room_id) { //Se trata de otro cuarto
                $occupancy += $this->getRoomOccupancy($arrayBookingsSameRoom, $room, $hr->cost_type, $people);
                $arrayBookingsSameRoom = [];
                $currentRoomId = $booking->room_id;
                $room = DB::table('rooms')->select('beds')
                        ->where('id', $booking->room_id)
                        ->whereNotIn('id', $blockedRoomsId)
                        ->first();
            }
            $arrayBookingsSameRoom[] = $booking;
            if ($index + 1 >= $reservedHousing->count()) { // Entra solo en la última iteración
                $occupancy += $this->getRoomOccupancy($arrayBookingsSameRoom, $room, $hr->cost_type, $people);
            }
        }

        if ($addExtra) {
            if ($hr->cost_type == 'room') {
                $occupancy++;
            }
            else {
                $occupancy += $people;
            }
        }
        return $occupancy;
    }

    private function getRoomOccupancy($arrayBookingsSameRoom, $room, $costType, $people) {
        $totalBedsOccupied = $this->getTotalBedsOccupied($arrayBookingsSameRoom, $room, $costType);
        if ($costType === 'bed' && $room) {
            $availableBeds = $room->beds - $totalBedsOccupied;
            // Se considera totalmente ocupado un cuarto en el cual no se podrán hospedar todas personas
            // del booking
            return $availableBeds < $people ? $room->beds : $totalBedsOccupied;
        }
        return $totalBedsOccupied;
    }

    /**
     * Obtiene el total de camas ocupadas de un cuarto
     *
     * @param Illuminate\Support\Collection|array $arrayBookingsSameRoom Las reservaciones de un cuarto
     * @param object $room El cuarto
     * @param string $costType Forma en que se vende el cuarto(cama o cuarto)
     * @return int
     */
    private function getTotalBedsOccupied($arrayBookingsSameRoom, $room, $costType = 'bed') {
        $arrayAvailableBeds = $room ? $this->simulateAssignment($arrayBookingsSameRoom, $room, $costType) : [];
        $totalBedsOccupied = 0;
        foreach ($arrayAvailableBeds as $bed) {
            if ($bed) {
                $totalBedsOccupied++;
            }
        }
        return $totalBedsOccupied;
    }

    /**
     * Simula la ocupación de las camas
     *
     * @param Illuminate\Support\Collection|array $arrayBookingsSameRoom Las reservaciones de un cuarto
     * @param object $room El cuarto
     * @param string $costType Forma en que se vende el cuarto(cama o cuarto)
     * @return array
     */
    private function simulateAssignment($arrayBookingsSameRoom, $room, $costType = 'bed') {
        $availableBeds = $costType === 'bed'? $room->beds : 1;
        $arrayAvailableBeds = $this->initArrayAvailableBeds($availableBeds);
        foreach ($arrayBookingsSameRoom as $booking) {
            if ($this->isEmptyArrayAvailableBeds($arrayAvailableBeds)) {
                $arrayAvailableBeds = $this->assignAReservation($arrayAvailableBeds, 0, $booking, $booking->people);
                continue;
            }
            $addDates = false;
            foreach ($arrayAvailableBeds as $indexBed => $bed) {
                $addDates = false;
                foreach ($bed as $dates) {
                    if ($this->isOutsideDateRange($booking->from_date, $booking->to_date, $dates['from_date'], $dates['to_date'])) {
                        $addDates = true;
                        continue;
                    }
                    $addDates = false;
                    break;
                }
                if ($addDates || count($bed) === 0) {
                    $arrayAvailableBeds = $this->assignAReservation($arrayAvailableBeds, $indexBed, $booking, $booking->people);
                    break;
                }
            }
        }
        return $arrayAvailableBeds;
    }

    /**
     * Inicializa el arreglo de camas
     *
     * @param int $availableBeds Total de camas de un cuarto
     * @return array
     */
    private function initArrayAvailableBeds($availableBeds) {
        $arrayAvailableBeds = [];
        for ($i = 0; $i < $availableBeds; $i++) {
            $arrayAvailableBeds[$i] = [];
        }
        return $arrayAvailableBeds;
    }

    /**
     * Valida si todos las camas están vacias
     *
     * @param array $arrayAvailableBeds Las camas disponibles
     * @return bool
     */
    private function isEmptyArrayAvailableBeds($arrayAvailableBeds) {
        foreach ($arrayAvailableBeds as $row) {
            if ($row) {
                return false;
            }
        }
        return true;
    }

    /**
    * Asigna las fechas en las que se va a ocupada una cama
    *
    * @param array $arrayAvailableBeds Las camas disponibles
    * @param int $indexBed Posición de la cama que será ocupada
    * @param object $booking La reserva
    * @param int $people Número de personas de la reserva
    * @return array
    */
    private function assignAReservation($arrayAvailableBeds, $indexBed = 0, $booking, $people = 1) {
        if ($indexBed >= count($arrayAvailableBeds)) {
            return $arrayAvailableBeds;
        }
        $arrayAvailableBeds[$indexBed][] = [
            'from_date' => $booking->from_date,
            'to_date' => $booking->to_date
        ];
        if ($booking->cost_type === 'bed' && $people > 1) {
            $arrayAvailableBeds = $this->assignAReservation($arrayAvailableBeds, $indexBed + 1, $booking, $people - 1);
        }
        return $arrayAvailableBeds;
    }

    /**
     * Valida si dos rangos de fechas no se entrelazan
     *
     * @param string $fromDate1 Fecha de inicio 1
     * @param string $toDate1 Fecha fin 1
     * @param string $fromDate2 Fecha de inicio 2
     * @param string $toDate2 Fecha fin 2
     * @return bool
     */
    private function isOutsideDateRange($fromDate1, $toDate1, $fromDate2, $toDate2) {
        $fromDate = new Carbon($fromDate1);
        $toDate = new Carbon($toDate1);
        $fDate = new Carbon($fromDate2); // fDate = from date
        $tDate = new Carbon($toDate2); // tDate = to date
        if ($fromDate->gte($tDate) || $toDate->lte($fDate)) {
            return true;
        }
        return false;
    }

    /**
     * Valida si un cuarto está disponible en un rango de fechas
     *
     * @access public
     * @param int $roomId Id del cuarto
     * @param string $costType De la forma en que se vende el cuarto('room' o 'bed')
     * @param string $fromDate Fecha de inicio
     * @param string $toDate Fecha de fin
     * @param int $people La cantidad de personas que van a ocupar el cuarto
     * @return bool
     */
    public function validateRoomAvailability($roomId, $costType, $fromDate, $toDate, $people, $commandHousingId = null)
    {
        // Obtener el cuarto
        $room = \App\Room::where('id', $roomId)
            // validar que el cuarto no esté bloqueado
            ->whereRaw('id not in (select room_id from blocked_rooms where ((from_at > ? and from_at < ?) or (to_at < ? and to_at > ?) or (from_at <= ? and to_at >= ?)) and deleted_at is null)', [$fromDate, $toDate, $toDate, $fromDate, $fromDate, $toDate])
            ->first();
        if (empty($room)) {
            return false;
        }
        // Validar disponibilidad del cuarto
        $result = DB::table('command_housing')
            ->select('command_housing.people', 'command_housing.from_date', 'command_housing.to_date', 'housing_rooms.cost_type')
            ->leftJoin('housing_room_packages', 'command_housing.housing_room_package_id', '=', 'housing_room_packages.id')
            ->leftJoin('housing_rooms', 'housing_room_packages.housing_room_id', '=', 'housing_rooms.id')
            ->where(function ($query) use ($fromDate, $toDate) {
                $query->where(function ($query0) use ($fromDate, $toDate) {
                    $query0->where('from_date', '>', $fromDate)
                        ->where('from_date', '<', $toDate);
                })
                ->orWhere(function ($query0) use ($fromDate, $toDate) {
                    $query0->where('to_date', '<', $toDate)
                        ->where('to_date', '>', $fromDate);
                })
                ->orWhere(function ($query0) use ($fromDate, $toDate) {
                    $query0->where('from_date', '<=', $fromDate)
                        ->where('to_date', '>=', $toDate);
                });
            })
            ->where('room_id', $roomId)
            ->whereNull('command_housing.deleted_at');
        if (!empty($commandHousingId)) {
            $result->where('command_housing.id', '!=', $commandHousingId);
        }
        $availableSpace = $this->getAvailableSpaceOfTheRoom($result->get(), $room, $costType);
        return $availableSpace >= (($costType == 'room') ? 1 : $people);
    }

    /**
     * Obtiene el espacio disponible de un cuarto
     *
     * Retorna un número entero mayor o igual a cero, donde cero quiere decir que no hay espacio disponible
     *
     * @access public
     * @param Illuminate\Support\Collection $bookingsOfTheSameRoom Reservaciones de un mismo cuarto
     * @param App\Room $room El cuarto
     * @return int
     */
    public function getAvailableSpaceOfTheRoom($bookingsOfTheSameRoom, $room, $costType)
    {
        $occupancy = $this->getTotalBedsOccupied($bookingsOfTheSameRoom, $room, $costType);
        if ($costType == 'room') {
            return 1 - $occupancy;
        }
        return $room->beds - $occupancy;
    }

    /**
     * Asigna un cuarto a las comandas que no tengan uno
     *
     * @access public
     * @return void
     */
    public function assignRooms() {
        // Seleccionar las comandas de hospedaje que no tenga un cuarto asignado
        $commandsHousing = DB::table('command_housing')
            ->select('command_housing.id', 'command_housing.people', 'housing_rooms.id as housing_room_id', 'housing_rooms.cost_type', 'from_date', 'to_date')
            ->leftJoin('housing_room_packages', 'housing_room_packages.id', '=', 'command_housing.housing_room_package_id')
            ->leftJoin('housing_rooms', 'housing_rooms.id', '=', 'housing_room_packages.housing_room_id')
            ->whereNull('room_id')
            ->whereNull('command_housing.deleted_at')
            ->get();
        // Asignar un cuarto temporal a las comandas
        foreach ($commandsHousing as $ch) {
            // Seleccionar primero los cuartos que no son multitipo (de esta forma primero se ocuparan estos cuartos)
            $rooms = DB::table('housing_rooms_rooms as hrr')
                ->select('room_id as id')
                ->where('housing_room_id', $ch->housing_room_id)
                ->whereRaw('(select count(room_id) from housing_rooms_rooms where room_id = hrr.room_id) = 1')
                ->orderBy('id', 'asc')
                ->get();
            // Seleccionar los cuarto multitipo y unirlos con los no multitipos
            $rooms = $rooms->merge(DB::table('housing_rooms_rooms as hrr')
                ->select('room_id as id')
                ->where('housing_room_id', $ch->housing_room_id)
                ->whereRaw('(select count(room_id) from housing_rooms_rooms where room_id = hrr.room_id) > 1')
                ->orderBy('id', 'asc')
                ->get());
            foreach ($rooms as $room) {
                $isAvailable = $this->validateRoomAvailability($room->id, $ch->cost_type, $ch->from_date, $ch->to_date, $ch->people);
                if ($isAvailable) {
                    DB::table('command_housing')
                        ->where('id', $ch->id)
                        ->update(['room_id' => $room->id]);
                    break;
                }
            }
        }
    }

    public function belongToTheHousingRoom($roomId, $housingRoomId) {
        return DB::table('housing_rooms_rooms')
            ->where('room_id', $roomId)
            ->where('housing_room_id', $housingRoomId)
            ->exists();
    }
}
