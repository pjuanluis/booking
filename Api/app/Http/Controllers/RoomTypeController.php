<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use App\RoomType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Transformers\RoomTypeTransformer;
use Dingo\Api\Exception\ResourceException;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class RoomTypeController extends Controller
{
    use Helpers;

    protected $validationRules = [
        'name' => 'required|string|max:255|unique:room_types,slug',
        'description' => 'nullable|string',
    ];

    public function __construct(Request $request)
    {
        $this->setPermissionAndModule($request, 'room_types');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $paginate = true;
        if ($request->get('no-paginate', '0') == 1) {
            $paginate = false;
        }

        $result = RoomType::orderBy('created_at', 'DESC');

        // Filtrar por estado
        if (($request->has('is_active'))) {
            $result = $result->where('is_active', $request->get('is_active'));
        }

        // Paginar registros
        if ($paginate) {
            $result = $result->paginate(20);
            return $this->response->paginator($result, new RoomTypeTransformer());
        }
        // No paginar, retornar todos los registros encontrados
        else {
            $result = $result->get();
            return $this->response->collection($result, new RoomTypeTransformer());
        }
    }

    /**
     * Validate the room type data
     * @param \Illuminate\Http\Request $request The data send by the user
     * @param array $validationRules Room type validation rules
     * @throws StoreResourceFailedException|UpdateResourceFailedException
     */
    private function validateRoomType(Request $request, $validationRules, $data)
    {
        $validator = Validator::make($data, $validationRules);
        $pronoun = trans_choice('models.pronouns.roomtype', 1);
        // Si falla la validación
        if ($validator->fails()) {
            // Responder con los errores
            if ($request->isMethod('post')) {
                $description = trans('models.responses.not_created', ['model' => $pronoun]);
                throw new StoreResourceFailedException($description, $validator->errors());
            }
            else {
                $description = trans('models.responses.not_updated', ['model' => $pronoun]);
                throw new UpdateResourceFailedException($description, $validator->errors());
            }
        }
    }

    /**
     * Find the room type by a given id
     * @param type $id The room type id
     * @return \App\RoomType
     * @throws NotFoundHttpException
     */
    private function getRoomType($id)
    {
        $roomType = null;
        $pronoun = trans_choice('models.pronouns.roomtype', 1);
        try {
            $roomType = RoomType::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $description = trans('models.responses.not_found', ['model' => $pronoun]);
            throw new NotFoundHttpException($description);
        }
        return $roomType;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        // Crear el slug, por cuestiones de validación se guarda como si fuera el nombre
        $data['name'] = str_slug($request->get('name'), '_');
        $data['description'] = $request->get('description', null);

        $this->validateRoomType($request, $this->validationRules, $data);

        // Crear el room type
        RoomType::create([
            'name' => $request->get('name'),
            'slug' => $data['name'],
            'description' => $data['description'],
            'is_active' => 1
        ]);

        $pronoun = trans_choice('models.pronouns.roomtype', 1);
        $description = trans('models.responses.created', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 201];
        return $this->response()->created(null, $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, RoomTypeTransformer $roomTypeTransformer)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $pronoun = trans('models.pronouns.roomtype');
        $roomType = $this->getRoomType($id);
        return $this->response->item($roomType, $roomTypeTransformer, [], function($resource, $fractal) {
            $exclude = '';
            $include = '';
            if (isset($_GET['exclude'])) {
                $exclude = $_GET['exclude'];
            }
            if (isset($_GET['include'])) {
                $include = $_GET['include'];
            }
            $fractal->parseExcludes($exclude);
            $fractal->parseIncludes($include);
        });
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $roomType = $this->getRoomType($id);

        $pronoun = trans_choice('models.pronouns.roomtype', 1);

        $this->validationRules['updated_at'] = 'required|date|date_format:Y-m-d H:i:s';
        $this->validationRules['name'] .= ",{$roomType->id}";

        // Crear el slug, por cuestiones de validación se guarda como si fuera el nombre
        $data['name'] = str_slug($request->get('name'), '_');
        $data['updated_at'] = $request->get('updated_at');
        $data['description'] = $request->get('description', null);

        $this->validateRoomType($request, $this->validationRules, $data);

        // Checar que el room type no haya sido modificado previamente
        if ($roomType->updated_at->greaterThan(new Carbon($request->get('updated_at')))) {
            $description = trans('models.responses.conflict', ['model' => $pronoun]);
            throw new ConflictHttpException($description);
        }

        // Actualizar datos del room type
        $roomType->update([
            'name' => $request->get('name'),
            'slug' => $data['name'],
            'description' => $data['description']
        ]);

        $description = trans('models.responses.updated', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $roomType = $this->getRoomType($id);
        $pronoun = trans_choice('models.pronouns.roomtype', 1);
        // Checar si hay referencias activas al room type a borrar
        $referencedTables = $roomType->activeReferencedTables('id');
        $related = null;
        if (count($referencedTables) > 0) {
            $related = \App\BaseModel::parseTableToModelName($referencedTables[0]);
            $related = $related === null ? studly_case($referencedTables[0]) :
                    trans_choice('models.pronouns.' . strtolower($related), 2);
            $description = trans('models.responses.not_deleted', [
                'model' => $pronoun,
                'related' => $related,
            ]);
            throw new DeleteResourceFailedException($description);
        }

        // Modificar (agreagar la fecha y hora actual) a el slug, para poder reutilizarlo con nuevos room type, dado que este campo tiene la restrinción de ser único
        $now = \Carbon\Carbon::now()->format('Y-m-d H:i:s');
        $roomType->slug .= '-' . $now;
        $roomType->save();

        // Eliminar el room type
        $roomType->delete();

        $description = trans('models.responses.deleted', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    /**
     * Activate / deactivate the specified room type.
     *
     * @param  int  $id The room type id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $roomType = $this->getRoomType($id);
        $pronoun = trans_choice('models.pronouns.roomtype', 1);
        $isActive = $roomType->is_active == 1 ? 0 : 1;
        $key = $isActive === 1 ? 'activated' : 'deactivated';
        $roomType->update(['is_active' => $isActive]);
        $description = trans("models.responses.$key", ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }
}
