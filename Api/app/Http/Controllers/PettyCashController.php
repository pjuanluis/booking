<?php

namespace App\Http\Controllers;

use Auth;
use File;
use Validator;
use Carbon\Carbon;
use App\PettyCash;
use App\ConsumptionCenter;
use App\Traits\FileAction;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Transformers\PettyCashTransformer;
use App\Transformers\ConsumptionCenterTransformer;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class PettyCashController extends Controller
{
    use Helpers, FileAction;

    protected $validationRules = [
        'consumption_center_id' => 'required|integer|exists:consumption_centers,id',
        'description' => 'required',
        'amount' => 'required|numeric|min:1',
        'is_deposit' => 'required|integer|in:0,1',
        'file' => 'nullable|array',
        'file.name' => 'required_with:file',
        'file.contents' => 'required_with:file',
    ];

    public function __construct(Request $request)
    {
        $this->setPermissionAndModule($request, 'petty_cash');
    }

    /**
     * Undocumented function
     *
     * @param integer $clientId
     * @return void
     */
    private function getConsumptionCenterPettyCash(int $consumptionCenterId = 0, Request $request)
    {
        try {
            $result = ConsumptionCenter::findOrFail($consumptionCenterId);
            $consumptionCenterTransformer = new ConsumptionCenterTransformer();
            $consumptionCenterTransformer->setRequest($request);
            $consumptionCenterTransformer->setDefaultIncludes(['pettyCashes']);
            return $this->response->item($result, $consumptionCenterTransformer);
        } catch(ModelNotFoundException $e) {
            $pronoun = trans_choice('models.pronouns.consumptionCenter', 1);
            $description = trans('models.responses.not_found', ['model' => $pronoun]);
            throw new NotFoundHttpException($description);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $paginate = true;
        if ($request->get('no-paginate', '0') == 1) {
            $paginate = false;
        }

        $consumptionCenterId = $request->has('with_consumption_center_id') ? $request->get('with_consumption_center_id', 0) : 0;

        if ($consumptionCenterId > 0) {
            return $this->getConsumptionCenterPettyCash($consumptionCenterId, $request);
        }

        $result = PettyCash::orderBy('created_at', 'DESC');

        // Paginar registros
        if ($paginate) {
            $result = $result->paginate(20);
            return $this->response->paginator($result, new PettyCashTransformer());
        }
        // No paginar, retornar todos los registros encontrados
        else {
            $result = $result->get();
            return $this->response->collection($result, new PettyCashTransformer());
        }
    }

    /**
     * Find the petty cash by a given id
     * @param type $id The petty cash id
     * @return \App\PettyCash
     * @throws NotFoundHttpException
     */
    private function getPettyCash($id)
    {
        $pettyCash = null;
        $pronoun = trans_choice('models.pronouns.pettyCash', 1);
        try {
            $pettyCash = PettyCash::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $description = trans('models.responses.not_found', ['model' => $pronoun]);
            throw new NotFoundHttpException($description);
        }
        return $pettyCash;
    }

    /**
     * Validate the petty cash data
     * @param \Illuminate\Http\Request $request The data send by the user
     * @param array $validationRules Petty cash validation rules
     * @throws StoreResourceFailedException|UpdateResourceFailedException
     */
    private function validatePettyCash(Request $request, $filePath, $validationRules)
    {
        $validator = Validator::make($request->all(), $validationRules);
        if ($filePath !== null) {
            $filePath = $this->getStoragePublicDir() . $filePath;
        }
        $validator->after(function ($validator) use($filePath) {
            if ($filePath !== null) {
                $mime = mime_content_type($filePath);
                // Validar que el archivo sea una imagen
                if (!in_array($mime, ['image/png', 'image/jpeg', 'image/gif'])) {
                    $validator->errors()->add("file", trans('validation.custom.file.image'));
                } else if (File::size($filePath) > 1000000) {
                    $validator->errors()->add("file", trans('validation.custom.file.size', ['size' => '1 MB']));
                }
            }
        });
        $pronoun = trans_choice('models.pronouns.pettyCash', 1);
        // Si falla la validación
        if ($validator->fails()) {
            if ($filePath !== null) {
                // Borrar archivo
                unlink($filePath);
            }
            // Responder con los errores
            if ($request->isMethod('post')) {
                $description = trans('models.responses.not_created', ['model' => $pronoun]);
                throw new StoreResourceFailedException($description, $validator->errors());
            }
            else {
                $description = trans('models.responses.not_updated', ['model' => $pronoun]);
                throw new UpdateResourceFailedException($description, $validator->errors());
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $filePath = null;
        if ($request->has('file')) {
            $filePath = $this->uploadFile($request->get('file'), 'pettycash');
        }

        // Validar datos
        $this->validatePettyCash($request, $filePath, $this->validationRules);

        if ($request->get('is_deposit') == 0) {
            // Es retiro
            $this->validateAmountField($request, $request->get('consumption_center_id'));
        }

        // Guardar el movimiento
        $pettyCash = PettyCash::create(array_merge($request->all(), ['user_id' => $user->id]));

        // Agregar archivo
        if ($request->has('file')) {
            $file = $request->get('file');
            $pettyCash->file()->create([
                'name' => $file['name'],
                'mime' => mime_content_type($this->getStoragePublicDir() . $filePath),
                'size' => filesize($this->getStoragePublicDir() . $filePath),
                'storage_path' => $filePath,
                'description' => null
            ]);
        }

        $pronoun = $request->get('is_deposit', 1) == 1? 'Deposit' : 'Withdrawal';
        $description = trans('models.responses.created', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 201];
        return $this->response()->created(null, $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, PettyCashTransformer $pettyCashTransformer)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $pettyCash = null;
        $pettyCash = $this->getPettyCash($id);
        return $this->response->item($pettyCash, $pettyCashTransformer, [], function($resource, $fractal) {
            $include = '';
            if (isset($_GET['include'])) {
                $include = $_GET['include'];
            }
            $fractal->parseIncludes($include);
        });
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $pettyCash = $this->getPettyCash($id);
        $pronoun = trans_choice('models.pronouns.pettyCash', 1);
        $filePath = null;
        if ($request->has('file')) {
            $filePath = $this->uploadFile($request->get('file'), 'pettycash');
        }

        $this->validationRules['updated_at'] = 'required|date|date_format:Y-m-d H:i:s';
        $this->validationRules['consumption_center_id'] = 'nullable';

        $this->validatePettyCash($request, $filePath, $this->validationRules);

        if ($request->get('is_deposit') == 0) {
            // Es retiro
            $this->validateAmountField($request, $pettyCash->consumptionCenter->id);
        }

        // Checar que la caja chica no haya sido modificado previamente
        if ($pettyCash->updated_at->greaterThan(new Carbon($request->get('updated_at')))) {
            $description = trans('models.responses.conflict', ['model' => $pronoun]);
            throw new ConflictHttpException($description);
        }
        // Actualizar datos del movimiento de la caja chica
        // Un movimiento(deposito o retiro) no se puede cambiar de centro de consumo
        $pettyCash->update($request->except('consumption_center_id'));

        // Actualizar archivo
        if ($request->has('file')) {
            $file = $request->get('file');
            $oldFile = $pettyCash->file;
            // Si el movimiento tiene un archivo actualmente, reemplazarlo
            if ($oldFile !== null) {
                // Borrar archivo guardado previamente para la validación
                unlink($this->getStoragePublicDir() . $filePath);
                // Reemplazar archivo del movimiento
                $filePath = $this->replaceFile($oldFile, $file);
                // Actualizar datos del archivo del movimiento
                $pettyCash->file->update([
                    'name' => $file['name'],
                    'mime' => mime_content_type($this->getStoragePublicDir() . $filePath),
                    'size' => filesize($this->getStoragePublicDir() . $filePath),
                    'storage_path' => $filePath,
                    'description' => null
                ]);
            }
            else {
                // Guardar nuevo registro del archivo
                $pettyCash->file()->create([
                    'name' => $file['name'],
                    'mime' => mime_content_type($this->getStoragePublicDir() . $filePath),
                    'size' => filesize($this->getStoragePublicDir() . $filePath),
                    'storage_path' => $filePath,
                    'description' => null
                ]);
            }
        }

        $pronoun = $request->get('is_deposit', 1) == 1? 'Deposit' : 'Withdrawal';

        $description = trans('models.responses.updated', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    /*
        Valida que la cantidad a retira no sea mayor al balance(cantidad disponible)
    */
    private function validateAmountField(Request $request, $consumptionCenterId) {
        // Obtener el total depositado
        $totalDeposit = PettyCash::where('consumption_center_id', $consumptionCenterId)
            ->where('is_deposit', 1)
            ->pluck('amount')
            ->sum();

        // Obtener el total retirado
        $totalWithdrawal = PettyCash::where('consumption_center_id', $consumptionCenterId)
            ->where('is_deposit', 0)
            ->pluck('amount')
            ->sum();

        // Calcular el balance
        $balance = $totalDeposit - $totalWithdrawal;

        // Validar que la cantidad a retirar sea menor o igual al balance
        $this->validationRules = ['amount' => 'numeric|max:' . $balance];
        $this->validatePettyCash($request, null, $this->validationRules);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $pettyCash = $this->getPettyCash($id);

        $pronoun = trans_choice('models.pronouns.pettyCash', 1);

        // Borrar archivo del movimiento en caso de que si tenga
        $file = $pettyCash->file;
        if ($file !== null) {
            $this->deleteFile($file, 'public', 'local', true);
        }

        // Borrar movimiento
        $pettyCash->delete();
        $description = trans('models.responses.deleted', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }
}
