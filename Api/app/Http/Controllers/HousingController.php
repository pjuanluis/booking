<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Validator;
use App\Housing;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Transformers\HousingTransformer;
use Dingo\Api\Exception\ResourceException;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class HousingController extends Controller
{
    use Helpers;

    protected $validationRules = [
        'name' => 'required|max:255|unique:housing,slug',
    ];

    public function __construct(Request $request)
    {
        $this->setPermissionAndModule($request, 'housing');
    }

    /**
     * Return a listing of housing.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, HousingTransformer $housingTransformer)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        // Obtener cursos
        $result = Housing::orderBy('name');
        // Filtrar por nombre
        if (!empty($request->get('name'))) {
            $result = $result->where('name', 'ilike', $request->get('name') . '%');
        }
        // Filtrar por status
        if (!empty($request->get('is_active'))) {
            $result = $result->where('is_active', $request->get('is_active'));
        }
        $result = $result->get();
        return $this->response->collection($result, $housingTransformer, [], function($resource, $fractal) {
            $exclude = '';
            if (isset($_GET['exclude'])) {
                $exclude = $_GET['exclude'];
            }
            $include = '';
            if (isset($_GET['include'])) {
                $include = $_GET['include'];
            }
            $fractal->parseExcludes($exclude);
            $fractal->parseIncludes($include);
        });
    }

    /**
     * Find the housing by a given id
     * @param type $id The housing id
     * @return \App\Housing
     * @throws NotFoundHttpException
     */
    private function getHousing($id)
    {
        $housing = null;
        $pronoun = trans_choice('models.pronouns.housing', 1);
        try {
            $housing = Housing::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $description = trans('models.responses.not_found', ['model' => $pronoun]);
            throw new NotFoundHttpException($description);
        }
        return $housing;
    }

    /**
     * Validate the housing data
     * @param \Illuminate\Http\Request $request The data send by the user
     * @param array $validationRules Consumption center validation rules
     * @throws StoreResourceFailedException|UpdateResourceFailedException
     */
    private function validateHousing(Request $request, $validationRules, $data)
    {
        $validator = Validator::make($data, $validationRules);
        $pronoun = trans_choice('models.pronouns.housing', 1);
        // Si falla la validación
        if ($validator->fails()) {
            // Responder con los errores
            if ($request->isMethod('post')) {
                $description = trans('models.responses.not_created', ['model' => $pronoun]);
                throw new StoreResourceFailedException($description, $validator->errors());
            }
            else {
                $description = trans('models.responses.not_updated', ['model' => $pronoun]);
                throw new UpdateResourceFailedException($description, $validator->errors());
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        // Crear el slug, por cuestiones de validación se guarda como si fuera el nombre
        $data['name'] = str_slug($request->get('name'), '_');

        $this->validateHousing($request, $this->validationRules, $data);

        // Crear el hospedaje
        Housing::create([
            'name' => $request->get('name'),
            'slug' => $data['name'],
            'is_active' => 1
        ]);

        $pronoun = trans_choice('models.pronouns.housing', 1);
        $description = trans('models.responses.created', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 201];
        return $this->response()->created(null, $response);
    }

    /**
     * Get the specified housing.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, HousingTransformer $courseTransformer)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $course = null;
        $pronoun = trans('models.pronouns.housing');
        try {
            $course = Housing::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $description = trans('models.responses.not_found', ['model' => $pronoun]);
            throw new NotFoundHttpException($description);
        }
        return $this->response->item($course, $courseTransformer, [], function($resource, $fractal) {
            $exclude = '';
            if (isset($_GET['exclude'])) {
                $exclude = $_GET['exclude'];
            }
            $include = 'rooms.packages.rates';
            if (isset($_GET['include'])) {
                $include = $_GET['include'];
            }
            $fractal->parseExcludes($exclude);
            $fractal->parseIncludes($include);
        });
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $housing = $this->getHousing($id);

        $pronoun = trans_choice('models.pronouns.housing', 1);

        $this->validationRules['updated_at'] = 'required|date|date_format:Y-m-d H:i:s';
        $this->validationRules['name'] .= ",{$housing->id}";

        // Crear el slug, por cuestiones de validación se guarda como si fuera el nombre
        $data['name'] = str_slug($request->get('name'), '_');
        $data['updated_at'] = $request->get('updated_at');

        $this->validateHousing($request, $this->validationRules, $data);

        // Checar que el hospedaje no haya sido modificado previamente
        if ($housing->updated_at->greaterThan(new Carbon($request->get('updated_at')))) {
            $description = trans('models.responses.conflict', ['model' => $pronoun]);
            throw new ConflictHttpException($description);
        }

        // Actualizar datos del hospedaje
        $housing->update([
            'name' => $request->get('name'),
            'slug' => $data['name'],
        ]);

        $description = trans('models.responses.updated', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $housing = $this->getHousing($id);
        $pronoun = trans_choice('models.pronouns.housing', 1);
        // Checar si hay referencias activas al hospedaje a borrar
        $referencedTables = $housing->activeReferencedTables('id');
        $related = null;
        if (count($referencedTables) > 0) {
            $related = \App\BaseModel::parseTableToModelName($referencedTables[0]);
            $related = $related === null ? studly_case($referencedTables[0]) :
                    trans_choice('models.pronouns.' . strtolower($related), 2);
            $description = trans('models.responses.not_deleted', [
                'model' => $pronoun,
                'related' => $related,
            ]);
            throw new DeleteResourceFailedException($description);
        }

        // Modificar (agreagar la fecha y hora actual) tanto el nombre como el slug, para poder reutilizar estos datos con nuevos hospedajes, dado que estos campos tienen la restrinción de ser únicos
        $now = \Carbon\Carbon::now()->format('Y-m-d H:i:s');
        $housing->name .= '-' . $now;
        $housing->slug .= '-' . $now;
        $housing->save();

        // Eliminar el hospedaje
        $housing->delete();

        $description = trans('models.responses.deleted', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    /**
     * Activate / deactivate the specified housing.
     *
     * @param  int  $id The housing id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $housing = $this->getHousing($id);
        $pronoun = trans_choice('models.pronouns.housing', 1);
        $isActive = $housing->is_active == 1 ? 0 : 1;
        $key = $isActive === 1 ? 'activated' : 'deactivated';
        $housing->update(['is_active' => $isActive]);
        $description = trans("models.responses.$key", ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    public function availability(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'from_date' => 'required|date_format:Y-m-d',
            'to_date' => 'required|date_format:Y-m-d|after:from_date',
            'housing_room_id' => 'required|integer|exists:housing_rooms,id',
            'people' => 'required|integer|min:1'
        ]);
        if ($validator->fails()) {
            throw new ResourceException("Incorrect parameters.", $validator->errors());
        }
        $totalBeds = $this->getTotalBedsOfARoomType($request->get('housing_room_id'), $request->get('from_date'), $request->get('to_date'));
        $occupancy = $this->getOcuppancy($request->get('from_date'), $request->get('to_date'), $request->get('housing_room_id'), $request->input('people', 0), null, true);

        $housingRoom = \App\HousingRoom::with('roomType')
            ->where('id', $request->get('housing_room_id'))
            ->first();
        $housingRoom->availability = $totalBeds - $occupancy;
        return $this->response->item($housingRoom, new \App\Transformers\HousingRoomTransformer(), [], function ($resource, $fractal) {
            $include = '';
            if (isset($_GET['include'])) {
                $include = $_GET['include'];
            }
            $fractal->parseIncludes($include);
        });
    }

    public function occupancyDates(Request $request) {
        $validator = Validator::make($request->all(), [
            //'month' => 'required|date_format:m-Y',
            'housing_room_id' => 'required|exists:housing_rooms,id'
        ]);
        if ($validator->fails()) {
            throw new ResourceException("Incorrect parameters.", $validator->errors());
        }
        $monthYear = explode('-', $request->get('month'));
        $beginDate = (new Carbon($monthYear[1] . '-' . $monthYear[0] . '-01'))->subDays(10);
        $endDate = (new Carbon($monthYear[1] . '-' . $monthYear[0] . '-01'))->endOfMonth()->addDays(10);
        $totalBeds = 0;
        $occupancy = 0;
        $result = [];
        $toDate = null;
        // Calcular ocupación del tipo de cuarto en cada uno de los días del mes especificados
        do {
            $toDate = (new Carbon($beginDate->format('Y-m-d')))->addDay();
            $totalBeds = $this->getTotalBedsOfARoomType($request->get('housing_room_id'), $beginDate->format('Y-m-d'), $toDate->format('Y-m-d'));
            $occupancy = $this->getOcuppancy($beginDate->format('Y-m-d'), $toDate->format('Y-m-d'), $request->get('housing_room_id'), 0, null, false);
            $result[] = [
                'date' => $beginDate->format('Y-m-d'),
                'occupancy' => $occupancy,
                'is_full' => (($totalBeds - $occupancy) <= 0)
            ];
            $beginDate->addDay();
        } while($beginDate->lessThanOrEqualTo($endDate));

        return $this->responseOk(['status_code' => 200, 'data' => $result]);
    }
}
