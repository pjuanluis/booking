<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Room;
use Validator;
use Carbon\Carbon;
use League\Fractal\Manager;
use Illuminate\Http\Request;
use App\Mail\BookingCreated;
use Dingo\Api\Routing\Helpers;
use Illuminate\Validation\Rule;
use League\Fractal\Resource\Item;
use Illuminate\Support\Facades\Mail;
use App\Transformers\RoomTransformer;
use Dingo\Api\Exception\ResourceException;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use App\BlockedRoom;

class RoomController extends Controller
{
    use Helpers;

    protected $validationRules = [
        'name' => 'required',
        'beds' => 'required|integer|min:1',
        'housing_rooms' => 'required|array',
        'housing_rooms.*' => 'required|exists:housing_rooms,id',
        'residence_id' => 'required|exists:residences,id',
    ];

    public function __construct(Request $request)
    {
        $this->setPermissionAndModule($request, 'rooms');
    }

    /**
     * Return a listing of rooms.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, RoomTransformer $roomTransformer)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        // Ordenar cuartos
        $sort_by = $request->get('sort_by', 'name');
        $dir = $request->get('dir', 'asc');
        $result = Room::orderBy($sort_by, $dir);

        // Filtrar por residencia
        if (!empty($request->get('residence_id'))) {
            $result = $result->where('residence_id', $request->get('residence_id'));
        }
        $result = $result->get();
        return $this->response->collection($result, $roomTransformer, [], function($resource, $fractal) {
            $exclude = '';
            if (isset($_GET['exclude'])) {
                $exclude = $_GET['exclude'];
            }
            $include = '';
            if (isset($_GET['include'])) {
                $include .= ',' . $_GET['include'];
            }
            $fractal->parseExcludes($exclude);
            $fractal->parseIncludes($include);
        });
    }

    /**
     * Find the room by a given id
     * @param type $id The room id
     * @return \App\Room
     * @throws NotFoundHttpException
     */
    private function getRoom($id)
    {
        $room = null;
        $pronoun = trans_choice('models.pronouns.room', 1);
        try {
            $room = Room::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $description = trans('models.responses.not_found', ['model' => $pronoun]);
            throw new NotFoundHttpException($description);
        }
        return $room;
    }

    /**
     * Get the specified room.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, RoomTransformer $roomTransformer)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $room = $this->getRoom($id);
        return $this->response->item($room, $roomTransformer, [], function($resource, $fractal) {
            $include = '';
            if (isset($_GET['include'])) {
                $include = $_GET['include'];
            }
            $fractal->parseIncludes($include);
        });
    }

    /**
     * Validate the room data
     * @param \Illuminate\Http\Request $request The data send by the user
     * @param array $validationRules Room validation rules
     * @throws StoreResourceFailedException|UpdateResourceFailedException
     */
    private function validateRoom(Request $request, $validationRules)
    {
        $validator = Validator::make($request->all(), $validationRules);
        $validator->after(function($validator) use($request) {
            if ($request->has('residence_id')) {
                if ($request->has('housing_rooms')) {
                    // Obtener residencia
                    $residence = \App\Residence::find($request->get('residence_id'));
                    $housingRooms = $request->get('housing_rooms', []);
                    if (!empty($residence)) {
                        foreach ($housingRooms as $hr) {
                            // Validar que el tipo de hospedaje del cuarto pertenezca al mismo tipo de hospedaje de la residencia seleccionada
                            $result = DB::table('housing_rooms')
                            ->select('id')
                            ->where('housing_id', $residence->housing_id)
                            ->where('id', $hr)
                            ->first();
                            if (empty($result)) {
                                $description = trans('validation.exists', ['attribute' => trans('validation.attributes.housing_rooms')]);
                                $validator->errors()->add("housing_rooms", $description);
                            }
                        }
                    }
                    // Validar que los tipos de costo de los housing rooms sean del mismo tipo
                    // $costTypes = DB::table('housing_rooms')
                    //     ->whereIn('id', $housingRooms)
                    //     ->pluck('cost_type');
                    // $costType = null;
                    // foreach ($costTypes as $ct) {
                    //     if ($costType === null) {
                    //         // Debe de entrar en la primer iteración
                    //         $costType = $ct;
                    //     } elseif ($costType !== $ct) {
                    //         $description = trans('validation.exists', ['attribute' => trans('validation.attributes.housing_rooms')]);
                    //         $validator->errors()->add("housing_rooms", $description);
                    //         break;
                    //     }
                    // }
                }
            }
        });
        $pronoun = trans_choice('models.pronouns.room', 1);
        // Si falla la validación
        if ($validator->fails()) {
            // Responder con los errores
            if ($request->isMethod('post')) {
                $description = trans('models.responses.not_created', ['model' => $pronoun]);
                throw new StoreResourceFailedException($description, $validator->errors());
            }
            else {
                $description = trans('models.responses.not_updated', ['model' => $pronoun]);
                throw new UpdateResourceFailedException($description, $validator->errors());
            }
        }
    }

    private function addBlockedDates(Room $room, Request $request)
    {
        $now = Carbon::now();
        $insert = [];

        foreach ($request->input('ranges-list', []) as $k => $v) {
            if ($v['id'] === '0') {
                array_push($insert, [
                    'room_id' => $room->id,
                    'from_at' => Carbon::createFromFormat('d-m-Y', $v['from_at'])->startOfDay(),
                    'to_at' => Carbon::createFromFormat('d-m-Y', $v['to_at'])->endOfDay(),
                    'created_at' => $now,
                    'updated_at' => $now,
                ]);
            }
            //By the moment not exist EDIT RAGE DATE, only create or delete.
        }

        if (count($insert) > 0 && $room->id > 0) {
            BlockedRoom::insert($insert);
        }
    }

    /**
     * Store a newly created room in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $this->validateRoom($request, $this->validationRules);
        $room = Room::create([
            'residence_id' => $request->get('residence_id'),
            'name' => $request->get('name'),
            'beds' => $request->get('beds'),
        ]);
        $room->housingRooms()->attach($request->get('housing_rooms'));
        $this->addBlockedDates($room, $request);
        $pronoun = trans_choice('models.pronouns.room', 1);
        $description = trans('models.responses.created', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 201];
        return $this->response()->created(null, $response);
    }

    private function deleteBlockedDates(Room $room, Request $request)
    {
        $datesId = $request->input('ranges_date_deleted', []);

        if (empty($datesId)) {
            return;
        }

        BlockedRoom::where('room_id', $room->id)
            ->whereIn('id', $datesId)
            ->delete();
    }

    /**
     * Update the specified room in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id The room id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $room = $this->getRoom($id);
        $pronoun = trans_choice('models.pronouns.room', 1);
        $this->validationRules['updated_at'] = 'required|date_format:Y-m-d H:i:s';
        $this->validateRoom($request, $this->validationRules);
        // Checar que la residencia no haya sido modificado previamente
        if ($room->updated_at->greaterThan(new Carbon($request->get('updated_at')))) {
            $description = trans('models.responses.conflict', ['model' => $pronoun]);
            throw new ConflictHttpException($description);
        }
        // Actualizar datos del cuarto
        $room->update([
            'residence_id' => $request->get('residence_id'),
            'name' => $request->get('name'),
            'beds' => $request->get('beds'),
        ]);

        $room->housingRooms()->sync($request->get('housing_rooms'));

        $this->addBlockedDates($room, $request);
        $this->deleteBlockedDates($room, $request);

        $description = trans('models.responses.updated', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    /**
     * Remove the specified room from storage.
     *
     * @param  int  $id The room id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $room = $this->getRoom($id);
        $pronoun = trans_choice('models.pronouns.room', 1);
        // Checar si hay clientes que tengan asignado el cuarto a eliminar
        $count = DB::table('command_housing')
                ->where('room_id', $room->id)
                ->whereNull('deleted_at')
                ->count();
        if ($count > 0) {
            $description = trans('models.responses.not_deleted', ['model' => $pronoun, 'related' => 'command housing']);
            throw new DeleteResourceFailedException($description);
        }
        // Borrar las relaciones con housing rooms
        $room->housingRooms()->detach();
        // Borrar cuarto
        $room->delete();
        $description = trans('models.responses.deleted', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    /**
     * Get the accommodations list
     *
     * @return \Illuminate\Http\Response
     */
    public function showAccommodations(Request $request)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $commands = \App\Command::with(['commandHousing' => function($query) use($request) {
                    $this->addConstrainsToQuery($query, $request);
                },
                'commandHousing.room.residence', 'client'])
                ->whereHas('commandHousing', function($query) use($request) {
                    $this->addConstrainsToQuery($query, $request);
                })
                ->get();
        $accommodations = [];
        foreach ($commands as $com) {
            foreach ($com->commandHousing as $ch) {
                if (!empty($ch->room)) {
                    $accommodations[] = [
                        'command_id' => $com->id,
                        'command_housing_id' => $ch->id,
                        'room_id' => $ch->room->id,
                        'room' => $ch->room->name,
                        'room_beds' => $ch->room->beds,
                        'housing_rooms' => $ch->room->housingRooms->pluck('name')->toArray(),
                        'residence_id' => $ch->room->residence->id,
                        'residence' => $ch->room->residence->name,
                        'booking_code' => $com->booking_code,
                        /*'client_id' => $com->client->id,
                        'client' => $com->client->getFullName(),
                        //'agency_id' => $com->agency->id,
                        //'agency' => $com->agency->name,*/
                        'text' => !empty($com->client) ? $com->client->getFullName() :
                        $com->client_first_name . ' ' . $com->client_last_name,
                        'housing_room_name' => $ch->housingRoom->name,
                        'start' => (new Carbon($ch->from_date))->format('Y-m-d'),
                        'end' => (new Carbon($ch->to_date))->format('Y-m-d'),
                        'people' => $ch->people
                    ];
                }
            }
        }
        $response = ['data' => $accommodations, 'status_code' => 200];
        return $this->responseOk($response);
    }

    private function addConstrainsToQuery($query, Request $request) {
        $query->whereNotNull('room_id');
        if (!empty($request->get('monthYear'))) {
            $minDate = explode('-', $request->get('monthYear'));
            $minDate = $minDate[1] . '-' . $minDate[0] . '-01';
            $maxDate = (new Carbon($minDate))->lastOfMonth()->format('Y-m-d');
            $query->where(function($query0) use($minDate, $maxDate) {
                $query0->where(function($query1) use($minDate, $maxDate) {
                        $query1->where('from_date', '>=', $minDate)
                            ->where('from_date', '<=', $maxDate);
                    })
                    ->orWhere(function($query1) use($minDate, $maxDate) {
                        $query1->where('to_date', '<=', $maxDate)
                            ->where('to_date', '>=', $minDate);
                    })
                    ->orWhere(function($query1) use($minDate, $maxDate) {
                        $query1->where('from_date', '<', $minDate)
                            ->where('to_date', '>', $maxDate);
                    });
            });
        }
    }
}
