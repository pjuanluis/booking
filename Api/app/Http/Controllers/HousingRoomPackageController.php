<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use App\Package;
use Carbon\Carbon;
use App\HousingRoom;
use App\HousingRoomPackage;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use Illuminate\Validation\Rule;
use App\Transformers\PackageTransformer;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class HousingRoomPackageController extends Controller
{
    use Helpers;

    protected $validationRules = [
        'name' => 'required|string|max:255',
        'description' => 'required|string',
        'housing_rates' => 'nullable|array',
        'housing_rates.*.quantity' => 'required_with:housing_rates|integer|min:1',
        'housing_rates.*.from' => 'nullable|date|date_format:Y-m-d',
        'housing_rates.*.to' => 'nullable|date|date_format:Y-m-d|after_or_equal:housing_rates.*.from',
        'housing_rates.*.unit' => 'required_with:housing_rates|string|in:month,week,night,day',
        'housing_rates.*.unit_cost' => 'required_with:housing_rates|numeric|min:1',
    ];

    public function __construct(Request $request)
    {
        $this->setPermissionAndModule($request, 'housing_rooms_options');
    }

    /**
     * Validate the housing room package data
     * @param \Illuminate\Http\Request $request The data send by the user
     * @param array $validationRules Housing room package validation rules
     * @throws StoreResourceFailedException|UpdateResourceFailedException
     */
    private function validateHousingRoomPackage(Request $request, $validationRules)
    {
        $validator = Validator::make($request->all(), $validationRules);
        $pronoun = trans_choice('models.pronouns.housingroompackage', 1);
        // Si falla la validación
        if ($validator->fails()) {
            // Responder con los errores
            if ($request->isMethod('post')) {
                $description = trans('models.responses.not_created', ['model' => $pronoun]);
                throw new StoreResourceFailedException($description, $validator->errors());
            }
            else {
                $description = trans('models.responses.not_updated', ['model' => $pronoun]);
                throw new UpdateResourceFailedException($description, $validator->errors());
            }
        }
    }

    /**
     * Find the Housing room by a given id
     * @param type $id The Housing room id
     * @return \App\ConsumptionCenter
     * @throws NotFoundHttpException
     */
    private function getHousingRoom($id)
    {
        $housingRoom = null;
        $pronoun = trans_choice('models.pronouns.housingroom', 1);
        try {
            $housingRoom = HousingRoom::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $description = trans('models.responses.not_found', ['model' => $pronoun]);
            throw new NotFoundHttpException($description);
        }
        return $housingRoom;
    }

    /**
     * Find the package by a given id
     * @param type $id The package id
     * @return \App\ConsumptionCenter
     * @throws NotFoundHttpException
     */
    private function getPackage($id, $packages)
    {
        $package = null;
        $pronoun = trans_choice('models.pronouns.package', 1);
        try {
            $package = $packages->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $description = trans('models.responses.not_found', ['model' => $pronoun]);
            throw new NotFoundHttpException($description);
        }
        return $package;
    }

    /**
     * Find the housing room package by a given id
     * @param type $id The housing room package id
     * @return \App\HousingRoomPackage
     * @throws NotFoundHttpException
     */
    private function getHousingRoomPackage($id)
    {
        $housingRoomPackage = null;
        $pronoun = trans_choice('models.pronouns.housingroompackage', 1);
        try {
            $housingRoomPackage = HousingRoomPackage::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $description = trans('models.responses.not_found', ['model' => $pronoun]);
            throw new NotFoundHttpException($description);
        }
        return $housingRoomPackage;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $housingRoomId)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $housingRoom = $this->getHousingRoom($housingRoomId);

        $this->validateHousingRoomPackage($request, $this->validationRules);

        // Crear el paquete
        $package = Package::create([
            'type' => 'housing',
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'unit' => 'pack',
            'quantity' => 1,
            'cost' => 0,
        ]);

        // Crear el housing room package
        $housingRoomPackages = HousingRoomPackage::create([
            'package_id' => $package->id,
            'housing_room_id' => $housingRoom->id,
            'is_active' => 1
        ]);

        $housingRates = $request->get('housing_rates', []);

        if (!empty($housingRates)) {
            // Guardar las tarifas
            foreach ($housingRates as $key => $rate) {
                $rate['from_date'] = !empty($rate['from'])? $rate['from'] : null;
                $rate['to_date'] = !empty($rate['to'])? $rate['to'] : null;
                $housingRoomPackages->housingRates()->create($rate);
            }
        }

        $pronoun = trans_choice('models.pronouns.housingroompackage', 1);
        $description = trans('models.responses.created', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 201];
        return $this->response()->created(null, $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $housingRoomPackages = $this->getHousingRoomPackage($id);
        $housingRoom = $housingRoomPackages->housingRoom;
        $package = $this->getPackage($housingRoomPackages->package_id, $housingRoom->packages());
        $packageTransformer = new PackageTransformer();
        $packageTransformer->setDefaultIncludes(['rates']);
        return $this->response->item($package, $packageTransformer, [], function($resource, $fractal) {
            $include = '';
            if (isset($_GET['include'])) {
                $include = $_GET['include'];
            }
            $fractal->parseIncludes($include);
        });
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $housingRoomPackage = $this->getHousingRoomPackage($id);

        $pronoun = trans_choice('models.pronouns.housingroompackage', 1);

        $this->validationRules['updated_at'] = 'required|date|date_format:Y-m-d H:i:s';

        $this->validateHousingRoomPackage($request, $this->validationRules);

        // Checar que el Housing room package no haya sido modificado previamente
        if ($housingRoomPackage->updated_at->greaterThan(new Carbon($request->get('updated_at')))) {
            $description = trans('models.responses.conflict', ['model' => $pronoun]);
            throw new ConflictHttpException($description);
        }

        // Actualizar información del paquete
        $housingRoomPackage->package()->update($request->only('name', 'description'));

        // Actualizar las tarifas
        $housingRates = $request->get('housing_rates', []);

        // Eliminar tarifas existentes
        $housingRoomPackage->housingRates()->delete();
        if (!empty($housingRates)) {
            // Crear las tarifas
            foreach ($housingRates as $key => $rate) {
                $rate['from_date'] = !empty($rate['from'])? $rate['from'] : null;
                $rate['to_date'] = !empty($rate['to'])? $rate['to'] : null;
                $housingRoomPackage->housingRates()->create($rate);
            }
        }

        $description = trans('models.responses.updated', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $housingRoomPackage = $this->getHousingRoomPackage($id);
        $pronoun = trans_choice('models.pronouns.housingroompackage', 1);

        // Checar si hay referencias activas al package a borrar
        $referencedTables = $housingRoomPackage->package->activeReferencedTables('id', [], ['housing_room_packages']);
        $related = null;
        if (count($referencedTables) > 0) {
            $related = \App\BaseModel::parseTableToModelName($referencedTables[0]);
            $related = $related === null ? studly_case($referencedTables[0]) :
                    trans_choice('models.pronouns.' . strtolower($related), 2);
            $description = trans('models.responses.not_deleted', [
                'model' => $pronoun,
                'related' => $related,
            ]);
            throw new DeleteResourceFailedException($description);
        }
        // Checar si hay referencias activas al housingRoomPackage a borrar
        $referencedTables = $housingRoomPackage->activeReferencedTables('id');
        $related = null;
        if (count($referencedTables) > 0) {
            $related = \App\BaseModel::parseTableToModelName($referencedTables[0]);
            $related = $related === null ? studly_case($referencedTables[0]) :
                    trans_choice('models.pronouns.' . strtolower($related), 2);
            $description = trans('models.responses.not_deleted', [
                'model' => $pronoun,
                'related' => $related,
            ]);
            throw new DeleteResourceFailedException($description);
        }

        // Eliminar el paquete
        $housingRoomPackage->package()->delete();

        // Eliminar tarifas en caso de que haya
        $housingRoomPackage->housingRates()->delete();

        // Eliminar el housing room package
        $housingRoomPackage->delete();

        $description = trans('models.responses.deleted', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    /**
     * Activate / deactivate the specified housing room package
     *
     * @param  int  $id The housing room package id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $housingRoomPackage = $this->getHousingRoomPackage($id);
        $pronoun = trans_choice('models.pronouns.housingroompackage', 1);
        $isActive = $housingRoomPackage->is_active == 1 ? 0 : 1;
        $key = $isActive === 1 ? 'activated' : 'deactivated';
        $housingRoomPackage->update(['is_active' => $isActive]);
        $description = trans("models.responses.$key", ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }
}
