<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use Validator;
use \Carbon\Carbon;
use App\Purchasable;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use App\Transformers\UserTransformer;
use Illuminate\Database\QueryException;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class UserController extends Controller
{
    use Helpers;

    use Helpers;

    protected $validationRules = [
        'first_name' => 'required',
        'last_name' => 'required',
        'email' => 'required|email|unique:users,email',
        'password' => 'required|min:6',
        'role_id' => 'required|exists:roles,id'
    ];

    public function __construct(Request $request)
    {
        $this->setPermissionAndModule($request, 'users');
    }

    /**
     * Return a listing of users.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, UserTransformer $userTransformer)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        // Verificar bandera para no paginar los resultados
        $paginate = true;
        if ($request->get('no-paginate', '0') == 1) {
            $paginate = false;
        }
        $result = User::orderBy('last_name')->orderBy('first_name');

        // Si el usuario logueado no es administrador, solo puede ver su información
        if (!empty($user->role) && $user->role->slug != "administrator") {
            $result = $result->where('id', $user->id);
        }

        // Filtrar por nombre
        if (!empty($request->get('name'))) {
            $result = $result->where(DB::raw("(first_name || ' ' || last_name)"), 'ilike', $request->get('name') . '%');
        }
        // Filtrar por email
        if (!empty($request->get('email'))) {
            $result = $result->where('email', 'ilike', $request->get('email') . '%');
        }
        // Paginar registros
        if ($paginate) {
            $result = $result->paginate(20);
            return $this->response->paginator($result, $userTransformer);
        }
        // No paginar, retornar todos los registros encontrados
        else {
            $result = $result->get();
            return $this->response->collection($result, $userTransformer);
        }
    }

    /**
     * Validate the user data
     * @param \Illuminate\Http\Request $request The data send by the user
     * @param array $validationRules User validation rules
     * @throws StoreResourceFailedException|UpdateResourceFailedException
     */
    private function validateUser(Request $request, array $validationRules)
    {
        $validator = Validator::make($request->all(), $validationRules);
        $pronoun = trans_choice('models.pronouns.user', 1);
        // Si falla la validación
        if ($validator->fails()) {
            // Responder con los errores
            if ($request->isMethod('post')) {
                $description = trans('models.responses.not_created', ['model' => $pronoun]);
                throw new StoreResourceFailedException($description, $validator->errors());
            }
            else {
                $description = trans('models.responses.not_updated', ['model' => $pronoun]);
                throw new UpdateResourceFailedException($description, $validator->errors());
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $pronoun = trans_choice('models.pronouns.user', 1);
        try {
            $this->validationRules['password'] = 'required|min:8';
            $this->validateUser($request, $this->validationRules);

            $user = User::create([
                'first_name' => $request->get('first_name'),
                'last_name' => $request->get('last_name'),
                'email' => $request->get('email'),
                'username' => $request->get('username'),
                'password' => bcrypt($request->get('password')),
                'role_id' => $request->get('role_id'),
            ]);
            $description = trans('models.responses.created', ['model' => $pronoun]);
            $response = ['message' => $description, 'status_code' => 201];
            return $this->response()->created(null, $response);
        } catch(QueryException $e) {
            $description = trans('models.responses.already_exists', ['model' => $pronoun]);
            throw new UnprocessableEntityHttpException($description);
        }
    }

    /**
     * Get the specified user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request, UserTransformer $userTransformer)
    {
        // Checar permiso
        $user = Auth::user();
        $pronoun = trans_choice('models.pronouns.user', 1);
        $this->checkPermission($user, $this->module, $this->permission);
        // Si el usuario logueado no es administrador, solo puede ver su información
        if (!empty($user->role) && $user->role->slug != "administrator") {
            if ($id != $user->id) {
                throw new AccessDeniedHttpException(trans('models.responses.not_allowed', [
                    'model' => $pronoun
                ]));
            }
        }

        $user = User::where('id', $id);
        if (!empty($request->get('remember_token'))) {
            $user = $user->where('remember_token', $request->get('remember_token'));
        }
        try {
            $user = $user->firstOrFail();
        } catch (ModelNotFoundException $e) {
            $description = trans('models.responses.not_found', ['model' => $pronoun]);
            throw new NotFoundHttpException($description);
        }
        return $this->response->item($user, $userTransformer, [], function($resource, $fractal) {
            $include = '';
            if (isset($_GET['include'])) {
                $include = $_GET['include'];
            }
            $fractal->parseIncludes($include);
        });
    }

    /**
     * Get the specified user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showUser($id, Request $request, UserTransformer $userTransformer)
    {
        $user = User::where('id', $id);
        if (!empty($request->get('remember_token'))) {
            $user = $user->where('remember_token', $request->get('remember_token'));
        }
        try {
            $user = $user->firstOrFail();
            $pronoun = trans_choice('models.pronouns.user', 1);
        } catch (ModelNotFoundException $e) {
            $description = trans('models.responses.not_found', ['model' => $pronoun]);
            throw new NotFoundHttpException($description);
        }
        return $this->response->item($user, $userTransformer, [], function($resource, $fractal) {
            $include = '';
            if (isset($_GET['include'])) {
                $include = $_GET['include'];
            }
            $fractal->parseIncludes($include);
        });
    }

    /**
     * Get the logged-in user.
     *
     * @return \Illuminate\Http\Response
     */
    public function me(UserTransformer $userTransformer)
    {
        $user = Auth::user();
        return $this->response->item($user, $userTransformer, [], function($resource, $fractal) {
            $include = '';
            if (isset($_GET['include'])) {
                $include = $_GET['include'];
            }
            $fractal->parseIncludes($include);
        });
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $pronoun = trans_choice('models.pronouns.user', 1);

        // Si el usuario logueado no es administrador, solo puede actualizar su información
        if (!empty($user->role) && $user->role->slug != "administrator") {
            if ($id != $user->id) {
                throw new AccessDeniedHttpException(trans('models.responses.not_allowed', [
                    'model' => $pronoun
                ]));
            }
        }

        $user = User::findOrFail($id);

        $this->validationRules['updated_at'] = 'required|date_format:Y-m-d H:i:s';
        $this->validationRules['email'] = [
            'required',
            'email',
            Rule::unique('users')->ignore($user->id),
        ];
        $this->validationRules['password'] = 'nullable|min:8';
        $this->validateUser($request, $this->validationRules);

         // Checar que el usuario no haya sido modificado previamente
         if ($user->updated_at->notEqualTo(new Carbon($request->get('updated_at')))) {
            // Borrar archivo
            if ($filePath !== null) {
                unlink($this->getStoragePublicDir() . $filePath);
            }
            $description = trans('models.responses.conflict', ['model' => $pronoun]);
            throw new ConflictHttpException($description);
        }

        // Actualizar usuario
        $data = [
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'email' => $request->get('email'),
            'role_id' => $request->get('role_id'),
        ];
        if (!empty($request->get('password'))) {
            $data['password'] = bcrypt($request->get('password'));
        }
        try {
            $user->update($data);
        } catch(QueryException $e) {
            $description = trans('models.responses.already_exists', ['model' => $pronoun]);
            throw new UnprocessableEntityHttpException($description);
        }

        $description = trans('models.responses.updated', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $pronoun = trans_choice('models.pronouns.user', 1);
        // Un usuario no se puede eliminar así mismo
        if ($user->id == $id) {
            $description = 'Could not delete User, the User can not remove himself.';
            throw new DeleteResourceFailedException($description);
        }

        $user = User::findOrFail($id);

        // Validar que el usuario a eliminar no sea responsable de una comanda
        $purchases = Purchasable::where('purchasable_type', 'App\Command')
            ->where('user_id', $user->id);
        if ($purchases->exists()) {
            // El usario es responsable de una o más comandas, por lo tanto no se puede eliminar
            $description = 'Could not delete User, the User is responsible for one or more bookings.';
            throw new DeleteResourceFailedException($description);
        }

        // Antes de eliminar el usuario hay que cambiar el email para que ese email pueda ser reutilizado con nuevos usuarios, ya que este campo tiene la restricción de ser único.
        $user->email = $user->email . "-" . Carbon::now()->format('Y-m-d H:i:s');
        $user->update();

        // Borrar usuario
        $user->delete();
        $description = trans('models.responses.deleted', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    /**
     * Update the remember token of the specified user in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id The user id
     * @return \Illuminate\Http\Response
     */
    public function updateRememberToken(Request $request, $id)
    {
        $user = User::find($id);
        $user->update([
            'remember_token' => $request->get('remember_token')
        ]);
    }
}
