<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use App\Housing;
use Carbon\Carbon;
use App\HousingRoom;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Transformers\HousingRoomTransformer;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class HousingRoomController extends Controller
{
    use Helpers;

    protected $validationRules = [
        'room_type_id' => 'required|integer|exists:room_types,id',
        'name' => 'required|string|max:255',
        'description' => 'nullable|string',
        'beds' => 'required|integer|min:1',
        'cost_type' => 'required|string|in:room,bed',
    ];

    public function __construct(Request $request)
    {
        $this->setPermissionAndModule($request, 'housing_rooms');
    }

    /**
     * Validate the housing room data
     * @param \Illuminate\Http\Request $request The data send by the user
     * @param array $validationRules housing room validation rules
     * @throws StoreResourceFailedException|UpdateResourceFailedException
     */
    private function validateHousingRoom(Request $request, $validationRules)
    {
        $validator = Validator::make($request->all(), $validationRules);
        $pronoun = trans_choice('models.pronouns.housingroom', 1);
        // Si falla la validación
        if ($validator->fails()) {
            // Responder con los errores
            if ($request->isMethod('post')) {
                $description = trans('models.responses.not_created', ['model' => $pronoun]);
                throw new StoreResourceFailedException($description, $validator->errors());
            }
            else {
                $description = trans('models.responses.not_updated', ['model' => $pronoun]);
                throw new UpdateResourceFailedException($description, $validator->errors());
            }
        }
    }

    /**
     * Find the housing by a given id
     * @param type $id The housing id
     * @return \App\Housing
     * @throws NotFoundHttpException
     */
    private function getHousing($id)
    {
        $housing = null;
        $pronoun = trans_choice('models.pronouns.housing', 1);
        try {
            $housing = Housing::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $description = trans('models.responses.not_found', ['model' => $pronoun]);
            throw new NotFoundHttpException($description);
        }
        return $housing;
    }

    /**
     * Find the Housing room by a given id
     * @param type $id The Housing room id
     * @return \App\ConsumptionCenter
     * @throws NotFoundHttpException
     */
    private function getHousingRoom($id)
    {
        $housingRoom = null;
        $pronoun = trans_choice('models.pronouns.housingroom', 1);
        try {
            $housingRoom = HousingRoom::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $description = trans('models.responses.not_found', ['model' => $pronoun]);
            throw new NotFoundHttpException($description);
        }
        return $housingRoom;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $housingId)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $housing = $this->getHousing($housingId);

        $this->validateHousingRoom($request, $this->validationRules);

        // Crear el la habitación del hospedaje
        $housing->housingRooms()->create($request->all());

        $pronoun = trans_choice('models.pronouns.housingroom', 1);
        $description = trans('models.responses.created', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 201];
        return $this->response()->created(null, $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $housingRoom = $this->getHousingRoom($id);
        return $this->response->item($housingRoom, new HousingRoomTransformer(), [], function($resource, $fractal) {
            $include = '';
            if (isset($_GET['include'])) {
                $include = $_GET['include'];
            }
            $fractal->parseIncludes($include);
        });
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $housingRoom = $this->getHousingRoom($id);

        $pronoun = trans_choice('models.pronouns.housingroom', 1);

        $this->validationRules['updated_at'] = 'required|date|date_format:Y-m-d H:i:s';

        $this->validateHousingRoom($request, $this->validationRules);

        // Checar que el Housing room no haya sido modificado previamente
        if ($housingRoom->updated_at->greaterThan(new Carbon($request->get('updated_at')))) {
            $description = trans('models.responses.conflict', ['model' => $pronoun]);
            throw new ConflictHttpException($description);
        }

        // Actualizar datos del Housing room
        $housingRoom->update($request->all());

        $description = trans('models.responses.updated', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $housingRoom = $this->getHousingRoom($id);
        $pronoun = trans_choice('models.pronouns.housingroom', 1);
        // Checar si hay referencias activas al housing room a borrar
        $referencedTables = $housingRoom->activeReferencedTables('id');
        $related = null;
        if (count($referencedTables) > 0) {
            $related = \App\BaseModel::parseTableToModelName($referencedTables[0]);
            $related = $related === null ? studly_case($referencedTables[0]) :
                    trans_choice('models.pronouns.' . strtolower($related), 2);
            $description = trans('models.responses.not_deleted', [
                'model' => $pronoun,
                'related' => $related,
            ]);
            throw new DeleteResourceFailedException($description);
        }

        // Eliminar el housing room
        $housingRoom->delete();

        $description = trans('models.responses.deleted', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    /**
     * Activate / deactivate the specified housing room.
     *
     * @param  int  $id The housing room id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);
        
        $housingRoom = $this->getHousingRoom($id);
        $pronoun = trans_choice('models.pronouns.housingroom', 1);
        $isActive = $housingRoom->is_active == 1 ? 0 : 1;
        $key = $isActive === 1 ? 'activated' : 'deactivated';
        $housingRoom->update(['is_active' => $isActive]);
        $description = trans("models.responses.$key", ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }
}
