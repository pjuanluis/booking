<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use Carbon\Carbon;
use App\Experience;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use League\Fractal\Resource\Collection;
use App\Transformers\ExperienceTransformer;
use Dingo\Api\Exception\ResourceException;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ExperienceController extends Controller
{
    use Helpers;

    protected $validationRules = [
        'description' => 'nullable|string',
        'name' => 'required|string|max:255|unique:experiences,slug',
    ];

    public function __construct(Request $request)
    {
        $this->setPermissionAndModule($request, 'experiences');
    }

    /**
     * Return a listing of courses.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, ExperienceTransformer $experienceTransformer)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        // Obtener cursos
        $result = Experience::orderBy('name');
        // Filtrar por nombre
        if (!empty($request->get('name'))) {
            $result = $result->where('name', 'ilike', $request->get('name') . '%');
        }
        $result = $result->get();
        return $this->response->collection($result, $experienceTransformer);
    }

    /**
     * Validate the experience data
     * @param \Illuminate\Http\Request $request The data send by the user
     * @param array $validationRules Experience validation rules
     * @throws StoreResourceFailedException|UpdateResourceFailedException
     */
    private function validateExperience(Request $request, $validationRules, $data)
    {
        $validator = Validator::make($data, $validationRules);
        $pronoun = trans_choice('models.pronouns.experience', 1);
        // Si falla la validación
        if ($validator->fails()) {
            // Responder con los errores
            if ($request->isMethod('post')) {
                $description = trans('models.responses.not_created', ['model' => $pronoun]);
                throw new StoreResourceFailedException($description, $validator->errors());
            }
            else {
                $description = trans('models.responses.not_updated', ['model' => $pronoun]);
                throw new UpdateResourceFailedException($description, $validator->errors());
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $data = $request->all();
        // Crear el slug, por cuestiones de validación se guarda como si fuera el nombre
        $data['name'] = str_slug($request->get('name', ''), '_');

        $this->validateExperience($request, $this->validationRules, $data);

        // Crear la experiencia
        Experience::create([
            'name' => $request->get('name'),
            'slug' => $data['name'],
            'description' => $request->get('description', null),
            'is_active' => 1
        ]);

        $pronoun = trans_choice('models.pronouns.experience', 1);
        $description = trans('models.responses.created', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 201];
        return $this->response()->created(null, $response);
    }

    /**
     * Get the specified course.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, ExperienceTransformer $experienceTransformer)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $experience = null;
        $pronoun = trans_choice('models.pronouns.course', 1);
        try {
            $experience = Experience::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $description = trans('models.responses.not_found', ['model' => $pronoun]);
            throw new NotFoundHttpException($description);
        }
        return $this->response->item($experience, $experienceTransformer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
