<?php

namespace App\Http\Controllers;

use Validator;
use App\Package;
use Carbon\Carbon;
use League\Fractal\Manager;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;
use App\Transformers\PackageTransformer;
use Dingo\Api\Exception\ResourceException;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PackageController extends Controller
{
    use Helpers;

    protected $validationRules = [
        'name' => 'required|string|max:255',
        'description' => 'required|string',
        'type' => 'required|string|in:experience,housing',
        'quantity' => 'required|integer|min:1',
        'unit' => 'required|string|in:class,project,pack',
        'cost' => 'required|numeric|min:0',
    ];

    /**
     * Return a listing of courses.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, PackageTransformer $packageTransformer)
    {
        $paginate = true;
        if ($request->get('no-paginate', '0') == 1) {
            $paginate = false;
        }
        // Obtener paquetes
        $result = Package::orderBy('name');
        // Filtrar por nombre
        if (!empty($request->get('name'))) {
            $result = $result->where('name', 'ilike', $request->get('name') . '%');
        }
        // filtrar por estado(activo, no activo)
        if (!empty($request->get('is_active'))) {
            $result = $result->where('is_active', $request->get('is_active'));
        }
        // filtrar por tipo
        if (!empty($request->get('type'))) {
            $result = $result->where('type', $request->get('type'));
        }
        // Filtrar por experiencia
        $tmp = null;
        if (!empty($request->get('experience'))) {
            $tmp = explode(',', $request->get('experience'));
            $result = $result->has('experiences', '=', count($tmp));
            $result = $result->whereHas('experiences', function($query) use($tmp) {
                $query->whereIn('slug', $tmp);
            });
        }
        // Paginar registros
        if ($paginate) {
            $result = $result->paginate(20);
            return $this->response->paginator($result, $packageTransformer);
        }
        // No paginar, retornar todos los registros encontrados
        else {
            $result = $result->get();
            return $this->response->collection($result, $packageTransformer);
        }
    }

    /**
     * Validate the package data
     * @param \Illuminate\Http\Request $request The data send by the user
     * @param array $validationRules Package validation rules
     * @throws StoreResourceFailedException|UpdateResourceFailedException
     */
    private function validatePackage(Request $request, $validationRules)
    {
        $validator = Validator::make($request->all(), $validationRules);
        $pronoun = trans_choice('models.pronouns.package', 1);
        // Si falla la validación
        if ($validator->fails()) {
            // Responder con los errores
            if ($request->isMethod('post')) {
                $description = trans('models.responses.not_created', ['model' => $pronoun]);
                throw new StoreResourceFailedException($description, $validator->errors());
            }
            else {
                $description = trans('models.responses.not_updated', ['model' => $pronoun]);
                throw new UpdateResourceFailedException($description, $validator->errors());
            }
        }
    }

    /**
     * Find the package by a given id
     * @param type $id The package id
     * @return \App\Package
     * @throws NotFoundHttpException
     */
    private function getPackage($id)
    {
        $package = null;
        $pronoun = trans_choice('models.pronouns.package', 1);
        try {
            $package = Package::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $description = trans('models.responses.not_found', ['model' => $pronoun]);
            throw new NotFoundHttpException($description);
        }
        return $package;
    }

    public function groupByExperiences(Request $request)
    {
        $experiences = $this->getExperiences();
        $packages = null;
        $data = [];
        $fractal = new Manager();
        foreach ($experiences as $tmp) {
            $packages = Package::with(['rates', 'experiences'])->orderBy('name');
            if (($request->has('is_active'))) {
                $packages = $packages->where('is_active', $request->get('is_active'));
            }
            $packages = $packages->has('experiences', '=', count($tmp));
            $packages = $packages->whereHas('experiences', function($query) use($tmp) {
                $query->whereIn('slug', $tmp);
            });
            $packages = $packages->get();
            // Deja solamente los paquetes que tengan exactamente las mismas experiencias que $tmp
            $packages = $packages->filter(function($package, $key) use($tmp) {
                return $tmp == $package->experiences->pluck('slug')->toArray();
            });
            $fractal->parseIncludes('rates,experiences');
            $packages = new Collection($packages, new PackageTransformer());
            $data[] = [
                'experience' => $tmp,
                'packages' => current($fractal->createData($packages)->toArray())
            ];
        }
        return response()->json(['data' => $data], 200);
    }

    /**
     * Obtiene todas las posibles combinaciones de experiencias de los paquetes
     */
    private function getExperiences() {
        $packages = Package::with('experiences')
            ->where('type', 'experience')
            ->get();
        $experiences = [];
        $experiencesTmp = [];
        $addExperience;
        foreach ($packages as $package) {
            $addExperience = true;
            $experiencesTmp = $package->experiences->pluck('slug')->toArray();
            // Validar si se deben de agregar o no las experiencias del paquete actual($package)
            foreach ($experiences as $exp) {
                if ($exp == $experiencesTmp) {
                    // Las experiencias del paquete actual ya están dentro de $experiences, por lo tanto ya no se deben de agregar
                    $addExperience = false;
                    break;
                }
            }
            if ($addExperience) {
                $experiences[] = $experiencesTmp;
            }
        }
        return $experiences;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validatePackage($request, $this->validationRules);

        // Crear el package
        Package::create($request->all());

        $pronoun = trans_choice('models.pronouns.package', 1);
        $description = trans('models.responses.created', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 201];
        return $this->response()->created(null, $response);
    }

    /**
     * Get the specified package.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, PackageTransformer $packageTransformer)
    {
        $package = null;
        $pronoun = trans_choice('models.pronouns.package', 1);
        $package = $this->getPackage($id);
        return $this->response->item($package, $packageTransformer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $package = $this->getPackage($id);

        $pronoun = trans_choice('models.pronouns.package', 1);

        $this->validationRules['updated_at'] = 'required|date|date_format:Y-m-d H:i:s';

        $this->validatePackage($request, $this->validationRules);

        // Checar que el package no haya sido modificado previamente
        if ($package->updated_at->greaterThan(new Carbon($request->get('updated_at')))) {
            $description = trans('models.responses.conflict', ['model' => $pronoun]);
            throw new ConflictHttpException($description);
        }

        // Actualizar datos del package
        $package->update($request->all());

        $description = trans('models.responses.updated', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $package = $this->getPackage($id);
        $pronoun = trans_choice('models.pronouns.package', 1);
        // Checar si hay referencias activas al package a borrar
        $referencedTables = $package->activeReferencedTables('id');
        $related = null;
        if (count($referencedTables) > 0) {
            $related = \App\BaseModel::parseTableToModelName($referencedTables[0]);
            $related = $related === null ? studly_case($referencedTables[0]) :
                    trans_choice('models.pronouns.' . strtolower($related), 2);
            $description = trans('models.responses.not_deleted', [
                'model' => $pronoun,
                'related' => $related,
            ]);
            throw new DeleteResourceFailedException($description);
        }

        // Eliminar el package
        $package->delete();

        $description = trans('models.responses.deleted', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    /**
     * Activate / deactivate the specified package.
     *
     * @param  int  $id The package id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        $package = $this->getPackage($id);
        $pronoun = trans_choice('models.pronouns.package', 1);
        $isActive = $package->is_active == 1 ? 0 : 1;
        $key = $isActive === 1 ? 'activated' : 'deactivated';
        $package->update(['is_active' => $isActive]);
        $description = trans("models.responses.$key", ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }
}
