<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Validator;
use Carbon\Carbon;
use App\Residence;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Mail;
use Dingo\Api\Exception\ResourceException;
use App\Transformers\ResidenceTransformer;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ResidenceController extends Controller
{
    use Helpers;

    protected $validationRules = [
        'name' => 'required',
        'address' => 'required',
        'housing_id' => 'required|exists:housing,id',
    ];

    public function __construct(Request $request)
    {
        $this->setPermissionAndModule($request, 'residences');
    }

    /**
     * Return a listing of residences.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, ResidenceTransformer $residenceTransformer)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        // Ordenar por nombre
        $result = Residence::orderBy('name');
        // Filtrar por nombre
        if (!empty($request->get('name'))) {
            $result = $result->where('name', 'ilike', $request->get('name') . '%');
        }
        if (!empty($request->get('housing'))) {
            $result = $result->where('housing_id', $request->get('housing'));
        }
        $result = $result->get();
        return $this->response->collection($result, $residenceTransformer, [], function($resource, $fractal) {
            $exclude = '';
            if (isset($_GET['exclude'])) {
                $exclude = $_GET['exclude'];
            }
            $include = '';
            if (isset($_GET['include'])) {
                $include .= ',' . $_GET['include'];
            }
            $fractal->parseExcludes($exclude);
            $fractal->parseIncludes($include);
        });
    }

    /**
     * Find the residence by a given id
     * @param type $id The residence id
     * @return \App\Residence
     * @throws NotFoundHttpException
     */
    private function getResidence($id)
    {
        $residence = null;
        $pronoun = trans_choice('models.pronouns.residence', 1);
        try {
            $residence = Residence::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $description = trans('models.responses.not_found', ['model' => $pronoun]);
            throw new NotFoundHttpException($description);
        }
        return $residence;
    }

    /**
     * Get the specified residence.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, ResidenceTransformer $residenceTransformer)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $residence = $this->getResidence($id);
        return $this->response->item($residence, $residenceTransformer, [], function($resource, $fractal) {
            $include = '';
            if (isset($_GET['include'])) {
                $include = $_GET['include'];
            }
            $fractal->parseIncludes($include);
        });
    }

    /**
     * Validate the residence data
     * @param \Illuminate\Http\Request $request The data send by the user
     * @param array $validationRules Residence validation rules
     * @throws StoreResourceFailedException|UpdateResourceFailedException
     */
    private function validateResidence(Request $request, $validationRules)
    {
        $validator = Validator::make($request->all(), $validationRules);
        $validator->after(function ($validator) {

        });
        $pronoun = trans_choice('models.pronouns.residence', 1);
        // Si falla la validación
        if ($validator->fails()) {
            // Responder con los errores
            if ($request->isMethod('post')) {
                $description = trans('models.responses.not_created', ['model' => $pronoun]);
                throw new StoreResourceFailedException($description, $validator->errors());
            }
            else {
                $description = trans('models.responses.not_updated', ['model' => $pronoun]);
                throw new UpdateResourceFailedException($description, $validator->errors());
            }
        }
    }

    /**
     * Store a newly created residence in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $this->validateResidence($request, $this->validationRules);
        Residence::create([
            'housing_id' => $request->get('housing_id'),
            'name' => $request->get('name'),
            'address' => $request->get('address'),
        ]);
        $pronoun = trans_choice('models.pronouns.residence', 1);
        $description = trans('models.responses.created', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 201];
        return $this->response()->created(null, $response);
    }

    /**
     * Update the specified residence in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id The residence id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $residence = $this->getResidence($id);
        $pronoun = trans_choice('models.pronouns.residence', 1);
        $this->validationRules['updated_at'] = 'required|date_format:Y-m-d H:i:s';
        $this->validateResidence($request, $this->validationRules);
        // Checar que la residencia no haya sido modificado previamente
        if ($residence->updated_at->greaterThan(new Carbon($request->get('updated_at')))) {
            $description = trans('models.responses.conflict', ['model' => $pronoun]);
            throw new ConflictHttpException($description);
        }
        // Actualizar datos de la residencia
        $residence->update([
            'housing_id' => $request->get('housing_id'),
            'name' => $request->get('name'),
            'address' => $request->get('address'),
        ]);
        $description = trans('models.responses.updated', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    /**
     * Remove the specified residence from storage.
     *
     * @param  int  $id The residence id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $residence = $this->getResidence($id);
        $pronoun = trans_choice('models.pronouns.residence', 1);
        // Checar si hay cuartos que pertenezcan a la residencia a eliminar
        $count = DB::table('rooms')
                ->where('residence_id', $residence->id)
                ->whereNull('deleted_at')
                ->count();
        if ($count > 0) {
            $description = trans('models.responses.not_deleted', ['model' => $pronoun, 'related' => 'Rooms']);
            throw new DeleteResourceFailedException($description);
        }
        // Borrar residencia
        $residence->delete();
        $description = trans('models.responses.deleted', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }
}
