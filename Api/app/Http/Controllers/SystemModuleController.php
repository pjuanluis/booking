<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SystemModule;
use Dingo\Api\Routing\Helpers;
use App\Transformers\SystemModuleTransformer;

class SystemModuleController extends Controller
{
    use Helpers;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Obtener modulos del sistema
        if ($request->has('order') && $request->has('dir')) {
            $result = SystemModule::orderBy(
                strtolower($request->get('order')),
                strtolower($request->get('dir'))
            );
        } else {
            $result = SystemModule::orderBy('name');
        }
        // Filtrar por nombre
        if (!empty($request->get('name'))) {
            $result = $result->where('name', 'ilike', $request->get('name') . '%');
        }
        if ($request->has('is_active')) {
            $result = $result->where('is_active', $request->get('is_active'));
        }
        $result = $result->get();

        return $this->response->collection($result, new SystemModuleTransformer());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
}
