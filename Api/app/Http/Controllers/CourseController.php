<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use App\Course;
use App\Package;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Transformers\CourseTransformer;
use League\Fractal\Resource\Collection;
use App\Transformers\PackageTransformer;
use Dingo\Api\Exception\ResourceException;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class CourseController extends Controller
{
    use Helpers;

    protected $validationRules = [
        'experiences' => 'required|array',
        'experiences.*' => 'required|integer|exists:experiences,id',
        'name' => 'required|string|max:255',
        'description' => 'required|string',
        'quantity' => 'required|integer|min:1',
        'unit' => 'required|string|in:class,project',
        'cost' => 'required|numeric|min:0',
        'course_rates' => 'nullable|array',
        'course_rates.*.from' => 'required_with:course_rates|integer|min:1',
        'course_rates.*.to' => 'nullable|integer|min:1',
        'course_rates.*.unit_cost' => 'required_with:course_rates|numeric|min:1',
    ];

    public function __construct(Request $request)
    {
        $this->setPermissionAndModule($request, 'courses');
    }

    /**
     * Return a listing of courses.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, PackageTransformer $packageTransformer)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $paginate = true;
        if ($request->get('no-paginate', '0') == 1) {
            $paginate = false;
        }
        // Obtener paquetes
        $result = Package::orderBy('name');
        // Filtrar por nombre
        if (!empty($request->get('name'))) {
            $result = $result->where('name', 'ilike', $request->get('name') . '%');
        }
        // filtrar por estado(activo, no activo)
        if (!empty($request->get('is_active'))) {
            $result = $result->where('is_active', $request->get('is_active'));
        }
        // filtrar por tipo
        if (!empty($request->get('type'))) {
            $result = $result->where('type', $request->get('type'));
        }
        // Filtrar por experiencia
        $tmp = null;
        if (!empty($request->get('experience'))) {
            $tmp = explode(',', $request->get('experience'));
            $result = $result->has('experiences', '=', count($tmp));
            $result = $result->whereHas('experiences', function($query) use($tmp) {
                $query->whereIn('slug', $tmp);
            });
        }
        // Paginar registros
        if ($paginate) {
            $result = $result->paginate(20);
            return $this->response->paginator($result, $packageTransformer);
        }
        // No paginar, retornar todos los registros encontrados
        else {
            $result = $result->get();
            return $this->response->collection($result, $packageTransformer);
        }
    }

    /**
     * Validate the course data
     * @param \Illuminate\Http\Request $request The data send by the user
     * @param array $validationRules Course validation rules
     * @throws StoreResourceFailedException|UpdateResourceFailedException
     */
    private function validateCourse(Request $request, $validationRules)
    {
        $validator = Validator::make($request->all(), $validationRules);
        $pronoun = trans_choice('models.pronouns.course', 1);
        // Si falla la validación
        if ($validator->fails()) {
            // Responder con los errores
            if ($request->isMethod('post')) {
                $description = trans('models.responses.not_created', ['model' => $pronoun]);
                throw new StoreResourceFailedException($description, $validator->errors());
            }
            else {
                $description = trans('models.responses.not_updated', ['model' => $pronoun]);
                throw new UpdateResourceFailedException($description, $validator->errors());
            }
        }
    }

    /**
     * Find the package by a given id
     * @param type $id The package id
     * @return \App\Package
     * @throws NotFoundHttpException
     */
    private function getPackage($id)
    {
        $package = null;
        $pronoun = trans_choice('models.pronouns.course', 1);
        try {
            $package = Package::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $description = trans('models.responses.not_found', ['model' => $pronoun]);
            throw new NotFoundHttpException($description);
        }
        return $package;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $this->validateCourse($request, $this->validationRules);

        // Crear el paquete
        $package = Package::create([
            'type' => 'experience',
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'unit' => $request->get('unit'),
            'quantity' => $request->get('quantity'),
            'cost' => $request->get('cost'),
        ]);

        // Agregar las experiencias
        $experiences = $request->get('experiences');
        foreach ($experiences as $key => $experience_id) {
            $package->experiences()->attach($experience_id);
        }

        // Guardar las tarifas
        $courseRates = $request->get('course_rates');

        foreach ($courseRates as $key => $rate) {
            $rate['quantity'] = !empty($rate['from'])? $rate['from'] : null;
            $rate['to_quantity'] = !empty($rate['to'])? $rate['to'] : null;
            $package->rates()->create($rate);
        }

        $pronoun = trans_choice('models.pronouns.course', 1);
        $description = trans('models.responses.created', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 201];
        return $this->response()->created(null, $response);
    }

    /**
     * Get the specified course.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, PackageTransformer $packageTransformer)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $package = null;
        $pronoun = trans_choice('models.pronouns.course', 1);
        $package = $this->getPackage($id);
        return $this->response->item($package, $packageTransformer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $package = $this->getPackage($id);

        $pronoun = trans_choice('models.pronouns.course', 1);

        $this->validationRules['updated_at'] = 'required|date|date_format:Y-m-d H:i:s';

        $this->validateCourse($request, $this->validationRules);

        // Checar que el package no haya sido modificado previamente
        if ($package->updated_at->greaterThan(new Carbon($request->get('updated_at')))) {
            $description = trans('models.responses.conflict', ['model' => $pronoun]);
            throw new ConflictHttpException($description);
        }

        // Actualizar datos del package
        $package->update([
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'unit' => $request->get('unit'),
            'quantity' => $request->get('quantity'),
            'cost' => $request->get('cost'),
        ]);

        // Actualizar las experiencias
        $experiences = $request->get('experiences');
        $package->experiences()->sync($experiences);

        // Eliminar las tarifas
        $package->rates()->delete();

        // Guardar las tarifas
        $courseRates = $request->get('course_rates');

        foreach ($courseRates as $key => $rate) {
            $rate['quantity'] = !empty($rate['from'])? $rate['from'] : null;
            $rate['to_quantity'] = !empty($rate['to'])? $rate['to'] : null;
            $package->rates()->create($rate);
        }

        $description = trans('models.responses.updated', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $package = $this->getPackage($id);
        $pronoun = trans_choice('models.pronouns.course', 1);

        // Checar si hay referencias activas al package a borrar
        $referencedTables = $package->activeReferencedTables('id', [], ['course_rates', 'package_experiences']);
        $related = null;
        if (count($referencedTables) > 0) {
            $related = \App\BaseModel::parseTableToModelName($referencedTables[0]);
            $related = $related === null ? studly_case($referencedTables[0]) :
                    trans_choice('models.pronouns.' . strtolower($related), 2);
            $description = trans('models.responses.not_deleted', [
                'model' => $pronoun,
                'related' => $related,
            ]);
            throw new DeleteResourceFailedException($description);
        }

        $pronoun = trans_choice('models.pronouns.course', 1);

        // Eliminar experiencias
        $package->experiences()->detach();

        // Eliminar tarifas
        $package->rates()->delete();

        // Eliminar el package
        $package->delete();

        $description = trans('models.responses.deleted', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    /**
     * Activate / deactivate the specified course.
     *
     * @param  int  $id The package id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $package = $this->getPackage($id);
        $pronoun = trans_choice('models.pronouns.course', 1);
        $isActive = $package->is_active == 1 ? 0 : 1;
        $key = $isActive === 1 ? 'activated' : 'deactivated';
        $package->update(['is_active' => $isActive]);
        $description = trans("models.responses.$key", ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }
}
