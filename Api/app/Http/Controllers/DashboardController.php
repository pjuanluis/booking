<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Validator;
use App\Command;
use App\Payment;
use App\Currency;
use App\Purchase;
use Carbon\Carbon;
use League\Fractal;
use App\CommandCourse;
use App\CommandHousing;
use League\Fractal\Manager;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use League\Fractal\Resource\Item;
use Dingo\Api\Exception\ResourceException;
use Illuminate\Database\Eloquent\Collection;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class DashboardController extends Controller
{
    use Helpers;

    protected $months = [
        1 => "January",
        2 => "February",
        3 => "March",
        4 => "April",
        5 => "May",
        6 => "June",
        7 => "July",
        8 => "August",
        9 => "September",
        10 => "October",
        11 => "November",
        12 => "December",
    ];
    private $currencyMxn = null;

    public function __construct(Request $request)
    {
        $this->setPermissionAndModule($request, 'home');
        // Moneda a la cual todas las cantidades van a ser convertidas
        $this->currencyMxn = Currency::with('exchangeRates')
                ->where('code', 'MXN')
                ->first();
    }

    public function index(Request $request)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $onlyCurrentYear = (int)$request->get('only_current_year', 1);

        $dataDashboard = $this->getDashboardInformation($onlyCurrentYear);

        $fractal = new Manager();
        $resource = new Item($dataDashboard, function(array $data) {
            return $data;
        });
        return $fractal->createData($resource)->toArray();
    }

    /**
     * Obtiene la información del año actual que se muestra en el dashboard
     */
    private function getDashboardInformation($onlyCurrentYear) {
        $incomeByMonts = [];
        $dataDashboard = [];
        $incomeLastMont = [];
        $average = 0;
        $bookingsLastMonth = [];
        $now = Carbon::now();
        $from = "{$now->year}-01-01";
        $to = "{$now->year}-12-31";
        $months = $this->divideDateRangeInMonths($from, $to);
        // Obtener los ingresos en cada mes
        $incomeByMonts = $this->getIncomeByMonth($months);
        // Obtener los bookings del mes actual
        $bookingsCurrentMonth = [
            'month' => [
                'name' => $this->months[$now->month],
                'number' => $now->month,
            ],
            'data' => $this->getBookingsInformationByDateRange("{$now->year}-{$now->month}-01", "{$now->year}-{$now->month}-{$now->daysInMonth}")
        ];

        // Calcular la información del último mes y el promedio
        $currentMonth = $now->month;
        $lastMonth = 0;
        if ($currentMonth > 1 ) {
            $lastMonth = $currentMonth - 1;
            // Obtner el ingreso en el útimo mes;
            $incomeLastMont = [
                    'month' => [
                        'name' => $this->months[$lastMonth],
                        'number' => $lastMonth,
                    ],
                    'amount' => $incomeByMonts[$lastMonth]
                ];
            // Calcular el promedio
            $average = $this->calculateAverage($lastMonth, $incomeByMonts);
            $daysInTheLastMonth = (new Carbon("{$now->year}-{$lastMonth}-01"))->daysInMonth;
            $bookingsLastMonth = [
                'month' => [
                    'name' => $this->months[$lastMonth],
                    'number' => $lastMonth,
                ],
                'data' => $this->getBookingsInformationByDateRange("{$now->year}-{$lastMonth}-01", "{$now->year}-{$lastMonth}-{$daysInTheLastMonth}")
            ];
        } elseif ($onlyCurrentYear === 0) {
            $lastMonth = 12; // Diciembre del año anterior al actual
            $lastYear = $now->year - 1;
            $monthsLastYear = $this->divideDateRangeInMonths("{$lastYear}-01-01", "{$lastYear}-12-31");
            $incomeByMontsLastYear = $this->getIncomeByMonth($monthsLastYear);
            $incomeLastMont = [
                    'month' => [
                        'name' => $this->months[$lastMonth],
                        'number' => $lastMonth,
                        'year' => $lastYear
                    ],
                    'amount' => $incomeByMontsLastYear[$lastMonth]
                ];
            // Calcular el promedio
            $average = $this->calculateAverage($lastMonth, $incomeByMontsLastYear);
            $bookingsLastMonth = [
                'month' => [
                    'name' => $this->months[$lastMonth],
                    'number' => $lastMonth,
                    'year' => $lastYear
                ],
                'data' => $this->getBookingsInformationByDateRange("{$lastYear}-{$lastMonth}-01", "{$lastYear}-{$lastMonth}-31")
            ];
            $incomeByMonts = $incomeByMontsLastYear;
        }
        // Formater la información
        foreach ($incomeByMonts as $key => $data) {
            $incomeByMonts[$key] = [
                'month' => [
                    'name' => $this->months[$key],
                    'number' => $key,
                ],
                'amount' => $data
            ];
        }

        return [
                'income' => [
                    'average' => $average,
                    'last_month' => $incomeLastMont,
                    'by_month' => $incomeByMonts,
                ],
                'bookings' => [
                    'current_month' => $bookingsCurrentMonth,
                    'last_month' => $bookingsLastMonth
                ]
            ];
    }

    private function calculateAverage($lastMonth, $incomeByMonts) {
        if ($lastMonth > 1) {
            $total = 0;
            $cont = 0;
            // El promedio se calcula de todos los meses anteriores al último mes($lastMonth)
            for ($i = 1; $i < $lastMonth; $i++) {
                if ($incomeByMonts[$i] !== 0) {
                    $total += $incomeByMonts[$i];
                    $cont++;
                }
            }
            return $cont !== 0? ($total / $cont) : 0;
        }
        return 0;
    }

    private function getIncomeByMonth($months) {
        $incomeByMonts = [];
        foreach ($months as $key => $month) {
            // La posicion del arreglo representa el mes(1,...,12)
            $incomeByMonts[$month['number']] = $this->getIncomeByDateRange($month['startDate'], $month['endDate']);
        }
        return $incomeByMonts;
    }

    private function divideDateRangeInMonths($from, $to)
    {
        $startDate = Carbon::createFromFormat('Y-m-d', $from);
        $endDate = Carbon::createFromFormat('Y-m-d', $to);

        $monthStart = $startDate->month;
        $monthEnd = $endDate->month;

        // sD = startDate
        $sD = '';
        // eD = endDate
        $eD = '';

        $arrayMonths = [];

        for ($i = $monthStart;  $i <= $monthEnd; $i++) {
            $currentDate = new Carbon("{$startDate->year}-{$i}-1");
            if ($i === $monthStart) {
                // Se toma la fecha de inicio selecciona por el usuario
                $sD = $startDate->format('Y-m-d');
            } else {
                // Se crear la fecha de inicio
                $sD = $currentDate->format('Y-m-d');
            }

            if ($i === $monthEnd) {
                // Se toma la fecha de fin selecciona por el usuario
                $eD = $endDate->format('Y-m-d');
            } else {
                // Se crear la fecha de fin
                $eD = "{$endDate->year}-{$i}-{$currentDate->daysInMonth}";
            }

            // $i representa cada mes
            $arrayMonths[] = [
                'number' => $i,
                'startDate' => $sD,
                'endDate' => $eD,
            ];
        }

        return $arrayMonths;
    }

    private function getIncomeByDateRange($startDate, $endDate)
    {
        // Obtener los pagos
        $payments = Payment::with('currency')
            ->whereDate('paid_at', '>=', $startDate)
            ->whereDate('paid_at', '<=', $endDate)
            ->get();

        // Sumar las cantidadades de los pagos
        return $this->sumAmounts($payments);
    }

    private function sumAmounts($payments) {
        $amountInMxn = 0;
        if (!($payments instanceof Collection) || !$this->currencyMxn) {
            return $amountInMxn;
        }
        foreach ($payments as $payment) {
            $amountInMxn += $payment->currency->convertTo($payment->amount, $this->currencyMxn->id, $payment->exchange_rates);
        }
        return $amountInMxn;
    }

    private function getBookingsInformationByDateRange($startDate, $endDate)
    {
        // Obtener las comandas
        $commands = Command::whereDate('created_at', '>=', $startDate)
            ->whereDate('created_at', '<=', $endDate);

        // Obtener los pagos de fee
        $payments = Payment::whereDate('paid_at', '>=', $startDate)
            ->whereDate('paid_at', '<=', $endDate)
            ->whereIn('type', ['registration_fee', 'reservation_payment']);

        $bookingsInformation = [
            'total' => $commands->count(),
            'paid_fee' => $payments->count(),
            'origin' => [
                'online' => 0,
                'agency' => 0,
                'walk_in' => 0
            ]
        ];

        // Encontrar el origen de las comandas
        foreach ($commands->get() as $key => $command) {
            if (strpos($command->booking_code, 'MAN') === 0) {
                // Comandas creadas en el admin
                $bookingsInformation['origin']['walk_in'] += 1;
            } elseif (strpos($command->booking_code, 'WWW') === 0) {
                // Comandas creadas en el booking público
                $bookingsInformation['origin']['online'] += 1;
            } else {
                // Comandas creadas en el admin por una agencia
                $bookingsInformation['origin']['agency'] += 1;
            }
        }

        return $bookingsInformation;
    }
}
