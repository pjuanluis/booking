<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use App\Command;
use App\Purchase;
use Carbon\Carbon;
use League\Fractal;
use App\CommandCourse;
use App\CommandHousing;
use League\Fractal\Manager;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use League\Fractal\Resource\Item;
use Dingo\Api\Exception\ResourceException;

class ReportController extends Controller
{
    use Helpers;

    protected $months = [
        1 => "January",
        2 => "February",
        3 => "March",
        4 => "April",
        5 => "May",
        6 => "June",
        7 => "July",
        8 => "August",
        9 => "September",
        10 => "October",
        11 => "November",
        12 => "December",
    ];

    private $availableReports = ['sales', 'bookings'];

    protected $validationRules = [
        'from' => 'required|date_format:Y-m-d|before_or_equal:to',
        'to' => 'required|date_format:Y-m-d|after_or_equal:from',
    ];

    public function __construct(Request $request)
    {
        $this->setPermissionAndModule($request, 'reports');
    }

    /**
    * Validate the dates
    * @param \Illuminate\Http\Request $request The data send by the user
    * @param array $validationRules validation rules
    * @throws ResourceException
    */
   private function validateData(Request $request, array $validationRules)
   {
       $validator = Validator::make($request->all(), $validationRules);

       // Si falla la validación
       if ($validator->fails()) {
           // Responder con los errores
           $description = "Could not execute the requested action because the sent data is incorrect";
           throw new ResourceException($description, $validator->errors());
       }
   }


    /**
     * Display a listing of the resource.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $report)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        if (!in_array($report, $this->availableReports)) {
            return $this->response->errorNotFound();
        }

        // Validar los datos
        $this->validateData($request, $this->validationRules);

        $from = $request->get('from');
        $to = $request->get('to');

        $dataReport = [];

        switch ($report) {
            case 'sales':
                $dataReport = $this->getSalesReport($request, $from, $to);
                break;
            case 'bookings':
                $dataReport = $this->getBookingsReport($request, $from, $to);
                break;
        }
        $fractal = new Manager();
        $resource = new Item($dataReport, function(array $data) {
            return $data;
        });
        return $fractal->createData($resource)->toArray();
    }

    private function getSalesReport(Request $request, $from, $to)
    {
        $template = [
            [
                'value' => '',
                'type_value' => 'string'
            ],
            [
                'value' => 0,
                'type_value' => 'currency'
            ],
            [
                'value' => 0,
                'type_value' => 'currency'
            ],
            [
                'value' => 'USD',
                'type_value' => 'string'
            ],
            [
                'value' => '',
                'type_value' => 'string'
            ],
            [
                'value' => '',
                'type_value' => 'date'
            ],
        ];

        $report = [
            'titles' => ['Concept', 'Cost', 'Discount(%)', 'Currency', 'Consumption center', 'Date'],
            'data' => []
        ];

        $commands = Command::with(['purchase', 'pickup'])
                            ->whereDate('created_at', '>=', $from)
                            ->whereDate('created_at', '<=', $to);
        $commandCourses = CommandCourse::with('purchase')
                            ->whereDate('created_at', '>=', $from)
                            ->whereDate('created_at', '<=', $to);
        $commandHousing = CommandHousing::with('purchase')
                            ->whereDate('created_at', '>=', $from)
                            ->whereDate('created_at', '<=', $to);
        $purchases = Purchase::with('purchasable')
                            ->whereDate('date', '>=', $from)
                            ->whereDate('date', '<=', $to);

        $filters = [];

        if ($request->get('filter_by', '')) {
            $filters = explode(',', $request->get('filter_by', ''));
        }

        // Filtrar por pickup
        if (in_array('pickup', $filters)) {
            $commands = $commands->has('pickup');
        } elseif ($filters) {
            // Si los filtros no están vaciós es por que se va a filtra por algo diferente a pickup
            $commands = $commands->take(0);
        }

        // Filtrar los cursos por experiencias
        if ($filters) {
            $commandCourses = $commandCourses->whereHas('package.experiences', function($query) use($filters) {
                $query->whereIn('slug', $filters);
            });
        }

        // Filtrar por housing
        if (!in_array('housing', $filters)) {
            if ($filters) {
                // Si los filtros no están vaciós es por que se va a filtra por algo diferente a housing
                $commandHousing = $commandHousing->take(0);
            }
        }

        // Filtrar por compras extras
        if (!in_array('extra_purchases', $filters)) {
            if ($filters) {
                // Si los filtros no están vaciós es por que se va a filtra por algo diferente a compras extras
                $purchases = $purchases->take(0);
            }
        }

        // De las comandas solo se van ha obtener los pickups
        // Formatear los pickup
        foreach ($commands->get() as $key => $command) {
            $pickup = $command->pickup;
            $templateTmp = $template;
            if (!$pickup) {
                continue;
            }
            $templateTmp[0]['value'] = 'Pickup - ' . $pickup->name;
            $templateTmp[1]['value'] = $command->pickup_cost;
            $templateTmp[2]['value'] = $command->purchase->discount;
            $templateTmp[3]['value'] = $command->purchase->currency->code;
            $templateTmp[4]['value'] = $command->purchase->consumptionCenter->name;
            $templateTmp[5]['value'] = $command->created_at->format('Y-m-d H:i:s');
            array_push($report['data'], $templateTmp);
        }

        // Formatear los cursos
        foreach ($commandCourses->get() as $key => $cc) {
            $templateTmp = $template;
            $experiences = implode(',', $cc->package->experiences->pluck('name')->toArray());
            $templateTmp[0]['value'] = "Course - {$cc->quantity}: {$cc->package->name}({$experiences})";
            $templateTmp[1]['value'] = $cc->cost;
            $templateTmp[2]['value'] = $cc->purchase->discount;
            $templateTmp[3]['value'] = $cc->purchase->currency->code;
            $templateTmp[4]['value'] = $cc->purchase->consumptionCenter->name;
            $templateTmp[5]['value'] = $cc->created_at->format('Y-m-d H:i:s');
            array_push($report['data'], $templateTmp);
        }

        // Formatear los cursos
        foreach ($commandHousing->get() as $key => $ch) {
            $templateTmp = $template;
            $templateTmp[0]['value'] = "Housing - {$ch->quantity} {$ch->unit}: {$ch->package->name}({$ch->housing->name} - {$ch->housingRoom->name})";
            $templateTmp[1]['value'] = $ch->cost;
            $templateTmp[2]['value'] = $ch->purchase->discount;
            $templateTmp[3]['value'] = $ch->purchase->currency->code;
            $templateTmp[4]['value'] = $ch->purchase->consumptionCenter->name;
            $templateTmp[5]['value'] = $ch->created_at->format('Y-m-d H:i:s');
            array_push($report['data'], $templateTmp);
        }

        // Formatear las compras extras
        foreach ($purchases->get() as $key => $purchase) {
            $templateTmp = $template;
            $templateTmp[0]['value'] = "Extra purchase - {$purchase->concept}";
            $templateTmp[1]['value'] = $purchase->amount;
            $templateTmp[2]['value'] = $purchase->purchasable->discount;
            $templateTmp[3]['value'] = $purchase->purchasable->currency->code;
            $templateTmp[4]['value'] = $purchase->purchasable->consumptionCenter->name;
            $templateTmp[5]['value'] = $purchase->date->format('Y-m-d H:i:s');
            array_push($report['data'], $templateTmp);
        }

        return $report;
    }

    private function getBookingsReport(Request $request, $from, $to)
    {
        $template = [
            [
                'value' => '',
                'type_value' => 'string'
            ],
            [
                'value' => '',
                'type_value' => 'string'
            ],
            [
                'value' => '',
                'type_value' => 'string'
            ],
            [
                'value' => '',
                'type_value' => 'string'
            ],
            [
                'value' => '',
                'type_value' => 'date'
            ],
            [
                'value' => '',
                'type_value' => 'date'
            ],
        ];

        $report = [
            'titles' => ['Booking reference', 'Client', 'Agency', 'Consumption center', 'From', 'To'],
            'data' => []
        ];

        $commands = Command::with(['purchase', 'pickup', 'agency'])
                            ->whereDate('created_at', '>=', $from)
                            ->whereDate('created_at', '<=', $to);


        foreach ($commands->get() as $key => $command) {
            $templateTmp = $template;
            $templateTmp[0]['value'] = $command->booking_code;
            $templateTmp[1]['value'] = $command->client? $command->client->getFullName() : '';
            $templateTmp[2]['value'] = $command->agency? $command->agency->name : '';
            $templateTmp[3]['value'] = $command->purchase->consumptionCenter->name;
            $templateTmp[4]['value'] = $command->from_date;
            $templateTmp[5]['value'] = $command->to_date;
            array_push($report['data'], $templateTmp);
        }

        return $report;
    }
}
