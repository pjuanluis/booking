<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use App\Person;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Mail;
use App\Transformers\PersonTransformer;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class PersonController extends Controller
{
    use Helpers;

    protected $validationRules = [
        'first_name' => 'required|string|max:255',
        'last_name' => 'required|string|max:255',
        'abbreviation' => 'nullable|string|max:255',
        'email' => 'required|string|max:255|email',
        'position' => 'nullable|string|max:255',
        'type' => 'nullable|string|max:255|in:spanish_teacher,surf_instructor,volunteer_responsible', ];

    public function __construct(Request $request)
    {
        $this->setPermissionAndModule($request, 'people');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, PersonTransformer $personTransformer)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        // Verificar bandera para no paginar los resultados
        $paginate = true;
        if ($request->get('no-paginate', '0') == 1) {
            $paginate = false;
        }
        // Ordenar por nombre
        $result = Person::orderBy('first_name', 'asc')->orderBy('last_name', 'asc');
        // Filtrar por nombre
        if (!empty($request->get('name'))) {
            $result = $result->where(DB::raw("(first_name || ' ' || last_name)"), 'ilike', $request->get('name') . '%');
        }
        if (!empty($request->get('type'))) {
            $result = $result->where('type', $request->get('type'));
        }
        // Paginar registros
        if ($paginate) {
            $result = $result->paginate(20);
            return $this->response->paginator($result, $personTransformer);
        }
        // No paginar, retornar todos los registros encontrados
        else {
            $result = $result->get();
            return $this->response->collection($result, $personTransformer);
        }
    }

     /**
     * Validate the person data
     * @param \Illuminate\Http\Request $request The data send by the user
     * @param array $validationRules Person validation rules
     * @throws StoreResourceFailedException|UpdateResourceFailedException
     */
    private function validatePerson(Request $request, $validationRules)
    {
        $validator = Validator::make($request->all(), $validationRules);
        $pronoun = trans_choice('models.pronouns.person', 1);
        // Si falla la validación
        if ($validator->fails()) {
            // Responder con los errores
            if ($request->isMethod('post')) {
                $description = trans('models.responses.not_created', ['model' => $pronoun]);
                throw new StoreResourceFailedException($description, $validator->errors());
            }
            else {
                $description = trans('models.responses.not_updated', ['model' => $pronoun]);
                throw new UpdateResourceFailedException($description, $validator->errors());
            }
        }
    }

    /**
     * Find the person by a given id
     * @param type $id The person id
     * @return \App\Person
     * @throws NotFoundHttpException
     */
    private function getPerson($id)
    {
        $person = null;
        $pronoun = trans_choice('models.pronouns.person', 1);
        try {
            $person = Person::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $description = trans('models.responses.not_found', ['model' => $pronoun]);
            throw new NotFoundHttpException($description);
        }
        return $person;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $this->validatePerson($request, $this->validationRules);

        // Crear la persona
        Person::create($request->all());

        $pronoun = trans_choice('models.pronouns.person', 1);
        $description = trans('models.responses.created', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 201];
        return $this->response()->created(null, $response);
    }

    /**
     * Get the specified person.
     *
     * @param  int  $id The person id
     * @return \Illuminate\Http\Response
     */
    public function show($id, PersonTransformer $personTransformer)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $person = null;
        $person = $this->getPerson($id);
        return $this->response->item($person, $personTransformer);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $person = $this->getPerson($id);

        $pronoun = trans_choice('models.pronouns.person', 1);

        $this->validationRules['updated_at'] = 'required|date|date_format:Y-m-d H:i:s';

        $this->validatePerson($request, $this->validationRules);

        // Checar que la persona no haya sido modificado previamente
        if ($person->updated_at->greaterThan(new Carbon($request->get('updated_at')))) {
            $description = trans('models.responses.conflict', ['model' => $pronoun]);
            throw new ConflictHttpException($description);
        }

        // Actualizar datos de people
        $person->update($request->all());

        $description = trans('models.responses.updated', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $person = $this->getPerson($id);
        $pronoun = trans_choice('models.pronouns.person', 1);
        // Checar si hay referencias activas al package a borrar
        $referencedTables = $person->activeReferencedTables('id');
        $related = null;
        if (count($referencedTables) > 0) {
            $related = \App\BaseModel::parseTableToModelName($referencedTables[0]);
            $related = $related === null ? studly_case($referencedTables[0]) :
                    trans_choice('models.pronouns.' . strtolower($related), 2);
            $description = trans('models.responses.not_deleted', [
                'model' => $pronoun,
                'related' => $related,
            ]);
            throw new DeleteResourceFailedException($description);
        }

        // Eliminar person
        $person->delete();

        $description = trans('models.responses.deleted', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }
}
