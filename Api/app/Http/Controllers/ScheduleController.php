<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Validator;
use App\Schedule;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use Illuminate\Validation\Rule;
use App\Transformers\ScheduleTransformer;
use Illuminate\Pagination\LengthAwarePaginator;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ScheduleController extends Controller
{
    use Helpers;

    protected $validationRules = [
        'group' => 'required|string|max:255',
        'level' => 'required|integer|min:1',
        'is_private' => 'nullable|boolean',
        'clients' => 'array|required',
        'clients.*' => 'required|integer|exists:clients,id',
        'responsible_id' => 'required|integer|exists:people,id',
        'place_id' => 'required|integer|exists:places,id',
        'subject' => 'required',
        'schedule_time' => 'array|required',
        'schedule_time.*.begin_at' => 'required|date_format:Y-m-d H:i:s',
        'schedule_time.*.finish_at' => 'required|date_format:Y-m-d H:i:s',
        'experiences' => 'array|required',
        'experiences.*' => 'required|integer|exists:experiences,id'
    ];

    public function __construct(Request $request)
    {
        $this->setPermissionAndModule($request, 'schedules');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, ScheduleTransformer $scheduleTransformer)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        // Verificar bandera para no paginar los resultados
        $paginate = true;
        if ($request->get('no-paginate', '0') == 1) {
            $paginate = false;
        }
        // Obtener parametros para ordenar los registros
        $orderBy = $request->get('orderBy', 'begin_at');
        $dir = $request->get('dir', 'asc');

        // Obtener los schedules con sus fechas(ordenadas)
        $result = Schedule::with(['dates' => function($query) use ($orderBy, $dir){
            return $query->orderBy($orderBy, $dir);
        }]);

        // Filtro from
        if (!empty($request->get('from'))) {
            $result = $result->whereHas('dates', function($query) use($request) {
                $query->whereDate('begin_at', '>=', $request->get('from'));
            });
        }
        // Filtro to
        if (!empty($request->get('to'))) {
            $result = $result->whereHas('dates', function($query) use($request) {
                $query->whereDate('begin_at', '<=', $request->get('to'));
            });
        }
        // Filtro experience
        if (!empty($request->get('experience'))) {
            $result = $result->whereHas('experiences', function($query) use($request) {
                $query->where('slug', $request->get('experience'));
            });
        }

        $result = $result->get();

        // Ordenar(asc o desc) los schedules por fecha de inicio
        $result = $result->sort(function($schedule1, $schedule2) use($dir){
            $dateS1 = $schedule1->dates->first();
            $dateS2 = $schedule2->dates->first();
            if ($dateS1 && $dateS2) {
                if ($dateS1->begin_at->eq($dateS2->begin_at)) {
                    return 0;
                }
                if ($dateS1->begin_at->lt($dateS2->begin_at)) {
                    return $dir === 'asc'? -1 : 1;
                }
                return $dir === 'asc'? 1 : -1;
            }
            if ($dateS1) {
                return $dir === 'asc'? -1 : 1;
            }
            if ($dateS2) {
                return $dir === 'asc'? 1 : -1;
            }
        });

        // Paginar registros
        if ($paginate) {
            $page = $request->get('page', 1);
            $result = new LengthAwarePaginator($result->forPage($page, 20), $result->count(), 20, $page);
            return $this->response->paginator($result, $scheduleTransformer);
        }
        // No paginar, retornar todos los registros encontrados
        else {
            return $this->response->collection($result, $scheduleTransformer);
        }
    }

    /**
     * Find the schedule by a given id
     * @param type $id The schedule id
     * @return \App\Schedule
     * @throws NotFoundHttpException
     */
    private function getSchedule($id)
    {
        $schedule = null;
        $pronoun = trans_choice('models.pronouns.schedule', 1);
        try {
            $schedule = Schedule::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $description = trans('models.responses.not_found', ['model' => $pronoun]);
            throw new NotFoundHttpException($description);
        }
        return $schedule;
    }

    /**
     * Validate the schedule data
     * @param \Illuminate\Http\Request $request The data send by the user
     * @param array $validationRules Schedule validation rules
     * @throws StoreResourceFailedException|UpdateResourceFailedException
     */
    private function validateSchedule(Request $request, $validationRules)
    {
        $validator = Validator::make($request->all(), $validationRules);
        $validator->after(function ($validator) use($request) {

        });
        $pronoun = trans_choice('models.pronouns.schedule', 1);
        // Si falla la validación
        if ($validator->fails()) {
            // Responder con los errores
            if ($request->isMethod('post')) {
                $description = trans('models.responses.not_created', ['model' => $pronoun]);
                throw new StoreResourceFailedException($description, $validator->errors());
            }
            else {
                $description = trans('models.responses.not_updated', ['model' => $pronoun]);
                throw new UpdateResourceFailedException($description, $validator->errors());
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $this->validateSchedule($request, $this->validationRules);
        $clients = $request->get('clients', []);
        $isPrivate = count($clients) === 1;
        $schedulableId = null;
        $scheduleTimes = $request->get('schedule_time');
        $levelFinishAt = $scheduleTimes[count($scheduleTimes) - 1]['finish_at'];
        $schedulable = \App\Group::create([
            'name' => $request->get('group'),
            'level' => $request->get('level'),
            'is_private' => $isPrivate ? 1 : 0,
            'level_finish_at' => $levelFinishAt
        ]);
        $schedulableId = $schedulable->id;
        foreach ($clients as $clientId) {
            $schedulable->clients()->attach($clientId);
        }
        $clients = $schedulable->clients()->wherePivot('is_active', 1)->get();
        $experiences = $request->get('experiences', []);
        $schedule = Schedule::create([
            'schedulable_type' => 'App\Group',
            'schedulable_id' => $schedulableId,
            'responsible_id' => $request->get('responsible_id'),
            'place_id' => $request->get('place_id'),
            'subject' => $request->get('subject'),
            'is_private' => $isPrivate ? 1 : 0,
        ]);
        foreach ($experiences as $experienceId) {
            $schedule->experiences()->attach($experienceId);
        }
        $scheduleDate = null;
        foreach ($scheduleTimes as $st) {
            $scheduleDate = $schedule->dates()->create([
                'begin_at' => $st['begin_at'],
                'finish_at' => $st['finish_at'],
            ]);
            foreach ($clients as $c) {
                DB::table('client_classes')->insert([
                    'schedule_date_id' => $scheduleDate->id,
                    'client_id' => $c->id,
                    'taken' => 0
                ]);
            }
        }
        $pronoun = trans_choice('models.pronouns.schedule', 1);
        $description = trans('models.responses.created', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 201];
        return $this->responseOk($response);
    }

    /**
     * Get the specified schedule.
     *
     * @param  int  $id The schedule id
     * @return \Illuminate\Http\Response
     */
    public function show($id, ScheduleTransformer $scheduleTransformer)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $group = $this->getSchedule($id);
        return $this->response->item($group, $scheduleTransformer);
    }

    /**
     * Update the specified schedule in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id The schedule id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $schedule = $this->getSchedule($id);
        $pronoun = trans_choice('models.pronouns.schedule', 1);

        // Solo puede ser actualizado por un usuario administrador
        if (!empty($user->authenticatable_type)) {
            throw new AccessDeniedHttpException(trans('models.responses.not_allowed', [
                'model' => $pronoun
            ]));
        }

        $this->validationRules['updated_at'] = 'required|date_format:Y-m-d H:i:s';
        $this->validateSchedule($request, $this->validationRules);
        // Checar que el horario no haya sido modificado previamente
        if ($schedule->updated_at->greaterThan(new Carbon($request->get('updated_at')))) {
            $description = trans('models.responses.conflict', ['model' => $pronoun]);
            throw new ConflictHttpException($description);
        }

        $clients = $request->get('clients');
        $isPrivate = count($clients) === 1;
        $schedulableId = null;
        $scheduleTimes = $request->get('schedule_time');
        $levelFinishAt = $scheduleTimes[count($scheduleTimes) - 1]['finish_at'];
        $schedulable = $schedule->schedulable;
        $schedulable->update([
            'name' => $request->get('group'),
            'level' => $request->get('level'),
            'is_private' => $isPrivate ? 1 : 0,
            'level_finish_at' => $levelFinishAt
        ]);
        $schedulable->clients()->sync($clients);
        // $schedulableId = $schedulable->id;
        // $groupClients = $schedulable->clients;
        // $exists = false;
        // foreach ($clients as $clientId) {
        //     $exists = false;
        //     foreach ($groupClients as $gc) {
        //         if ($clientId == $gc->id) {
        //             $exists = true;
        //             break;
        //         }
        //     }
        //     if (!$exists) {
        //         $schedulable->clients()->attach($clientId);
        //     }
        // }
        // foreach ($groupClients as $gc) {
        //     $exists = false;
        //     foreach ($clients as $clientId) {
        //         if ($gc->id == $clientId) {
        //             $exists = true;
        //             break;
        //         }
        //     }
        //     if (!$exists) {
        //         $schedulable->clients()->updateExistingPivot($gc->id, ['is_active' => 0]);
        //     }
        // }
        $clients = $schedulable->clients()->wherePivot('is_active', 1)->get();
        $experiences = $request->get('experiences', []);
        $schedule->update([
            'responsible_id' => $request->get('responsible_id'),
            'place_id' => $request->get('place_id'),
            'subject' => $request->get('subject'),
            'is_private' => $isPrivate ? 1 : 0,
        ]);
        // $schedule->experiences()->detach();
        $schedule->experiences()->sync($experiences);
        // foreach ($experiences as $experienceId) {
        // }
        $scheduleDates = $schedule->dates;
        if (!empty($scheduleDates)) {
            foreach ($scheduleDates as $scheduleDate) {
                DB::table('client_classes')->where('schedule_date_id', $scheduleDate->id)->delete();
                $scheduleDate->delete();
            }
        }
        // Borrar datos de asistencia de clases del horario
        // DB::table('client_classes')->where('schedule_id', $schedule->id)->delete();
        // DB::table('schedule_date')->where('schedule_id', $schedule->id)->delete();
        foreach ($scheduleTimes as $st) {
            $scheduleDate = $schedule->dates()->create([
                'begin_at' => $st['begin_at'],
                'finish_at' => $st['finish_at'],
            ]);
            foreach ($clients as $c) {
                DB::table('client_classes')->insert([
                    'schedule_date_id' => $scheduleDate->id,
                    'client_id' => $c->id,
                    'taken' => 0
                ]);
            }
        }

        $description = trans('models.responses.updated', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    /**
     * Remove the specified schedule from storage.
     *
     * @param  int  $id The schedule id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $schedule = $this->getSchedule($id);
        $pronoun = trans_choice('models.pronouns.schedule', 1);

        $schedule->experiences()->detach();
        $scheduleDates = $schedule->dates;
        if (!empty($scheduleDates)) {
            foreach ($scheduleDates as $scheduleDate) {
                DB::table('client_classes')->where('schedule_date_id', $scheduleDate->id)->delete();
                $scheduleDate->delete();
            }
        }
        $schedule->delete();
        $description = trans('models.responses.deleted', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    /**
     * Update the specified schedule date in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id The schedule date id
     * @return \Illuminate\Http\Response
     */
    public function updateClassStatus(Request $request, $id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $pronoun = trans_choice('models.pronouns.client_class', 1);
        $validator = Validator::make($request->all(), [
            'client_id' => 'required|integer|exists:clients,id',
            'taken' => 'nullable|boolean',
            'canceled' => 'nullable|boolean',
        ]);

        // Si el usuario logueado es una cliente, solo puede modificar su clase
        if ($user->authenticatable_type === "App\Client") {
            if ($request->get('client_id') != $user->authenticatable_id) {
                throw new AccessDeniedHttpException(trans('models.responses.not_allowed', [
                    'model' => $pronoun
                ]));
            }
        }

        // Si falla la validación
        if ($validator->fails()) {
            $description = trans('models.responses.not_updated', ['model' => $pronoun]);
            throw new UpdateResourceFailedException($description, $validator->errors());
        }
        $data = [];
        if (isset($request->taken)) {
            $data['taken'] = $request->taken;
        }
        if (isset($request->canceled)) {
            $data['canceled_at'] = $request->canceled == 1 ? Carbon::now() : null;
        }
        if (!empty($data)) {
            DB::table('client_classes')
                    ->where('schedule_date_id', $id)
                    ->where('client_id', $request->get('client_id'))
                    ->update($data);
        }
        $description = trans('models.responses.updated', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }
}
