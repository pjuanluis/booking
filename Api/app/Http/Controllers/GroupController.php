<?php

namespace App\Http\Controllers;

use Auth;
use App\Group;
use Validator;
use App\Schedule;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Mail;
use App\Transformers\GroupTransformer;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class GroupController extends Controller
{
    use Helpers;

    protected $validationRules = [
        'name' => 'required',
        'level' => 'required',
        'is_private' => 'nullable|boolean',
        'clients' => 'array|required',
        'clients.*' => 'required|integer|exists:clients,id'
    ];

    public function __construct(Request $request)
    {
        $this->setPermissionAndModule($request, 'groups');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, GroupTransformer $groupTransformer)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        // Verificar bandera para no paginar los resultados
        $paginate = true;
        if ($request->get('no-paginate', '0') == 1) {
            $paginate = false;
        }
        // Ordenar por nombre
        $result = Group::orderBy('created_at', 'desc')->orderBy('name');
        // Filtrar por nombre
        if (!empty($request->get('name'))) {
            $result = $result->where('name', 'ilike', $request->get('name') . '%');
        }
        if (($request->has('is_private'))) {
            $result = $result->where('is_private', $request->get('is_private'));
        }
        // Paginar registros
        if ($paginate) {
            $result = $result->paginate(20);
            return $this->response->paginator($result, $groupTransformer);
        }
        // No paginar, retornar todos los registros encontrados
        else {
            $result = $result->get();
            return $this->response->collection($result, $groupTransformer);
        }
    }


    /**
     * Find the group by a given id
     * @param type $id The group id
     * @return \App\Group
     * @throws NotFoundHttpException
     */
    private function getGroup($id)
    {
        $group = null;
        $pronoun = trans_choice('models.pronouns.group', 1);
        try {
            $group = Group::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $description = trans('models.responses.not_found', ['model' => $pronoun]);
            throw new NotFoundHttpException($description);
        }
        return $group;
    }

    /**
     * Validate the group data
     * @param \Illuminate\Http\Request $request The data send by the user
     * @param array $validationRules Group validation rules
     * @throws StoreResourceFailedException|UpdateResourceFailedException
     */
    private function validateGroup(Request $request, $validationRules)
    {
        $validator = Validator::make($request->all(), $validationRules);
        $pronoun = trans_choice('models.pronouns.group', 1);
        // Si falla la validación
        if ($validator->fails()) {
            // Responder con los errores
            if ($request->isMethod('post')) {
                $description = trans('models.responses.not_created', ['model' => $pronoun]);
                throw new StoreResourceFailedException($description, $validator->errors());
            }
            else {
                $description = trans('models.responses.not_updated', ['model' => $pronoun]);
                throw new UpdateResourceFailedException($description, $validator->errors());
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $this->validateGroup($request, $this->validationRules);
        $group = Group::create([
            'name' => $request->get('name'),
            'level' => $request->get('level'),
            'is_private' => empty($request->get('is_private')) ? 0 : $request->get('is_private'),
        ]);
        $clients = $request->get('clients', []);
        foreach ($clients as $clientId) {
            $group->clients()->attach($clientId);
        }
        $pronoun = trans_choice('models.pronouns.group', 1);
        $description = trans('models.responses.created', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 201];
        return $this->responseOk($response);
    }

    /**
     * Get the specified group.
     *
     * @param  int  $id The group id
     * @return \Illuminate\Http\Response
     */
    public function show($id, GroupTransformer $groupTransformer)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $group = null;
        $pronoun = trans_choice('models.pronouns.group', 1);
        try {
            $group = Group::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $description = trans('models.responses.not_found', ['model' => $pronoun]);
            throw new NotFoundHttpException($description);
        }
        return $this->response->item($group, $groupTransformer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $group = $this->getGroup($id);
        $pronoun = trans_choice('models.pronouns.group', 1);
        $this->validationRules['updated_at'] = 'required|date_format:Y-m-d H:i:s';
        $this->validateGroup($request, $this->validationRules);
        // Checar que el grupo no haya sido modificado previamente
        if ($group->updated_at->greaterThan(new Carbon($request->get('updated_at')))) {
            $description = trans('models.responses.conflict', ['model' => $pronoun]);
            throw new ConflictHttpException($description);
        }
        $group->update([
            'name' => $request->get('name'),
            'level' => $request->get('level'),
            'is_private' => empty($request->get('is_private')) ? 0 : $request->get('is_private'),
        ]);
        $clients = $request->get('clients', []);
        // $groupClients = $group->clients;
        $group->clients()->sync($clients);
        // $exists = false;
        // foreach ($clients as $clientId) {
        //     $exists = false;
        //     foreach ($groupClients as $gc) {
        //         if ($clientId == $gc->id) {
        //             $exists = true;
        //             break;
        //         }
        //     }
        //     if (!$exists) {
        //         $group->clients()->attach($clientId);
        //     }
        // }
        // foreach ($groupClients as $gc) {
        //     $exists = false;
        //     foreach ($clients as $clientId) {
        //         if ($gc->id == $clientId) {
        //             $exists = true;
        //             break;
        //         }
        //     }
        //     if (!$exists) {
        //         $group->clients()->updateExistingPivot($gc->id, ['is_active' => 0, 'deleted_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        //     }
        // }
        $description = trans('models.responses.updated', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $group = Group::findOrFail($id);
        $pronoun = trans_choice('models.pronouns.group', 1);

        // Checar si hay referencias activas al cliente a borrar
        $referencedTables = $group->activeReferencedTables('id', [], ['group_students']);
        $related = null;
        if (count($referencedTables) > 0) {
            $related = \App\BaseModel::parseTableToModelName($referencedTables[0]);
            $related = $related === null ? studly_case($referencedTables[0]) :
                    trans_choice('models.pronouns.' . strtolower($related), 2);
            $description = trans('models.responses.not_deleted', [
                'model' => $pronoun,
                'related' => $related,
            ]);
            throw new DeleteResourceFailedException($description);
        }

        // Validar que el grupo no esté relacionado con un schedule
        $numSchedules = Schedule::where('schedulable_id', $id)
            ->where('schedulable_type', 'App\Group')
            ->count();
        if ($numSchedules > 0) {
            $related = trans_choice('models.pronouns.schedule', 2);
            $description = trans('models.responses.not_deleted', [
                'model' => $pronoun,
                'related' => $related,
            ]);
            throw new DeleteResourceFailedException($description);
        }

        $group->clients()->detach();
        $group->delete();

        $description = trans('models.responses.deleted', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }
}
