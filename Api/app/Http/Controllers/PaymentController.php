<?php

namespace App\Http\Controllers;

use Dingo\Api\Routing\Helpers;
use Dingo\Api\Http\Response;
use Dingo\Api\Exception\StoreResourceFailedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;
use Carbon\Carbon;
use App\Mail\PaymentReceived;
use App\MethodPayment;
use App\Currency;
use App\Payment;
use App\Command;
use Validator;
use App\Role;

class PaymentController extends Controller
{
    use Helpers;

    protected $validationRules = [
        'command_id' => 'required|integer|exists:commands,id',
        'concept' => 'required',
        'amount' => 'required|numeric',
        'paid_at' => 'required|date_format:Y-m-d H:i:s',
        'vendor' => 'nullable',
        'reference_id' => 'nullable',
        'type' => 'nullable'
    ];

    /**
     * Perform a debit or credit card via Conekta service
     * @param Request $request
     */
    public function conekta(Request $request)
    {
        $currencyId = Currency::where('slug', 'us_dollar')->value('id');
        // TODO: Cambiar el método de pago a conekta, cuando se hizo esto no existiá este tipo de pago
        $paymentMethodId = MethodPayment::where('slug', 'paypal')->value('id');
        $command = null;
        $pronoun = trans_choice('models.pronouns.booking', 1);
        // Buscar comanda segun el código de reservación
        try {
            $command = Command::where('booking_code', $request->get('code'))->firstOrFail();
        } catch (ModelNotFoundException $e) {
            $description = trans('models.responses.not_found', ['model' => $pronoun]);
            throw new NotFoundHttpException($description);
        }
        $response = [];
        $count = $command->payments()->where('type', 'registration_fee')->count();
        if ($count > 0) {
            $response['status_code'] = 500;
            $response['message'] = "Your transaction wasn't processed, due to your booking already have registered the registration fee.";
            $response = new Response($response);
            $response->setStatusCode(500);
            return $response;
        }
        $response = Payment::processConektaPayment([
            'booking_code' => $request->get('code'),
            'conekta_token_card_id' => $request->get('conekta_token_card_id'),
            'name' => $command->client_first_name .  ' ' . $command->client_last_name,
            'email' => $command->client_email,
            'phone' => empty($command->client_phone) ? '01010101010' : $command->client_phone,
        ]);
        if ($response['success']) {
            // Registrar pago a la comanda
            if ($response['data']['payment_status'] === 'paid') {
                $command->payments()->create([
                    'concept' => "Registration Fee",
                    'amount' => 60,
                    'paid_at' => \Carbon\Carbon::createFromTimestamp($response['data']['charges']['data'][0]['paid_at'])->format('Y-m-d H:i:s'),
                    'vendor' => 'conekta',
                    'reference_id' => $response['data']['id'],
                    'type' => 'registration_fee',
                    'currency_id' => $request->input('currency_id')? $request->input('currency_id') : $currencyId,
                    'method_payment_id' => $request->input('payment_method_id')? $request->input('payment_method_id') : $paymentMethodId
                ]);
                // Enviar email de confirmación de pago
                $client = [
                    'name' => $command->client_first_name . ' ' . $command->client_last_name,
                    'email' => $command->client_email
                ];
                // Enviar correo al cliente
                Mail::to((object) $client)->send(new PaymentReceived($command, true));
                // Enviar correo al admin
                $role = Role::where('slug', 'administrator')->first();
                $admins = DB::table('users')
                        ->select('email', DB::raw("first_name || ' ' || last_name as name"))
                        ->where('role_id', $role->id)
                        ->whereNull('deleted_at')
                        ->get();
                Mail::to($admins)->send(new PaymentReceived($command, false));
            }
            else {
                $response['message'] = "Couldn't complete the transaction.";
                $this->response()->error($response, 402);
            }
            $response['status_code'] = 200;
            $response['message'] = "Transaction successfully complete.";
            return $this->responseOk($response);
        }
        $response['message'] = "Couldn't complete the transaction.";
        $code = $response['status_code'];
        $response = new Response($response);
        $response->setStatusCode((int) $code);
        return $response;
    }

    /**
     * Validate the payment data
     * @param \Illuminate\Http\Request $request The data send by the user
     * @param array $validationRules Payment validation rules
     * @throws StoreResourceFailedException|UpdateResourceFailedException
     */
    private function validatePayment(Request $request, array $validationRules)
    {
        $pronoun = trans_choice('models.pronouns.payment', 1);
        $validationRules['type'] = [
            'nullable',
            Rule::in(['registration_fee', 'other'])
        ];
        $validationRules['vendor'] = [
            'nullable',
            Rule::in(['conekta', 'paypal'])
        ];
        $validator = Validator::make($request->all(), $validationRules);
        // Si falla la validación
        if ($validator->fails()) {
            // Responder con los errores
            if ($request->isMethod('post')) {
                $description = trans('models.responses.not_created', ['model' => $pronoun]);
                throw new StoreResourceFailedException($description, $validator->errors());
            }
            else {
                $description = trans('models.responses.not_updated', ['model' => $pronoun]);
                throw new UpdateResourceFailedException($description, $validator->errors());
            }
        }
    }

    /**
     * Store a newly created payment in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $currencyId = Currency::where('slug', 'us_dollar')->value('id');
        // TODO: Cambiar el método de pago a conekta, cuando se hizo esto no existiá este tipo de pago
        $paymentMethodId = MethodPayment::where('slug', 'paypal')->value('id');
        $pronoun = trans_choice('models.pronouns.payment', 1);
        $this->validatePayment($request, $this->validationRules);
        $command = Command::find($request->get('command_id'));
        if ($request->get('type') === 'registration_fee') {
            $count = $command->payments()->where('type', 'registration_fee')->count();
            if ($count > 0) {
                $description = "Your request wasn't processed, due to your booking already have registered the registration fee.";
                throw new StoreResourceFailedException($description);
            }
        }
        // Crear pago
        $command->payments()->create([
            'concept' => $request->get('concept'),
            'amount' => $request->get('amount'),
            'paid_at' => empty($request->get('paid_at')) ? Carbon::now() : $request->get('paid_at'),
            'type' => empty($request->get('type')) ? 'other' : $request->get('type'),
            'vendor' => empty($request->get('vendor')) ? null : $request->get('vendor'),
            'reference_id' => empty($request->get('reference_id')) ? null : $request->get('reference_id'),
            'currency_id' => $request->input('currency_id')? $request->input('currency_id') : $currencyId,
            'method_payment_id' => $request->input('payment_method_id')? $request->input('payment_method_id') : $paymentMethodId
        ]);
        $description = trans('models.responses.created', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 201];
        return $this->response()->created(null, $response);
    }
}
