<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use Carbon\Carbon;
use App\Currency;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use Illuminate\Support\Facades\Auth;
use App\Transformers\CurrencyTransformer;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CurrencyController extends Controller
{
    use Helpers;

    protected $validationRules = [
        'slug' => 'required|unique:currencies,slug',
        'code' => 'required|size:3|unique:currencies,code',
        'exchange_rates' => 'required|array',
        'exchange_rates.*.to_currency_id' => 'required|exists:currencies,id',
        'exchange_rates.*.amount' => 'required|numeric',
    ];

    public function __construct(Request $request)
    {
        $this->setPermissionAndModule($request, 'currencies');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, CurrencyTransformer $currencyTransformer)
    {
        $user = Auth::user();
        // Checar permiso
        $this->checkPermission($user, $this->module, $this->permission);

        // Verificar bandera para no paginar los resultados
        $paginate = true;
        if ($request->get('no-paginate', '0') == 1) {
            $paginate = false;
        }
        // Obtener las monedas
        $result = Currency::where('is_active', 1);
        // Filtrar por nombre
        if (!empty($request->get('name'))) {
            $result->where('name', 'ilike', $request->get('name') . '%');
        }
        // Filtrar por cdóigo
        if (!empty($request->get('code'))) {
            $result->where('code', $request->get('code') . '%');
        }
        $result->orderBy('code', 'desc');
        // Paginar registros
        if ($paginate) {
            $result = $result->paginate(20);
            return $this->response->paginator($result, $currencyTransformer);
        }
        // No paginar, retornar todos los registros encontrados
        else {
            $result = $result->get();
            return $this->response->collection($result, $currencyTransformer);
        }
    }

    /**
     * Validate the currency data
     * @param \Illuminate\Http\Request $request The data send by the user
     * @param array $validationRules Currency validation rules
     * @throws StoreResourceFailedException|UpdateResourceFailedException
     */
    private function validateCurrency(Request $request, array $validationRules)
    {
        $pronoun = trans_choice('models.pronouns.currency', 1);
        $validator = Validator::make($request->all(), $validationRules);
        // Si falla la validación
        if ($validator->fails()) {
            // Obtener los mensajes de error
            $errors = $validator->errors();
            // Obtener todos los errores del campo slug
            foreach ($errors->get('slug') as $message) {
                // Asignar los mensajes de error del campo slug al campo name
                $validator->errors()->add('name', str_replace('slug', 'name', $message));
            }
            // Responder con los errores
            if ($request->isMethod('post')) {
                $description = trans('models.responses.not_created', ['model' => $pronoun]);
                throw new StoreResourceFailedException($description, $validator->errors());
            }
            else {
                $description = trans('models.responses.not_updated', ['model' => $pronoun]);
                throw new UpdateResourceFailedException($description, $validator->errors());
            }
        }
    }

    /**
     * Find the currency by a given id
     * @param type $id The currency id
     * @return \App\Currency
     * @throws NotFoundHttpException
     */
    private function getCurrency($id)
    {
        $currency = null;
        $pronoun = trans_choice('models.pronouns.currency', 1);
        try {
            $currency = Currency::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $description = trans('models.responses.not_found', ['model' => $pronoun]);
            throw new NotFoundHttpException($description);
        }
        return $currency;
    }

    /**
     * Store a newly created currency in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        // Checar permiso
        $this->checkPermission($user, $this->module, $this->permission);

        // El nombre de la moneda debe ser único y se valida con el slug
        $name = $request->input('name', '');
        // Agregar el campo slug al request
        $request->merge(['slug' => str_slug($name, '_')]);
        // Convertir el código a mayúsculas
        $request->merge(['code' => strtoupper($request->input('code', ''))]);
        $this->validateCurrency($request, $this->validationRules);
        // Crear moneda
        $currency = Currency::create([
            'name' => $name,
            'slug' => $request->input('slug'),
            'code' => $request->input('code'),
        ]);
        // Guardar tipo de cambio
        $exchangeRates = $request->get('exchange_rates');
        foreach ($exchangeRates as $xr) {
            $currency->exchangeRates()->create([
                'base_amount' => 1,
                'to_currency_id' => $xr['to_currency_id'],
                'amount' => $xr['amount']
            ]);
        }
        $pronoun = trans_choice('models.pronouns.currency', 1);
        $description = trans('models.responses.created', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 201];
        return $this->response()->created(null, $response);
    }

    /**
     * Get the specified currency.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, CurrencyTransformer $currencyTransformer)
    {
        $user = Auth::user();
        // Checar permiso
        $this->checkPermission($user, $this->module, $this->permission);

        $currency = $this->getCurrency($id);
        return $this->response->item($currency, $currencyTransformer);
    }

    /**
     * Update the specified currency in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id The currency id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();
        // Checar permiso
        $this->checkPermission($user, $this->module, $this->permission);

        $currency = $this->getCurrency($id);
        $pronoun = trans_choice('models.pronouns.currency', 1);

        $this->validationRules['code'] = "required|size:3|unique:currencies,code,{$currency->id}";
        $this->validationRules['slug'] = "required|unique:currencies,slug,{$currency->id}";

        // El nombre de la moneda debe ser único y se valida con el slug
        $name = $request->input('name', '');
        // Agregar el campo slug al request
        $request->merge(['slug' => str_slug($name, '_')]);
        // Convertir el código a mayúsculas
        $request->merge(['code' => strtoupper($request->input('code', ''))]);

        // Validar datos
        $this->validateCurrency($request, $this->validationRules);

        // Actualizar moneda
        $currency->update([
            'name' => $name,
            'slug' => $request->input('slug'),
            'code' => $request->input('code')
        ]);
        // Guardar tipos de cambio
        $exchangeRates = $request->get('exchange_rates');
        foreach ($exchangeRates as $xr) {
            // Buscar tipo de cambio
            $exchangeRate = \App\ExchangeRate::where('base_currency_id', $currency->id)
                    ->where('to_currency_id', $xr['to_currency_id'])
                    ->first();
            // Si ya existe, actualizarlo
            if (!empty($exchangeRate)) {
                $exchangeRate->update([
                    'amount' => $xr['amount']
                ]);
            }
            // Si no existe, crearlo
            else {
                $currency->exchangeRates()->create([
                    'base_amount' => 1,
                    'to_currency_id' => $xr['to_currency_id'],
                    'amount' => $xr['amount']
                ]);
            }
        }
        $description = trans('models.responses.updated', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    /**
     * Remove the specified currency from storage.
     *
     * @param  int  $id The currency id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        // Checar permiso
        $this->checkPermission($user, $this->module, $this->permission);

        $currency = $this->getCurrency($id);
        $pronoun = trans_choice('models.pronouns.currency', 1);
        // Obtener referencias a la moneda a borrar
        $references = $currency->getModelReferences();
        // Checar si hay referencias activas a la moneda a borrar
        foreach ($references as $r) {
            if ($r->table_name === 'exchange_rates') {
                continue;
            }
            $columns = DB::connection()->getSchemaBuilder()->getColumnListing($r->table_name);
            $count = DB::table($r->table_name)
                    ->where($r->column_name, $currency->id);
            // Si existe la columna deleted_at, checar que su valor sea nulo
            if (in_array('deleted_at', $columns)) {
                $count = $count->whereNull('deleted_at');
            }
            $count = $count->count();
            if ($count > 0) {
                $description = trans('models.responses.not_deleted', ['model' => $pronoun]);
                throw new DeleteResourceFailedException($description);
            }
        }
        // Modificar los campos únicos para que sus valores puedan ser utilizados en nuevos registros
        $date = Carbon::now();
        $currency->name = $currency->name . "_{$date->format('dmYHis')}";
        $currency->slug = $currency->slug . "_{$date->format('dmYHis')}";
        $currency->code = $currency->code . "_{$date->format('dmYHis')}";
        $currency->save();
        // Borrar moneda
        $currency->delete();
        $description = trans('models.responses.deleted', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    /**
     * Activate the specified currency.
     *
     * @param  int  $id The currency id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        $user = Auth::user();
        // Checar permiso
        $this->checkPermission($user, $this->module, $this->permission);

        $currency = $this->getCurrency($id);
        $pronoun = trans_choice('models.pronouns.currency', 1);
        $isActive = $currency->is_active == 1 ? 0 : 1;
        $key = $isActive === 1 ? 'activated' : 'deactivated';
        $currency->update(['is_active' => $isActive]);
        $description = trans("models.responses.$key", ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }
}
