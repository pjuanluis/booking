<?php

namespace App\Http\Controllers;

use Auth;
use File;
use Validator;
use App\Agency;
use Carbon\Carbon;
use App\Traits\FileAction;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use Illuminate\Validation\Rule;
use App\Transformers\AgencyTransformer;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class AgencyController extends Controller
{
    use Helpers, FileAction;

    protected $validationRules = [
        'name' => 'required',
        'code' => 'required|string|size:3|unique:agencies,code',
        'email' => 'required|unique:users,email',
        'password' => 'required|string|min:8',
        'logo' => 'array|required',
        'logo.name' => 'required',
        'logo.contents' => 'required',
    ];

    public function __construct(Request $request)
    {
        $this->setPermissionAndModule($request, 'agencies');
    }

    /**
     * Return a listing of agencies.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, AgencyTransformer $agencyTransformer)
    {
        $user = Auth::user();
        // Checar permiso
        $this->checkPermission($user, $this->module, $this->permission);

        // Verificar bandera para no paginar los resultados
        $paginate = true;
        if ($request->get('no-paginate', '0') == 1) {
            $paginate = false;
        }
        // Ordenar por nombre
        $result = Agency::orderBy('name');

        // Si el usuario logueado es una agencia, solo puede ver su información
        if ($user->authenticatable_type === "App\Agency") {
            $result = $result->where('id', $user->authenticatable_id);
        }

        // Filtrar por nombre
        if (!empty($request->get('name'))) {
            $result = $result->where('name', 'ilike', $request->get('name') . '%');
        }
        // Filtrar por email
        if (!empty($request->get('email'))) {
            $result = $result->where('email', 'ilike', $request->get('email') . '%');
        }
        if (($request->has('is_active'))) {
            $result = $result->where('is_active', $request->get('is_active'));
        }
        // Paginar registros
        if ($paginate) {
            $result = $result->paginate(20);
            return $this->response->paginator($result, $agencyTransformer);
        }
        // No paginar, retornar todos los registros encontrados
        else {
            $result = $result->get();
            return $this->response->collection($result, $agencyTransformer);
        }
    }

    /**
     * Find the agency by a given id
     * @param type $id The agency id
     * @return \App\Agency
     * @throws NotFoundHttpException
     */
    private function getAgency($id)
    {
        $agency = null;
        $pronoun = trans_choice('models.pronouns.agency', 1);
        try {
            $agency = Agency::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $description = trans('models.responses.not_found', ['model' => $pronoun]);
            throw new NotFoundHttpException($description);
        }
        return $agency;
    }

    /**
     * Get the specified agency.
     *
     * @param  int  $id The agency id
     * @return \Illuminate\Http\Response
     */
    public function show($id, AgencyTransformer $agencyTransformer)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $pronoun = trans_choice('models.pronouns.agency', 1);
        $agency = $this->getAgency($id);

        // Si el usuario logueado es una agencia, solo puede ver su información
        if ($user->authenticatable_type === "App\Agency") {
            if ($agency->id != $user->authenticatable_id) {
                throw new AccessDeniedHttpException(trans('models.responses.not_allowed', [
                    'model' => $pronoun
                ]));
            }
        }
        return $this->response->item($agency, $agencyTransformer);
    }


    /**
     * Validate the agency data
     * @param \Illuminate\Http\Request $request The data send by the user
     * @param string $filePath The agency logo file path
     * @param array $validationRules Agency validation rules
     * @throws StoreResourceFailedException|UpdateResourceFailedException
     */
    private function validateAgency(Request $request, $filePath, $validationRules)
    {
        $validator = Validator::make($request->all(), $validationRules);
        if ($filePath !== null) {
            $filePath = $this->getStoragePublicDir() . $filePath;
        }
        $validator->after(function ($validator) use($filePath) {
            if ($filePath !== null) {
                $mime = mime_content_type($filePath);
                // Validar que el archivo sea una imagen
                if (!in_array($mime, ['image/png', 'image/jpeg', 'image/gif'])) {
                    $description = trans('validation.custom.file.image');
                    $validator->errors()->add("file", $description);
                } else if (File::size($filePath) > 1000000) {
                    $validator->errors()->add("file", trans('validation.custom.file.size', ['size' => '1 MB']));
                }
            }
        });
        $pronoun = trans_choice('models.pronouns.agency', 1);
        // Si falla la validación
        if ($validator->fails()) {
            if ($filePath !== null) {
                // Borrar archivo
                unlink($filePath);
            }
            // Responder con los errores
            if ($request->isMethod('post')) {
                $description = trans('models.responses.not_created', ['model' => $pronoun]);
                throw new StoreResourceFailedException($description, $validator->errors());
            }
            else {
                $description = trans('models.responses.not_updated', ['model' => $pronoun]);
                throw new UpdateResourceFailedException($description, $validator->errors());
            }
        }
    }

    /**
     * Store a newly created agency in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Checar permiso
        $this->checkPermission(Auth::user(), $this->module, $this->permission);

        $filePath = null;
        if ($request->has('logo')) {
            $filePath = $this->uploadFile($request->get('logo'), 'agencies');
        }
        $request->merge(['code' => strtoupper($request->get('code'))]);
        $this->validateAgency($request, $filePath, $this->validationRules);
        $agency = Agency::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'code' => $request->input('code'),
        ]);
        $role = \App\Role::where('slug', 'agency')->first();
        // Crear cuenta de usuario
        $agency->user()->create([
            'role_id' => $role->id,
            'first_name' => $request->get('name'),
            'last_name' => '',
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
        ]);
        $logo = null;
        // Agregar archivo
        if (!empty($filePath)) {
            $logo = $request->get('logo');
            $agency->file()->create([
                'name' => $logo['name'],
                'mime' => mime_content_type($this->getStoragePublicDir() . $filePath),
                'size' => filesize($this->getStoragePublicDir() . $filePath),
                'storage_path' => $filePath,
            ]);
        }
        $pronoun = trans_choice('models.pronouns.agency', 1);
        $description = trans('models.responses.created', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 201];
        return $this->response()->created(null, $response);
    }

    /**
     * Update the specified agency in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id The agency id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $pronoun = trans_choice('models.pronouns.agency', 1);
        $agency = $this->getAgency($id);
        // Si el usuario logueado es una agencia, solo puede editar sus datos
        if ($user->authenticatable_type === 'App\Agency') {
            if ($agency->id != $user->authenticatable_id) {
                throw new AccessDeniedHttpException(trans('models.responses.not_allowed', [
                    'model' => $pronoun
                ]));
            }
        }
        $this->validationRules['updated_at'] = 'required|date_format:Y-m-d H:i:s';
        $this->validationRules['password'] = 'nullable|string|min:8';
        $this->validationRules['code'] = "required|string|size:3|unique:agencies,code,{$agency->id}";
        $this->validationRules['email'] = [
            'required',
            Rule::unique('users')->ignore($agency->user->id),
        ];
        $this->validationRules['logo'] = 'array';
        $this->validationRules['logo.name'] = '';
        $this->validationRules['logo.contents'] = '';
        $filePath = null;
        if ($request->has('logo')) {
            $filePath = $this->uploadFile($request->get('logo'), 'agencies');
        }
        // Convertir el código a mayúsculas
        $request->merge(['code' => strtoupper($request->get('code'))]);
        $this->validateAgency($request, $filePath, $this->validationRules);
        // Checar que la agencia no haya sido modificado previamente
        if ($agency->updated_at->notEqualTo(new Carbon($request->get('updated_at')))) {
            if ($filePath !== null) {
                // Borrar archivo
                unlink($this->getStoragePublicDir() . $filePath);
            }
            $description = trans('models.responses.conflict', ['model' => $pronoun]);
            throw new ConflictHttpException($description);
        }
        $data = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'code' => $request->input('code'),
        ];
        // Actualizar agencia
        $agency->update($data);
        // Actualizar los bookings code
        $agency->updateBookingCode();
        $role = \App\Role::where('slug', 'agency')->first();
        $user = $agency->user;
        if (empty($user)) {
            // Crear cuenta de usuario
            $agency->user()->create([
                'role_id' => $role->id,
                'first_name' => $request->get('name'),
                'last_name' => '',
                'email' => $request->get('email'),
                'password' => bcrypt($request->get('password')),
            ]);
        }
        else {
            // Actualizar cuenta de usuario
            $data = [
                'first_name' => $request->get('name'),
                'email' => $request->get('email'),
            ];
            if (!empty($request->get('password'))) {
                $data['password'] = bcrypt($request->get('password'));
            }
            $user->update($data);
        }
        // Actualizar archivo
        $logo = null;
        $oldFile = null;
        if (!empty($filePath)) {
            $logo = $request->get('logo');
            $oldFile = $agency->file;
            // Si el cliente tiene un archivo actualmente, reemplazarlo
            if ($oldFile !== null) {
                // Reemplazar archivo del usuario
                $this->deleteFile($oldFile, 'public', 'local', false, true);
                // Actualizar datos del archivo del cliente
                $oldFile->update([
                    'name' => $logo['name'],
                    'mime' => mime_content_type($this->getStoragePublicDir() . $filePath),
                    'size' => filesize($this->getStoragePublicDir() . $filePath),
                    'storage_path' => $filePath,
                ]);
            }
            else {
                // Guardar nuevo registro del archivo
                $agency->file()->create([
                    'name' => $logo['name'],
                    'mime' => mime_content_type($this->getStoragePublicDir() . $filePath),
                    'size' => filesize($this->getStoragePublicDir() . $filePath),
                    'storage_path' => $filePath,
                ]);
            }
        }
        $description = trans('models.responses.updated', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    /**
     * Remove the specified agency from storage.
     *
     * @param  int  $id The agency id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $agency = $this->getAgency($id);
        $pronoun = trans_choice('models.pronouns.agency', 1);

        // Checar si hay referencias activas a la agencia a borrar
        $referencedTables = $agency->activeReferencedTables('id');
        $related = null;
        if (count($referencedTables) > 0) {
            $related = \App\BaseModel::parseTableToModelName($referencedTables[0]);
            $related = $related === null ? studly_case($referencedTables[0]) :
                    trans_choice('responses.pronouns.' . strtolower($related), 2);
            $description = trans('responses.messages.not_deleted', [
                'model' => $pronoun,
                'related' => $related,
            ]);
            throw new DeleteResourceFailedException($description);
        }
        // Borrar agencia
        $agencyUser = $agency->user;
        if (!empty($agencyUser)) {
            // Un usuario no se puede eliminar así mismo
            if ($agencyUser->id == $user->id) {
                $description = 'Could not delete User, the User can not remove himself.';
                throw new DeleteResourceFailedException($description);
            }
            // Antes de eliminar el usuario de la agencia hay que cambiar el email para que ese email pueda ser reutilizado con nuevas agencias, ya que este campo tiene la restricción de ser único.
            $agencyUser->email = $agencyUser->email . "-" . Carbon::now()->format('Y-m-d H:i:s');
            $agencyUser->update();
            $agencyUser->delete();
        }
        // Modificar el código y el email antes de eliminar la agencia, para que estos datos puedan ser utilizados nuevamente
        $agency->code = $agency->code . "-" . Carbon::now()->format('Y-m-d H:i:s');
        $agency->email = $agency->email . "-" . Carbon::now()->format('Y-m-d H:i:s');
        $agency->update();
        // Eliminar la agencia
        $agency->delete();
        $description = trans('models.responses.deleted', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }
}
