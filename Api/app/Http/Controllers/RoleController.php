<?php

namespace App\Http\Controllers;

use Auth;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;
use App\Transformers\RoleTransformer;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use \App\Role;
use \Carbon\Carbon;
use Validator;
use DB;

class RoleController extends Controller
{
    use Helpers;

    protected $validationRules = [
        'name' => 'required',
        'accesses' => 'array',
        'accesses.*.module_id' => 'required|exists:system_modules,id',
        'accesses.*.permissions' => 'required|array',
    ];

    public function __construct(Request $request)
    {
        $this->setPermissionAndModule($request, 'roles');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);
        
        $paginate = true;
        if ($request->get('no-paginate', '0') == 1) {
            $paginate = false;
        }

        $result = Role::where('id', '!=', 0)->orderBy('name');

        if ($request->has('is_active')) {
            $result = $result->where('is_active', $request->get('is_active'));
        }

        // Paginar registros
        if ($paginate) {
            $result = $result->paginate(20);
            return $this->response->paginator($result, new RoleTransformer);
        }
        // No paginar, retornar todos los registros encontrados
        else {
            $result = $result->get();
            return $this->response->collection($result, new RoleTransformer);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Validate the role data
     * @param \Illuminate\Http\Request $request The data send by the user
     * @param array $validationRules Role validation rules
     * @throws StoreResourceFailedException|UpdateResourceFailedException
     */
    private function validateRole(Request $request, array $validationRules)
    {
        $validator = Validator::make($request->all(), $validationRules);
        $validator->after(function ($validator) use($request) {
            // Validar que los vuelos pertenezcan a la comanda
            $traductions = \Illuminate\Support\Arr::dot((array) trans('validation.custom'));
            if ($request->has('accesses')) {
                $accesses = $request->get('accesses');
                foreach ($accesses as $i => $acc) {
                    if (!empty($acc['permissions'])) {
                        $v0 = Validator::make($acc, [
                            'permissions.*.id' => 'required|exists:system_permissions,id',
                        ]);
                        $messages = $v0->messages()->getMessages();
                        foreach ($messages as $key => $msg) {
                            foreach ($msg as $m) {
                                $validator->errors()->add("permissions.$i.$key", $m);
                            }
                        }
                        $perms = $acc['permissions'];
                        if (empty($acc['module_id'])) {
                            continue;
                        }
                        foreach ($perms as $j => $pe) {
                            if (empty($pe['id'])) {
                                continue;
                            }
                            $count = DB::table('system_module_permissions')
                                    ->where('system_module_id', $acc['module_id'])
                                    ->where('system_permission_id', $pe['id'])
                                    ->count();
                            if ($count == 0) {
                                $msg = $traductions['permissions.*.id.exists'];
                                $validator->errors()->add("accesses.$i.permissions.$j.id", $msg);
                            }
                        }
                    }
                }
            }
        });
        $pronoun = trans_choice('models.pronouns.role', 1);
        // Si falla la validación
        if ($validator->fails()) {
            // Responder con los errores
            if ($request->isMethod('post')) {
                $description = trans('models.responses.not_created', ['model' => $pronoun]);
                throw new StoreResourceFailedException($description, $validator->errors());
            }
            else {
                $description = trans('models.responses.not_updated', ['model' => $pronoun]);
                throw new UpdateResourceFailedException($description, $validator->errors());
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $pronoun = trans_choice('models.pronouns.role', 1);
        $this->validateRole($request, $this->validationRules);
        try {
            $role = Role::create([
                'name' => $request->get('name'),
                'slug' => str_slug($request->get('name'), '_'),
            ]);
        } catch(QueryException $e) {
            $description = trans('models.responses.already_exists', ['model' => $pronoun]);
            throw new UnprocessableEntityHttpException($description);
        }
        // Agregar permisos
        $accesses = $request->get('accesses');
        foreach ($accesses as $acc) {
            foreach ($acc['permissions'] as $p) {
                DB::table('system_access_control')->insert([
                    'role_id' => $role->id,
                    'system_module_id' => $acc['module_id'],
                    'system_permission_id' => $p['id'],
                ]);
            }
        }
        $pronoun = trans_choice('models.pronouns.role', 1);
        $description = trans('models.responses.created', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 201];
        return $this->response()->created(null, $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);
        
        try {
            $role = Role::findOrFail($id);
            return $this->response->item($role, new RoleTransformer());
        } catch (ModelNotFoundException $e) {
            $description = trans('models.responses.not_found', ['model' => $pronoun]);
            throw new NotFoundHttpException($description);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $role = Role::findOrFail($id);
        $pronoun = trans_choice('models.pronouns.role', 1);
        $this->validationRules['updated_at'] = 'required|date_format:Y-m-d H:i:s';
        $this->validateRole($request, $this->validationRules);
        // Checar que el rol no haya sido modificado previamente
        if ($role->updated_at->notEqualTo(new Carbon($request->get('updated_at')))) {
            $description = trans('models.responses.conflict', ['model' => $pronoun]);
            throw new ConflictHttpException($description);
        }
        // Actualizar rol
        try {
            $role->update([
                'name' => $request->get('name'),
                'slug' => str_slug($request->get('name'), '_'),
            ]);
        } catch(QueryException $e) {
            $description = trans('models.responses.already_exists', ['model' => $pronoun]);
            throw new UnprocessableEntityHttpException($description);
        }
        // Borrar accesos
        DB::table('system_access_control')
                ->where('role_id', $role->id)
                ->delete();
        // Agregar accesos
        $accesses = $request->get('accesses');
        foreach ($accesses as $acc) {
            foreach ($acc['permissions'] as $p) {
                DB::table('system_access_control')->insert([
                    'role_id' => $role->id,
                    'system_module_id' => $acc['module_id'],
                    'system_permission_id' => $p['id'],
                ]);
            }
        }
        $description = trans('models.responses.updated', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $role = Role::findOrFail($id);
        $pronoun = trans_choice('models.pronouns.role', 1);
        // Checar si hay referencias activas al cliente a borrar
        $referencedTables = $role->activeReferencedTables('id');
        $related = null;
        if (count($referencedTables) > 0) {
            $related = \App\BaseModel::parseTableToModelName($referencedTables[0]);
            $related = $related === null ? studly_case($referencedTables[0]) :
                    trans_choice('models.pronouns.' . strtolower($related), 2);
            $description = trans('models.responses.not_deleted', [
                'model' => $pronoun,
                'related' => $related,
            ]);
            throw new DeleteResourceFailedException($description);
        }

        // Borrar rol
        $role->delete();
        $description = trans('models.responses.deleted', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    /**
     * Activate / deactivate the specified consumption center.
     *
     * @param  int  $id The consumption center id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $role = Role::findOrFail($id);
        $pronoun = trans_choice('models.pronouns.role', 1);
        $isActive = $role->is_active == 1 ? 0 : 1;
        $key = $isActive === 1 ? 'activated' : 'deactivated';
        $role->update(['is_active' => $isActive]);
        $description = trans("models.responses.$key", ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }
}
