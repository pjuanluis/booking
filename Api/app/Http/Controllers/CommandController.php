<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use App\Role;
use App\Bill;
use Validator;
use App\Client;
use App\Command;
use App\Payment;
use App\Currency;
use App\Purchase;
use Carbon\Carbon;
use App\CommandLog;
use App\Purchasable;
use App\MethodPayment;
use League\Fractal\Manager;
use League\Fractal\Serializer\DataArraySerializer;
use Illuminate\Http\Request;
use App\Mail\BookingCreated;
use App\Mail\PaymentReceived;
use Dingo\Api\Routing\Helpers;
use Illuminate\Validation\Rule;
use League\Fractal\Resource\Item;
use Illuminate\Support\Facades\Mail;
use App\Transformers\CommandTransformer;
use App\Transformers\CommandLogTransformer;
use Dingo\Api\Exception\ResourceException;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use App\TravelInformation;

class CommandController extends Controller
{
    use Helpers;

    private $requestHasTravelInformation = null;

    protected $validationRules = [
        'client_id' => 'required_without:client_first_name|integer|exists:clients,id',
        'client_first_name' => 'required_without:client_id',
        'client_last_name' => 'required_without:client_id',
        'client_email' => 'required_without:client_id|email',
        'country_id' => 'required_without:client_id|integer|exists:countries,id',
        'agency_id' => 'nullable|integer|exists:agencies,id',
        'pickup_id' => 'nullable|integer|exists:pickup,id',
        'pickup_cost' => 'required_with:pickup_id|nullable|numeric|min:0',
        'pickup_currency_id' => 'required_with:pickup_cost|nullable|exists:currencies,id',
        'pickup_discount' => 'nullable|numeric|min:0',
        'pickup_exchange_rates' => 'nullable|array',
        'pickup_exchange_rates.*.amount' => 'required|numeric',
        'pickup_exchange_rates.*.base_amount' => 'required|numeric',
        'pickup_exchange_rates.*.to_currency_id' => 'required|integer|exists:currencies,id',
        'pickup_exchange_rates.*.base_currency_id' => 'required|integer|same:pickup_currency_id',
        'payment_status' => 'required|in:paid,partially_paid,not_payed',
        'courses' => 'array|required_without_all:housing,purchases',
        'courses.*.package_id' => 'required|exists:packages,id',
        'courses.*.currency_id' => 'required|exists:currencies,id',
        'courses.*.quantity' => 'required|integer|min:1',
        'courses.*.discount' => 'required|integer|min:0',
        'courses.*.cost' => 'required|numeric',
        'courses.*.from_date' => 'required|date_format:Y-m-d',
        'courses.*.to_date' => 'required|date_format:Y-m-d|after_or_equal:courses.*.from_date',
        'courses.*.exchange_rates' => 'required|array',
        'courses.*.exchange_rates.*.amount' => 'required|numeric',
        'courses.*.exchange_rates.*.base_amount' => 'required|numeric',
        'courses.*.exchange_rates.*.to_currency_id' => 'required|integer|exists:currencies,id',
        'courses.*.exchange_rates.*.base_currency_id' => 'required|integer|same:courses.*.currency_id',
        'housing' => 'array|required_without_all:courses,purchases',
        'housing.*.housing_room_package_id' => 'required|exists:housing_room_packages,id',
        'housing.*.quantity' => 'required|integer|min:1',
        'housing.*.unit' => 'required',
        'housing.*.cost' => 'required|numeric|min:0',
        'housing.*.currency_id' => 'required|exists:currencies,id',
        'housing.*.discount' => 'required|integer|min:0',
        'housing.*.people' => 'required|integer|min:1',
        'housing.*.from_date' => 'required|date_format:Y-m-d',
        'housing.*.to_date' => 'required|date_format:Y-m-d|after:housing.*.from_date',
        'housing.*.exchange_rates' => 'required|array',
        'housing.*.exchange_rates.*.amount' => 'required|numeric',
        'housing.*.exchange_rates.*.base_amount' => 'required|numeric',
        'housing.*.exchange_rates.*.to_currency_id' => 'required|integer|exists:currencies,id',
        'housing.*.exchange_rates.*.base_currency_id' => 'required|integer|same:housing.*.currency_id',
        'command_bills' => 'nullable|array',
        'command_bills.*.reference_number' => 'required_with:command_bills.*.amount,command_bills.*.currency_id|string|max:255|unique:bills,reference_number',
        'command_bills.*.amount' => 'required_with:command_bills.*.reference_number,command_bills.*.currency_id|numeric|min:0',
        'command_bills.*.currency_id' => 'required_with:command_bills.*.amount,command_bills.*.reference_number|integer|exists:currencies,id',
        'purchases' => 'array|required_without_all:housing,courses',
        'purchases.*.date' => 'required|date_format:Y-m-d',
        'purchases.*.reference_number' => 'nullable',
        'purchases.*.concept' => 'required',
        'purchases.*.amount' => 'required|numeric',
        'purchases.*.discount' => 'nullable|numeric|min:0',
        'purchases.*.currency_id' => 'required|integer|exists:currencies,id',
        'purchases.*.consumption_center_id' => 'required|integer|exists:consumption_centers,id',
        'purchases.*.exchange_rates' => 'required|array',
        'purchases.*.exchange_rates.*.amount' => 'required|numeric',
        'purchases.*.exchange_rates.*.base_amount' => 'required|numeric',
        'purchases.*.exchange_rates.*.to_currency_id' => 'required|integer|exists:currencies,id',
        'purchases.*.exchange_rates.*.base_currency_id' => 'required|integer|same:purchases.*.currency_id',
        'payments' => 'array|nullable',
        'payments.*.paid_at' => 'required|date_format:Y-m-d H:i:s',
        'payments.*.reference_id' => 'nullable',
        'payments.*.vendor' => 'nullable',
        'payments.*.concept' => 'required',
        'payments.*.type' => 'required',
        'payments.*.amount' => 'required|numeric|min:1',
        'payments.*.currency_id' => 'required|integer|exists:currencies,id',
        'payments.*.payment_method_id' => 'required|integer|exists:methods_payment,id',
        'payments.*.exchange_rates' => 'required|array',
        'payments.*.exchange_rates.*.amount' => 'required|numeric',
        'payments.*.exchange_rates.*.base_amount' => 'required|numeric',
        'payments.*.exchange_rates.*.to_currency_id' => 'required|integer|exists:currencies,id',
        'payments.*.exchange_rates.*.base_currency_id' => 'required|integer|same:payments.*.currency_id',
    ];

    protected $bookingValidationRules = [
        'client_first_name' => 'required',
        'client_last_name' => 'required',
        'client_email' => 'required|email',
        'pickup_id' => 'nullable|exists:pickup,id',
        'courses' => 'array|required_without:housing',
        'courses.*.package_id' => 'required|exists:packages,id',
        'courses.*.quantity' => 'required|integer|min:1',
        'courses.*.from_date' => 'nullable|date_format:Y-m-d',
        'courses.*.to_date' => 'nullable|date_format:Y-m-d|after_or_equal:courses.*.from_date',
        'housing' => 'array|required_without:courses',
        'housing.*.housing_room_package_id' => 'required|exists:housing_room_packages,id',
        'housing.*.quantity' => 'required|integer|min:1',
        'housing.*.unit' => 'required',
        'housing.*.people' => 'required|integer|min:1',
        'housing.*.from_date' => 'required|date_format:Y-m-d',
        'housing.*.to_date' => 'required|date_format:Y-m-d|after:housing.*.from_date',
    ];

    protected $paymentValidationRules = [
        'concept' => 'required',
        'amount' => 'required|numeric',
        'paid_at' => 'required|date_format:Y-m-d H:i:s',
        'type' => 'required',
        'currency_id' => 'nullable|integer|exists:currencies,id',
        'payment_method_id' => 'nullable|integer|exists:methods_payment,id',
    ];

    public function __construct(Request $request)
    {
        $this->setPermissionAndModule($request, 'bookings');
        $this->requestHasTravelInformation = null; // is necessary the null to initialize.
    }

    /*
     * Validate command data
     */
    private function validateCommand($request, $validationRules, $command = null)
    {
        $validator = Validator::make($request->all(), $validationRules);
        $validator->after(function ($validator) use($request, $command) {
            // Validar datos de los cursos seleccionados
            $data = $request->get('courses', []);
            $beginDate = null;
            $endDate = null;
            $occupancy = 0;
            $messages = \Illuminate\Support\Arr::dot((array) trans('validation.custom'));
            if (!empty($data)) {
                foreach ($data as $i => $r) {
                    $package = \App\Package::find($r['package_id']);
                    if (!empty($package)) {
                        if ($package->type !== 'experience') {
                            $description = $messages['courses.*.package_id.exists'];
                            $validator->errors()->add("courses.$i.package_id", $description);
                        }
                    }
                }
            }
            // Validar datos del hospedaje
            $data = $request->get('housing', []);
            if (!empty($data)) {
                foreach ($data as $i => $r) {
                    if (!in_array($r['unit'], ['night', 'week', 'month'])) {
                        $description = $messages['housing.*.unit.exists'];
                        $validator->errors()->add("housing.$i.unit", $description);
                    }
                    if (isset($r['from_date']) && isset($r['to_date'])) {
                        // Validar fecha de termino respecto al numero de dias o semanas seleccionado
                        $beginDate = new \Carbon\Carbon($r['from_date'] . ' 00:00:00');
                        $endDate = new \Carbon\Carbon($r['to_date'] . ' 00:00:00');
                        $days = $r['quantity'];
                        if ($r['unit'] === 'week') {
                            // Para hospedaje la semana es completa, de 7 dias
                            $days *= 7;
                        } elseif ($r['unit'] === 'month') {
                            $days *= 28; // 1 mes = 4 semanas, 1 semana = 7 días, 7 * 4 = 28 días del mes
                        }
                        if (($endDate->diffInDays($beginDate)) != $days) {
                            $description = str_replace(':quantity', $r['quantity'], $messages['housing.*.to_date.val']);
                            $description = str_replace(':unit', trans_choice('validation.attributes.' . $r['unit'], (int) $r['quantity']), $description);
                            $validator->errors()->add("housing.$i.to_date", $description);
                        }
                        // Validar disponibilidad
                        if (!empty($r['housing_room_package_id'])) {
                            $result = DB::table('housing_room_packages')
                                    ->select('housing_room_id')
                                    ->where('id', $r['housing_room_package_id'])
                                    ->first();
                            if (!empty($result)) {
                                // Obtener total de camas del tipo de cuarto seleccionado
                                $totalBeds = $this->getTotalBedsOfARoomType($result->housing_room_id, $r['from_date'], $r['to_date']);
                                $occupancy = $this->getOcuppancy($r['from_date'], $r['to_date'], $result->housing_room_id, $r['people'], !empty($r['id'])? $r['id'] : null);
                                if ($totalBeds < $occupancy) {
                                    $description = $messages['housing.*.housing_room_package_id.unavailable'];
                                    $validator->errors()->add("housing.$i.housing_room_package_id", $description);
                                }
                            }
                        }
                    }
                }
            }
            // Validar que las facturas sean únicas(solo cuando se edita una comanda)
            if ($request->isMethod('put')) {
                $bills = $request->get('command_bills', []);
                $tmpReferences = [];
                $attributes = \Illuminate\Support\Arr::dot((array) trans('validation.attributes'));
                foreach ($bills as $k => $bill) {
                    $referenceKey = "b_{$bill['reference_number']}"; // El prefijo b_ se pone para garantizar que siempre sea un string
                    // Buscar algún registro que conincida con el numero de referencia enviado por el usuario
                    $billBd = Bill::where('reference_number', $bill['reference_number'])->first();
                    // Validar que el número de referencia sea único tanto en los registros enviados por el usuario como en la bd
                    if (isset($tmpReferences[$referenceKey]) || ($billBd !== null && $billBd->command_id !== $command->id)) {
                        $validator->errors()->add("command_bills.$k.reference_number", trans('validation.unique', ['attribute' => $attributes["command_bills.*.reference_number"]]));
                    }
                    $tmpReferences[$referenceKey] = $k; // La $k no tiene importancia pero debe ir algùn valor ahì
                }
            }
        });
        $pronoun = trans_choice('models.pronouns.command', 1);
        // Si falla la validación
        if ($validator->fails()) {
            // Responder con los errores
            if ($request->isMethod('post')) {
                $description = trans('models.responses.not_created', ['model' => $pronoun]);
                throw new StoreResourceFailedException($description, $validator->errors());
            }
            else {
                $description = trans('models.responses.not_updated', ['model' => $pronoun]);
                throw new UpdateResourceFailedException($description, $validator->errors());
            }
        }
    }

    /**
     * Find the command by a given id
     * @param type $id The command id
     * @return \App\Command The requested command object
     * @throws NotFoundHttpException If the requested command doesn't exists
     * to the command trotter.
     */
    private function getCommand($id)
    {
        $command = null;
        $pronoun = trans_choice('models.pronouns.command', 1);
        try {
            $command = Command::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            $description = trans('models.responses.not_found', ['model' => $pronoun]);
            throw new NotFoundHttpException($description);
        }
        return $command;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CommandTransformer $commandTransformer, Request $request)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $sort = 'created_at';
        $dir = 'asc';
        $paginate = true;
        if ($request->get('no-paginate', '0') == 1) {
            $paginate = false;
        }
        if (!empty($request->get('sort'))) {
            $sort = $request->get('sort');
        }
        if (!empty($request->get('dir'))) {
            $dir = $request->get('dir');
        }
        $result = Command::orderBy($sort, $dir);
        if ($user->authenticatable_type === 'App\Agency') {
            $result = $result->where('agency_id', $user->authenticatable_id);
        } else if ($user->authenticatable_type === 'App\Client') {
            $result = $result->where('client_id', $user->authenticatable_id);
        }
        if (!empty($request->get('registration_from'))) {
            $result = $result->where('registration_from', $request->get('registration_from'));
        }
        if (!empty($request->get('client_id'))) {
            $result = $result->where('client_id', $request->get('client_id'));
        }
        if (!empty($request->get('agency_id'))) {
            $result = $result->where('agency_id', $request->get('agency_id'));
        }
        if (!empty($request->get('status'))) {
            $result = $result->where('status', $request->get('status'));
        }
        if (!empty($request->get('client_email'))) {
            $result = $result->where('client_email', $request->get('client_email'));
        }
        if (!empty($request->get('booking_code'))) {
            $result = $result->where('booking_code', 'ilike', "%{$request->get('booking_code')}%");
        }
        if (!empty($request->get('token'))) {
            $result = $result->where('token', $request->get('token'));
        }
        if (!empty($request->get('from_date'))) {
            $result = $result->where('from_date', $request->get('from_date'));
        }
        if (!empty($request->get('to_date'))) {
            $result = $result->where('to_date', $request->get('to_date'));
        }
        if (!empty($request->get('month'))) {
            $result = $result->whereMonth('created_at', $request->get('month'));
        }
        // Filtrar comandas dependiendo si se les ha pagado el fee o no
        if (!empty($request->get('registration_fee'))) {
            $registrationFee = $request->get('registration_fee');
            // Unicamente las que tienen el fee pagado
            if ($registrationFee === 'paid') {
                $result = $result->whereHas('payments', function ($query) {
                        $query->where('type', 'registration_fee');
                    });
            } elseif ($registrationFee === 'not-paid') { // Unicamente las que no tienen el fee pagado
                $result = $result->whereDoesntHave('payments', function ($query) {
                    $query->where('type', 'registration_fee');
                });
            }
        }
        // Filtrar comandas por experiencias
        if (!empty($request->get('experiences'))) {
            $experiences = explode(',', $request->get('experiences'));
            $result = $result->whereHas('commandCourses.package.experiences', function($query) use($experiences) {
                $query->whereIn('slug', $experiences);
            });
        }
        // Filtrar comandas por fecha de llegada
        if (!empty($request->get('arrival_at'))) {
            $arrivalAt = $request->get('arrival_at');
            $result = $result->whereHas('travelInformation', function($query) use($arrivalAt) {
                $query->whereDate('arrival_at', $arrivalAt);
            });
        }
        // Filtrar comandas por fecha de partida
        if (!empty($request->get('departure_at'))) {
            $departureAt = $request->get('departure_at');
            $result = $result->whereHas('travelInformation', function($query) use($departureAt) {
                $query->whereDate('departure_at', "%{$departureAt}%");
            });
        }
        if (!empty($request->get('command-type'))) {
            // Booking
            if (strcasecmp('booking', $request->get('command-type')) == 0) {
                $result = $result->where(function($query) {
                    $query->where('registration_from', 'public_site')
                        ->orWhereNull('registration_from')
                        ->doesntHave('client')
                        ->doesntHave('payments', 'or')
                        ->orWhereHas('payments', function($q) {
                            $q->whereNotIn('payments.type', ['registration_fee', 'reservation_payment']);
                        });
                });
            }
            // Confirmed
            else if (strcasecmp('confirmed', $request->get('command-type')) == 0) {
                $result = $result->where(function($query) {
                    $query->where(function($query0) {
                        $query0->whereHas('client')
                            ->where(function($query1) {
                                $query1->whereHas('payments', function($q) {
                                        $q->whereIn('payments.type', ['registration_fee', 'reservation_payment']);
                                    })
                                    ->orWhereRaw("booking_code like 'WWW-%'")
                                    ->orWhere('registration_from', 'public_site');
                            });
                    })
                    ->orWhere('registration_from', 'admin_site');
                });
            }
        }
        if ($request->routeIs('commands.count')) {
            $result = $result->count();
            $result = ['count' => $result, 'status_code' => 200];
            return $this->responseOk($result);
        }
        // Paginar registros
        if ($paginate) {
            $result = $result->paginate(20);
            return $this->response->paginator($result, $commandTransformer);
        }
        // No paginar, retornar todos los registros encontrados
        else {
            if ($request->get('generate-csv') == 1) {
                return $result->with([
                        'travelInformation',
                        'purchase.user',
                        'client.country',
                        'agency',
                        'commandCourses.package.experiences',
                        'commandCourses.purchase.currency',
                        'commandHousing.room.residence',
                        'commandHousing.purchase.currency',
                        'extraPurchases.purchasable.currency',
                        'extraPurchases.purchasable.consumptionCenter',
                        'payments.currency',
                        'payments.paymentMethod',
                        'bills',
                        'pickup',
                    ])
                    ->get();
            }
            $result = $result->get();
            return $this->response->collection($result, $commandTransformer);
        }
    }

    public function generateCSV(Request $request) {
        $request->request->add(['no-paginate' => 1]);
        $request->request->add(['generate-csv' => 1]);
        $bookings = $this->index(new CommandTransformer(), $request);
        // Formatear la información
        foreach($bookings as $b) {
            $dataClient = ['--', '--', '--', '--', '--'];
            $payments = $b->payments;
            // Agregar datos de la comanda
            $dataCommand = [
                'created_at' => $b->created_at->format('d-m-Y'),
                'booking_code' => $b->booking_code,
                'responsible' => !empty($b->purchase->user) ? $b->purchase->user->first_name . ' ' . $b->purchase->user->last_name : '--',
                'agency' => $b->agency->name ?? '--',
            ];
            if (!empty($b->client)) {
                // Agregar los datos del cliente
                $dataClient = [
                    'first_name' => $b->client->first_name,
                    'last_name' => $b->client->last_name,
                    'email' => $b->client->email,
                    'birth_date' => (new Carbon($b->client->birth_date))->format('d-m-Y'),
                    'country' => $b->client->country->name,
                ];
            }
            // Unir datos
            $dataCommand = array_merge(
                    $dataCommand,
                    $dataClient,
                    ['travel_partner' => $b->travel_partner ? $b->travel_partner : '--']
                );
            $cc = $b->commandCourses; // $cc = Command courses
            $ch = $b->commandHousing; // $ch = Command housing
            $ep = $b->extraPurchases; // $ep = Extra purchases;
            $fp = $payments->where('type', 'registration_fee')->values()->toArray(); // $fp = Fee payment
            $p = $payments->where('type', '!=', 'registration_fee')->values()->toArray(); // $p = payments
            $br = $b->bills; // $br = Billing reference
            $pu = $b->pickup; // $pu = Pick up
            $max = max(count($cc), count($ch), count($ep), count($fp), count($p), count($br));
            $i = 0;
            do {
                // Inicializar arreglos
                $dataCourse = [
                    'experience' => '--',
                    'package' => '--',
                    'quantity' => '--',
                    'cost' => '--',
                    'currency' => '--',
                    'discount' => '--',
                    'begin' => '--',
                    'end' => '--',
                ];
                $dataHousing = [
                    'housing' => '--',
                    'room_type' => '--',
                    'package_h' => '--',
                    'room' => '--',
                    'residence' => '--',
                    'quantity_h' => '--',
                    'unit' => '--',
                    'cost_h' => '--',
                    'currency_h' => '--',
                    'discount_h' => '--',
                    'begin_h' => '--',
                    'end_h' => '--',
                ];
                $dataExtraPurchase = [
                    'date' => '--',
                    'reference_number' => '--',
                    'concept' => '--',
                    'amount' => '--',
                    'currency_ep' => '--',
                    'discount_ep' => '--',
                    'consumption_center' => '--',
                ];
                $dataPickup = [
                    'pickup_at' => '--',
                    'pickup_cost' => '--',
                    'pickup_discount' => '--',
                ];
                $dataFeePayment = [
                    'concept_fp' => '--',
                    'amount_fp' => '--',
                    'currency_fp' => '--',
                    'paid_at' => '--',
                    'vendor' => '--',
                ];
                $dataPayment = [
                    'invoiced' => '--',
                    'reference_number_p' => '--',
                    'concept_p' => '--',
                    'amount_p' => '--',
                    'currency_p' => '--',
                    'payment_method' => '--',
                    'paid_at_p' => '--',
                    'vendor_p' => '--',
                ];
                $dataBill = [
                    'reference_number_br' => '--',
                    'amount_br' => '--',
                    'currency_br' => '--',
                ];

                // Agregar datos de los cursos
                if (isset($cc[$i])) {
                    $dataCourse = [
                        'experience' => implode(', ', array_column($cc[$i]->package->experiences->toArray(), 'name')),
                        'package' => $cc[$i]->package->name,
                        'quantity' => $cc[$i]->quantity,
                        'cost' => $cc[$i]->cost,
                        'currency' => $cc[$i]->purchase->currency->code,
                        'discount' => $cc[$i]->purchase->discount,
                        'begin' => (new Carbon($cc[$i]->from_date))->format('d-m-Y'),
                        'end' => (new Carbon($cc[$i]->to_date))->format('d-m-Y'),
                    ];
                }
                // Agregar datos del housing
                if (isset($ch[$i])) {
                    $dataHousing = [
                        'housing' => $ch[$i]->housing->name,
                        'room_type' => $ch[$i]->housingRoom->name,
                        'package_h' => $ch[$i]->package->name,
                        'room' => $ch[$i]->room->name ?? '--',
                        'residence' => $ch[$i]->room->residence->name ?? '--',
                        'quantity_h' => $ch[$i]->quantity,
                        'unit' => $ch[$i]->unit,
                        'cost_h' => $ch[$i]->cost,
                        'currency_h' => $ch[$i]->purchase->currency->code,
                        'discount_h' => $ch[$i]->purchase->discount,
                        'begin_h' => (new Carbon($ch[$i]->from_date))->format('d-m-Y'),
                        'end_h' => (new Carbon($ch[$i]->to_date))->format('d-m-Y'),
                    ];
                }
                // Agregar datos de las compras extras
                if (isset($ep[$i])) {
                    $dataExtraPurchase = [
                        'date' => (new Carbon($ep[$i]->date))->format('d-m-Y'),
                        'reference_number' => $ep[$i]->reference_number ? $ep[$i]->reference_number : '--',
                        'concept' => $ep[$i]->concept,
                        'amount' => $ep[$i]->amount,
                        'currency_ep' => $ep[$i]->purchasable->currency->code,
                        'discount_ep' => $ep[$i]->purchasable->discount,
                        'consumption_center' => $ep[$i]->purchasable->consumptionCenter->name,
                    ];
                }
                // Agregar datos del pickup
                if ($pu && $i === 0) {
                    $dataPickup = [
                        'pickup_at' => $pu->name,
                        'pickup_cost' => $pu->cost,
                        'pickup_discount' => $b->purchase->discount,
                    ];
                }
                // Agregar datos del fee payment
                if (isset($fp[$i])) {
                    $dataFeePayment = [
                        'concept_fp' => $fp[$i]['concept'],
                        'amount_fp' => $fp[$i]['amount'],
                        'currency_fp' => $fp[$i]['currency']['code'] ?? '--',
                        'paid_at' => (new Carbon($fp[$i]['paid_at']))->format('d-m-Y'),
                        'vendor' => $fp[$i]['vendor'] ? $fp[$i]['vendor'] : '--',
                    ];
                }
                // Agregar datos de los pagos
                if (isset($p[$i])) {
                    $dataPayment = [
                        'invoiced' => $p[$i]['invoiced_at'] ? 'Yes' : 'No',
                        'reference_number_p' => $p[$i]['reference_id'] ? $p[$i]['reference_id'] : '--',
                        'concept_p' => $p[$i]['concept'],
                        'amount_p' => $p[$i]['amount'],
                        'currency_p' => $p[$i]['currency']['code'] ?? '--',
                        'payment_method' => $p[$i]['paymentMethod']['name'] ?? '--',
                        'paid_at_p' => (new Carbon($p[$i]['paid_at']))->format('d-m-Y'),
                        'vendor_p' => $p[$i]['vendor'] ? $p[$i]['vendor'] : '--',
                    ];
                }
                // Agregar datos de los billing references
                if (isset($br[$i])) {
                    $dataBill = [
                        'reference_number_br' => $br[$i]->reference_number,
                        'amount_br' => $br[$i]->amount,
                        'currency_br' => $br[$i]->currency->code?? '--',
                    ];
                }

                $dataExport[] = $dataCommand + $dataCourse + $dataHousing + $dataExtraPurchase + $dataPickup + $dataFeePayment + $dataPayment + $dataBill;
                $i++;
            } while($i < $max);
        }
        // Agregar los títulos de las columnas
        $dataExport = array_merge([['Reg. date', 'Booking Reference', 'Responsible', 'Agency', 'First name', 'Last name', 'Email', 'Birth date', 'Country', 'Travel Partner','Experience', 'Package', 'Q', 'Cost', 'Currency', 'Discount', 'Begin', 'End', 'Housing', 'Room type', 'Package', 'Room', 'Residence', 'Q', 'U', 'Cost', 'Currency', 'Discount', 'Begin', 'End', 'Date', 'Reference number', 'Concept', 'Amount', 'Currency', 'Discount', 'Consumption center', 'Transfer service', 'Transfer cost', 'Transfer discount', 'Concept', 'Amount', 'Currency', 'Paid at', 'Vendor', 'Invoiced', 'Reference number', 'Concept', 'Amount', 'Currency', 'Payment method', 'Paid at', 'Vendor','Reference number','Amount','Currency']], $dataExport);
        // Exportar los datos
        $csv = Excel::create(uniqid() . '_bookings', function($excel) use($dataExport) {
                $excel->setTitle('bookings');
                $excel->setCompany('Experiencia');
                $excel->sheet("bookings", function($sheet) use($dataExport) {
                    $sheet->fromArray($dataExport, null, 'A1', true, false);
                });
            })
            ->store('csv', storage_path('app/public/csv/command/'), true);
        return response()->json(['path' => 'storage/csv/command/' . $csv['file']], 201);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Check if travel information exist in request and asign it to $this->requestHasTravelInformation.
     *
     * @return bool
     */
    private function hasRequestTravelInformation(Request $request)
    {
        $ret = $this->requestHasTravelInformation;
        $fieldsTravelInformation = [];

        if ($ret === null) {
            $fieldsTravelInformation = [
                'travel_by',

                'arrival_place', 'arrival_carrier', 'arrival_date', 'arrival_time', 'arrival_travel_number',

                'departure_place', 'departure_carrier', 'departure_date', 'departure_time',
                'departure_travel_number',
            ];

            foreach ($fieldsTravelInformation as $k => $v) {
                if ($ret = $request->has($v)) {
                    break;
                }
            }
        }

        $this->requestHasTravelInformation = $ret;
        return $ret;
    }

    /**
     * Create validations rules for travel information.
     */
    private function createTravelInformationValidationRules()
    {
        $this->validationRules = array_merge($this->validationRules, [
            'travel_by' => [ 'required', Rule::in(['plane', 'bus']) ],
            'arrival_place' => 'required_without:departure_place|required_with:arrival_carrier,arrival_date,arrival_time,arrival_travel_number',
            'arrival_date' => 'nullable|required_with:arrival_time|date_format:Y-m-d',
            'arrival_time' => 'nullable|required_with:arrival_date|date_format:H:i',
            'departure_place' => 'required_without:arrival_place|required_with:departure_carrier,departure_date,departure_time,departure_travel_number',
            'departure_date' => 'nullable|required_with:departure_time|date_format:Y-m-d',
            'departure_time' => 'nullable|required_with:departure_date|date_format:H:i'
        ]);
    }

    /**
     * Create array with data from request and command to use in create / update travel information.
     *
     * @param Command $command
     * @param Request $request
     * @return array
     */
    private function createDataTravelInformation(Command $command, Request $request): array
    {
        $ret = [];

        $arrivalAt = null;
        if (!empty($request->get('arrival_date'))) {
            $arrivalAt = $request->get('arrival_date') . ' ' . $request->get('arrival_time') . ':00';
        }
        $departureAt = null;
        if (!empty($request->get('departure_date'))) {
            $departureAt = $request->get('departure_date') . ' ' . $request->get('departure_time') . ':00';
        }

        $ret = [
            'travel_by' => $request->get('travel_by'),
            'arrival_place' => $request->get('arrival_place'),
            'arrival_carrier' => $request->get('arrival_carrier'),
            'arrival_at' => $arrivalAt,
            'arrival_travel_number' => $request->get('arrival_travel_number'),
            'departure_place' => $request->get('departure_place'),
            'departure_carrier' => $request->get('departure_carrier'),
            'departure_at' => $departureAt,
            'departure_travel_number' => $request->get('departure_travel_number'),
        ];

        return $ret;
    }

    /**
     * Create if exist a request for travel information.
     */
    private function createTravelInformation(Command $command, Request $request)
    {
        if (!$this->hasRequestTravelInformation(($request))) {
            return false;
        }

        $command->travelInformation()
            ->create($this->createDataTravelInformation($command, $request));
    }

    /**
     * Updet if exist a request for travel information.
     */
    private function updateTravelInformation(Command $command, Request $request)
    {
        if (!$this->hasRequestTravelInformation(($request))) {
            return false;
        }

        $travelInformation = DB::table('travel_information')->where('id', $request->get('id_travel_information'));

        if ($travelInformation->exists()) {
            $travelInformation->update($this->createDataTravelInformation($command, $request));
        } else {
            $command->travelInformation()
                ->create($this->createDataTravelInformation($command, $request));
        }
    }

    /**
     * Store a newly created command in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        if ($this->hasRequestTravelInformation($request)) {
            $this->createTravelInformationValidationRules();
        }

        $this->validationRules['status'] = [
            'required',
            Rule::in(['booked', 'authorized']),
        ];
        $this->validateCommand($request, $this->validationRules);
        // Generar booking code
        $code = uniqid("exp_");
        $agencyId = $request->get('agency_id');
        if ($user->authenticatable_type === 'App\Agency') {
            $agencyId = $user->authenticatable_id;
        }
        $data = [
            'client_first_name' => $request->get('client_first_name'),
            'client_last_name' => $request->get('client_last_name'),
            'client_email' => $request->get('client_email'),
            'pickup_id' => $request->get('pickup_id'),
            'pickup_cost' => empty($request->get('pickup_cost')) ? 0 : $request->get('pickup_cost'),
            'client_id' => $request->get('client_id'),
            'agency_id' => empty($agencyId) ? null : $agencyId,
            'booking_code' => $code,
            'token' => base64_encode(uniqid()),
            'status' => $request->get('status'),
            'travel_partner' => $request->get('travel_partner'),
            'payment_status' => $request->get('payment_status'),
            'comments' => $request->get('comments'),
            'registration_from' => 'admin_site'
        ];
        // Obtener client
        $client = null;
        if (!empty($request->get('client_id'))) {
            $client = \App\Client::find($request->get('client_id'));
            $data['client_first_name'] = $client->first_name;
            $data['client_last_name'] = $client->last_name ;
            $data['client_email'] = $client->email;
        }
        else {
            $client = Client::create([
                'first_name' => $request->get('client_first_name'),
                'last_name' => $request->get('client_last_name'),
                'email' => $request->get('client_email'),
                'country_id' => $request->get('country_id'),
            ]);
            $data['client_id'] = $client->id;
        }
        // Guardar commanda
        $command = Command::create($data);

        // Guardar las facturas en caso de que haya
        $bills = $request->get('command_bills', []);
        if ($bills) {
            // Guardar las facturas
            $command->bills()->createMany($bills);
        }
        // Generar y actualizar codigo
        $now = Carbon::now();
        $prefix = 'MAN';
        // Si la comanda pertenese a una agencia, agregar el código de la agencia como prefijo del booking_code de la comanda
        $agency = $command->agency;
        if ($agency !== null) {
            $prefix = $agency->code;
        }
        $code = $prefix . '-' . $now->format('y') . $now->format('m') . '-' . str_pad($command->id, 5, '0', STR_PAD_LEFT);
        $command->booking_code = $code;
        $command->save();
        $beginDate = null;
        $endDate = null;
        $tmpDate = null;

        // Guardar cursos seleccionados
        $data = $request->get('courses');
        if (!empty($data)) {
            foreach ($data as $r) {
                // Guardar datos de pagos para después acceder a ellos en el CommandCourseObserver
                $request->merge(['currency_id' => $r['currency_id']]);
                $request->merge(['discount' => $r['discount']]);
                $request->merge(['exchange_rates' => $r['exchange_rates']]);
                // Calcular fecha de inicio y termino de la comanda
                $tmpDate = new \Carbon\Carbon($r['from_date']);
                if ($beginDate === null) {
                    $beginDate = $tmpDate;
                }
                else if ($tmpDate->lessThan($beginDate)) {
                    $beginDate = $tmpDate;
                }
                /*
                 Validar que la fecha de termino no sea un fin de semana, siempre y cuando el paquete
                 seleccionado no sea de una sola clase
                */
                $tmpDate = new \Carbon\Carbon($r['to_date']);
                $chosenPackage = DB::table('packages')->where('id', $r['package_id'])
                        ->where('unit', 'class')
                        ->where('quantity', 1)
                        ->whereNull('deleted_at')
                        ->first();
                if ($chosenPackage === null && $tmpDate->isWeekend()) {
                    // Si es fin de semana cambiar la fecha de termino a viernes
                    $tmpDate->subDays($tmpDate->isSunday() ? 2 : 1);
                    $r['to_date'] = $tmpDate->format('Y-m-d');
                }
                if ($endDate === null) {
                    $endDate = $tmpDate;
                }
                else if ($tmpDate->greaterThan($endDate)) {
                    $endDate = $tmpDate;
                }
                //Agregar curso a la reserva
                $commandCourse = $command->commandCourses()->create([
                    'package_id' => $r['package_id'],
                    'quantity' => $r['quantity'],
                    'cost' => $r['cost'],
                    'from_date' => $r['from_date'],
                    'to_date' => $r['to_date'],
                ]);
            }
        }
        // Guardar hospedaje seleccionado
        $data = $request->get('housing');
        if (!empty($data)) {
            foreach ($data as $r) {
                // Guardar datos de pagos para después acceder a ellos en el CommandHousingObserver
                $request->merge(['currency_id' => $r['currency_id']]);
                $request->merge(['discount' => $r['discount']]);
                $request->merge(['exchange_rates' => $r['exchange_rates']]);
                // Agregar hospedaje a la reserva
                $commandHousing = $command->commandHousing()->create([
                    'housing_room_package_id' => $r['housing_room_package_id'],
                    'quantity' => $r['quantity'],
                    'unit' => $r['unit'],
                    'cost' => $r['cost'],
                    'people' => $r['people'],
                    'from_date' => $r['from_date'],
                    'to_date' => $r['to_date'],
                ]);

                // Calcular fecha de inicio y termino de la comanda
                $tmpDate = new \Carbon\Carbon($r['from_date']);
                if ($beginDate === null) {
                    $beginDate = $tmpDate;
                }
                else if ($tmpDate->lessThan($beginDate)) {
                    $beginDate = $tmpDate;
                }
                $tmpDate = new \Carbon\Carbon($r['to_date']);
                if ($endDate === null) {
                    $endDate = $tmpDate;
                }
                else if ($tmpDate->greaterThan($endDate)) {
                    $endDate = $tmpDate;
                }
            }
        }

        // Asignar un caurto temporal a las comandas que no tengan uno
        $this->assignRooms();

        $this->createTravelInformation($command, $request);

        // Crear las compras extras
        $this->createOrUpdateExtraPurchases($command, $request->get('purchases', []));
        $this->createOrUpdateAllPayments($command, $request->get('payments', []));

        $command->update([
            'from_date' => $beginDate? $beginDate->format('Y-m-d') : null,
            'to_date' => $endDate? $endDate->format('Y-m-d') : null,
        ]);

        // Actualizar las relaciones
        $command->commandCourses = $command->commandCourses()->get();
        $command->commandHousing = $command->commandHousing()->get();

        // Guardar logs
        $this->storeLogs($request, $command, 'Create booking');

        // Enviar correo a los admin
        $from = [];
        $role = Role::where('slug', 'administrator')->first();
        $admins = DB::table('users')
            ->select('email', DB::raw("first_name || ' ' || last_name as name"))
            ->where('role_id', $role->id)
            ->whereNull('deleted_at')
            ->get();
        Mail::to($admins)->send(new BookingCreated($command, false, $from));

        $pronoun = trans_choice('models.pronouns.command', 1);
        $description = trans('models.responses.created', ['model' => $pronoun]);

        $manager = new Manager();
        $manager->setSerializer(new DataArraySerializer());
        $resource = new Item($command, new CommandTransformer(), 'command');
        $response = [
            'message' => $description,
            'status_code' => 201,
            'command' => $manager->createData($resource)->toArray()
        ];
        return $this->response()->created(null, $response);
    }

    private function storeLogs(Request $request, Command $command, $action)
    {
        $data = [];
        if ($action === 'Store accommodation' || $action === 'Exchange accommodation') {
            $data = $request->only('command_housing', 'command_housing_exchange');
        } elseif ($action === 'mark payments as invoiced') {
            $data = $request->only('is_paid', 'payments');
        } else {
            $data = $request->except('_token', '_method', 'updated_at', 'status', 'currency_id', 'discount');
        }
        $user = Auth::user();
        $command->commandLogs()->create([
            'user_id' => $user->id,
            'action' => $action,
            'url' => $request->url(),
            'data' => $data
        ]);
    }

    public function createOrUpdateAllPayments(Command $command, array $payments)
    {
        // Validar los pagos
        foreach ($payments as $key => $payment) {
            if ($payment['type'] !== 'registration_fee' && $payment['type'] !== 'reservation_payment') {
                $requestTmp = new Request();
                $requestTmp->merge($payment);
                $this->validatePayment($requestTmp, $this->paymentValidationRules);
            }
        }
        // Eliminar todos los pagos de la comanda excepto el del pago del fee y el pago total de la reservación
        $command->payments()->whereNotIn('type', ['registration_fee', 'reservation_payment'])->delete();
        foreach ($payments as $key => $payment) {
            if ($payment['type'] !== 'registration_fee' && $payment['type'] !== 'reservation_payment') {
                // Guardar solo los pagos que no sean el pago del fee y el pago total de la reservación
                $requestTmp = new Request();
                $requestTmp->merge($payment);
                $this->storePayment($command->id, $requestTmp);
            }
        }
    }

    /**
     * Get the specified booking.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, CommandTransformer $commandTransformer)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $pronoun = trans_choice('models.pronouns.command', 1);
        $command = $this->getCommand($id);
        // Si el usuario logueado es un cliente, solo puede ver su comanda y si el usuario logueado es una agencia, solo puede ver sus comandas
        if (in_array($user->authenticatable_type, ["App\Client", "App\Agency"])) {
            $this->checkUser($command, $user);
        }
        return $this->response->item($command, $commandTransformer, [], function($resource, $fractal) {
            $exclude = '';
            if (isset($_GET['exclude'])) {
                $exclude .= ',' . $_GET['exclude'];
            }
            $include = 'payments,commandCourses,commandHousing,pickup';
            if (isset($_GET['include'])) {
                $include = $_GET['include'];
            }
            $fractal->parseExcludes($exclude);
            $fractal->parseIncludes($include);
        });
    }

    /**
     * Get the specified booking.
     *
     * @param  int  $token
     * @return \Illuminate\Http\Response
     */
    public function getByToken($token, CommandTransformer $commandTransformer)
    {
        $command = null;
        $pronoun = trans_choice('models.pronouns.command', 1);
        try {
            $command = Command::where('token', $token)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            $description = trans('models.responses.not_found', ['model' => $pronoun]);
            throw new NotFoundHttpException($description);
        }
        return $this->response->item($command, $commandTransformer);
    }

    /**
     * Check the command owner
     * @param \App\Command $command
     * @param \App\User $user
     * @throws AccessDeniedHttpException
     */
    private function checkUser($command, $user) {
        $pronoun = trans_choice('models.pronouns.command', 1);
        $allowed = true;
        if (in_array($user->authenticatable_type, ['App\Client', 'App\Agency'])) {
            if ($user->authenticatable_type === 'App\Client') {
                if ($command->client_id != $user->authenticatable_id) {
                    $allowed = false;
                }
            }
            else if ($user->authenticatable_type === 'App\Agency') {
                if ($command->agency_id != $user->authenticatable_id) {
                    $allowed = false;
                }
            }
            if (!$allowed) {
                throw new AccessDeniedHttpException(trans('models.responses.not_allowed', [
                    'model' => $pronoun
                ]));
            }
        }
    }

    /**
     * Update the specified command in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $isAgency = false;

        if ($this->hasRequestTravelInformation($request)) {
            $this->createTravelInformationValidationRules();
        }

        $command = $this->getCommand($id);
        // Si el usuario logueado es una agencia, validar que la comanda le pertenezca
        if (in_array($user->authenticatable_type, ['App\Agency'])) {
            $this->checkUser($command, $user);
            $isAgency = true;
        }
        $pronoun = trans_choice('models.pronouns.command', 1);
        $this->validationRules['updated_at'] = 'required|date_format:Y-m-d H:i:s';
        $this->validationRules['housing.*.id'] = 'required';
        $this->validationRules['pickup_exchange_rates'] = 'required|array';
        $this->validationRules['command_bills.*.reference_number'] = [
                'required_with:command_bills.*.amount,command_bills.*.currency_id',
                'string',
                'max:255',
            ];
        $this->validationRules['status'] = [
            'required',
            Rule::in(['booked', 'authorized']),
        ];
        $this->validateCommand($request, $this->validationRules, $command);
        // Checar que la comanda no haya sido modificada previamente
        if ($command->updated_at->greaterThan(new Carbon($request->get('updated_at')))) {
            $description = trans('models.responses.conflict', ['model' => $pronoun]);
            throw new ConflictHttpException($description);
        }

        $courses = $request->get('courses');
        if (empty($courses)) {
            $courses = [];
        }
        $commandCourses = $command->commandCourses;
        // Borrar cursos de la comanda que no existan en el arreglo 'courses'
        foreach ($commandCourses as $cc) {
            $exists = false;
            foreach ($courses as $r) {
                if ($cc->id == $r['id']) {
                    $exists = true;
                    break;
                }
            }
            if (!$exists) {
                $cc->delete();
            }
        }
        $beginDate = null;
        $endDate = null;
        $tmpDate = null;
        // Actualizar cursos de la comanda
        foreach ($courses as $r) {
            // Guardar datos de pagos para después acceder a ellos en el CommandCourseObserver
            $request->merge(['currency_id' => $r['currency_id']]);
            $request->merge(['discount' => $r['discount']]);
            $request->merge(['exchange_rates' => $r['exchange_rates']]);
            $exists = false;
            // Checar si ya existe el curso de la comanda
            foreach ($commandCourses as $cc) {
                if ($cc->id == $r['id']) {
                    $exists = true;
                    break;
                }
            }
            // Calcular fecha de inicio y termino de la comanda
            $tmpDate = new \Carbon\Carbon($r['from_date']);
            if ($beginDate === null) {
                $beginDate = $tmpDate;
            }
            else if ($tmpDate->lessThan($beginDate)) {
                $beginDate = $tmpDate;
            }
            /*
             Validar que la fecha de termino no sea un fin de semana, siempre y cuando el paquete
             seleccionado no sea de una sola clase
            */
            $tmpDate = new \Carbon\Carbon($r['to_date']);
            $chosenPackage = DB::table('packages')->where('id', $r['package_id'])
                    ->where('unit', 'class')
                    ->where('quantity', 1)
                    ->whereNull('deleted_at')
                    ->first();
            if ($chosenPackage === null && $tmpDate->isWeekend()) {
                // Si es fin de semana cambiar la fecha de termino a viernes
                $tmpDate->subDays($tmpDate->isSunday() ? 2 : 1);
                $r['to_date'] = $tmpDate->format('Y-m-d');
            }
            if ($endDate === null) {
                $endDate = $tmpDate;
            }
            else if ($tmpDate->greaterThan($endDate)) {
                $endDate = $tmpDate;
            }
            // Si no existe, agregarlo
            if (!$exists) {
                //Agregar curso a la reserva
                $command->commandCourses()->create($r);
            }
            // Si existe, actualizar datos
            else {
                $cc->update($r);
            }
        }

        $housing = $request->get('housing');
        if (empty($housing)) {
            $housing = [];
        }
        $commandHousing= $command->commandHousing;
        // Borrar hospedaje de la comanda que no existan en el arreglo 'housing'
        foreach ($commandHousing as $ch) {
            $exists = false;
            foreach ($housing as $r) {
                if ($ch->id == $r['id']) {
                    $exists = true;
                    break;
                }
            }
            if (!$exists) {
                $ch->delete();
            }
        }
        // Actualizar hospedaje de la comanda
        foreach ($housing as $r) {
            $exists = false;
            $request->merge(['currency_id' => $r['currency_id']]);
            $request->merge(['discount' => $r['discount']]);
            $request->merge(['exchange_rates' => $r['exchange_rates']]);
            // Checar si ya existe el hospedaje de la comanda
            foreach ($commandHousing as $ch) {
                if ($ch->id == $r['id']) {
                    $exists = true;
                    break;
                }
            }
            // Calcular fecha de inicio y termino de la comanda
            $tmpDate = new \Carbon\Carbon($r['from_date']);
            if ($beginDate === null) {
                $beginDate = $tmpDate;
            }
            else if ($tmpDate->lessThan($beginDate)) {
                $beginDate = $tmpDate;
            }
            $tmpDate = new \Carbon\Carbon($r['to_date']);
            if ($endDate === null) {
                $endDate = $tmpDate;
            }
            else if ($tmpDate->greaterThan($endDate)) {
                $endDate = $tmpDate;
            }
            // Si no existe, agregarlo
            if (!$exists) {
                $command->commandHousing()->create($r);
            }
            // Si existe, actualizar datos
            else {
                // Asignar los valores que se van a guardar
                foreach ($ch->getFillable() as $attribute) {
                    if ($attribute === 'id') {
                        continue;
                    }
                    if (isset($r[$attribute])) {
                        $ch->$attribute = $r[$attribute];
                    }
                }
                /*
                Si cambió el paquete o la fecha de inico o la fecha de fin o el número de personas, también debe de
                cambiar el cuarto
                */
                if ($ch->isDirty(['housing_room_package_id', 'from_date', 'to_date', 'people'])) {
                    $ch->room_id = null;
                }
                $ch->save();
            }
        }

        // Asignar un caurto temporal a las comandas que no tengan uno
        $this->assignRooms();

        // Actualizar datos de la comanda
        $data = [
            'pickup_id' => $request->get('pickup_id'),
            'pickup_cost' => empty($request->get('pickup_cost')) ? 0 : $request->get('pickup_cost'),
            'client_id' => $request->get('client_id'),
            'agency_id' => $isAgency? $user->authenticatable_id : $request->get('agency_id'),
            'from_date' => $beginDate? $beginDate->format('Y-m-d') : null,
            'to_date' => $endDate? $endDate->format('Y-m-d') : null,
            'travel_partner' => $request->get('travel_partner'),
            'payment_status' => $request->get('payment_status'),
            'comments' => $request->get('comments'),
        ];
        // Obtener client
        $client = null;
        if (!empty($request->get('client_id'))) {
            $client = \App\Client::find($request->get('client_id'));
            $data['client_first_name'] = $client->first_name;
            $data['client_last_name'] = $client->last_name ;
            $data['client_email'] = $client->email;
        }
        else {
            $client = Client::create([
                'first_name' => $request->get('client_first_name'),
                'last_name' => $request->get('client_last_name'),
                'email' => $request->get('client_email'),
                'country_id' => $request->get('country_id'),
            ]);
            $data['client_id'] = $client->id;
        }

        $this->updateTravelInformation($command, $request);

        $command->update($data);
        $command->updateBookingCode();

        // Guardar las facturas en caso de que haya
        $bills = $request->get('command_bills', []);
        // Eliminar todas las facturas de la comanda
        $command->bills()->delete();
        if ($bills) {
            // Guardar las facturas
            $command->bills()->createMany($bills);
        }

        // Actualizar las compras extras
        $this->createOrUpdateExtraPurchases($command, $request->get('purchases', []));
        $this->createOrUpdateAllPayments($command, $request->get('payments', []));

        // Guardar logs
        $this->storeLogs($request, $command, 'Update booking');

        // Verificar si se debe de enviar el correo
        if ($request->has('send_email') && $request->input('send_email') == 1) {
            $from = [];
            $client = [];
            if ($command->client) {
                $client = [
                    'name' => $command->client->first_name . ' ' . $command->client->last_name,
                    'email' => $command->client->email
                ];
            } else {
                $client = [
                    'name' => $command->client_first_name . ' ' . $command->client_last_name,
                    'email' => $command->client_email
                ];
            }
            // Actualizar reservaciones
            $command->commandCourses = $command->commandCourses()->get();
            $command->commandHousing = $command->commandHousing()->get();

            // Enviar correo al cliente
            Mail::to((object) $client)->send(new BookingCreated($command, true, $from));

            // Enviar correo a los admin
            $role = Role::where('slug', 'administrator')->first();
            $admins = DB::table('users')
                ->select('email', DB::raw("first_name || ' ' || last_name as name"))
                ->where('role_id', $role->id)
                ->whereNull('deleted_at')
                ->get();
            Mail::to($admins)->send(new BookingCreated($command, false, $from));
        }

        $description = trans('models.responses.updated', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $command = $this->getCommand($id);
        $pronoun = 'command';
        if ($command->status === 'booked') {
            $pronoun = 'booking';
        }
        $pronoun = trans_choice("models.pronouns.$pronoun", 1);

        // Si el usuario logueado es una agencia, solo puede borrar sus comandas
        if ($user->authenticatable_type === "App\Agency") {
            $this->checkUser($command, $user);
        }

        $description = trans('models.responses.deleted', ['model' => $pronoun]);
        // Borrar comanda
        $command->delete();
        $response = ['message' => $description, 'status_code' => 200];
        return $this->responseOk($response);
    }

    /**
     * Store a command booking in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function book(Request $request)
    {
        $validator = Validator::make($request->all(), $this->bookingValidationRules);
        $validator->after(function ($validator) use($request) {
            $messages = \Illuminate\Support\Arr::dot((array) trans('validation.custom'));
            // Validar datos de los cursos seleccionados
            $data = $request->get('courses', []);
            $occupancy = 0;
            foreach ($data as $i => $r) {
                $package = \App\Package::find($r['package_id']);
                if (!empty($package)) {
                    if ($package->type !== 'experience') {
                        $description = trans('validation.in', ['attribute' => trans('validation.attributes.package_id')]);
                        $validator->errors()->add("courses.$i.package_id", $description);
                    }
                }
            }
            // Validar datos del hospedaje
            $data = $request->get('housing', []);
            foreach ($data as $i => $r) {
                if (!in_array($r['unit'], ['night', 'week', 'month'])) {
                    $description = trans('validation.in', ['attribute' => trans('validation.attributes.unit')]);
                    $validator->errors()->add("housing.$i.unit", $description);
                }
                if (isset($r['from_date']) && isset($r['to_date'])) {
                    // Validar fecha de termino respecto al numero de dias o semanas seleccionado
                    $beginDate = new \Carbon\Carbon($r['from_date'] . ' 00:00:00');
                    $endDate = new \Carbon\Carbon($r['to_date'] . ' 00:00:00');
                    // Para hospedaje la semana es completa, de 7 dias
                    $days = $r['quantity'];
                    if ($r['unit'] === 'week') {
                        // Para hospedaje la semana es completa, de 7 dias
                        $days *= 7;
                    } elseif ($r['unit'] === 'month') {
                        $days *= 28; // 1 mes = 4 semanas, 1 semana = 7 días, 7 * 4 = 28 días del mes
                    }
                    if ($endDate->diffInDays($beginDate) != $days) {
                        $description = str_replace(':quantity', $r['quantity'], $messages['housing.*.to_date.val']);
                        $description = str_replace(':unit', trans_choice('validation.attributes.' . $r['unit'], (int) $r['quantity']), $description);
                        $validator->errors()->add("housing.$i.to_date", $description);
                    }
                    if (!empty($r['housing_room_package_id'])) {
                        $result = DB::table('housing_room_packages')
                                ->select('housing_room_id')
                                ->where('id', $r['housing_room_package_id'])
                                ->first();
                        if (!empty($result)) {
                            // Obtener total de camas del tipo de cuarto seleccionado
                            $totalBeds = $this->getTotalBedsOfARoomType($result->housing_room_id, $r['from_date'], $r['to_date']);
                            $occupancy = $this->getOcuppancy($r['from_date'], $r['to_date'], $result->housing_room_id, $r['people']);
                            if ($totalBeds < $occupancy) {
                                $description = $messages['housing.*.housing_room_package_id.unavailable'];
                                $validator->errors()->add("housing.$i.housing_room_package_id", $description);
                            }
                        }
                    }
                }
            }
        });
        $pronoun = trans_choice('models.pronouns.booking', 1);
        // Si falla la validación
        if ($validator->fails()) {
            // Responder con los errores
            $description = trans('models.responses.not_created', ['model' => $pronoun]);
            throw new StoreResourceFailedException($description, $validator->errors());
        }
        // Obtener costo de la recogida en aeropuerto
        $pickup = null;
        if (!empty($request->get('pickup_id'))) {
            $pickup = \App\Pickup::find($request->get('pickup_id'));
        }
        // Generar booking code
        $code = uniqid("exp_");

        // Guardar reserva
        $command = Command::create([
            'client_first_name' => $request->get('client_first_name'),
            'client_last_name' => $request->get('client_last_name'),
            'client_email' => $request->get('client_email'),
            'client_phone' => $request->get('client_phone'),
            'client_comments' => $request->get('client_comments'),
            'travel_partner' => $request->get('travel_partner'),
            'pickup_id' => $pickup === null ? null : $pickup->id,
            'status' => 'booked',
            'booking_code' => $code,
            'token' => base64_encode(uniqid()),
            'pickup_cost' => $pickup === null ? 0 : $pickup->cost,
            'registration_from' => 'public_site'
        ]);
        // Generar y actualizar codigo
        $now = Carbon::now();
        $code = "WWW-" . $now->format('y') . $now->format('m') . '-' . str_pad($command->id, 5, '0', STR_PAD_LEFT);
        $command->booking_code = $code;
        $command->save();
        $beginDate = null;
        $endDate = null;
        $tmpDate = null;
        // Guardar cursos seleccionados
        $data = $request->get('courses', []);
        foreach ($data as $r) {
            // Buscar tarifa del curso
            $result = DB::table('course_rates')
                    ->select('unit_cost')
                    ->where('package_id', $r['package_id'])
                    ->where(function($query) use ($r) {
                        $query->whereRaw($r['quantity'] . ' between quantity and to_quantity')
                                ->orWhere('quantity', '<=', $r['quantity']);
                    })
                    ->whereNull('deleted_at')
                    ->orderBy('quantity', 'desc')
                    ->first();
            $cost = 0;
            if (!empty($result)) {
                $cost = $result->unit_cost * $r['quantity'];
            }
            // Calcular fecha de inicio y termino de la comanda
            if (!empty($r['to_date'])) {
                /*
                 Validar que la fecha de termino no sea un fin de semana, siempre y cuando el paquete
                 seleccionado no sea de una sola clase
                */
                $tmpDate = new \Carbon\Carbon($r['to_date']);
                $chosenPackage = DB::table('packages')->where('id', $r['package_id'])
                        ->where('unit', 'class')
                        ->where('quantity', 1)
                        ->whereNull('deleted_at')
                        ->first();
                if ($chosenPackage === null && $tmpDate->isWeekend()) {
                    // Si es fin de semana cambiar la fecha de termino a viernes
                    $tmpDate->subDays($tmpDate->isSunday() ? 2 : 1);
                    $r['to_date'] = $tmpDate->format('Y-m-d');
                }
            }
            if (empty($request->get('housing', [])) && (!empty($r['from_date']) && !empty($r['to_date']))) {
                $tmpDate = new \Carbon\Carbon($r['from_date']);
                if ($beginDate === null) {
                    $beginDate = $tmpDate;
                }
                else if ($tmpDate->lessThan($beginDate)) {
                    $beginDate = $tmpDate;
                }
                $tmpDate = new \Carbon\Carbon($r['to_date']);
                if ($endDate === null) {
                    $endDate = $tmpDate;
                }
                else if ($tmpDate->greaterThan($endDate)) {
                    $endDate = $tmpDate;
                }
            }
            //Agregar curso a la reserva
            $command->commandCourses()->create([
                'package_id' => $r['package_id'],
                'quantity' => $r['quantity'],
                'from_date' => !empty($r['from_date']) ? $r['from_date'] : null,
                'to_date' => !empty($r['to_date']) ? $r['to_date'] : null,
                'cost' => $cost,
            ]);
        }
        // Guardar hospedaje seleccionado
        $data = $request->get('housing', []);
        foreach ($data as $r) {
            // Buscar tarifa de hospedaje
            $result = DB::table('housing_rates')
                    ->select('unit_cost')
                    ->where('housing_room_package_id', $r['housing_room_package_id'])
                    ->where(function($query) use($r) {
                        $query->where(function($query1) use($r) {
                                $query1->where(function($query0) use($r) {
                                            $query0->where('from_date', '>=', $r['from_date'])
                                                    ->where('from_date', '<=', $r['to_date']);
                                        })
                                        ->orWhere(function($query0) use($r) {
                                            $query0->where('to_date', '<=', $r['to_date'])
                                                    ->where('to_date', '>=', $r['from_date']);
                                        })
                                        ->orWhere(function($query0) use($r) {
                                            $query0->where('from_date', '<=', $r['from_date'])
                                                    ->where('to_date', '>=', $r['to_date']);
                                        });
                            })
                            ->orWhere(function($query0) {
                                $query0->whereNull('from_date')
                                        ->whereNull('to_date');
                            });
                    })
                    ->where('unit', $r['unit'])
                    ->whereNull('deleted_at')
                    ->orderBy('from_date', 'asc')
                    ->first();
            $unitCost = 0;
            if (!empty($result)) {
                $unitCost = $result->unit_cost;
            }
            $housingRoom = DB::table('housing_room_packages')
                    ->leftJoin('housing_rooms', 'housing_rooms.id', '=', 'housing_room_packages.housing_room_id')
                    ->select('cost_type')
                    ->where('housing_room_packages.id', $r['housing_room_package_id'])
                    ->first();
            $beds = 1;
            if (!empty($housingRoom) && $housingRoom->cost_type === 'bed') {
                $beds = $r['people'];
            }
            // Agregar hospedaje a la reserva
            $command->commandHousing()->create([
                'housing_room_package_id' => $r['housing_room_package_id'],
                'quantity' => $r['quantity'],
                'unit' => $r['unit'],
                'people' => $r['people'],
                'from_date' => !empty($r['from_date']) ? $r['from_date'] : null,
                'to_date' => !empty($r['to_date']) ? $r['to_date'] : null,
                'cost' => ($unitCost * $r['quantity']) * $beds,
            ]);
            // Calcular fecha de inicio y termino de la comanda
            $tmpDate = new \Carbon\Carbon($r['from_date']);
            if ($beginDate === null) {
                $beginDate = $tmpDate;
            }
            else if ($tmpDate->lessThan($beginDate)) {
                $beginDate = $tmpDate;
            }
            $tmpDate = new \Carbon\Carbon($r['to_date']);
            if ($endDate === null) {
                $endDate = $tmpDate;
            }
            else if ($tmpDate->greaterThan($endDate)) {
                $endDate = $tmpDate;
            }
        }

        // Asignar un caurto temporal a las comandas que no tengan uno
        $this->assignRooms();

        $command->update([
            'from_date' => $beginDate->format('Y-m-d'),
            'to_date' => $endDate->format('Y-m-d'),
        ]);
        // Enviar email de confirmación del booking con links de pago y form de datos del cliente
        $from = [];
        /*$from['email'] = $request->get('email_reservations');
        $from['name'] = null;*/
        $client = [
            'name' => $command->client_first_name . ' ' . $command->client_last_name,
            'email' => $command->client_email
        ];
        $command->commandCourses = $command->commandCourses()->get();
        $command->commandHousing = $command->commandHousing()->get();
        // Enviar correo al cliente
        Mail::to((object) $client)->send(new BookingCreated($command, true, $from));
        // Enviar correo al admin
        $role = Role::where('slug', 'administrator')->first();
        $admins = DB::table('users')
                ->select('email', DB::raw("first_name || ' ' || last_name as name"))
                ->where('role_id', $role->id)
                ->whereNull('deleted_at')
                ->get();
        Mail::to($admins)->send(new BookingCreated($command, false, $from));
        $pronoun = trans_choice('models.pronouns.booking', 1);
        $description = trans('models.responses.created', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 201, 'data' => $command];
        return $this->response()->created(null, $response);
    }

    /**
     * Store accommodation.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeAccommodation($id, Request $request)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $command = $this->getCommand($id);
        $pronoun = trans_choice('models.pronouns.command', 1);

        // Si no hay usuario logueado es poque la acomodación se está haciendo desde el booking público
        if (!empty($user)) {
            // Si hay usuario logueado, solo puede acceder un usuario admin y agencia
            if (!empty($user->authenticatable_type)) {
                // Si el usuario logueado es agencia, solo puede acomadar en sus comandas
                if ($user->authenticatable_type === 'App\Agency') {
                    $this->checkUser($command, $user);
                } else {
                    throw new AccessDeniedHttpException(trans('models.responses.not_allowed', [
                        'model' => $pronoun
                    ]));
                }
            }
        }

        $pronoun = trans_choice('models.pronouns.accommodation', 1);

        // Validar datos
        $validator = Validator::make($request->all(), [
            'command_housing_id' => 'required|exists:command_housing,id',
            'room_id' => 'required|exists:rooms,id',
            'exchange' => 'required|in:1,0',
            'command_housing_exchange' => 'required_if:exchange,1|nullable|exists:command_housing,id'
        ]);

        // Si falla la validación
        if ($validator->fails()) {
            $description = trans('models.responses.not_created', ['model' => $pronoun]);
            throw new StoreResourceFailedException($description, $validator->errors());
        }
        $exchange = $request->input('exchange');
        $commandHousingExchange = null;
        $commandHousing = \App\CommandHousing::find($request->get('command_housing_id'));

        if ($exchange == 1) {
            $commandHousingExchange = \App\CommandHousing::find($request->input('command_housing_exchange'));
        }

        $roomId = $exchange == 1 ? $commandHousing->room_id : $request->input('room_id');
        $room = \App\Room::find($roomId);

        $validator->after(function($validator) use($command, $commandHousing, $room, $exchange, $commandHousingExchange) {
            if ($command->id != $commandHousing->command_id) {
                $description = trans('validation.in', ['attribute' => 'Command housing']);
                $validator->errors()->add("command_housing_id", $description);
            }
            // Validar que el tipo de hospedaje de la comanda pertenezca al mismo tipo de hospedaje del cuarto
            $housingRoomPackage1 = DB::table('housing_room_packages')
                ->select('housing_room_id')
                ->where('id', $commandHousing->housing_room_package_id)
                ->whereNull('deleted_at')
                ->first();
            if (empty($housingRoomPackage1)) {
                $description = trans('validation.custom.accommodation.room_id.in');
                $validator->errors()->add("room_id", $description);
            } elseif (!in_array($housingRoomPackage1->housing_room_id, $room->housingRooms->pluck('id')->toArray())) {
                $description = trans('validation.custom.accommodation.room_id.in');
                $validator->errors()->add("room_id", $description);
            }
            // Validar la disponibilidad del cuarto
            if (empty($commandHousing->from_date) || empty($commandHousing->to_date)) {
                $description = trans('validation.in', ['attribute' => 'Command housing']);
                $validator->errors()->add("command_housing_id", $description);
            } else {
                // Validar que el cuarto esté disponilbe
                $isAvailible = $this->validateRoomAvailability($room->id, $commandHousing->housingRoom->cost_type, $commandHousing->from_date, $commandHousing->to_date, $commandHousing->people, $commandHousing->id);

                if (!$isAvailible) {
                    $description = trans('validation.custom.accommodation.room_id.taken');
                    $validator->errors()->add("room_id", $description);
                }
            }
            if ($exchange == 1 && !empty($housingRoomPackage1)) {
                $housingRoomPackage2 = DB::table('housing_room_packages')
                    ->select('housing_room_id')
                    ->where('id', $commandHousingExchange->housing_room_package_id)
                    ->whereNull('deleted_at')
                    ->first();
                // Validar que exista el paquete
                if (!empty($housingRoomPackage2)) {
                    // Validar que las comandas a intercambiar sean del mismo tipo de cuarto y que no sean las mismas
                    if (
                        $housingRoomPackage2->housing_room_id == $housingRoomPackage1->housing_room_id &&
                        $commandHousingExchange->id != $commandHousing->id
                    ) {
                        // Validar la disponibilidad de los cuartos
                        $isAvailible1 = $this->validateRoomAvailability($commandHousingExchange->room_id, $commandHousing->housingRoom->cost_type, $commandHousing->from_date, $commandHousing->to_date, $commandHousing->people, $commandHousingExchange->id);

                        $isAvailible2 = $this->validateRoomAvailability($room->id, $commandHousingExchange->housingRoom->cost_type, $commandHousingExchange->from_date, $commandHousingExchange->to_date, $commandHousingExchange->people, $commandHousing->id);

                        if (!$isAvailible1 || !$isAvailible2) {
                            $description = trans('validation.in', ['attribute' => 'booking']);
                            $validator->errors()->add("command_housing_exchange", $description);
                        }
                    } else {
                        $description = trans('validation.in', ['attribute' => 'booking']);
                        $validator->errors()->add("command_housing_exchange", $description);
                    }
                } else {
                    $description = trans('validation.exists', ['attribute' => 'booking']);
                    $validator->errors()->add("command_housing_exchange", $description);
                }
            }

        });

        if ($exchange == 1) {
            $pronoun = trans_choice('models.pronouns.exchange', 1);
        }
        // Si falla la validación
        if ($validator->fails()) {
            $description = trans('models.responses.not_created', ['model' => $pronoun]);
            throw new StoreResourceFailedException($description, $validator->errors());
        }
        // Obtener la información para guardar en el log
        $oldRoom = $commandHousing->room;
        $housingDataLog = ['command_housing' => [
                'id' => $commandHousing->id,
                'name' => $this->commandHousingToString($commandHousing),
                // Datos del cuarto que tendrá la comanda principal antes de ser modificado
                'before' => [
                    'room_id' => !empty($oldRoom) ? $oldRoom->id : null,
                    'room' => !empty($oldRoom) ? $oldRoom->name : null
                ],
            ]];
        if ($exchange == 1) {
            // Datos de la comanda de intercambio que se guardarán en el log
            $housingExchangeDataLog = ['command_housing_exchange' => [
                    'id' => $commandHousingExchange->id,
                    'name' => $this->commandHousingToString($commandHousingExchange),
                    // Datos del cuarto que tendrá la comanda de intercambio después del intercambio
                    'after' => [
                        'room_id' => !empty($oldRoom) ? $oldRoom->id : null,
                        'room' => !empty($oldRoom) ? $oldRoom->name : null
                    ]
                ]];

            $oldRoom = $commandHousingExchange->room;

            // Datos del caurto que tendrá la comanda principal después del intercambio
            $housingDataLog['command_housing']['after'] = [
                'room_id' => !empty($oldRoom) ? $oldRoom->id : null,
                'room' => !empty($oldRoom) ? $oldRoom->name : null
            ];
            // Datos del cuarto que tendrá la comanda de intercambio antes del intercambio
            $housingExchangeDataLog['command_housing_exchange']['before'] = [
                'room_id' => !empty($oldRoom) ? $oldRoom->id : null,
                'room' => !empty($oldRoom) ? $oldRoom->name : null
            ];

            // Hacer el intercambio de cuartos
            $commandHousing->update([
                'room_id' => $commandHousingExchange->room_id
            ]);
            $commandHousingExchange->update([
                'room_id' => $room->id
            ]);
            $request->merge($housingExchangeDataLog);
        } else {
            $housingDataLog['command_housing']['after'] = [
                'room_id' => $room->id,
                'room' => $room->name
            ];

            // Guardar el cuarto
            $commandHousing->update([
                'room_id' => $room->id
            ]);
        }
        $request->merge($housingDataLog);
        // Guardar logs
        $this->storeLogs($request, $command, $exchange == 1 ? 'Exchange accommodation' : 'Store accommodation');
        $description = trans('models.responses.created', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 201];
        return $this->response()->created(null, $response);
    }

    private function commandHousingToString($commandHousing) {
        return $commandHousing->housingRoomPackage->housingRoom->housing->name . '/' .
            $commandHousing->housingRoomPackage->housingRoom->name . '/' .
            $commandHousing->housingRoomPackage->package->name . '/' .
            (new Carbon($commandHousing->from_date))->format('d-m-Y') . '/' .
            (new Carbon($commandHousing->to_date))->format('d-m-Y');
    }

    /**
     * Validate the payment data
     * @param \Illuminate\Http\Request $request The data send by the user
     * @param array $validationRules Payment validation rules
     * @throws StoreResourceFailedException|UpdateResourceFailedException
     */
    private function validatePayment(Request $request, array $validationRules)
    {
        $pronoun = trans_choice('models.pronouns.payment', 1);
        $validationRules['type'] = [
            'required',
            Rule::in(['registration_fee', 'reservation_payment', 'purchase_payment', 'other'])
        ];
        $validator = Validator::make($request->all(), $validationRules);
        // Si falla la validación
        if ($validator->fails()) {
            // Responder con los errores
            if ($request->isMethod('post')) {
                $description = trans('models.responses.not_created', ['model' => $pronoun]);
                throw new StoreResourceFailedException($description, $validator->errors());
            }
            else {
                $description = trans('models.responses.not_updated', ['model' => $pronoun]);
                throw new UpdateResourceFailedException($description, $validator->errors());
            }
        }
    }

    /**
     * Store a newly created payment in storage.
     *
     * @param int $id The command id
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storePayment($id, Request $request)
    {
        $currency = Currency::with('exchangeRates')->where('code', 'USD')->first();
        $paymentMethodId = MethodPayment::where('slug', 'paypal')->value('id');
        $exchangeRates = $currency->exchangeRates;
        $exchangeRates = $exchangeRates->map(function($item) {
                    return [
                        'amount' => $item->amount,
                        'base_currency_id' => $item->base_currency_id,
                        'to_currency_id' => $item->to_currency_id,
                        'base_amount' => $item->base_amount
                    ];
                })
                ->toArray();

        $pronoun = trans_choice('models.pronouns.payment', 1);
        $command = $this->getCommand($id);
        if ($request->routeIs('commands.payments.store')) {
            $this->validatePayment($request, $this->paymentValidationRules);
        }
        if ($request->input('type') === 'registration_fee' || $request->input('type') === 'reservation_payment') {
            $count = $command->payments()
                    ->whereIn('type', ['registration_fee', 'reservation_payment'])
                    ->count();
            if ($count > 0) {
                $description = "Your request wasn't processed, due to your booking has already been paid.";
                throw new StoreResourceFailedException($description);
            }
        }
        if (!empty($request->input('exchange_rates'))) {
            $exchangeRates = $request->input('exchange_rates');
        }
        // Crear pago
        $command->payments()->create([
            'concept' => $request->input('concept'),
            'amount' => $request->input('amount'),
            'paid_at' => $request->input('paid_at'),
            'type' => $request->input('type'),
            'vendor' => empty($request->input('vendor')) ? null : $request->input('vendor'),
            'reference_id' => empty($request->input('reference_id')) ? null : $request->input('reference_id'),
            'currency_id' => $request->input('currency_id') ? $request->input('currency_id') : $currency->id,
            'method_payment_id' => $request->input('payment_method_id') ? $request->input('payment_method_id') : $paymentMethodId,
            'invoiced_at' => $request->input('invoiced_at') ?  $request->input('invoiced_at') : null,
            'exchange_rates' => $exchangeRates,
        ]);
        if ($request->input('type') === 'reservation_payment') {
            // El pago se hizo con paypal, por lo tanto se pagó todo, entonces hay que cambiar el estatus a pagado
            $command->payment_status = 'paid';
            $command->save();
        }
        $client = null;
        $admins = [];
        if ($request->get('send_mail') == 1) {
            // Enviar correo al cliente
            $client = [
                'name' => $command->client_first_name . ' ' . $command->client_last_name,
                'email' => $command->client_email
            ];
            Mail::to((object) $client)->send(new PaymentReceived($command, true));
            // Enviar correo al admin
            $role = Role::where('slug', 'administrator')->first();
            $admins = DB::table('users')
                    ->select('email', DB::raw("first_name || ' ' || last_name as name"))
                    ->where('role_id', $role->id)
                    ->whereNull('deleted_at')
                    ->get();
            Mail::to($admins)->send(new PaymentReceived($command, false));
        }
        $description = trans('models.responses.created', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 201];
        return $this->response()->created(null, $response);
    }

    public function createOrUpdateExtraPurchases(Command $command, Array $purchases)
    {
        // Eliminar las compras la comanda
        $extraPurchases = $command->extraPurchases;
        foreach ($extraPurchases as $ep) {
            $ep->delete();
        }
        // Guardar las compras
        foreach ($purchases as $key => $purchase) {
            $purchaseCreated = Purchase::create([
                'date' => $purchase['date'],
                'reference_number' => $purchase['reference_number'],
                'concept' => $purchase['concept'],
                'amount' => $purchase['amount'],
                'command_id' => $command->id
            ]);
            Purchasable::create([
                'discount' => $purchase['discount'],
                'consumption_center_id' => $purchase['consumption_center_id'],
                'currency_id' => $purchase['currency_id'],
                'client_id' => $command->client->id,
                'purchasable_id' => $purchaseCreated->id,
                'purchasable_type' => Purchase::class,
                'user_id' => null,
                'exchange_rates' => $purchase['exchange_rates'],
            ]);
        }
    }

    /**
     * Mark as invoiced the specified payment in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function markPaymentsAsInvoiced(Request $request)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);
        $pronoun = trans_choice('models.pronouns.payment', 1);

        $command = $this->getCommand($request->input('command_id', 0));
        $payments = $request->get('payments', []);
        $isPaid = $request->get('is_paid', 1);

        // Data que se va a guardar en el log
        $data = ['payments' => [
                'before' => [],
                'after' => []
            ]];
        // Obtener los pagos que se van a guardar en el log
        $paymentsTmp = Payment::with(['currency', 'paymentMethod'])
                ->whereIn('id', $payments)
                ->where('command_id', $command->id)
                ->get();
        // Guardar la información antes de ser modificada
        $data['payments']['before'] = $paymentsTmp->toArray();

        // Marcar ordenes de pago como pagados
        $invoicedAt = '';
        Payment::whereIn('id', $payments)
                ->where('command_id', $command->id)
                ->update([
                    'invoiced_at' => ($isPaid == 1 ? $invoicedAt = Carbon::now()->format('Y-m-d H:i:s') : $invoicedAt = null)
                ]);
        // Validar si la comando ya tiene responsable
        if (!empty($command->purchase) && empty($command->purchase->user_id)) {
            // La comanda no tiene responsable, asignar como responsable al usuario que modificó los pagos
            $purchase = $command->purchase;
            $purchase->user_id = $user->id;
            $purchase->save();
        }
        // Obtener los pagos que se van a guardar en el log
        $paymentsTmp = Payment::with(['currency', 'paymentMethod'])
                ->whereIn('id', $payments)
                ->where('command_id', $command->id)
                ->get();
        // Guardar la información después de ser modificada
        $data['payments']['after'] = $paymentsTmp->toArray();

        // Agrega al request la data
        $request->merge($data);
        // Guardar los logs
        $this->storeLogs($request, $command, 'mark payments as invoiced');

        $description = trans('models.responses.updated', ['model' => $pronoun]);
        $response = ['message' => $description, 'status_code' => 200, 'invoiced_at' => $invoicedAt];
        return $this->responseOk($response);
    }

    /**
     * Display a listing of the logs of one command.
     *
     * @return \Illuminate\Http\Response
     */
    public function logs($id, CommandLogTransformer $commandLogTransformer, Request $request)
    {
        // Checar permiso
        $user = Auth::user();
        $this->checkPermission($user, $this->module, $this->permission);

        $pronoun = trans_choice('models.pronouns.command', 1);
        // Solo pueden acceder a los logs los usuario administradores
        if (!empty($user->authenticatable_type)) {
            throw new AccessDeniedHttpException(trans('models.responses.not_allowed', [
                'model' => $pronoun
            ]));
        }

        $sort = 'created_at';
        $dir = 'desc';
        $paginate = true;
        if ($request->get('no-paginate', '0') == 1) {
            $paginate = false;
        }
        if (!empty($request->get('sort'))) {
            $sort = $request->get('sort');
        }
        if (!empty($request->get('dir'))) {
            $dir = $request->get('dir');
        }
        $result = CommandLog::with('user')
            ->where('command_id', $id)
            ->orderBy($sort, $dir);

        // Paginar registros
        if ($paginate) {
            $result = $result->paginate(10);
            return $this->response->paginator($result, $commandLogTransformer);
        }
        // No paginar, retornar todos los registros encontrados
        else {
            $result = $result->get();
            return $this->response->collection($result, $commandLogTransformer);
        }
    }
}
