<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Place extends BaseModel
{
    use SoftDeletes;
    
    protected $fillable = [
    	'name', 'address', 'latitude', 'longitude'
    ];
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at',];
}
