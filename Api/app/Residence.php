<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Residence extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
    	'housing_id', 'name', 'address',
    ];
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at',];
    
    /**
     * Get related housing
     */
    public function housing()
    {
        return $this->belongsTo('App\Housing');
    }
    
    /**
     * Get related rooms
     */
    public function rooms()
    {
        return $this->hasMany('App\Room');
    }
}
