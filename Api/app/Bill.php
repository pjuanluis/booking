<?php

namespace App;

class Bill extends BaseModel
{
    protected $fillable = ['command_id', 'reference_number', 'amount', 'currency_id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * Get the related command.
     */
    public function command()
    {
        return $this->belongsTo('App\Command');
    }

    /**
     * Get the related command.
     */
    public function currency()
    {
        return $this->belongsTo('App\Currency');
    }
}
