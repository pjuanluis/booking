<?php

namespace App;

require_once(__DIR__ . "/../vendor/conekta/conekta-php/lib/Conekta.php");

use Conekta\Conekta;
use Conekta\Customer;
use Conekta\Order;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use SoftDeletes;

    protected $fillable = [
    	'command_id', 'concept', 'amount', 'paid_at', 'vendor', 'reference_id', 'type', 'currency_id', 'method_payment_id', 'invoiced_at', 'exchange_rates',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at',];

    /**
     * The attributes that should be cast to native types.
     *
     * @var Object
     */
    protected $casts = [
        'exchange_rates' => 'Object',
    ];

    /**
     * Process Conekta payment
     */
    public static function processConektaPayment(array $data)
    {
        $config = \Config::get("services");
        Conekta::setApiKey($config['conekta']['private_key']);
        Conekta::setApiVersion("2.0.0");
        Conekta::setLocale('en');
        $customer = null;
        $response = ['success' => true];
        try {
            $customer = Customer::create([
                "name" => $data['name'],
                "email" => $data['email'],
                "phone" => $data['phone'],
                "payment_sources" => [
                    [
                        "type" => "card",
                        //"token_id" => "tok_test_visa_4242",
                        "token_id" => $data['conekta_token_card_id']
                    ]
                ]
            ]);
        } catch (Handler $error) {
            $conektaError = $error->getConektaMessage();
            $response['success'] = false;
            $response['errors'] = [];
            foreach ($conektaError->details as $key) {
                $response['errors'][$key->param] = $key->message;
            }
            return $response;
        }
        // Crear orden
        $order = null;
        try {
            $order = Order::create([
                "line_items" => [
                    [
                        "name" => "Registration Fee",
                        "unit_price" => 6000,
                        "quantity" => 1
                    ]
                ],
                "currency" => "USD",
                "customer_info" => [
                    "customer_id" => $customer->id,
                ],
                "metadata" => ["reference" => $data['booking_code'], "more_info" => ""],
                "charges" => [
                    [
                        "payment_method" => [
                            "type" => "card",
                            //"token_id" => "tok_test_visa_4242",
                            "token_id" => $data['conekta_token_card_id']
                        ]
                    ]
                ]
            ]);
        } catch (ProccessingError $error) {
            $conektaError = $error->getConektaMessage();
            $response['success'] = false;
            $response['status_code'] = 402;
            $response['errors'] = [];
            foreach ($conektaError->details as $key) {
                $response['errors']['processing'] = $key->message;
            }
            return $response;
        } catch (ParameterValidationError $error) {
            $conektaError = $error->getConektaMessage();
            $response['success'] = false;
            $response['status_code'] = 422;
            $response['errors'] = [];
            foreach ($conektaError->details as $key) {
                $response['errors'][$key->param] = $key->message;
            }
            return $response;
        }
        $response['data'] = json_decode(json_encode($order), true);
        return $response;
    }

    /**
     * Get related command
     */
    public function command()
    {
        return $this->belongsTo('App\Command');
    }

    /**
     * Get related currency
     */
    public function currency()
    {
        return $this->belongsTo('App\Currency');
    }

    /**
     * Get related payment method
     */
    public function paymentMethod()
    {
        return $this->belongsTo('App\MethodPayment', 'method_payment_id');
    }
}
