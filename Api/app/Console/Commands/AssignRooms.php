<?php

namespace App\Console\Commands;

use DB;
use App\CommandHousing;
use Illuminate\Console\Command;
use App\Http\Controllers\Controller;

class AssignRooms extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command-housing:assign-rooms';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Asigna un cuarto a las comandas que no tengan uno';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Asignar un cuarto a las comandas que no tengan uno
        $controller = new Controller();
        $controller->assignRooms();
    }
}
