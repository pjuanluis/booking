<?php

namespace App\Console\Commands;

use Illuminate\Console\Command as ConsoleCommand;
use Carbon\Carbon;
use App\Package;
use App\Housing;
use App\Client;
use App\Command;
use Excel;
use DB;

class ImportBookings extends ConsoleCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:bookings {file?} {--id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import bookings';
    
    /**
     * The attributes that should be mutated to dates
     * 
     * @var array 
     */
    protected $dates = [
        'fecha_nac', 'fecha_registro', 'fecha_entrada', 'fecha_salida', 'inicio_curso',
        'fin_curso',
    ];
    
    /**
     * The courses attributes
     * 
     * @var array 
     */
    protected $coursesFields = [
        'experiencia', 'curso_paquete', 'inicio_curso', 'fin_curso', 'cantidad'
    ];
    
    /**
     * The excel booking fields
     * 
     * @var array 
     */
    protected $allFields = [
        'nombre', 'apellidos', 'email', 'sexo', 'fecha_nac', 'nivel', 'pais', 'agencia',
        'fecha_registro', 'hospedaje', 'tipo_cuarto', 'paquete', 'fecha_entrada',
        'fecha_salida', 'semanas', 'experiencia', 'curso_paquete', 'inicio_curso', 
        'fin_curso', 'cantidad'
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file = $this->argument('file');
        $importId = $this->option('id');
        if (empty($file) && empty($importId)) {
            return;
        }
        $coursesFields = $this->coursesFields;
        $datesFields = $this->dates;
        $allFields = $this->allFields;
        $packages = Package::with(['rates', 'experiences'])->get();
        $housing = Housing::with(['housingRooms.packages'])->get();
        $invalid = false;
        $getCoursesCount = function ($row, $coursesFields) {
            $count = 0;
            $tmp = null;
            // Buscar campos de cursos en la fila dada
            foreach ($row as $field => $value) {
                // Recorrer los posibles campos de cursos
                foreach ($coursesFields as $courseField) {
                    // Si existe un campo de curso en la fila
                    if (strpos($field, $courseField) === 0 && !empty($value)) {
                        // Reemplazar nombre del campo de curso para obtener el numero de curso
                        $tmp = str_replace($courseField, '', $field);
                        // Si el tamaño del string del numero de curso es mayor a cero
                        if (strlen($tmp) > 0) {
                            // Obtener el numero eliminando el guion bajo
                            $tmp = str_replace('_', '', $tmp);
                            $tmp = (int) $tmp;
                            $tmp++;
                        }
                        else {
                            $tmp = 1;
                        }
                        if ($tmp > $count) {
                            $count = $tmp;
                        }
                    }
                }
            }
            return $count;
        };
        $getRelatedCourseField = function ($field, $coursesFields) {
            // Recorrer los posibles campos de cursos
            foreach ($coursesFields as $courseField) {
                // Si es un campo de curso en la fila
                if (strpos($field, $courseField) === 0) {
                    return $courseField;
                }
            }
            return null;
        };
        $findCourseByName = function($courseName) use($packages) {
            foreach ($packages as $p) {
                if (strtolower($p->name) === trim(strtolower($courseName))) {
                    return $p;
                }
            }
            return null;
        };
        $findHousingRoomPackageByName = function($housingName, $housingRoomName, $packName) use($housing) {
            $housingRooms = null;
            $packages = null;
            foreach ($housing as $h) {
                if (strtolower($h->name) === trim(strtolower($housingName))) {
                    $housingRooms = $h->housingRooms;
                    foreach ($housingRooms as $hr) {
                        if (strtolower($hr->name) === trim(strtolower($housingRoomName))) {
                            $packages = $hr->packages;
                            foreach ($packages as $p) {
                                if (strtolower($p->name) === trim(strtolower($packName))) {
                                    return $p;
                                }
                            }
                        }
                    }
                }
            }
            return null;
        };
        $now = Carbon::now();
        $data = null;
        if (empty($importId)) {
            $importId = DB::table('imports')->insertGetId([
                'filename' => basename($file),
                'storage_path' => $file,
                'status' => 'loading_excel',
                'details' => "Fetching data from excel file",
                'percent_completed' => 0,
                'created_at' => $now,
                'updated_at' => $now,
            ]);
        }
        else {
            $data = [
                'status' => 'loading_excel',
                'details' => "Fetching data from excel file",
                'percent_completed' => 0,
                'created_at' => $now,
                'updated_at' => $now,
            ];
            if (!empty($file)) {
                $data['filename'] = basename($file);
                $data['storage_path'] = $file;
            }
            DB::table('imports')->where('id', $importId)->update($data);
        }
        if (empty($file)) {
            $file = DB::table('imports')->where('id', $importId)->first();
            $file = $file->storage_path;
        }
        Excel::filter('chunk')->load($file)->chunk(1, function($results) use($importId, $coursesFields, $datesFields, $allFields, 
                &$invalid, $getCoursesCount, $getRelatedCourseField, $findCourseByName, $findHousingRoomPackageByName) {
            if (count($results) === 0) {
                return;
            }
            if ($invalid) {
                return;
            }
            $data = [];
            $tmp = [];
            $tmpField = null;
            $package = null;
            $rate = null;
            $coursesCount = 0;
            $i = 0;
            $isCourseField = false;
            $attributes = [];
            $index = 2;
            foreach($results as $row) {
                $package = null;
                $rate = null;
                $row = $row->toArray();
                // Validar que existan todos los campos
                $attributes = array_keys($row);
                foreach ($allFields as $field) {
                    if (!in_array($field, $attributes)) {
                        DB::table('imports')->where('id', $importId)->update([
                            'status' => 'invalid_excel',
                            'details' => "The field " . ucwords(str_replace('_', ' ', $field)) . " doesn't exists.",
                            'finished_at' => Carbon::now(),
                        ]);
                        $invalid = true;
                        break;
                    }
                }
                if ($invalid) {
                    break;
                }
                $tmp = [
                    'courses' => []
                ];
                $coursesCount = call_user_func($getCoursesCount, $row, $coursesFields);
                for ($i = 0; $i < $coursesCount; $i++) {
                    $tmp['courses'][] = [];
                }
                foreach ($row as $field => $value) {
                    $isCourseField = false;
                    $tmpField = call_user_func($getRelatedCourseField,$field, $coursesFields);
                    $isCourseField = $tmpField === null ? false : true;
                    $tmpField = $tmpField === null ? $field : $tmpField;
                    if (in_array($tmpField, $datesFields) && !empty($value)) {
                        $value = ($value - 25569) * 86400;
                        $value = gmdate("Y-m-d", $value);
                    }
                    // Si es un campo de cursos
                    if ($isCourseField) {
                        if (!empty($value)) {
                            $i = str_replace($tmpField, '', $field);
                            $i = (strlen($i) > 0) ? ((int) str_replace('_', '', $i)) : 0;
                            $tmp['courses'][$i][$tmpField] = $value;
                        }
                    }
                    else {
                        $tmp[$tmpField] = $value;
                    }
                }
                if ($invalid) {
                    break;
                }
                // Validar país
                if (!empty($tmp['pais'])) {
                    $tmpField = DB::table('countries')->where('code', $tmp['pais'])->first();
                    if (empty($tmpField)) {
                        $invalid = true;
                        $value = $tmp['pais'];
                        DB::table('imports')->where('id', $importId)->update([
                            'status' => 'invalid_excel',
                            'details' => "Error in line $index: Data '$value' of field 'Pais' is invalid.",
                            'finished_at' => Carbon::now(),
                        ]);
                        break;
                    }
                    else {
                        $tmp['country_id'] = $tmpField->id;
                    }
                }
                else {
                    $invalid = true;
                    DB::table('imports')->where('id', $importId)->update([
                        'status' => 'invalid_excel',
                        'details' => "Error in line $index: Field 'Pais' is required.",
                        'finished_at' => Carbon::now(),
                    ]);
                    break;
                }
                // Validar agencia
                $tmp['agency_id'] = null;
                if (!empty($tmp['agencia'])) {
                    $tmpField = DB::table('agencies')->where('name', $tmp['agencia'])->first();
                    if (empty($tmpField)) {
                        $invalid = true;
                        $value = $tmp['agencia'];
                        DB::table('imports')->where('id', $importId)->update([
                            'status' => 'invalid_excel',
                            'details' => "Error in line $index: Data '$value' of field 'Agencia' is invalid.",
                            'finished_at' => Carbon::now(),
                        ]);
                        break;
                    }
                    else {
                        $tmp['agency_id'] = $tmpField->id;
                    }
                }
                // Validar datos de hospedaje
                $tmp['housing_room_package_id'] = null;
                if (!empty($tmp['paquete'])) {
                    $package = call_user_func($findHousingRoomPackageByName, $tmp['hospedaje'], $tmp['tipo_cuarto'], $tmp['paquete']);
                    if ($package === null) {
                        $invalid = true;
                        $value = $tmp['paquete'];
                        DB::table('imports')->where('id', $importId)->update([
                            'status' => 'invalid_excel',
                            'details' => "Error in line $index: Data '$value' of field 'Paquete' is invalid.",
                            'finished_at' => Carbon::now(),
                        ]);
                        break;
                    }
                    else {
                        // Calcular costo del hospedaje
                        $tmp['housing_room_package_id'] = $package->pivot->id;
                        $tmp['housing_cost'] = 0;
                        $rate = $package->findHousingRate($tmp['fecha_entrada'], 'week');
                        if ($rate !== null) {
                            $tmp['housing_cost'] = $rate->unit_cost * $tmp['semanas'];
                        }
                    }
                }
                // Validar que los campos de cursos esten completos
                if (count($tmp['courses']) > 0) {
                    foreach ($tmp['courses'] as $i => $package) {
                        foreach ($coursesFields as $courseField) {
                            $attributes = array_keys($package);
                            if (!in_array($courseField, $attributes)) {
                                DB::table('imports')->where('id', $importId)->update([
                                    'status' => 'invalid_excel',
                                    'details' => "The field " . ucwords(str_replace('_', ' ', $courseField)) . " doesn't exists in line $index.",
                                    'finished_at' => Carbon::now(),
                                ]);
                                $invalid = true;
                                break;
                            }
                        }
                        // Validar curso / paquete
                        $value = $package['curso_paquete'];
                        $package = call_user_func($findCourseByName, $value);
                        if ($package === null) {
                            $invalid = true;
                            DB::table('imports')->where('id', $importId)->update([
                                'status' => 'invalid_excel',
                                'details' => "Error in line $index: Data '$value' of field 'Curso / Paquete' is invalid.",
                                'finished_at' => Carbon::now(),
                            ]);
                            break;
                        }
                        else {
                            // Calcular costo del curso
                            $tmp['courses'][$i]['package_id'] = $package->id;
                            $tmp['courses'][$i]['cost'] = 0;
                            $rate = $package->findRate($tmp['courses'][$i]['cantidad']);
                            if ($rate !== null) {
                                $tmp['courses'][$i]['cost'] = $rate->unit_cost * $tmp['courses'][$i]['cantidad'];
                            }
                        }
                        if ($invalid) {
                            break;
                        }
                    }
                }
                // Si no hay cursos y tampoco hospedaje
                else if (empty($tmp['housing_room_package_id'])) {
                    $invalid = true;
                    DB::table('imports')->where('id', $importId)->update([
                        'status' => 'invalid_excel',
                        'details' => "Error in line $index: Courses data are required if housing data doesn't exists.",
                        'finished_at' => Carbon::now(),
                    ]);
                    break;
                }
                if ($invalid) {
                    break;
                }
                $data[] = $tmp;
                DB::table('booking_imports')->insert(['data' => json_encode($tmp)]);
                $index++;
            }
            DB::table('imports')->where('id', $importId)->increment('percent_completed', 1, ['updated_at' => Carbon::now()]);
            // Insertar en la bd
        }, false);
        if ($invalid) {
            echo "Invalid";
            DB::table('booking_imports')->delete();
            return;
        }
        $this->importDataFromDB($importId);
    }
    
    private function importDataFromDB($importId)
    {
        DB::table('imports')->where('id', $importId)->update([
            'status' => 'importing_data',
            'details' => "Importing data.",
            'percent_completed' => 50,
            'updated_at' => Carbon::now(),
        ]);
        
        $count = DB::table('booking_imports')->count();
        $take = 100;
        $completed = 50;
        $increment = (100 / $completed) / $count;
        $incAcum = 0;
        $bookings = null;
        $page = 1;
        do {
            $bookings = DB::table('booking_imports')
                    ->paginate($take, ['*'], 'page', $page);
            foreach ($bookings as $r) {
                $this->storeBooking(json_decode($r->data));
                $incAcum += $increment;
                $completed += $increment;
                if ($incAcum >= 5) {
                    DB::table('imports')->where('id', $importId)->update([
                        'percent_completed' => $completed,
                        'updated_at' => Carbon::now(),
                    ]);
                    $incAcum = 0;
                }
            }
            $page++;
        } while ($bookings->hasMorePages());
        
        DB::table('imports')->where('id', $importId)->update([
            'status' => 'imported',
            'details' => "Excel file succesfully imported.",
            'percent_completed' => 100,
            'finished_at' => Carbon::now(),
        ]);
        DB::table('booking_imports')->delete();
        echo "Imported!";
    }
    
    private function storeBooking($data)
    {
        // Generar booking code
        $code = uniqid("exp_");
        $client = Client::create([
            'first_name' => $data->nombre,
            'last_name' => $data->apellidos,
            'email' => $data->email,
            'country_id' => $data->country_id,
        ]);
        $bookingData = [
            'client_first_name' => $data->nombre,
            'client_last_name' => $data->apellidos,
            'client_email' => $data->email,
            'client_id' => $client->id,
            'agency_id' => $data->agency_id,
            'booking_code' => $code,
            'token' => base64_encode(uniqid()),
            'status' => 'booked',
        ];
        // Guardar commanda
        $command = Command::create($bookingData);
        // Generar y actualizar codigo
        $creationDate = new Carbon($data->fecha_registro);
        $code = "WWW-" . $creationDate->format('y') . $creationDate->format('m') . '-' . str_pad($command->id, 5, '0', STR_PAD_LEFT);
        $command->booking_code = $code;
        $command->save();
        $beginDate = null;
        $endDate = null;
        $tmpDate = null;
        // Guardar cursos seleccionados
        $courses = $data->courses;
        if (!empty($courses)) {
            foreach ($courses as $r) {
                // Calcular fecha de inicio y termino de la comanda
                $tmpDate = new \Carbon\Carbon($r->inicio_curso);
                if ($beginDate === null) {
                    $beginDate = $tmpDate;
                }
                else if ($tmpDate->lessThan($beginDate)) {
                    $beginDate = $tmpDate;
                }
                // Validar que la fecha de termino no sea un fin de semana
                $tmpDate = new \Carbon\Carbon($r->fin_curso);
                if ($tmpDate->isWeekend()) {
                    // Si es fin de semana cambiar la fecha de termino a viernes
                    $tmpDate->subDays($tmpDate->isSunday() ? 2 : 1);
                    $r->fin_curso = $tmpDate->format('Y-m-d');
                }
                if ($endDate === null) {
                    $endDate = $tmpDate;
                }
                else if ($tmpDate->greaterThan($endDate)) {
                    $endDate = $tmpDate;
                }
                //Agregar curso a la reserva
                $command->commandCourses()->create([
                    'package_id' => $r->package_id,
                    'quantity' => $r->cantidad,
                    'cost' => $r->cost,
                    'from_date' => $r->inicio_curso,
                    'to_date' => $r->fin_curso,
                ]);
            }
        }
        // Guardar hospedaje seleccionado
        if (!empty($data->housing_room_package_id)) {
            // Agregar hospedaje a la reserva
            $command->commandHousing()->create([
                'housing_room_package_id' => $data->housing_room_package_id,
                'quantity' => $data->semanas,
                'unit' => 'week',
                'cost' => $data->housing_cost,
                'people' => 1,
                'from_date' => $data->fecha_entrada,
                'to_date' => $data->fecha_salida,
            ]);
            // Calcular fecha de inicio y termino de la comanda
            $tmpDate = new \Carbon\Carbon($data->fecha_entrada);
            if ($beginDate === null) {
                $beginDate = $tmpDate;
            }
            else if ($tmpDate->lessThan($beginDate)) {
                $beginDate = $tmpDate;
            }
            $tmpDate = new \Carbon\Carbon($data->fecha_salida);
            if ($endDate === null) {
                $endDate = $tmpDate;
            }
            else if ($tmpDate->greaterThan($endDate)) {
                $endDate = $tmpDate;
            }
        }
        $command->update([
            'from_date' => $beginDate->format('Y-m-d'),
            'to_date' => $endDate->format('Y-m-d'),
        ]);
    }
}
