<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PettyCash extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['description', 'amount', 'is_deposit', 'consumption_center_id', 'user_id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * The related file
     */
    public function file()
    {
        return $this->morphOne('App\File', 'fileable');
    }

    public function consumptionCenter() {
        return $this->belongsTo('App\ConsumptionCenter');
    }

    /**
     * Get the related user
     */
    public function user() {
        return $this->belongsTo('App\User');
    }
}
