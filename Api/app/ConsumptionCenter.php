<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

class ConsumptionCenter extends BaseModel
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug', 'is_active'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    public function purchases() {
        return $this->hasMeny('App\Purchase');
    }

    public function pettyCashes()
    {
        return $this->hasMany('App\PettyCash');
    }
}
