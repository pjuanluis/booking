<?php

use Illuminate\Database\Seeder;
use App\Housing;
use App\HousingRoom;
use App\Room;


class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $housing = Housing::with(['housingRooms', 'residences'])->get();
        $faker = \Faker\Factory::create();
        for ($i = 0; $i < 10; $i++) {
            $h = $housing->random();
            $hr = $h->housingRooms->random();
            $r = $h->residences->random();
            $room = Room::create([
                'residence_id' => $r->id,
                'name' => $faker->word,
                'beds' => $hr->beds
            ]);
            $room->housingRooms()->attach($hr->id);
        }
    }
}
