<?php

use Illuminate\Database\Seeder;
use App\Residence;
use App\Housing;

class ResidencesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $school = Housing::where('slug', 'school_residence')->first();
        $surfCamp = Housing::where('slug', 'surf_camp')->first();
        $mexicanFamily = Housing::where('slug', 'mexican_family')->first();
        Residence::create([
            'name' => "Revolución",
            'address' => "Zona adoquinada",
            'housing_id' => $school->id
        ]);
        Residence::create([
            'name' => "Surf Camp",
            'address' => "Zicatela",
            'housing_id' => $surfCamp->id
        ]);
        Residence::create([
            'name' => "Familia Morales",
            'address' => "Desconocido",
            'housing_id' => $mexicanFamily->id
        ]);
        Residence::create([
            'name' => "Familia Estela",
            'address' => "Desconocido",
            'housing_id' => $mexicanFamily->id
        ]);
        Residence::create([
            'name' => "Familia Ángela",
            'address' => "Desconocido",
            'housing_id' => $mexicanFamily->id
        ]);
        Residence::create([
            'name' => "Familia Citlali",
            'address' => "Desconocido",
            'housing_id' => $mexicanFamily->id
        ]);
    }
}
