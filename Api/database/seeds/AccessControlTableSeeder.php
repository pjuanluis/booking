<?php

use App\Role;
use App\SystemModule;
use App\SystemPermission;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AccessControlTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Dar acceso al rol administrador (tiene acceso a todos los módulos)
        $roleId = Role::where('slug', 'administrator')->value('id');
        $modules = SystemModule::all();
        $permissions = SystemPermission::all();
        $this->createAccess($modules, $permissions, $roleId);

        // Dar acceso al rol agencia
        // Módulo Agencies
        $roleId = Role::where('slug', 'agency')->value('id');
        $modules = SystemModule::where('slug', 'agencies')->get();
        // Dar solo permisos para editar y leer
        $permissions = SystemPermission::whereIn('slug', ['read', 'edit'])->get();
        $this->createAccess($modules, $permissions, $roleId);
        // Módulo centros de consumo
        $modules = SystemModule::where('slug', 'consumption_centers')->get();
        // Dar solo permisos para leer
        $permissions = SystemPermission::where('slug', 'read')->get();
        $this->createAccess($modules, $permissions, $roleId);
        // Módulo Bookings
        $modules = SystemModule::where('slug', 'bookings')->get();
        // Dar todos los permisos
        $permissions = SystemPermission::all();
        $this->createAccess($modules, $permissions, $roleId);
        // Módulo clientes
        $modules = SystemModule::where('slug', 'clients')->get();
        // Dar solo permisos para editar, leer y borrar
        $permissions = SystemPermission::whereIn('slug', ['read', 'edit', 'delete'])->get();
        $this->createAccess($modules, $permissions, $roleId);
        // Módulo Experiences
        $permissions = SystemPermission::whereIn('slug', ['read'])->get();
        $modules = SystemModule::where('slug', 'experiences')->get();
        $this->createAccess($modules, $permissions, $roleId);
        // Módulo Housing
        $modules = SystemModule::where('slug', 'housing')->get();
        $this->createAccess($modules, $permissions, $roleId);
        // Módulo countries
        $modules = SystemModule::where('slug', 'countries')->get();
        $this->createAccess($modules, $permissions, $roleId);
        // Módulo rooms
        $modules = SystemModule::where('slug', 'rooms')->get();
        $this->createAccess($modules, $permissions, $roleId);
        // Módulo residences
        $modules = SystemModule::where('slug', 'residences')->get();
        $this->createAccess($modules, $permissions, $roleId);
        // Módulo currencies
        $modules = SystemModule::where('slug', 'currencies')->get();
        $this->createAccess($modules, $permissions, $roleId);
        // Módulo Schedules
        $modules = SystemModule::where('slug', 'schedules')->get();
        // Agregar solo permiso de leer y editar
        $permissions = SystemPermission::whereIn('slug', ['read', 'edit'])->get();
        $this->createAccess($modules, $permissions, $roleId);

        // Dar acceso al rol cliente
        // Módulo clients
        $roleId = Role::where('slug', 'client')->value('id');
        $modules = SystemModule::where('slug', 'clients')->get();
        // Dar solo permisos para editar y leer
        $permissions = SystemPermission::whereIn('slug', ['read', 'edit'])->get();
        $this->createAccess($modules, $permissions, $roleId);
        // Módulo Bookings
        $modules = SystemModule::where('slug', 'bookings')->get();
        // Dar solo permiso de leer
        $permissions = SystemPermission::whereIn('slug', ['read'])->get();
        $this->createAccess($modules, $permissions, $roleId);
        // Módulo Purchases
        $modules = SystemModule::where('slug', 'purchases')->get();
        $this->createAccess($modules, $permissions, $roleId);
        // Módulo Experiences
        $modules = SystemModule::where('slug', 'experiences')->get();
        $this->createAccess($modules, $permissions, $roleId);
        // Módulo Housing
        $modules = SystemModule::where('slug', 'housing')->get();
        $this->createAccess($modules, $permissions, $roleId);
        // Módulo countries
        $modules = SystemModule::where('slug', 'countries')->get();
        $this->createAccess($modules, $permissions, $roleId);
        // Módulo Agencies
        $modules = SystemModule::where('slug', 'agencies')->get();
        $this->createAccess($modules, $permissions, $roleId);
        // Módulo Consumption center
        $modules = SystemModule::where('slug', 'consumption_centers')->get();
        $this->createAccess($modules, $permissions, $roleId);
        // Módulo rooms
        $modules = SystemModule::where('slug', 'rooms')->get();
        $this->createAccess($modules, $permissions, $roleId);
        // Módulo residences
        $modules = SystemModule::where('slug', 'residences')->get();
        $this->createAccess($modules, $permissions, $roleId);
        // Módulo currencies
        $modules = SystemModule::where('slug', 'currencies')->get();
        $this->createAccess($modules, $permissions, $roleId);
        // Módulo Schedules
        $modules = SystemModule::where('slug', 'schedules')->get();
        // Agregar solo permiso de leer y editar
        $permissions = SystemPermission::whereIn('slug', ['read', 'edit'])->get();
        $this->createAccess($modules, $permissions, $roleId);
    }

    public function createAccess($modules, $permissions, $roleId) {
        foreach ($modules as $module) {
            foreach ($permissions as $permission) {
                $exists = DB::table('system_access_control')->where('role_id', $roleId)
                            ->where('system_module_id', $module->id)
                            ->where('system_permission_id', $permission->id)
                            ->exists();
                if (!$exists) {
                    DB::table('system_access_control')
                        ->insert([
                            'role_id' => $roleId,
                            'system_module_id' => $module->id,
                            'system_permission_id' => $permission->id
                        ]);
                }
            }
        }
    }
}
