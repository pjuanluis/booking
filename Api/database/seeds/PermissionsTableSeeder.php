<?php

use App\SystemPermission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            [
                'name' => 'Read',
                'slug' => 'read'
            ],
            [
                'name' => 'Add',
                'slug' => 'add'
            ],
            [
                'name' => 'Edit',
                'slug' => 'edit'
            ],
            [
                'name' => 'Delete',
                'slug' => 'delete'
            ],
        ];

        foreach ($permissions as $permission) {
            if (!SystemPermission::where('slug', $permission['slug'])->exists()) {
                SystemPermission::create($permission);
            }
        }
    }
}
