<?php

use Illuminate\Database\Seeder;

class PeopleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $data = null;
        $types = ['spanish_teacher', 'surf_instructor', 'volunteer_responsible'];
        for ($i = 0; $i < 10; $i++) {
            $data = [
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'email' => $faker->safeEmail,
                'position' => null,
                'type' => $faker->randomElement($types)
            ];
            $data['abbreviation'] = $data['first_name'][0] . $data['last_name'][0];
            \App\Person::create($data);
        }
    }
}
