<?php

use Illuminate\Database\Seeder;
use App\Course;
use App\CourseRate;

class CoursePackagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $courses = Course::all();
        foreach ($courses as $course) {
            if ($course->name === 'Spanish') {
                $pack = $pack = $course->packages()->create([
                    'name' => 'Standard 15',
                    'description' => "15 lessons per week:\nThree lessons daily in communicative grammar.",
                    'has_fee_per_duration' => 1,
                ]);
                // Insertar tarifas
                $rates = [
                    ['from' => 1, 'to' => null, 'unit' => 'day', 'unit_cost' => 35],
                    ['from' => 1, 'to' => 3, 'unit' => 'week', 'unit_cost' => 173],
                    ['from' => 4, 'to' => 7, 'unit' => 'week', 'unit_cost' => 156],
                    ['from' => 8, 'to' => null, 'unit' => 'week', 'unit_cost' => 140],
                ];
                foreach ($rates as $rate) {
                    CourseRate::create([
                        'package_id' => $pack->id,
                        'quantity' => $rate['from'],
                        'to_quantity' => $rate['to'],
                        'unit' => $rate['unit'],
                        'unit_cost' => $rate['unit_cost'],
                    ]);
                }
                $pack = $course->packages()->create([
                    'name' => 'Standard 20',
                    'description' => "20 lessons per week:\nThree lessons daily in communicative grammar.\n".
                            "One lessons daily in practical conversation.",
                    'has_fee_per_duration' => 1,
                ]);
                // Insertar tarifas
                $rates = [
                    ['from' => 1, 'to' => null, 'unit' => 'day', 'unit_cost' => 42],
                    ['from' => 1, 'to' => 3, 'unit' => 'week', 'unit_cost' => 213],
                    ['from' => 4, 'to' => 7, 'unit' => 'week', 'unit_cost' => 192],
                    ['from' => 8, 'to' => null, 'unit' => 'week', 'unit_cost' => 173],
                ];
                foreach ($rates as $rate) {
                    CourseRate::create([
                        'package_id' => $pack->id,
                        'quantity' => $rate['from'],
                        'to_quantity' => $rate['to'],
                        'unit' => $rate['unit'],
                        'unit_cost' => $rate['unit_cost'],
                    ]);
                }
                $pack = $course->packages()->create([
                    'name' => 'Standard 25',
                    'description' => "25 lessons per week:\nThree lessons daily in communicative grammar.\n".
                            "Two lessons daily in practical conversation.",
                    'has_fee_per_duration' => 1,
                ]);
                // Insertar tarifas
                $rates = [
                    ['from' => 1, 'to' => null, 'unit' => 'day', 'unit_cost' => 48],
                    ['from' => 1, 'to' => 3, 'unit' => 'week', 'unit_cost' => 244],
                    ['from' => 4, 'to' => 7, 'unit' => 'week', 'unit_cost' => 220],
                    ['from' => 8, 'to' => null, 'unit' => 'week', 'unit_cost' => 198],
                ];
                foreach ($rates as $rate) {
                    CourseRate::create([
                        'package_id' => $pack->id,
                        'quantity' => $rate['from'],
                        'to_quantity' => $rate['to'],
                        'unit' => $rate['unit'],
                        'unit_cost' => $rate['unit_cost'],
                    ]);
                }
                $pack = $course->packages()->create([
                    'name' => 'Travellers',
                    'description' => "Ten group classes and 5 private lessons per week:\n".
                            "Two group classes daily in practical conversation.\nOne private lesson daily in grammar.",
                    'has_fee_per_duration' => 1,
                ]);
                // Insertar tarifas
                $rates = [
                    ['from' => 1, 'to' => null, 'unit' => 'day', 'unit_cost' => 47],
                    ['from' => 1, 'to' => 3, 'unit' => 'week', 'unit_cost' => 235],
                    ['from' => 4, 'to' => 7, 'unit' => 'week', 'unit_cost' => 195],
                    ['from' => 8, 'to' => null, 'unit' => 'week', 'unit_cost' => 176],
                ];
                foreach ($rates as $rate) {
                    CourseRate::create([
                        'package_id' => $pack->id,
                        'quantity' => $rate['from'],
                        'to_quantity' => $rate['to'],
                        'unit' => $rate['unit'],
                        'unit_cost' => $rate['unit_cost'],
                    ]);
                }
                $pack = $course->packages()->create([
                    'name' => 'Private',
                    'description' => "15 private classes per week\n*Recommended for people who want flexible ".
                            "schedules, individual attention or professionals that want to have a very intensive ".
                            "Spanish program in a short period of time.",
                    'has_fee_per_duration' => 1,
                ]);
                // Insertar tarifas
                $rates = [
                    ['from' => 1, 'to' => null, 'unit' => 'day', 'unit_cost' => 48],
                    ['from' => 1, 'to' => null, 'unit' => 'week', 'unit_cost' => 240],
                ];
                foreach ($rates as $rate) {
                    CourseRate::create([
                        'package_id' => $pack->id,
                        'quantity' => $rate['from'],
                        'to_quantity' => $rate['to'],
                        'unit' => $rate['unit'],
                        'unit_cost' => $rate['unit_cost'],
                    ]);
                }
                $pack = $course->packages()->create([
                    'name' => 'Executive',
                    'description' => "25 group lessons and 15 private lessons per week:\nThree lessons daily in grammar.\n".
                            "Two lessons daily in practical conversation.\nThree private lessons daily, focuses on ".
                            "the individual needs.\n*Recommended for professionals that want to have a very intensive ".
                            "Spanish program in a short period of time.",
                    'has_fee_per_duration' => 1,
                ]);
                // Insertar tarifas
                $rates = [
                    ['from' => 1, 'to' => null, 'unit' => 'day', 'unit_cost' => 105],
                    ['from' => 1, 'to' => 3, 'unit' => 'week', 'unit_cost' => 529],
                    ['from' => 4, 'to' => 7, 'unit' => 'week', 'unit_cost' => 482],
                    ['from' => 8, 'to' => null, 'unit' => 'week', 'unit_cost' => 434],
                ];
                foreach ($rates as $rate) {
                    CourseRate::create([
                        'package_id' => $pack->id,
                        'quantity' => $rate['from'],
                        'to_quantity' => $rate['to'],
                        'unit' => $rate['unit'],
                        'unit_cost' => $rate['unit_cost'],
                    ]);
                }
                $pack = $course->packages()->create([
                    'name' => 'Seniors',
                    'description' => "Airline, Medical, Latin America Business or Studies – To sign up for ".
                            "professional courses please contact us.",
                    'has_fee_per_duration' => 1,
                ]);
                $rates = [
                    ['from' => 1, 'to' => null, 'unit' => 'day', 'unit_cost' => 58],
                    ['from' => 1, 'to' => null, 'unit' => 'week', 'unit_cost' => 288],
                ];
                foreach ($rates as $rate) {
                    CourseRate::create([
                        'package_id' => $pack->id,
                        'quantity' => $rate['from'],
                        'to_quantity' => $rate['to'],
                        'unit' => $rate['unit'],
                        'unit_cost' => $rate['unit_cost'],
                    ]);
                }
            }
            else if ($course->name === 'Surf') {
                $pack = $course->packages()->create([
                    'name' => 'Private class',
                    'description' => "1 student\n1 instructor\ndaily or weekly",
                    'has_fee_per_duration' => 1,
                ]);
                // Insertar tarifas
                CourseRate::create([
                    'package_id' => $pack->id,
                    'quantity' => 1,
                    'to_quantity' => null,
                    'unit' => 'day',
                    'unit_cost' => 33,
                ]);
                $pack = $course->packages()->create([
                    'name' => 'Semi-Private class',
                    'description' => "2 students\n1 instructor\ndaily or weekly",
                    'has_fee_per_duration' => 1,
                ]);
                // Insertar tarifas
                CourseRate::create([
                    'package_id' => $pack->id,
                    'quantity' => 1,
                    'to_quantity' => null,
                    'unit' => 'day',
                    'unit_cost' => 23,
                ]);
                $pack = $course->packages()->create([
                    'name' => '5 Class Pack',
                    'description' => "1-2 students\n1 instructor\n5 Semi Private Classes\n1 week",
                    'has_fee_per_duration' => 0,
                ]);
                // Insertar tarifas
                CourseRate::create([
                    'package_id' => $pack->id,
                    'quantity' => 1,
                    'to_quantity' => null,
                    'unit' => 'package',
                    'unit_cost' => 109,
                ]);
                $pack = $course->packages()->create([
                    'name' => '7 Class Pack',
                    'description' => "1-2 students\n1 instructor\n7 Semi Private Classes\n1 week",
                    'has_fee_per_duration' => 0,
                ]);
                // Insertar tarifas
                CourseRate::create([
                    'package_id' => $pack->id,
                    'quantity' => 1,
                    'to_quantity' => null,
                    'unit' => 'package',
                    'unit_cost' => 153,
                ]);
                $pack = $course->packages()->create([
                    'name' => '10 Class Pack',
                    'description' => "1-2 students\n1 instructor\n10 Semi Private Classes\n2 weeks",
                    'has_fee_per_duration' => 0,
                ]);
                // Insertar tarifas
                CourseRate::create([
                    'package_id' => $pack->id,
                    'quantity' => 1,
                    'to_quantity' => null,
                    'unit' => 'package',
                    'unit_cost' => 209,
                ]);
                $pack = $course->packages()->create([
                    'name' => 'Surf Insight',
                    'description' => "1-2 students\n1 instructor\n5 surf-classes\n1 week\nPhoto session\n".
                            "3 Yoga Classes for Surfers",
                    'has_fee_per_duration' => 0,
                ]);
                // Insertar tarifas
                CourseRate::create([
                    'package_id' => $pack->id,
                    'quantity' => 1,
                    'to_quantity' => null,
                    'unit' => 'package',
                    'unit_cost' => 149,
                ]);
                $pack = $course->packages()->create([
                    'name' => 'Pro Surfer',
                    'description' => "1-2 students\n1 instructor\n10 surf-classes\n2 weeks\nPhoto session\n".
                            "6 Yoga Classes for Surfers",
                    'has_fee_per_duration' => 0,
                ]);
                // Insertar tarifas
                CourseRate::create([
                    'package_id' => $pack->id,
                    'quantity' => 1,
                    'to_quantity' => null,
                    'unit' => 'package',
                    'unit_cost' => 290,
                ]);
            }
            else if ($course->name === 'Spanish & Surf') {
                $pack = $course->packages()->create([
                    'name' => 'Light Pack',
                    'description' => "Standard Spanish 15 hours per week (3 lessons daily in ".
                            "communicative grammar)\n8 hours of surfing (4 classes 2 hours each)",
                    'has_fee_per_duration' => 1,
                ]);
                // Insertar tarifas
                $rates = [
                    ['from' => 1, 'to' => null, 'unit' => 'day', 'unit_cost' => 49],
                    ['from' => 1, 'to' => 3, 'unit' => 'week', 'unit_cost' => 249],
                    ['from' => 4, 'to' => 7, 'unit' => 'week', 'unit_cost' => 224],
                    ['from' => 8, 'to' => null, 'unit' => 'week', 'unit_cost' => 208],
                ];
                foreach ($rates as $rate) {
                    CourseRate::create([
                        'package_id' => $pack->id,
                        'quantity' => $rate['from'],
                        'to_quantity' => $rate['to'],
                        'unit' => $rate['unit'],
                        'unit_cost' => $rate['unit_cost'],
                    ]);
                }
                $pack = $course->packages()->create([
                    'name' => 'Progressive',
                    'description' => "Standard Spanish 20 hours per week (3 daily lessons in communicative ".
                            "grammar , 1 in practical conversation)\n5 surf-classes (2 hours each)",
                    'has_fee_per_duration' => 1,
                ]);
                // Insertar tarifas
                $rates = [
                    ['from' => 1, 'to' => null, 'unit' => 'day', 'unit_cost' => 63],
                    ['from' => 1, 'to' => 3, 'unit' => 'week', 'unit_cost' => 315],
                    ['from' => 4, 'to' => 7, 'unit' => 'week', 'unit_cost' => 284],
                    ['from' => 8, 'to' => null, 'unit' => 'week', 'unit_cost' => 261],
                ];
                foreach ($rates as $rate) {
                    CourseRate::create([
                        'package_id' => $pack->id,
                        'quantity' => $rate['from'],
                        'to_quantity' => $rate['to'],
                        'unit' => $rate['unit'],
                        'unit_cost' => $rate['unit_cost'],
                    ]);
                }
                $pack = $course->packages()->create([
                    'name' => 'All Inclusive',
                    'description' => "25 hours of Spanish (5 hours daily in communicative grammar and ".
                            "speaking practice)\n5 surf-classes (2 hours each)\nPhoto session\n3 yoga ".
                            "classes for surfers",
                    'has_fee_per_duration' => 1,
                ]);
                // Insertar tarifas
                $rates = [
                    ['from' => 1, 'to' => null, 'unit' => 'day', 'unit_cost' => 75],
                    ['from' => 1, 'to' => 3, 'unit' => 'week', 'unit_cost' => 375],
                    ['from' => 4, 'to' => 7, 'unit' => 'week', 'unit_cost' => 349],
                    ['from' => 8, 'to' => null, 'unit' => 'week', 'unit_cost' => 333],
                ];
                foreach ($rates as $rate) {
                    CourseRate::create([
                        'package_id' => $pack->id,
                        'quantity' => $rate['from'],
                        'to_quantity' => $rate['to'],
                        'unit' => $rate['unit'],
                        'unit_cost' => $rate['unit_cost'],
                    ]);
                }
                $pack = $course->packages()->create([
                    'name' => 'Morning surfer',
                    'description' => "6 morning surf lessons (8.00-10.00 am)\n10 group Spanish lessons ".
                            "(100min.) in practical conversation\n5 private Spanish lesson (50min) ".
                            "focused on your needs",
                    'has_fee_per_duration' => 1,
                ]);
                // Insertar tarifas
                $rates = [
                    ['from' => 1, 'to' => null, 'unit' => 'day', 'unit_cost' => 73],
                    ['from' => 1, 'to' => 3, 'unit' => 'week', 'unit_cost' => 369],
                    ['from' => 4, 'to' => 7, 'unit' => 'week', 'unit_cost' => 341],
                    ['from' => 8, 'to' => null, 'unit' => 'week', 'unit_cost' => 325],
                ];
                foreach ($rates as $rate) {
                    CourseRate::create([
                        'package_id' => $pack->id,
                        'quantity' => $rate['from'],
                        'to_quantity' => $rate['to'],
                        'unit' => $rate['unit'],
                        'unit_cost' => $rate['unit_cost'],
                    ]);
                }
                $pack = $course->packages()->create([
                    'name' => 'Private Pack',
                    'description' => "10 Private spanish classes per week (2 daily classes)\n5 private ".
                            "surf lessons ( 2hrs each)",
                    'has_fee_per_duration' => 1,
                ]);
                // Insertar tarifas
                $rates = [
                    ['from' => 1, 'to' => null, 'unit' => 'day', 'unit_cost' => 59],
                    ['from' => 1, 'to' => 3, 'unit' => 'week', 'unit_cost' => 299],
                    ['from' => 4, 'to' => 7, 'unit' => 'week', 'unit_cost' => 271],
                    ['from' => 8, 'to' => null, 'unit' => 'week', 'unit_cost' => 255],
                ];
                foreach ($rates as $rate) {
                    CourseRate::create([
                        'package_id' => $pack->id,
                        'quantity' => $rate['from'],
                        'to_quantity' => $rate['to'],
                        'unit' => $rate['unit'],
                        'unit_cost' => $rate['unit_cost'],
                    ]);
                }
            }
            else if ($course->name === 'Volunteer') {
                $pack = $course->packages()->create([
                    'name' => 'Orphanage',
                    'description' => "The Orphanage regularly needs volunteers with long term and very good Spanish level.\n".
                            "Casa Hogar Nueva Vida provides care for children who have been abandoned by their parents, ".
                            "have been abused physically and mentally, or whose parents are deceased.",
                    'has_fee_per_duration' => 1,
                ]);
                $pack = $course->packages()->create([
                    'name' => 'Kindergarten',
                    'description' => "The teachers need support in their classrooms. It is a good short-term project.\n".
                            "Coralitos is a privately run kindergarten with the purpose of taking care of children with ".
                            "working families. The kindergarten works closely with SEDESOL which is a government organization ".
                            "providing assistance to families with low incomes.",
                    'has_fee_per_duration' => 1,
                ]);
                $pack = $course->packages()->create([
                    'name' => 'Primary school',
                    'description' => "The school regularly needs help and english teachers and also various classes. ".
                            "For volunteers with experience in working with kids and with a long term stay planning and ".
                            "teaching educational English lessons.",
                    'has_fee_per_duration' => 1,
                ]);
                $pack = $course->packages()->create([
                    'name' => 'Turtles & Iguanas',
                    'description' => "This is for volunteers who like to be in the nature and are not afraid of any reptiles.\n".
                            "Protection and Conservation of reptiles. The Iguanario center is a small private organization ".
                            "located about 10 min. outside from Puerto Escondido.",
                    'has_fee_per_duration' => 1,
                ]);
                $pack = $course->packages()->create([
                    'name' => 'Street Dogs',
                    'description' => "Volunteers should not only love dogs, but also be prepared to care for very ".
                            "large groups of dogs in an open space\nIn Puerto Escondido there are two organizations helping ".
                            "to take care of the street dogs in the area. The two companies share the same objective, ".
                            "to improve the lives and health of dogs through education and control programs",
                    'has_fee_per_duration' => 1,
                ]);
                $pack = $course->packages()->create([
                    'name' => 'Rancho Tierra Luna',
                    'description' => "Volunteer Work on a Mexican Dairy Farm\nThis is for volunteer who loves the outdoors, ".
                            "organic farming, cows, cheese and a friendly family environment.\nTierra Luna is a dairy farm ".
                            "situated about 40 minutes from Puerto Escondido. The 120-hectare farm normally holds 50 animals.",
                    'has_fee_per_duration' => 1,
                ]);
            }
        }
    }
}
