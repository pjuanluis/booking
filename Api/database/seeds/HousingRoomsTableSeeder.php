<?php

use Illuminate\Database\Seeder;
use App\Housing;
use App\RoomType;
use App\HousingRate;
use App\Package;

class HousingRoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $housing = Housing::all();
        $bunkBeds = RoomType::create([
            'name' => 'Bunk Beds',
            'slug' => 'bunk_beds',
            'description' => $faker->text(),
        ]);
        $privateRoomCouple = RoomType::create([
            'name' => 'Private Room Per Couple',
            'slug' => 'private_room_per_couple',
            'description' => $faker->text(),
        ]);
        $privateRoom = RoomType::create([
            'name' => 'Private Room',
            'slug' => 'private_room',
            'description' => $faker->text(),
        ]);
        $sharedRoom = RoomType::create([
            'name' => 'Shared Room (2 Beds)',
            'slug' => 'shared_room',
            'description' => $faker->text(),
        ]);
        $packRoomBreakfast = Package::create([
            'type' => 'housing',
            'name' => 'Room + Continental BreakFast + Shared Kitchen',
            'description' => $faker->text(),
            'unit' => 'pack',
            'quantity' => 1,
            'cost' => 0,
        ]);
        $packHalfBoard = Package::create([
            'type' => 'housing',
            'name' => 'Half Board',
            'description' => $faker->text(),
            'unit' => 'pack',
            'quantity' => 1,
            'cost' => 0,
        ]);
        $packBedBreakfast = Package::create([
            'type' => 'housing',
            'name' => 'Bed & Breakfast',
            'description' => $faker->text(),
            'unit' => 'pack',
            'quantity' => 1,
            'cost' => 0,
        ]);
        foreach ($housing as $h) {
            if ($h->name === 'School Residence') {
                // Insertar cuartos
                $room = $h->housingRooms()->create([
                    'housing_id' => $h->id,
                    'room_type_id' => $privateRoom->id,
                    'name' => 'Private Room',
                    'description' => $faker->text(),
                    'beds' => 2
                ]);
                $id = DB::table('housing_room_packages')->insertGetId([
                    'package_id' => $packRoomBreakfast->id,
                    'housing_room_id' => $room->id,
                ]);
                // Insertar tarifas
                $rates = [
                    ['quantity' => 1, 'unit' => 'week', 'from' => null, 'to' => null, 'unit_cost' => 209],
                    ['quantity' => 1, 'unit' => 'week', 'from' => '2017-12-20', 'to' => '2018-01-10', 'unit_cost' => 239],
                    ['quantity' => 1, 'unit' => 'week', 'from' => '2017-04-05', 'to' => '2017-04-20', 'unit_cost' => 239],
                    ['quantity' => 1, 'unit' => 'day', 'from' => null, 'to' => null, 'unit_cost' => 36],
                    ['quantity' => 1, 'unit' => 'day', 'from' => '2017-12-20', 'to' => '2018-01-10', 'unit_cost' => 39],
                    ['quantity' => 1, 'unit' => 'day', 'from' => '2017-04-05', 'to' => '2017-04-20', 'unit_cost' => 39],
                ];
                foreach ($rates as $rate) {
                    HousingRate::create([
                        'housing_room_package_id' => $id,
                        'quantity' => $rate['quantity'],
                        'unit' => $rate['unit'],
                        'from_date' => $rate['from'],
                        'to_date' => $rate['to'],
                        'unit_cost' => $rate['unit_cost'],
                    ]);
                }
                $room = $h->housingRooms()->create([
                    'housing_id' => $h->id,
                    'room_type_id' => $privateRoomCouple->id,
                    'name' => 'Private Room Per Couple',
                    'description' => $faker->text(),
                    'beds' => 2
                    
                ]);
                $id = DB::table('housing_room_packages')->insertGetId([
                    'package_id' => $packRoomBreakfast->id,
                    'housing_room_id' => $room->id,
                ]);
                // Insertar tarifas
                $rates = [
                    ['quantity' => 1, 'unit' => 'week', 'from' => null, 'to' => null, 'unit_cost' => 272],
                    ['quantity' => 1, 'unit' => 'week', 'from' => '2017-12-20', 'to' => '2018-01-10', 'unit_cost' => 309],
                    ['quantity' => 1, 'unit' => 'week', 'from' => '2017-04-05', 'to' => '2017-04-20', 'unit_cost' => 309],
                    ['quantity' => 1, 'unit' => 'day', 'from' => null, 'to' => null, 'unit_cost' => 50],
                    ['quantity' => 1, 'unit' => 'day', 'from' => '2017-12-20', 'to' => '2018-01-10', 'unit_cost' => 55],
                    ['quantity' => 1, 'unit' => 'day', 'from' => '2017-04-05', 'to' => '2017-04-20', 'unit_cost' => 55],
                ];
                foreach ($rates as $rate) {
                    HousingRate::create([
                        'housing_room_package_id' => $id,
                        'quantity' => $rate['quantity'],'unit' => $rate['unit'],
                        'from_date' => $rate['from'],
                        'to_date' => $rate['to'],
                        'unit_cost' => $rate['unit_cost'],
                    ]);
                }
                $room = $h->housingRooms()->create([
                    'housing_id' => $h->id,
                    'room_type_id' => $sharedRoom->id,
                    'name' => 'Shared Room (2 Beds)',
                    'description' => $faker->text(),
                    'beds' => 2
                ]);
                $id = DB::table('housing_room_packages')->insertGetId([
                    'package_id' => $packRoomBreakfast->id,
                    'housing_room_id' => $room->id,
                ]);
                // Insertar tarifas
                $rates = [
                    ['quantity' => 1, 'unit' => 'week', 'from' => null, 'to' => null, 'unit_cost' => 124],
                    ['quantity' => 1, 'unit' => 'week', 'from' => '2017-12-20', 'to' => '2018-01-10', 'unit_cost' => 139],
                    ['quantity' => 1, 'unit' => 'week', 'from' => '2017-04-05', 'to' => '2017-04-20', 'unit_cost' => 139],
                    ['quantity' => 1, 'unit' => 'day', 'from' => null, 'to' => null, 'unit_cost' => 22],
                    ['quantity' => 1, 'unit' => 'day', 'from' => '2017-12-20', 'to' => '2018-01-10', 'unit_cost' => 25],
                    ['quantity' => 1, 'unit' => 'day', 'from' => '2017-04-05', 'to' => '2017-04-20', 'unit_cost' => 25],
                ];
                foreach ($rates as $rate) {
                    HousingRate::create([
                        'housing_room_package_id' => $id,
                        'quantity' => $rate['quantity'],
                        'unit' => $rate['unit'],
                        'from_date' => $rate['from'],
                        'to_date' => $rate['to'],
                        'unit_cost' => $rate['unit_cost'],
                    ]);
                }
            }
            else if ($h->name === 'Surf Camp') {
                $room = $h->housingRooms()->create([
                    'housing_id' => $h->id,
                    'room_type_id' => $privateRoom->id,
                    'name' => 'Private Room',
                    'description' => $faker->text(),
                    'beds' => 2
                ]);
                $id = DB::table('housing_room_packages')->insertGetId([
                    'package_id' => $packRoomBreakfast->id,
                    'housing_room_id' => $room->id,
                ]);
                // Insertar tarifas
                $rates = [
                    ['quantity' => 1, 'unit' => 'week', 'from' => null, 'to' => null, 'unit_cost' => 209],
                    ['quantity' => 1, 'unit' => 'week', 'from' => '2017-12-20', 'to' => '2018-01-10', 'unit_cost' => 239],
                    ['quantity' => 1, 'unit' => 'week', 'from' => '2017-04-05', 'to' => '2017-04-20', 'unit_cost' => 239],
                    ['quantity' => 1, 'unit' => 'day', 'from' => null, 'to' => null, 'unit_cost' => 36],
                    ['quantity' => 1, 'unit' => 'day', 'from' => '2017-12-20', 'to' => '2018-01-10', 'unit_cost' => 39],
                    ['quantity' => 1, 'unit' => 'day', 'from' => '2017-04-05', 'to' => '2017-04-20', 'unit_cost' => 39],
                ];
                foreach ($rates as $rate) {
                    HousingRate::create([
                        'housing_room_package_id' => $id,
                        'quantity' => $rate['quantity'],
                        'unit' => $rate['unit'],
                        'from_date' => $rate['from'],
                        'to_date' => $rate['to'],
                        'unit_cost' => $rate['unit_cost'],
                    ]);
                }
                $room = $h->housingRooms()->create([
                    'housing_id' => $h->id,
                    'room_type_id' => $privateRoomCouple->id,
                    'name' => 'Private Room Per Couple',
                    'description' => $faker->text(),
                    'beds' => 2
                ]);
                $id = DB::table('housing_room_packages')->insertGetId([
                    'package_id' => $packRoomBreakfast->id,
                    'housing_room_id' => $room->id,
                ]);
                // Insertar tarifas
                $rates = [
                    ['quantity' => 1, 'unit' => 'week', 'from' => null, 'to' => null, 'unit_cost' => 272],
                    ['quantity' => 1, 'unit' => 'week', 'from' => '2017-12-20', 'to' => '2018-01-10', 'unit_cost' => 309],
                    ['quantity' => 1, 'unit' => 'week', 'from' => '2017-04-05', 'to' => '2017-04-20', 'unit_cost' => 309],
                    ['quantity' => 1, 'unit' => 'day', 'from' => null, 'to' => null, 'unit_cost' => 50],
                    ['quantity' => 1, 'unit' => 'day', 'from' => '2017-12-20', 'to' => '2018-01-10', 'unit_cost' => 55],
                    ['quantity' => 1, 'unit' => 'day', 'from' => '2017-04-05', 'to' => '2017-04-20', 'unit_cost' => 55],
                ];
                foreach ($rates as $rate) {
                    HousingRate::create([
                        'housing_room_package_id' => $id,
                        'quantity' => $rate['quantity'],
                        'unit' => $rate['unit'],
                        'from_date' => $rate['from'],
                        'to_date' => $rate['to'],
                        'unit_cost' => $rate['unit_cost'],
                    ]);
                }
                $room = $h->housingRooms()->create([
                    'housing_id' => $h->id,
                    'room_type_id' => $sharedRoom->id,
                    'name' => 'Shared Room (2 Beds)',
                    'description' => $faker->text(),
                    'beds' => 2
                ]);
                $id = DB::table('housing_room_packages')->insertGetId([
                    'package_id' => $packRoomBreakfast->id,
                    'housing_room_id' => $room->id,
                ]);
                // Insertar tarifas
                $rates = [
                    ['quantity' => 1, 'unit' => 'week', 'from' => null, 'to' => null, 'unit_cost' => 124],
                    ['quantity' => 1, 'unit' => 'week', 'from' => '2017-12-20', 'to' => '2018-01-10', 'unit_cost' => 139],
                    ['quantity' => 1, 'unit' => 'week', 'from' => '2017-04-05', 'to' => '2017-04-20', 'unit_cost' => 139],
                    ['quantity' => 1, 'unit' => 'day', 'from' => null, 'to' => null, 'unit_cost' => 22],
                    ['quantity' => 1, 'unit' => 'day', 'from' => '2017-12-20', 'to' => '2018-01-10', 'unit_cost' => 25],
                    ['quantity' => 1, 'unit' => 'day', 'from' => '2017-04-05', 'to' => '2017-04-20', 'unit_cost' => 25],
                ];
                foreach ($rates as $rate) {
                    HousingRate::create([
                        'housing_room_package_id' => $id,
                        'quantity' => $rate['quantity'],
                        'unit' => $rate['unit'],
                        'from_date' => $rate['from'],
                        'to_date' => $rate['to'],
                        'unit_cost' => $rate['unit_cost'],
                    ]);
                }
                $room = $h->housingRooms()->create([
                    'housing_id' => $h->id,
                    'room_type_id' => $bunkBeds->id,
                    'name' => 'Bunk Beds',
                    'description' => $faker->text(),
                    'cost_type' => 'bed',
                    'beds' => 4
                ]);
                $id = DB::table('housing_room_packages')->insertGetId([
                    'package_id' => $packRoomBreakfast->id,
                    'housing_room_id' => $room->id,
                ]);
                // Insertar tarifas
                $rates = [
                    ['quantity' => 1, 'unit' => 'week', 'from' => null, 'to' => null, 'unit_cost' => 77],
                    ['quantity' => 1, 'unit' => 'week', 'from' => '2017-12-20', 'to' => '2018-01-10', 'unit_cost' => 79],
                    ['quantity' => 1, 'unit' => 'week', 'from' => '2017-04-05', 'to' => '2017-04-20', 'unit_cost' => 79],
                    ['quantity' => 1, 'unit' => 'day', 'from' => null, 'to' => null, 'unit_cost' => 11],
                    ['quantity' => 1, 'unit' => 'day', 'from' => '2017-12-20', 'to' => '2018-01-10', 'unit_cost' => 14],
                    ['quantity' => 1, 'unit' => 'day', 'from' => '2017-04-05', 'to' => '2017-04-20', 'unit_cost' => 14],
                ];
                foreach ($rates as $rate) {
                    HousingRate::create([
                        'housing_room_package_id' => $id,
                        'quantity' => $rate['quantity'],
                        'unit' => $rate['unit'],
                        'from_date' => $rate['from'],
                        'to_date' => $rate['to'],
                        'unit_cost' => $rate['unit_cost'],
                    ]);
                }
            }
            else if ($h->name === 'Mexican Family') {
                $room = $h->housingRooms()->create([
                    'housing_id' => $h->id,
                    'room_type_id' => $privateRoom->id,
                    'name' => 'Private Room',
                    'description' => $faker->text(),
                    'beds' => 1
                ]);
                $id1 = DB::table('housing_room_packages')->insertGetId([
                    'package_id' => $packHalfBoard->id,
                    'housing_room_id' => $room->id,
                ]);
                $id2 = DB::table('housing_room_packages')->insertGetId([
                    'package_id' => $packBedBreakfast->id,
                    'housing_room_id' => $room->id,
                ]);
                // Insertar tarifas
                $rates = [
                    ['quantity' => 1, 'unit' => 'week', 'from' => null, 'to' => null, 'unit_cost' => 229],
                    ['quantity' => 1, 'unit' => 'week', 'from' => '2017-12-20', 'to' => '2018-01-10', 'unit_cost' => 263],
                    ['quantity' => 1, 'unit' => 'week', 'from' => '2017-04-05', 'to' => '2017-04-20', 'unit_cost' => 263],
                    ['quantity' => 1, 'unit' => 'day', 'from' => null, 'to' => null, 'unit_cost' => 33],
                    ['quantity' => 1, 'unit' => 'day', 'from' => '2017-12-20', 'to' => '2018-01-10', 'unit_cost' => 38],
                    ['quantity' => 1, 'unit' => 'day', 'from' => '2017-04-05', 'to' => '2017-04-20', 'unit_cost' => 38],
                ];
                foreach ($rates as $rate) {
                    HousingRate::create([
                        'housing_room_package_id' => $id1,
                        'quantity' => $rate['quantity'],
                        'unit' => $rate['unit'],
                        'from_date' => $rate['from'],
                        'to_date' => $rate['to'],
                        'unit_cost' => $rate['unit_cost'],
                    ]);
                }
                // Insertar tarifas
                $rates = [
                    ['quantity' => 1, 'unit' => 'week', 'from' => null, 'to' => null, 'unit_cost' => 199],
                    ['quantity' => 1, 'unit' => 'week', 'from' => '2017-12-20', 'to' => '2018-01-10', 'unit_cost' => 228],
                    ['quantity' => 1, 'unit' => 'week', 'from' => '2017-04-05', 'to' => '2017-04-20', 'unit_cost' => 228],
                    ['quantity' => 1, 'unit' => 'day', 'from' => null, 'to' => null, 'unit_cost' => 28],
                    ['quantity' => 1, 'unit' => 'day', 'from' => '2017-12-20', 'to' => '2018-01-10', 'unit_cost' => 32],
                    ['quantity' => 1, 'unit' => 'day', 'from' => '2017-04-05', 'to' => '2017-04-20', 'unit_cost' => 32],
                ];
                foreach ($rates as $rate) {
                    HousingRate::create([
                        'housing_room_package_id' => $id2,
                        'quantity' => $rate['quantity'],
                        'unit' => $rate['unit'],
                        'from_date' => $rate['from'],
                        'to_date' => $rate['to'],
                        'unit_cost' => $rate['unit_cost'],
                    ]);
                }
                $room = $h->housingRooms()->create([
                    'housing_id' => $h->id,
                    'room_type_id' => $privateRoomCouple->id,
                    'name' => 'Private Room Per Couple',
                    'description' => $faker->text(),
                    'beds' => 2
                ]);
                $id1 = DB::table('housing_room_packages')->insertGetId([
                    'package_id' => $packHalfBoard->id,
                    'housing_room_id' => $room->id,
                ]);
                $id2 = DB::table('housing_room_packages')->insertGetId([
                    'package_id' => $packBedBreakfast->id,
                    'housing_room_id' => $room->id,
                ]);
                // Insertar tarifas
                $rates = [
                    ['quantity' => 1, 'unit' => 'week', 'from' => null, 'to' => null, 'unit_cost' => 389],
                    ['quantity' => 1, 'unit' => 'week', 'from' => '2017-12-20', 'to' => '2018-01-10', 'unit_cost' => 440],
                    ['quantity' => 1, 'unit' => 'week', 'from' => '2017-04-05', 'to' => '2017-04-20', 'unit_cost' => 440],
                    ['quantity' => 1, 'unit' => 'day', 'from' => null, 'to' => null, 'unit_cost' => 56],
                    ['quantity' => 1, 'unit' => 'day', 'from' => '2017-12-20', 'to' => '2018-01-10', 'unit_cost' => 64],
                    ['quantity' => 1, 'unit' => 'day', 'from' => '2017-04-05', 'to' => '2017-04-20', 'unit_cost' => 64],
                ];
                foreach ($rates as $rate) {
                    HousingRate::create([
                        'housing_room_package_id' => $id1,
                        'quantity' => $rate['quantity'],
                        'unit' => $rate['unit'],
                        'from_date' => $rate['from'],
                        'to_date' => $rate['to'],
                        'unit_cost' => $rate['unit_cost'],
                    ]);
                }
                // Insertar tarifas
                $rates = [
                    ['quantity' => 1, 'unit' => 'week', 'from' => null, 'to' => null, 'unit_cost' => 338],
                    ['quantity' => 1, 'unit' => 'week', 'from' => '2017-12-20', 'to' => '2018-01-10', 'unit_cost' => 388],
                    ['quantity' => 1, 'unit' => 'week', 'from' => '2017-04-05', 'to' => '2017-04-20', 'unit_cost' => 388],
                    ['quantity' => 1, 'unit' => 'day', 'from' => null, 'to' => null, 'unit_cost' => 48],
                    ['quantity' => 1, 'unit' => 'day', 'from' => '2017-12-20', 'to' => '2018-01-10', 'unit_cost' => 55],
                    ['quantity' => 1, 'unit' => 'day', 'from' => '2017-04-05', 'to' => '2017-04-20', 'unit_cost' => 55],
                ];
                foreach ($rates as $rate) {
                    HousingRate::create([
                        'housing_room_package_id' => $id2,
                        'quantity' => $rate['quantity'],
                        'unit' => $rate['unit'],
                        'from_date' => $rate['from'],
                        'to_date' => $rate['to'],
                        'unit_cost' => $rate['unit_cost'],
                    ]);
                }
            }
        }
    }
}
