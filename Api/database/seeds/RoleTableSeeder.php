<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [[
            'name' => 'Administrator',
            'slug' => 'administrator'
        ], [
            'name' => 'Agency',
            'slug' => 'agency'
        ], [
            'name' => 'Client',
            'slug' => 'client'
        ]];

        foreach ($roles as $role) {
            if (!Role::where('slug', $role['slug'])->exists()) {
                Role::create($role);
            }
        }
    }
}
