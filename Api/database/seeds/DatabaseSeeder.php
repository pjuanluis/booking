<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CoursesTableSeeder::class);
        $this->call(HousingTableSeeder::class);
        $this->call(HousingRoomsTableSeeder::class);
        $this->call(ResidencesTableSeeder::class);
        $this->call(PickupAtTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
    }
}
