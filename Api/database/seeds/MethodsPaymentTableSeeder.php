<?php

use App\MethodPayment;
use Illuminate\Database\Seeder;

class MethodsPaymentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* Para agregar nuevos métodos de pagos a la bd, agregarlos al arreglo $arrayMethodsPayment y ejecutar
        solo este seeder (php artisan make:seeder MethodsPaymentTableSeeder) */
        $arrayMethodsPayment = [
            [
                'name' => 'Credit/Debit Card',
                'slug' => str_slug('Credit/Debit Card', '_')
            ]
        ];

        foreach ($arrayMethodsPayment as $method) {
            if (!MethodPayment::where('slug', $method['slug'])->exists()) {
                MethodPayment::create($method);
            }
        }
    }
}
