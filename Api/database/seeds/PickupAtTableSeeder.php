<?php

use Illuminate\Database\Seeder;
use App\Pickup;

class PickupAtTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pickup = [
            ['name' => "Puerto Escondido Airport pick-up", 'cost' => 12],
            ['name' => "Huatulco Airport pick-up", 'cost' => 149],
            ['name' => "Huatulco Airport pick-up and drop-off", 'cost' => 298],
            ['name' => "Puerto Escondido Airport pick-up and drop-off", 'cost' => 24],
            ['name' => "PKW - Puerto Escondido Airport pick-up and drop-off", 'cost' => 26],
        ];
        
        foreach ($pickup as $pkp) {
            $p = Pickup::where('name', $pkp['name'])->first();
            if ($p) {
                $p->update($pkp);
            } else {
                Pickup::create($pkp);
            }
        }
    }
}
