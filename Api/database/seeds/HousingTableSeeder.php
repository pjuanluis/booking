<?php

use Illuminate\Database\Seeder;
use App\Housing;

class HousingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Housing::create(['name' => "School Residence", 'slug' => "school_residence"]);
        Housing::create(['name' => "Surf Camp", 'slug' => "surf_camp"]);
        Housing::create(['name' => "Mexican Family", 'slug' => "mexican_family"]);
    }
}
