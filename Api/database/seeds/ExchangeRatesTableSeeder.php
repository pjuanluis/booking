<?php

use App\Currency;
use App\ExchangeRate;
use Illuminate\Database\Seeder;

class ExchangeRatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Tipos de cambios por default
        $exchangeRates = [
            'MXN' => ['USD' => 0.051, 'EUR' => 0.046],
            'USD' => ['MXN' => 19.54, 'EUR' => 0.91],
            'EUR' => ['MXN' => 21.54, 'USD' => 1.10]
        ];
        $currency = null;
        $currency1 = null;
        foreach($exchangeRates as $code => $exchangeRate) {
            $currency = Currency::where('code', $code)->first();
            if (empty($currency)) {
                continue;
            }
            foreach ($exchangeRate as $cod => $amount) {
                $currency1 = Currency::where('code', $cod)->first();
                if (empty($currency1)) {
                    continue;
                }
                $exRa = ExchangeRate::where('base_currency_id', $currency->id)
                        ->where('to_currency_id', $currency1->id)
                        ->first();
                if (empty($exRa)) {
                    $currency->exchangeRates()->create([
                        'base_amount' => 1,
                        'to_currency_id' => $currency1->id,
                        'amount' => $amount
                    ]);
                } else {
                    $exRa->update([
                        'amount' => $amount
                    ]);
                }
            }
        }
    }
}
