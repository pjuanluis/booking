<?php

use App\Role;
use App\SystemModule;
use App\SystemPermission;
use Illuminate\Database\Seeder;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $modules = [
            [
                'name' => 'Agencies',
                'slug' => 'agencies'
            ],
            [
                'name' => 'Clients',
                'slug' => 'clients'
            ],
            [
                'name' => 'Bookings',
                'slug' => 'bookings'
            ],
            [
                'name' => 'Consumption centers',
                'slug' => 'consumption_centers'
            ],
            [
                'name' => 'Countries',
                'slug' => 'countries'
            ],
            [
                'name' => 'Home',
                'slug' => 'home'
            ],
            [
                'name' => 'Experiences',
                'slug' => 'experiences'
            ],
            [
                'name' => 'Groups',
                'slug' => 'groups'
            ],
            [
                'name' => 'Housing',
                'slug' => 'housing'
            ],
            [
                'name' => 'Housing/rooms',
                'slug' => 'housing_rooms'
            ],
            [
                'name' => 'Housing/rooms options',
                'slug' => 'housing_rooms_options'
            ],
            [
                'name' => 'Import bookings',
                'slug' => 'import_bookings'
            ],
            [
                'name' => 'People',
                'slug' => 'people'
            ],
            [
                'name' => 'Petty cash',
                'slug' => 'petty_cash'
            ],
            [
                'name' => 'Places',
                'slug' => 'places'
            ],
            [
                'name' => 'Purchases',
                'slug' => 'purchases'
            ],
            [
                'name' => 'Residences',
                'slug' => 'residences'
            ],
            [
                'name' => 'Rooms',
                'slug' => 'rooms'
            ],
            [
                'name' => 'Room types',
                'slug' => 'room_types'
            ],
            [
                'name' => 'Schedules',
                'slug' => 'schedules'
            ],
            [
                'name' => 'Courses',
                'slug' => 'courses'
            ],
            [
                'name' => 'Roles',
                'slug' => 'roles'
            ],
            [
                'name' => 'Users',
                'slug' => 'users'
            ],
            [
                'name' => 'Reports',
                'slug' => 'reports'
            ],
            [
                'name' => 'Currencies',
                'slug' => 'currencies'
            ],
        ];

        foreach ($modules as $module) {
            if (!SystemModule::where('slug', $module['slug'])->exists()) {
                SystemModule::create($module);
            }
        }

        // Asignar permisos a los módulos
        $permissions = SystemPermission::all();
        $modules = SystemModule::all();
        foreach ($modules as $module) {
            foreach ($permissions as $permission) {
                if (!$module->permissions()->where('system_permission_id', $permission->id)->exists()) {
                    $module->permissions()->attach($permission->id);
                }
            }
        }
    }
}
