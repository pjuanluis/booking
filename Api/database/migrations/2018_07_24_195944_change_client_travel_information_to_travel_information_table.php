<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Client;

class ChangeClientTravelInformationToTravelInformationTable extends Migration
{

    private $oldTableName = 'client_travel_information';
    private $newTableName = 'travel_information';
    private $dateOfChange = '2018-08-01 00:00:00';

    /**
     * Change id to first command and logical deletion for rest.
     */
    private function changeToCommandId()
    {
        $client = null;
        $clientIdColumn = 'command_id'; // Because client_id was changed to command_id
        $changeDate = Carbon::parse($this->dateOfChange);
        $newTableRows = DB::table($this->newTableName)
            ->select('id', "$clientIdColumn as client_id")
            ->get();

        foreach ($newTableRows as $k => $v) {
            $client = Client::find($v->client_id);

            if (
                isset($client) && 
                ($firstCommand = $client->commands()->select('id')->first()) !== null
            ) {
                // UPDATE command_id
                DB::update(
                    "update {$this->newTableName} set {$clientIdColumn} = {$firstCommand->id} where id = ?",
                    [ $v->id ]
                );
            } else {
                // DELETE travel
                DB::update(
                    "update {$this->newTableName} set deleted_at = '{$changeDate}' where id = ?",
                    [ $v->id ]
                );
            }
        }
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->oldTableName, function (Blueprint $table) {
            $table->dropForeign(['client_id']);
        });
        Schema::rename($this->oldTableName, $this->newTableName);
        Schema::table($this->newTableName, function (Blueprint $table) {
            $table->increments('id');
            $table->renameColumn('client_id', 'command_id');
            $table->foreign('command_id')->references('id')->on('commands');
        });
        $this->changeToCommandId();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->newTableName, function (Blueprint $table) {
            $table->dropForeign(['command_id']);
        });
        Schema::rename($this->newTableName, $this->oldTableName);
        Schema::table($this->oldTableName, function (Blueprint $table) {
            $table->dropColumn('id');
            $table->renameColumn('command_id', 'client_id');
            $table->foreign('client_id')->references('id')->on('clients');
        });
        // TODO: How to return rows of travel_information to client_travel_information
    }
}
