<?php

use App\Currency;
use App\Payment;
use App\MethodPayment;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->integer('currency_id')->nullable();
            $table->integer('method_payment_id')->nullable();

            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->foreign('method_payment_id')->references('id')->on('methods_payment');
        });

        // Asignar una moneda y un método de pago a los págos existentes
        $currencyId = Currency::where('slug', 'us_dollar')->value('id');
        $paymentMethodId = MethodPayment::where('slug', 'paypal')->value('id');
        $payments = Payment::whereIn('type', ['registration_fee', 'reservation_payment'])->get();
        if ($currencyId && $paymentMethodId) {
            foreach ($payments as $key => $payment) {
                $payment->update([
                    'currency_id' => $currencyId,
                    'method_payment_id' => $paymentMethodId,
                ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->dropColumn(['currency_id', 'method_payment_id']);
        });
    }
}
