<?php

use App\Country;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTheOrderOfGermanyInTheCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $germany = Country::where('code', 'DE')->first();
        $afghanistan = Country::where('code', 'AF')->first();
        $order = $germany->order;
        $germany->order = $afghanistan->order;
        $afghanistan->order = $order;
        $germany->save();
        $afghanistan->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $germany = Country::where('code', 'DE')->first();
        $afghanistan = Country::where('code', 'AF')->first();
        $order = $afghanistan->order;
        $afghanistan->order = $germany->order;
        $germany->order = $order;
        $germany->save();
        $afghanistan->save();
    }
}
