<?php

use App\Status;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('slug')->unique();
            $table->tinyInteger('is_active')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });

        $arrayStatuses = [
            [
                'name' => 'Pending',
                'slug' => 'pending'
            ],
            [
                'name' => 'Paid',
                'slug' => 'paid'
            ],
            [
                'name' => 'Cancelled',
                'slug' => 'cancelled'
            ]
        ];

        foreach ($arrayStatuses as $key => $status) {
            Status::create($status);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statuses');
    }
}
