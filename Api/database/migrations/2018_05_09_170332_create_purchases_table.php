<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->string('reference_number')->nullable();
            $table->string('concept');
            $table->float('amount')->default(0);
            $table->float('discount')->default(0);
            $table->integer('client_id');
            $table->integer('command_id')->nullable();
            $table->integer('status_id');
            $table->integer('method_payment_id');
            $table->integer('consumption_center_id');
            $table->integer('currency_id');

            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('command_id')->references('id')->on('commands');
            $table->foreign('status_id')->references('id')->on('statuses');
            $table->foreign('method_payment_id')->references('id')->on('methods_payment');
            $table->foreign('consumption_center_id')->references('id')->on('consumption_centers');
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}
