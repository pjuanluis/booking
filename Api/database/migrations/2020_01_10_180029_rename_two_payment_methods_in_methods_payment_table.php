<?php

use App\MethodPayment;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameTwoPaymentMethodsInMethodsPaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Cambiar el nombre de la cuenta A
        $paymentMethod = MethodPayment::where('slug', str_slug('Bank Account A', '_'))
                ->first();
        if ($paymentMethod) {
            $paymentMethod->update([
                'name' => 'Santander Bank',
                'slug' => str_slug('Santander Bank', '_')
            ]);
        }
        // Cambiar el nombre de la cuenta B
        $paymentMethod = MethodPayment::where('slug', str_slug('Bank Account B', '_'))
                ->first();
        if ($paymentMethod) {
            $paymentMethod->update([
                'name' => 'HSBC Bank',
                'slug' => str_slug('HSBC Bank', '_')
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Cambiar el nombre de la cuenta Santander Bank
        $paymentMethod = MethodPayment::where('slug', str_slug('Santander Bank', '_'))
                ->first();
        if ($paymentMethod) {
            $paymentMethod->update([
                'name' => 'Bank Account A',
                'slug' => str_slug('Bank Account A', '_')
            ]);
        }
        // Cambiar el nombre de la cuenta HSBC Bank
        $paymentMethod = MethodPayment::where('slug', str_slug('HSBC Bank', '_'))
                ->first();
        if ($paymentMethod) {
            $paymentMethod->update([
                'name' => 'Bank Account B',
                'slug' => str_slug('Bank Account B', '_')
            ]);
        }
    }
}
