<?php

use App\Currency;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyInformationInTheCurrenciesTablep extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('currencies', function (Blueprint $table) {
            $table->string('code')->nullable();
        });
        $arrayCurrencies = [
            [
                'name' => 'US Dollar',
                'code' => 'USD',
                'slug' => 'us_dollar'
            ],
            [
                'name' => 'Euro',
                'code' => 'EUR',
                'slug' => 'euro',
            ],
            [
                'name' => 'Mexican Peso',
                'code' => 'MXN',
                'slug' => 'mexican_peso',
            ]
        ];
        Currency::where('slug', 'usd')->update($arrayCurrencies[0]);
        Currency::where('slug', 'euro')->update($arrayCurrencies[1]);
        Currency::where('slug', 'peso')->update($arrayCurrencies[2]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('currencies', function (Blueprint $table) {
            $table->dropColumn('code');
        });
        $arrayCurrencies = [
            [
                'name' => 'USD',
                'slug' => 'usd'
            ],
            [
                'name' => 'Euro',
                'slug' => 'euro'
            ],
            [
                'name' => 'Peso',
                'slug' => 'peso'
            ]
        ];
        Currency::where('slug', 'us_dollar')->update($arrayCurrencies[0]);
        Currency::where('slug', 'euro')->update($arrayCurrencies[1]);
        Currency::where('slug', 'mexican_peso')->update($arrayCurrencies[2]);
    }
}
