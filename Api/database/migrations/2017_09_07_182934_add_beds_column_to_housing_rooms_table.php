<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBedsColumnToHousingRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('housing_rooms', function (Blueprint $table) {
            $table->smallInteger('beds')->default(1);
        });
        $result = DB::table('housing_rooms')
                ->select('housing_rooms.id', 'room_types.slug')
                ->leftJoin('room_types', 'housing_rooms.room_type_id', '=', 'room_types.id')
                ->get();
        foreach ($result as $r) {
            $beds = 1;
            if ($r->slug === 'bunk_beds') {
                $beds = 4;
            }
            else if ($r->slug === 'shared_room') {
                $beds = 2;
            }
            DB::table('housing_rooms')
                    ->where('id', $r->id)
                    ->update([
                        'beds' => $beds
                    ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('housing_rooms', function (Blueprint $table) {
            $table->dropColumn('beds');
        });
    }
}
