<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduleDateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule_date', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('schedule_id');
            $table->foreign('schedule_id')->references('id')->on('schedules');
            $table->timestamp('begin_at');
            $table->timestamp('finish_at');
        });
        Schema::table('schedules', function (Blueprint $table) {
            $table->dropColumn('begin_at');
            $table->dropColumn('finish_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedule_date');
        Schema::table('schedules', function (Blueprint $table) {
            $table->timestamp('begin_at')->nullable();
            $table->timestamp('finish_at')->nullable();
        });
    }
}
