<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_experiences', function (Blueprint $table) {
            $table->integer('package_id');
            $table->foreign('package_id')->references('id')->on('packages');
            $table->integer('experience_id');
            $table->foreign('experience_id')->references('id')->on('experiences');
            $table->unique(['package_id', 'experience_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_experiences');
    }
}
