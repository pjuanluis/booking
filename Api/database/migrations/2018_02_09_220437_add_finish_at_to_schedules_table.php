<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;
use App\Schedule;

class AddFinishAtToSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schedules', function (Blueprint $table) {
            $table->dateTime('finish_at')->nullable();
        });

        Schedule::all()->each(function($schedule) {
            $schedule->finish_at = Carbon::parse($schedule->begin_at)->addHour();
            $schedule->save();
        });

        Schema::table('schedules', function (Blueprint $table) {
            $table->dateTime('finish_at')->nullable(false)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('schedules', function (Blueprint $table) {
            $table->dropColumn('finish_at');
        });
    }
}
