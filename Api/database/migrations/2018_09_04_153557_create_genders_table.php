<?php

use App\Gender;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGendersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('genders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->tinyInteger('is_active')->default(1);
            $table->timestamps();
        });

        // Insertar los generos por default
        $genders = [
            [
                'name' => 'Male',
                'slug' => 'male'
            ],
            [
                'name' => 'Female',
                'slug' => 'female'
            ],
            [
                'name' => 'No binary',
                'slug' => 'no_binary'
            ],
        ];
        foreach ($genders as $key => $gender) {
            Gender::create($gender);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('genders');
    }
}
