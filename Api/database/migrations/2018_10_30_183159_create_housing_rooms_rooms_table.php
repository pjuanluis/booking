<?php

use App\Room;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHousingRoomsRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('housing_rooms_rooms', function (Blueprint $table) {
            $table->integer('room_id');
            $table->foreign('room_id')->references('id')->on('rooms');
            $table->integer('housing_room_id');
            $table->foreign('housing_room_id')->references('id')->on('housing_rooms');
            $table->unique(['room_id', 'housing_room_id']);
        });

        // Llenar la nueva tabla con los registros existentes
        $rooms = Room::all();
        foreach ($rooms as $room) {
            $room->housingRooms()->attach($room->housing_room_id);
        }

        // Borrar la columna housing_room_id
        Schema::table('rooms', function (Blueprint $table) {
            $table->dropColumn('housing_room_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('housing_rooms_rooms');
        Schema::table('rooms', function (Blueprint $table) {
            $table->integer('housing_room_id');
            $table->foreign('housing_room_id')->references('id')->on('housing_rooms');
        });
    }
}
