<?php

use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class GenerateBookingCodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $result = DB::table('commands')
                ->select('id', 'created_at')
                ->get();
        $code = '';
        $date = Carbon::now();
        foreach ($result as $r) {
            $date = new Carbon($r->created_at);
            $code = "WWW-" . $date->format('y') . $date->format('m') . '-' . str_pad($r->id, 5, '0', STR_PAD_LEFT);
            DB::table('commands')
                    ->where('id', $r->id)
                    ->update(['booking_code' => $code]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
