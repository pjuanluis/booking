<?php

use App\Payment;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExchangeRatesColumnToPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->jsonb('exchange_rates')->default('[]');
        });

        $mxnExchangeRates = json_encode([
            ['base_currency_id' => 3, 'to_currency_id' => 1, 'base_amount' => 1, 'amount' => 0.051],
            ['base_currency_id' => 3, 'to_currency_id' => 2, 'base_amount' => 1, 'amount' => 0.046]
        ]);
        $usdExchangeRates = json_encode([
            ['base_currency_id' => 1, 'to_currency_id' => 3, 'base_amount' => 1, 'amount' => 19.2],
            ['base_currency_id' => 1, 'to_currency_id' => 2, 'base_amount' => 1, 'amount' => 0.91]
        ]);
        $eurExchangeRates = json_encode([
            ['base_currency_id' => 2, 'to_currency_id' => 3, 'base_amount' => 1, 'amount' => 21.54],
            ['base_currency_id' => 2, 'to_currency_id' => 1, 'base_amount' => 1, 'amount' => 1.1]
        ]);

        // Agregar los tipos de cambios a los pagos hechos con USD apartir de 2020-04-09(fecha en que se cambio el tipo de cambio)
        Payment::whereDate('paid_at', '>=', '2020-04-09')
                ->whereHas('currency', function($query) {
                    $query->where('code', 'USD');
                })
                ->update([
                    'exchange_rates' => json_encode([
                        ['base_currency_id' => 1, 'to_currency_id' => 3, 'base_amount' => 1, 'amount' => 24],
                        ['base_currency_id' => 1, 'to_currency_id' => 2, 'base_amount' => 1, 'amount' => 0.91]
                    ])
                ]);
        // Agregar los tipos de cambios a los pagos hechos con USD anteriores a 2020-04-09(fecha en que se cambio el tipo de cambio)
        Payment::whereDate('paid_at', '<', '2020-04-09')
                ->whereHas('currency', function($query) {
                    $query->where('code', 'USD');
                })
                ->update([
                    'exchange_rates' => $usdExchangeRates
                ]);
        // Agregar los tipos de cambios a los pagos hechos con MXN
        Payment::whereHas('currency', function($query) {
                    $query->where('code', 'MXN');
                })
                ->update([
                    'exchange_rates' => $mxnExchangeRates
                ]);
        // Agregar los tipos de cambios a los pagos hechos con EUR
        Payment::whereHas('currency', function($query) {
                    $query->where('code', 'EUR');
                })
                ->update([
                    'exchange_rates' => $eurExchangeRates
                ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->dropColumn('exchange_rates');
        });
    }
}
