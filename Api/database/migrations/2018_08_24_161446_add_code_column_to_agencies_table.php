<?php

use App\Agency;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodeColumnToAgenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('agencies', function (Blueprint $table) {
            $table->string('code')->default('AGE');
        });
        // Cambiar el prefijo del booking_code de las comandas que pertenecen a una agencia
        $agencies = Agency::all();
        foreach ($agencies as $key => $agency) {
            $agency->updateBookingCode();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('agencies', function (Blueprint $table) {
            $table->dropColumn('code');
        });
    }
}
