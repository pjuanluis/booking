<?php

use App\Country;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetTheOrderFieldValueInTheCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $countries = Country::orderBy('name', 'asc')->get();
        foreach ($countries as $key => $country) {
            $country->order = $key + 1;
            $country->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $countries = Country::orderBy('name', 'asc')->get();
        foreach ($countries as $key => $country) {
            $country->order = 0;
            $country->save();
        }
    }
}
