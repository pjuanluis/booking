<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commands', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')->unsigned()->nullable();
            $table->foreign('client_id')->references('id')->on('clients');
            $table->integer('agency_id')->unsigned()->nullable();
            $table->foreign('agency_id')->references('id')->on('agencies');
            $table->integer('pickup_id')->unsigned()->nullable();
            $table->foreign('pickup_id')->references('id')->on('pickup');
            $table->string('client_first_name');
            $table->string('client_last_name');
            $table->string('client_email');
            $table->string('booking_code')->unique();
            $table->string('token')->unique();
            $table->date('from_date')->nullable();
            $table->date('to_date')->nullable();
            $table->double('pickup_cost')->default(0);
            $table->string('status')->default('booked');
            $table->tinyInteger('is_active')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commands');
    }
}
