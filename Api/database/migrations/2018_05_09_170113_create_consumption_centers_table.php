<?php

use App\ConsumptionCenter;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsumptionCentersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consumption_centers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('slug')->unique();
            $table->tinyInteger('is_active')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });

        $arrayConsumptionCenters = [
            [
                'name' => 'Surf Camp',
                'slug' => 'surf_camp'
            ],
            [
                'name' => 'School Cafeteria',
                'slug' => 'school_cafeteria'
            ],
            [
                'name' => 'School Reception',
                'slug' => 'school_reception'
            ],
            [
                'name' => 'School Accountancy',
                'slug' => 'school_accountancy'
            ]
        ];

        foreach ($arrayConsumptionCenters as $key => $consumptionCenter) {
            ConsumptionCenter::create($consumptionCenter);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consumption_centers');
    }
}
