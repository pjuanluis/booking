<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnsInTravelInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('travel_information', function (Blueprint $table) {
            $table->string('arrival_place')->nullable()->change();
            $table->string('departure_place')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('travel_information', function (Blueprint $table) {
            $table->string('arrival_place')->change();
            $table->string('departure_place')->change();
        });
    }
}
