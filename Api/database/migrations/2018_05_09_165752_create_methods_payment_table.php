<?php

use App\MethodPayment;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMethodsPaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('methods_payment', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('slug')->unique();
            $table->tinyInteger('is_active')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });

        $arrayMethodsPayment = [
            [
                'name' => 'None',
                'slug' => 'none'
            ],
            [
                'name' => 'Cash',
                'slug' => 'cash'
            ],
            [
                'name' => 'Paypal',
                'slug' => 'paypal'
            ],
            [
                'name' => 'iZettle',
                'slug' => 'izettle'
            ]
        ];

        foreach ($arrayMethodsPayment as $key => $methodPayment) {
            MethodPayment::create($methodPayment);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('methods_payment');
    }
}
