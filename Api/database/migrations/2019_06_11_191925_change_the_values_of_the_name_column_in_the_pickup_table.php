<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTheValuesOfTheNameColumnInThePickupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('pickup')
            ->where('name', 'Puerto Escondido airport')
            ->whereNull('deleted_at')
            ->update(['name' => 'Puerto Escondido Airport pick-up']);
        DB::table('pickup')
            ->where('name', 'Huatulco airport')
            ->whereNull('deleted_at')
            ->update(['name' => 'Huatulco Airport pick-up']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('pickup')
            ->where('name', 'Puerto Escondido Airport pick-up')
            ->whereNull('deleted_at')
            ->update(['name' => 'Puerto Escondido airport']);
        DB::table('pickup')
            ->where('name', 'Huatulco Airport pick-up')
            ->whereNull('deleted_at')
            ->update(['name' => 'Huatulco airport']);
    }
}
