<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreHousingRoomsColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('housing_rooms', function (Blueprint $table) {
            $table->string('cost_type')->default('room');
        });
        Schema::table('command_housing', function (Blueprint $table) {
            $table->smallInteger('people')->default(1);
        });
        $housingRooms = DB::table('housing_rooms as hr')
                ->select('hr.id', 'rt.slug')
                ->leftJoin('room_types as rt', 'hr.room_type_id', '=', 'rt.id')
                ->get();
        foreach ($housingRooms as $hr) {
            if (in_array($hr->slug, ['bunk_beds'])) {
                DB::table('housing_rooms')->where('id', $hr->id)->update(['cost_type' => 'bed']);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('housing_rooms', function (Blueprint $table) {
            $table->dropColumn('cost_type');
        });
        Schema::table('command_housing', function (Blueprint $table) {
            $table->dropColumn('people');
        });
    }
}
