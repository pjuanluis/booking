<?php

use App\ConsumptionCenter;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertIntoConsumptionCentersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('consumption_centers', function (Blueprint $table) {
            $arrayConsumptionCenters = [
                [
                    'name' => 'Website',
                    'slug' => 'website'
                ],
                [
                    'name' => 'Walk in',
                    'slug' => 'walk_in'
                ]
            ];

            foreach ($arrayConsumptionCenters as $key => $consumptionCenter) {
                ConsumptionCenter::create($consumptionCenter);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('consumption_centers', function (Blueprint $table) {
            ConsumptionCenter::where('slug', 'website')->where('slug', 'walk_in')->delete();
        });
    }
}
