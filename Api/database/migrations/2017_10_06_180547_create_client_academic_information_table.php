<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientAcademicInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_academic_information', function (Blueprint $table) {
            $table->integer('client_id');
            $table->foreign('client_id')->references('id')->on('clients');
            $table->string('spanish_level');
            $table->string('spanish_knowledge_duration');
            $table->string('has_surf_experience')->default(0);
            $table->string('surf_experience_duration');
            $table->string('has_volunteer_experience')->default(0);
            $table->string('volunteer_experience_duration');
            $table->string('volunteer_experience')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_academic_information');
    }
}
