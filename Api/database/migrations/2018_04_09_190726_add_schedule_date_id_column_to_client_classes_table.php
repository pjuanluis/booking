<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddScheduleDateIdColumnToClientClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_classes', function (Blueprint $table) {
            $table->dropColumn('schedule_id');
            $table->integer('schedule_date_id')->nullable();
            $table->foreign('schedule_date_id')->references('id')->on('schedule_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_classes', function (Blueprint $table) {
            $table->dropColumn('schedule_date_id');
            $table->integer('schedule_id');
            $table->foreign('schedule_id')->references('id')->on('schedules');
        });
    }
}
