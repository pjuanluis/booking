<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RefactorTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('packages', function (Blueprint $table) {
            $table->dropColumn('packageable_id');
            $table->dropColumn('packageable_type');
            $table->dropColumn('has_fee_per_duration');
            $table->string('type')->default('experience');
            $table->integer('quantity')->default(1);
            $table->string('unit');
            $table->double('cost')->default(0);
        });
        Schema::table('course_rates', function (Blueprint $table) {
            $table->dropColumn('unit');
        });
        Schema::table('command_courses', function (Blueprint $table) {
            $table->dropColumn('unit');
        });
        Schema::dropIfExists('courses');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
