<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHousingRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('housing_rates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('housing_room_package_id')->unsigned();
            $table->foreign('housing_room_package_id')->references('id')->on('housing_room_packages');
            $table->date('from_date')->nullable();
            $table->date('to_date')->nullable();
            $table->smallInteger('quantity')->default(1);
            $table->string('unit')->default('day');
            $table->double('unit_cost');
            $table->tinyInteger('is_active')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('housing_rates');
    }
}
