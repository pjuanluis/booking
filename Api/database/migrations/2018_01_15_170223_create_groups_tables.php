<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('level');
            $table->tinyInteger('is_private')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('group_students', function (Blueprint $table) {
            $table->integer('group_id');
            $table->foreign('group_id')->references('id')->on('groups');
            $table->integer('client_id');
            $table->foreign('client_id')->references('id')->on('clients');
            $table->tinyInteger('is_active')->default(1);
            $table->timestamps();
            $table->softDeletes();
            $table->unique(['group_id', 'client_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_students');
        Schema::dropIfExists('groups');
    }
}
