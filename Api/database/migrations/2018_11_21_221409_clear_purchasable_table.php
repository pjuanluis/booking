<?php

use App\Command;
use App\Purchase;
use App\CommandCourse;
use App\CommandHousing;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ClearPurchasableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Eliminar las compras extras que no estén relacionadas a una comanda
        Purchase::whereNull('command_id')->delete();

        // Obtener unicamente las comandas eliminadas;
        $deletedCommands = Command::withTrashed()
                            ->with('purchase')
                            ->whereNotNull('deleted_at')
                            ->get();
        // Obtener unicamente las comandas de cursos eliminadas;
        $deletedCommandCourses = CommandCourse::withTrashed()
                            ->with('purchase')
                            ->whereNotNull('deleted_at')
                            ->get();
        // Obtener unicamente las comandas de hospedaje eliminadas;
        $deletedCommandHousing = CommandHousing::withTrashed()
                            ->with('purchase')
                            ->whereNotNull('deleted_at')
                            ->get();
        // Obtener unicamente las compras extras eliminadas
        $deletedPurchases = Purchase::withTrashed()
                            ->with('purchasable')
                            ->whereNotNull('deleted_at')
                            ->get();

        // Eliminar las compras que pertenecián a las comandas
        foreach ($deletedCommands as $command) {
            $command->purchase()->delete();
        }
        // Eliminar las compras que pertenecián a las comandas de cursos
        foreach ($deletedCommandCourses as $cc) {
            $cc->purchase()->delete();
        }
        // Eliminar las compras que pertenecián a las comandas de hospedaje
        foreach ($deletedCommandHousing as $ch) {
            $ch->purchase()->delete();
        }
        // Eliminar las compras que pertenecián a las compras extras
        foreach ($deletedPurchases as $p) {
            $p->purchasable()->delete();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // No hay nada que hacer, dado que el modelo Purchasable no usa SoftDeletes
    }
}
