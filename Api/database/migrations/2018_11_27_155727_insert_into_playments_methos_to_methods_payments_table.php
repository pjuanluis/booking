<?php

use App\MethodPayment;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertIntoPlaymentsMethosToMethodsPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        $arrayMethodsPayment = [
            [
                'name' => 'Bank Account A',
                'slug' => str_slug('Bank Account A', '_')
            ],
            [
                'name' => 'Bank Account B',
                'slug' => str_slug('Bank Account B', '_')
            ]
        ];
        foreach($arrayMethodsPayment as $key => $methodPayment) {
            MethodPayment::create($methodPayment);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('methods_payment')->whereIn('slug', [str_slug('Bank Account A', '_'), str_slug('Bank Account B', '_')])->delete();
    }
}
