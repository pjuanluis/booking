<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyPurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchases', function (Blueprint $table) {
            $table->datetime('date')->change();
            $table->float('discount')->nullable()->change();
            $table->integer('client_id')->nullable()->change();
            $table->text('concept')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchases', function (Blueprint $table) {
            $table->date('date')->change();
            $table->float('discount')->change();
            $table->integer('client_id')->change();
            $table->string('concept')->change();
        });
    }
}
