<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('filename');
            $table->string('storage_path');
            $table->text('description')->nullable();
            $table->string('status');
            $table->text('details')->nullable();
            $table->integer('percent_completed');
            $table->timestamps();
            $table->timestamp('finished_at')->nullable();
        });
        Schema::create('booking_imports', function (Blueprint $table) {
            $table->increments('id');
            $table->text('data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imports');
        Schema::dropIfExists('booking_imports');
    }
}
