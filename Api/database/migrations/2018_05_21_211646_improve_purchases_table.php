<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Observers\CommandObserver;
use App\Observers\CommandCourseObserver;
use App\Observers\CommandHousingObserver;
use App\Purchase;
use App\Purchasable;
use App\Command;
use App\CommandCourse;
use App\CommandHousing;

class ImprovePurchasesTable extends Migration
{
    
    private function createTablePurchasables()
    {
        Schema::create('purchasables', function (Blueprint $table) {
            $table->increments('id');
            $table->float('discount')->default(0);
            $table->integer('status_id')->unsigned();
            $table->integer('method_payment_id')->unsigned();
            $table->integer('consumption_center_id')->unsigned();
            $table->integer('currency_id')->unsigned();
            $table->integer('client_id')->nullable();
            $table->integer('purchasable_id')->unsigned();
            $table->string('purchasable_type');

            $table->foreign('status_id')->references('id')->on('statuses');
            $table->foreign('method_payment_id')->references('id')->on('methods_payment');
            $table->foreign('consumption_center_id')->references('id')->on('consumption_centers');
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->foreign('client_id')->references('id')->on('clients');
        });
    }

    private function dropPurchasesColumns()
    {
        Schema::table('purchases', function (Blueprint $table) {
            $table->dropColumn('discount');
            $table->dropColumn('status_id');
            $table->dropColumn('method_payment_id');
            $table->dropColumn('consumption_center_id');
            $table->dropColumn('currency_id');
            $table->dropColumn('command_id');
            $table->dropColumn('client_id');
        });
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->createTablePurchasables();
        $this->dropPurchasesColumns();
        
        $commandObserver = new CommandObserver();
        foreach (Command::all() as $c) {
            $commandObserver->created($c);
        }
        
        $commandCourseObserver = new CommandCourseObserver();
        foreach (CommandCourse::all() as $cc) {
            $commandCourseObserver->created($cc);
        }
        
        $commandHousingObserver = new CommandHousingObserver();
        foreach (CommandHousing::all() as $cc) {
            $commandHousingObserver->created($cc);
        }
    }

    private function restorePurchasesColumns()
    {
        Schema::table('purchases', function (Blueprint $table) {
            $table->float('discount')->default(0);
            $table->integer('command_id')->nullable();
            $table->integer('status_id')->nullable();
            $table->integer('method_payment_id')->nullable();
            $table->integer('consumption_center_id')->nullable();
            $table->integer('currency_id')->nullable();
            $table->integer('client_id')->nullable();

            $table->foreign('command_id')->references('id')->on('commands');
            $table->foreign('status_id')->references('id')->on('statuses');
            $table->foreign('method_payment_id')->references('id')->on('methods_payment');
            $table->foreign('consumption_center_id')->references('id')->on('consumption_centers');
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->foreign('client_id')->references('id')->on('clients');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchasables');
        $this->restorePurchasesColumns(); 
    }
}
