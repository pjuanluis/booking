<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('schedulable_id'); // ID del grupo o ID del cliente
            $table->string('schedulable_type'); // App\Group o App\Client
            $table->integer('responsible_id');
            $table->foreign('responsible_id')->references('id')->on('people');
            $table->integer('place_id');
            $table->foreign('place_id')->references('id')->on('places');
            $table->string('subject');
            $table->timestamp('begin_at');
            $table->tinyInteger('is_private')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('client_classes', function (Blueprint $table) {
            $table->integer('schedule_id');
            $table->foreign('schedule_id')->references('id')->on('schedules');
            $table->integer('client_id');
            $table->foreign('client_id')->references('id')->on('clients');
            $table->tinyInteger('taken')->default(0);
            $table->timestamp('canceled_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_classes');
        Schema::dropIfExists('schedules');
    }
}
