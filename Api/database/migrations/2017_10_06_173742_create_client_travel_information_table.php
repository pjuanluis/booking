<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientTravelInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_travel_information', function (Blueprint $table) {
            $table->integer('client_id');
            $table->foreign('client_id')->references('id')->on('clients');
            $table->string('travel_by');
            $table->string('arrival_place');
            $table->string('arrival_carrier')->nullable();
            $table->timestamp('arrival_at')->nullable();
            $table->string('arrival_travel_number')->nullable();
            $table->string('departure_place');
            $table->string('departure_carrier')->nullable();
            $table->timestamp('departure_at')->nullable();
            $table->string('departure_travel_number')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_travel_information');
    }
}
