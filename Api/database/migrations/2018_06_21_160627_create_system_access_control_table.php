<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemAccessControlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_access_control', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role_id')->unsigned();
            $table->foreign('role_id')->references('id')->on('roles');
            $table->integer('system_module_id')->unsigned();
            $table->foreign('system_module_id')->references('id')->on('system_modules');
            $table->integer('system_permission_id')->unsigned();
            $table->foreign('system_permission_id')->references('id')->on('system_permissions');
            $table->unique(['role_id', 'system_module_id', 'system_permission_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_access_control');
    }
}
