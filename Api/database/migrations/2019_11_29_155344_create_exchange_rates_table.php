<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExchangeRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exchange_rates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('base_currency_id')->unsigned();
            $table->foreign('base_currency_id')->references('id')->on('currencies');
            $table->integer('to_currency_id')->unsigned();
            $table->foreign('to_currency_id')->references('id')->on('currencies');
            $table->integer('base_amount')->unsigned()->default(1);
            $table->double('amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exchange_rates');
    }
}
