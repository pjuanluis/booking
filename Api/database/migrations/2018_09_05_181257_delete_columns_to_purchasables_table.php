<?php

use App\Status;
use App\MethodPayment;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteColumnsToPurchasablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchasables', function (Blueprint $table) {
            $table->dropColumn('status_id');
            $table->dropColumn('method_payment_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $status = Status::where('slug', 'pending')->value('id');
        $methodPayment = MethodPayment::where('slug', 'none')->value('id');
        if ($status && $methodPayment) {
            Schema::table('purchasables', function (Blueprint $table) use($status, $methodPayment){
                $table->integer('status_id')->unsigned()->default($status);
                $table->integer('method_payment_id')->unsigned()->default($methodPayment);

                $table->foreign('status_id')->references('id')->on('statuses');
                $table->foreign('method_payment_id')->references('id')->on('methods_payment');
            });
        }
    }
}
