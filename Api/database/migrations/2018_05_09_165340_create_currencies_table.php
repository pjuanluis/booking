<?php

use App\Currency;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currencies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('slug')->unique();
            $table->tinyInteger('is_active')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });

        $arrayCurrencies = [
            [
                'name' => 'USD',
                'slug' => 'usd'
            ],
            [
                'name' => 'Euro',
                'slug' => 'euro'
            ],
            [
                'name' => 'Peso',
                'slug' => 'peso'
            ]
        ];

        foreach ($arrayCurrencies as $key => $currency) {
            Currency::create($currency);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currencies');
    }
}
