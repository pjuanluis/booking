<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderColumnToCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('countries', function (Blueprint $table) {
            $table->integer('order')->default(0);
        });
        $defaults = [
            'US' => 1,
            'DE' => 2,
            'NO' => 3,
            'RU' => 4,
            'CZ' => 5,
            'MX' => 6
        ];
        $countries = \App\Country::orderBy('name')->get();
        $order = 7;
        foreach ($countries as $country) {
            if (array_key_exists($country->code, $defaults)) {
                $country->order = $defaults[$country->code];
            }
            else {
                $country->order = $order;
            }
            $country->save();
            $order++;
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('countries', function (Blueprint $table) {
            $table->dropColumn('order');
        });
    }
}
