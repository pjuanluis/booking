<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteRoomTmpIdColumnInCommandHousingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('command_housing', function (Blueprint $table) {
            $table->dropColumn('room_tmp_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('command_housing', function (Blueprint $table) {
            $table->integer('room_tmp_id')->nullable();
            $table->foreign('room_tmp_id')->references('id')->on('rooms');
        });
    }
}
