<?php

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRoomTmpIdColumnToCommandHousingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('command_housing', function (Blueprint $table) {
            $table->integer('room_tmp_id')->nullable();
            $table->foreign('room_tmp_id')->references('id')->on('rooms');
        });

        // Seleccionar las comandas de hospedaje con cuarto asignado
        $commandsHousing = DB::table('command_housing')
            ->select('id', 'room_id')
            ->whereNotNull('room_id')
            ->whereNull('deleted_at')
            ->get();
        // Poner el cuarto de la comanda también como cuarto temporal
        foreach ($commandsHousing as $ch) {
            DB::table('command_housing')
                ->where('id', $ch->id)
                ->update(['room_tmp_id' => $ch->room_id]);
        }

        // Asignar un cuarto temporal a las comandas que no tengan uno
        // $controller = new Controller();
        // $controller->assignRooms();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('command_housing', function (Blueprint $table) {
            $table->dropColumn('room_tmp_id');
        });
    }
}
