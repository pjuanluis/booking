<?php

use App\Purchasable;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExchangeRatesColumnToPurchasablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchasables', function (Blueprint $table) {
            $table->jsonb('exchange_rates')->default('[]');
        });

        $mxnExchangeRates = json_encode([
            ['base_currency_id' => 3, 'to_currency_id' => 1, 'base_amount' => 1, 'amount' => 0.051],
            ['base_currency_id' => 3, 'to_currency_id' => 2, 'base_amount' => 1, 'amount' => 0.046]
        ]);
        $usdExchangeRates = json_encode([
            ['base_currency_id' => 1, 'to_currency_id' => 3, 'base_amount' => 1, 'amount' => 19.2],
            ['base_currency_id' => 1, 'to_currency_id' => 2, 'base_amount' => 1, 'amount' => 0.91]
        ]);
        $usdExchangeRatesNew = json_encode([
            ['base_currency_id' => 1, 'to_currency_id' => 3, 'base_amount' => 1, 'amount' => 24],
            ['base_currency_id' => 1, 'to_currency_id' => 2, 'base_amount' => 1, 'amount' => 0.91]
        ]);
        $eurExchangeRates = json_encode([
            ['base_currency_id' => 2, 'to_currency_id' => 3, 'base_amount' => 1, 'amount' => 21.54],
            ['base_currency_id' => 2, 'to_currency_id' => 1, 'base_amount' => 1, 'amount' => 1.1]
        ]);

        // Agregar los tipos de cambios a las compras de pickup hechas con USD apartir de 2020-04-09(fecha en que se cambio el tipo de cambio)
        Purchasable::whereHas('currency', function($query) {
                    $query->where('code', 'USD');
                })
                ->whereHas('command', function($query) {
                    $query->whereDate('created_at', '>=', '2020-04-09');
                })
                ->where('purchasable_type', 'App\Command')
                ->update([
                    'exchange_rates' => $usdExchangeRatesNew
                ]);
        // Agregar los tipos de cambios a las compras de pickup hechas con USD anteriores a 2020-04-09(fecha en que se cambio el tipo de cambio)
        Purchasable::whereHas('currency', function($query) {
                    $query->where('code', 'USD');
                })
                ->whereHas('command', function($query) {
                    $query->whereDate('created_at', '<', '2020-04-09');
                })
                ->where('purchasable_type', 'App\Command')
                ->update([
                    'exchange_rates' => $usdExchangeRates
                ]);

        // Agregar los tipos de cambios a las compras de cursos hechas con USD apartir 2020-04-09(fecha en que se cambio el tipo de cambio)
        Purchasable::whereHas('currency', function($query) {
                    $query->where('code', 'USD');
                })
                ->whereHas('commandCourse', function($query) {
                    $query->whereDate('created_at', '>=', '2020-04-09');
                })
                ->where('purchasable_type', 'App\CommandCourse')
                ->update([
                    'exchange_rates' => $usdExchangeRatesNew
                ]);
        // Agregar los tipos de cambios a las compras de cursos hechas con USD anteriores a 2020-04-09(fecha en que se cambio el tipo de cambio)
        Purchasable::whereHas('currency', function($query) {
                    $query->where('code', 'USD');
                })
                ->whereHas('commandCourse', function($query) {
                    $query->whereDate('created_at', '<', '2020-04-09');
                })
                ->where('purchasable_type', 'App\CommandCourse')
                ->update([
                    'exchange_rates' => $usdExchangeRates
                ]);

        // Agregar los tipos de cambios a las compras de hospedaje hechas con USD apartir 2020-04-09(fecha en que se cambio el tipo de cambio)
        Purchasable::whereHas('currency', function($query) {
                    $query->where('code', 'USD');
                })
                ->whereHas('commandHousing', function($query) {
                    $query->whereDate('created_at', '>=', '2020-04-09');
                })
                ->where('purchasable_type', 'App\CommandHousing')
                ->update([
                    'exchange_rates' => $usdExchangeRatesNew
                ]);
        // Agregar los tipos de cambios a las compras de hospedaje hechas con USD anteriores a 2020-04-09(fecha en que se cambio el tipo de cambio)
        Purchasable::whereHas('currency', function($query) {
                    $query->where('code', 'USD');
                })
                ->whereHas('commandHousing', function($query) {
                    $query->whereDate('created_at', '<', '2020-04-09');
                })
                ->where('purchasable_type', 'App\CommandHousing')
                ->update([
                    'exchange_rates' => $usdExchangeRates
                ]);

        // Agregar los tipos de cambios a las compras extras hechas con USD apartir 2020-04-09(fecha en que se cambio el tipo de cambio)
        Purchasable::whereHas('currency', function($query) {
                    $query->where('code', 'USD');
                })
                ->whereHas('purchase', function($query) {
                    $query->whereDate('created_at', '>=', '2020-04-09');
                })
                ->where('purchasable_type', 'App\Purchase')
                ->update([
                    'exchange_rates' => $usdExchangeRatesNew
                ]);
        // Agregar los tipos de cambios a las compras extras hechas con USD anteriores a 2020-04-09(fecha en que se cambio el tipo de cambio)
        Purchasable::whereHas('currency', function($query) {
                    $query->where('code', 'USD');
                })
                ->whereHas('purchase', function($query) {
                    $query->whereDate('created_at', '<', '2020-04-09');
                })
                ->where('purchasable_type', 'App\Purchase')
                ->update([
                    'exchange_rates' => $usdExchangeRates
                ]);
        // Agregar los tipos de cambios a las compras hechas con MXN
        Purchasable::whereHas('currency', function($query) {
                    $query->where('code', 'MXN');
                })
                ->update([
                    'exchange_rates' => $mxnExchangeRates
                ]);
        // Agregar los tipos de cambios a las compras hechas con EUR
        Purchasable::whereHas('currency', function($query) {
                    $query->where('code', 'EUR');
                })
                ->update([
                    'exchange_rates' => $eurExchangeRates
                ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchasables', function (Blueprint $table) {
            $table->dropColumn('exchange_rates');
        });
    }
}
