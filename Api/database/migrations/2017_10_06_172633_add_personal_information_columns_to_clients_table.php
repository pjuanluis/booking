<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPersonalInformationColumnsToClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->date('birth_date')->nullable();
            $table->string('phone')->nullable();
            $table->string('emergency_phone')->nullable();
            $table->smallInteger('has_alergies')->default(0);
            $table->string('alergies')->nullable();
            $table->smallInteger('has_diseases')->default(0);
            $table->string('diseases')->nullable();
            $table->smallInteger('is_smoker')->default(0);
            $table->string('fb_profile')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->dropColumn('birth_date');
            $table->dropColumn('phone');
            $table->dropColumn('emergency_phone');
            $table->dropColumn('has_alergies');
            $table->dropColumn('alergies');
            $table->dropColumn('has_diseases');
            $table->dropColumn('diseases');
            $table->dropColumn('is_smoker');
            $table->dropColumn('fb_profile');
        });
    }
}
