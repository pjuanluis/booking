<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommandHousingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('command_housing', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('command_id');
            $table->foreign('command_id')->references('id')->on('commands');
            $table->integer('housing_room_package_id');
            $table->foreign('housing_room_package_id')->references('id')->on('housing_room_packages');
            $table->date('from_date')->nullable();
            $table->date('to_date')->nullable();
            $table->integer('quantity');
            $table->string('unit')->default('day');
            $table->double('cost');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('command_housing');
    }
}
