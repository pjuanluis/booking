<?php

use App\Purchase;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommandIdColumnToPurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchases', function (Blueprint $table) {
            $table->integer('command_id')->unsigned()->nullable();

            $table->foreign('command_id')->references('id')->on('commands');
        });

        // Tratar de asignar las compras existentes a una commanda, si algún cliente tiene compras pero no tiene comandas, sus compras se perderán
        $purchases = Purchase::all();
        foreach ($purchases as $key => $purchase) {
            $purchasable = $purchase->purchasable;
            if (!empty($purchasable->client->commands)) {
                // Seleccionar los ids de las comandas que pertenecen al cliente del purchasable
                $arrayCommandsId = $purchasable->client->commands->pluck('id')->toArray();
                if ($arrayCommandsId) {
                    $purchase->update([
                        /*
                        Seleccionar de manera aleatoria un id de las comandas y asignarlo a la compra.
                        Esto se hace así porque no hay manera exacta de saber que compra va a pernenecer a que comanda,
                        ya que antes de implementar esta parte la compra estaba relacionada con un cliente, y un cliente
                        tiene muchas comandas.
                        */
                        'command_id' => $arrayCommandsId[array_rand($arrayCommandsId)]
                    ]);
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchases', function (Blueprint $table) {
            $table->dropColumn('command_id');
        });
    }
}
